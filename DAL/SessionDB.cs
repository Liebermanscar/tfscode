﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using BOL;

namespace DAL
{

    public class SessionDB : Database
    {
        public SessionDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(int userID, out int sessionID)
        {
            string serverMessage;
            //int sessionIDParam

            SqlParameter outParam = new SqlParameter("@UserSessionID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecords = this.ExecuteNonQuery("AddUserSessions",
                new SqlParameter[] { new SqlParameter("@UserID", userID), outParam },
                System.Data.CommandType.StoredProcedure
                , out serverMessage);
            if (noOfRecords > 0)
                sessionID = Convert.ToInt32(outParam.Value);
            else
                sessionID = 0;

            return noOfRecords > 0;
        }

        public ObservableCollection<UserSession> Get(int sessionID)
        {
            ObservableCollection<UserSession> sessions = new ObservableCollection<UserSession>();
            using (SqlDataReader dr = this.GetReader("GetUserSessions", new SqlParameter[] { new SqlParameter("@UserSessionID", sessionID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    sessions.Add(new UserSession()
                    {
                        SessionID = Convert.ToInt32(dr["UserSessionID"]),
                        SessionUser = new User()
                        {
                            UserID = Convert.ToInt32(dr["UserID"]),
                            UserName = dr["UserName"].ToString(),
                            UserType =(UserTypes)dr["UserType"],
                            UserCode = dr["UserCode"].ToString()
                        },
                        StartTime = dr["StartTime"] == DBNull.Value ? new DateTime?() : Convert.ToDateTime(dr["StartTime"]),
                        EndTime = dr["EndTime"] == DBNull.Value ? new DateTime?() : Convert.ToDateTime(dr["EndTime"])
                    });
                }
            }
            return sessions;
        }

        public static UserSession AddNewSession(int userID, out int sessionID)
        {
            //int userSessionID;
            UserSession userSession = null;
            using (SessionDB sessionDB = new SessionDB(null, null))
            {
                sessionDB.Add(userID, out sessionID);
                if (sessionID > 0)
                    userSession = sessionDB.Get(sessionID).FirstOrDefault();
            }
            return userSession;
        }
    }
}
