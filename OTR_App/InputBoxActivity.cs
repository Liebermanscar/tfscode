using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
    [Activity(Label = "Input Box")]
    public class InputBoxActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
			SetContentView(Resource.Layout.InputBox);

			EditText tabletName = FindViewById<EditText> (Resource.Id.tabletName);

			Button saveButton = FindViewById<Button> (Resource.Id.saveTabletName);

			saveButton.Click += (object sender, EventArgs e) => 
			{
				if(string.IsNullOrWhiteSpace(tabletName.Text))
					Toast.MakeText(this, "Please enter the Tablet's name", ToastLength.Short).Show();
				else
				{
					if (UIController.SaveTabletName(tabletName.Text))
						SetResult(Result.Ok);					
					else
						Toast.MakeText(this, "Tablet is not setup on the system.\n\rPlease contact the system admin (Issac) to add it.", ToastLength.Long).Show();
				}
			};
        }
    }
}