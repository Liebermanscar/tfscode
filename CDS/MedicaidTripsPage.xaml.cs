﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for MedicaidTripsPage.xaml
    /// </summary>
    public partial class MedicaidTripsPage : Page, IDisplayTextBlock
    {
        public MedicaidTripsPage()
        {
            InitializeComponent();
        }

        public MedicaidTripsPage(ObservableCollection<MedicaidTrip> medicaidTrips) : this()
        {
            // TODO: Complete member initialization
            this.MedicaidTrips = medicaidTrips;
        }

        public ObservableCollection<MedicaidTrip> MedicaidTrips
        {
            get
            {
                return this.MedicaidTripsDataGrid.DataContext as ObservableCollection<MedicaidTrip>;
            }
            set
            {
                this.MedicaidTripsDataGrid.DataContext = value;
            }
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Medicaid Trips";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
