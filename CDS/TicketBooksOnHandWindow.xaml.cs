﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for TicketBooksOnHandWindow.xaml
    /// </summary>
    public partial class TicketBooksOnHandWindow : Window
    {
        public TicketBooksOnHandWindow()
        {
            InitializeComponent();
        }
        public TicketBooksOnHandWindow(ObservableCollection<TicketBookStatus> ticketBookStatuses)
            : this()
        {
            try
            {

                if (ticketBookStatuses != null)
                    this.ActiveTicketBookStatuses = ticketBookStatuses;

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        public ObservableCollection<TicketBookStatus> ActiveTicketBookStatuses
        {
            get { return this.DataContext as ObservableCollection<TicketBookStatus>; }
            set { this.DataContext = value; }
        }
    }
}
