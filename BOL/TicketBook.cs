﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    public class TicketBook : BO, IEditableObject
    {
        struct TicketBookStruct
        {
            public int TicketBookID;
            public string BookName;
            public decimal BookPrice;
            public decimal TicketValue;
            public bool IsCompanyBook;
        }

        TicketBookStruct ticketBookData;
        TicketBookStruct backupData;
        private bool inTxn = false;
        public bool IsEditing
        {
            get { return this.inTxn; }
        }

        public TicketBook()
        {
            this.ticketBookData = new TicketBookStruct();
        }

        public int TicketBookID
        {
            get { return this.ticketBookData.TicketBookID; }
            set { this.ticketBookData.TicketBookID = value; }
        }


        public string BookName
        {
            get { return this.ticketBookData.BookName; }
            set { this.ticketBookData.BookName = value; }
        }

        public string BookNameID
        {
            get { return this.ticketBookData.BookName; }
            set { this.ticketBookData.BookName = value; }
        }

        public decimal BookPrice
        {
            get { return this.ticketBookData.BookPrice; }
            set { this.ticketBookData.BookPrice = value; }
        }

        public decimal TicketValue
        {
            get { return this.ticketBookData.TicketValue; }
            set { this.ticketBookData.TicketValue = value; }
        }

        public bool IsCompanyBook
        {
            get { return this.ticketBookData.IsCompanyBook; }
            set { this.ticketBookData.IsCompanyBook = value; }
        }

        public override string ToString()
        {
            return this.BookName;
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.BookName) &&
                    this.BookPrice > 0 &&
                    this.TicketValue > 0;
            }

        }

        #region IEditableObject Members

        public void BeginEdit()
        {
            if (!inTxn)
            {
                this.backupData = this.ticketBookData;
                this.inTxn = true;
                this.OnPropertyChanged("IsEditing");
            }
        }

        public void CancelEdit()
        {
            if (inTxn)
            {
                this.ticketBookData = backupData;
                inTxn = false;
                this.OnPropertyChanged("BookName");
                this.OnPropertyChanged("BookPrice");
                this.OnPropertyChanged("TicketValue");
                this.OnPropertyChanged("IsCompanyBook");
                this.OnPropertyChanged("IsEditing");
            }
        }

        public void EndEdit()
        {
            if (inTxn)
            {
                backupData = new TicketBookStruct();
                inTxn = false;
                this.OnPropertyChanged("IsEditing");
            }
        }

        #endregion
    }

    public class TicketsReceived : BO
    {
        int _dETicketReceivedID;

        public int DETicketReceivedID
        {
            get { return _dETicketReceivedID; }
            set { _dETicketReceivedID = value; }
        }
        TicketBook _fromTicketBook;

        public TicketBook FromTicketBook
        {
            get { return _fromTicketBook; }
            set
            {
                _fromTicketBook = value;
                this.TicketValue = value.TicketValue;
                this.OnPropertyChanged("FromTicketBook");
                this.OnPropertyChanged("TicketValue");
                this.OnPropertyChanged("AmountOfMissingTickets");

            }
        }
        decimal _ticketValue;

        public decimal TicketValue
        {
            get { return _ticketValue; }
            set
            {
                _ticketValue = value;
                this._totalValue = this.TicketValue * this.QtyReceived;
                this.OnPropertyChanged("TicketValue");
                this.OnPropertyChanged("AmountOfMissingTickets");
                this.OnPropertyChanged("TotalValue");
            }
        }

        int _qtyBooked;

        public int QtyBooked
        {
            get { return _qtyBooked; }
            set
            {
                _qtyBooked = value;
                this.OnPropertyChanged("QtyReceived");
                this.OnPropertyChanged("QtyBooked");
                //   this.OnPropertyChanged("");
                this.OnPropertyChanged("TotalValue");
                this.OnPropertyChanged("AmountOfMissingTickets");
                this.OnPropertyChanged("QtyMissing");
            }
        }


        int _qtyReceived;

        public int QtyReceived
        {
            get { return _qtyReceived; }
            set
            {
                _qtyReceived = value; this.OnPropertyChanged("QtyMissing"); this.OnPropertyChanged("QtyReceived");


                this.OnPropertyChanged("TotalValue");
                this.OnPropertyChanged("AmountOfMissingTickets");
            }
        }

        //public int QtyReceived
        //{
        //    get { return this.QtyBooked - this.QtyMissing; }
        //    //set
        //    //{
        //    //    _qtyReceived = value; 
        //    //    this._totalValue = this.TicketValue * this.QtyReceived;                
        //    //    this.OnPropertyChanged("TotalValue");
        //    //    this.OnPropertyChanged("NoOfMissingTicket");
        //    //}
        //}

        int _qtyMissing;

        public int QtyMissing
        {
            get { return this.QtyBooked - this.QtyReceived; }

            set { _qtyMissing = value; }

        }

        decimal _totalValue;

        public decimal TotalValue
        {
            get
            {
                //  return this.TicketValue * this.QtyBooked;
                decimal total;
                if (
                        //this.Owes <= 0 ?
                        //this.QtyBooked + Math.Abs(this.Owes) < this.QtyReceived : 
                        this.QtyBooked < this.QtyReceived
                    )
                    total = this.QtyBooked * this.TicketValue;
                else
                    total = this.QtyReceived * this.TicketValue;

                return total;
            }
            set { _totalValue = value; }
        }

        public decimal AmountOfMissingTickets
        {
            get
            {
                if (this.QtyMissing >= 0)
                    return this.TicketValue * this.QtyMissing;
                return 0;
            }
            set { _totalValue = value; }
        }

        public decimal TotalValueBooked
        {
            get { return this.QtyBooked * this.TicketValue; }
        }

        public decimal TotalValueRecived
        {
            get { return this.QtyReceived * this.TicketValue; }
        }

        public decimal TotalExtraSubmited
        {
            get
            {
                /* Old version wrong
                decimal val = 0;
                if (this.QtyReceived > this.QtyBooked)
                {
                    int over = this.QtyReceived - this.QtyBooked;
                    val = over * this.TicketValue;
                }
                return val;*/

                //fix as of 9/16/2014
                //if there is extra ticket in amount he ows give him full amount (cause was deducted from him)
                //if there is more then he ows or there is more tickets then booked (and ows nothing) take that qty * value and give half to driver
                //if ows is negative number so it was taken care already, ignore it
                decimal val = 0;

                if (this.QtyReceived > this.QtyBooked)
                {
                    int ows = this.Owes > 0 ? this.Owes : 0;
                    int over = this.QtyReceived - this.QtyBooked;

                    int paidOws = 0;
                    int extra = 0;

                    if (over <= ows)
                        paidOws = over;
                    else //over > ows
                    {
                        paidOws = ows;
                        extra = over - ows;
                    }

                    val = (extra * this.TicketValue);
                }
                return val;
            }
        }

        /// <summary>
        /// give full amount to driver
        /// </summary>
        public decimal TotalPaidOws
        {
            get
            {
                decimal val = 0;

                if (this.QtyReceived > this.QtyBooked)
                {
                    int ows = this.Owes > 0 ? this.Owes : 0;
                    int over = this.QtyReceived - this.QtyBooked;

                    int paidOws = 0;
                    int extra = 0;

                    if (over <= ows)
                        paidOws = over;
                    else //over > ows
                    {
                        paidOws = ows;
                        extra = over - ows;
                    }

                    val = (paidOws * this.TicketValue);
                }
                return val;
            }
        }

        /// <summary>
        /// extra tickets, need to give commission to driver as a regular trip (was probebly unreported trip)
        /// </summary>
        public decimal TotalExtraForCommission
        {
            get
            {
                decimal val = 0;

                if (this.QtyReceived > this.QtyBooked)
                {
                    int ows = this.Owes > 0 ? this.Owes : 0;
                    int over = this.QtyReceived - this.QtyBooked;

                    int paidOws = 0;
                    int extra = 0;

                    if (over <= ows)
                        paidOws = over;
                    else //over > ows
                    {
                        paidOws = ows;
                        extra = over - ows;
                    }

                    val = (extra * this.TicketValue);
                }
                return val;
            }
        }

        public int NoOfTripsNotReported
        {
            get
            {
                int paidOws = 0;
                int extra = 0;

                if (this.QtyReceived > this.QtyBooked)
                {
                    int ows = this.Owes > 0 ? this.Owes : 0;
                    int over = this.QtyReceived - this.QtyBooked;

                    if (over <= ows)
                        paidOws = over;
                    else //over > ows
                    {
                        paidOws = ows;
                        extra = over - ows;
                    }

                }
                return extra;
            }
        }
        /*   decimal _amountDriversMissingTicketsValue;

           public decimal AmountDriversMissingTicketsValue
           {
               get
               { 
                   return this.QtyMissing 
                       * this.TicketValue; 
               }
        
           }*/
        int _owes;

        public int Owes
        {
            get { return _owes; }
            set { _owes = value; this.OnPropertyChanged("Owes"); }


            /* get
             {
                 if (this.DETicketReceivedID == 0)
                     return 0;
                 return this.Owes;//(this.TripsIncome + this.TotalChargeTrips + this.TotalDriverTrips + (this.TicketsReceivedValue - this.TotalPaidForDriverWithTickets) - this.TotalPaidForDriverWithTickets) * Convert.ToDecimal((this.CommisionTypeUsed.DriversCommision / 100));
             }*/
            /*  get 
              {
                return _owes; }*/
            // set { _owes = value; }
        }

        public override bool AllowSave
        {
            get
            {
                throw new NotImplementedException();
            }

        }

        public int DailyEnvelopID { get; set; }

        ObservableCollection<DETicketLog> _deTicketLogs;

        public ObservableCollection<DETicketLog> DETicketLogs
        {
            get { return _deTicketLogs; }
            set { _deTicketLogs = value; }
        }
    }
}
