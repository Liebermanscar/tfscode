﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using AlertDialog = Android.Support.V7.App.AlertDialog;

namespace OTR_App
{
	[Activity (Label = "Leibermans", MainLauncher = true, Icon = "@drawable/Leib")]			
	public class StartupActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

            //UIController.ActiveTrip = new BOL_OTR.Trip_OTR() { Price = 7.50m };
            //var intent = new Intent(this, typeof(TripPaymentActivity));
            //StartActivityForResult(intent, 0);
            //return;
            //UIController.DisplayTripScreen();
            //return;

            UIController.StartListenToDispatchingService();

            Application.Context.BindService(new Intent(Application.Context, typeof(ListenToDispatchingService)),
                new ListenToDispatchingServiceConnection(null),
                Bind.AutoCreate);

            if (UIController.GetTabletName () == "")
				StartTabletNameActivity ();
			else {
				UIController.StarMainActivity ();
				Finish ();
			}

		}

		void StartTabletNameActivity()
		{
			var i = new Intent (this, typeof(InputBoxActivity));
			StartActivityForResult (i, 1);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if (requestCode == 1) {
				if (resultCode == Result.Ok) {
					UIController.StarMainActivity ();
					Finish ();
				} else if (resultCode == Result.Canceled) {

					var builder = new AlertDialog.Builder (this);
					builder.SetTitle ("Login");
					builder.SetMessage ("Please Login in order to be able to use the Liebermans OTR App");
					builder.SetPositiveButton ("Ok", delegate {
						StartTabletNameActivity ();
					});

					//builder.Create ();

					builder.Show ();


				}
			} 
		}
	}
}

