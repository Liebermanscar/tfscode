﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AddCommisionTypeWindow.xaml
    /// </summary>
    public partial class AddCommisionTypeWindow : Window
    {
        public AddCommisionTypeWindow()
        {
            InitializeComponent();
        }

        public AddCommisionTypeWindow(CommisionType commisionType) : this()
        {
            this.ActiveCommisionType = commisionType;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        public CommisionType ActiveCommisionType
        {
            get { return this.DataContext as CommisionType; }
            set { this.DataContext = value; }
        }
    }
}
