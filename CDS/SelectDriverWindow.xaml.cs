﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SelectDriverWindow.xaml
    /// </summary>
    public partial class SelectDriverWindow : Window
    {
        public SelectDriverWindow()
        {
            InitializeComponent();
            this.DriversListBox.Focus();
        }

        public SelectDriverWindow(ObservableCollection<Driver> drivers, string header, bool ShowSignInLocation = false) : this()
        {
            this.Title = header;

            this.DriversList = drivers;
            if (drivers.Count > 0)
                this.DriversListBox.SelectedIndex = 0;

            if (ShowSignInLocation)
                this.SignInLocationPanel.Visibility = System.Windows.Visibility.Visible;
            else
                this.SignInLocationPanel.Visibility = System.Windows.Visibility.Collapsed;
        }

        public ObservableCollection<Driver> DriversList
        {
            get { return this.DataContext as ObservableCollection<Driver>; }
            set { this.DataContext = value; }
        }

        public Driver SelectedDriver
        {
            get { return this.DriversListBox.SelectedItem as Driver; }
            set { this.DriversListBox.SelectedItem = value; }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void DriversListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedDriver != null)
                this.DialogResult = true;
        }
    }
}
