﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using Telerik.Windows.Controls.Map;
using System.ComponentModel;
using RadMap = Telerik.Windows.Controls.Map;

namespace MapApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BingGeocodeProvider geocodeProvider;
        DispatchingMapClient _dispatcherService;
        public static string BingMapsKey = Properties.Settings.Default.BingMapsKey;
        public static string MapSessionID;

        public MainWindow()
        {
            InitializeComponent();

            radMap.Provider = new BingMapProvider(MapMode.Road, true, BingMapsKey);
            DataContext = new DispatchingMapModel();

            _dispatcherService = new DispatchingMapClient(ActiveMapModel, TripAddedHandler);

            if (_dispatcherService.ConnectToDispatcherService())
            {
                var trips = _dispatcherService.GetAllTrips();

                foreach (var trip in trips.Where(t => t.DrivenBy == null))
                {
                    ActiveMapModel.Trips.Add(new TripOnMap() { Trip = trip });
                }

                var dti = _dispatcherService.GetAllDrivers();

                foreach (var driver in dti.Where(d => d.DriverStatus == BOL.DriverStatusOptions.NintyEight))
                {
                    ActiveMapModel.Drivers.Add(driver);
                }
            }

            InitializeGeoCodeProvider();
        }

        DispatchingMapModel ActiveMapModel
        {
            get { return DataContext as DispatchingMapModel; }
        }

        void TripAddedHandler(Trip trip)
        {
            RequstGeoCode(string.Format("{0} {1}", trip.FromLocation, "Broolyn, NY"), trip);
        }

        void InitializeGeoCodeProvider()
        {
            try
            {
                if (geocodeProvider == null)
                {
                    geocodeProvider = new BingGeocodeProvider();
                    geocodeProvider.ApplicationId = BingMapsKey;
                    geocodeProvider.MapControl = radMap;
                    geocodeProvider.GeocodeCompleted += geocodeProvider_GeocodeCompleted;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void geocodeProvider_GeocodeCompleted(object sender, GeocodeCompletedEventArgs e)
        {
            try
            {
                Trip trip = (Trip)e.Response.RequestUserData;
                var location = e.Response.Results.FirstOrDefault();

                if (location != null)
                {
                    var tripLocation = location.Locations.FirstOrDefault();

                    radMap.Center = new RadMap.Location(tripLocation.Latitude, tripLocation.Longitude);
                }

                if (e.Response.Results.Count > 1)
                {
                    var results = string.Join("\n", e.Response.Results.Select(r => r.DisplayName));
                    MessageBox.Show(string.Format("Address is not clear, there are multiple results\n{0}", results));
                }
                //else
                {
                    ActiveMapModel.AddTrip(trip, location.Locations.FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RequstGeoCode(string address, Trip trip)
        {
            try
            {
                GeocodeRequest request = new GeocodeRequest();
                request.Query = address;
                request.UserData = trip;
                geocodeProvider.GeocodeAsync(request);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    public class TripOnMap : INotifyPropertyChanged
    {
        private RadMap.Location location = RadMap.Location.Empty;

        public RadMap.Location Location
        {
            get
            {
                return location;
            }

            set
            {
                location = value;
                OnPropertyChanged("Location");
            }
        }

        public string Caption
        {
            get { return _Trip.FromLocation; }
        }

        public string ToolTipDisplay
        {
            get { return ""; }
        }

        public Trip Trip
        {
            get
            {
                return _Trip;
            }

            set
            {
                _Trip = value;
            }
        }

        Trip _Trip;

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class DispatchingMapModel
    {
        public DispatchingMapModel()
        {
            _Trips = new ObservableCollection<TripOnMap>();
            _Drivers = new ObservableCollection<DriverAndTripInformation>();
        }

        public void AddTrip(Trip trip)
        {
            _Trips.Add(new TripOnMap() { Trip = trip });
        }

        public void AddTrip(Trip trip, RadMap.Location location)
        {
            _Trips.Add(new TripOnMap() { Trip = trip, Location = location });
        }

        ObservableCollection<TripOnMap> _Trips;

        public ObservableCollection<TripOnMap> Trips
        {
            get
            {
                return _Trips;
            }

            set
            {
                _Trips = value;
            }
        }

        public ObservableCollection<DriverAndTripInformation> Drivers
        {
            get
            {
                return _Drivers;
            }

            set
            {
                _Drivers = value;
            }
        }

        ObservableCollection<DriverAndTripInformation> _Drivers;
    }
}
