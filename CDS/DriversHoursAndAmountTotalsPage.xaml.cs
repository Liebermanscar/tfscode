﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriversLogListPage.xaml
    /// </summary>
    public partial class DriversHoursAndAmountTotalsPage : Page, IDisplayTextBlock
    {
        public DriversHoursAndAmountTotalsPage()
        {
            InitializeComponent();
        }

        public ObservableCollection<DriverLogTotal> DriverLogTotals
        {
            get { return this.DataContext as ObservableCollection<DriverLogTotal>; }
            set { this.DataContext = value; }
        }



        private void buttonGetTotals_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.dpFromDate.SelectedDate == null || this.dpTillDate.SelectedDate == null)
                    return;
                this.DriverLogTotals = DAL.DriverLogTotalDB.GetDriverLogTotals(this.dpFromDate.SelectedDate.Value, this.dpTillDate.SelectedDate.Value);
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }


        #region IDisplayTextBlockMemebera
        public string HeaderText
        {
            get
            {
                return "Drivers Hours And Amount Totals Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            try
            {
                UIController.ExportTelerikGrid(this.DriverLogTotalsGrid, UIController.ExportFormat.Excel);
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

    }
}
