﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AssignDriversToScheduledTrips.xaml
    /// </summary>
    public partial class AssignDriversToScheduledTrips : Window
    {
        public AssignDriversToScheduledTrips()
        {
            InitializeComponent();

            this.DatePicker.SelectedDate = DateTime.Today;

            int UnConfirmdTripsOnly = 1;
            var scheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, this.DatePicker.SelectedDate, this.DatePicker.SelectedDate, this.DatePicker.SelectedDate.Value, false, TripAreaTypes.LongDistance, UnConfirmdTripsOnly, true);
            this.ActiveScheduledTrips = scheduledTrips;
        }


        private void ScheduledTripsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                if (e.EditAction == DataGridEditAction.Cancel)
                    return;

                e.Cancel = true;

                //  ScheduledTrip scheduledtrip = new ScheduledTrip();
                var scheduledtrip = e.Row.DataContext as ScheduledTrip;

                if (scheduledtrip.ScheduledTripID > 0)
                    e.Cancel = !DAL.ScheduledTripDB.EditScheduledTrip(scheduledtrip);
                else if (scheduledtrip.ScheduledTripID == 0)
                    return;
                //   e.Cancel = !DAL.ScheduledTripDB.AddScheduledTrip(scheduledtrip);


                /*
                if (e.EditAction == DataGridEditAction.Cancel)
                    return;

                e.Cancel = true;

                Trip trip = new Trip();
                trip.AttachedScheduledTrip = e.Row.DataContext as ScheduledTrip;

                if (trip.AttachedScheduledTrip != null)
                {
                    int fromStreetNameIndex = trip.AttachedScheduledTrip.PickupAddress.IndexOf(' ');
                    int toStreetNameIndex = trip.AttachedScheduledTrip.DropOffAddress.IndexOf(' ');
                    trip.FromLocationHouseNo = trip.AttachedScheduledTrip.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                    trip.FromLocationStreetName = trip.AttachedScheduledTrip.PickupAddress.Substring(fromStreetNameIndex + 1);
                    trip.ToLocationHouseNo = trip.AttachedScheduledTrip.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                    trip.ToLocationStreetName = trip.AttachedScheduledTrip.DropOffAddress.Substring(toStreetNameIndex + 1);

                    DateTime newDateTime = trip.AttachedScheduledTrip.PickupDate.Date.Add(TimeSpan.Parse(trip.AttachedScheduledTrip.PickupTime.Value.ToString("HH:mm:ss")));
                    trip.TripStarted = newDateTime;

                    trip.TimeOfCall  = trip.AttachedScheduledTrip.DateAdded;
                
                    trip.DrivenBy = new Driver()
                    {
                        DriverID = trip.AttachedScheduledTrip.AssignedToDriverID
                    };
                    trip.ETAToLocation1 = trip.AttachedScheduledTrip.ETAToLocation1;
                    trip.ETABackToBase = trip.AttachedScheduledTrip.ETABackToBase;
                    trip.TripPrice = trip.AttachedScheduledTrip.TripPrice;
                    trip.ExtraCharge = trip.AttachedScheduledTrip.ExtraCharge;
                    trip.MiniVanCharge = trip.AttachedScheduledTrip.MiniVanCharge;
                    trip.WaitingCharge = trip.AttachedScheduledTrip.WaitingCahrge;
                    trip.TripAreaType = trip.AttachedScheduledTrip.TripAreaType;
                    trip.TripStatus = TripStatusTypes.Assigned;
                    trip.TripID = trip.AttachedScheduledTrip.TripID;

                }
 
                if (trip.DrivenBy.DriverID==0)
                    return;
                if ( trip.TripID> 0)
                    e.Cancel = !DAL.TripDB.EditTrip(trip);
                else if (trip.TripID == 0)
                    e.Cancel = !DAL.TripDB.AddTrip(trip);*/
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.SearchTextBox.Text))
                this.ScheduledTrips = this.ActiveScheduledTrips;
            else

                this.ScheduledTrips = new ObservableCollection<ScheduledTrip>(this.ActiveScheduledTrips.Where(ft => ft.CustomerPhoneNo.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())).ToList());
            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }


        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            //          CbLongDistanceTrips
            TripAreaTypes tripAreType;
            if (CbLongDistanceTrips.IsChecked != true)
                tripAreType = TripAreaTypes.Local;
            else
                tripAreType = TripAreaTypes.LongDistance;
            int UnConfirmdTripsOnly = 2;
            if (this.CbUnConfirmdTripsOnly.IsChecked == true)
                UnConfirmdTripsOnly = 1;
            var scheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, this.DatePicker.SelectedDate, this.DatePicker.SelectedDate, this.DatePicker.SelectedDate.Value, false, tripAreType, UnConfirmdTripsOnly, true);
            this.ActiveScheduledTrips = scheduledTrips;
        }

        ObservableCollection<ScheduledTrip> ActiveScheduledTrips
        {
            get { return this.DataContext as ObservableCollection<ScheduledTrip>; }
            set { this.DataContext = value; }
        }

        ObservableCollection<ScheduledTrip> ScheduledTrips
        {
            get { return this.DataContext as ObservableCollection<ScheduledTrip>; }
            set { this.DataContext = value; }
        }

        ScheduledTrip SelectedScheduledTrip
        {
            get { return this.ScheduledTripsDataGrid.SelectedItem as ScheduledTrip; }
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

            try
            {


                if (e.Key == Key.Insert)
                {
                    if (SelectedScheduledTrip == null)
                        return;
                    SelectedScheduledTrip.NeedToConfirm = false;
                    if (SelectedScheduledTrip.ScheduledTripID > 0)
                    {
                        if (SelectedScheduledTrip.PickupTime == null)
                            SelectedScheduledTrip.PickupTime = DateTime.Now;
                        DAL.ScheduledTripDB.EditScheduledTrip(SelectedScheduledTrip);
                    }
                    else if (SelectedScheduledTrip.ScheduledTripID == 0)
                        return;


                }
                if (e.Key == Key.Escape)
                    this.DialogResult = true;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void mConvertToTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (SelectedScheduledTrip == null)
                    return;
                if (SelectedScheduledTrip.ScheduledTripID > 0)
                {
                    SelectedScheduledTrip.NeedToConfirm = false;
                    if (SelectedScheduledTrip.PickupTime == null)
                        SelectedScheduledTrip.PickupTime = DateTime.Today;
                    DAL.ScheduledTripDB.EditScheduledTrip(SelectedScheduledTrip);
                }
                else if (SelectedScheduledTrip.ScheduledTripID == 0)
                    return;

                DAL.ScheduledTripDB.ProcessScheduledTripsAsTrips(SelectedScheduledTrip.ScheduledTripID);
            }
            catch (Exception Ex)
            {

                UIController.DisplayErrorMessage(Ex);
            }
        }

        private void mCancelTrip_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedScheduledTrip == null)
                return;
            if (SelectedScheduledTrip.ScheduledTripID > 0)
            {
                SelectedScheduledTrip.IsCanceldTrip = true;
                var trip = DAL.TripDB.GetTrips(0, 0, 0, 0, null, null, false, null, 0, 0, false, 0, false, 0, SelectedScheduledTrip.ScheduledTripID).FirstOrDefault();
                if (trip != null)
                {
                    if (trip.TripEnded != null)
                        if (MessageBox.Show(string.Format("Trip Was Driven \nBy Unit {0}\nDo you still want to cancel trip?", trip.DrivenBy.DriverCode), "Trip Completed", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) != MessageBoxResult.Yes)
                            return;
                }

                DAL.ScheduledTripDB.EditScheduledTrip(SelectedScheduledTrip);
            }
        }
    }
}

