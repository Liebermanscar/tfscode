﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace DAL
{
    public class DriverStatusLogDB : Database
    {
        public DriverStatusLogDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public ObservableCollection<DriverStatusLog> Get(DateTime? TimeLogged1, DateTime? TimeLogged2, int driveID)
        {
            ObservableCollection<DriverStatusLog> driverStatusLogs = new ObservableCollection<DriverStatusLog>();
            using (SqlDataReader dr = this.GetReader("GetDriverStatusLogs", new SqlParameter[] {
                new SqlParameter("@TimeLogged1", TimeLogged1),
                new SqlParameter("@TimeLogged2", TimeLogged2),
                new SqlParameter("@DriveID", driveID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    driverStatusLogs.Add(new DriverStatusLog()
                    {
                        DriverStatusLogID = Convert.ToInt32(dr["DriverStatusLogID"]),
                        UnitDriver = new Driver()
                        {
                            DriverID = Convert.ToInt32(dr["DriverID"]),
                            DriverCode = dr["DriverCode"].ToString()
                        },
                        DriverStatusType = (DriverStatusOptions)dr["DriverStatusType"],
                        TimeLogged = Convert.ToDateTime(dr["TimeLogged"])
                    });
                }
            }
            return driverStatusLogs;
        }
        public static ObservableCollection<DriverStatusLog> GetDriverStatusLogs(DateTime? TimeLogged1, DateTime? TimeLogged2, int driveID = 0)
        {
            using (DriverStatusLogDB dslDb = new DriverStatusLogDB(null, null))
            {
                return dslDb.Get(TimeLogged1, TimeLogged2, driveID);
            }
        }

        public bool Add(DriverStatusLog driverStatusLog)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@DriverStatusLogID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddDriverStatusLogs", new SqlParameter[]
            {

                new SqlParameter("@DriverStatusType",driverStatusLog.DriverStatusType),
                new SqlParameter("@DriverID", driverStatusLog.DriverID),
                new SqlParameter("@TimeLogged",driverStatusLog.TimeLogged),
                new SqlParameter("@SessionID",Database.SessionID),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                driverStatusLog.DriverStatusLogID = Convert.ToInt32(outParam.Value);
            return noOfRecordsAffected > 0;
        }
        public static bool AddDriverStatusLog(DriverStatusLog driverStatusLog)
        {
            using (DriverStatusLogDB driverStatusLogDB = new DriverStatusLogDB(null, null))
            {
                return driverStatusLogDB.Add(driverStatusLog);
            }
        }
    }

    public class DriverLogTotalDB : Database
    {
        public DriverLogTotalDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }


        public ObservableCollection<DriverLogTotal> Get(DateTime fromDate, DateTime tillDate)
        {
            var driverLogTotals = new ObservableCollection<DriverLogTotal>();
            using (SqlDataReader dr = this.GetReader("GetDriversLogTotals",
                new SqlParameter[]
                {
                    new SqlParameter("@FromDate", fromDate),
                    new SqlParameter("@TillDate",tillDate)
                },

                    System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    driverLogTotals.Add(new DriverLogTotal()
                    {
                        DateLoged = Convert.ToDateTime(dr["DateLoged"]),
                        LogedDriver = new Driver()
                        {
                            DriverID = Convert.ToInt32(dr["DriverID"]),
                            DriverCode = dr["DriverCode"].ToString(),
                            DriverFirstName = dr["DriverFirstName"].ToString(),
                            DriverLastName = dr["DriverLastName"].ToString()

                        },
                        TotalWorked = (TimeSpan)(dr["TotalWorked"]),
                        TotalWorkedWithBreaks = (TimeSpan)(dr["TotalWorkedWithBreaks"]),
                        QtySmallBreaks = Convert.ToInt32(dr["QtySmallBreaks"]),
                        TotalSmallBreaks = (TimeSpan)(dr["TotalSmallBreaks"]),
                        QtyLongBreaks = Convert.ToInt32(dr["QtyLongBreaks"]),
                        TotalLongBreaks = (TimeSpan)(dr["TotalLongBreaks"]),
                        TotalAmountBooked = Convert.ToDecimal(dr["TotalAmountBooked"])

                    });
                }

                return driverLogTotals;
            }
        }
        public static ObservableCollection<DriverLogTotal> GetDriverLogTotals(DateTime fromDate, DateTime tillDate)
        {
            using (DriverLogTotalDB driverStatusLogTotalDB = new DriverLogTotalDB(null, null))
            {
                return driverStatusLogTotalDB.Get(fromDate, tillDate);
            }
        }
    }

}
