﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTRWebApi.Controllers
{
    //[Authorize]
    public class DriverController : ApiController
    {
        //GET: api/Driver
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //GET: api/Driver/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet]
        public string SignIn(string userName, string password, string tabletName)
        {
            string message = "";
            int driverID = 0;
            var result = DAL_EF.DAL_OTR.DriverDb.DriverLogIn(userName, password, tabletName, ref message, ref driverID );
            if (result == DAL_EF.DbSaveResult.Success)
                return "DriverID: " + driverID.ToString();
            else if (result == DAL_EF.DbSaveResult.Faliure)
                return message;
            else
                return "ERROR";
        }

        [HttpGet]
        public string IsTabletNameInSystem(string tabletName)
        {
            return DAL_EF.DAL_OTR.TabletDb.IsTabletNameInSystem(tabletName).ToString();
        }

        [HttpGet]
        public string GetDailyDriverTotal(int driverID, DateTime forDate)
        {
            return DAL_EF.DAL.DriverDb.GetDailyDriverTotal(driverID, forDate);
        }

        [HttpGet]
        public string SignOut(int driverID, string tabletName)
        {
            string message = "";
            var result = DAL_EF.DAL_OTR.DriverDb.DriverLogOut(driverID, tabletName, message);
            if (result == DAL_EF.DbSaveResult.Success)
                return "SIGNED_OUT";
            else if (result == DAL_EF.DbSaveResult.Faliure)
                return message;
            else
                return "ERROR";
        }

        [HttpGet]
        public IHttpActionResult GetDriverInfo(int driverID)
        {
            var driverInfo = DAL_EF.DAL.DriverDb.GetDriverInfo(driverID);
            if(driverInfo == null)
            {
                return NotFound();
            }

            return Ok(driverInfo);
        }   
        
            

        // POST: api/Driver
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Driver/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Driver/5
        public void Delete(int id)
        {
        }
    }
}
