﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ScheduledTripsPage.xaml
    /// </summary>
    public partial class ScheduledTripsPage : Page, IDisplayTextBlock
    {
        public ScheduledTripsPage()
        {
            InitializeComponent();
        }

        public ScheduledTripsPage(ObservableCollection<ScheduledTrip> scheduledTrips) : this()
        {
            if (scheduledTrips == null)
                scheduledTrips = new ObservableCollection<ScheduledTrip>();
            this.ScheduleTripsList = scheduledTrips;
        }

        private void ScheduledTripsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {


                if (e.EditAction == DataGridEditAction.Cancel)
                    return;

                e.Cancel = true;

                if (this.SelectedScheduledTrip == null || !this.SelectedScheduledTrip.AllowSave)
                    return;



                if (this.SelectedScheduledTrip.ScheduledTripID == 0)
                    switch (this.SelectedScheduledTrip.ScheduledTripType)
                    {
                        case ScheduledTripTypes.ByCustomer:
                            e.Cancel = !DAL.ScheduledTripDB.AddScheduledTrip(this.SelectedScheduledTrip);
                            break;
                        case ScheduledTripTypes.ByMedicaid:
                            e.Cancel = !DAL.MedicaidTripDB.AddMedicaidTrips(new ObservableCollection<MedicaidTrip>()
                            {
                                new MedicaidTrip()
                            {
                                CIN = this.SelectedScheduledTrip.CardNo,
                                LastName = this.SelectedScheduledTrip.CustomerName,
                                FirstName = "",
                                MiddleInitial = "",
                                PhoneNo = this.SelectedScheduledTrip.CustomerPhoneNo,
                                DaysApproved = 1,
                                PickUpDate = this.SelectedScheduledTrip.PickupDate,
                                PickUpTime = this.SelectedScheduledTrip.PickupTime.GetValueOrDefault(DateTime.Now).TimeOfDay,
                                PickUpAddress = this.SelectedScheduledTrip.PickupAddress,
                                PickUpCity = this.SelectedScheduledTrip.PickupCity,
                                PickupState = this.SelectedScheduledTrip.PickupState,
                                PickUpZip = this.SelectedScheduledTrip.PickupZip,
                                DropOffAddress = this.SelectedScheduledTrip.DropOffAddress,
                                DropOffCity = this.SelectedScheduledTrip.DropOffCity,
                                DropOffState = this.SelectedScheduledTrip.DropOffState,
                                DropOffZip = this.SelectedScheduledTrip.DropOffZip

                            }});
                            break;

                        case ScheduledTripTypes.ByFidelis:
                            e.Cancel = !DAL.FidelisTripDB.AddFidelisTrips(new ObservableCollection<FidelisTrip>()
                            {
                                new FidelisTrip()
                                {
                                    MemberID = this.SelectedScheduledTrip.CardNo,
                                    TripScheduledOn = this.SelectedScheduledTrip.PickupDate.Add(this.SelectedScheduledTrip.PickupTime.Value.TimeOfDay),
                                    MemberName = this.SelectedScheduledTrip.CustomerName,
                                    MemberPhoneNo = this.SelectedScheduledTrip.CustomerPhoneNo,
                                    MemberAddress = this.SelectedScheduledTrip.PickupAddress,
                                    MemberCityStatePostalCode = this.SelectedScheduledTrip.PickupCity + ", " + this.SelectedScheduledTrip.PickupState + "  " + this.SelectedScheduledTrip.PickupZip,
                                    DestinationAddress = this.SelectedScheduledTrip.DropOffCityStatePostalCode
                                }
                            });
                            break;
                        default:
                            break;
                    }
                else
                    e.Cancel = !DAL.ScheduledTripDB.EditScheduledTrip(this.SelectedScheduledTrip);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                e.Cancel = true;
            }
        }



        public ObservableCollection<ScheduledTrip> ScheduleTripsList
        {
            get
            {
                return this.DataContext as ObservableCollection<ScheduledTrip>;
            }
            set
            {
                this.DataContext = value;
            }
        }

        ScheduledTrip SelectedScheduledTrip
        {
            get { return this.ScheduledTripsDataGrid.SelectedItem as ScheduledTrip; }
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Scheduled Trips";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ScheduleTripsList = DAL.ScheduledTripDB.GetScheduledTrips(0,
                    null,
                    this.FromDatePicker.SelectedDate.Value.Date,
                    this.ToDatePicker.SelectedDate.Value.Date,
                    false, (TripAreaTypes)this.TripAreaTypesComboBox.SelectedItem);
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                this.SelectedScheduledTrip.ScheduledByCustomer = UIController.SelectCustomerFromList(null);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void ScheduledTripsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.OpenFidelisMedicaidTripInfo();
        }

        private void OpenFidelisMedicaidTripInfo()
        {
            try
            {
                if (this.SelectedScheduledTrip == null ||
                 this.SelectedScheduledTrip.ScheduledTripType == ScheduledTripTypes.ByCustomer)
                    return;

                if (this.SelectedScheduledTrip.ScheduledTripType == ScheduledTripTypes.ByMedicaid)
                    UIController.ShowAddEditMedicaidTripWindow(DAL.MedicaidTripDB.GetMedicaidTrips(0,
                        this.SelectedScheduledTrip.FidelisMedicaidReferencNo,
                        this.SelectedScheduledTrip.PickupDate).FirstOrDefault());
                else if (this.SelectedScheduledTrip.ScheduledTripType == ScheduledTripTypes.ByFidelis)
                    UIController.ShowAddEditFidelisTripWindow(DAL.FidelisTripDB.GetFidelisTrips(this.SelectedScheduledTrip.FidelisMedicaidReferencNo, this.SelectedScheduledTrip.PickupDate).FirstOrDefault());
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                var grid = this.ScheduledTripsDataGrid;
                grid.SelectAllCells();
                grid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, grid);

                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "Scheduled Trips"; // Default file name
                dlg.DefaultExt = ".xls"; // Default file extension
                dlg.Filter = "Excel|*.xls"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process save file dialog box results
                if (result == true)
                {
                    // Save document
                    string filename = dlg.FileName;

                    System.IO.File.WriteAllText(filename, Clipboard.GetText());

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (this.SelectedScheduledTrip == null)
                    return;

                Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo,
                    (sender as TextBox).Text, DataRequestTypes.LiteData, false).FirstOrDefault();

                if (cust != null)
                {
                    this.SelectedScheduledTrip.ScheduledByCustomer = cust;
                    this.SelectedScheduledTrip.CustomerName = this.SelectedScheduledTrip.ScheduledByCustomer.CustomerName;
                    this.SelectedScheduledTrip.PickupAddress = this.SelectedScheduledTrip.ScheduledByCustomer.CustomersAddress;
                    this.SelectedScheduledTrip.CustomerPhoneNo = this.SelectedScheduledTrip.ScheduledByCustomer.HomePhoneNo;
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedScheduledTrip == null)
                    return;

                UIController.LoadCustomerInfoPage(DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID,
                        this.SelectedScheduledTrip.ScheduledByCustomer.CustomerID.ToString(),
                        DataRequestTypes.FullData, false).FirstOrDefault());
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.OpenFidelisMedicaidTripInfo();
        }
    }
}
