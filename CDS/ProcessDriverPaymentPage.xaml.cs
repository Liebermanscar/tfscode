﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriverPayment.xaml
    /// </summary>
    public partial class ProcessDriverPaymentPage : Page, IDisplayTextBlock
    {
        public bool loadAllTransactions = true;

        public ProcessDriverPaymentPage()
        {
            try
            {
                InitializeComponent();

                this.ActiveDriverPayment = new BOL.DriverPayment();
                this.Drivers = new ObservableCollection<Driver>(DAL.DriversDB.GetDrivers(true).Where(d => d.IsMain).ToList());
                this.driversComboBox.ItemsSource = this.Drivers;
                this.ActiveDriverPayment.CashTotal = DAL.DriverPaymentDB.GetCashTotal();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void driversComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            this.LoadData();
        }

        void LoadData()
        {
            try
            {
                //  if(loadAllTransactions==true)
                if (this.ActiveDriverPayment != null && this.ActiveDriverPayment.DriverID > 0)//if selected a driver 
                    GetEnvelopes();
                // e.Handled = true;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void SaveDriverPaymentButton_Click(object sender, RoutedEventArgs e)
        {
            loadAllTransactions = false;
            SaveDriverPayment();

        }

        void ProcessNewDriverPayment()
        {
            try
            {
                this.ActiveDriverPayment = new BOL.DriverPayment() { ObjectState = BOState.New };

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            CancelDriverPayment();
        }

        private void ButtonShowAllOpenEnvelopes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loadAllTransactions = true;
                GetAllOpenEnvelopes();
                this.btnSaveDriverPayment.IsEnabled = false;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void GetAllOpenEnvelopes()
        {
            try
            {


                this.ActiveDriverPayment.EnvelopesOpen = DAL.DailyEnvelopeDB.GetDailyEnvelopes(0, 0, null, true, 0);
                this.ActiveDriverPayment.DriverExpenses = DAL.DriverExpenseDB.GetDriverExpense(0, 0, true, 0);
                this.ActiveDriverPayment.ChargeToDriverTrips = DAL.TripDB.GetTrips(0, 0, 0, 0, null, ActiveDriverPayment.DatePaidTill, false, null, 0, 0, true, 0, true);
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void GetEnvelopes()
        {
            try
            {
                this.ActiveDriverPayment.EnvelopesOpen = DAL.DailyEnvelopeDB.GetDailyEnvelopes(0, this.ActiveDriverPayment.DriverID, this.ActiveDriverPayment.DatePaidTill, true, 0);
                this.ActiveDriverPayment.DriverExpenses = DAL.DriverExpenseDB.GetDriverExpense(this.ActiveDriverPayment.DriverID, 0, true, 0);
                this.ActiveDriverPayment.ChargeToDriverTrips = DAL.TripDB.GetTrips(0, 0, 0, 0, null, null, false, null, this.ActiveDriverPayment.DriverID, 0, true, 0, true, withLinkedDriverTrips: true);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        void SaveDriverPayment()
        {
            try
            {
                DAL.DriverPaymentDB.AddDriverPayment(this.ActiveDriverPayment, this.chkIncludeBonus.IsChecked.GetValueOrDefault());
                ProcessNewDriverPayment();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
        void CancelDriverPayment()
        {
            try
            {
                string message;
                if (this.ActiveDriverPayment.ObjectState == BOState.New)
                    message = "You're about to cancel the Active Driver Payment\n\nContinue?";
                else
                    message = "You're about to cancel the changes made to the ActiveDriver Payment\n\nContinue?";
                if (MessageBox.Show(message, "Cancel", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
                    this.ProcessNewDriverPayment();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        public DriverPayment ActiveDriverPayment
        {
            get { return this.DataContext as BOL.DriverPayment; }
            set { this.DataContext = value; }
        }


        ObservableCollection<Driver> Drivers { get; set; }
        #region IDisplayTextBlock Members
        public string HeaderText
        {
            get
            {
                return "Driver Payment";
                // throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
        public BOState ObjectState { get; set; }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                    return;

                switch (e.Key)
                {

                    case Key.F1:
                        this.SaveDriverPayment();
                        e.Handled = true;
                        break;
                    case Key.F2:
                        this.CancelDriverPayment();
                        e.Handled = true;
                        break;

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void EnvelopesGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {

        }


        DailyEnvelope SelectedEnvelope
        {
            get { return this.EnvelopesGrid.SelectedItem as DailyEnvelope; }
        }

        private void EnvelopesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {

                if (SelectedEnvelope != null)
                {
                    UIController.LoadProcessDailyEnvelopesPage(DAL.DailyEnvelopeDB.GetDailyEnvelopes(SelectedEnvelope.DailyEnvelopeID, 0, null, false, 0)[0]);
                }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void MenuItemSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var EG = this.EnvelopesGrid.Items.Cast<DailyEnvelope>();

                foreach (var DE in EG)
                {
                    if (sender == this.SelectAllMenuItem)
                    {
                        DE.Selected = true;

                    }
                    else
                        DE.Selected = false;
                }
                var TCFTD = this.TripsChargedForThisDriver.Items.Cast<Trip>();

                foreach (var T in TCFTD)
                {
                    if (sender == this.SelectAllMenuItem)
                    {
                        T.Selected = true;

                    }
                    else
                        T.Selected = false;
                }
                var EXFTD = this.ExpensesForThisDriver.Items.Cast<DriverExpense>();

                foreach (var DEX in EXFTD)
                {
                    if (sender == this.SelectAllMenuItem)
                    {
                        DEX.Selected = true;

                    }
                    else
                        DEX.Selected = false;
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void MenuItemUnSelectAll_Click(object sender, RoutedEventArgs e)
        {/*
          EnvelopesGrid
          * TripsChargedForThisDriver
          * ExpensesForThisDriver
          
          
          
          */
            try
            {
                var EG = this.EnvelopesGrid.Items.Cast<DailyEnvelope>();

                foreach (var DE in EG)
                {
                    if (sender == this.UnSelectAllMenuItem)
                    {
                        DE.Selected = false;

                    }
                    else
                        DE.Selected = true;
                }
                var TCFTD = this.TripsChargedForThisDriver.Items.Cast<Trip>();

                foreach (var T in TCFTD)
                {
                    if (sender == this.UnSelectAllMenuItem)
                    {
                        T.Selected = false;

                    }
                    else
                        T.Selected = true;
                }
                var EXFTD = this.ExpensesForThisDriver.Items.Cast<DriverExpense>();

                foreach (var DEX in EXFTD)
                {
                    if (sender == this.UnSelectAllMenuItem)
                    {
                        DEX.Selected = false;

                    }
                    else
                        DEX.Selected = true;
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            this.LoadData();
        }





    }
}
