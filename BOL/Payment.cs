﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CCProc;

namespace BOL
{
    public class Payment : BO, IEditableObject
    {
        public Payment()
        {
            this.DatePaid = DateTime.Now;
            this.IsCreditable = true;
        }
        int _paymentID;

        public int PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }

        int _customerID;

        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }
        Customer _customer;

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        Trip _paidForTrip;

        public Trip PaidForTrip
        {
            get { return _paidForTrip; }
            set { _paidForTrip = value; }
        }

        public string DrivenBy
        {
            get
            {
                if (PaidForTrip == null || PaidForTrip.DrivenBy == null)
                    return "";

                return _paidForTrip.DrivenBy.ToString();
            }
        }

        DateTime _datePaid;

        public DateTime DatePaid
        {
            get { return _datePaid; }
            set { _datePaid = value; }
        }
        decimal _amountPaid;

        public decimal AmountPaid
        {
            get { return _amountPaid; }
            set { _amountPaid = value; }
        }
        PaymentMethods _paymentMethod;

        public PaymentMethods PaymentMethod
        {
            get { return _paymentMethod; }
            set { _paymentMethod = value; }
        }
        string _referenceNo;

        public string ReferenceNo
        {
            get { return _referenceNo; }
            set { _referenceNo = value; }
        }

        string _paymentComments;

        public string PaymentComments
        {
            get { return _paymentComments; }
            set { _paymentComments = value; }
        }

        CCProc.CCInfo _ccInfo;

        public CCProc.CCInfo CcInfo
        {
            get { return _ccInfo; }
            set { _ccInfo = value; }
        }
        bool _isProcessed;

        public bool IsProcessed
        {
            get { return _isProcessed; }
            set { _isProcessed = value; }
        }

        bool _isCreditable;

        public bool IsCreditable
        {
            get { return _isCreditable; }
            set { _isCreditable = value; }
        }

        decimal _bookFee;

        public decimal BookFee
        {
            get { return _bookFee; }
            set { _bookFee = value; }
        }

        public override bool AllowSave
        {
            get
            {

                //    return true;

                return this.DatePaid != null && this.AmountPaid > 0 && this.PaymentMethod != 0;
                //  && !string.IsNullOrWhiteSpace(this.PaymentMethod);
                // throw new NotImplementedException();
            }



        }
        //  public object CustomerID { get; set; }
    }

    public class CCOnFile : BO
    {
        public CCOnFile()
        {
            this.CcInformation = new CCProc.CCInfo();

        }
        int _customerID;

        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }
        int _ccID;

        public int CcID
        {
            get { return _ccID; }
            set { _ccID = value; }
        }
        bool _default;

        public bool Default
        {
            get { return _default; }
            set { _default = value; }
        }
        CCProc.CCInfo _ccInformation;

        public CCProc.CCInfo CcInformation
        {
            get { return _ccInformation; }
            set { _ccInformation = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return this.CcInformation.CCNumber != null && this.CcInformation.ExpDate != null;
                //   return this.CcInformation != null;
            }

        }
    }



    public class CreditCardResponse : BO
    {
        public CreditCardResponse()
        {
            //  this.CcInformation = new CCProc.CCInfo();
            this.CCResponse = new CCProc.TransactionResponse();

        }
        int _customerID;

        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }
        int _ccID;

        public int CcID
        {
            get { return _ccID; }
            set { _ccID = value; }
        }
        int _responseID;

        public int ResponseID
        {
            get { return _responseID; }
            set { _responseID = value; }
        }


        CCProc.TransactionResponse _ccResponse;

        public CCProc.TransactionResponse CCResponse
        {
            get { return _ccResponse; }
            set { _ccResponse = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return this.CCResponse.TransactionNo != null && this.CcID != null;

            }

        }
    }

    public enum PaymentMethods
    {
        Cash = 1,
        Check = 2,
        CreditCard = 3,
        Other = 4
    }
}
