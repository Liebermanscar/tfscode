﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using BOL;
using System.Data;

namespace DAL
{
    public class DETicketLogDB : Database
    {
        public DETicketLogDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(ObservableCollection<DETicketLog> deTicketLogs)
        {
            if (this.Transaction == null)
            {
                this._localTransaction = true;
                this.Transaction = this.Connection.BeginTransaction();
            }
            int noOfRowsInsetrted = 0;
            int totalNoOfRowsAdded = 0;
            try
            {
                foreach (DETicketLog tl in deTicketLogs)
                {
                    string serverMessage;
                    var outParam = new SqlParameter("@TicketLogId", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    noOfRowsInsetrted = this.ExecuteNonQuery("AddDETicketLog", new SqlParameter[]
                    {
                        new SqlParameter("@TicketBookId", tl.TicketBookId),
                        new SqlParameter("@TicketNumber", tl.TicketNumber),
                        new SqlParameter("@DailyEnvelopeId", tl.DailyEnvelopeId),
                          outParam
                    }, System.Data.CommandType.StoredProcedure, out serverMessage);
                    if (noOfRowsInsetrted > 0)
                        tl.TicketLogId = Convert.ToInt32(outParam.Value);
                    totalNoOfRowsAdded++;
                }

                if (this._localTransaction)
                    this.Transaction.Commit();

                return totalNoOfRowsAdded > 0;
            }
            catch (Exception ex)
            {
                if (this._localTransaction)
                    this.Transaction.Rollback();
                throw;
            }
            finally
            {
                if (this._localTransaction)
                    this.Transaction.Dispose();
            }
        }

        public static bool AddDETicketLog(ObservableCollection<DETicketLog> deTicketLogs)
        {
            using (DETicketLogDB tlDB = new DETicketLogDB(null, null))
            {
                return tlDB.Add(deTicketLogs);
            }
        }

        public ObservableCollection<DETicketLog> Get(int ticketBookId, int dailyEnvelopeId)
        {
            var deTicketLogs = new ObservableCollection<DETicketLog>();
            using (SqlDataReader dr = this.GetReader("GetDETicketLog", new SqlParameter[]
            {
                new SqlParameter("@TicketBookId",ticketBookId),
                new SqlParameter("@DailyEnvelopeId", dailyEnvelopeId)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var deTicketLog = new DETicketLog()
                    {
                        TicketLogId = Convert.ToInt32(dr["TicketLogId"]),
                        TicketBookId = Convert.ToInt32(dr["TicketBookId"]),
                        TicketNumber = Convert.ToInt32(dr["TicketNumber"]),
                        DailyEnvelopeId = Convert.ToInt32(dr["DailyEnvelopeId"])
                    };

                    deTicketLogs.Add(deTicketLog);
                }
            }
            return deTicketLogs;

        }

        public static ObservableCollection<DETicketLog> GetDETicketLog(int ticketBookId, int dailyEnvelopeId)
        {
            using (DETicketLogDB tlDB = new DETicketLogDB(null, null))
            {
                return tlDB.Get(ticketBookId, dailyEnvelopeId);
            }
        }
    }
}
