﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for CommisionTypesListPage.xaml
    /// </summary>
    public partial class CommisionTypesListPage : Page, IDisplayTextBlock
    {

        public CommisionTypesListPage()
        {
            InitializeComponent();
        }

        public CommisionTypesListPage(ObservableCollection<CommisionType> commisionTypes) : this()
        {
            // TODO: Complete member initialization
            this.CommisionTypes = commisionTypes;

        }


        public ObservableCollection<CommisionType> CommisionTypes
        {
            get { return this.DataContext as ObservableCollection<CommisionType>; }
            set { this.DataContext = value; }
        }

        CommisionType SelectedCommisionType
        {
            get
            {
                return this.CommisionTypesDataGrid.SelectedItem as CommisionType;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedCommisionType == null)
                    return;
                CommisionType updatedCommisionType = UIController.AddEditCommisionType(this.SelectedCommisionType.CommisionTypeID, ModificationTypes.Edit);
                if (updatedCommisionType != null)
                {
                    this.CommisionTypes.Remove(this.SelectedCommisionType);
                    this.CommisionTypes.Add(updatedCommisionType);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                CommisionType updatedCommisionType = UIController.AddEditCommisionType(0, ModificationTypes.Add);
                if (updatedCommisionType != null)
                {
                    this.CommisionTypes.Add(updatedCommisionType);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }



        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Commision Types List page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
