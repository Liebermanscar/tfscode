﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;



namespace BOL
{
    public class DriverPayment : BO
    {
        //public static DriverPayment DriverToMakePaymentFor()
        public DriverPayment()
        {
            this.DateDriverPayment = DateTime.Today;
            this.DatePaidTill = DateTime.Today;
            this.OnPropertyChanged("Selected");
            //  this.DateOfTrips = DateTime.Today;

            //  return new DriverPayment() { ObjectState = BOState.New };


        }

        Driver _driver;
        public Driver Driver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        /*  DailyEnvelope _dailyEnvelope;
          public DailyEnvelope DailyEnvelope
          {
            get { return _dailyEnvelope; }
            set { _dailyEnvelope = value; }
          }*/

        //added for DriverPaymentStubReport

        /* DriverExpense _driverExpense;

         public DriverExpense DriverExpense
         {
             get { return _driverExpense; }
             set { _driverExpense = value; }
         }*/

        int _driverID;
        public int DriverID
        {
            get { return _driverID; }
            set
            {
                _driverID = value;
                this.OnPropertyChanged("DriverID");
                this.OnPropertyChanged("AllowSave");
            }
        }

        /*
        int _paymentChargeToDriverID;

        public int PaymentChargeToDriverID
        {
            get { return _paymentChargeToDriverID; }
            set { _paymentChargeToDriverID = value; }
        }*/


        int _driverPaymentID;

        public int DriverPaymentID
        {
            get { return _driverPaymentID; }
            set { _driverPaymentID = value; }
        }

        DateTime _dateDriverPayment;
        public DateTime DateDriverPayment
        {
            get { return _dateDriverPayment; }
            set { _dateDriverPayment = value; }
        }

        DateTime _datePaidTill;
        public DateTime DatePaidTill
        {
            get { return _datePaidTill; }
            set { _datePaidTill = value; }
        }

        decimal _amountForDriver;
        public decimal AmountForDriver
        {
            get
            {
                /* if (this.EnvelopesOpen != null)
                 {
                    return  this.EnvelopesOpen.Where(oe => oe.Selected).Sum(oe => oe.OwedForDriverAmount);
                 }

                // else if (this.DriverExpenses!=null)
                // {
               //    return   this.DriverExpenses.Where(de => de.Selected).Sum(de => de.AmountOfExpense);
               //  }
                    */
                if (!this.StaticData)
                {
                    if (this.EnvelopesOpen != null || this.DriverExpenses != null || this.ChargeToDriverTrips != null)
                    {
                        //   return  this.DriverExpenses.Where(de => de.Selected).Sum(de => de.AmountOfExpense);

                        decimal ttlEO = this.EnvelopesOpen.Where(oe => oe.Selected).Sum(oe => oe.OwedForDriverAmount);//OwedForDriverAmount);
                        decimal ttlDE = this.DriverExpenses.Where(de => de.Selected).Sum(de => de.AmountOfExpense);
                        decimal ttlCTDT = this.ChargeToDriverTrips.Where(ctdi => ctdi.Selected).Sum(ctdi => ctdi.TotalPrice);

                        return (ttlEO + this.BonusPayment) - ttlDE - ttlCTDT;
                    }
                    else
                        return _amountForDriver;
                }
                else
                    return _amountForDriver;
            }
            set
            {
                _amountForDriver = value;
                this.OnPropertyChanged("Selected");
            }

        }

        decimal bonusCalculatedAmount
        {
            get
            {
                if (this.EnvelopesOpen == null)
                    return 0;

                //if driver driven for amount of money reached his bonus bracket, he get percent of the money
                //bonus is only for the week proccessing the payment
                DateTime firstDayOfWeek = DatePaidTill.AddDays(-(int)DatePaidTill.DayOfWeek);

                decimal sumElgiebleForBonus = this.EnvelopesOpen.Where(oe => oe.Selected &&
                                                                oe.DateOfTrips.Value.Date >= firstDayOfWeek.Date &&
                                                                oe.CommisionTypeUsed.IsBonusEligible).Sum(oe => oe.GrossBooking);


                decimal bonus = 0;
                if (this.Driver != null && sumElgiebleForBonus >= this.Driver.BonusBracket)
                {
                    bonus = (sumElgiebleForBonus - this.Driver.BonusBracket) * (this.Driver.BonusPercent / 100);
                }

                return bonus;
            }

        }

        private decimal _BonusPayment;

        public decimal BonusPayment
        {
            get { return _BonusPayment; }
            set { _BonusPayment = value; this.OnPropertyChanged("BonusPayment"); }
        }
        public decimal TotalUnpaidBonus { get; set; }
        private int _BonusPaidWithID;

        public int BonusPaidWithID
        {
            get { return _BonusPaidWithID; }
            set { _BonusPaidWithID = value; this.OnPropertyChanged("BonusPaidWithID"); }
        }
        decimal _cashTotal;

        public decimal CashTotal
        {
            get { return _cashTotal; }
            set { _cashTotal = value; }
        }

        bool _paidFromChecking;

        public bool PaidFromChecking
        {
            get { return _paidFromChecking; }
            set
            {
                _paidFromChecking = value;
                this.OnPropertyChanged("AllowSave");
            }
        }
        string _referenceNumber;

        public string ReferenceNumber
        {
            get { return _referenceNumber; }
            set
            {
                _referenceNumber = value;
                this.OnPropertyChanged("AllowSave");
            }
        }

        decimal _BonusAmount;

        public decimal BonusAmount
        {
            get
            {
                if (this.StaticData)
                    return _BonusAmount;
                else
                    return this.bonusCalculatedAmount;
            }
            set { _BonusAmount = value; }
        }

        bool IsAllowEditBonus
        {
            get
            { return this.BonusPaidWithID == 0; }
        }
        //  decimal _toatlForDriver;
        //public decimal ToatlForDriver
        //   {
        //       get
        //       {
        //           if (AmountForDriverFromEnvelope != 0)

        //               return this.ToatlForDriver + this.AmountForDriverFromEnvelope;
        //           else return _toatlForDriver;
        //       }//{ return _toatlForDriver; }
        //       set { _toatlForDriver = value; this.OnPropertyChanged("ToatlForDriver"); }
        //   }

        /*   ObservableCollection<DailyEnvelope> _amountOfDailyEnvelope;

           public ObservableCollection<DailyEnvelope> AmountOfDailyEnvelope
           {
               get { return _amountOfDailyEnvelope; }
               set { _amountOfDailyEnvelope = value; }
           }*/
        /*     decimal _owedForDriverAmountP;
             public decimal OwedForDriverAmountP
             {
                 get
                 {
                     return _owedForDriverAmountP;
                 }
                 set { _owedForDriverAmountP = value; this.OnPropertyChanged("OwedForDriverAmountP"); }
             }*/

        /* ObservableCollection<Driver> _driver;

            public ObservableCollection<Driver> Driver
            {
                get { return _driver; }
                set { }
            }
           */


        ObservableCollection<DailyEnvelope> _envelopesOpen;

        public ObservableCollection<DailyEnvelope> EnvelopesOpen
        {
            get { return _envelopesOpen; }
            set
            {
                _envelopesOpen = value;
                foreach (var dailyEnvelope in value)
                {
                    dailyEnvelope.PropertyChanged += new PropertyChangedEventHandler(dailyEnvelope_PropertyChanged);
                }
                this.OnPropertyChanged("EnvelopesOpen");
                this.OnPropertyChanged("OwedForDriverAmount");

            }
        }

        string _driverNotes;

        public string DriverNotes
        {
            get { return _driverNotes; }
            set { _driverNotes = value; }
        }

        ObservableCollection<DriverExpense> _driverExpenses;

        public ObservableCollection<DriverExpense> DriverExpenses
        {
            get { return _driverExpenses; }
            set
            {
                _driverExpenses = value;

                foreach (var driverExpense in value)
                {
                    driverExpense.PropertyChanged += new PropertyChangedEventHandler(driverExpense_PropertyChanged);
                }
                this.OnPropertyChanged("DriverExpenses");
            }
        }


        ObservableCollection<Trip> _chargeToDriverTrips;

        public ObservableCollection<Trip> ChargeToDriverTrips
        {
            get { return _chargeToDriverTrips; }
            set
            {
                _chargeToDriverTrips = value;

                foreach (var chargeToDriverTrip in value)
                {
                    chargeToDriverTrip.PropertyChanged += new PropertyChangedEventHandler(chargeToDriverTrip_PropertyChanged);
                }
                this.OnPropertyChanged("ChargeToDriverTrips");


            }
        }




        void dailyEnvelope_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Selected")
            {
                this.OnPropertyChanged("AmountForDriver");
                this.OnPropertyChanged("BonusAmount");
            }
        }

        void driverExpense_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Selected")
                this.OnPropertyChanged("AmountForDriver");
        }


        void chargeToDriverTrip_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Selected")
                this.OnPropertyChanged("AmountForDriver");
        }

        public override bool AllowSave
        {
            get
            {
                if (!this.PaidFromChecking)
                    return (this.DriverID != 0);
                else
                    return !string.IsNullOrWhiteSpace(this.ReferenceNumber);
            }

        }
        public bool StaticData { get; set; }
    }
}
