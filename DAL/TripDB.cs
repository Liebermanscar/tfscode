﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using BOL;
using System.Data;

namespace DAL
{
    public class TripDB : Database
    {
        public TripDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        
        public ObservableCollection<Trip> Get(int drivenCustomerID,            
            int tripID, 
            int driverID,
            int invoiceID,
            DateTime? fromDateOfTrip,
            DateTime? toDateOfTrip,
            bool withMedicaidInfo,
            DateTime? dateOfCall, int chargeToDriverID,int driverPaymentID,
            bool NotAssignedToDailyEnvelope,
            int assignedToDailyEnvelopeID =0,
            bool isForChargeToDriverList = false,
            bool isForLongDistance = false,
            int schedueldTripsBatchID =0,
            int scheduledTripID =0,
            bool withLinkedDriverTrips = false
            )
        {
            var trips = new ObservableCollection<Trip>();
            using (SqlDataReader dr = this.GetReader("GetTrips", new SqlParameter[]
            {
                new SqlParameter("@DrivenCustomerID", drivenCustomerID),
                new SqlParameter("@InvoiceID", invoiceID),
                new SqlParameter("@TripID", tripID),
                new SqlParameter("@DriverID", driverID),
                new SqlParameter("@FromDateOfTrip", fromDateOfTrip),
                new SqlParameter("@ToDateOfTrip", toDateOfTrip),
                new SqlParameter("@WithActualTripInfo", withMedicaidInfo),
                new SqlParameter("@TripDate", dateOfCall),
                new SqlParameter("@ChargeToDriverID", chargeToDriverID),
                new SqlParameter("@ChargeToDriverPaymentID", driverPaymentID),
                new SqlParameter("@NotAssignedToDailyEnvelope",NotAssignedToDailyEnvelope),
                new SqlParameter("@AssignedToDailyEnvelopeID",assignedToDailyEnvelopeID),
                new SqlParameter("@IsChargeToDriver",isForChargeToDriverList),
                new SqlParameter("@SchedueldTripsBatchID",schedueldTripsBatchID),
                new SqlParameter("@ScheduledTripID",scheduledTripID),
                new SqlParameter("@WithLinkedDriverTrips", withLinkedDriverTrips)

            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    if (driverPaymentID == 0)
                    {

                        var trip = new Trip()
                           {
                               TripID = Convert.ToInt32(dr["TripID"]),
                               TimeOfCall = Convert.ToDateTime(dr["TimeOfCall"]),
                               TripStarted = dr["TripStarted"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripStarted"]),
                               ScheduledFor = dr["ScheduledFor"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ScheduledFor"].ToString()),
                               TimeDriverAssigned = dr["AssignedAt"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["AssignedAt"].ToString()),
                               TripEnded = dr["TripEnded"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripEnded"]),
                               TakenByDispatcherID = Convert.ToInt32(dr["TakenByDispatcherID"]),
                               ETA = dr["ETA"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ETA"]),
                               DrivenBy = dr["DriverID"] == DBNull.Value ? null : new Driver()
                               {
                                   DriverID = Convert.ToInt32(dr["DriverID"]),
                                   DriverCode = dr["DriverCode"].ToString(),
                                   DriverFirstName = dr["DriverFirstName"].ToString(),
                                   DriverLastName = dr["DriverLastName"].ToString()
                               },
                               FromLocation = dr["FromLocation"].ToString(),
                               FromLocationComment = dr["FromLocationComment"].ToString(),
                               ToLocation = dr["ToLocation"].ToString(),
                               ToLocationComment = dr["ToLocationComment"].ToString(),
                               TripStatus = (TripStatusTypes)Convert.ToInt32(dr["TripStatus"]),
                               TripType = (TripTypes)Convert.ToInt32(dr["TripType"]),
                               IsRoundTrip = Convert.ToBoolean(dr["IsRoundTrip"]),
                               ReturnTripOfTripID = Convert.ToInt32(dr["IsReturnTripOfTripID"]),
                               ChargeToCustomer = Convert.ToInt32(dr["DrivenCustomerID"]) == 0 ? null : new Customer()
                               {
                                   CustomerID = Convert.ToInt32(dr["DrivenCustomerID"]),
                                   CustomerType = (CustomerTypes)Convert.ToInt32(dr["CustomerType"]),
                                   FirstName = dr["FirstName"].ToString(),
                                   LastName = dr["LastName"].ToString(),
                                   CompanyName = dr["CompanyName"].ToString(),//==DBNull.Value ?"" //.ToString()
                               },
                               CallInPhoneNo = dr["CallInPhoneNo"].ToString(),
                               CallBackPhoneNo = dr["CallBackPhoneNo"].ToString(),
                               BilledOnReferenceID = Convert.ToInt32(dr["BilledOnReferenceID"]),
                               AssignedToDailyEnvelopeID = Convert.ToInt32(dr["AssignedToDailyEnvelopeID"]),
                               ChargeToDriver = Convert.ToInt32(dr["ChargeToDriverID"]) == 0 ? null : new Driver()
                               {
                                   DriverID = Convert.ToInt32(dr["ChargeToDriverID"]),
                                   DriverCode = dr["ChargeDriverCode"].ToString()
                               },
                               NoOfStops = Convert.ToInt32(dr["NoOfStops"]),
                               WaitingTime = Convert.ToDouble(dr["WaitingTime"]),
                               StopsCharge = Convert.ToDecimal(dr["StopsCharge"]),
                               OutOfCarCharge = Convert.ToDecimal(dr["OutOfCarCharge"]),
                               MiniVanCharge = Convert.ToDecimal(dr["MiniVanCharge"]),
                               WaitingCharge = Convert.ToDecimal(dr["WaitingCharge"]),
                               TollCharge = Convert.ToDecimal(dr["TollCharge"]),
                               ExtraCharge = Convert.ToDecimal(dr["ExtraCharge"]),
                               TripPrice = Convert.ToDecimal(dr["TripPrice"]),
                               TotalPrice = Convert.ToDecimal(dr["TotalPrice"]),
                               TripDescription = dr["TripDescription"].ToString(),
                               TripAreaType = (TripAreaTypes)dr["TripAreaType"],
                               ETAToLocation1 = dr["ETAToLocation1"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ETAToLocation1"]),
                               ETABackToBase = dr["ETABackToBase"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ETABackToBase"]),
                               FromCity = dr["PickUpCity"].ToString(),
                               ToCity = dr["DropOffCity"].ToString(),
                               CustomerPassCodeID = Convert.ToInt32(dr["CustomerPassCodeID"]),
                               CashPaid = Convert.ToDecimal(dr["CashPaid"]),
                               CheckPaid = Convert.ToDecimal(dr["CheckPaid"]),
                               CCPaid = Convert.ToDecimal(dr["CCPaid"]),
                               ChargePaid = Convert.ToDecimal(dr["ChargePaid"]),
                               TicketTypePaid = Convert.ToInt32(dr["TicketType"]),// DAL.TicketBookDB.GetTicketBooks(Convert.ToInt32(dr["TicketType"]))[0],
                               TicketPaid = Convert.ToDecimal(dr["TicketPaid"]), // dont take price from ticket value cause maybe now changed from time driven
                               TicketQt = Convert.ToInt32(dr["TicketQt"]),
                               CompanyIncome = Convert.ToDecimal(dr["CompanyIncome"]),
                               IsBonusEligible = Convert.ToBoolean(dr["IsBonusEligible"]),
                               MedicaidTripPay = dr["MedicaidTripPay"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["MedicaidTripPay"]),
                               MedicaidOverPay = dr["MedicaidOverPay"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["MedicaidOverPay"]),
                               AttachedScheduledTrip = new ScheduledTrip()
                               {
                                   ScheduledTripID = Convert.ToInt32(dr["ScheduledTripID"]),
                               },
                               //CCPaid = dr["AmountPaid"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["AmountPaid"])
                           };

                        if (dr["VehicleTypeRequested"] != DBNull.Value) 
                            trip.VehicleTypeRequested = (VehicleTypes)dr["VehicleTypeRequested"];
                                
                         if (dr["MediDispatchTripNo"] != DBNull.Value)
                             trip.MedicaidDispatchTripNo = Convert.ToInt32(dr["MediDispatchTripNo"]);
                                
                       /* if (trip.ChargeToDriver!= null) 
                                {
                                    using (var driversDB = new  DriversDB(null,null))
                                    {
                                        trip.ChargeToDriver= driversDB.Get(true, trip.ChargeToDriver.DriverID).FirstOrDefault();
                                    }
                                    
                                }*/
                        Trip tt = new Trip();
                        tt.TripType = (TripTypes)Convert.ToInt32(dr["TripType"]);
                        if (withMedicaidInfo)
                        {
                            trip.AttachedScheduledTrip = new ScheduledTrip()
                            {
                                PickupTime = Convert.ToDateTime(dr["TimeOfCall"]),
                            };

                            trip.MedicaidTripInfo = new MedicaidTrip()
                            {
                                FirstName = dr["PFirstName"].ToString(),
                                LastName = dr["PLastName"].ToString(),
                                CIN = dr["CIN"].ToString(),
                                PickUpAddress = dr["PickUpAddress"].ToString(),
                                DropOffAddress = dr["DropOffAddress"].ToString(),
                                MedicaidDispatchTripNo = (int)dr["MediDispatchTripNo"],
                                IsManualEntry = Convert.ToBoolean(dr["IsManualEntry"])
                            };

                            if (dr["VehicleCode"] != DBNull.Value)
                                trip.VehicleInfo = new Vehicle()
                                {
                                    VehicleCode = dr["VehicleCode"].ToString()
                                };
                        }
                    
                    trips.Add(trip);
                    }
                        //End driverPaymentID==0
                    else if (driverPaymentID!=0)
                    {
                         var trip = new Trip()
                        {   
                            StaticData=true,
                            TripID = Convert.ToInt32(dr["TripID"]),
                            TimeOfCall = Convert.ToDateTime(dr["TimeOfCall"]),
                            TripStarted = dr["TripStarted"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripStarted"]),
                            TripEnded = dr["TripEnded"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripEnded"]),
                            DrivenBy = new Driver()
                            {
                                DriverID = Convert.ToInt32(dr["TakenByDriverID"]),
                                DriverCode = dr["DriverCode"].ToString(),
                            },
                           /* ChargeToDriver = new Driver()
                            {
                          //  DriverCode
                            },*/
                            FromLocation = dr["FromLocation"].ToString(),
                            ToLocation = dr["ToLocation"].ToString(),
                            TotalPrice = Convert.ToDecimal(dr["TotalPrice"]),
                        };
                        trips.Add(trip);
                    }
                }
            }

            return trips;
        }

        public bool Add(Trip trip)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@TripID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter outParamMediTripNo = new SqlParameter("@MediDispatchTripNo", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IsReturnTripOfTripID", trip.ReturnTripOfTripID),
                new SqlParameter("@DrivenCustomerID", trip.ChargeToCustomer == null ? 0 : trip.ChargeToCustomer.CustomerID),
                new SqlParameter("@CallInPhoneNo", trip.CallInPhoneNo),
                new SqlParameter("@CallBackPhoneNo", trip.CallBackPhoneNo),
                new SqlParameter("@ETA",trip.ETA),
                new SqlParameter("@ETAToLocation1",trip.ETAToLocation1),
                new SqlParameter("@ETABackToBase",trip.ETABackToBase),
                new SqlParameter("@TimeOfCall", trip.TimeOfCall),
                new SqlParameter("@ScheduledFor",trip.ScheduledFor),
                new SqlParameter("@AssignedAt",trip.TimeDriverAssigned),
                new SqlParameter("@TripStarted", trip.TripStarted),
                new SqlParameter("@TripEnded", trip.TripEnded),
                new SqlParameter("@TakenByDispatcherID", trip.TakenByDispatcherID),
                new SqlParameter("@TakenByDriverID", trip.DrivenBy == null ? 0 : trip.DrivenBy.DriverID),
                new SqlParameter("@DrivenWithVehicleID", trip.DrivenWithVehicle == null ? 0 : trip.DrivenWithVehicle.VehicleID),
                new SqlParameter("@FromLocation", trip.FromLocation),
                new SqlParameter("@ToLocation", trip.ToLocation),
                new SqlParameter("@TripStatus", trip.TripStatus),
                new SqlParameter("@IsRoundTrip", trip.IsRoundTrip),
                new SqlParameter("@TripType", trip.TripType),
                new SqlParameter("@BilledOnReferenceID", trip.BilledOnReferenceID),
                new SqlParameter("@AssignedToDailyEnvelopeID", trip.AssignedToDailyEnvelopeID),
                new SqlParameter("@ChargeToDriverID", trip.ChargeToDriver == null ? 0 : trip.ChargeToDriver.DriverID),
                new SqlParameter("@NoOfStops", trip.NoOfStops),
                new SqlParameter("@WaitingTime", trip.WaitingTime),
                new SqlParameter("@StopsCharge", trip.StopsCharge),
                new SqlParameter("@OutOfCarCharge", trip.OutOfCarCharge),
                new SqlParameter("@MiniVanCharge", trip.MiniVanCharge),
                new SqlParameter("@WaitingCharge", trip.WaitingCharge),
                new SqlParameter("@TollCharge", trip.TollCharge),
                new SqlParameter("@ExtraCharge", trip.ExtraCharge),
                new SqlParameter("@TripPrice", trip.TripPrice),                
                new SqlParameter("@TotalPrice", trip.TotalPrice),
                new SqlParameter("@TripDescription", trip.TripDescription),
                new SqlParameter("@ScheduledTripID", trip.AttachedScheduledTrip == null ? 0 : trip.AttachedScheduledTrip.ScheduledTripID),
                new SqlParameter("@SessionIDAdded", trip.AddedBySessionID == 0 ? Database.SessionID : trip.AddedBySessionID),
                new SqlParameter("@ToLocationComment", trip.ToLocationComment),
                new SqlParameter("@FromLocationComment", trip.FromLocationComment),
                new SqlParameter("@TripAreaType",trip.TripAreaType),
                new SqlParameter("@VehicleTypeRequested",trip.VehicleTypeRequested),
                new SqlParameter("@CustomerPassCodeID",trip.CustomerPassCodeID),
                new SqlParameter("@CashPaid", trip.CashPaid),
                new SqlParameter("@CheckPaid", trip.CheckPaid),
                new SqlParameter("@CCPaid",trip.CCPaid),
                new SqlParameter("@ChargePaid",trip.ChargePaid),
                new SqlParameter("@TicketPaid",trip.TicketPaid),
                new SqlParameter("@TicketType",trip.TicketTypePaid),
                new SqlParameter("@TicketQt",trip.TicketQt),
                new SqlParameter("@CompanyIncome",trip.CompanyIncome),
                new SqlParameter("@IsBonusEligible", trip.IsBonusEligible),
               outParamMediTripNo,
                outParam
            };

            int noOfAffectedRecords = this.ExecuteNonQuery("AddTrips", param, CommandType.StoredProcedure, out serverMessage);
            if (noOfAffectedRecords > 0)
            {
                trip.TripID = Convert.ToInt32(outParam.Value);
                if (outParamMediTripNo!=null)
                {
               
                trip.MedicaidDispatchTripNo = Convert.ToInt32(outParamMediTripNo.Value);
                }
                
                return true;
            }
            else
                return false;
        }
        public bool Edit(Trip trip)
        {
            string serverMessage;
            SqlParameter outParamMediTripNo = new SqlParameter("@MediDispatchTripNo", SqlDbType.Int) { Direction = ParameterDirection.Output };
            
            int noOfTripsAffected = this.ExecuteNonQuery("UpdateTrips", new SqlParameter[] 
            {
           
                  new SqlParameter("@TripID", trip.TripID)
                  ,new SqlParameter("@IsReturnTripOfTripID", 0)
                  ,new SqlParameter("@DrivenCustomerID", trip.ChargeToCustomer == null ? 0 : trip.ChargeToCustomer.CustomerID)
                  ,new SqlParameter("@CallInPhoneNo", trip.CallInPhoneNo)
                  ,new SqlParameter("@CallBackPhoneNo", trip.CallBackPhoneNo)
                  ,new SqlParameter("@TimeOfCall", trip.TimeOfCall)
                  ,new SqlParameter("@ScheduledFor",trip.ScheduledFor)
                  ,new SqlParameter("@AssignedAt",trip.TimeDriverAssigned)
                  ,new SqlParameter("@ETAInMinutes", trip.ETAInMinutes)
                  ,new SqlParameter("@ETA", trip.ETA)
                  ,new SqlParameter("@ETAToLocation1",trip.ETAToLocation1)
                  ,new SqlParameter("@ETABackToBase",trip.ETABackToBase)
                  ,new SqlParameter("@TripStarted", trip.TripStarted)
                  ,new SqlParameter("@TripEnded", trip.TripEnded)
                  ,new SqlParameter("@TakenByDispatcherID", trip.TakenByDispatcherID)
                  ,new SqlParameter("@TakenByDriverID", trip.DrivenBy == null ? 0 :trip.DrivenBy.DriverID)
                  ,new SqlParameter("@DrivenWithVehicleID", trip.DrivenWithVehicle == null ? 0 : trip.DrivenWithVehicle.VehicleID)
                  ,new SqlParameter("@FromLocation", trip.FromLocation)
                  ,new SqlParameter("@ToLocation", trip.ToLocation)
                  ,new SqlParameter("@TripStatus", trip.TripStatus)
                  ,new SqlParameter("@IsRoundTrip", trip.IsRoundTrip)
                  ,new SqlParameter("@TripType", trip.TripType)
                  ,new SqlParameter("@BilledOnReferenceID", trip.BilledOnReferenceID)
                  ,new SqlParameter("@AssignedToDailyEnvelopeID", trip.AssignedToDailyEnvelopeID)
                  ,new SqlParameter("@ChargeToDriverID", trip.ChargeToDriver == null ? 0 : trip.ChargeToDriver.DriverID)
                  ,new SqlParameter("@NoOfStops", trip.NoOfStops)
                  ,new SqlParameter("@WaitingTime", trip.WaitingTime)
                  ,new SqlParameter("@StopsCharge", trip.StopsCharge)
                  ,new SqlParameter("@OutOfCarCharge", trip.OutOfCarCharge)
                  ,new SqlParameter("@MiniVanCharge", trip.MiniVanCharge)
                  ,new SqlParameter("@WaitingCharge", trip.WaitingCharge)
                  ,new SqlParameter("@TollCharge", trip.TollCharge)
                  ,new SqlParameter("@ExtraCharge", trip.ExtraCharge)
                  ,new SqlParameter("@TripPrice", trip.TripPrice)
                  ,new SqlParameter("@TotalPrice", trip.TotalPrice)
                  ,new SqlParameter("@TripDescription", trip.TripDescription)
                  ,new SqlParameter("@ScheduledTripID", trip.AttachedScheduledTrip == null ? 0 : trip.AttachedScheduledTrip.ScheduledTripID)
                  ,new SqlParameter("@ToLocationComment", trip.ToLocationComment)
                  ,new SqlParameter("@FromLocationComment", trip.FromLocationComment)
                  ,new SqlParameter("@TripAreaType",trip.TripAreaType)
                  ,new SqlParameter("@VehicleTypeRequested",trip.VehicleTypeRequested)
                  ,new SqlParameter("@CustomerPassCodeID",trip.CustomerPassCodeID)
                  ,new SqlParameter("@PaymentID",trip.PaymentID)
                  ,new SqlParameter("@CashPaid", trip.CashPaid)
                  ,new SqlParameter("@CheckPaid", trip.CheckPaid)
                  ,new SqlParameter("@CCPaid",trip.CCPaid)
                  ,new SqlParameter("@ChargePaid",trip.ChargePaid)
                  ,new SqlParameter("@TicketPaid",trip.TicketPaid)
                  ,new SqlParameter("@TicketType",trip.TicketTypePaid)
                  ,new SqlParameter("@TicketQt",trip.TicketQt)
                  ,new SqlParameter("@CompanyIncome",trip.CompanyIncome)
                  ,new SqlParameter("@IsBonusEligible", trip.IsBonusEligible)
                  ,outParamMediTripNo 
            }, CommandType.StoredProcedure, out serverMessage);
            if(noOfTripsAffected > 0)
            {
                trip.MedicaidDispatchTripNo = outParamMediTripNo.Value == DBNull.Value ? 0 : Convert.ToInt32(outParamMediTripNo.Value);
                
                return true;
            }
            return false;
        }
        private bool Delete(Trip trip)
        {
            /*string serverMessage;
            SqlParameter outParam = new SqlParameter("@TripID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IsReturnTripOfTripID", trip.ReturnTripOfTripID),
             
                new SqlParameter("@ScheduledTripID", trip.AttachedScheduledTrip == null ? 0 : trip.AttachedScheduledTrip.ScheduledTripID),
                new SqlParameter("@SessionIDAdded", trip.AddedBySessionID == 0 ? Database.SessionID : trip.AddedBySessionID),
                outParam
            };

            int noOfAffectedRecords = this.ExecuteNonQuery("AddTrips", param, CommandType.StoredProcedure, out serverMessage);
            if (noOfAffectedRecords > 0)
            {
                trip.TripID = Convert.ToInt32(outParam.Value);
                return true;
            }
            else
                return false;*/

            string serverMessage;
            return this.ExecuteNonQuery("DeleteTrips", new SqlParameter[]
            {
                new SqlParameter("@TripID", trip.TripID)
             }, CommandType.StoredProcedure, out serverMessage) > 0;




            //throw new NotImplementedException();
        }
        public static ObservableCollection<Trip> GetTrips(
            int drivenCustomerID, 
            int tripID,
            int driverID,
            int invoiceID,
            DateTime? fromDateOfTrip,
            DateTime? toDateOfTrip ,
            bool withMedicaidInfo,
            DateTime? dateOfCall, 
            int chargeToDriverID, 
            int driverPaymentID,
            bool notAssignedToDailyEnvelope = false, 
            int assignedToDailyEnvelopeID = 0, 
            bool isForChargeToDriverList = false, 
            int schedueldTripsBatchID =0,
            int scheduledTripID=0,
            bool withLinkedDriverTrips = false)
        {
            using (TripDB tripDB = new TripDB(null, null))
            {
                return tripDB.Get(drivenCustomerID, tripID, driverID, invoiceID, fromDateOfTrip, toDateOfTrip, withMedicaidInfo, dateOfCall, chargeToDriverID, driverPaymentID, notAssignedToDailyEnvelope, assignedToDailyEnvelopeID,isForChargeToDriverList,false, schedueldTripsBatchID, scheduledTripID, withLinkedDriverTrips);
            }
        }

        public static ObservableCollection<Trip> GetTrips(int tripID)
        {
            return  GetTrips(0, tripID, 0, 0, null, null, false, null, 0, 0);
        }

        public static bool AddTrip(Trip trip)
        {
            using (var tripDB = new TripDB(null, null))
            {
                return tripDB.Add(trip);
            }
        }

        public static bool EditTrip(Trip trip)
        {
            using (var tripDB = new TripDB(null, null))
            {
                return tripDB.Edit(trip);
            }
        }

        public static bool DeleteTrip(Trip trip)
        {
            using (var tripDB = new TripDB(null, null))
            {
                return tripDB.Delete(trip);
            }
        }
        public static ObservableCollection<Trip> GetAverageTripsAmountByDriver(
            int drivenCustomerID,
            int tripID,
            int driverID,
            int invoiceID,
            DateTime? fromDateOfTrip,
            DateTime? toDateOfTrip,
            bool withMedicaidInfo,
            DateTime? dateOfCall,
            int chargeToDriverID,
            int driverPaymentID,
            bool notAssignedToDailyEnvelope = false,
            int assignedToDailyEnvelopeID = 0,
            bool isForChargeToDriverList = false,
            int schedueldTripsBatchID = 0,
            int scheduledTripID = 0,
            bool withLinkedDriverTrips = false)
        {
            using (TripDB tripDB = new TripDB(null, null))
            {
                return tripDB.GetTripAmoutByDriver(drivenCustomerID, tripID, driverID, invoiceID, fromDateOfTrip, toDateOfTrip, withMedicaidInfo, dateOfCall, chargeToDriverID, driverPaymentID, notAssignedToDailyEnvelope, assignedToDailyEnvelopeID, isForChargeToDriverList, false, schedueldTripsBatchID, scheduledTripID, withLinkedDriverTrips);
            }
        }

        public ObservableCollection<Trip> GetTripAmoutByDriver(
             int drivenCustomerID,
             int tripID,
             int driverID,
             int invoiceID,
             DateTime? fromDateOfTrip,
             DateTime? toDateOfTrip,
             bool withMedicaidInfo,
             DateTime? dateOfCall, int chargeToDriverID, int driverPaymentID,
             bool NotAssignedToDailyEnvelope,
             int assignedToDailyEnvelopeID = 0,
             bool isForChargeToDriverList = false,
             bool isForLongDistance = false,
             int schedueldTripsBatchID = 0,
             int scheduledTripID = 0,
             bool withLinkedDriverTrips = false
             )
        {
            var trips = new ObservableCollection<Trip>();
            using (SqlDataReader dr = this.GetReader("GetTotalTripsPaymentDriverForReport", new SqlParameter[]
            {
                new SqlParameter("@DrivenCustomerID", drivenCustomerID),
                new SqlParameter("@InvoiceID", invoiceID),
                new SqlParameter("@TripID", tripID),
                new SqlParameter("@DriverID", driverID),
                new SqlParameter("@FromDateOfTrip", fromDateOfTrip),
                new SqlParameter("@ToDateOfTrip", toDateOfTrip),
                new SqlParameter("@WithActualTripInfo", withMedicaidInfo),
                new SqlParameter("@TripDate", dateOfCall),
                new SqlParameter("@ChargeToDriverID", chargeToDriverID),
                new SqlParameter("@ChargeToDriverPaymentID", driverPaymentID),
                new SqlParameter("@NotAssignedToDailyEnvelope",NotAssignedToDailyEnvelope),
                new SqlParameter("@AssignedToDailyEnvelopeID",assignedToDailyEnvelopeID),
                new SqlParameter("@IsChargeToDriver",isForChargeToDriverList),
                new SqlParameter("@SchedueldTripsBatchID",schedueldTripsBatchID),
                new SqlParameter("@ScheduledTripID",scheduledTripID),
                new SqlParameter("@WithLinkedDriverTrips", withLinkedDriverTrips)

            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    if (driverPaymentID == 0)
                    {

                        var trip = new Trip()
                        {
                            //TripID = Convert.ToInt32(dr["TripID"]),                            
                            DrivenBy = dr["DriverID"] == DBNull.Value ? null : new Driver()
                            {
                                DriverID = Convert.ToInt32(dr["DriverID"]),
                                DriverCode = dr["DriverCode"].ToString(),
                                //  DriverFirstName = dr["DriverFirstName"].ToString(),
                                //   DriverLastName = dr["DriverLastName"].ToString()
                            },
                            CashPaid = Convert.ToDecimal(dr["CashPaid"]),
                            CheckPaid = Convert.ToDecimal(dr["CheckPaid"]),
                            CCPaid = Convert.ToDecimal(dr["CCPaid"]),
                            ChargePaid = Convert.ToDecimal(dr["ChargePaid"]),
                            TicketPaidTotal = Convert.ToDecimal(dr["TicketPaid"]), // dont take price from ticket value cause maybe now changed from time driven
                            TicketQt = Convert.ToInt32(dr["TicketQt"]),
                            CompanyIncome = Convert.ToDecimal(dr["CompanyIncome"]),
                            IsEzpass = Convert.ToBoolean(dr["IsCompanyEZPass"]),
                            TollReceivables = Convert.ToBoolean(dr["IsCompanyEZPass"]) == true ? Convert.ToDecimal(dr["TollCharge"]) : 0,
                            TollPayables = Convert.ToBoolean(dr["IsCompanyEZPass"]) == false ? Convert.ToDecimal(dr["TollCharge"]) : 0,
                            CCFee = Convert.ToDecimal(dr["CCPaid"]) < 10 && Convert.ToDecimal(dr["CCPaid"]) >0 ? Convert.ToDecimal(0.50) : 0,
                        };
                        trips.Add(trip);
                    }
                    //End driverPaymentID==0
                    else if (driverPaymentID != 0)
                    {
                        var trip = new Trip()
                        {
                            // StaticData = true,
                            // TripID = Convert.ToInt32(dr["TripID"]),
                            // TimeOfCall = Convert.ToDateTime(dr["TimeOfCall"]),
                            // TripStarted = dr["TripStarted"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripStarted"]),
                            // TripEnded = dr["TripEnded"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripEnded"]),
                            // DrivenBy = new Driver()
                            // {
                            //     DriverID = Convert.ToInt32(dr["TakenByDriverID"]),
                            //     DriverCode = dr["DriverCode"].ToString(),
                            // },
                            // /* ChargeToDriver = new Driver()
                            //  {
                            ////  DriverCode
                            //  },*/
                            // FromLocation = dr["FromLocation"].ToString(),
                            // ToLocation = dr["ToLocation"].ToString(),
                            // TotalPrice = Convert.ToDecimal(dr["TotalPrice"]),
                        };
                        trips.Add(trip);
                    }
                }
            }

            return trips;
        }

        public static int GetTodaysTripCount()
        {
            using (var db = new Database(null, null))
            {
                var o = db.ExecuteScalar("SELECT * FROM Trips WHERE Cast(TripStarted  AS DATE) = CAST(GETDATE() as DATE)", null, CommandType.Text);
                if (o == null)
                    return 0;
                else
                    return (int)o;
            }
        }
    }

    public class ScheduledTripDB : Database
    {
        public ScheduledTripDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }


        public ObservableCollection<Trip> ProcessScheduledTrips(int scheduledTripID = 0)
        {
            var tripsProcessed = new ObservableCollection<Trip>();
            string serverMessage;

            SqlParameter outParam = new SqlParameter("@SchedueldTripsBatchID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            //  SqlParameter MediOutParam = new SqlParameter("@MediDispatchTripNo", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@ScheduledTripID",scheduledTripID),
                new SqlParameter("@SessionIDAdded",Database.SessionID),
                outParam
            };

            int noOfAffectedRecords = this.ExecuteNonQuery("ProcessScheduledTrips", param, CommandType.StoredProcedure, out serverMessage);
            if (noOfAffectedRecords > 0)
            {
                int schedueldTripsBatchID = 0;
                schedueldTripsBatchID = Convert.ToInt32(outParam.Value);
                using (TripDB tripDB = new TripDB(this.Connection, this.Transaction))
                {
                    tripsProcessed.AddRange(tripDB.Get(0, 0, 0, 0, DateTime.Today, DateTime.Today, false, DateTime.Today, 0, 0, false, 0, false, true, schedueldTripsBatchID));
                }
            }
            return tripsProcessed;

        }
        public static ObservableCollection<Trip> ProcessScheduledTripsAsTrips(int scheduledTripID = 0)
        {
            using (ScheduledTripDB stDB = new ScheduledTripDB(null, null))
            {
                return stDB.ProcessScheduledTrips(scheduledTripID);
            }
        }

        public bool Add(ScheduledTrip scheduledTrip)
        {
            string serverMessage;
            var ScheduledTripIDParam = new SqlParameter("@ScheduledTripID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            int noOfRecAffected = this.ExecuteNonQuery("AddScheduledTrips", new SqlParameter[]
            {
                new SqlParameter("@ScheduledTripType", scheduledTrip.ScheduledTripType),
                new SqlParameter("@FidelisMedicaidReferencNo", scheduledTrip.FidelisMedicaidReferencNo),
                new SqlParameter("@NoOfLegs", scheduledTrip.NoOfLegs),
                new SqlParameter("@CustomerID", scheduledTrip.ScheduledByCustomer == null ? 0 : scheduledTrip.ScheduledByCustomer.CustomerID),
                new SqlParameter("@CustomerName", scheduledTrip.ScheduledByCustomer == null ? "": scheduledTrip.ScheduledByCustomer.CustomerName),
                new SqlParameter("@FirstName",scheduledTrip.DrivenFirstName),
                new SqlParameter("@LastName",scheduledTrip.DrivenLastName),
                new SqlParameter("@CustomerPhoneNo", scheduledTrip.CustomerPhoneNo),
                new SqlParameter("@PickupDate", scheduledTrip.PickupDate),
                new SqlParameter("@PickupTime", scheduledTrip.PickupTime == null ? DateTime.Now  :scheduledTrip.PickupTime.Value),
                new SqlParameter("@PickupAddress", scheduledTrip.PickupAddress),
                new SqlParameter("@PickupCity", scheduledTrip.PickupCity),
                new SqlParameter("@PickupState", scheduledTrip.PickupState),
                new SqlParameter("@PickupZip", scheduledTrip.PickupZip),
                new SqlParameter("@DropOffAddress", scheduledTrip.DropOffAddress),
                new SqlParameter("@DropOffCity", scheduledTrip.DropOffCity),
                new SqlParameter("@DropOffState", scheduledTrip.DropOffState),
                new SqlParameter("@DropOffZip", scheduledTrip.DropOffZip),
                new SqlParameter("@TripPrice",scheduledTrip.TripPrice),
                new SqlParameter("@WaitingCahrge",scheduledTrip.WaitingCahrge),
                new SqlParameter("@ExtraCharge",scheduledTrip.ExtraCharge),
                new SqlParameter("@MiniVanCharge",scheduledTrip.MiniVanCharge),
                new SqlParameter("@NeedToConfirm",scheduledTrip.NeedToConfirm),
                new SqlParameter("@AssignedToDriverID",scheduledTrip.AssignedToDriverID),
                new SqlParameter("@ETAToLocation1",scheduledTrip.ETAToLocation1 ),
                new SqlParameter("@ETABackToBase",scheduledTrip.ETABackToBase),
                new SqlParameter("@IsRoundTrip",scheduledTrip.IsRoundTrip),
                new SqlParameter("@sFromLoactionComment",scheduledTrip.SFromLocationComment),
                new SqlParameter("@sToLocationComment",scheduledTrip.SToLocationComment),
                new SqlParameter("@sVehicleTypeRequested",scheduledTrip.SVehicleTypeRequested),
                new SqlParameter("@sCustomerPassCodeID",scheduledTrip.SCustomerPassCodeID),
                new SqlParameter("@sNoOfStops",scheduledTrip.SNoOfStops),

                new SqlParameter("@SessionID", Database.SessionID),
                ScheduledTripIDParam
            }, CommandType.StoredProcedure, out serverMessage);

            if (noOfRecAffected > 0)
                scheduledTrip.ScheduledTripID = Convert.ToInt32(ScheduledTripIDParam.Value);

            return noOfRecAffected > 0;
        }

        public ObservableCollection<ScheduledTrip> Get(int scheduledTripID,
            DateTime? scheduledTripDate,
            DateTime? fromTripDate,
            DateTime? toTripDate,
            bool notAppliedToTrip,
            TripAreaTypes areaTripType,
              int NeedToConfirm,//0for all 1 for true 2 for false
            bool includeMedicaidInfo
            )
        {
            var scheduledTrips = new ObservableCollection<ScheduledTrip>();
            using (var dr = this.GetReader("GetScheduledTrips", new SqlParameter[]
            {
                new SqlParameter("@ScheduledTripID", scheduledTripID),
                new SqlParameter("@ScheduledTripDate", scheduledTripDate),
                new SqlParameter("@FromTripDate", fromTripDate),
                new SqlParameter("@ToTripDate", toTripDate),
                new SqlParameter("@NotAppliedToTrip", notAppliedToTrip),
                new SqlParameter("@AreaType", areaTripType),
                new SqlParameter("@NeedToConfirm",NeedToConfirm),
                new SqlParameter("@IncludeMedicaidInfo",includeMedicaidInfo)
            }, CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    //  scheduledTrips.Add(new ScheduledTrip()
                    var scheduledTrip = new ScheduledTrip()
                    {
                        ScheduledTripID = Convert.ToInt32(dr["ScheduledTripID"]),
                        ScheduledTripType = (ScheduledTripTypes)Convert.ToInt32(dr["ScheduledTripType"]),
                        FidelisMedicaidReferencNo = Convert.ToInt32(dr["FidelisMedicaidReferencNo"]),
                        NoOfLegs = Convert.ToInt32(dr["NoOfLegs"]),
                        ScheduledByCustomer = Convert.ToInt32(dr["CustomerID"]) == 0 ? new Customer() { CustomerID = 0 } : DAL.CustomerDB.FillCustomer(dr),
                        CustomerName = dr["CustomerName"].ToString(),
                        CustomerPhoneNo = dr["CustomerPhoneNo"].ToString(),
                        PickupDate = Convert.ToDateTime(dr["PickUpDate"]),
                        PickupTime = dr["PickupTime"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["PickupTime"].ToString()),
                        PickupAddress = dr["PickupAddress"].ToString(),
                        PickupCity = dr["PickupCity"].ToString(),
                        PickupState = dr["PickupState"].ToString(),
                        PickupZip = dr["PickupZip"].ToString(),
                        DropOffAddress = dr["DropOffAddress"].ToString(),
                        DropOffCity = dr["DropOffCity"].ToString(),
                        DropOffState = dr["DropOffState"].ToString(),
                        DropOffZip = dr["DropOffZip"].ToString(),
                        FirstLegAppliedToTripID = Convert.ToInt32(dr["FirstLegAppliedToTripID"]),
                        SecondLegAppledToTripID = Convert.ToInt32(dr["SecondLegAppledToTripID"]),
                        ThirdLegAppliedToTripID = Convert.ToInt32(dr["ThirdLegAppliedToTripID"]),
                        TripAreaType = (TripAreaTypes)dr["TripAreaType"],
                        TripPrice = Convert.ToDecimal(dr["TripPrice"]),
                        ExtraCharge = Convert.ToDecimal(dr["ExtraCharge"]),
                        MiniVanCharge = Convert.ToDecimal(dr["MiniVanCharge"]),
                        WaitingCahrge = Convert.ToDecimal(dr["WaitingCahrge"]),
                        DateAdded = Convert.ToDateTime(dr["DateAdded"]),
                        TripID = dr.HasColumn("TripID") ? Convert.ToInt32(dr["TripID"]) : 0,
                        NeedToConfirm = Convert.ToBoolean(dr["NeedToConfirm"]),
                        AssignedToDriverID = dr.HasColumn("AssignedToDriverID") ? Convert.ToInt32(dr["AssignedToDriverID"]) : 0,
                        IsCanceldTrip = Convert.ToBoolean(dr["TripIsCanceld"]),
                        SFromLocationComment = dr["sFromLocationComment"].ToString(),
                        SToLocationComment = dr["sToLocationComment"].ToString(),
                        SCustomerPassCodeID = Convert.ToInt32(dr["sCustomerPassCodeID"])//, scheduledTrip.SCustomerPassCodeID),

                    };
                    /*ETAToLocation1 = dr.HasColumn("ETAToLocation1") ?  Convert.ToDateTime(dr["ETAToLocation1"]) : new DateTime?(),
                    ETABackToBase = dr.HasColumn("ETABackToBase") ? Convert.ToDateTime(((TimeSpan)dr["ETABackToBase"]).Ticks) : new DateTime?(),*/
                    //  ETABackToBase = dr.HasColumn("ETABackToBase") ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETABackToBase"]).Ticks),
                    //ETAToLocation1 = dr["ETAToLocation1"] == DBNull.Value ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETAToLocation1"]).Ticks),
                    //  ETABackToBase = dr["ETABackToBase"] == DBNull.Value ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETABackToBase"]).Ticks),



                    /*  DrivenByDriverID = dr.HasColumn("DriverID") ? Convert.ToInt32(dr["DriverID"]) : 0,
                      ETAToLocation1 = dr["ETAToLocation1"] == DBNull.Value ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETAToLocation1"]).Ticks),
                      ETABackToBase = dr["ETABackToBase"] == DBNull.Value ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETABackToBase"]).Ticks),*/
                    //    if (dr.HasColumn("DriverID"))


                    // }
                    if (dr.HasColumn("sETAToLocation1"))
                    {
                        // if (dr[""] !DBNull.Value ){}
                        scheduledTrip.ETAToLocation1 = dr["sETAToLocation1"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["sETAToLocation1"]);
                        scheduledTrip.ETABackToBase = dr["sETABackToBase"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["sETABackToBase"]);
                        scheduledTrip.IsRoundTrip = dr["sIsRoundTrip"] == DBNull.Value ? new Boolean() : Convert.ToBoolean(dr["sIsRoundTrip"]);//Convert.ToBoolean(dr["IsRoundTrip]);
                                                                                                                                               //  scheduledTrip.ETABackToBase = dr["ETABackToBase"] == DBNull.Value ? new Nullable<DateTime>() : new DateTime(((TimeSpan)dr["ETABackToBase"]).Ticks);
                                                                                                                                               //convertoDatetime dr(["ETAToLocation1])
                    };
                    if (dr["sVehicleTypeRequested"] != DBNull.Value)
                    {


                        scheduledTrip.SVehicleTypeRequested = (VehicleTypes)dr["sVehicleTypeRequested"];
                    }

                    if (scheduledTrip.SVehicleTypeRequested == VehicleTypes.MiniVan) { scheduledTrip.NeedsMiniVan = true; }
                    scheduledTrips.Add(scheduledTrip);



                    if (includeMedicaidInfo)
                    {
                        if (dr["FirstName"] != DBNull.Value)
                        {

                            scheduledTrip.DrivenFirstName = dr["FirstName"].ToString();
                            scheduledTrip.DrivenLastName = dr["LastName"].ToString();
                            /* var mediciadInfo = new MedicaidTrip()
                             {
                                 //CIN = dr[""].ToString(),
                                 //  MedicaidDispatchTripNo = Convert.ToInt32(dr["MedicaidDispatchTripNo"]),//dr[""](),

                                 FirstName = dr["FirstName"].ToString(),
                                 LastName = dr["LastName"].ToString()
                             };/*/
                        }
                    }
                }

                return scheduledTrips;
            }
        }
        public bool Edit(ScheduledTrip scheduledTrip)
        {
            string serverMessage;
            var ScheduledTripIDParam = new SqlParameter("@ScheduledTripID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            int noOfRecAffected = this.ExecuteNonQuery("UpdateScheduledTrips", new SqlParameter[]
            {
                new SqlParameter("ScheduledTripID", scheduledTrip.ScheduledTripID),
                new SqlParameter("@ScheduledTripType", scheduledTrip.ScheduledTripType),
                new SqlParameter("@FidelisMedicaidReferencNo", scheduledTrip.FidelisMedicaidReferencNo),
                new SqlParameter("@NoOfLegs", scheduledTrip.NoOfLegs),
                new SqlParameter("@CustomerID", scheduledTrip.ScheduledByCustomer == null ? 0 : scheduledTrip.ScheduledByCustomer.CustomerID),
                new SqlParameter("@CustomerName", scheduledTrip.CustomerName == null ? "": scheduledTrip.ScheduledByCustomer.CustomerName),
                new SqlParameter("@FirstName",scheduledTrip.DrivenFirstName),
                new SqlParameter("@LastName",scheduledTrip.DrivenLastName),
                new SqlParameter("@CustomerPhoneNo", string.IsNullOrWhiteSpace( scheduledTrip.CustomerPhoneNo) ? scheduledTrip.ScheduledByCustomer.HomePhoneNo : scheduledTrip.CustomerPhoneNo),
                new SqlParameter("@PickupDate", scheduledTrip.PickupDate),
             //   new SqlParameter("@PickupTime", scheduledTrip.PickupTime.Value.TimeOfDay),
             // new SqlParameter("@PickupTime", scheduledTrip.PickupTime == null ? (TimeSpan.FromTicks(00)) :scheduledTrip.PickupTime.Value.TimeOfDay),
               //new SqlParameter("@PickupTime", scheduledTrip.PickupTime == null ? DateTime.Now  :scheduledTrip.PickupTime.Value), 
                  new SqlParameter("@PickupTime", scheduledTrip.PickupTime ),//== null ? Null :scheduledTrip.PickupTime.Value), 
                new SqlParameter("@PickupAddress", scheduledTrip.PickupAddress),
                new SqlParameter("@PickupCity", scheduledTrip.PickupCity),
                new SqlParameter("@PickupState", scheduledTrip.PickupState),
                new SqlParameter("@PickupZip", scheduledTrip.PickupZip),
                new SqlParameter("@DropOffAddress", scheduledTrip.DropOffAddress),
                new SqlParameter("@DropOffCity", scheduledTrip.DropOffCity),
                new SqlParameter("@DropOffState", scheduledTrip.DropOffState),
                new SqlParameter("@DropOffZip", scheduledTrip.DropOffZip),
                new SqlParameter("@FirstLegAppliedToTripID", scheduledTrip.FirstLegAppliedToTripID),
                new SqlParameter("@SecondLegAppliedToTripID", scheduledTrip.SecondLegAppledToTripID),
                new SqlParameter("@ThirdLegAppliedToTripID", scheduledTrip.ThirdLegAppliedToTripID),
                new SqlParameter("@TripPrice",scheduledTrip.TripPrice),
                new SqlParameter("@WaitingCahrge",scheduledTrip.WaitingCahrge),
                new SqlParameter("@ExtraCharge",scheduledTrip.ExtraCharge),
                new SqlParameter("@MiniVanCharge",scheduledTrip.MiniVanCharge),
                new SqlParameter("@TripAreaType",scheduledTrip.TripAreaType),
                new SqlParameter("@NeedToConfirm",scheduledTrip.NeedToConfirm),
                new SqlParameter("@ETAToLocation1",scheduledTrip.ETAToLocation1),
                new SqlParameter("@ETABackToBase",scheduledTrip.ETABackToBase),
                new SqlParameter("@IsRoundTrip",scheduledTrip.IsRoundTrip),
                new SqlParameter("@AssignedToDriverID",scheduledTrip.AssignedToDriverID),
                new SqlParameter("@IsCanceldTrip",scheduledTrip.IsCanceldTrip),
                new SqlParameter("@sFromLocationComment",scheduledTrip.SFromLocationComment),
                new SqlParameter("@sToLocationComment",scheduledTrip.SToLocationComment),
                new SqlParameter("@sVehicleTypeRequested",scheduledTrip.SVehicleTypeRequested),
                new SqlParameter("@sCustomerPassCodeID",scheduledTrip.SCustomerPassCodeID),
                new SqlParameter("@sNoOfStops",scheduledTrip.SNoOfStops),

            }, CommandType.StoredProcedure, out serverMessage);
            return noOfRecAffected > 0;
        }
        public static bool AddScheduledTrip(ScheduledTrip scheduledTrip)
        {
            using (var stDB = new ScheduledTripDB(null, null))
            {
                return stDB.Add(scheduledTrip);
            }
        }

        public static bool EditScheduledTrip(ScheduledTrip scheduledTrip)
        {
            using (var stDB = new ScheduledTripDB(null, null))
            {
                return stDB.Edit(scheduledTrip);
            }
        }

        public static ObservableCollection<ScheduledTrip> GetScheduledTrips(int tripID,
            DateTime? tripDate,
            DateTime? fromTripDate,
            DateTime? toTripDate,
            bool notAppliedTo,
            TripAreaTypes areaTripType,
            int NeedToConfirm = 0,
            bool includeMedicaidInfo = false)
        {
            using (ScheduledTripDB stDB = new ScheduledTripDB(null, null))
            {
                return stDB.Get(0, tripDate, fromTripDate, toTripDate, notAppliedTo, areaTripType, NeedToConfirm, includeMedicaidInfo);
            }
        }
    }

    public class DEExpenseDB : Database
    {
        public DEExpenseDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(DEExpense deExpense)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@DEExpenseID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@DailyEnvelopID", deExpense.DailyEnvelopeID),
                new SqlParameter("@ExpenseTypeID", deExpense.ExpenseTypeID),
                new SqlParameter("@ExpenseAmount", deExpense.ExpenseAmount),
                outParam
            };

            int noOfRecordsAffected = this.ExecuteNonQuery("AddDEExpenses", param, CommandType.StoredProcedure, out serverMessage);
            if (noOfRecordsAffected > 0)
            {
                deExpense.DEExpenseID = Convert.ToInt32(outParam.Value);
                return true;
            }
            else
                return false;
        }
        public bool Edit(DEExpense dEExpense)
        {
            string serverMessage;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@DEExpenseID",dEExpense.DEExpenseID),
                new SqlParameter("@DailyEnvelopID",dEExpense.DailyEnvelopeID),
                new SqlParameter("@ExpenseTypeID",dEExpense.ExpenseTypeID),
                new SqlParameter("@ExpenseAmount",dEExpense.ExpenseAmount)
            };

            int nofOfRecordsAffected = this.ExecuteNonQuery("UpdateDEExpenses", param, System.Data.CommandType.StoredProcedure, out serverMessage);
            return nofOfRecordsAffected > 0;
        }
        public static bool EditDEExpense(DEExpense dEExpense)
        {
            using (DEExpenseDB dEExpenseDB = new DEExpenseDB(null, null))
            {
                return dEExpenseDB.Edit(dEExpense);
            }
        }

        public ObservableCollection<DEExpense> Get(int dailyEnvelopeID)
        {
            var dEExpense = new ObservableCollection<DEExpense>();
            using (SqlDataReader dr = this.GetReader("GetDEExpenses",
                new SqlParameter[]
                {
                    new SqlParameter("@DailyEnvelopeID",dailyEnvelopeID)
                }, System.Data.CommandType.StoredProcedure))
                while (dr.Read())
                {
                    dEExpense.Add(new DEExpense()
                    {
                        DEExpenseID = Convert.ToInt32(dr["DEExpenseID"]),
                        DailyEnvelopeID = Convert.ToInt32(dr["DailyEnvelopeID"]),
                        ExpenseTypeID = Convert.ToInt32(dr["ExpenseTypeID"]),
                        /*   Expense = new ExpenseType()
                           {
                              ExpenseTypeID =Convert.ToInt32(dr["ExpenseTypeID"]),
                              ExpenseCode = dr["ExpenseCode"].ToString()   
                           },*/
                        ExpenseAmount = Convert.ToDecimal(dr["ExpenseAmount"]),
                    });
                }

            return dEExpense;
        }

        public static ObservableCollection<DEExpense> GetDEExpense(int dailyEnvelopeID)
        {

            using (DEExpenseDB dEExpenseDB = new DEExpenseDB(null, null))
            {
                return dEExpenseDB.Get(dailyEnvelopeID);
            }

        }
    }

    public class DETicketsReceivedDB : Database
    {
        public DETicketsReceivedDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public bool Add(TicketsReceived ticketsReceived)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@DETicketReceivedID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@DailyEnvelopeID", ticketsReceived.DailyEnvelopID),
                new SqlParameter("@FromTicketBookID", ticketsReceived.FromTicketBook.TicketBookID),
                new SqlParameter("@TicketValue", ticketsReceived.TicketValue),
                new SqlParameter("@QtyBooked", ticketsReceived.QtyBooked),
                new SqlParameter("@QtyReceived",ticketsReceived.QtyReceived),
                new SqlParameter("@QtyMissing", ticketsReceived.QtyMissing),
                new SqlParameter("@TotalValue", ticketsReceived.TotalValue),
                outParam
            };

            int noOfRecordsAffected = this.ExecuteNonQuery("AddDETicketsReceived", param, CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
            {
                ticketsReceived.DETicketReceivedID = Convert.ToInt32(outParam.Value);
                return true;
            }
            else
                return false;
        }

        public bool Edit(TicketsReceived ticketsReceived)
        {
            string serverMessage;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@DETicketReceivedID",ticketsReceived.DETicketReceivedID),
                new SqlParameter("@DailyEnvelopeID",ticketsReceived.DailyEnvelopID ),
                new SqlParameter("@FromTicketBookID",ticketsReceived.FromTicketBook.TicketBookID),
                new SqlParameter("@TicketValue",ticketsReceived.TicketValue),
                new SqlParameter("@QtyBooked",ticketsReceived.QtyBooked),
                new SqlParameter("@QtyReceived",ticketsReceived.QtyReceived),
                new SqlParameter("@QtyMissing",ticketsReceived.QtyMissing),
                new SqlParameter("@TotalValue",ticketsReceived.TotalValue)
            };

            int nofOfRecordsAffected = this.ExecuteNonQuery("UpdateDETicketsReceived", param, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (nofOfRecordsAffected > 0)
            {
                using (DETicketLogDB DETicketLogDB = new DETicketLogDB(this.Connection, this.Transaction))
                {
                    //wana add the new records
                    var trtl = ticketsReceived.DETicketLogs.Where(tl => tl.TicketLogId == 0);
                    ObservableCollection<DETicketLog> newDEtl = new ObservableCollection<DETicketLog>(trtl);
                    DETicketLogDB.Add(newDEtl);
                }
            }
            return nofOfRecordsAffected > 0;

        }

        public static bool EditTicketBooksRecived(TicketsReceived ticketsReceived)
        {
            using (DETicketsReceivedDB dETicketsReceivedDB = new DETicketsReceivedDB(null, null))
            {
                return dETicketsReceivedDB.Edit(ticketsReceived);
            }

        }

        public ObservableCollection<TicketsReceived> Get(int dailyEnvelopeID)
        {

            var ticketsReceived = new ObservableCollection<TicketsReceived>();
            using (SqlDataReader dr = this.GetReader("GetDETicketsReceived",
                new SqlParameter[]
                {
                    new SqlParameter("@DailyEnvelopeID",dailyEnvelopeID)
                }, System.Data.CommandType.StoredProcedure))
                while (dr.Read())
                {
                    ticketsReceived.Add(new TicketsReceived()
                    {
                        DETicketReceivedID = Convert.ToInt32(dr["DETicketReceivedID"]),
                        DailyEnvelopID = Convert.ToInt32(dr["DailyEnvelopeID"]),
                        //AmountOfMissingTickets = Convert.ToDecimal(dr[""]),
                        FromTicketBook = new TicketBook()
                        {
                            TicketBookID = Convert.ToInt32(dr["FromTicketBookID"]),
                            BookName = dr["BookName"].ToString(),
                        },
                        TicketValue = Convert.ToDecimal(dr["TicketValue"]),
                        QtyBooked = Convert.ToInt32(dr["QtyBooked"]),
                        QtyReceived = Convert.ToInt32(dr["QtyReceived"]),
                        QtyMissing = Convert.ToInt32(dr["QtyMissing"]),
                        TotalValue = Convert.ToDecimal(dr["TotalValue"]),
                    });
                }

            return ticketsReceived;
        }
        public static ObservableCollection<TicketsReceived> GetTicketsRecived(int dailyEnvelopeID)
        {
            using (DETicketsReceivedDB dETicketsReceivedDB = new DETicketsReceivedDB(null, null))
            {
                return dETicketsReceivedDB.Get(dailyEnvelopeID);

            }
        }

        public ObservableCollection<TicketsReceived> GetTicketsOwed(int DriverID)
        {
            var Owe = new ObservableCollection<TicketsReceived>();
            using (SqlDataReader dr = this.GetReader("GetTicketsOwed",
                new SqlParameter[]
                {
                    new SqlParameter("@DriverID",DriverID)
                }
                , System.Data.CommandType.StoredProcedure))
                while (dr.Read())
                {
                    Owe.Add(new TicketsReceived()
                    {
                        Owes = Convert.ToInt32(dr["Ows"]),

                        FromTicketBook = new TicketBook()
                        {
                            TicketBookID = Convert.ToInt32(dr["FromTicketBookID"])
                        }
                    });
                }

            return Owe;
        }
        public static ObservableCollection<TicketsReceived> getTicketsOwed(int DriverID)
        {
            using (DETicketsReceivedDB TicketsOwed = new DETicketsReceivedDB(null, null))//(null, null))
            {
                return TicketsOwed.GetTicketsOwed(DriverID);
            }

        }



    }

    public class ExpenseTypeDB : Database
    {
        public ExpenseTypeDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<ExpenseType> Get(int expenseTypeID)
        {
            var expenseTypes = new ObservableCollection<ExpenseType>();
            using (SqlDataReader dr = this.GetReader("GetExpenseTypes", new SqlParameter[] { new SqlParameter("@ExpenseTypeID", expenseTypeID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    expenseTypes.Add(new ExpenseType()
                    {
                        ExpenseTypeID = Convert.ToInt32(dr["ExpenseTypeID"]),
                        ExpenseCode = dr["ExpenseCode"].ToString()
                    });
                }
            }

            return expenseTypes;
        }

        public static ObservableCollection<ExpenseType> GetExpenseTypes(int expenseTypeID = 0)
        {
            using (ExpenseTypeDB etDB = new ExpenseTypeDB(null, null))
            {
                return etDB.Get(expenseTypeID);
            }
        }
        private bool Add(ExpenseType et)
        {
            string serverMessage;
            var outParam = new SqlParameter("@ExpenseTypeID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddExpenseTypes", new SqlParameter[]
            {
                new SqlParameter("@ExpenseCode", et.ExpenseCode),
                outParam
            }, CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                et.ExpenseTypeID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }
        public static bool AddExpenseType(ExpenseType expenseType)
        {
            using (var expenseTypeDB = new ExpenseTypeDB(null, null))
            {
                return expenseTypeDB.Add(expenseType);
            }
        }




        private bool Edit(ExpenseType et)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateExpenseType", new SqlParameter[]
            {
                new SqlParameter("@ExpenseTypeID",et.ExpenseTypeID),
                new SqlParameter("@ExpenseCode",et.ExpenseCode)
            }, CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }

        public static bool EditExpenseType(ExpenseType et)
        {
            using (ExpenseTypeDB etDB = new ExpenseTypeDB(null, null))
            {
                return etDB.Edit(et);
            }
        }




        /**/
    }

    public class TicketBookDB : Database
    {
        public TicketBookDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<TicketBook> Get(int ticketBookID)
        {
            ObservableCollection<TicketBook> ticketBooks = new ObservableCollection<TicketBook>();
            using (SqlDataReader dr = this.GetReader("GetTicketBooks", new SqlParameter[] { new SqlParameter("@TicketBookID", ticketBookID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    ticketBooks.Add(new TicketBook()
                    {
                        TicketBookID = Convert.ToInt32(dr["TicketBookID"]),
                        BookName = dr["BookName"].ToString(),
                        BookPrice = Convert.ToDecimal(dr["BookPrice"]),
                        TicketValue = Convert.ToDecimal(dr["TicketValue"]),
                        IsCompanyBook = Convert.ToBoolean(dr["IsCompanyBook"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"])
                    });
                }
            }

            return ticketBooks;
        }

        private bool Add(TicketBook tb)
        {
            string serverMessage;
            var outParam = new SqlParameter("@TicketBookID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddTicketBooks", new SqlParameter[]
            {
                new SqlParameter("@BookName", tb.BookName),
                new SqlParameter("@BookPrice", tb.BookPrice),
                new SqlParameter("@TicketValue", tb.TicketValue),
                new SqlParameter("@IsCompanyBook", tb.IsCompanyBook),
                outParam
            }, CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                tb.TicketBookID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }

        private bool Edit(TicketBook tb)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateTicketBooks", new SqlParameter[]
            {
                new SqlParameter("@TicketBookID", tb.TicketBookID),
                new SqlParameter("@BookName", tb.BookName),
                new SqlParameter("@BookPrice", tb.BookPrice),
                new SqlParameter("@TicketValue", tb.TicketValue),
                new SqlParameter("@IsCompanyBook", tb.IsCompanyBook)
            }, CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }

        public static ObservableCollection<TicketBook> GetTicketBooks(int ticketBookID = 0)
        {
            using (TicketBookDB tbDB = new TicketBookDB(null, null))
            {
                return tbDB.Get(ticketBookID);
            }
        }

        public static bool EditTicketBook(TicketBook tb)
        {
            using (TicketBookDB tbDB = new TicketBookDB(null, null))
            {
                return tbDB.Edit(tb);
            }
        }



        public static bool AddTicketBook(TicketBook tb)
        {
            using (TicketBookDB tbDB = new TicketBookDB(null, null))
            {
                return tbDB.Add(tb);
            }
        }
    }


}
