﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace CDS
{
    /// <summary>
    /// Interaction logic for ReportViewerWindow.xaml
    /// </summary>
    public partial class ReportViewerWindow : Window , IDisplayTextBlock
    {
        public ReportViewerWindow()
        {
            InitializeComponent();
        }

          string ReportName;
          public ReportViewerWindow(Telerik.Reporting.Report report, string reportName)
            : this()
        {
            //type name need full qulifeid report name then comma then assembly name
            var typeReportSource = new Telerik.Reporting.TypeReportSource() { TypeName = String.Format("Rpts.{0}, Rpts", reportName) };
            this.reportViewer1.ReportSource = typeReportSource;

              //Old version
            //this.reportViewer1.Report = report;
            //this.ReportName = reportName;
        }









          

          public string HeaderText
          {
              get
              {
                  return this.ReportName;
              }
              set
              {
                  throw new NotImplementedException();
              }
          }

          public bool AllowOnlyOnce
          {
              get
              {
                  return false;
              }
              set
              {
                  throw new NotImplementedException();
              }
          }

          public bool ConfirmOnClosing
          {
              get
              {
                  return true;
              }
              set
              {
                  throw new NotImplementedException();
              }
          }

          public string ConfirmOnClosingText
          {
              get
              {
                  return "";
              }
              set
              {
                  throw new NotImplementedException();
              }
          }
    }
}
