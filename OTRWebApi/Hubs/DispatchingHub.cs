﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using DAL_EF;
using BOL_OTR;
using OTRWebApi.General;

namespace OTRSignalRWebService
{
    //[Authorize(RequireOutgoing = false)]
    public class DispatchingHub : Hub
    {

        #region private methods

        private bool IsDispatchingServiceConnected()
        {
            return this.Clients.Group("DispatchingService") != null;
        }

        private bool IsAutheticated(string loginUserName, string loginPassword)
        {
            return DAL_OTR.DriverDb.AuthenticateDriver(loginUserName, loginPassword) ?? false;
        }

        #endregion

        #region tablets calls - call that the tablet makes to the hub

        public void Join(string clientType)
        {
            Groups.Add(Context.ConnectionId, clientType);
        }

        public bool Join(string loginUserName, string loginPassword, string clientType)
        {
            //if (clientType == "Dispatcher")
            //{
            Groups.Add(Context.ConnectionId, clientType);

            return true;
            //}
            //else if (IsDispatchingServiceConnected())
            //{
            //    Groups.Add(Context.ConnectionId, clientType);

            //    return true;
            //}
            //else
            //    throw new Exception("Dispatching Service is NOT connected to the hub");
        }

        public TicketPaid_OTR[] GetTicketTypes()
        {
            BOL_OTR.TicketPaid_OTR[] _ticketsPaid;

            _ticketsPaid = new BOL_OTR.TicketPaid_OTR[] {
                new BOL_OTR.TicketPaid_OTR ()
                {
                    TicketID = 0,
                    TicketName = "None",
                    TicketValue = 0
                },
                new BOL_OTR.TicketPaid_OTR ()
                {
                    TicketID = 1,
                    TicketName = "Ticket 1",
                    TicketValue = 3
                },
                new BOL_OTR.TicketPaid_OTR ()
                {
                    TicketID = 2,
                    TicketName = "Leibermans",
                    TicketValue = 5
                }
            };

            return _ticketsPaid;
        }

        public void SignInDriver(int driverID)
        {
            this.Clients.Group("DispatchingService").SignInDriver(driverID);
        }

        public void RemoveDriver(int driverID)
        {
            this.Clients.Group("DispatchingService").RemoveDriver(driverID);
        }

        public void DriverStatusChanged(int driverID, BOL_OTR.DriverStatusOptions status)
        {
            this.Clients.Group("DispatchingService").DriverStatusChanged(driverID, status);
        }

        //driver requests break
        public void RequestBreak(int driverID, bool longBreak)
        {
            this.Clients.Group("DispatchingService").DriverRequestedBreak(driverID, longBreak);
        }
        
        //driver accepted the trip
        public void TripStatusChanged(int tripID, BOL_OTR.TripDetailedStatusOptions changedTo)
        {
            this.Clients.Group("DispatchingService").TripStatusChanged(tripID, changedTo);
        }

        //report location
        public void TabletReportedLocation(double latitude, double longitude, float speed)
        {
            DAL_EF.DAL_OTR.TabletDb.LogGPSRecording(Context.ConnectionId, latitude, longitude, speed);
            var driverID = DAL_OTR.DriverDb.GetDriversIDFromConnectionID(Context.ConnectionId);
            var tabletID = DAL_OTR.TabletDb.IsTabletNameInSystem(Context.QueryString["tabletName"]);

            this.Clients.Group("Dispatcher").TabletReportedLocation(driverID, tabletID, latitude, longitude, speed);
        }

        //set driver to trip - or reject assignment
        public void DriverResponseToTripAssignment(int driverID, int tripId, bool accepted)
        {
            this.Clients.Group("DispatchingService").DriverResponseToTripAssignment(driverID, tripId, accepted);

            Logging.Log(new Log() { LogedID = tripId, LogedName = LogNames.Trip.ToString(), LogType = "SIGNALR", LogedValue = accepted.ToString() });
        }

        public void DriverRequestChangeDestinationPermission(int driverID, int tripId)
        {
            this.Clients.Group("Dispatcher").DriverRequestChangeDestinationPermission(driverID, tripId);
        }

        public void SaveTripDetails(Trip_OTR trip)
        {
            this.Clients.Group("DispatchingService").SaveTripDetails(trip);
        }

        #endregion

        #region dispatcher calls - call that the dispacher makes to the hub

        /// <summary>
        /// Dispatcher will send a trip to a driver
        /// </summary>
        /// <param name="trip">Trip object to send</param>
        public void DispatchTripToDriver(Trip_OTR trip)
        {
            var connectionID = DAL_EF.DAL_OTR.DriverDb.GetDriversTabletConnectionID(trip.DriverID);

            this.Clients.Client(connectionID).AssignTrip(trip);
            
            this.Clients.Group("DispatchingService").DispatchedNewTripRequestToDriver(trip.TripID);

            //if (client.AssignTrip(trip))
                Logging.Log(new Log() { LogedID = trip.TripID, LogedName = LogNames.Trip.ToString(), LogType = "SIGNALR", LogedValue = "SENT_TO_TABLET" });
        }

        public void PermissionRequestResponse(int driverID, bool isPermited)
        {
            var connectionID = DAL_EF.DAL_OTR.DriverDb.GetDriversTabletConnectionID(driverID);

            this.Clients.Client(connectionID).PermissionRequestResponse(isPermited);
        }

        /// <summary>
        /// Response to driver on his break request
        /// </summary>
        /// <param name="driverID">send tdriver id</param>
        /// <param name="approved">true as approved, otherwise not approved</param>
        public async void ResponseToBreakRequest(int driverID, bool approved, bool longBreak, BOL_OTR.Trip_OTR takeTripBeforeBreak)
        {
            var connectionID = DAL_EF.DAL_OTR.DriverDb.GetDriversTabletConnectionID(driverID);

            var client = await this.Clients.Client(connectionID);

            //send approval to app
            //if dispatcher requested to take a trip before the break, send along the trip
            client.BreakRequestResponse(approved, takeTripBeforeBreak);            
        }

        
        #endregion

        #region hub methods
        /// <summary>
        /// On Connected mark the user as connected and store the connectionid in db
        /// </summary>
        /// <returns></returns>
        public override Task OnConnected()
        {
            var clientType = Context.QueryString["clientType"];
            if (clientType == "tablet")
            {
                var tabletName = Context.QueryString["tabletName"];

                DAL_OTR.TabletDb.MarkTabletConnectionStatus(tabletName, true, Context.ConnectionId);
            }
            else if (clientType == "dispatcher")
            {
                var dispatcherID = Context.QueryString["dispatcherID"];
                DAL_OTR.DispatcherDb.MarkDispatcherConnectionStatus(Convert.ToInt32(dispatcherID), true, Context.ConnectionId);
            }
            return base.OnConnected();

        }

        public override Task OnReconnected()
        {
            var clientType = Context.QueryString["clientType"];
            if (clientType == "tablet")
            {
                var tabletName = Context.QueryString["tabletName"];

                DAL_OTR.TabletDb.MarkTabletConnectionStatus(tabletName, true, Context.ConnectionId);
            }
            else if(clientType == "dispatcher")
            {
                var dispatcherID = Context.QueryString["dispatcherID"];
                DAL_OTR.DispatcherDb.MarkDispatcherConnectionStatus(Convert.ToInt32(dispatcherID), true, Context.ConnectionId);
            }
            return base.OnReconnected();
        }


        /// <summary>
        /// OnDisconnected Mark the user/dispatcher as disconnected and delete the connectionid
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            var clientType = Context.QueryString["clientType"];
            if (clientType == "tablet")
            {

                var tabletName = Context.QueryString["tabletName"];
                DAL_OTR.TabletDb.MarkTabletConnectionStatus(tabletName, false, Context.ConnectionId);
            }

            else if (clientType == "dispatcher")
            {
                var dispatcherID = Context.QueryString["dispatcherID"];
                DAL_OTR.DispatcherDb.MarkDispatcherConnectionStatus(Convert.ToInt32(dispatcherID), false, Context.ConnectionId);
            }
            return base.OnDisconnected(stopCalled);
        }
        #endregion
    }
}