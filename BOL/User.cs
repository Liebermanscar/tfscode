﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class User
    {
        int _userID;

        [DataMember]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }
        string _userName;

        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string UserCode { get; set; }
        string _password;

        [DataMember]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }


        UserTypes _userType;

        [DataMember]
        public UserTypes UserType
        {
            get { return _userType; }
            set { _userType = value; }
        }


        public override string ToString()
        {
            return this.UserName;
        }
    }

    [DataContract]
    public enum UserTypes
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        DataEntry = 1,
        [EnumMember]
        ReportsOnly = 2,
        [EnumMember]
        Administrator = 3,
        [EnumMember]
        Supervisor = 4,
        [EnumMember]
        BookKeeping = 5,
        [EnumMember]
        DriverManger = 6

    }
}
