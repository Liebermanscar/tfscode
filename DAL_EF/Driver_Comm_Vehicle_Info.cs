//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL_EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Driver_Comm_Vehicle_Info
    {
        public int DriverID { get; set; }
        public string DriverCode { get; set; }
        public string DriverName { get; set; }
        public int CommisionTypeID { get; set; }
        public int DrivesVehicleID { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public string LicenseNumber { get; set; }
        public string CellNumber { get; set; }
        public Nullable<bool> DriverActive { get; set; }
        public Nullable<bool> AllowsChargeToHisAccount { get; set; }
        public Nullable<int> Expr1 { get; set; }
        public string CommisionTypeCode { get; set; }
        public Nullable<double> DriversCommision { get; set; }
        public Nullable<double> PercentOfGasDriverPays { get; set; }
        public Nullable<int> VehicleID { get; set; }
        public string VehicleCode { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public Nullable<int> Year { get; set; }
        public byte[] VehicleImage { get; set; }
        public Nullable<bool> IsCompanyEZPass { get; set; }
        public Nullable<int> EZPassTagID { get; set; }
        public string TagNumber { get; set; }
        public Nullable<int> Expr2 { get; set; }
    }
}
