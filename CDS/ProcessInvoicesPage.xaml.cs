﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ProcessInvoicesPage.xaml
    /// </summary>
    public partial class ProcessInvoicesPage : Page, IDisplayTextBlock
    {
        public ProcessInvoicesPage()
        {
            InitializeComponent();
            this.CutOffDatePicker.SelectedDate = DateTime.Today;
            this.InvoiceFromDatePicker.SelectedDate = DateTime.Today.AddDays(-30);


        }

        public ObservableCollection<UnbilledCustomer> UnbilledCustomersButton
        {
            get { return this.DataContext as ObservableCollection<UnbilledCustomer>; }
            set { this.DataContext = value; }
        }

        private void showUnbilledCustomersButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                this.UnbilledCustomersButton = DAL.InvoiceDB.GetUnbilledCustomersByDate(this.InvoiceFromDatePicker.SelectedDate.Value, this.CutOffDatePicker.SelectedDate.Value);
                this.processInvoicesButton.IsEnabled = this.UnbilledCustomersButton.Count > 0;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Process Invoice Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion              

        private void processInvoicesButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool IncludeTripsWithZeroTotalPrice = false;
                if (this.UnbilledCustomersButton.Count(i => i.Selected) <= 0)
                {

                    MessageBox.Show("No Invoice was selected to process", "No Invoice Selected", MessageBoxButton.OK);
                    return;
                }
                if (this.chkbIncludeTripsWithZeroTotalPrice.IsChecked == true)
                    IncludeTripsWithZeroTotalPrice = true;

                /*  int invoiceBatcheID;
                  if (DAL.InvoiceDB.ProcessInvoicesForUnbilledTrips(this.CutOffDatePicker.SelectedDate.Value, out invoiceBatcheID))
                  {
                      IList<Invoice> invoicesProcessed = DAL.InvoiceDB.GetInvoices(0, 0, invoiceBatcheID, true);
                      UIController.PrintInvoice(invoicesProcessed.ToList());
                      UIController.CloseActivePage();
                  }*/
                // foreach (var pmt in this.UnbilledCustomersButton.Where(i => i.Selected))


                {
                    if (InvoiceFromDatePicker == null)
                        return;
                    var customerToInvoiceID = UnbilledCustomersButton.Where(i => i.Selected).Select(i => i.CustomerToInvoice.CustomerID).ToList();
                    string delimitedCustomerToInvoiceID = string.Join(",", customerToInvoiceID.ToArray());
                    int invoiceBatcheID;
                    if (DAL.InvoiceDB.ProcessInvoicesForUnbilledTrips(delimitedCustomerToInvoiceID, this.InvoiceFromDatePicker.SelectedDate.Value, this.CutOffDatePicker.SelectedDate.Value, IncludeTripsWithZeroTotalPrice, out invoiceBatcheID))
                    {
                        IList<Invoice> invoicesProcessed = DAL.InvoiceDB.GetInvoices(0, 0, invoiceBatcheID, true);
                        if (invoicesProcessed.Count == 0)
                            return;

                        /*   foreach (var custInvoice in invoicesProcessed)
                           {

                               custInvoice.InvoiceDate = CutOffDatePicker.SelectedDate.Value; ;
                               custInvoice.LastInvoiceDate = InvoiceFromDatePicker.SelectedDate.Value;
                           }*/
                        //foreach (var custInvoice in invoicesProcessed)
                        //{

                        //    if (InvoiceFromDatePicker ==null)
                        //        return;
                        //    custInvoice.PaymentsFromThisBillingCycle = DAL.CustomerPaymentInfoDB.GetPayments(custInvoice.InvoicedCustomer.CustomerID, 0, this.InvoiceFromDatePicker.SelectedDate.Value, this.CutOffDatePicker.SelectedDate.Value).ToList();
                        //    DateTime dtOver30 = CutOffDatePicker.SelectedDate.Value;
                        //      dtOver30 = dtOver30.AddDays(-30);
                        //    custInvoice.TotalDueOver30Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtOver30);
                        //    DateTime dtover60 = CutOffDatePicker.SelectedDate.Value;//.AddDays(-60);
                        //    dtover60 = dtover60.AddDays(-60);
                        //    custInvoice.TotalDueOver60Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtover60);
                        //    DateTime dtover90 = CutOffDatePicker.SelectedDate.Value;//.AddDays(-90);
                        //    dtover90 = dtover90.AddDays(-90);
                        //    custInvoice.TotalDueOver90Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtover90);
                        //}


                        if (invoicesProcessed.Count == 0)
                            return;
                        UIController.PrintInvoice(invoicesProcessed.ToList());
                        UIController.CloseActivePage();
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void menuItemSelectAll_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                var itp = this.dgInvoicesToProcess.Items.Cast<UnbilledCustomer>();

                foreach (var i in itp)
                {
                    if (sender == this.menuItemSelectAll)
                    {

                        i.Selected = true;

                    }
                    else
                        i.Selected = false;
                }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void menuItemUnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            var itp = this.dgInvoicesToProcess.Items.Cast<UnbilledCustomer>();

            foreach (var i in itp)
            {
                if (sender == this.menuItemUnSelectAll)
                {
                    i.Selected = false;

                }
                else
                    i.Selected = true;
            }

        }
        UnbilledCustomer SelectedInvoice
        {
            get { return this.dgInvoicesToProcess.SelectedItem as UnbilledCustomer; }
        }

        private void menuItemShowInvoiceDetails_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedInvoice == null)
                return;


        }
        //private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{

        //    DataGridCell cell = sender as DataGridCell;

        //    if (!cell.IsEditing)
        //    {
        //        // enables editing on single click

        //        if (!cell.IsFocused)

        //            cell.Focus();

        //        if (!cell.IsSelected)

        //            cell.IsSelected = true;
        //    }

        //}

    }
}
