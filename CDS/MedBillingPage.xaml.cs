﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for MedBillingPage.xaml
    /// </summary>
    public partial class MedBillingPage : Page, IDisplayTextBlock
    {
        public MedBillingPage()
        {
            InitializeComponent();
        }

        string[] panFromWeb;
        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                panFromWeb = UIController.GetMedicaidPANFromWeb(this.dpkrFrom.SelectedDate, this.dpkrTo.SelectedDate);

                if (panFromWeb.Count() > 0)
                {
                    string fileName = string.Format("PANFromWeb/{0}.txt", this.GetFileName());
                    System.IO.File.WriteAllLines(fileName, panFromWeb);

                    //this.txtFileName.Text = fileName;
                    this.txtStatus.Text = string.Format("There are {0} records to import", panFromWeb.Count());
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public string GetFileName()
        {
            try
            {
                var dateTimeNowFormated = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt").Replace("/", ".");
                dateTimeNowFormated = dateTimeNowFormated.Replace(":", ".");
                dateTimeNowFormated = dateTimeNowFormated.Replace(" ", "_");

                string fileName = string.Format("PANFromWeb_{0}", dateTimeNowFormated);

                return fileName;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex); return null;
            }
        }

        private void btnGetExtarnelFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                string filePath;
                openFileDialog.Filter = "Text files (*.txt)|*.txt";

                if (openFileDialog.ShowDialog() == true && openFileDialog.FileName.Length > 0)
                {
                    filePath = openFileDialog.FileName;
                    this.txtFileName.Text = filePath;
                    var s = this.GetRecordsFromFile(filePath);
                    this.txtStatus.Text = string.Format("There are {0} records to import", s.Count());
                    panFromWeb = s;
                }

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public string[] GetRecordsFromFile(string path)
        {
            try
            {
                string fileContent = System.IO.File.ReadAllText(path);
                var ss = new List<string>();
                var s = fileContent.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                ss.AddRange(s.Skip(1).ToList()); //I want to skip the header of the text file

                return ss.ToArray<string>();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex); return null;
            }
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //string[] fileContent;

                //if (panFromWeb != null)
                //    fileContent = panFromWeb;
                //else
                //    fileContent = this.GetRecordsFromFile(this.txtFileName.Text);

                if (panFromWeb == null)
                {
                    MessageBox.Show("Nothing to import");
                    return;
                }

                var medicaidPANParsed = UIController.ParseMedicaidPAN(panFromWeb);
                //this.txtStatus.Text = string.Format("There are {0} records to import", medicaidPANParsed.Count);
                //System.Threading.Thread.Sleep(500); //refresh ui

                if (medicaidPANParsed.Count > 0)
                {
                    this.IsEnabled = false;
                    System.Threading.Thread.Sleep(250);
                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            if (DAL.MedicaidTripPanDB.AddMedicaidTripsPan(medicaidPANParsed))
                            {
                                App.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    this.txtStatus.Text = "Import Completed";
                                    this.IsEnabled = true;
                                    panFromWeb = null;
                                }));

                            }
                            else
                            {
                                App.Current.Dispatcher.Invoke((Action)(() =>
                                {
                                    this.txtStatus.Text = "Import NOT Completed";
                                    this.IsEnabled = true;
                                    panFromWeb = null;
                                }));
                            }
                        }
                        catch (Exception ex)
                        {
                            App.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                UIController.DisplayErrorMessage(ex);
                            }));
                        }

                    });

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        #region IDisplay
        public string HeaderText
        {
            get
            {
                return "Med Billing";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion IDisplay

        private void btnGetUnbilledTrips_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Location> locations = DAL.LocationsDB.GetLocations();
                List<TollRate> tollPrices = DAL.TollRatesDB.GetTollRates();

                foreach (TollRate tr in tollPrices)
                {
                    tr.FromLocation = locations.Where(l => l.LocationID == tr.FromLocationID).FirstOrDefault();
                    tr.ToLocations = locations.Where(l => l.LocationID == tr.ToLocationID).FirstOrDefault();
                }

                ObservableCollection<MedicaidTrip> trips = new ObservableCollection<MedicaidTrip>(DAL.MedicaidTripDB.GetMedicaidTrips(0, 0, null, true, false, "A0100", true).OrderBy(t => t.InvoiceNo));

                foreach (MedicaidTrip mt in trips)
                {
                    TollRate tollRate = tollPrices.Where(tp => tp.FromLocation.City == mt.PickUpCity && tp.FromLocation.State == mt.PickupState &&
                                                               tp.ToLocations.City == mt.DropOffCity && tp.ToLocations.State == mt.DropOffState).FirstOrDefault();

                    if (tollRate != null)
                        mt.TollPrice = tollRate.EZPrice;
                }

                this.Grid_Tolls.ItemsSource = trips;
                this.txtCount.Text = string.Format("{0} Records", trips.Count);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
    }
}
