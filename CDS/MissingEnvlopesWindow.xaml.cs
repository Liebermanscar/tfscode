﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for MissingEnvlopesWindow.xaml
    /// </summary>
    public partial class MissingEnvlopesWindow : Window
    {
        public MissingEnvlopesWindow()
        {
            InitializeComponent();
        }
        public MissingEnvlopesWindow(ObservableCollection<DateTime> missingDates)
            : this()
        {
            if (missingDates != null)
                this.ActiveMissingDates = missingDates;
        }

        public ObservableCollection<DateTime> ActiveMissingDates
        {
            get
            {
                return this.DataContext as ObservableCollection<DateTime>;
            }
            set
            {
                this.DataContext = value;
            }
        }

    }
}
