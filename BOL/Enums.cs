﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public enum TripStatusTypes
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        ReceivedCall = 1,
        [EnumMember]
        Scheduled = 2,
        [EnumMember]
        Assigned = 3,
        [EnumMember]
        InProcess = 4,
        [EnumMember]
        Complete = 5,
        [EnumMember]
        Canceled = 6,
        [EnumMember]
        SentToDriver = 7
    }

    public enum TripAreaTypes
    {
        All = 0,
        Local = 1,
        LongDistance = 2
    }

    public enum VehicleTypes
    {
        //   [DisplayName("not specified")] 
        Not_Specified = 0,
        Car = 1,
        MiniVan = 2
    }

    [DataContract]
    public enum TripTypes
    {
        [EnumMember]
        Regular = 0,
        [EnumMember]
        Charge = 1,
        [EnumMember]
        DriverTrip = 2
    }

    public enum ScheduledTripTypes
    {
        ByCustomer = 1,
        ByMedicaid = 2,
        ByFidelis = 3
    }

    public enum DriverStatusOptions
    {
        None = 0,
        NintyEight = 1,
        ShortBreak = 2,
        LongTrip = 202,
        Assigned = 3,
        LongBreak = 4,
        SignedOut = 5
    }
}
