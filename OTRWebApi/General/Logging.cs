﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL_EF;

namespace OTRWebApi.General
{
    public static class Logging
    {
        public static LogToOptions LogToOption = LogToOptions.Database;

        

        public static void Log(DAL_EF.Log logInfo)
        {
            if(LogToOption == LogToOptions.Database)            
                DAL_EF.DAL_OTR.LogDb.AddLog(logInfo);            
        }
    }    

    public enum LogToOptions
    {
        Database,
        File
    }

    public enum LogType
    {

    }

    public enum LogNames
    {
        Trip
    }
}