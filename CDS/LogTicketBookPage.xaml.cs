﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for LogTicketBookPage.xaml
    /// </summary>
    public partial class LogTicketBookPage : Page, IDisplayTextBlock
    {
        public LogTicketBookPage()
        {
            InitializeComponent();

        }
        public LogTicketBookPage(GenerateTicketBookStatuses generateTicketBookStatuses) : this()
        {

            try
            {
                this.ActiveGenerateTicketBookStatuses = generateTicketBookStatuses;

                //gets the list of ticketBook Types
                this.TicketBooks = DAL.TicketBookDB.GetTicketBooks();
                this.TicketBookIDComboBox.ItemsSource = TicketBooks;

                this.ActiveGenerateTicketBookStatuses.Posessions = DAL.TicketBookStatusDB.GetPossessions();
                if (this.TicketBookIDComboBox.SelectedValue != null)
                    this.ActiveGenerateTicketBookStatuses.TicketBookID = (int)this.TicketBookIDComboBox.SelectedValue;
                //this.InPossessionComboBox.ItemsSource = this.TicketBookStatusesToGenarate.PosessionsFilterd;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void showError(string err)
        {
            MessageBox.Show(err, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public GenerateTicketBookStatuses ActiveGenerateTicketBookStatuses
        {
            get { return this.DataContext as GenerateTicketBookStatuses; }
            set { this.DataContext = value; }
        }

        ObservableCollection<TicketBook> TicketBooks { get; set; } //Ticket Book Types

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ActiveGenerateTicketBookStatuses.GenarateBooks();

                int startBook = this.ActiveGenerateTicketBookStatuses.StartBookNumber;
                int endBook = this.ActiveGenerateTicketBookStatuses.EndBookNumber;
                int bookType = this.ActiveGenerateTicketBookStatuses.TicketBookID;
                int user = UIController.CurrentSession.SessionUser.UserID;

                //if ((this.ActiveGenerateTicketBookStatuses.BookStatus == TicketBookStatusOptions.Stock) && (startBook < DAL.TicketBookStatusDB.GetMaxBookNumber(bookType,0)))
                //{
                //    this.MessageText.Text = "Sorry can't save, You cant place that ticket's in stock, your stock count is already above that range";
                //    return;
                //}

                //if user pick office check if the tickets are already in stock
                if ((this.ActiveGenerateTicketBookStatuses.BookStatus == TicketBookStatusOptions.Office) && (!DAL.TicketBookStatusDB.CheckIfTicketsInStock(startBook, endBook, bookType)))
                {
                    this.MessageText.Text = "Sorry cant save, You dont have that ticket's in stock";
                    return;
                }

                //if user pick driver or sold (to sell to customer walks in to the office) check if he have that tickets
                if (((this.ActiveGenerateTicketBookStatuses.BookStatus == TicketBookStatusOptions.Driver) || (ActiveGenerateTicketBookStatuses.BookStatus == TicketBookStatusOptions.Sold)) &&
                    (!DAL.TicketBookStatusDB.CheckIfCouldGiveAwayTickets(startBook, endBook, bookType, user)))
                {
                    this.MessageText.Text = "Sorry can't save, You dont have this books to give away";
                    return;
                }

                //Is Member discount is selected for sold book, check if is paid member
                if (this.ActiveGenerateTicketBookStatuses.TBDiscountType == BookDiscountTypes.Member)
                {
                    string homePhone = "";
                    string cellPhone = "";
                    if (this.ActiveGenerateTicketBookStatuses.Customer != null)
                    {
                        homePhone = this.ActiveGenerateTicketBookStatuses.Customer.HomePhoneNo;
                        cellPhone = this.ActiveGenerateTicketBookStatuses.Customer.CellPhoneNo;
                    }
                    else
                    {
                        homePhone = this.ActiveGenerateTicketBookStatuses.SoldToInfo;
                    }

                    if (!DAL.TicketBookStatusDB.CheckIfIsPaidMember(homePhone, cellPhone))
                    {
                        this.MessageText.Text = "Sorry You can't assign a member discount, This person is not a paid member";
                        return;
                    }
                }

                if (DAL.TicketBookStatusDB.AddTicketBookStatuses(this.ActiveGenerateTicketBookStatuses.TicketBookStatuses))
                {
                    System.Media.SystemSounds.Asterisk.Play();
                    this.MessageText.Text = "";
                    this.ActiveGenerateTicketBookStatuses.Customer = null;
                    this.ActiveGenerateTicketBookStatuses.SoldToInfo = "";
                    this.ActiveGenerateTicketBookStatuses.TBDiscountType = BookDiscountTypes.None;
                    this.ActiveGenerateTicketBookStatuses.TBNotes = "";
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        private void ButtonTransfer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ActiveGenerateTicketBookStatuses.GenarateBooks();

                int startBook = this.ActiveGenerateTicketBookStatuses.StartBookNumber;
                int endBook = this.ActiveGenerateTicketBookStatuses.EndBookNumber;
                int bookType = this.ActiveGenerateTicketBookStatuses.TicketBookID;
                int user = UIController.CurrentSession.SessionUser.UserID;

                if ((!DAL.TicketBookStatusDB.CheckIfCouldGiveAwayTickets(startBook, endBook, bookType, 0)))
                {
                    this.MessageText.Text = "Sorry can't save, You dont have this books in a transferable state";
                    return;
                }

                if (DAL.TicketBookStatusDB.AddTicketBookStatuses(this.ActiveGenerateTicketBookStatuses.TicketBookStatuses))
                {
                    System.Media.SystemSounds.Asterisk.Play();
                    this.MessageText.Text = "";
                    this.ActiveGenerateTicketBookStatuses.Customer = null;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            UIController.CloseActivePage();
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Log Ticket Books";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void TicketBookIDComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                this.ActiveGenerateTicketBookStatuses.StartBookNumber = DAL.TicketBookStatusDB.GetMaxBookNumber(this.ActiveGenerateTicketBookStatuses.TicketBookID, 0);
                this.ActiveGenerateTicketBookStatuses.OnPropertyChanged("StartBookNumber");
                this.ActiveGenerateTicketBookStatuses.ActiveTicketBook = this.TicketBooks.Where(i => i.TicketBookID == this.ActiveGenerateTicketBookStatuses.TicketBookID).FirstOrDefault();
                this.ActiveGenerateTicketBookStatuses.GeneratedPriceSold = this.ActiveGenerateTicketBookStatuses.ActiveTicketBook.BookPrice;
                this.MessageText.Text = "";
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void txtContactName_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {
                if ((sender as TextBox).Text == "")
                    return;
                ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, (sender as TextBox).Text, DataRequestTypes.LiteData, false);
                //Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

                if (customers.Count == 1)
                {
                    this.ActiveGenerateTicketBookStatuses.Customer = customers[0];
                    this.ActiveGenerateTicketBookStatuses.SoldToInfo = null;
                }
                else if (customers.Count > 1)
                {
                    this.ActiveGenerateTicketBookStatuses.Customer = UIController.SelectCustomerFromList(customers);
                    this.ActiveGenerateTicketBookStatuses.SoldToInfo = null;
                }
                else if (customers.Count == 0)
                {
                    this.ActiveGenerateTicketBookStatuses.SoldToInfo = (sender as TextBox).Text.Trim();
                    this.ActiveGenerateTicketBookStatuses.Customer = null;
                }
                (sender as TextBox).Text = "";
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                var customerToCharge = UIController.SelectCustomerFromList(null, false);
                if (customerToCharge == null) return;
                this.ActiveGenerateTicketBookStatuses.Customer = customerToCharge;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_ChargeCC(object sender, RoutedEventArgs e)
        {
            int pmtid = 0;
            decimal amount2 = 0;
            decimal amount = this.ActiveGenerateTicketBookStatuses.TotalPrice;
            int cstmrID = this.ActiveGenerateTicketBookStatuses.Customer == null ? 0 : this.ActiveGenerateTicketBookStatuses.Customer.CustomerID;
            UIController.ShowChargeCCWindow(new CCOnFile() { CustomerID = cstmrID }, false, ref pmtid, ref amount, ref amount2);
            this.ActiveGenerateTicketBookStatuses.PaymentID = pmtid;
        }


    }
}
