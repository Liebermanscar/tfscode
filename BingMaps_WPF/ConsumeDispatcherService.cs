﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DispatcherService;
using System.ServiceModel;
using BOL;

namespace BingMaps_WPF
{
    public  class ConsumeDispatcherService : IDispatcherCallBack
    {
        static string endpointUrl = Properties.Settings.Default.DispatcherServiceURL;

        static NetTcpBinding binding = new NetTcpBinding();
        static EndpointAddress endpoint = new EndpointAddress(endpointUrl);
        static DuplexChannelFactory<IDispatcher> channelFactory;
        static IDispatcher clientProxy;

        public bool ConnectToDispatcherService()
        {
            try
            {
                channelFactory = new DuplexChannelFactory<IDispatcher>(this, binding, endpoint);
                clientProxy = channelFactory.CreateChannel();

                return clientProxy.DispatcherJoin(new User() { UserID = 123456789, UserName = "MapApp" },2.0);
                
            }
            catch (Exception ex) { return false; }
        }

        public BOL.Trip[] GetAllTrips()
        {
            return clientProxy.GetAllTrips();
        }

        public BOL.DriverAndTripInformation[] GetAllDrivers()
        {
            return clientProxy.GetAllDriversAndTripInfo();
        }
        
        void IDispatcherCallBack.TripAdded(Trip trip, int byUserID)
        {
            MapsController.UpdateTrip( trip, ObjectAction.Add);
        }

        void IDispatcherCallBack.TripEdited(Trip trip)
        {
            //var tripOnMap = new TripOnMap(trip.TripID, trip.FromLocation, 0, 0);
            if (trip.DrivenBy != null)
                MapsController.UpdateTrip(trip, ObjectAction.Remove);
            else
            {
                MapsController.UpdateTrip(trip, ObjectAction.Remove);
                MapsController.UpdateTrip(trip, ObjectAction.Add);
            }
        }

        void IDispatcherCallBack.TripRemoved(Trip trip)
        {
            
            MapsController.UpdateTrip(trip, ObjectAction.Remove);
        }

        void IDispatcherCallBack.DriverAdded(DriverAndTripInformation driver)
        {
            //MapsController.BingMap.
            //MapsController.UpdateCarStatus(new CarOnMap(driver.UnitDriver.DrivesVehicle.VehicleCode,
            //    driver.UnitDriver.DriverCode,
            //    driver.DriverStatus.ToDescription(), 
            //    driver.DriversLocation));
        }

        void IDispatcherCallBack.DriverEdited(DriverAndTripInformation driver)
        {
            if (driver.DriverStatus == DriverStatusOptions.NintyEight)
                MapsController.UpdateCarStatus(driver, ObjectAction.Add);
            
        }

        void IDispatcherCallBack.DriverRemoved(DriverAndTripInformation driver)
        {
            //var carOnMap = new CarOnMap(driver.UnitDriver.DriverID, driver.UnitDriver.DriverCode, 0,0,);
            MapsController.UpdateCarStatus(driver, ObjectAction.Remove);
        }

        AssignedUnitDiscrepencyOption IDispatcherCallBack.GetAssignedUnitOptions()
        {
            throw new NotImplementedException();
        }

        void IDispatcherCallBack.TrisAndDriversWereReset(bool isLongDistance)
        {
            throw new NotImplementedException();
        }

        void IDispatcherCallBack.DriversAndTripInfoReset(DriverAndTripInformation[] driverAndTripsArray)
        {
            throw new NotImplementedException();
        }

        public void SectionCarAvailabilityChanged(SectionCarsAvailability sectionChanged)
        {
            throw new NotImplementedException();
        }

        public void TripLocationAdded(Trip trip)
        {
            throw new NotImplementedException();
        }
    }

    


}
