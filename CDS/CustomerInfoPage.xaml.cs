﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;


namespace CDS
{
    /// <summary>
    /// Interaction logic for CustomerInfoPage.xaml
    /// </summary>
    public partial class CustomerInfoPage : Page, IDisplayTextBlock
    {
        public CustomerInfoPage()
        {
            InitializeComponent();

            this.CustomerTypeComboBox.ItemsSource = Enum.GetValues(typeof(CustomerTypes));

            this.customerSearchCtrl.Owner = this;
        }

        public CustomerInfoPage(Customer customer) : this()
        {
            this.ActiveCustomer = customer;
        }

        public void AddCustomerBalanceInfo()
        {
            if ((ActiveCustomer == null) || (ActiveCustomer.CustomerID == 0))
                return;
            decimal b;
            decimal.TryParse(DAL.CustomerDB.GetSumNotInvoiced(ActiveCustomer.CustomerID, true), out b);
            this.ActiveCustomer.OpenBalanceDue = DAL.CustomerDB.GetCustomerOpenBalance(ActiveCustomer.CustomerID, false)[0].OpenBalanceDue;
            this.ActiveCustomer.BalanceNotInvoiced = b;
        }

        public Customer ActiveCustomer
        {
            get { return this.DataContext as Customer; }
            set { this.DataContext = value; AddCustomerBalanceInfo(); }
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                if (this.ActiveCustomer != null)
                    return "Customer Information Page: " + this.ActiveCustomer.CustomerName;
                else
                    return "Customer Information Page: None";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "You're about to close \n\n" + this.HeaderText + "\n\nContinue?";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void btnAddAH_Click(object sender, RoutedEventArgs e)
        {
            this.NewCustomer();
        }

        public void NewCustomer()
        {
            try
            {
                this.ActiveCustomer = Customer.NewCustomer();
                this.ActiveCustomer.CustomerType = CustomerTypes.Person;
                this.customerSearchCtrl.DataContext = null;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }




        private void btnSaveAH_Click(object sender, RoutedEventArgs e)
        {
            SaveCustomerInfo();
        }
        private void SaveCustomerInfo()
        {



            try
            {
                if (MessageBox.Show(string.Format("You're about to make changes to the customer \n\n {0} \n\nContinue?", this.ActiveCustomer.CustomerName), "Save Changes", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
                PassCodeSetUp();
                if (this.ActiveCustomer.ObjectState == BOState.New)
                {

                    if (DAL.CustomerDB.AddCustomer(this.ActiveCustomer))
                    {
                        this.ActiveCustomer.ObjectState = BOState.Saved;
                        this.customerSearchCtrl.DataContext = this.ActiveCustomer;
                        MessageBox.Show(string.Format("{0} was succesfully saved", this.ActiveCustomer.CustomerName), "Saved", MessageBoxButton.OK);
                    }
                }
                else
                {
                    if (DAL.CustomerDB.EditCustomer(this.ActiveCustomer))
                    {
                        this.customerSearchCtrl.DataContext = this.ActiveCustomer;
                        MessageBox.Show(string.Format("{0} was succesfully saved", this.ActiveCustomer.CustomerName), "Saved", MessageBoxButton.OK);
                    }
                }


            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void PassCodeSetUp()
        {
            /* try
             {
                 if (this.ActiveCustomer==null)
                 return;

                 if (this.ActiveCustomer.NeedsPassCode != true)
                 return;

               //  var pc = new CustomerPassCode();
                 var pc = DAL.CustomerPassCodeDB.GetCustomerPassCode(this.ActiveCustomer.CustomerID);
                 if (pc.Count> 0)
                     if (MessageBox.Show("This Customer Has already Pass Codes Set up  \n\n  \n\n Do you wish to add Another PassCode ?", this.ActiveCustomer.CustomerName, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                         return; 
                     string userName = "";
                     string passCode = "";
                     var results1 = UIController.GetInput("User Name", "Enter user's Name:", out userName);
                     var results2 = UIController.GetInput("Pass Code", "Enter user's Pass Code:", out passCode);

                     var cpc = new CustomerPassCode
                     {
                         PassUserName = userName,
                         PassUserCode = passCode,
                         ForCustomerID = this.ActiveCustomer.CustomerID
                     };
                     DAL.CustomerPassCodeDB.AddCustomerPassCode(cpc);

                 }



             catch (Exception ex)
             {

                 UIController.DisplayErrorMessage(ex);
             }

         */

        }
        private void customerSearchCtrl_OnCustomerSelected(object sender, CustomerSearchEventArgs e)
        {
            if (e.CustomerSelected != null)
                this.ActiveCustomer = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID, e.CustomerSelected.CustomerID.ToString(), DataRequestTypes.FullData, false).FirstOrDefault();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return && !string.IsNullOrWhiteSpace(this.StreetNameTextBox.Text))
                {
                    int spaceIndex = this.ActiveCustomer.StreetAddress.IndexOf(" ");
                    var abbreviations = DAL.StreetAbbreviationDB.GetStreetAbbreviations(0, this.ActiveCustomer.StreetName.Substring(spaceIndex < 0 ? 0 : spaceIndex).Trim());
                    if (abbreviations.Count > 0)
                    {
                        this.ActiveCustomer.StreetName = abbreviations[0].StreetName;
                        this.ActiveCustomer.City = abbreviations[0].City;
                        this.ActiveCustomer.State = abbreviations[0].State;
                        this.ActiveCustomer.PostalCode = abbreviations[0].PostalCode;
                    }

                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void CustomersTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (this.ActiveCustomer.CustomerID == 0)
                { //CustomersTabControl.IsEnabled = false; 
                    return;
                }

                else
                {
                    try
                    {
                        if (this.CustomersTabControl.SelectedItem == this.HomeTabItem)
                            this.AddCustomerBalanceInfo();
                        else if (this.CustomersTabControl.SelectedItem == this.TripsTabItem)
                            this.ActiveCustomer.CustomerTrips = DAL.TripDB.GetTrips(this.ActiveCustomer.CustomerID, 0, 0, 0, null, null, false, null, 0, 0);
                        else if (this.CustomersTabControl.SelectedItem == this.InvoicesTabItem)
                            this.ActiveCustomer.CustomerInvoices = DAL.InvoiceDB.GetInvoices(0, this.ActiveCustomer.CustomerID, 0);
                        else if (this.CustomersTabControl.SelectedItem == this.CustomerPaymentTab)
                            this.ActiveCustomer.Payments = DAL.CustomerPaymentInfoDB.GetPayments(this.ActiveCustomer.CustomerID, 0);// = DAL.InvoiceDB.GetInvoices(0, this.ActiveCustomer.CustomerID, 0);   
                        else if (this.CustomersTabControl.SelectedItem == this.CustomerCreditCardsTab)
                        {
                            this.ActiveCustomer.CCOnFile = DAL.CustomerCreditCardInfoDB.GetCards(this.ActiveCustomer.CustomerID, 0);//Payments = DAL.CustomerPaymentInfoDB.GetPayments(this.ActiveCustomer.CustomerID, 0);// = DAL.InvoiceDB.GetInvoices(0, this.ActiveCustomer.CustomerID, 0);   
                            this.AddCustomerBalanceInfo();
                        }
                        else if (this.CustomersTabControl.SelectedItem == this.CustomerPassCodesTab)
                            this.ActiveCustomer.CustPassCodes = DAL.CustomerPassCodeDB.GetCustomerPassCode(this.ActiveCustomer.CustomerID);//(this.ActiveCustomer.CustomerID, 0);//Payments = DAL.CustomerPaymentInfoDB.GetPayments(this.ActiveCustomer.CustomerID, 0);// = DAL.InvoiceDB.GetInvoices(0, this.ActiveCustomer.CustomerID, 0);   
                        else if (this.CustomersTabControl.SelectedItem == this.CustomerTicketBooksTab)
                            this.ActiveCustomer.CustTicketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatuses(customerID: this.ActiveCustomer.CustomerID);
                    }

                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                    }
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(this.TripsDataGrid.SelectedIndex.ToString());
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.SelectedInvoice == null)
                return;
            this.SelectedInvoice.TripsInvoiced = DAL.TripDB.GetTrips(0, 0, 0, this.SelectedInvoice.InvoiceID, null, null, false, null, 0, 0);
            UIController.PrintInvoice(new List<Invoice>() { this.SelectedInvoice });
        }

        Invoice SelectedInvoice
        {
            get
            {
                return this.InvoiceDataGrid.SelectedItem as Invoice;
            }
        }

        private void CustomerInsuranceInfoDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                var custIns = e.Row.DataContext as CustomerInsuranceInfo;
                if (!custIns.AllowSave)
                    return;
                custIns.CustomerID = this.ActiveCustomer.CustomerID;
                if (custIns.CustomerInsuranceInfoID == 0)
                    e.Cancel = !DAL.CustomerInsuranceInfoDB.AddCustomerInsuranceInfo(custIns);
                else
                    e.Cancel = !DAL.CustomerInsuranceInfoDB.EditCustomerInsuranceInfo(custIns);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        /*        private void CustomerPaymentDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
                {
                    if (e.EditAction == DataGridEditAction.Cancel)
                        return;

                    e.Cancel = true;

                    try
                    {
                        var custPayment = e.Row.DataContext as Payment;

                        if (custPayment.PaymentID == 0)
                        {
                            custPayment.IsCreditable = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);

                    }
                }*/

        private void CustomerPaymentInfoDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Cancel)
                return;

            e.Cancel = true;

            try
            {
                var custPayment = e.Row.DataContext as Payment;
                if (!custPayment.AllowSave)
                    return;
                custPayment.Customer = this.ActiveCustomer;
                if (custPayment.PaymentID == 0)
                {
                    //  e.Cancel=null;
                    //   e.Cancel = !DAL.CustomerInsuranceInfoDB.AddCustomerInsuranceInfo(custIns);
                    custPayment.IsCreditable = true;
                    e.Cancel = !DAL.CustomerPaymentInfoDB.AddPayment(custPayment);
                }
                else
                    e.Cancel = !DAL.CustomerPaymentInfoDB.EditCustomerPaymentsInfo(custPayment);//EditCustomerInsuranceInfo(custIns);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        private void CustomerCreditCardGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Cancel)
                return;

            e.Cancel = true;

            try
            {
                var custCreditCard = e.Row.DataContext as CCOnFile;

                if (!custCreditCard.AllowSave)
                    return;
                custCreditCard.CustomerID = this.ActiveCustomer.CustomerID;
                if (custCreditCard.CcID == 0)

                    e.Cancel = !DAL.CustomerCreditCardInfoDB.AddCreditCard(custCreditCard);
                else
                    e.Cancel = !DAL.CustomerCreditCardInfoDB.EditCreditCardInfo(custCreditCard);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }

        private void MenuItemChargeCard_Click(object sender, RoutedEventArgs e)
        {
            var ccOnFIle = this.CustomerCreditCardGrid.SelectedItem as CCOnFile;

            if (ccOnFIle.CustomerID == 0)
                ccOnFIle.CustomerID = this.ActiveCustomer.CustomerID;

            if (ccOnFIle.CcID == 0 && !DAL.CustomerCreditCardInfoDB.AddCreditCard(ccOnFIle))
                return;



            if (ccOnFIle == null)
                return;
            if (ccOnFIle != null)
            {
                int pmtid = 0;
                decimal amountForBook = 0;
                decimal amountForTrip = 0;
                UIController.ShowChargeCCWindow(ccOnFIle, ActiveCustomer.IsInvoiceCustomer, ref pmtid, ref amountForBook, ref amountForTrip);
            }


        }

        private void checkBoxInvoiceCustomer_CheckedChanged(object sender, RoutedEventArgs e)
        {
            isInvoiceCustomer();
        }
        public bool isInvoiceCustomer()
        {
            bool invoiceCustomer;
            if (this.checkBoxIsInvoiceCustomer.IsChecked.Value)
            {
                invoiceCustomer = true;

            }
            else invoiceCustomer = false;
            return invoiceCustomer;

        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                    return;

                switch (e.Key)
                {

                    case Key.F1:
                        SaveCustomerInfo();
                        e.Handled = true;
                        break;
                    case Key.F2:
                        UIController.CloseActivePage();
                        e.Handled = true;
                        break;

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void CustomerPassCodesGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Cancel)
                return;

            e.Cancel = true;

            try
            {
                var custPassCodes = e.Row.DataContext as CustomerPassCode;

                if (!custPassCodes.AllowSave)
                    return;
                custPassCodes.ForCustomerID = this.ActiveCustomer.CustomerID;
                if (custPassCodes.CustomerPassCodeID == 0)

                    e.Cancel = !DAL.CustomerPassCodeDB.AddCustomerPassCode(custPassCodes);
                else
                    e.Cancel = !DAL.CustomerPassCodeDB.EditCustomerPassCode(custPassCodes);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }


    }
}
