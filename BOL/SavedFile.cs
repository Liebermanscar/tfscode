﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class SavedFile : BO
    {
        public int FileID { get; set; }
        public string FileName { get; set; }
        public int DailyEnvelopeID { get; set; }
        public bool IsMarkedForDeletion { get; set; }
        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }
}
