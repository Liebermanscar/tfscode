﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AddEditFidelisTripWindow.xaml
    /// </summary>
    public partial class AddEditFidelisTripWindow : Window
    {
        public AddEditFidelisTripWindow()
        {
            InitializeComponent();
        }

        public AddEditFidelisTripWindow(FidelisTrip fidelisTrip) : this()
        {
            this.ActiveFidelisTrip = fidelisTrip;
        }

        public FidelisTrip ActiveFidelisTrip
        {
            get { return this.DataContext as FidelisTrip; }
            set { this.DataContext = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
