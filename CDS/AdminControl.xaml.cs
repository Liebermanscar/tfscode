﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AdminControl.xaml
    /// </summary>
    public partial class AdminControl : UserControl
    {
        public AdminControl()
        {
            InitializeComponent();
            try
            {

                LoadData();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void LoadData()
        {
            try
            {


                SumOfTripsForDay = DAL.TripDB.GetTrips(0, 0, 0, 0, DateTime.Today, DateTime.Today, false, null, 0, 0, false, 0, false, 0, 0).ToList();
                int LocalCount = SumOfTripsForDay.Count(t => t.TripAreaType == TripAreaTypes.Local);
                int LongDistanceCount = SumOfTripsForDay.Count(t => t.TripAreaType == TripAreaTypes.LongDistance);
                this.txtSumLocalToday.Text = LocalCount.ToString();
                this.txtSumLongDistanceToday.Text = LongDistanceCount.ToString();


            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        List<Trip> SumOfTripsForDay = new List<Trip>();




        /*  ObservableCollection<Trip> sumOfLongDistanceTripsForDay;

          public ObservableCollection<Trip> SumOfLongDistanceTripsForDay
          {
              get { return sumOfLongDistanceTripsForDay; }
              set { sumOfLongDistanceTripsForDay = value; }
          }
          */
        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            LoadData();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }



    }
}
