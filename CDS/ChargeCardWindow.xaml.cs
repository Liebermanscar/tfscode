﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace CDS
{
    /// <summary>
    /// Interaction logic for ChargeCardWindow.xaml
    /// </summary>
    public partial class ChargeCardWindow : Window
    {

        public ChargeCardWindow()
        {
            InitializeComponent();
        }

        public ChargeCardWindow(BOL.CCOnFile ccOnfile) : this()
        {
            // TODO: Complete member initialization
            this.ActiveCCOnFile = ccOnfile;
        }

        public BOL.CCOnFile ActiveCCOnFile
        {
            get
            {
                return this.DataContext as BOL.CCOnFile;
            }

            set
            {
                this.DataContext = value;
            }
        }

        private void btnCghCard_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.cardHoldersName_txtBox.Focus();
        }
    }
}
