using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
	[Activity(Label = "Login", ScreenOrientation=Android.Content.PM.ScreenOrientation.Landscape)]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
			SetContentView(Resource.Layout.Login);

			EditText userName = FindViewById<EditText> (Resource.Id.UserName);
			EditText password = FindViewById<EditText> (Resource.Id.Password);

			Button loginBtn = FindViewById<Button> (Resource.Id.Login);
			Button cancelBtn = FindViewById<Button> (Resource.Id.Cancel);

			userName.Text = UIController.GetUserName ();

			if (!string.IsNullOrWhiteSpace (userName.Text))
				userName.RequestFocus (FocusSearchDirection.Forward);

            password.EditorAction += (object sender, TextView.EditorActionEventArgs e) =>
			{
				if (e.ActionId == Android.Views.InputMethods.ImeAction.Done)
					loginBtn.PerformClick();				
			};


			loginBtn.Click += (object sender, EventArgs e) => { 
				if (UIController.Modal.IsAuthenticated)
					UIController.Logout();
				
				if (UIController.Login(userName.Text, password.Text))
				{
					SetResult(Result.Ok);
					Finish();

                    UIController.ltdServiceBinder.GetListenToDispatchingServiceBinder().SignInDriver();

                    UIController.GetDriverInfo();

                    UIController.DisplayDriverInfoActivity();
					//StartActivity(typeof(MainActivity));
				}
				else
					UIController.ShowAlrtDialog(this, "Credentials NOT Authenitcated", "The User Name and Password that you provided could not be Authenitcated\nPlease try again");
				};

			cancelBtn.Click += (object sender, EventArgs e) => {
				
				SetResult(Result.Canceled);
				Finish();
			};
        }


    }
}