﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for CompanyFinancialControl.xaml
    /// </summary>
    public partial class CompanyFinancialControl : UserControl
    {
        public CompanyFinancialControl()
        {

            InitializeComponent();
            try
            {

                LoadData();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        void LoadData()
        {
            try
            {
                var sum = DAL.AccountTransactionDB.TotalOfOpenInvoices();
                this.txtOpenInvoices.Text = sum.ToString();

                var sumNotInvoiced = DAL.CustomerDB.GetSumNotInvoiced(0, true);
                this.txtNotInvoiced.Text = sumNotInvoiced.ToString();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            LoadData();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCustomerOpenBalancePage();
        }

        private void TextBlockTotalNotInvoiced_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }


        //  ObservableCollection<
    }
}
