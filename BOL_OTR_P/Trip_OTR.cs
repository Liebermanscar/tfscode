﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BOL_OTR
{
    public class Trip_OTR
    {        
        public Trip_OTR()
        {
            TripLogs = new List<TripLog>();
        }

        public int TripID { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public int CustomerID { get; set; }
        public string CustomerHomePhoneNo { get; set; }
        public string CustomerCellPhoneNo { get; set; }
		public string CustomerName {get; set;}
        public int DriverID { get; set; }
        public int AssignedByDispatcherUserID { get; set; }
		public DateTime StartWaitingTime { get; set; }
		public DateTime StopWaitingTime { get; set; }
        public int TotalStops { get; set; }
        public decimal Price { get; set; }
        public int StopsDone { get; set; }
        public bool HaveStopsToDo
        {
            get { return TotalStops > StopsDone; }
        }
        public decimal TotalWaitingSeconds { get; set; }
		public TripDetailedStatusOptions TripDetailedStatus { get; set; } 
		public DateTime TripDetailedSetOn { get; set; }

        public GeoCoordinate FromCoordinate { get; set; }
        public GeoCoordinate ToCoordinate { get; set; }

        public decimal CashPaid { get; set; }
        public decimal CheckPaid { get; set; }
        public decimal CCPaid { get; set; }

        public List<TripLog> TripLogs { get; set; }
    }

    public class TicketPaid_OTR
    {
        public int TicketID { get; set; }
        public string TicketName { get; set; }
        public decimal TicketValue { get; set; }
        public int NoOfTicketsReceived { get; set; }
    }

    [DataContract]
    public enum DriversRequestOptions
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        ShortBreak = 101,
        [EnumMember]
        LongBreak = 102
    }

    public class TripLog
    {
        public DateTime LogTime { get; set; }
        public TripDetailedStatusOptions TripStatus { get; set; }
    }

    public enum PaymentMethodTypes
    {
        None = 0,
        Cash = 101,
        Check = 201,
        CC = 301,
        CustomerAccount = 401,
        DriverAccount = 501,
        Ticket = 601,
        NotPaid = 701,
        Other = 1001
    }
}
