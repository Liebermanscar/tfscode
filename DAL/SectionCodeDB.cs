﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
namespace DAL
{
    class SectionCodeDB : Database
    {
        public SectionCodeDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public bool Add(SectionCode sectionCode)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@SectionID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddSections", new SqlParameter[]
            {
               new SqlParameter("@SectionName", sectionCode.SectionName),


                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                sectionCode.SectionID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }
        public bool Edit(SectionCode sectionCode)
        {
            return true;
        }
        public ObservableCollection<SectionCode> Get(int sectionID)
        {
            ObservableCollection<SectionCode> sectionsCodes = new ObservableCollection<SectionCode>();






            return sectionsCodes;

        }

        public static bool AddSectionCode(SectionCode sectionCode)
        {
            using (SectionCodeDB sectionCodeDB = new SectionCodeDB(null, null))
            {
                return sectionCodeDB.Add(sectionCode);
            }

        }
        public static bool EditSectionCode(SectionCode sectionCode)
        {
            using (SectionCodeDB sectionDB = new SectionCodeDB(null, null))
            {
                return sectionDB.Edit(sectionCode);
            }

        }
        public static ObservableCollection<SectionCode> GetSectionCodes(int sectionID)
        {
            using (SectionCodeDB sectionCode = new SectionCodeDB(null, null))

            {
                return SectionCodeDB.GetSectionCodes(sectionID);
            }

        }

    }
}
