using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
    [Activity(Label = "AddStopsActivity")]
    public class AddStopsActivity : Activity
    {
        NumberPicker np;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AddStops);

            TextView tvMsg = FindViewById<TextView>(Resource.Id.tvNumberPickerMsg);
            np = FindViewById<NumberPicker>(Resource.Id.numberPickerStops);

            np.MinValue = 0;
            np.MaxValue = 10;
            np.WrapSelectorWheel = true;

            string message = Intent.GetStringExtra("messageText");
            string presetNumber = Intent.GetStringExtra("presetNumber");

            if (message != null)
                tvMsg.Text = message;

            int pickerVal;
            if (presetNumber != null && int.TryParse(presetNumber, out pickerVal))
            {
                np.Value = pickerVal;
            }  
            else
                np.Value = 1;

            //string[] nums = new string[21];

            //for (int i = 0; i < nums.Length; i++)
            //{
            //    nums[i] = i.ToString();
            //}
            //np.SetDisplayedValues(nums);

            Button btnOk = FindViewById<Button>(Resource.Id.btnNumberOfStopsOK);
            btnOk.Click += delegate {
                Intent resultIntent = new Intent(this, typeof(ActiveTripActivity));
                int noOfStops = np.Value;
                resultIntent.PutExtra("NumberOfStops", noOfStops.ToString());
                SetResult(Result.Ok, resultIntent);
                Finish();
            };

            Button btnCancel = FindViewById<Button>(Resource.Id.btnNumberOfStopsCancel);
            btnCancel.Click += delegate
            {
                SetResult(Result.Canceled);
                Finish();
            };
        }

    }
}