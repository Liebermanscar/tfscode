using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.AspNet.SignalR.Client;
using Android.Locations;
using Android.Support.V4.Content;
using System.Threading.Tasks;

namespace OTR_App
{
    [Service]
	public class ListenToDispatchingService : Service, ILocationListener
    {
		 string _dispatchingHubUrl = "http://192.168.1.11:37999/signalr/";
         HubConnection _hubConnection;
         IHubProxy _dispatchingHubProxy;

        LocationManager locMgr;

		//communicate with main activity
		Intent broadcastToMainUIIntent = new Intent(MainActivity.UpdateUIBroadcastReceiver.UPDATE_MAIN_UI);
        Intent dispatchResponsIntent = new Intent(DispatcherResponsBroadcastReceiver.Active_Trip_Listener);

        public override void OnCreate()
		{
			base.OnCreate();  

			var tabletName = UIController.GetTabletName ();

			//if (string.IsNullOrWhiteSpace (tabletName)) {
			//	StartActivity (typeof(InputBoxActivity));
			//}

			var contentTitle = (string.IsNullOrWhiteSpace(tabletName)) ? "CDS OTR Service is running" : "CDS OTR Service is running on " + tabletName;

            var pendingIntent = PendingIntent.GetActivity(this, 0, new Intent(this, typeof(MainActivity)), 0);
            var noti = new Notification.Builder(this)
                .SetContentIntent(pendingIntent)
                .SetContentTitle("CDS OTR Service")
				.SetContentText(contentTitle);           
			
            StartForeground((int)NotificationFlags.ForegroundService, noti.Build());

            StartSignalRDispatchingHubClient();


        }

		public override void OnDestroy ()
		{
			UIController.StartListenToDispatchingService();

		}
        public override IBinder OnBind(Intent intent)
        {
			return new ListenToDispatchingServiceBinder(this);
        }

		async void StartSignalRDispatchingHubClient()
		{
			var querystringData = new Dictionary<string, string>();
			querystringData.Add("clientType", "tablet");
			querystringData.Add("tabletName", UIController.GetTabletName());

			_hubConnection = new HubConnection(_dispatchingHubUrl, querystringData);
			//_hubConnection.Headers.Add("Authorization", 

			_dispatchingHubProxy = _hubConnection.CreateHubProxy("DispatchingHub");

			await _hubConnection.Start();
            
			Criteria locationCriteria = new Criteria();

			locationCriteria.Accuracy = Accuracy.Fine;
			locationCriteria.PowerRequirement = Power.High;

            locMgr = Application.Context.GetSystemService(LocationService) as LocationManager;

			var provider = LocationManager.NetworkProvider;

			//provider = locMgr.GetBestProvider(locationCriteria, true);
			//if (locMgr.IsProviderEnabled (provider)) {				
				//locMgr.RequestLocationUpdates (provider, 5000, 0, this);
				locMgr.RequestLocationUpdates (provider, 5000, 0, this);
				//locMgr.RequestSingleUpdate (provider, this, null);
				var loc = locMgr.GetLastKnownLocation (provider);
            //}


            #region Calls from hub to client
            // Invoke the 'UpdateNick' method on the server
            //	await dispatchingHubProxy.Invoke("Join", "JohnDoe", "r","v");

            _dispatchingHubProxy.On<BOL_OTR.Trip_OTR>("AssignTrip", async (newTrip) => {

                UIController.ShowNewTripActivity(newTrip);

                var ticketTypes = await UIController.SignalRCalls.SignalR.GetTicketTypes();

                if (ticketTypes != null)
                {
                    UIController.TicketTypes = ticketTypes;
                }
            });

            _dispatchingHubProxy.On<bool>("PermissionRequestResponse", (isGranted) => {

                Intent i = new Intent();
                i.SetAction("Permission" + (isGranted ? "Granted" : "Revoked"));

                SendBroadcast(i);

                //dispatchResponsIntent.PutExtra("isGranted", isGranted);
                
                //LocalBroadcastManager.GetInstance(Application.Context).SendBroadcast(dispatchResponsIntent);
            });
            #endregion Calls from hub to client
        }

        #region Calls from client to hub

        public async Task<BOL_OTR.TicketPaid_OTR[]> GetTicketTypes()
        {
            try
            {
                if (_hubConnection.State == ConnectionState.Connected)
                {
                    var tickets = await _dispatchingHubProxy.Invoke<BOL_OTR.TicketPaid_OTR[]>("GetTicketTypes");
                    return tickets;
                }
            }
            catch (Exception)
            {

                //throw;
            }

            return null;
        }

        public void SignInDriver()
        {
            if (_hubConnection.State == ConnectionState.Connected)
                _dispatchingHubProxy.Invoke("SignInDriver", UIController.GetDriverIDint());
        }

        public void RemoveDriver()
        {
            if (_hubConnection.State == ConnectionState.Connected)
                _dispatchingHubProxy.Invoke("RemoveDriver", UIController.GetDriverIDint());
        }

        public void NotifySignalRDriverStatus(BOL_OTR.DriverStatusOptions status)
        {
            if (_hubConnection.State == ConnectionState.Connected)
                _dispatchingHubProxy.Invoke("DriverStatusChanged", UIController.GetDriverIDint(), status);
        }

		public void UpdateSignalRServiceWithTripSatus(BOL_OTR.TripDetailedStatusOptions status)
		{
			if (_hubConnection.State == ConnectionState.Connected)
				_dispatchingHubProxy.Invoke("TripStatusChanged", UIController.ActiveTrip.TripID, status);
		}

		public void ResponseToTripAssignment(bool accept)
		{
			if (_hubConnection.State == ConnectionState.Connected)
				_dispatchingHubProxy.Invoke("DriverResponseToTripAssignment", UIController.GetDriverIDint(), UIController.NewTrip.TripID, accept);
		}

        public void RequestPermissionFromDispatcher()
        {
            if (_hubConnection.State == ConnectionState.Connected)
                _dispatchingHubProxy.Invoke("DriverRequestChangeDestinationPermission", UIController.GetDriverIDint(), UIController.ActiveTrip.TripID);
        }

        public void SaveTripDetails()
        {
            if (_hubConnection.State == ConnectionState.Connected)
                _dispatchingHubProxy.Invoke("SaveTripDetails", UIController.ActiveTrip);
        }
        #endregion Calls from client to hub

        #region location interface implementation
        public async void OnLocationChanged (Location location)
		{
			if (_hubConnection.State == ConnectionState.Connected)
				await _dispatchingHubProxy.Invoke("TabletReportedLocation", location.Latitude, location.Longitude, location.Speed);

//			using (var h = new Handler( Looper.MainLooper)	)			
//				h.Post (() => {
//					Toast.MakeText(this, "BOO", ToastLength.Long);
//				});

			LocalBroadcastManager.GetInstance(Application.Context).SendBroadcast(broadcastToMainUIIntent);

			
		}

		public void OnProviderDisabled (string provider)
		{
			throw new NotImplementedException ();
		}

		public void OnProviderEnabled (string provider)
		{
			throw new NotImplementedException ();
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{
			throw new NotImplementedException ();
		}
		#endregion
    }

	public class ListenToDispatchingServiceBinder : Binder
	{
		ListenToDispatchingService service;

		public ListenToDispatchingServiceBinder (ListenToDispatchingService service)
		{
			this.service = service;
		}

		public ListenToDispatchingService GetListenToDispatchingServiceBinder()
		{
			return service;
		}
	}

	class ListenToDispatchingServiceConnection : Java.Lang.Object, IServiceConnection
	{
		//UIController ui_controller;

		public ListenToDispatchingServiceConnection (object uicontroller)
		{
			//this.ui_controller = uicontroller;
		}

		public void OnServiceConnected (ComponentName name, IBinder service)
		{
			var ltdServiceBinder = service as ListenToDispatchingServiceBinder;
			if (ltdServiceBinder != null) {
				UIController.ltdServiceBinder = ltdServiceBinder;
				UIController.isBound = true;
			}
		}

		public void OnServiceDisconnected (ComponentName name)
		{
			UIController.isBound = false;
		}
	}
}