﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for PaymentDetailsWindow.xaml
    /// </summary>
    public partial class PaymentDetailsWindow : Window
    {
        public PaymentDetailsWindow(Trip trip)
        {
            InitializeComponent();

            try
            {
                ObservableCollection<TicketBook> bookList = DAL.TicketBookDB.GetTicketBooks();

                int unicode = 65;
                foreach (TicketBook tb in bookList)
                {
                    char character = (char)unicode;
                    tb.BookNameID = character.ToString() + "-" + tb.BookName;
                    unicode++;
                }
                bookList.Insert(0, new TicketBook() { TicketBookID = 0, BookName = "None", BookNameID = "N-None" });
                this.cboTicketTypes.ItemsSource = bookList;

                this.TripsGrid.ItemsSource = new ObservableCollection<Trip>() { trip };
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }


        }

        Trip SelectedTrip
        {
            get { return this.TripsGrid.SelectedItem as Trip; }
        }
        private void txtContactName_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {


                if ((sender as TextBox).Text == "")
                    return;
                ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, (sender as TextBox).Text, DataRequestTypes.LiteData, true);
                //Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

                if (customers.Count == 1)
                    this.SelectedTrip.ChargeToCustomer = customers[0];
                else if (customers.Count > 1)
                    this.SelectedTrip.ChargeToCustomer = UIController.SelectCustomerFromList(customers);

                /*  DataGridRow dgrow = (DataGridRow)this.TripsGrid.ItemContainerGenerator.ContainerFromItem(TripsGrid.Items[TripsGrid.SelectedIndex]);
                  dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));*/

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                var customerToCharge = UIController.SelectCustomerFromList(null);
                if (customerToCharge == null) return;
                this.SelectedTrip.ChargeToCustomer = customerToCharge;
                //if (this.SelectedTrip.ChargeToCustomer == null)
                //    this.SelectedTrip.AttachedScheduledTrip = null;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void ButtonDriver_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var drivers = DAL.DriversDB.GetDrivers(false, 0);
                string gg;
                var driverSelected = UIController.SelectDriver(drivers, "Manual entry on process daily envelope", out gg, false);
                if (driverSelected == null)
                    return;

                if (!driverSelected.AllowsChargeToHisAccount)
                {
                    MessageBox.Show(string.Format("Unit No {0} does NOT allow charges to his account", driverSelected.DriverCode), "Charges NOT Allowed", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                    return;
                }
                this.SelectedTrip.ChargeToDriver = driverSelected; //SelectCustomerFromList(null);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        void cbo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0 || this.SelectedTrip == null)
                return;
            this.SelectedTrip.TicketBook = (TicketBook)e.AddedItems[0];
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }


        public static bool EnterPaymentDetails(Trip trip)
        {
            PaymentDetailsWindow wind = new PaymentDetailsWindow(trip);
            wind.ShowDialog();
            return wind.DialogResult.GetValueOrDefault();
        }
    }
}
