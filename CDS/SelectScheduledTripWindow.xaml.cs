﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SelectFidelisTripWindow.xaml
    /// </summary>
    public partial class SelectSceduledTripWindow : Window
    {
        public SelectSceduledTripWindow()
        {
            InitializeComponent();
        }

        public SelectSceduledTripWindow(ObservableCollection<ScheduledTrip> scheduledTrips) : this()
        {
            // TODO: Complete member initialization
            this.fullScheduledList = scheduledTrips;
            this.ScheduledTrips = scheduledTrips;
            this.TransTimeColumn.Visibility = System.Windows.Visibility.Collapsed;
        }

        public ObservableCollection<ScheduledTrip> fullScheduledList = new ObservableCollection<ScheduledTrip>();

        public ObservableCollection<ScheduledTrip> ScheduledTrips
        {
            get { return this.DataContext as ObservableCollection<ScheduledTrip>; }
            set { this.DataContext = value; }
        }

        public ScheduledTrip SelectedScheduledTrip
        {
            get
            { return this.ScheduledTripsDataGrid.SelectedItem as ScheduledTrip; }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.SearchTextBox.Text))
                this.ScheduledTrips = this.fullScheduledList;
            else
                this.ScheduledTrips = new ObservableCollection<ScheduledTrip>(this.fullScheduledList.Where(ft => ft.CustomerPhoneNo.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())).ToList());
        }

        private void FidelisTrpsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = true;
        }



        private void Button_Import_Medicaid_From_web_Click(object sender, RoutedEventArgs e)
        {
            UIController.ImportMedicaidTrips(true);
            this.ScheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, DateTime.Today, null, null, true, TripAreaTypes.All);
            //  ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(DateTime.Today);
        }

        private void Button_Import_Medicaid_From_File_Click(object sender, RoutedEventArgs e)
        {
            UIController.ImportMedicaidTrips(false);
            this.ScheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, DateTime.Today, null, null, true, TripAreaTypes.All);


            //  ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(DateTime.Today);
        }

        private void Button_Add_Medicaid_Trip_Click(object sender, RoutedEventArgs e)
        {
            UIController.ShowAddEditMedicaidTripWindow(new MedicaidTrip());
            this.ScheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, DateTime.Today, null, null, true, TripAreaTypes.All);
            // ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(DateTime.Today);
        }


    }
}
