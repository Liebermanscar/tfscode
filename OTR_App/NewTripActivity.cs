﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
	[Activity (Label = "NewTrip")]			
	public class NewTripActivity : Activity
	{
		TextView tvPickUp;
		TextView tvDropOff;
		Button btnAccept;
		Button btnReject;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView(Resource.Layout.NewTrip);

			tvPickUp = FindViewById<TextView>(Resource.Id.tvPickUp);
			tvDropOff = FindViewById<TextView>(Resource.Id.tvDropOff);

			tvPickUp.Text = UIController.NewTrip.FromLocation;

			btnAccept = FindViewById<Button> (Resource.Id.btnAccept);
			btnReject = FindViewById<Button> (Resource.Id.btnReject);
                
			btnAccept.Click += (object sender, EventArgs e) => {
				UIController.SignalRCalls.NewTripStatus(true);
                //tvDropOff.Text = UIController.NewTrip.ToLocation;
                Finish();
			};

			btnReject.Click += (object sender, EventArgs e) => {
				UIController.SignalRCalls.NewTripStatus(false);
				Finish();
			};


		}
	}
}

