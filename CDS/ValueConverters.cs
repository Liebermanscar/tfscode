﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;
using System.Windows;
using BOL;

namespace CDS
{

    [ValueConversion(typeof(DateTime), typeof(String))]
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return value;
            DateTime date = (DateTime)value;
            if (parameter != null && parameter.ToString() == "LongDate")
                return date.ToLongDateString();
            else
                return date.ToShortDateString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value.ToString();
            DateTime resultDateTime;
            if (DateTime.TryParse(strValue, out resultDateTime))
            {
                return resultDateTime;
            }
            return value;
        }
    }

    [ValueConversion(typeof(decimal), typeof(string))]
    public class PriceConverter : IValueConverter
    {

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
         {
            if (value != null && value.ToString() != String.Empty)
            {
                                      decimal price = (decimal)value;
                if (price == 0)
                {
                    if (parameter != null && parameter.ToString() == "EmptyZero")
                        return "";
                }
                return price.ToString("C");
            }
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string price = value.ToString();

            if (price == "")
                return 0;
            decimal result;
            if (Decimal.TryParse(price, NumberStyles.Any, null, out result))
            {
                return result;
            }
            return value;
        }
        #endregion
    }

    [ValueConversion(typeof(string), typeof(string))]
    public class StringToPhoneNoFormatConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value.ToString().Length == 10)
            {
                string returnValue = value.ToString();
                //returnValue = returnValue.Insert(0, "(");
                returnValue = returnValue.Insert(3, ".");
                returnValue = returnValue.Insert(7, ".");
                return returnValue;
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.ToString().Length != 12)
                return value;

            string returnValue = value.ToString();
            //returnValue = returnValue.Remove(0);
            returnValue = returnValue.Remove(3, 1);
            returnValue = returnValue.Remove(6, 1);
            return returnValue;
        }

        #endregion
    }

    //[ValueConversion(typeof(object), typeof(int))]
    //public class NumberToPolarValueConverter : IValueConverter
    //{
    //    public object Convert(
    //     object value, Type targetType,
    //     object parameter, CultureInfo culture)
    //    {
    //        if ((string)parameter == "BadScan")
    //        {
    //            if ((bool)value)
    //                return Brushes.Red;
    //            else
    //                return Brushes.AliceBlue;
    //        }
    //        if ((string)parameter == "LoanBalance")
    //        {
    //            if ((decimal)value > 0)
    //                return Brushes.Red;
    //            else
    //                return Brushes.Blue;
    //        }
    //        if ((string)parameter == "AccountBalance")
    //        {
    //            if ((decimal)value >= 0)
    //                return Brushes.Blue;
    //            else
    //                return Brushes.Red;
    //        }
    //        if ((string)parameter == "Check")
    //        {
    //            TransTypes transType = (TransTypes)System.Convert.ChangeType(value, typeof(TransTypes));

    //            if (transType == TransTypes.Received)
    //                return Brushes.Green;
    //            else
    //                return Brushes.Red;
    //        }

    //        if ((string)parameter == "PostDated")
    //        {
    //            bool IsPostDated = (bool)System.Convert.ChangeType(value, typeof(bool));
    //            if (IsPostDated)
    //                return Brushes.Red;
    //            else
    //                return Brushes.Black;
    //        }

    //        if (value is TransTypes)
    //        {
    //            TransTypes transType = (TransTypes)System.Convert.ChangeType(value, typeof(TransTypes));

    //            if (transType == TransTypes.Received)
    //                return Brushes.DarkGreen;
    //            else
    //                return Brushes.Red;
    //        }
    //        else
    //        {
    //            decimal balance = (decimal)value;
    //            if ((parameter as string) == "Loan")
    //            {
    //                if (balance > 0)
    //                    return Brushes.Red;
    //                else
    //                    return Brushes.Green;
    //            }
    //            else
    //            {
    //                if (balance >= 0)
    //                    return Brushes.Green;
    //                else
    //                    return Brushes.Red;
    //            }
    //        }
    //    }

    //    public object ConvertBack(
    //     object value, Type targetType,
    //     object parameter, CultureInfo culture)
    //    {
    //        throw new NotSupportedException("ConvertBack not supported");
    //    }
    //}

    [ValueConversion(typeof(bool), typeof(Brush))]
    public class IsEditedToBrushConverter : IValueConverter
    {
        public object Convert(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {

            if ((bool)value)
                
                return Brushes.Purple;
            else
                return Application.Current.Resources["BackgroundBrush"] as Brush;
        }                  

        public object ConvertBack(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("ConvertBack not supported");
        }
    }

    [ValueConversion(typeof(bool), typeof(Brush))]
    public class DriverStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if ((DriverStatusOptions)value == DriverStatusOptions.NintyEight)
                return Brushes.LightGreen;
            else if ((DriverStatusOptions)value == DriverStatusOptions.ShortBreak)
                return Brushes.LightBlue;
            //Red 252 Green 160 Blue 24
            else if ((DriverStatusOptions)value == DriverStatusOptions.LongBreak)
                return new SolidColorBrush(Color.FromRgb(252, 160, 24));
            else if ((DriverStatusOptions)value == DriverStatusOptions.LongTrip)
                return Brushes.LightSalmon;
            else
                return Brushes.Pink;
        }

        public object ConvertBack(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("ConvertBack not supported");
        }
    }

    [ValueConversion(typeof(bool), typeof(Brush))]
    public class TripStatusConverter : IValueConverter
    {
        public object Convert(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {

            if ((TripStatusTypes)value == TripStatusTypes.ReceivedCall)
                return new SolidColorBrush(Color.FromRgb(252, 160, 24));
            else if ((TripStatusTypes)value == TripStatusTypes.Assigned)
                return Brushes.Pink;
            else
                return Brushes.LightGreen;
        }

        public object ConvertBack(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("ConvertBack not supported");
        }
    }


    //[ValueConversion(typeof(object), typeof(bool))]
    //public class TransTypeToBoolValueConverter : IValueConverter
    //{
    //    public object Convert(
    //     object value, Type targetType,
    //     object parameter, CultureInfo culture)
    //    {
    //        TransTypes transType = (TransTypes)System.Convert.ChangeType(value, typeof(TransTypes));

    //        if ((parameter as string) == "Received")
    //        {
    //            if (transType == TransTypes.Received)
    //                return Visibility.Visible;
    //            else
    //                return Visibility.Hidden;
    //        }
    //        else
    //        {
    //            if (transType == TransTypes.Dispensed)
    //                return Visibility.Visible;
    //            else
    //                return Visibility.Hidden;
    //        }
    //    }
    //    public object ConvertBack(
    //     object value, Type targetType,
    //     object parameter, CultureInfo culture)
    //    {
    //        throw new NotSupportedException("ConvertBack not supported");
    //    }
    //}

    [ValueConversion(typeof(object), typeof(bool))]
    public class CurrencyToBoolValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null)
                return value;

            decimal d = (decimal)value;
            return d != 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    //[ValueConversion(typeof(object), typeof(bool))]
    //public class ApplyTypeToBoolValueConverter : IValueConverter
    //{
    //    #region IValueConverter Members
    //    object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        ApplyTypes applyType = (ApplyTypes)System.Convert.ChangeType(value, typeof(ApplyTypes));            
    //        if ((parameter as string) == "Account")
    //        {
    //            if (applyType == ApplyTypes.LoanPayment || applyType == ApplyTypes.Loan)
    //                return false;
    //            else
    //                return true;
    //        }
    //        else
    //        {
    //            if (applyType == ApplyTypes.LoanPayment || 
    //                applyType == ApplyTypes.Loan ||
    //                applyType == ApplyTypes.Fee ||
    //                applyType == ApplyTypes.Adjustment ||
    //                applyType == ApplyTypes.CheckReturned)
    //                return true;
    //            else
    //                return false;
    //        }
    //    }

    //    object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    #endregion
    //}

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                              CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;

            if (value is Boolean)
            {
                return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
            }
            
            return Visibility.Collapsed;

            //return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                  CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
