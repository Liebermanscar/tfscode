﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOL
{
    public class OrganizationInfo : BO
    {
        public string OrganizationName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CityStatePostalCode
        {
            get
            { return this.City + ", " + this.State + "  " + this.PostalCode; }
        }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string EmailAddress { get; set; }
        public string FederalTaxID { get; set; }

        public override string ToString()
        {
            return this.OrganizationName;
        }

        public override bool AllowSave
        {
            get
            {
                throw new NotImplementedException();
            }

        }
    }
}
