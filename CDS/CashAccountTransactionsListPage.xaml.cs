﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for CashAccountTransactionsListPage.xaml
    /// </summary>
    public partial class CashAccountTransactionsListPage : Page, IDisplayTextBlock
    {
        public CashAccountTransactionsListPage()
        {
            InitializeComponent();
            ToDatePicker.SelectedDate = DateTime.Today;
        }

        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            UIController.ExportTelerikGrid(CashAccountTransactionsDataGrid, UIController.ExportFormat.Excel);

        }

        private void CashAccountTransactionsDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            this.CashAccountTransactions = DAL.AccountTransactionDB.GetAccountTransaction();
            CalculateRuuningTotal(this.CashAccountTransactions.ToList());

            this.CashAccountTransactionsDataGrid.DataContext = this.CashAccountTransactions;
        }

        private void CalculateRuuningTotal(List<AccountTransaction> accountTransactions)
        {
            decimal PreviousTotal = 0;
            foreach (var transaction in accountTransactions)
            {

                transaction.AccountRuningBalance = PreviousTotal + transaction.Amount;
                PreviousTotal = transaction.AccountRuningBalance;
            }
        }

        private void CashAccountTransactionsDataGrid_Filtered(object sender, Telerik.Windows.Controls.GridView.GridViewFilteredEventArgs e)
        {
            try
            {
                this.CalculateRuuningTotal(this.CashAccountTransactionsDataGrid.Items.Cast<AccountTransaction>().ToList());
            }
            catch (Exception ex)
            { UIController.DisplayErrorMessage(ex); }
        }
        ObservableCollection<AccountTransaction> CashAccountTransactions
        {
            get;
            set;
        }

        public string HeaderText
        {
            get
            {
                return "Cash Account Transactions";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        private void CashAccountTransactionsDataGrid_Sorted(object sender, Telerik.Windows.Controls.GridViewSortedEventArgs e)
        {
            try
            {
                this.CalculateRuuningTotal(this.CashAccountTransactionsDataGrid.Items.Cast<AccountTransaction>().ToList());
            }
            catch (Exception ex)
            { UIController.DisplayErrorMessage(ex); }

        }


    }
}
