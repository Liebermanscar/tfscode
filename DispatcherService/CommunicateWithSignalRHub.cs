﻿using BOL;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatcherService
{
    public static class CommunicateWithSignalRHub
    {
        //signalR variables
        static string _dispatchingHubUrl;
        static HubConnection _hubConnection;
        static IHubProxy _dispatchingHubProxy;
        static DispatchService _dispatcherSerivce;


        static bool _isSubscribed = false;

        internal static void Subscribe(string url, DispatcherService.DispatchService dispatcherService)
        {
            _dispatcherSerivce = dispatcherService;

            _dispatchingHubUrl = "http://" + url + "/signalr/";

            var querystringData = new Dictionary<string, string>();
            querystringData.Add("clientType", "DispatchingService");

            _hubConnection = new HubConnection(_dispatchingHubUrl, querystringData);

            _dispatchingHubProxy = _hubConnection.CreateHubProxy("DispatchingHub");

            _hubConnection.Start().Wait();

            _dispatchingHubProxy.Invoke("Join", "DispatchingService");

            _isSubscribed = true;

            //called from signalr hub to update drivers status;

            _dispatchingHubProxy.On<int>("SignInDriver", driverID =>
            {
                var driver = DAL.DriversDB.GetDrivers(true, driverID).FirstOrDefault();
                dispatcherService.SignInDriver(driver, "");
                //dispatcherService.SignInDriver(new Driver() { DriverID = driverID }, "");
            });

            _dispatchingHubProxy.On<int>("RemoveDriver", driverID =>
            {
                dispatcherService.RemoveDriver(driverID);
            });

            _dispatchingHubProxy.On<int>("DispatchedNewTripRequestToDriver", tripID =>
            {
                if (tripID == 0)
                    return;

                var trip = _dispatcherSerivce.GetTrip(tripID);
                trip.TripStatus = TripStatusTypes.SentToDriver;
                dispatcherService.EditTrip(trip);
            });

            _dispatchingHubProxy.On<int, int, bool>("DriverResponseToTripAssignment", (driverID, tripID, accepted) =>
            {
                if (accepted)
                {
                    _dispatcherSerivce.AssignDriverToTrip(tripID, driverID);
                }
            });

            _dispatchingHubProxy.On<int, BOL_OTR.DriverStatusOptions>("DriverStatusChanged", (driverID, status) => {
                dispatcherService.EditDriverStatus(driverID, (DriverStatusOptions)status);
            });

            _dispatchingHubProxy.On<int, BOL_OTR.TripDetailedStatusOptions>("TripStatusChanged", (tripId, status) => {

                var trip = _dispatcherSerivce.GetTrip(tripId);

                if (status == BOL_OTR.TripDetailedStatusOptions.TripCompleted)
                {
                    _dispatcherSerivce.ChangeTripStatus(trip, TripStatusTypes.Complete, null, false);
                }
                else
                {
                    trip.DetailedStatus = status;

                    _dispatcherSerivce.EditTrip(trip);
                }
            });

            _dispatchingHubProxy.On<int, bool>("DriverRequestedBreak", (driverID, longBreak) =>
            {
                _dispatcherSerivce.SendRequestToDispatcher(driverID, longBreak ?
                    BOL_OTR.DriversRequestOptions.LongBreak :
                    BOL_OTR.DriversRequestOptions.ShortBreak);
            });

            _dispatchingHubProxy.On<BOL_OTR.Trip_OTR>("SaveTripDetails", (trip) =>
            {
                var serverTrip = _dispatcherSerivce.GetTrip(trip.TripID);
                serverTrip.CashPaid = trip.CashPaid;
                serverTrip.CheckPaid = trip.CheckPaid;
                _dispatcherSerivce.EditTrip(serverTrip);
            });
        }
    }
}
