﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Data;

namespace DAL
{
    public class DailyEnvelopeDB : Database
    {
        public DailyEnvelopeDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(DailyEnvelope de)
        {
            this.Transaction = this.Connection.BeginTransaction();
            try
            {
                string serverMessage;
                SqlParameter outParam = new SqlParameter("@DailyEnvelopeID", System.Data.SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@DriverID", de.UnitDriver.DriverID),
                    new SqlParameter("@CommisionTypeID", de.CommisionTypeUsed.CommisionTypeID),
                    new SqlParameter("@DriversCommisionAmount", de.DriverCommisionAmount),
                    new SqlParameter("@EnvelopeDate", de.DateOfTrips),
                    new SqlParameter("@CashAmountBooked", de.CashAmountBooked),
                    new SqlParameter("@AmountSubmitted", de.AmountSubmitted),
                    new SqlParameter("@OwedForDriverAmount", de.OwedForDriverAmount),
                    new SqlParameter("@GrossIncome", de.GrossBooking),
                    new SqlParameter("@CompanyCoveredExpenseAmount", de.CompanyCoveredExpenseAmount),
                    new SqlParameter("@DriverCommisionAmount", de.DriverCommisionAmount),
                    new SqlParameter("@GasPaid", de.GasPaid),
                    new SqlParameter("@GasCharged", de.GasCharged),
                    new SqlParameter("@IncomeOfBooksSold", de.IncomeOfBooksSold),
                    new SqlParameter("@TotalForDriverAmount", de.TotalForDriverAmount),
                    new SqlParameter("@TotalPaidForDriverWithTickets", de.TotalValueOfMissingTickets),
                    new SqlParameter("@TicketsReceivedValue", de.TicketsReceivedValue),
                    new SqlParameter("@TotalChargeTrips", de.TotalChargeTrips),
                    new SqlParameter("@TotalFreeTrips", de.TotalDriverTrips),
                    new SqlParameter("@TripsIncome", de.TripsIncome),
                    new SqlParameter("@VehicleUsedID",de.VehicleUsed.VehicleID),
                    new SqlParameter("@NoteToDriver",de.NoteForDriver),
                    new SqlParameter("@TotalCashFromBooksSold", de.TotalCashFromBooksSold),
                    new SqlParameter("@SessionIDAdded", Database.SessionID),

                    outParam
                };

                int noOfRecordsAffected = this.ExecuteNonQuery("AddDailyEnvelopes", param, CommandType.StoredProcedure, out serverMessage);
                if (noOfRecordsAffected > 0)
                {
                    de.DailyEnvelopeID = Convert.ToInt32(outParam.Value);

                    using (TripDB tripDB = new TripDB(this.Connection, this.Transaction))
                    {
                        foreach (Trip trip in de.TripsCovered)
                        {
                            if (trip.TripID == 0)
                            {
                                trip.AssignedToDailyEnvelopeID = de.DailyEnvelopeID;
                                trip.DrivenBy = de.UnitDriver;
                                trip.DrivenWithVehicle = de.VehicleUsed;
                                trip.TripStarted = de.DateOfTrips.Value;
                                trip.TripEnded = de.DateOfTrips.Value;
                                trip.TimeOfCall = de.DateOfTrips.Value;
                                tripDB.Add(trip);
                            }
                            else
                            {
                                trip.AssignedToDailyEnvelopeID = de.DailyEnvelopeID;
                                tripDB.Edit(trip);
                            }
                        }
                    }

                    using (DETicketsReceivedDB trDB = new DETicketsReceivedDB(this.Connection, this.Transaction))
                    {
                        foreach (TicketsReceived tr in de.TicketsReceivedByDriver)
                        {
                            tr.DailyEnvelopID = de.DailyEnvelopeID;
                            trDB.Add(tr);
                        }
                    }

                    using (DEExpenseDB deExpenseDB = new DEExpenseDB(this.Connection, this.Transaction))
                    {
                        foreach (DEExpense deExp in de.Expenses)
                        {
                            deExp.DailyEnvelopeID = de.DailyEnvelopeID;
                            deExpenseDB.Add(deExp);
                        }
                    }

                    using (TicketBookStatusDB tbsDB = new TicketBookStatusDB(this.Connection, this.Transaction))
                    {
                        foreach (var tbs in de.TicketBooksSold.Where(tbs => tbs.TicketBookStatusesSold != null))
                        {
                            ObservableCollection<TicketBookStatus> TBS = tbs.TicketBookStatusesSold;
                            for (int i = 0; i < TBS.Count; i++)
                            {
                                //save the customer cause will get wiped out in next line when taking info from db
                                TicketBookStatus tempTbs = new TicketBookStatus()
                                {
                                    CustomerSoldTo = TBS[i].CustomerSoldTo,
                                    SoldToInfo = TBS[i].SoldToInfo,
                                    TBDiscountType = TBS[i].TBDiscountType,
                                    IsPaidByCC = TBS[i].IsPaidByCC
                                };

                                TBS[i] = DAL.TicketBookStatusDB.GetTicketBookStatuses(0, 0, TBS[i].BookNumber, TBS[i].TicketBookID)[0];
                                TBS[i].CustomerSoldTo = tempTbs.CustomerSoldTo; //now give him back the customer
                                TBS[i].DailyEnvelopeIdSold = de.DailyEnvelopeID;
                                TBS[i].BookStatus = TicketBookStatusOptions.Sold;
                                TBS[i].SoldToInfo = tempTbs.SoldToInfo;
                                TBS[i].TBDiscountType = tempTbs.TBDiscountType;
                                TBS[i].IsPaidByCC = tempTbs.IsPaidByCC;
                            }
                            tbsDB.EditCollection(TBS, true);
                        }
                    }

                    using (DETicketLogDB tlDB = new DETicketLogDB(this.Connection, this.Transaction))
                    {
                        foreach (var detr in de.TicketsReceivedByDriver.Where(t => t.DETicketLogs != null))
                        {
                            foreach (var tr in detr.DETicketLogs)
                            {
                                tr.TicketBookId = detr.FromTicketBook.TicketBookID;
                                tr.DailyEnvelopeId = de.DailyEnvelopeID;
                            }
                            DAL.DETicketLogDB.AddDETicketLog(detr.DETicketLogs);
                        }
                    }

                    using (SavedFilesDB fileDB = new SavedFilesDB(this.Connection, this.Transaction))
                    {
                        foreach (var file in de.SavedFiles.Where(sf => !sf.IsMarkedForDeletion))
                        {
                            file.DailyEnvelopeID = de.DailyEnvelopeID;
                            fileDB.Add(file);
                        }
                    }
                }
                this.Transaction.Commit();

                return noOfRecordsAffected > 0;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw ex;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }
        public static bool AddDailyEnvelope(DailyEnvelope dailyEnvelope)
        {
            using (DailyEnvelopeDB deDB = new DailyEnvelopeDB(null, null))
            {
                return deDB.Add(dailyEnvelope);
            }
        }


        public ObservableCollection<DailyEnvelope> Get(int dailyEnvelopeID, int driverID, DateTime? DateOfEnvelope, bool notPaidOnly, int driverPaymentID,
            DataRequestTypes requestType = DataRequestTypes.LiteData)
        {
            var envelopes = new ObservableCollection<DailyEnvelope>();
            //      var driverPayment = new ObservableCollection<DriverPayment>();
            using (SqlDataReader dr = this.GetReader("GetDailyEnvelopes", new SqlParameter[]
            {
                new SqlParameter("@DailyEnvelopeID",dailyEnvelopeID),
                new SqlParameter("@DriverID", driverID),
                new SqlParameter("@DateOfEnvelope", DateOfEnvelope),
                new SqlParameter("@NotPaidOnly", notPaidOnly),
                new SqlParameter("@DriverPaymentID",driverPaymentID)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var envelope = new DailyEnvelope()
                    {
                        StaticData = true,
                        OwedForDriverAmount = (Convert.ToDecimal(dr["OwedForDriverAmount"])),
                        DateOfTrips = Convert.ToDateTime(dr["EnvelopeDate"]),
                        DailyEnvelopeID = Convert.ToInt32(dr["DailyEnvelopeID"]),
                        DriverCode = dr["DriverCode"].ToString(),
                        AmountSubmitted = Convert.ToDecimal(dr["AmountSubmitted"]),
                        CashAmountBooked = Convert.ToDecimal(dr["CashAmountBooked"]),
                        GrossBooking = Convert.ToDecimal(dr["GrossIncome"]),
                        GasPaid = Convert.ToDecimal(dr["GasPaid"]),
                        GasCharged = Convert.ToDecimal(dr["GasCharged"]),
                        DriverCommisionAmount = Convert.ToDecimal(dr["DriverCommisionAmount"]),
                        TotalForDriverAmount = Convert.ToDecimal(dr["TotalForDriverAmount"]),
                        DriverPaymentID = Convert.ToInt32(dr["DriverPaymentId"]),
                        NoteForDriver = Convert.ToString(dr["NoteForDriver"]),
                        TotalCashFromBooksSold = Convert.ToDecimal(dr["TotalCashFromBooksSold"]),


                        CommisionTypeUsed = dr["CommisionTypeID"] == DBNull.Value ? null : new CommisionType()
                        {
                            CommisionTypeID = Convert.ToInt32(dr["CommisionTypeID"]),

                            CommisionTypeCode = dr["CommisionTypeCode"].ToString(),
                            IsBonusEligible = Convert.ToBoolean(dr["IsBonusEligible"]),
                            //   DriversCommision=Convert.ToDouble(dr[""]),
                            //   PercentOfGasDriverPays=Convert.ToDouble(dr[""])
                        },
                        UnitDriver = dr["DriverID"] == DBNull.Value ? null : new Driver()
                        {
                            DriverID = Convert.ToInt32(dr["DriverID"]),
                            DriverCode = dr["DriverCode"].ToString(),
                            DriverFirstName = dr["DriverFirstName"].ToString(),
                            DriverLastName = dr["DriverLastName"].ToString(),
                        },
                        VehicleUsed = dr["VehicleUsedID"] == DBNull.Value ? null : new Vehicle()
                        {
                            VehicleID = Convert.ToInt32(dr["VehicleUsedID"]),
                            VehicleCode = dr["VehicleCode"].ToString(),

                        }

                    };

                    if (requestType == DataRequestTypes.FullData)
                    {
                        //envelope.TripsCovered = DAL.TripDB.GetTrips(
                    }
                    envelopes.Add(envelope);
                }
            }
            return envelopes;

        }


        public static ObservableCollection<DailyEnvelope> GetDailyEnvelopes(int dailyEnvelopeID, int driverID, DateTime? fromDateOfTrip, bool notPaidOnly, int driverPaymentID)
        {
            using (DailyEnvelopeDB dailyEnvelopeDB = new DailyEnvelopeDB(null, null))
            {
                return dailyEnvelopeDB.Get(dailyEnvelopeID, driverID, fromDateOfTrip, notPaidOnly, driverPaymentID);
            }
        }

        public bool Edit(DailyEnvelope dailyEnvelope)
        {
            this.Transaction = this.Connection.BeginTransaction();
            try
            {
                string serverMessage;
                // var ScheduledTripIDParam = new SqlParameter("ScheduledTripID", SqlDbType.Int) { Direction = ParameterDirection.Output };
                int noOfRecAffected = this.ExecuteNonQuery("UpdateDailyEnvelopes", new SqlParameter[]
                {
                new SqlParameter("@DailyEnvelopeID", dailyEnvelope.DailyEnvelopeID),
               new SqlParameter("@DriverID", dailyEnvelope.UnitDriver.DriverID),
                new SqlParameter("@CommisionTypeID", dailyEnvelope.CommisionTypeUsed.CommisionTypeID),
                new SqlParameter("@EnvelopeDate", dailyEnvelope.DateOfTrips),
              //  new SqlParameter("@CustomerID", dailyEnvelope.ScheduledByCustomer == null ? 0 : dailyEnvelope.ScheduledByCustomer.CustomerID),
                new SqlParameter("@CashAmountBooked", dailyEnvelope.CashAmountBooked),
                new SqlParameter("@AmountSubmitted", dailyEnvelope.AmountSubmitted),
                new SqlParameter("@OwedForDriverAmount", dailyEnvelope.OwedForDriverAmount),
                new SqlParameter("@GrossIncome", dailyEnvelope.GrossBooking),
                new SqlParameter("@CompanyCoveredExpenseAmount", dailyEnvelope.CompanyCoveredExpenseAmount),
                new SqlParameter("@DriverCommisionAmount", dailyEnvelope.DriverCommisionAmount),
                new SqlParameter("@GasPaid", dailyEnvelope.GasPaid),
                new SqlParameter("@GasCharged", dailyEnvelope.GasCharged),
                new SqlParameter("@IncomeOfBooksSold", dailyEnvelope.IncomeOfBooksSold),
                new SqlParameter("@TotalForDriverAmount", dailyEnvelope.TotalForDriverAmount),
                new SqlParameter("@TotalPaidForDriverWithTickets", dailyEnvelope.TotalValueOfMissingTickets),
                new SqlParameter("@TicketsReceivedValue", dailyEnvelope.TicketsReceivedValue),
                new SqlParameter("@TotalChargeTrips", dailyEnvelope.TotalChargeTrips),
                new SqlParameter("@TotalFreeTrips", dailyEnvelope.TotalDriverTrips),
                new SqlParameter("@TripsIncome", dailyEnvelope.TripsIncome),
                new SqlParameter("@VehicleUsedID",dailyEnvelope.VehicleUsed.VehicleID),
                new SqlParameter("@NoteToDriver",dailyEnvelope.NoteForDriver),
                new SqlParameter("@TotalCashFromBooksSold", dailyEnvelope.TotalCashFromBooksSold),
                new SqlParameter("@DriverPaymentId", dailyEnvelope.DriverPaymentID)

                }, CommandType.StoredProcedure, out serverMessage);

                if (noOfRecAffected > 0)
                {
                    using (TripDB tripDB = new TripDB(this.Connection, this.Transaction))
                    {
                        foreach (Trip trip in dailyEnvelope.TripsCovered)
                        {
                            if (trip.TripID == 0)
                            {
                                trip.AssignedToDailyEnvelopeID = dailyEnvelope.DailyEnvelopeID;
                                trip.DrivenBy = dailyEnvelope.UnitDriver;
                                trip.DrivenWithVehicle = dailyEnvelope.VehicleUsed;
                                trip.TripStarted = dailyEnvelope.DateOfTrips.Value;
                                trip.TripEnded = dailyEnvelope.DateOfTrips.Value;
                                trip.TimeOfCall = dailyEnvelope.DateOfTrips.Value;
                                tripDB.Add(trip);
                            }
                            else
                            {
                                trip.AssignedToDailyEnvelopeID = dailyEnvelope.DailyEnvelopeID;
                                tripDB.Edit(trip);
                            }
                        }
                    }

                    using (DEExpenseDB deExpenseDB = new DEExpenseDB(this.Connection, this.Transaction))
                    {
                        foreach (DEExpense deExp in dailyEnvelope.Expenses)
                        {
                            if (deExp.DEExpenseID > 0)
                                deExpenseDB.Edit(deExp);
                            else
                            {
                                deExp.DailyEnvelopeID = dailyEnvelope.DailyEnvelopeID;
                                deExpenseDB.Add(deExp);
                            }
                        }
                    }

                    using (TicketBookStatusDB tbsDB = new TicketBookStatusDB(this.Connection, this.Transaction))
                    {
                        foreach (var tbs in dailyEnvelope.TicketBooksSold.Where(tbs => tbs.TicketBookStatusesSold != null))
                        {
                            ObservableCollection<TicketBookStatus> TBS = tbs.TicketBookStatusesSold;
                            for (int i = 0; i < TBS.Count; i++)
                            {
                                if (TBS[i].DailyEnvelopeIdSold > 0)
                                    continue;

                                //save the customer cause will get wiped out in next line when taking info from db
                                TicketBookStatus tempTbs = new TicketBookStatus()
                                {
                                    CustomerSoldTo = TBS[i].CustomerSoldTo,
                                    SoldToInfo = TBS[i].SoldToInfo,
                                    TBDiscountType = TBS[i].TBDiscountType,
                                    IsPaidByCC = TBS[i].IsPaidByCC
                                };

                                TBS[i] = DAL.TicketBookStatusDB.GetTicketBookStatuses(0, 0, TBS[i].BookNumber, TBS[i].TicketBookID)[0];
                                TBS[i].CustomerSoldTo = tempTbs.CustomerSoldTo; //now give him back the customer
                                TBS[i].DailyEnvelopeIdSold = dailyEnvelope.DailyEnvelopeID;
                                TBS[i].BookStatus = TicketBookStatusOptions.Sold;
                                TBS[i].SoldToInfo = tempTbs.SoldToInfo;
                                TBS[i].TBDiscountType = tempTbs.TBDiscountType;
                                TBS[i].IsPaidByCC = tempTbs.IsPaidByCC;
                            }
                            tbsDB.EditCollection(TBS, true);
                        }
                    }

                    //using (DETicketLogDB tlDB = new DETicketLogDB(this.Connection, this.Transaction))
                    //{
                    //    foreach (var detr in de.TicketsReceivedByDriver.Where(t => t.DETicketLogs != null))
                    //    {
                    //        foreach (var tr in detr.DETicketLogs)
                    //        {
                    //            tr.TicketBookId = detr.FromTicketBook.TicketBookID;
                    //            tr.DailyEnvelopeId = de.DailyEnvelopeID;
                    //        }
                    //        DAL.DETicketLogDB.AddDETicketLog(detr.DETicketLogs);
                    //    }
                    //}


                    using (SavedFilesDB fileDB = new SavedFilesDB(this.Connection, this.Transaction))
                    {
                        foreach (SavedFile file in dailyEnvelope.SavedFiles)
                        {
                            if (file.FileID == 0)
                            {
                                file.DailyEnvelopeID = dailyEnvelope.DailyEnvelopeID;
                                fileDB.Add(file);
                            }
                            if (file.IsMarkedForDeletion)
                            {
                                fileDB.Delete(file.FileID, 0);
                            }
                        }
                    }

                }

                this.Transaction.Commit();

                return noOfRecAffected > 0;
            }
            catch (Exception)
            {
                this.Transaction.Rollback();
                throw;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }

        public static bool EditDailyEnvelope(DailyEnvelope dailyEnvelope)
        {
            using (DailyEnvelopeDB dailyEnvelopeDB = new DailyEnvelopeDB(null, null))
            {
                return dailyEnvelopeDB.Edit(dailyEnvelope);
            }
        }
        public static DateTime? GetNextEnvelopeDate(int driverID)
        {
            DateTime? dateOfNextEnvelope = null;
            using (var db = new Database(null, null))
            {
                var tempDate = db.ExecuteScalar("SELECT dbo.GetNextEnvelopeDate(" + driverID + ")", new SqlParameter[] { }, System.Data.CommandType.Text);

                if (tempDate != DBNull.Value)
                    dateOfNextEnvelope = Convert.ToDateTime(tempDate);
                else dateOfNextEnvelope = DateTime.Today;
            }

            return dateOfNextEnvelope;
        }

        public static bool IfDateOfEnvelopeExists(int driverID, DateTime dateToCheck)
        {
            bool results = false;


            using (var db = new Database(null, null))
            {
                var YesNO = db.ExecuteScalar("SELECT dbo.CheckIfEnvelopeDateExsist(" + driverID + ",'" + dateToCheck + "')", new SqlParameter[] { }, System.Data.CommandType.Text);

                if (YesNO != DBNull.Value)
                    results = Convert.ToBoolean(YesNO);

            }

            return results;

        }



    }

    public class SavedFilesDB : Database
    {
        public SavedFilesDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public bool Add(SavedFile savedFile)
        {
            string serverMsg;
            SqlParameter outParam = new SqlParameter("@FileIDOUT", DbType.Int32) { Direction = ParameterDirection.Output };
            int noOfRecords = this.ExecuteNonQuery("AddSavedFile", new SqlParameter[]
            {
                new SqlParameter("@FileName", savedFile.FileName),
                new SqlParameter("@DailyEnvelopeID", savedFile.DailyEnvelopeID),
                outParam
            }, CommandType.StoredProcedure, out serverMsg);

            if (noOfRecords > 0)
            {
                if (outParam.Value != DBNull.Value)
                    savedFile.FileID = Convert.ToInt32(outParam.Value);

                return true;
            }
            return false;
        }

        public static bool AddSavedFile(SavedFile savedFile)
        {
            using (SavedFilesDB savedFileDB = new SavedFilesDB(null, null))
            {
                return savedFileDB.Add(savedFile);
            }
        }

        public bool Delete(int fileID, int forEnvelopeID)
        {
            string serverMsg;
            int noOfRecords = this.ExecuteNonQuery("DeleteSavedFile", new SqlParameter[]
            {
                new SqlParameter("@FileID", fileID),
                new SqlParameter("@ForEnvelopeID", forEnvelopeID)
            }, CommandType.StoredProcedure, out serverMsg);
            return noOfRecords > 0;
        }

        public static bool DeleteSavedFile(int fileID, int forEnvelopeID)
        {
            using (SavedFilesDB savedFileDB = new SavedFilesDB(null, null))
            {
                return savedFileDB.Delete(fileID, forEnvelopeID);
            }
        }

        public ObservableCollection<SavedFile> Get(int forEnvelopeID)
        {
            ObservableCollection<SavedFile> files = new ObservableCollection<SavedFile>();
            using (var dr = this.GetReader("GetSavedFiles", new SqlParameter[] { new SqlParameter("@ForEnvelopeID", forEnvelopeID) }, CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    files.Add(new SavedFile()
                    {
                        FileID = Convert.ToInt32(dr["FileID"]),
                        FileName = dr["FileName"].ToString(),
                        DailyEnvelopeID = Convert.ToInt32(dr["DailyEnvelopeID"])
                    });
                }
            }
            return files;
        }

        public static ObservableCollection<SavedFile> GetSavedFiles(int forEnvelopeID)
        {
            using (SavedFilesDB savedFileDB = new SavedFilesDB(null, null))
            {
                return savedFileDB.Get(forEnvelopeID);
            }
        }
    }

    public class StreetAbbreviationDB : Database
    {
        public StreetAbbreviationDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<StreetAbbreviation> Get(int streetAbbreviationID, string abbreviation = "")
        {
            var streetAbbreviations = new ObservableCollection<StreetAbbreviation>();

            using (SqlDataReader dr = this.GetReader("GetStreetAbbreviations", new SqlParameter[]
            {
                new SqlParameter("@StreetAbbreviationID", streetAbbreviationID),
                new SqlParameter("@Abbreviation", abbreviation)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    streetAbbreviations.Add(new StreetAbbreviation()
                    {
                        StreetAbbreviationID = Convert.ToInt32(dr["StreetAbbreviationID"]),
                        Abbreviation = dr["Abbreviation"].ToString(),
                        StreetName = dr["StreetName"].ToString(),
                        City = dr["City"].ToString(),
                        State = dr["State"].ToString(),
                        PostalCode = dr["PostalCode"].ToString()
                    });
                }
            }

            return streetAbbreviations;
        }
        public bool Add(StreetAbbreviation streetAbbreviation)
        {
            string serverMessage;
            var outParam = new SqlParameter("@StreetAbbreviationID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddStreetAbbreviations", new SqlParameter[]
            {
                new SqlParameter("@Abbreviation", streetAbbreviation.Abbreviation),
                new SqlParameter("@Streetname", streetAbbreviation.StreetName),
                new SqlParameter("@City", streetAbbreviation.City),
                new SqlParameter("@State", streetAbbreviation.State),
                new SqlParameter("@PostalCode", streetAbbreviation.PostalCode),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                streetAbbreviation.StreetAbbreviationID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;

        }
        public bool Edit(StreetAbbreviation streetAbbreviation)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateStreetAbbreviations", new SqlParameter[]
            {
                new SqlParameter("@StreetAbbreviationID", streetAbbreviation.StreetAbbreviationID),
                new SqlParameter("@Abbreviation", streetAbbreviation.Abbreviation),
                new SqlParameter("@Streetname", streetAbbreviation.StreetName),
                new SqlParameter("@City", streetAbbreviation.City),
                new SqlParameter("@State", streetAbbreviation.State),
                new SqlParameter("@PostalCode", streetAbbreviation.PostalCode),
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;

        }

        public string GetStreetName(string abbreviation)
        {
            object o = this.ExecuteScalar("GetStreetNameFromAbbreviation", new SqlParameter[] { new SqlParameter("@Abbreviation", abbreviation) }, System.Data.CommandType.StoredProcedure);
            return o == null ? null : o.ToString();
        }
        public static ObservableCollection<StreetAbbreviation> GetStreetAbbreviations(int streetAbbreviationID = 0, string abbreviation = "")
        {
            using (StreetAbbreviationDB saDB = new StreetAbbreviationDB(null, null))
            {
                return saDB.Get(streetAbbreviationID, abbreviation);
            }
        }

        public static bool AddStreetAbbreviation(StreetAbbreviation streetAbbreviation)
        {
            using (StreetAbbreviationDB saDB = new StreetAbbreviationDB(null, null))
            {
                return saDB.Add(streetAbbreviation);
            }
        }

        public static bool EditStreetAbbreviation(StreetAbbreviation streetAbbreviation)
        {
            using (StreetAbbreviationDB saDB = new StreetAbbreviationDB(null, null))
            {
                return saDB.Edit(streetAbbreviation);
            }
        }

        public static string GetStreetNameByAbbreviation(string abbreviation)
        {
            using (StreetAbbreviationDB saDB = new StreetAbbreviationDB(null, null))
            {
                return saDB.GetStreetName(abbreviation);
            }
        }

    }
}
