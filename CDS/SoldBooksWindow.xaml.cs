﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SoldBooksWindow.xaml
    /// </summary>
    public partial class SoldBooksWindow : Window
    {
        public SoldBooksWindow()
        {
            InitializeComponent();
        }
        public SoldBooksWindow(TicketBookSold ticketBookSold)
            : this()
        {
            this.ActiveTicketBookSold = ticketBookSold;
        }


        TicketBookSold ActiveTicketBookSold
        {
            get
            {
                return this.DataContext as TicketBookSold;
            }
            set
            {
                this.DataContext = value;
            }
        }


        TicketBookStatus ActiveTicketBookStatus
        {
            get
            {
                return this.SoldBooksDataGrid.SelectedItem as TicketBookStatus;
            }
            //set
            //{
            //    TicketBookStatus tbs = this.ActiveTicketBookSold.TicketBookStatusesSold.FirstOrDefault(tb => tb.BookNumber == value.BookNumber);
            //    //TicketBookStatus tbs2 = value;
            //    //tbs = new TicketBookStatus(value);
            //    //this.SoldBooksDataGrid.SelectedItem = value as TicketBookStatus;
            //}
        }
        private void SoldBooksDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //this.ActiveTicketBookStatus.CustomerSoldTo = UIController.SelectCustomerFromList(null); //first version
        }

        private void txtContactName_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {
                if ((sender as TextBox).Text == "")
                    return;
                ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, (sender as TextBox).Text, DataRequestTypes.LiteData, false);
                //Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

                if (customers.Count == 1)
                    this.ActiveTicketBookStatus.CustomerSoldTo = customers[0];
                else if (customers.Count > 1)
                    this.ActiveTicketBookStatus.CustomerSoldTo = UIController.SelectCustomerFromList(customers);
                else if (customers.Count == 0)
                {
                    this.ActiveTicketBookStatus.SoldToInfo = (sender as TextBox).Text.Trim();
                }

                /*  DataGridRow dgrow = (DataGridRow)this.TripsGrid.ItemContainerGenerator.ContainerFromItem(TripsGrid.Items[TripsGrid.SelectedIndex]);
                  dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));*/

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }
        private void Button_SelectCustomer(object sender, RoutedEventArgs e)
        {
            try
            {
                var customerToCharge = UIController.SelectCustomerFromList(null, false);
                if (customerToCharge == null) return;
                this.ActiveTicketBookStatus.CustomerSoldTo = customerToCharge;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = null;
        }

        private void SoldBooksDataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            //DataGridColumn dgc = e.Column;
            /* if (e.Column.Header.ToString() == "Bar-Code")
             {
                 TicketBookStatus tbs = e.Row.DataContext as TicketBookStatus;
                 TicketBookStatus tbs2 = this.ActiveTicketBookSold.TicketBookStatusesSold.FirstOrDefault(tb => tb.BookNumber == tbs.BookNumber);
                 int i = this.ActiveTicketBookSold.TicketBookStatusesSold.IndexOf(tbs2);
                 tbs2 = DAL.TicketBookStatusDB.GetTicketBookStatuses(0,0,tbs.BookNumber,tbs.TicketBookID)[0];
                 this.ActiveTicketBookSold.TicketBookStatusesSold[i] = tbs2;

             }*/
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.ActiveTicketBookSold.TicketBookStatusesSold.Remove(this.ActiveTicketBookStatus);
        }

        private void Button_MemberDiscount(object sender, RoutedEventArgs e)
        {
            //verify if is paid member, if yes save as member discount msg "Approved", else msg alert
            string homePhone = "";
            string cellPhone = "";
            if (this.ActiveTicketBookStatus.CustomerSoldTo != null)
            {
                homePhone = this.ActiveTicketBookStatus.CustomerSoldTo.HomePhoneNo;
                cellPhone = this.ActiveTicketBookStatus.CustomerSoldTo.CellPhoneNo;
            }
            else
            {
                homePhone = this.ActiveTicketBookStatus.SoldToInfo;
            }

            if (DAL.TicketBookStatusDB.CheckIfIsPaidMember(homePhone, cellPhone))
            {
                MessageBox.Show("Paid member!", "Approved", MessageBoxButton.OK);
                this.ActiveTicketBookStatus.TBDiscountType = BookDiscountTypes.Member;
            }
            else
            {
                MessageBox.Show("Not A Paid Member!", "Declined", MessageBoxButton.OK);
                this.ActiveTicketBookStatus.TBDiscountType = BookDiscountTypes.None;
            }
        }
    }
}
