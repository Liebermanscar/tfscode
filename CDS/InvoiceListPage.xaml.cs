﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for InvoiceListPage.xaml
    /// </summary>
    public partial class InvoiceListPage : Page, IDisplayTextBlock
    {
        public InvoiceListPage()
        {
            InitializeComponent();
        }



        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt1 = new DateTime();
                DateTime dt2 = new DateTime();
                if (FromDatePicker.SelectedDate != null) { dt1 = FromDatePicker.SelectedDate.Value.Date; }
                if (ToDatePicker.SelectedDate != null) { dt2 = ToDatePicker.SelectedDate.Value.Date; }
                this.Invoices = DAL.InvoiceDB.GetInvoices(0, 0, 0, false);
                InvoicesByDateDataGrid.DataContext = this.Invoices;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            UIController.ExportTelerikGrid(InvoicesByDateDataGrid, UIController.ExportFormat.Excel);
        }
        private void RadMenuItemRePrintInvoice_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (this.SelectedInvoice == null)
                return;

            try
            {
                IList<Invoice> invoicesProcessed = DAL.InvoiceDB.GetInvoices(this.SelectedInvoice.InvoiceID, 0, 0, true);
                UIController.PrintInvoice(invoicesProcessed.ToList());
                //  UIController.PrintInvoice(this.SelectedInvoice);//LoadCustomerInfoPage(SelectedInvoice.InvoicedCustomer);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }
        private void InvoicesByDateDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedInvoice == null)
                return;

            try
            {
                UIController.LoadCustomerInfoPage(SelectedInvoice.InvoicedCustomer);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        Invoice SelectedInvoice
        {
            get { return this.InvoicesByDateDataGrid.SelectedItem as Invoice; }
        }
        public ObservableCollection<Invoice> Invoices { get; set; }

        #region
        public string HeaderText
        {
            get
            {
                return "Invoice List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        private void RadMenuItemDeleteInvoice_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            if (SelectedInvoice == null)
                return;
            if (MessageBox.Show("Are You sure you want to Delete this invoice ?", "Delete Invoice?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                DAL.InvoiceDB.DeleteInvoice(SelectedInvoice.InvoiceID);

            }
        }



    }
}
