﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using BOL;

namespace DAL
{
    public class InvoiceDB : Database
    {
        public InvoiceDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<Invoice> Get(int invoiceID, int customerID, int invoiceBatchID, bool withTrips = false)
        {
            ObservableCollection<Invoice> invoices = new ObservableCollection<Invoice>();
            using (SqlDataReader dr = this.GetReader("GetInvoices",
                new SqlParameter[]
                {
                    new SqlParameter("@InvoiceID", invoiceID),
                    new SqlParameter("@InvoiceBatchID", invoiceBatchID),
                    new SqlParameter("@CustomerID", customerID)
                }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    invoices.Add(new Invoice()
                    {
                        InvoiceID = Convert.ToInt32(dr["InvoiceID"]),
                        InvoiceBatchID = Convert.ToInt32(dr["InvoiceBatchID"]),
                        LastInvoiceDate = Convert.ToDateTime(dr["InvoiceStartDate"]),
                        InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]),
                        InvoicedCustomer = CustomerDB.FillCustomer(dr),
                        InvoiceTotal = Convert.ToDecimal(dr["InvoiceTotal"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        // GrandTotal = Convert.ToDecimal(dr["GrandTotal"])

                    });



                }
            }

            if (withTrips)
            {
                using (TripDB tripDB = new TripDB(null, null))
                {
                    foreach (Invoice invoice in invoices)
                    {
                        invoice.TripsInvoiced = tripDB.Get(0, 0, 0, invoice.InvoiceID, null, null, false, null, 0, 0, true);
                    }
                }
            }

            return invoices;
        }

        public ObservableCollection<UnbilledCustomer> GetUnbilledCustomers(DateTime startDate, DateTime cutOffDate)
        {
            ObservableCollection<BOL.UnbilledCustomer> unbilledCustomers = new ObservableCollection<BOL.UnbilledCustomer>();

            using (SqlDataReader dr = this.GetReader("GetUnbilledCustomers",
                new SqlParameter[] {
                     new SqlParameter("@InvoiceStartDate", startDate),
                    new SqlParameter("@CutOffDate", cutOffDate)
                }
                , System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    unbilledCustomers.Add(new BOL.UnbilledCustomer()
                    {
                        CustomerToInvoice = new BOL.Customer()
                        {
                            CustomerID = Convert.ToInt32(dr["CustomerID"]),
                            CustomerType = (CustomerTypes)Convert.ToInt32(dr["CustomerType"]),
                            CompanyName = dr["CompanyName"].ToString(),
                            Prefix = dr["Prefix"].ToString(),
                            FirstName = dr["FirstName"].ToString(),
                            MI = dr["MI"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Suffix = dr["Suffix"].ToString(),
                            HouseNo = dr["HouseNo"].ToString(),
                            StreetName = dr["StreetName"].ToString(),
                            SuiteNo = dr["SuiteNo"].ToString(),
                            City = dr["City"].ToString(),
                            State = dr["State"].ToString(),
                            PostalCode = dr["PostalCode"].ToString(),
                            Country = dr["Country"].ToString(),

                            HomePhoneNo = dr["HomePhoneNo"].ToString(),
                            BusinessPhoneNo = dr["BusinessPhoneNo"].ToString(),
                            CellPhoneNo = dr["CellPhoneNo"].ToString(),

                            EmailAddress = dr["EmailAddress"].ToString()
                        },
                        UnbilledAmount = Convert.ToDecimal(dr["UnbilledAmount"])
                    });
                }
            }

            return unbilledCustomers;
        }

        public static ObservableCollection<Invoice> GetInvoices(int invoiceID, int customerID, int invoceBatchID, bool withTrips = false)
        {
            using (InvoiceDB invDb = new InvoiceDB(null, null))
            {
                return invDb.Get(invoiceID, customerID, invoceBatchID, withTrips);
            }
        }

        public static ObservableCollection<UnbilledCustomer> GetUnbilledCustomersByDate(DateTime startDate, DateTime cutOffDate)
        {
            using (InvoiceDB invDB = new InvoiceDB(null, null))
            {
                return invDB.GetUnbilledCustomers(startDate, cutOffDate);
            }
        }

        private bool ProcessInvoices(
            string delimitedCustomerToInvoiceID,
            DateTime invoiceStartDate,
            DateTime cutOffDate,
            bool IncludeTripsWithZeroTotalPrice,
            out int invoiceBatchID
            )
        {
            string serverMessage;
            SqlParameter invoiceBatcheIDparam = new SqlParameter("@InvoiceBatchID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecAffected = this.ExecuteNonQuery("AddInvoiceBatches", new SqlParameter[]
            {
                 new SqlParameter("@InvoiceStartDate", invoiceStartDate),
                new SqlParameter("@CutOffDate", cutOffDate),
                new SqlParameter("@IncludeTripsWithZeroTotalPrice",IncludeTripsWithZeroTotalPrice),
                invoiceBatcheIDparam,
                new SqlParameter("@CustomerID",delimitedCustomerToInvoiceID),
                new SqlParameter("SessionID", Database.SessionID)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecAffected > 0)
            {
                invoiceBatchID = Convert.ToInt32(invoiceBatcheIDparam.Value);
                return true;
            }
            else
            {
                invoiceBatchID = 0;
                return false;
            }
        }



        public static decimal GetPreviousTotalDue(int customerID, DateTime tillDate)
        {

            decimal PreviousTotalDue = 0;


            using (var db = new Database(null, null))
            {

                PreviousTotalDue = Convert.ToDecimal(db.ExecuteScalar("SELECT dbo.GetCustomerPreviousBalance(" + customerID + ",'" + tillDate + "')", new SqlParameter[] { }, System.Data.CommandType.Text));

            }


            return PreviousTotalDue;

        }

        public List<Invoice> GetPaymentsFromThisBillingCycle(int customerID, DateTime tillDate)
        {
            List<Invoice> paymentsFromThisBillingCycle = new List<Invoice>();
            using (SqlDataReader dr = this.GetReader("GetPaymentsFromThisBillingCycle", new SqlParameter[]
            {
              new SqlParameter("@CustomerID",customerID),
              new SqlParameter("@TillDate", tillDate),

            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var payment = new Invoice()
                    {
                        //   pa
                    };


                    paymentsFromThisBillingCycle.Add(payment);
                }





            }
            return paymentsFromThisBillingCycle;
        }
        public static bool ProcessInvoicesForUnbilledTrips(string delimitedCustomerToInvoiceID, DateTime invoiceStartDate, DateTime cutOffDate, bool includeTripsWithZeroTotalPrice, out int invoiceBatchID)
        {
            using (InvoiceDB invDB = new InvoiceDB(null, null))
            {
                return invDB.ProcessInvoices(delimitedCustomerToInvoiceID, invoiceStartDate, cutOffDate, includeTripsWithZeroTotalPrice, out invoiceBatchID);
            }
        }

        public bool Delete(int invoiceID)
        {
            string serverMessage;

            int noOfRecAffected = this.ExecuteNonQuery("DeleteInvoice", new SqlParameter[]
            {
                new SqlParameter("@InvoiceID", invoiceID),
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecAffected > 0)
                return true;
            return false;

        }
        public static bool DeleteInvoice(int invoiceID)
        {
            using (InvoiceDB invDB = new InvoiceDB(null, null))
            {
                return invDB.Delete(invoiceID);
            }

        }




    }
}



