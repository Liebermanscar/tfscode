﻿using System;
using System.Configuration;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.IO;
using System.Linq;

namespace CCProc
{

    public class ProcessCC
    {
        public static string SourceKey = "";
        public static string Pin = "";

        usaEPay.ueSoapServerPortTypeClient client;
        usaEPay.ueSecurityToken token;

        public ProcessCC()
        {
            try
            {
                //client = new usaEPay.ueSoapServerPortTypeClient();
                //token = new usaEPay.ueSecurityToken();

                //token.SourceKey = ProcessCC.SourceKey;
                //token.ClientIP = this.GetPublicIPAddress();

                //usaEPay.ueHash hash = new usaEPay.ueHash();
                //hash.Type = "md5";  // Type of encryption 
                //hash.Seed = Guid.NewGuid().ToString();  // unique encryption seed

                //string prehashvalue = string.Concat(token.SourceKey, hash.Seed, ProcessCC.Pin);  // combine data into single string
                //hash.HashValue = GenerateHash(prehashvalue); // generate hash

                //token.PinHash = hash;	// add hash value to token
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private usaEPay.ueSecurityToken getToken()
        {
            // Instantiate token object
            usaEPay.ueSecurityToken token = new usaEPay.ueSecurityToken();

            // SourceKey and Pin (created in merchant console)
            token.SourceKey = ProcessCC.SourceKey;
            string pin = ProcessCC.Pin;

            // IP address of end user (if applicable)
            token.ClientIP = this.GetPublicIPAddress();

            // Instantiate Hash
            usaEPay.ueHash hash = new usaEPay.ueHash();
            hash.Type = "md5";  // Type of encryption 
            hash.Seed = Guid.NewGuid().ToString();  // unique encryption seed

            // Assemble string and hash
            string prehashvalue = string.Concat(token.SourceKey, hash.Seed, pin);
            hash.HashValue = GenerateHash(prehashvalue);

            // Add hash to token
            token.PinHash = hash;

            return token;
        }

        private string GenerateHash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private string GetLocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        private string GetPublicIPAddress()
        {
            string direction;
            WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
            WebResponse response = request.GetResponse();
            StreamReader stream = new StreamReader(response.GetResponseStream());
            direction = stream.ReadToEnd();
            stream.Close();
            response.Close();

            //Search for the ip in the html
            int first = direction.IndexOf("Address: ") + 9;
            int last = direction.LastIndexOf("</body>");
            direction = direction.Substring(first, last - first);

            return direction;
        }

        public TransactionResponse ProcessCCTransaction(CCInfo ccInfo, CCTransactionTypes ccTransType, double amount, string description, string clerk)
        {
            string command = "cc:";
            switch (ccTransType)
            {
                case CCTransactionTypes.Sale:
                    command += "sale";
                    break;
                case CCTransactionTypes.AuthoOnly:
                    command += "authonly";
                    break;
                case CCTransactionTypes.Capture:
                    command += "capture";
                    break;
                case CCTransactionTypes.Credit:
                    command += "credit";
                    break;
                case CCTransactionTypes.PostAuth:
                    command += "postauth";
                    break;
                case CCTransactionTypes.Void:
                    command += "void";
                    break;
                case CCTransactionTypes.Refund:
                    command += "refund";
                    break;
                case CCTransactionTypes.CreditVoid:
                    command = "voidrefund";
                    break;
                default:
                    break;
            }


            try
            {
                //usaEPay.TransactionRequestObject tran = new usaEPay.TransactionRequestObject();



                //tran.Command = command;
                //tran.Details = new usaEPay.TransactionDetail();
                //tran.Details.Amount = amount;
                //tran.Details.AmountSpecified = true;
                ////tran.Details.Invoice = "1234";
                //tran.Details.Description = description;
                //tran.Details.Clerk = clerk;

                //tran.CreditCardData = new usaEPay.CreditCardData();
                //tran.AccountHolder = ccInfo.NameOnCard;
                //tran.CreditCardData.CardNumber = ccInfo.CCNumber;
                //tran.CreditCardData.CardExpiration = ccInfo.ExpDate;
                //tran.CreditCardData.CardCode = ccInfo.CSC;                
                //tran.CreditCardData.AvsZip = ccInfo.PostalCode;
                //tran.CreditCardData.AvsStreet = ccInfo.StreetAddress;

                //usaEPay.TransactionResponse response = new usaEPay.TransactionResponse();


                //response = client.runTransaction(token, tran);
                //return new TransactionResponse()
                //{
                //    AuthorizationCode = response.AuthCode,
                //    ResponseDescription = response.Error,
                //    ErrorCode = response.ErrorCode,
                //    Result = response.Result,
                //    TransactionNo = response.RefNum
                //};


                System.Collections.Specialized.NameValueCollection MyPost = new System.Collections.Specialized.NameValueCollection();
                MyPost.Add("xkey", "15097786a97841318c691fc6340574a9Liebermans");
                MyPost.Add("xVersion", "4.5.3");
                MyPost.Add("xSoftwareName", "CarDispatchSystem");
                MyPost.Add("xSoftwareVersion", "1.0");
                MyPost.Add("xCommand", command);
                MyPost.Add("xCardNum", ccInfo.CCNumber);
                MyPost.Add("xExp", ccInfo.ExpDate);
                MyPost.Add("xCVV", ccInfo.CSC);
                MyPost.Add("xName", ccInfo.NameOnCard);
                MyPost.Add("xAmount", amount.ToString());
                MyPost.Add("xdescription", description);
                MyPost.Add("xCustom01", clerk);
                MyPost.Add("xZip", ccInfo.PostalCode);
                //MyPost.Add("xStreet",  ccInfo.StreetAddress + ""); - original code, changed to below on 6/11/2015
                string xStreet = (ccInfo.HouseNo == null || ccInfo.HouseNo == string.Empty) ? ccInfo.StreetAddress : ccInfo.HouseNo;
                if (xStreet == null) { xStreet = string.Empty; }
                MyPost.Add("xStreet", xStreet.Trim());
                System.Net.WebClient MyClient = new System.Net.WebClient();
                string MyResponse = System.Text.UTF8Encoding.ASCII.GetString(MyClient.UploadValues("https://x1.cardknox.com/gateway", MyPost));
                // Response
                System.Collections.Specialized.NameValueCollection MyResponseData = ParseQueryString(MyResponse);
                string MyResult = "";
                if (MyResponseData.AllKeys.Contains("xResult"))
                    MyResult = MyResponseData["xResult"];
                string MyStatus = "";
                if (MyResponseData.AllKeys.Contains("xStatus"))
                    MyStatus = MyResponseData["xStatus"];
                string MyError = "";
                if (MyResponseData.AllKeys.Contains("xError"))
                    MyError = MyResponseData["xError"];
                string MyRefNum = "";
                if (MyResponseData.AllKeys.Contains("xRefNum"))
                    MyRefNum = MyResponseData["xRefNum"];
                string MyAuthCode = "";
                if (MyResponseData.AllKeys.Contains("xAuthCode"))
                    MyAuthCode = MyResponseData["xAuthCode"];


                return new TransactionResponse()
                {
                    AuthorizationCode = MyAuthCode,
                    ResponseDescription = MyStatus + " - " + MyError + " (" + MyRefNum.ToString() + ")",
                    ErrorCode = "00000",
                    Result = MyStatus,
                    TransactionNo = MyRefNum
                };


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TransactionResponse ProcessPayTraceTransaction(CCInfo ccInfo, CCTransactionTypes ccTransType, double amount, string description, string clerk)
        {
            try
            {
                string sPayTraceURL = ConfigurationManager.AppSettings["PayTraceUrl"].ToString();
                string sPayUserName = ConfigurationManager.AppSettings["PayTraceUserName"].ToString();
                string sPayPassword = ConfigurationManager.AppSettings["PayTracePassword"].ToString();
                string IsLive = ConfigurationManager.AppSettings["IsAccount"].ToString();
                string userName = "UN~" + sPayUserName;
                string password = "PSWD~" + sPayPassword;
                string terms = "TERMS~Y";
                string test = string.Empty;
                if (IsLive.ToLower() == "no")
                {
                    test = "TEST~Y";
                }
                else
                {
                    test = "TEST~N";
                }
                string method = "METHOD~ProcessTranx";
                string tranxtype = "TRANXTYPE~Sale";
                string cC = "CC~" + ccInfo.CCNumber;
                string csc = "CSC~" + ccInfo.CSC;
                string dt = ccInfo.ExpDate;
                string month = string.Empty;
                string year = string.Empty;
                if (dt.Length > 3)
                {
                    month = dt.Remove(2, 2);
                    year = dt.Remove(0, 2);
                }
                else
                {
                    month = dt.Remove(1, 2);
                    if (month.Length == 1)
                        month = "0" + month;
                    year = dt.Remove(0, 1);
                }
                string expmnth = "EXPMNTH~" + month;
                string expyr = "EXPYR~" + year;
                if (Convert.ToString(amount) == "0" || string.IsNullOrEmpty(Convert.ToString(amount)) || Convert.ToString(amount) == "0.00")
                {
                    amount = -1.00;
                }
                string amount1 = "AMOUNT~" + amount.ToString();
                string bname = "BNAME~" + ccInfo.NameOnCard;
                string xStreet = (ccInfo.HouseNo == null || ccInfo.HouseNo == string.Empty) ? ccInfo.StreetAddress : ccInfo.HouseNo;
                if (xStreet == null) { xStreet = string.Empty; }
                string baddress = "BADDRESS~" + xStreet;
                string bzip = "BZIP~" + ccInfo.PostalCode;
                //String sRequest = "PARMLIST=" + HttpUtility.UrlEncode("" + userName + "|" + password + "|" + terms + "|" + method + "|" +
                //"" + tranxtype + "|" + cC + "|" + csc + "|" + expmnth + "|" + expyr + "|" + amount1 + "|" + bname + "|" + baddress + "|" + bzip + "|");
                String sRequest = "PARMLIST=" + HtmlUtility.GetUrlEncode("" + userName + "|" + password + "|" + terms + "|" + method + "|" + test + "|" +
               "" + tranxtype + "|" + cC + "|" + csc + "|" + expmnth + "|" + expyr + "|" + amount1 + "|" + bname + "|" + baddress + "|" + bzip + "|");

                //MyPost.Add("xStreet",  ccInfo.StreetAddress + ""); - original code, changed to below on 6/11/2015

                System.Net.WebClient myClient = new System.Net.WebClient();
                myClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                string myResponse = myClient.UploadString(sPayTraceURL, sRequest);
                // Response
                System.Collections.Specialized.NameValueCollection myResponseData = ParsePaymentResult(myResponse);
                string myResult = "";
                if (myResponseData.AllKeys.Contains("RESPONSE"))
                {
                    myResult = myResponseData["RESPONSE"];
                    myResult = myResult.Remove(myResult.LastIndexOf("."));
                }
                string myStatus = "";
                if (myResponseData.AllKeys.Contains("APPMSG"))
                {
                    myStatus = myResponseData["APPMSG"];
                    //  NO  MATCH      - Approved and completed
                    if (myStatus.Contains("-"))
                    {
                        myStatus = myStatus.Remove(0, myStatus.IndexOf("-") + 1).Trim();
                    }
                }
                string myError = "";
                if (myResponseData.AllKeys.Contains("ERROR"))
                {
                    myError = myResponseData["ERROR"];
                    myError = myError.Remove(myError.LastIndexOf("."));
                }
                string myRefNum = "";
                if (myResponseData.AllKeys.Contains("TRANSACTIONID"))
                {
                    myRefNum = myResponseData["TRANSACTIONID"];
                }
                string myAuthCode = "";
                if (myResponseData.AllKeys.Contains("APPCODE"))
                    myAuthCode = myResponseData["APPCODE"];


                if (myResult.Contains("."))
                    myResult = myResult.Remove(0, myResult.LastIndexOf(".") + 1);
                string[] error = myError.Split('.');
                if (myError.Contains("."))
                    myError = error[1];
                string responseDescription = string.Empty;
                string errorCode = string.Empty;
                if (string.IsNullOrEmpty(myError.ToString()))
                {
                    responseDescription = myStatus + " (" + myRefNum.ToString() + ")";
                    errorCode = "00000";
                }
                else
                {
                    responseDescription = myError;
                    errorCode = error[0].ToString();
                }
                return new TransactionResponse()
                {
                    AuthorizationCode = myAuthCode,
                    ResponseDescription = responseDescription,// myStatus + " - " + myError + " (" + myRefNum.ToString() + ")",
                    ErrorCode = errorCode,
                    Result = myStatus,
                    TransactionNo = myRefNum
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        System.Collections.Specialized.NameValueCollection ParseQueryString(string queryString)
        {
            System.Collections.Specialized.NameValueCollection nvc = new System.Collections.Specialized.NameValueCollection();

            if (queryString != null)
            {
                foreach (string element in queryString.Split('&'))
                {
                    string[] pair = element.Split('=');
                    if (pair.Length == 2)
                    {
                        string name = Uri.UnescapeDataString(pair[0]);
                        string value = Uri.UnescapeDataString(pair[1]);
                        nvc.Add(name, value);
                    }
                }
            }

            return nvc;
        }
        System.Collections.Specialized.NameValueCollection ParsePaymentResult(string queryString)
        {
            System.Collections.Specialized.NameValueCollection nvc = new System.Collections.Specialized.NameValueCollection();

            if (queryString != null)
            {
                foreach (string element in queryString.Split('|'))
                {
                    string[] pair = element.Split('~');
                    if (pair.Length == 2)
                    {
                        string name = Uri.UnescapeDataString(pair[0]);
                        string value = Uri.UnescapeDataString(pair[1]);
                        nvc.Add(name, value);
                    }
                }
            }

            return nvc;
        }
    }

    public class CCInfo
    {
        string _cCNumber;

        public string CCNumber
        {
            get { return _cCNumber; }
            set { _cCNumber = value; }
        }
        string _expDate;

        public string ExpDate
        {
            get { return _expDate; }
            set { _expDate = value; }
        }
        string _nameOnCard;

        public string NameOnCard
        {
            get { return _nameOnCard; }
            set { _nameOnCard = value; }
        }
        string _houseNo;

        public string HouseNo
        {
            get { return _houseNo; }
            set { _houseNo = value; }
        }
        string _streetAddress;

        public string StreetAddress
        {
            get { return _streetAddress; }
            set { _streetAddress = value; }
        }
        string _suiteNo;

        public string SuiteNo
        {
            get { return _suiteNo; }
            set { _suiteNo = value; }
        }
        string _city;

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        string _state;

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        string _postalCode;

        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        string _cSC;

        public string CSC
        {
            get { return _cSC; }
            set { _cSC = value; }
        }
    }

    public class TransactionResponse
    {
        public string TransactionNo { get; set; }
        public string Result { get; set; }
        public string AuthorizationCode { get; set; }
        public string ErrorCode { get; set; }
        public string ResponseDescription { get; set; }

        public override string ToString()
        {
            return string.Format("Result: {0}\nAuthorization Code: {1}\nDescription: {2}", this.Result, this.AuthorizationCode, this.ResponseDescription);
        }
    }

    public enum CCTransactionTypes
    {
        Sale = 1,
        AuthoOnly,
        Capture,
        Credit,
        PostAuth,
        Void,
        Refund,
        CreditVoid
    }

    public enum ACHTransactionTypes
    {
        Sale = 1,
        Credit
    }
}
