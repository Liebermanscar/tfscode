﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlTypes;

namespace DAL
{
    public class DriverPaymentDB : Database
    {
        public DriverPaymentDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public bool Add(DriverPayment driverPayment, bool addBonusPayment)
        {
            string serverMessage;

            var dailyEnvelopesID = driverPayment.EnvelopesOpen.Where(oe => oe.Selected).Select(oe => oe.DailyEnvelopeID).ToList();
            string delimitedDailyEnvelopesID = string.Join(",", dailyEnvelopesID.ToArray());
            var driverExpenseID = driverPayment.DriverExpenses.Where(de => de.Selected).Select(de => de.DriverExpenseID).ToList();
            string delimitedDriverExpensesID = string.Join(",", driverExpenseID);
            var tripID = driverPayment.ChargeToDriverTrips.Where(ctdt => ctdt.Selected).Select(ctdt => ctdt.TripID).ToList();
            string delimitedTripsID = string.Join(",", tripID);

            SqlParameter outParam = new SqlParameter("@DriverPaymentID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            SqlParameter[] param = new SqlParameter[]
              {
             new SqlParameter("@DriverID",driverPayment.DriverID),
             new SqlParameter("@DriverPaymentDate",driverPayment.DateDriverPayment),
             new SqlParameter("@DatePaidTill",driverPayment.DatePaidTill),
             new SqlParameter("@AmountForDriver", driverPayment.AmountForDriver),
             new SqlParameter("@DailyEnvelopesID", delimitedDailyEnvelopesID),
             new SqlParameter("@DriverExpenseID", delimitedDriverExpensesID),
             new SqlParameter("@PaidFromChecking", driverPayment.PaidFromChecking),
             new SqlParameter("@ReferenceNumber",driverPayment.ReferenceNumber),
             new SqlParameter("@TripsID",delimitedTripsID),
             new SqlParameter("@SessionIDAdded", Database.SessionID),
             new SqlParameter("@BonusAmount", driverPayment.BonusAmount),
             new SqlParameter("@AddUnpaidBonus", addBonusPayment),
                outParam
              };

            int noOfAffectedRecords = this.ExecuteNonQuery("AddDriverPayment", param, System.Data.CommandType.StoredProcedure, out serverMessage);
            if (noOfAffectedRecords > 0)
            {
                driverPayment.DriverPaymentID = Convert.ToInt32(outParam.Value);
            }

            return noOfAffectedRecords > 0;

        }
        public static bool AddDriverPayment(DriverPayment driverPayment, bool addBonusPayment)
        {
            using (DriverPaymentDB dpDB = new DriverPaymentDB(null, null))
            {
                return dpDB.Add(driverPayment, addBonusPayment);
            }
        }

        public bool Edit(DriverPayment driverPayment, bool removeBonusPayment)
        {
            string serverMessage;

            SqlParameter[] param = new SqlParameter[]
             {
             new SqlParameter("@DriverPaymentId",driverPayment.DriverPaymentID),
             new SqlParameter("@AmountPaidForDriver", driverPayment.AmountForDriver),
             new SqlParameter("@BonusAmount", driverPayment.BonusAmount),
             new SqlParameter("@BonusPaidWithID", driverPayment.BonusPaidWithID),
             new SqlParameter("@BonusPayment", driverPayment.BonusPayment),
             new SqlParameter("@RemoveBonusPayment", removeBonusPayment)
              };

            int noOfAffectedRecords = this.ExecuteNonQuery("UpdateDriverPayment", param, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfAffectedRecords > 0;
        }

        public static bool EditDriverPayment(DriverPayment driverPayment, bool removeBonusPayment)
        {
            using (DriverPaymentDB dpDB = new DriverPaymentDB(null, null))
            {
                return dpDB.Edit(driverPayment, removeBonusPayment);
            }
        }

        public bool Delete(DriverPayment driverPayment)
        {
            string serverMsg;
            int noOfRecords = this.ExecuteNonQuery("DeleteDriverPayment", new SqlParameter[]
            {
               new SqlParameter("@DriverPaymentId", driverPayment.DriverPaymentID)
            }, System.Data.CommandType.StoredProcedure, out serverMsg);

            return noOfRecords > 0;
        }

        public static bool DeleteDriverPayment(DriverPayment driverPayment)
        {
            using (DriverPaymentDB dpDB = new DriverPaymentDB(null, null))
            {
                return dpDB.Delete(driverPayment);
            }
        }

        public ObservableCollection<DriverPayment> Get(int driverPaymentID, int driverID, DateTime? dateDriverPayment, DateTime datePaidTill)
        {
            var driverPayments = new ObservableCollection<DriverPayment>();
            using (SqlDataReader dr = this.GetReader("GetDriverPayment", new SqlParameter[] {
          new SqlParameter("@DriverPaymentId", driverPaymentID),
            new SqlParameter("@DriverPaymentDate", dateDriverPayment == null ? SqlDateTime.Null : dateDriverPayment.Value)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var driverPayment = new DriverPayment()
                    {
                        StaticData = true,
                        DriverPaymentID = Convert.ToInt32(dr["DriverPaymentID"]),
                        AmountForDriver = Convert.ToDecimal(dr["AmountPaidForDriver"]),
                        DriverID = Convert.ToInt32(dr["DriverID"]),
                        DateDriverPayment = Convert.ToDateTime(dr["DriverPaymentDate"]),
                        PaidFromChecking = Convert.ToBoolean(dr["PaidFromChecking"]),
                        ReferenceNumber = dr["ReferenceNumber"].ToString(),
                        BonusAmount = Convert.ToDecimal(dr["BonusAmount"]),
                        TotalUnpaidBonus = Convert.ToDecimal(dr["TotalUnpaidBonus"]),
                        BonusPayment = Convert.ToDecimal(dr["BonusPayment"]),
                        BonusPaidWithID = Convert.ToInt32(dr["BonusPaidWithID"])
                    };
                    if (dr["DriverCode"] != DBNull.Value)
                        driverPayment.Driver = new Driver()
                        {
                            DriverCode = dr["DriverCode"].ToString(),
                            DriverFirstName = dr["DriverFirstName"].ToString(),
                            DriverLastName = dr["DriverLastName"].ToString(),
                        };
                    /*   if (dr.HasColumn("CashAmountBooked"))
                       {
                           if (dr["CashAmountBooked"] != DBNull.Value)
                           driverPayment.DailyEnvelope = new DailyEnvelope()
                           {
                               StaticData=true,
                               DateOfTrips = Convert.ToDateTime(dr["EnvelopeDate"]),
                               TripsIncome = Convert.ToDecimal(dr["TripsIncome"]),
                               CashAmountBooked = Convert.ToDecimal(dr["CashAmountBooked"]),
                               AmountSubmitted = Convert.ToDecimal(dr["AmountSubmitted"]),
                               GasPaid = Convert.ToDecimal(dr["GasPaid"]),
                               OwedForDriverAmount=Convert.ToDecimal(dr["DriverCommisionAmount"]),
                               NoOfTrips = Convert.ToInt32(dr["TotalChargeTrips"]),
                           };
                       };*/
                    /*  if (dr.HasColumn("DateOfExpense"))
                      {
                          if (dr["DateOfExpense"] != DBNull.Value)
                              driverPayment.DriverExpense = new DriverExpense()
                              {
                                 DateOfExpense =  Convert.ToDateTime(dr["DateOfExpense"]),
                                 AmountOfExpense = Convert.ToDecimal(dr["AmountOfExpense"]),
                                 DetailsOfExpense = dr["DetailsOfExpense"].ToString(),
                              };
                      };*/
                    driverPayments.Add(driverPayment);
                };
            }
            return driverPayments;
        }






        //      public ObservableCollection<DriverPayment>Get(int driverPaymentID,int driverID,DateTime dateDriverPayment,DateTime datePaidTill)
        //       {
        //           var driverPayments = new ObservableCollection<DriverPayment>();
        //           using (SqlDataReader dr = this.GetReader("GetDriverPayment", new SqlParameter[] { 
        //            new SqlParameter("@DriverPaymentId", driverPaymentID),
        //            new SqlParameter("@DriverId", driverID),
        //            new SqlParameter("@DriverPaymentDate", dateDriverPayment)
        //            }, System.Data.CommandType.StoredProcedure))
        //           {

        //               while (dr.Read())
        //               {
        //                   driverPayments.Add(new DriverPayment()
        //                   {
        //                       DriverPaymentID = Convert.ToInt32(dr["DriverPaymentID"]),
        //                       AmountForDriver = Convert.ToDecimal(dr["AmountPaidForDriver"]),
        //                       DriverID = Convert.ToInt32(dr["DriverID"]),
        //                       DateDriverPayment = Convert.ToDateTime(dr["DriverPaymentDate"]),
        //                       Driver = new Driver()
        //                       {
        //                           DriverCode = dr["DriverCode"].ToString(),
        //                       }/*,
        //                       DailyEnvelope = new DailyEnvelope()
        //                       {
        //                           CashAmountBooked = Convert.ToDecimal(dr["CashAmountBooked"]),
        //                           AmountSubmitted = Convert.ToDecimal(dr["AmountSubmitted"]),
        //                         //  GasPaid = Convert.ToDecimal(dr[""]),
        //                           NoOfTrips = Convert.ToInt32(dr["TotalChargeTrips"]),

        //                       },
        //                      // DatePaidTill = Convert.ToDateTime(dr[""]),
        //                    //   DriverExpenses = new DriverExpense() { }
        //*/
        //                   });
        //               }
        //               return driverPayments;
        //           }
        //       }
        public static ObservableCollection<DriverPayment> GetDriverPayment(int driverPaymentID, int driverID, DateTime? dateDriverPayment, DateTime datePaidTill)
        {
            using (DriverPaymentDB driverPaymentDB = new DriverPaymentDB(null, null))
            {
                return driverPaymentDB.Get(driverPaymentID, driverID, dateDriverPayment, datePaidTill);
            }
        }




        public static decimal GetCashTotal()

        {
            decimal cashOnHand = 0;
            using (var db = new Database(null, null))
            {
                //   cashOnHand = Convert.ToDecimal(db.ExecuteScalar("GetCashTotal", new SqlParameter[] { }, System.Data.CommandType.StoredProcedure));//Text));
                cashOnHand = Convert.ToDecimal(db.ExecuteScalar("SELECT dbo.GetCashTotal()", new SqlParameter[] { }, System.Data.CommandType.Text));//Text));

                // cashOnHand=db.
            }

            //SqlDataReader dr = this.ExecuteScalar("GetCashTotal", "", System.Data.SqlClient.Sq))

            return cashOnHand;
        }

    }
}
