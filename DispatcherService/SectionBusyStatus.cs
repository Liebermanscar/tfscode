﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class SectionCarsAvailability : BO
    {
        [DataMember]
        public string SectionName { get; set; }
        
        CarsAvailabilityTypes _availability;

        [DataMember]
        public CarsAvailabilityTypes Availability
        {
            get { return _availability; }
            set { _availability = value; this.OnPropertyChanged("Availability");}
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }

    public enum CarsAvailabilityTypes
	{
        [Description("Available")]
        Available, //green
        [Description("Not Available")]
        NotAvailable, //red
        [Description("Will be Avail.")]
        WillBeAvailabe //yellow
	}
}
