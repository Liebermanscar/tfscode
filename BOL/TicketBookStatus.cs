﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace BOL
{
    public class TicketBookStatus : BO
    {

        int _ticketBookStatusID;

        public int TicketBookStatusID
        {
            get { return _ticketBookStatusID; }
            set { _ticketBookStatusID = value; }
        }
        int _bookNumber;

        public int BookNumber
        {
            get { return _bookNumber; }
            set { _bookNumber = value; }
        }
        int _ticketBookID;

        public int TicketBookID
        {
            get { return _ticketBookID; }
            set { _ticketBookID = value; }
        }
        TicketBookStatusOptions _bookStatus;

        public TicketBookStatusOptions BookStatus
        {
            get { return _bookStatus; }
            set { _bookStatus = value; }
        }

        TicketBook _ticketBookType;

        public TicketBook TicketBookType
        {
            get { return _ticketBookType; }
            set { _ticketBookType = value; this.TicketBookID = value == null ? 0 : value.TicketBookID; }
        }
        int _lastSessionID;

        public int LastSessionID
        {
            get { return _lastSessionID; }
            set { _lastSessionID = value; }
        }
        int _inPossessionID;

        public int InPossessionID
        {
            get { return _inPossessionID; }
            set { _inPossessionID = value; }
        }
        int _dailyEnvelopeIdSold;

        public int DailyEnvelopeIdSold
        {
            get { return _dailyEnvelopeIdSold; }
            set { _dailyEnvelopeIdSold = value; }
        }
        int _customerIdSold;

        public int CustomerIdSold
        {
            get { return _customerIdSold; }
            set { _customerIdSold = value; this.OnPropertyChanged("CustomerIdSold"); }
        }
        Customer _customerSoldTo;

        public Customer CustomerSoldTo
        {
            get { return _customerSoldTo; }
            set { _customerSoldTo = value; this.CustomerIdSold = value == null ? 0 : value.CustomerID; }
        }
        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }

        string _userLogged;

        public string UserLogged
        {
            get { return _userLogged; }
            set { _userLogged = value; }
        }
        DateTime _dateLogged;

        public DateTime DateLogged
        {
            get { return _dateLogged; }
            set { _dateLogged = value; }
        }
        string _givenTo;

        public string GivenTo
        {
            get { return _givenTo; }
            set { _givenTo = value; this.OnPropertyChanged("GivenTo"); }
        }
        string _soldToInfo;

        public string SoldToInfo
        {
            get { return _soldToInfo; }
            set { _soldToInfo = value; this.OnPropertyChanged("SoldToInfo"); }
        }
        decimal _priceSold;

        public decimal PriceSold
        {
            get { return _priceSold; }
            set { _priceSold = value; }
        }

        BookDiscountTypes _discountType;

        public BookDiscountTypes TBDiscountType
        {
            get { return _discountType; }
            set { _discountType = value; }
        }

        public string DiscountDescription
        {
            get { return this.TBDiscountType.ToDescription(); }
        }

        string _ticketNote;

        public string TicketNote
        {
            get { return _ticketNote; }
            set { _ticketNote = value; }
        }

        int _paymentID;

        public int PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }

        bool _isPaidByCC;

        public bool IsPaidByCC
        {
            get { return _isPaidByCC; }
            set { _isPaidByCC = value; }
        }
    }

    public class GenerateTicketBookStatuses : BO
    {
        int _startBookNumber;
        public int StartBookNumber
        {
            get { return _startBookNumber; }
            set { _startBookNumber = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("AllowTransfer"); }
        }

        int _endBookNumber;
        public int EndBookNumber
        {
            get { return _endBookNumber; }
            set { _endBookNumber = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("AllowTransfer"); }
        }
        int _ticketBookID;
        public int TicketBookID
        {
            get { return _ticketBookID; }
            set { _ticketBookID = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("AllowTransfer"); }
        }
        TicketBookStatusOptions _BookStatus;
        public TicketBookStatusOptions BookStatus
        {
            get { return _BookStatus; }
            set
            {
                _BookStatus = value; this.OnPropertyChanged("PosessionsFilterd");
                this.OnPropertyChanged("AllowSave");
                this.OnPropertyChanged("AllowTransfer");
                this.OnPropertyChanged("EnableCustomerInfo");
                this.Customer = null; this.SoldToInfo = null;
            }
        }
        int _inPossessionID;
        public int InPossessionID

        {
            get { return _inPossessionID; }
            set { _inPossessionID = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("AllowTransfer"); }
        }

        Customer _customer;

        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value; this.OnPropertyChanged("SoldToDisplay"); this.OnPropertyChanged("AllowSave"); }
        }
        string _soldToInfo;

        public string SoldToInfo
        {
            get { return _soldToInfo; }
            set { _soldToInfo = value; this.OnPropertyChanged("SoldToDisplay"); this.OnPropertyChanged("AllowSave"); }
        }

        public string SoldToDisplay
        {
            get { return this.Customer == null ? this.SoldToInfo : this.Customer.ToString(); }

        }
        public ObservableCollection<TicketBookStatus> TicketBookStatuses { get; set; }

        public void GenarateBooks()
        {
            this.TicketBookStatuses = new ObservableCollection<TicketBookStatus>();

            for (int i = this.StartBookNumber; i <= this.EndBookNumber; i++)
            {
                TicketBookStatus ticketBookStatus = new TicketBookStatus();
                ticketBookStatus.BookNumber = i;
                ticketBookStatus.TicketBookID = this.TicketBookID;
                ticketBookStatus.BookStatus = this.BookStatus;
                ticketBookStatus.InPossessionID = this.InPossessionID;
                ticketBookStatus.CustomerIdSold = this.Customer == null ? 0 : this.Customer.CustomerID;
                ticketBookStatus.SoldToInfo = this.SoldToInfo;
                ticketBookStatus.PriceSold = this.GeneratedPriceSold;
                ticketBookStatus.TBDiscountType = this.TBDiscountType;
                ticketBookStatus.TicketNote = this.TBNotes;
                ticketBookStatus.PaymentID = this.PaymentID;
                TicketBookStatuses.Add(ticketBookStatus);
            }

        }

        public bool AllowTransfer
        {
            get
            {
                return this.StartBookNumber > 0
                    && this.EndBookNumber >= this.StartBookNumber
                    && this.TicketBookID > 0
                    && this.InPossessionID > 0
                    && this.BookStatus == TicketBookStatusOptions.Office;
            }
        }

        public bool EnableCustomerInfo
        {
            get
            {
                return this.BookStatus == TicketBookStatusOptions.Sold;
            }
        }

        public override bool AllowSave
        {
            get
            {
                return this.StartBookNumber > 0
                  && this.EndBookNumber >= this.StartBookNumber
                  && this.TicketBookID > 0
                  && this.BookStatus > 0
                  && ((this.BookStatus == TicketBookStatusOptions.Stock || this.InPossessionID > 0) ||
                  (this.BookStatus == TicketBookStatusOptions.Sold && (this.SoldToDisplay != null) && (this.SoldToDisplay != "")));
            }
        }


        ObservableCollection<Posession> _posessions;

        public ObservableCollection<Posession> Posessions
        {
            get { return _posessions; }
            set { _posessions = value; }
        }


        public ObservableCollection<Posession> PosessionsFilterd
        {
            get
            {
                if (this.BookStatus == TicketBookStatusOptions.Office || this.BookStatus == TicketBookStatusOptions.Driver)
                    return new ObservableCollection<Posession>(this.Posessions.Where(psn => psn.TicketBookStatusType == this.BookStatus));
                else
                    return new ObservableCollection<Posession>();
            }

        }
        decimal _generatedPriceSold;

        public decimal GeneratedPriceSold
        {
            get { return _generatedPriceSold; }
            set { _generatedPriceSold = value; this.OnPropertyChanged("GeneratedPriceSold"); }
        }

        BookDiscountTypes _tBDiscount;

        public BookDiscountTypes TBDiscountType
        {
            get { return _tBDiscount; }
            set { _tBDiscount = value; this.OnPropertyChanged("TBDiscountType"); }
        }

        string _tBNotes;

        public string TBNotes
        {
            get { return _tBNotes; }
            set { _tBNotes = value; }
        }
        TicketBook _activeTicketBook;

        public TicketBook ActiveTicketBook
        {
            get { return _activeTicketBook; }
            set { _activeTicketBook = value; }
        }

        int _paymentID;

        public int PaymentID
        {
            get { return _paymentID; }
            set { _paymentID = value; }
        }

        public int CountOfBooks
        {
            get
            {
                return this._endBookNumber - (this._startBookNumber - 1);
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return this._generatedPriceSold * (decimal)this.CountOfBooks;
            }
        }

    }

    public class TicketBookSummery : BO
    {

        TicketBook _ticketBookID;

        public TicketBook TicketBookID
        {
            get { return _ticketBookID; }
            set { _ticketBookID = value; }
        }
        int _bookNumber;

        public int BookNumber
        {
            get { return _bookNumber; }
            set { _bookNumber = value; }
        }
        int _customerIdSold;

        public int CustomerIdSold
        {
            get { return _customerIdSold; }
            set { _customerIdSold = value; }
        }
        string _soldToInfo;

        public string SoldToInfo
        {
            get { return _soldToInfo; }
            set { _soldToInfo = value; }
        }
        DateTime _soldDate;

        public DateTime SoldDate
        {
            get { return _soldDate; }
            set { _soldDate = value; }
        }
        int _countUsed;

        public int CountUsed
        {
            get { return _countUsed; }
            set { _countUsed = value; }
        }
        DateTime _dateFirstTicket;

        public DateTime DateFirstTicket
        {
            get { return _dateFirstTicket; }
            set { _dateFirstTicket = value; }
        }
        DateTime _dateLastTicket;

        public DateTime DateLastTicket
        {
            get { return _dateLastTicket; }
            set { _dateLastTicket = value; }
        }
        int _daysDiff;

        public int DaysDiff
        {
            get { return _daysDiff; }
            set { _daysDiff = value; }
        }
        string _customerName;

        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }

    public enum BookDiscountTypes
    {
        [Description("None")]
        None = 0,
        [Description("Long Distance")]
        LongDistance = 1,
        [Description("Member")]
        Member = 2,
        [Description("Referred Driver")]
        ReferredDriver = 3,
        Misc_1 = 4,
        Misc_2 = 5,
        Misc_3 = 6,
        Misc_4 = 7

    }


    public class Posession : BO
    {
        int _inPossessionID;
        public int InPossessionID
        {
            get { return _inPossessionID; }
            set { _inPossessionID = value; }
        }

        string _inPosessionName;
        public string InPosessionName
        {
            get { return _inPosessionName; }
            set { _inPosessionName = value; }
        }

        TicketBookStatusOptions _ticketBookStatusType;

        public TicketBookStatusOptions TicketBookStatusType
        {
            get { return _ticketBookStatusType; }
            set { _ticketBookStatusType = value; }
        }


        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }

    }
    public enum TicketBookStatusOptions
    {
        NotSpecified = 0,
        Stock = 1,
        Office = 2,
        Driver = 3,
        Sold = 4
    }
}

