﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for CustomersListPage.xaml
    /// </summary>
    public partial class CustomersListPage : Page, IDisplayTextBlock
    {
        public CustomersListPage()
        {
            InitializeComponent();
        }

        public CustomersListPage(List<Customer> customers) : this()
        {
            this.CustomersList = customers;
            this.FullCustomerList = customers;
        }
        public List<Customer> FullCustomerList = new List<Customer>();
        public List<Customer> CustomersList
        {
            get { return this.DataContext as List<Customer>; }
            set { this.DataContext = value; }

        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Customer List Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void CustomersDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (this.SelectedCustomer == null)
                    return;

                UIController.LoadCustomerInfoPage(
                    DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID, this.SelectedCustomer.CustomerID.ToString(), DataRequestTypes.FullData, false).FirstOrDefault()
                    );
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }

        public Customer SelectedCustomer
        {
            get { return this.CustomersDataGrid.SelectedItem as Customer; }
            set { this.CustomersDataGrid.SelectedItem = value; }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.SearchTextBox.Text))
                this.CustomersDataGrid.DataContext = this.FullCustomerList;
            else
            {
                this.CustomersList = new List<Customer>(
                    this.FullCustomerList.Where(
                    ft =>
                        ft.HomePhoneNo.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                        ||
                        ft.CustomersAddress.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                          ||
                          ft.CustomerName.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                            ||
                            ft.FirstName.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                              ||
                              ft.LastName.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                                ||
                                ft.BusinessPhoneNo.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                                 ||
                                 ft.CellPhoneNo.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                        ).ToList()
                    );
                this.CustomersDataGrid.DataContext = this.CustomersList;
            }
        }


    }
}
