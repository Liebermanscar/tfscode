﻿using System;

using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Android.App;

namespace OTR_App
{
	[Activity(Label = "Leibermans",  LaunchMode=Android.Content.PM.LaunchMode.SingleInstance, ScreenOrientation=Android.Content.PM.ScreenOrientation.Landscape)]
	public class MainActivity : AppCompatActivity
    {
        //int count = 1;
		TextView tbTablet;
		UpdateUIBroadcastReceiver uuiBR;
		Button btnLogout;

        protected override void OnCreate(Bundle bundle)
        {
            
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);
            //wireup controls
            btnLogout = FindViewById<Button> (Resource.Id.btnLogout);
			btnLogout.Click += (object sender, EventArgs e) => 
			{
				var builder = new AlertDialog.Builder(this);
				builder.SetTitle("Log Off?");
				builder.SetMessage("Are you sure you want to log off?");
				builder.SetPositiveButton("YES", (s, ex) => { UIController.Logout(); this.ShowLoginActiviety(); });
				builder.SetNegativeButton("NO", (s, ex) => { /* do something on Cancel click */ });
				builder.Create().Show();
			};

			UIController.MainActivityRef = this;

			//StartService(new Intent(this, typeof(ListenToDispatchingService)));
			//SetContentView(Resource.Layout.Main);

			if (!UIController.Modal.IsAuthenticated) {
				var intent = new Intent (this, typeof(LoginActivity));
				StartActivityForResult (intent, 0);
				//Finish ();
			}

			//wire up the broadcast receiver
			uuiBR = new UpdateUIBroadcastReceiver();
			uuiBR.mainActivity = this;
			LocalBroadcastManager.GetInstance(this).RegisterReceiver(uuiBR, new IntentFilter(UpdateUIBroadcastReceiver.UPDATE_MAIN_UI));
            
			//populate UI
			tbTablet = FindViewById<TextView>(Resource.Id.tvTabletName);
			var tbltName = UIController.GetTabletName();
			if (!string.IsNullOrWhiteSpace (tbltName))
				tbTablet.Text = tbltName;
        }

		public void ShowLoginActiviety()
		{
			var intent = new Intent (this, typeof(LoginActivity));
			StartActivityForResult (intent, 0);
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			if (resultCode == Result.Canceled) {
				var builder  = new AlertDialog.Builder (this);
				builder.SetTitle ("Login");
				builder.SetMessage ("Please Login in order to be able to use the Liebermans OTR App");
				builder.SetPositiveButton ("Ok", delegate {
					ShowLoginActiviety ();
				});

				//builder.Create ();

				builder.Show ();
				

			}
		}

		protected override void OnDestroy ()
		{
			LocalBroadcastManager.GetInstance(this).UnregisterReceiver(uuiBR);
			UIController.MainActivityRef = null;

			base.OnDestroy ();
		}

		public class UpdateUIBroadcastReceiver : BroadcastReceiver
		{
			public MainActivity mainActivity;
			public override void OnReceive (Context context, Intent intent)
			{
				mainActivity.tbTablet.Text = "fgdf";
				//Toast.MakeText ( mainActivity, "Boogy Main", ToastLength.Long );
			}

			public static string UPDATE_MAIN_UI = "update-main-ui";
		}
    }
}

