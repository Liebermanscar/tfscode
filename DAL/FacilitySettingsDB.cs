﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;
using System.Data;

namespace DAL
{
    public class FacilitySettingsDB : Database
    {
        public FacilitySettingsDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Edit(FacilitySetting facilitySetting)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateFacilitySettings", new SqlParameter[]
            {

               new SqlParameter("@SettingID",facilitySetting.SettingID),
               new SqlParameter("@SettingKey",facilitySetting.SettingKey),
               new SqlParameter("@SettingValue",facilitySetting.SettingValue),
                new SqlParameter("@SettingBit",facilitySetting.SettingBit),
                new SqlParameter("@SessionID",Database.SessionID),
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }
        public static bool EditFacilitySetting(FacilitySetting facilitySetting)
        {
            using (var facilitySettingsDB = new FacilitySettingsDB(null, null))
            {
                return facilitySettingsDB.Edit(facilitySetting);

            }
        }

        public FacilitySetting Get(string key, DateTime? forDate, bool activeOnly)
        {
            FacilitySetting facilitySettings = new FacilitySetting();

            using (var dr = this.GetReader("GetFacilitySettings", new SqlParameter[] {
               new SqlParameter("@SettingKey", key),
               new SqlParameter("@ForDate", forDate == null ? System.Data.SqlTypes.SqlDateTime.Null : forDate.Value),
               new SqlParameter("@ActiveOnly", activeOnly)
           }, CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    facilitySettings.SettingKey = Convert.ToString(dr["SettingKey"]);
                    facilitySettings.SettingNum = Convert.ToDecimal(dr["SettingNum"]);
                    facilitySettings.SettingValue = Convert.ToString(dr["SettingValue"]);
                    facilitySettings.SettingBit = Convert.ToBoolean(dr["SettingBit"]);
                    facilitySettings.SettingTime = TimeSpan.Parse(Convert.ToString(dr["SettingTime"]));
                    facilitySettings.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    facilitySettings.ToDate = Convert.ToDateTime(dr["ToDate"]);
                }

                return facilitySettings;
            }
        }

        public static FacilitySetting GetFacilitySettings(string key, DateTime? forDate = null, bool activeOnly = false)
        {
            using (var facilitySettingsDB = new FacilitySettingsDB(null, null))
            {
                return facilitySettingsDB.Get(key, forDate, activeOnly);
            }
        }
    }
}
