﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriversListPage.xaml
    /// </summary>
    public partial class DriversListPage : Page, IDisplayTextBlock
    {
        public DriversListPage()
        {
            InitializeComponent();
        }

        public DriversListPage(ObservableCollection<Driver> drivers) : this()
        {
            this.Drivers = drivers;
        }

        public ObservableCollection<Driver> Drivers
        {
            get { return this.DataContext as ObservableCollection<Driver>; }
            set { this.DataContext = value; }
        }

        public Driver SelectedDriver { get { return this.DriversDataGrid.SelectedItem as Driver; } }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedDriver == null)
                    return;

                Driver editedDriver = UIController.AddEditDriver(this.SelectedDriver.DriverID, ModificationTypes.Edit);
                if (editedDriver != null)
                {
                    this.Drivers.Remove(this.SelectedDriver);
                    this.Drivers.Add(editedDriver);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                Driver eaddedDriver = UIController.AddEditDriver(0, ModificationTypes.Add);
                if (eaddedDriver != null)
                {
                    this.Drivers.Add(eaddedDriver);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }



        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Drivers List Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
