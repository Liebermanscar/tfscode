﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CDS
{
    /// <summary>
    /// Interaction logic for InputBox.xaml
    /// </summary>
    public partial class InputBox : Window
    {
        public InputBox()
        {
            InitializeComponent();
        }

        public InputBox(string headerText, string descriptionText, ref string valueText)
            : this()
        {
            this.Title = headerText;
            this.InputDescription.Text = descriptionText;
            if (this.ShowDialog().GetValueOrDefault(false))
                valueText = this.ValueTextBox.Text;

        }

        public static bool GetInput(string headerText, string descriptionText, out string valueText)
        {
            InputBox ib = new InputBox();
            ib.Title = headerText;
            ib.InputDescription.Text = descriptionText;
            if (ib.ShowDialog().GetValueOrDefault(false))
            {
                valueText = ib.ValueTextBox.Text;
                return true;
            }
            else
            {
                valueText = "";
                return false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

    }
}
