﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;

namespace BOL
{
    public class CustomerPassCode : BO
    {

        int _customerPassCodeID;
        public int CustomerPassCodeID
        {
            get { return _customerPassCodeID; }
            set { _customerPassCodeID = value; }
        }

        string _passUserName;
        public string PassUserName
        {
            get { return _passUserName; }
            set { _passUserName = value; }
        }

        string _passUserCode;
        public string PassUserCode
        {
            get { return _passUserCode; }
            set { _passUserCode = value; }
        }

        int _forCustomerID;

        public int ForCustomerID
        {
            get { return _forCustomerID; }
            set { _forCustomerID = value; }
        }


        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(PassUserCode)
                    && !string.IsNullOrWhiteSpace(PassUserName) && this.ForCustomerID != null;
            }


        }
    }
}
