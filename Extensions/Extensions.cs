﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Extensions
{


    public static class ControlExtensions
    {
        public static bool IsObjectFocused(this UIElement aControl, bool aCheckChildren, UIElement checkFor)
        {
            var oFocused = System.Windows.Input.FocusManager.GetFocusedElement(aControl) as DependencyObject;
            if(oFocused == checkFor)
                return true;
            if (!aCheckChildren)
                return false;
            while (oFocused != null)
            {
                if (oFocused == checkFor)
                    return true;
                oFocused = System.Windows.Media.VisualTreeHelper.GetParent(oFocused);
            }
            return false;
        }
    }
}
