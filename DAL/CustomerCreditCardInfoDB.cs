﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using CCProc;



namespace DAL
{
    public class CustomerCreditCardInfoDB : Database
    {
        public CustomerCreditCardInfoDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<CCOnFile> Get(int customerID, int CcId)
        {
            var Cards = new ObservableCollection<CCOnFile>();


            using (SqlDataReader dr = this.GetReader("GetCreditCards",
               new SqlParameter[]
                    { new SqlParameter("@customerID", customerID),
                    new SqlParameter("@CCID", CcId) }
               , System.Data.CommandType.StoredProcedure))

                while (dr.Read())
                {
                    Cards.Add(new CCOnFile()
                    {
                        CustomerID = Convert.ToInt32(dr["CustomerID"]),
                        CcID = Convert.ToInt32(dr["CCID"]),
                        Default = Convert.ToBoolean(dr["Default"]),
                        CcInformation = new CCInfo()
                        {
                            CCNumber = dr["CCNumber"].ToString(),
                            ExpDate = (dr["ExpDate"].ToString()),
                            NameOnCard = (dr["NameOnCard"].ToString()),
                            HouseNo = (dr["Address"].ToString()),
                            City = (dr["City"].ToString()),
                            State = (dr["State"].ToString()),
                            PostalCode = (dr["PostalCode"].ToString()),
                            CSC = (dr["CV2Code"].ToString())

                        }

                    });
                }
            return Cards;

        }
        public static ObservableCollection<CCOnFile> GetCards(int customerID, int CcId)
        {
            using (CustomerCreditCardInfoDB CustCardsDb = new CustomerCreditCardInfoDB(null, null))//(null, null))
            {
                return CustCardsDb.Get(customerID, CcId);//.Get(invoiceID, customerID, invoceBatchID, withTrips);
            }
        }
        public bool Add(CCOnFile customerCreditCardInfo)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@CCID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCreditCard", new SqlParameter[]
            {
                new SqlParameter("@CustomerID",customerCreditCardInfo.CustomerID),
                new SqlParameter("@CCNumber",customerCreditCardInfo.CcInformation.CCNumber),
                new SqlParameter("@ExpDate", customerCreditCardInfo.CcInformation.ExpDate),
                new SqlParameter("@CV2Code", customerCreditCardInfo.CcInformation.CSC),
                new SqlParameter("@NameOnCard", customerCreditCardInfo.CcInformation.NameOnCard),
                new SqlParameter("@Address", customerCreditCardInfo.CcInformation.HouseNo),
                new SqlParameter("@City", customerCreditCardInfo.CcInformation.City),
                new SqlParameter("@State", customerCreditCardInfo.CcInformation.State),
                new SqlParameter("@PostalCode", customerCreditCardInfo.CcInformation.PostalCode),
                new SqlParameter("@Default", customerCreditCardInfo.Default),

                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customerCreditCardInfo.CcID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }
        public static bool AddCreditCard(CCOnFile card)
        {
            using (CustomerCreditCardInfoDB customerCreditCardDB = new CustomerCreditCardInfoDB(null, null))
            {
                return customerCreditCardDB.Add(card);
            }
        }
        private bool Edit(CCOnFile customerCreditCardInfo)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UupdateCreditCards", new SqlParameter[]
            {
                new SqlParameter("@CcId", customerCreditCardInfo.CcID),
                new SqlParameter("@CustomerID",customerCreditCardInfo.CustomerID),
                new SqlParameter("@CCNumber",customerCreditCardInfo.CcInformation.CCNumber),
                new SqlParameter("@ExpDate", customerCreditCardInfo.CcInformation.ExpDate),
                new SqlParameter("@CV2Code", customerCreditCardInfo.CcInformation.CSC),
                new SqlParameter("@NameOnCard", customerCreditCardInfo.CcInformation.NameOnCard),
                new SqlParameter("@Address", customerCreditCardInfo.CcInformation.HouseNo),
                new SqlParameter("@City", customerCreditCardInfo.CcInformation.City),
                new SqlParameter("@State", customerCreditCardInfo.CcInformation.State),
                new SqlParameter("@PostalCode", customerCreditCardInfo.CcInformation.PostalCode),
                new SqlParameter("@Default", customerCreditCardInfo.Default)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }
        public static bool EditCreditCardInfo(CCOnFile CustomerCard)
        {
            using (var customerCreditCardInfo = new CustomerCreditCardInfoDB(null, null))
            {

                return customerCreditCardInfo.Edit(CustomerCard);
            }

        }
    }
}
