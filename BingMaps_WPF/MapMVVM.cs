﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BingMaps_WPF
{
    public class MapMVVM : ObservableObject
    {
        public ObservableCollection<CarOnMap> Cars { get; set; }
        public ObservableCollection<TripOnMap> Trips { get; set; }

        public int MapZoomLevel
        {
            get
            {
                return mapZoomLevel;
            }

            set
            {
                mapZoomLevel = value;
                this.OnPropertyChanged("MapZoomLevel");
            }
        }

        public MapMVVM()
        {
            this.Cars = new ObservableCollection<CarOnMap>();
            this.Trips = new ObservableCollection<TripOnMap>();
        }

        private int mapZoomLevel;


    }

    public class TripOnMap : ObservableObject
    {
        public TripOnMap(int tripID, string address, double lat, double lon)
        {
            this.TripID = tripID;
            this.Address = address;
            this.Location = new Microsoft.Maps.MapControl.WPF.Location(lat, lon);

        }

        public string Address { get; private set; }
        public int TripID { get; private set; }

        public Microsoft.Maps.MapControl.WPF.Location Location
        {
            get
            {
                return _location;
            }

            set
            {
                _location = value; this.OnPropertyChanged("Location");
            }
        }

        private Microsoft.Maps.MapControl.WPF.Location _location;
    }

    public class CarOnMap : ObservableObject
    {
        public CarOnMap(int carID, string unitNo, string status, double lat, double lon) : this(carID, unitNo, status, new Microsoft.Maps.MapControl.WPF.Location(lat, lon)) { }

        public CarOnMap(int carID, string unitNo, string status, Microsoft.Maps.MapControl.WPF.Location location)
        {
            this.CarID = carID;
            this.UnitNo = unitNo;
            this.Status = status;
            this.Location = location;
        }

        public string UnitNo { get; set; }
        public int CarID { get; set; }
        public string Status { get; set; }
        private Microsoft.Maps.MapControl.WPF.Location _location;

        

        public Microsoft.Maps.MapControl.WPF.Location Location
        {
            get
            {
                return _location;
            }

            set
            {
                _location = value; this.OnPropertyChanged("Location");
            }
        }               
    }

    public class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public static class Extensions
    {
        public static void AddOrUpdate<T>(this ObservableCollection<T> collection, T o)
        {
            collection.Remove(o);
            collection.Add(o);            
        }
    }

    public enum ObjectAction
    {
        Add,
        Remove,
        AddOrUpdate
    }

}
