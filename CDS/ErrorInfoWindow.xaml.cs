﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Mail;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ErrorInfoWindow.xaml
    /// </summary>
    public partial class ErrorInfoWindow : Window
    {
        public ErrorInfoWindow()
        {
            InitializeComponent();
        }

        private ClientFeedback _clientFeedback;

        public ClientFeedback ClientsFeedback
        {
            get { return _clientFeedback; }
            set { _clientFeedback = value; this.DataContext = value; }
        }

        private void btnEmailError_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                MailMessage mm = new MailMessage("Shloma@LiebermansCarService.com", "YWeber@prosysdesign.com", "CDS Error", "");//this.ClientsFeedback.OrganizationInformation.EmailAddress, "fr.error@gmail.com", "FR Error", "");
                mm.ReplyToList.Add(new MailAddress(this.ClientsFeedback.ReplyToEmailAddress));

                string message = string.Format("Error Type: {0}\n\nError Message: {1}\n\nSource: {2}\n\nStack Trace: {3}\n\nTarget Site: {4}\n\nClienName: {5}\n\nApplication Information: {6}\n\nClient Input: {7}",
                    this.ClientsFeedback.ExceptionCatched.GetType(),
                    this.ClientsFeedback.ExceptionCatched.Message,
                    this.ClientsFeedback.ExceptionCatched.Source,
                    this.ClientsFeedback.ExceptionCatched.StackTrace,
                    this.ClientsFeedback.ExceptionCatched.TargetSite,
                    "Leibermans",
                    this.ClientsFeedback.ApplicationInfo,
                    this.ClientsFeedback.ClientInput);
                mm.Body = message;

                this.Cursor = Cursors.Wait;
                SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
                emailClient.Port = 587;

                emailClient.Credentials = new System.Net.NetworkCredential("Shloma@LiebermansCarService.com", "?Blue#2835!");
                emailClient.EnableSsl = true;
                //MessageBox.Show("going to send now..");
                emailClient.Send(mm);
                this.DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "   " + ex.InnerException);

            }
            finally { this.Cursor = Cursors.Arrow; }
        }

        private void btnPrintErroInfo_Click(object sender, RoutedEventArgs e)
        {
            PrintErrorInfo();
        }

        private void PrintErrorInfo()
        {
            //PrintDialog printDlg = new PrintDialog();
            //if (printDlg.ShowDialog() ?? false)
            //{
            //    //PrintQueue.CreateXpsDocumentWriter(printDlg.PrintQueue);
            //    printDlg.PrintVisual(this.LayoutGrid, "Error Information");

            //}

            UIController.PrintErrorInfo(this.ClientsFeedback);
        }
    }
}
