﻿using System;
using System.Collections.ObjectModel;
using BOL;
using System.Collections.Generic;
namespace CDS
{
    public interface IDispatchingMVVM : IDisposable
    {
        Trip AddedEditedTrip { get; set; }
        string CallerIDInfo { get; set; }
        void AddEditTrip(Trip trip);
        void AddTripLocation(Trip trip);
        System.Windows.Visibility AddEditTripIsVisible { get; }
        void AddTripToList(Trip tripToAdd);
        bool AssignDriverToTrip(Trip trip, Driver driver, bool warnDriverChanged = true);
        void ChangeDriversStatus(Driver driver, DriverStatusOptions assignStatus);
        void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced);
        ObservableCollection<DriverAndTripInformation> DriversAndTripInformation { get; set; }
        ObservableCollection<Driver> DriversOnBreak { get; }
        void MarkDriverForBreak(Driver driver, DriverStatusOptions breakType);
        void OnPropertyChanged(string propertyName);
        event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        void RefereshStatusTimesOnDrivers();
        ObservableCollection<Driver> SignedInDrivers { get; }
        DriverAndTripInformation SignInDriver(Driver driver, string signInLocation);
        ObservableCollection<DriverAndTripInformation> SortedDriversAndTrips { get; }
        ObservableCollection<Trip> SortedTrips { get; }
        ObservableCollection<StreetAbbreviation> StreetAbbreviations { get; set; }
        ObservableCollection<Trip> Trips { get; set; }
        void Undo98Status(DriverAndTripInformation dti);
        void SignOutDriver(Driver driver);
        void CancelAddEditTrip();
        void ResetDispatchingInformation();
        void DeleteTrip(Trip trip);
        void EditTrip(Trip trip);
        void ProcessScheduledTrip();
        string[] GetLogOnOff();
        void ChangeDriverLocation(DriverAndTripInformation driverAndTripInformation, string newLocation);
        bool IsLongDistanceMode{ get; set;}

        void UpdateDriverTripTimers();
        void UpdateSectionCarAvailability(string sectionName, BOL.CarsAvailabilityTypes availability);
        ObservableCollection<DriverPermissionRequest> DriversPermissionRequests { get; set; }
    }
}
