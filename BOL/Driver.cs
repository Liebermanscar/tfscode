﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class Driver : BO
    {

        public Driver()
        {
            this.OnCommisionType = new CommisionType();
            this.DrivesVehicle = new Vehicle();
        }
        int _driverID;

        [DataMember]
        public int DriverID
        {
            get { return _driverID; }
            set { _driverID = value; }
        }

        bool _allowsChargeToHisAccount;

        [DataMember]
        public bool AllowsChargeToHisAccount
        {
            get { return _allowsChargeToHisAccount; }
            set { _allowsChargeToHisAccount = value; }
        }

        string _driverCode;
        [DataMember]
        public string DriverCode
        {
            get { return _driverCode; }
            set { _driverCode = value; this.OnPropertyChanged("AllowSave"); }
        }

        public string DriverName
        {
            get { return _driverFirstName + " " + _driverLastName; }
        }

        string _driverFirstName;
        [DataMember]
        public string DriverFirstName
        {
            get { return _driverFirstName; }
            set { _driverFirstName = value; this.OnPropertyChanged("AllowSave"); }
        }

        string _driverLastName;
        [DataMember]
        public string DriverLastName
        {
            get { return _driverLastName; }
            set { _driverLastName = value; this.OnPropertyChanged("AllowSave"); }
        }

        string _driverLicenseNumber;
        [DataMember]
        public string DriverLicenseNumber
        {
            get { return _driverLicenseNumber; }
            set { _driverLicenseNumber = value; this.OnPropertyChanged("AllowSave"); }
        }

        string _cellNumber;

        [DataMember]
        public string CellNumber
        {
            get { return _cellNumber; }
            set { _cellNumber = value; this.OnPropertyChanged("AllowSave"); }
        }

        string _cellPhoneCarrier;
        [DataMember]
        public string CellPhoneCarrier
        {
            get { return _cellPhoneCarrier; }
            set { _cellPhoneCarrier = value; }
        }

        public string TextMsgAddress
        { get { return this.CellNumber + "@" + this.CellPhoneCarrier; } }
        CommisionType _onCommisionType;
        [DataMember]
        public CommisionType OnCommisionType
        {
            get { return _onCommisionType; }
            set { _onCommisionType = value; this.OnPropertyChanged("AllowSave"); }
        }

        bool _IsMain;

        public bool IsMain
        {
            get { return _IsMain; }
            set { _IsMain = value; }
        }
        int _LinkedDriverID;

        public int LinkedDriverID
        {
            get { return _LinkedDriverID; }
            set { _LinkedDriverID = value; }
        }

        Driver _LinkedDriver;

        public Driver LinkedDriver
        {
            get { return _LinkedDriver; }
            set { _LinkedDriver = value; if (value != null) this.LinkedDriverID = value.DriverID; }
        }
        decimal _BonusBracket;

        public decimal BonusBracket
        {
            get { return _BonusBracket; }
            set { _BonusBracket = value; }
        }
        decimal _BonusPercent;

        public decimal BonusPercent
        {
            get { return _BonusPercent; }
            set { _BonusPercent = value; }
        }

        string _CurrentLocation;
        public string CurrentLocation
        {
            get { return _CurrentLocation; }
            set { _CurrentLocation = value; }
        }
        DriverStatusOptions _DriverStatus;
        public DriverStatusOptions DriverStatus
        {
            get { return _DriverStatus; }
            set { _DriverStatus = value; }
        }
        int _AssignedToTripID;
        public int AssignedToTripID
        {
            get { return _AssignedToTripID; }
            set { _AssignedToTripID = value; }
        }
        int _LastTripID;
        public int LastTripID
        {
            get { return _LastTripID; }
            set { _LastTripID = value; }
        }
        bool _IsLongDistanceDriver;
        public bool IsLongDistanceDriver
        {
            get { return _IsLongDistanceDriver; }
            set { _IsLongDistanceDriver = value; }
        }
        DateTime? _StatusChangeTime;
        public DateTime? StatusChangeTime
        {
            get { return _StatusChangeTime; }
            set { _StatusChangeTime = value; }
        }
        bool _driverActive;
        [DataMember]
        public bool DriverActive
        {
            get { return _driverActive; }
            set { _driverActive = value; this.OnPropertyChanged("AllowSave"); }
        }

        Vehicle _drives;
        [DataMember]
        public Vehicle DrivesVehicle
        {
            get { return _drives; }
            set { _drives = value; }
        }

        public override string ToString()
        {
            return this.DriverCode;
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.DriverCode)
                   && !string.IsNullOrWhiteSpace(this.DriverFirstName)
                  && !string.IsNullOrWhiteSpace(this.DriverLastName)
                   && !string.IsNullOrWhiteSpace(this.DriverLicenseNumber)
                   && !string.IsNullOrWhiteSpace(this.CellNumber)
                  && !string.IsNullOrWhiteSpace(this.OnCommisionType.CommisionTypeCode);
            }
        }
    }
}
