﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections.ObjectModel;
using BOL;
using System.Net.Mail;
using System.IO;
using System.Diagnostics;

namespace DispatcherService
{
    [ServiceContract(SessionMode = SessionMode.Allowed, CallbackContract = typeof(IDispatcherCallBack))]
    public interface IDispatcher
    {
        [OperationContract(IsOneWay = false)]
        bool DispatcherJoin(BOL.User dispatcher, double version);

        [OperationContract(IsOneWay = true)]
        void DispatcherLeave(BOL.User dispatcher);

        [OperationContract(IsOneWay = false)]
        BOL.Trip[] GetAllTrips();

        [OperationContract(IsOneWay = false)]
        BOL.DriverAndTripInformation[] GetAllDriversAndTripInfo();

        [OperationContract(IsOneWay = false)]
        BOL.DriverAndTripInformation[] GetDriverTripInformation();

        [OperationContract(IsOneWay = false)]
        BOL.SectionCarsAvailability[] GetAllSectionsCarAvailability();

        [OperationContract(IsOneWay = true)]
        void AddTrip(Trip trip, int byUserID, bool isSystemTrips = false);

        [OperationContract(IsOneWay = true)]
        void EditTrip(Trip trip);

        [OperationContract(IsOneWay = true)]
        void AddTripLocation(Trip trip);

        [OperationContract(IsOneWay = true)]
        void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced);

        [OperationContract(IsOneWay = true)]
        void AssignDriverToTrip(Trip trip, Driver driver, bool warnDriverChanged = true);

        [OperationContract(IsOneWay = true)]
        void RemoveTrip(Trip trip, bool fromDatabase = true);

        [OperationContract(IsOneWay = true)]
        void AddDriver(Driver driver, string signInLocation, bool isLongDistanceDriver);

        [OperationContract(IsOneWay = true)]
        void EditDriver(BOL.DriverAndTripInformation driverAndTripInfo);

        [OperationContract(IsOneWay = true)]
        void UpdateDriverTripTimers(DriverAndTripInformation[] driversAndTripsInfo);

        [OperationContract(IsOneWay = false)]
        DriverAndTripInformation SignInDriver(Driver driver, string driverLocation);

        [OperationContract(IsOneWay = true)]
        void EditDriverStatus(Driver driver, DriverStatusOptions driverStatus);

        [OperationContract(IsOneWay = true)]
        void Undo98Status(Driver driver);

        [OperationContract(IsOneWay = true)]
        void RemoveDriver(Driver driver);

        [OperationContract(IsOneWay = true)]
        void ResetTripsAndDrivers(bool isLongDistance);

        [OperationContract(IsOneWay = false)]
        string[] GetLogOnOffLog();

        [OperationContract(IsOneWay = true)]
        void ProcessScheduledTrip();

        [OperationContract(IsOneWay = true)]
        void UpdateSectionCarAvailability(string sectionName, BOL.CarsAvailabilityTypes availability);

    }

    public interface IDispatcherCallBack
    {
        [OperationContract(IsOneWay = true)]
        void TripAdded(BOL.Trip trip, int byUserID);

        [OperationContract(IsOneWay = true)]
        void TripEdited(BOL.Trip trip);

        [OperationContract(IsOneWay = true)]
        void TripLocationAdded(Trip trip);

        [OperationContract(IsOneWay = true)]
        void TripRemoved(BOL.Trip trip);

        [OperationContract(IsOneWay = true)]
        void DriverAdded(BOL.DriverAndTripInformation driver);

        [OperationContract(IsOneWay = true)]
        void DriverEdited(BOL.DriverAndTripInformation driver);

        [OperationContract(IsOneWay = true)]
        void DriverRemoved(BOL.DriverAndTripInformation driver);

        [OperationContract(IsOneWay = false)]
        AssignedUnitDiscrepencyOption GetAssignedUnitOptions();

        [OperationContract(IsOneWay = true)]
        void TrisAndDriversWereReset(bool isLongDistance);

        [OperationContract(IsOneWay = true)]
        void DriversAndTripInfoReset(DriverAndTripInformation[] driverAndTripsArray);

        [OperationContract(IsOneWay = true)]
        void SectionCarAvailabilityChanged(SectionCarsAvailability sectionChanged);
    }

    #region Public enums/event args
    /// <summary>
    /// A simple enumeration for dealing with the chat message types
    /// </summary>
    public enum MessageType
    {
        TripAdded,
        TripEdited,
        TripRemoved,
        DriverAdded,
        DriverEdited,
        DriverRemoved,
        GetAssignedUnitOptions,
        TripDriverResetLocal,
        TripDriverResetLongDistance,
        DriversAndTripInfoReset,
        CarsAvailabilitChanged,
        TripLocationAdded
    };

    /// <summary>
    /// This class is used when carrying out any of the 4 chat callback actions
    /// such as Receive, ReceiveWhisper, UserEnter, UserLeave <see cref="IChatCallback">
    /// IDispatcherCallBack</see> for more details
    /// </summary>
    public class DispatcherEventArgs : EventArgs
    {
        public MessageType msgType;
        public Trip TripAffected;
        public DriverAndTripInformation driverAndTripInfo;
        public int byUserID;
        public bool isLongDistance;
        public int DriverID;
        public SectionCarsAvailability SectionAffected { get; set; }
    }
    #endregion

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class DispatchService : IDispatcher
    {
        #region Instance fields

        //thread sync lock object
        private static Object syncObj = new Object();
        //callback interface for clients
        IDispatcherCallBack callback = null;

        Dictionary<User, IDispatcherCallBack> dispatchers = new Dictionary<User, IDispatcherCallBack>();

        List<User> dispatcherList = new List<User>();

        List<string> logOnOffLog = new List<string>();

        static List<IDispatcherCallBack> m_Callbacks = new List<IDispatcherCallBack>();

        //delegate used for BroadcastEvent
        public delegate void DispatcherEventHandler(object sender, DispatcherEventArgs e);
        public static event DispatcherEventHandler DispatchEvent;
        private DispatcherEventHandler myEventHandler = null;
        //holds a list of dispatchers, and a delegate to allow the BroadcastEvent to work
        //out which dispatcher delegate to invoke
        //static Dictionary<User, DispatcherEventHandler> dispatchers = new Dictionary<User, DispatcherEventHandler>();
        //current person 
        private User dispatcher;

        static DispatchingData dispatchingInfo = new DispatchingData();

        System.Timers.Timer longDistanceSheduledTripsTimer = new System.Timers.Timer() { Interval = 10000, Enabled = true };
        System.Timers.Timer clearOldTripsFromDispatchingService = new System.Timers.Timer() { Interval = 10000, Enabled = true };

        public DispatchService()
        {
            try
            {
                LogEvent("Starting Step 1");
                DAL.Database.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
                //DAL.Database.ConnectionString = "Data Source=SERVER;Initial Catalog=CDS;Persist Security Info=True;User ID=CDC";
                // this.GetScheduledLongDistanceTrips();

                LogEvent("Conn String: " + DAL.Database.ConnectionString);

                clearOldTripsFromDispatchingService.Elapsed += delegate (object sender, System.Timers.ElapsedEventArgs e)
                {
                    clearOldTripsFromDispatchingService.Interval = 3600000;//3,600,000 milli seconds in every hour// use this to test 5000;
                    this.ClearOldTripsFromDispatchingService();
                };

                longDistanceSheduledTripsTimer.Elapsed += delegate (object sender, System.Timers.ElapsedEventArgs e)
                {
                    longDistanceSheduledTripsTimer.Interval = 300000;//every 5 minutes
                    this.GetScheduledLongDistanceTrips();
                };

                LogEvent("SignalR: " + Properties.Settings.Default.UseSignalR.ToString());

                if (Properties.Settings.Default.UseSignalR)
                    CommunicateWithSignalRHub.Subscribe(Properties.Settings.Default.SignalRServiceIPAddressAndPort, this);

                LogEvent("Ending Init Step 2");

                RecoverServiceData();
            }
            catch (Exception ex)
            {
                LogEvent(ex.Message, EventLogEntryType.Error);
                //throw;
            }
            
        }

        internal static void LogEvent(string log, EventLogEntryType logType = EventLogEntryType.Information, int code = 0)
        {
            try
            {
                
                string sSource;
                string sLog;
                string sEvent;

                sSource = "DispatchingService";
                sLog = "Application";
                sEvent = log;

                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sLog);

                EventLog.WriteEntry(sSource, sEvent, logType, code);
            }
            catch (Exception) { }
        }

        void LogSteps(string text)
        {

            string path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + "\\Dispatcher Service\\Log.txt";
            try
            {
                using (FileStream fs = File.OpenWrite(path))
                {
                    Byte[] info =
                        new UTF8Encoding(true).GetBytes(string.Format("Date: {0} Event: {1}", DateTime.Now, text));

                    // Add some information to the file.
                    
                    fs.Write(info, 0, info.Length);
                }                
            }

            catch (Exception e) { }
            finally
            {
                //if (fileLogWriter != null)
                //    fileLogWriter.Close();
            }
        }

        void ClearOldTripsFromDispatchingService()
        {
            var tripsToRemove = this.GetAllTrips();
            DateTime timeTocheck = DateTime.Now.AddHours(-2);//  use this to test  -->DateTime.Now.AddSeconds(-40);
            foreach (var tripToRemove in tripsToRemove.Where(t => t.TripEnded < timeTocheck))
            {                
                RemoveTrip(tripToRemove, false);
                System.Threading.Thread.Sleep(300);
            }
        }

        void GetScheduledLongDistanceTrips()
        {
            LogSteps("Starting GetScheduledLongDistanceTrips");
            var tripsToAdd = DAL.ScheduledTripDB.ProcessScheduledTripsAsTrips();
            LogSteps("Ran: DAL.ScheduledTripDB.ProcessScheduledTripsAsTrips()");

            LogSteps("Starting Loop: tripsToAdd");

            if (tripsToAdd != null)
            {
                tripsToAdd = new ObservableCollection<Trip>(tripsToAdd.OrderBy(t => t.ScheduledFor));
                foreach (var trip in tripsToAdd.Where(t => t.IsLongDistanceTrip))
                {
                    AddTrip(trip, 0, true);
                    //dispatchingInfo.AddTripToList(trip, BroadcastMessage, this.AssignedUnitDiscrepencyOption, 0);
                };
            }
            LogSteps("Starting Loop: tripsToAdd");

        }

        void RecoverServiceData()
        {
            //need to add overload to be able to add the time in the param
            var tripsToAdd = DAL.TripDB.GetTrips(0, 0, 0, 0, DateTime.Today, DateTime.Today, false, null, 0, 0);
            var systemUsers = DAL.UserDB.GetUsers(0);

            if (tripsToAdd != null)
            {
                int totalTrips = tripsToAdd.Count;
                tripsToAdd = new ObservableCollection<Trip>(tripsToAdd.Where(t => !t.TripEnded.HasValue || t.TripEnded >= DateTime.Now.AddHours(-2)));
                int startingNumber = totalTrips - tripsToAdd.Count;

                foreach (var trip in tripsToAdd)
                {
                    var byUser = systemUsers.FirstOrDefault(su => su.UserID == trip.TakenByDispatcherID);

                    if (byUser != null)
                        trip.DispatcherCode = byUser.UserCode;

                    startingNumber++;
                    trip.TripNo = startingNumber;

                    dispatchingInfo.Trips.Add(trip);
                };
            }

            var drivers = DAL.DriversDB.GetDrivers(false);

            var driversToRecover = new List<Driver>(drivers.Where(d => d.DriverStatus != DriverStatusOptions.None && d.DriverStatus != DriverStatusOptions.SignedOut));

            foreach (Driver driver in driversToRecover)
            {
                DriverAndTripInformation dti = new DriverAndTripInformation()
                {
                    UnitDriver = driver,
                    DriverStatus = driver.DriverStatus,
                    CalledInLocation = driver.CurrentLocation,
                    IsLongDistanceDriver = driver.IsLongDistanceDriver,
                    StatusChangedTime = driver.StatusChangeTime
                };

                if (driver.AssignedToTripID > 0)
                {
                    dti.AssignedToTrip = dispatchingInfo.Trips.FirstOrDefault(t => t.TripID == driver.AssignedToTripID);
                }

                if (driver.LastTripID > 0)
                {
                    dti.LastTrip = dispatchingInfo.Trips.FirstOrDefault(t => t.TripID == driver.LastTripID);
                }

                dispatchingInfo.DriversAndTripInformation.Add(dti);
            }
        }

        void ProcessScheduledTrip()
        {
            this.GetScheduledLongDistanceTrips();
        }

        #endregion


        #region Helpers
        /// <summary>
        /// Searches the intenal list of chatters for a particular person, and returns
        /// true if the person could be found
        /// </summary>
        /// <param name="name">the name of the <see cref="Common.Person">Person</see> to find</param>
        /// <returns>True if the <see cref="Common.Person">Person</see> was found in the
        /// internal list of chatters</returns>
        private bool checkIfDispatcherExists(int userID)
        {
            foreach (User d in dispatchers.Keys)
            {
                if (d.UserID == userID)
                {
                    return true;
                }
            }
            return false;
        }

        private IDispatcherCallBack CurrentCallback
        {
            get { return OperationContext.Current.GetCallbackChannel<IDispatcherCallBack>(); }
        }

        private bool SearchDispatcherByID(int id)
        {
            foreach (User d in dispatchers.Keys)
            {
                if (d.UserID == id)
                {
                    return true;
                }
            }
            return false;
        }

        private User getDispatcher(int userID)
        {
            foreach (User d in dispatchers.Keys)
            {
                if (d.UserID == userID)
                {
                    return d;
                }
            }
            return null;
        }
        #endregion

        #region IDispatcher Members

        public bool DispatcherJoin(User dispatcher, double version)
        {
            try
            {
                if (version < 1.0)
                {
                    return false;
                }

                if (!dispatchers.ContainsValue(CurrentCallback))
                {
                    dispatchers.Add(dispatcher, CurrentCallback);
                    dispatcherList.Add(dispatcher);
                }

                string s = dispatcher.UserName + " loged in @ " + DateTime.Now;
                Console.WriteLine(s);
                this.logOnOffLog.Add(s);

                return true;

            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(ex, new FaultReason(ex.Message), new FaultCode("Sender"));
            }
        }

        public void DispatcherLeave(User dispatcher)
        {
            KeyValuePair<User, IDispatcherCallBack>? dispatcCallbackToRemove = null;
            foreach (KeyValuePair<User, IDispatcherCallBack> dicItem in dispatchers)
            {
                if (dicItem.Value == CurrentCallback)
                {
                    dispatcCallbackToRemove = dicItem;
                    break;
                }
            }

            if (dispatcCallbackToRemove != null)
            {
                dispatchers.Remove(dispatcCallbackToRemove.Value.Key);

                string s = dispatcCallbackToRemove.Value.Key.UserName + " loged off @ " + DateTime.Now;
                Console.WriteLine(s);
                this.logOnOffLog.Add(s);
            }
        }

        public Trip[] GetAllTrips()
        {
            Trip[] list = new Trip[dispatchingInfo.Trips.Count()];
            //carry out a critical section that copy all chatters to a new list
            lock (syncObj)
            {
                dispatchingInfo.Trips.CopyTo(list, 0);
            }
            return list;
        }

        public Trip GetTrip(int tripID)
        {
            return dispatchingInfo.Trips.FirstOrDefault(t => t.TripID == tripID);
        }

        public DriverAndTripInformation[] GetAllDriversAndTripInfo()
        {
            DriverAndTripInformation[] list = new DriverAndTripInformation[dispatchingInfo.DriversAndTripInformation.Count];
            //carry out a critical section that copy all chatters to a new list
            lock (syncObj)
            {
                dispatchingInfo.DriversAndTripInformation.CopyTo(list, 0);
            }
            return list;
        }

        public DriverAndTripInformation[] GetDriverTripInformation()
        {
            throw new NotImplementedException();
        }

        public void AddTrip(Trip trip, int byUserID, bool isSystemTrips = false)
        {
            try
            {
                dispatchingInfo.AddTripToList(trip, BroadcastMessage, this.AssignedUnitDiscrepencyOption, byUserID, isSystemTrips);
            }
            catch (Exception ex)
            {
                throw new FaultException(
                new FaultReason(ex.Message),
                new FaultCode("Data Access Error"));
            }
        }

        public void EditTrip(Trip trip)
        {
            Trip tripFromList = dispatchingInfo.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();

            var tripIx = dispatchingInfo.Trips.IndexOf(tripFromList);

            dispatchingInfo.Trips[tripIx] = trip;

            DAL.TripDB.EditTrip(trip);        

            DispatcherEventArgs e = new DispatcherEventArgs();
            e.msgType = MessageType.TripEdited;
            e.TripAffected = trip;

            BroadcastMessage(e);

            var driverAndTrip = dispatchingInfo.DriversAndTripInformation.FirstOrDefault(t => t.AssignedToTrip != null && t.AssignedToTrip.TripNo == trip.TripNo);

            if (driverAndTrip != null)
            {
                driverAndTrip.AssignedToTrip = trip;
                BroadcastMessage(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = driverAndTrip });
                
                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = driverAndTrip.UnitDriver.DriverID, DriverStatusType = driverAndTrip.DriverStatus, TimeLogged = DateTime.Now });
            }
        }

        public void AddTripLocation(Trip trip)
        {
            Trip tripFromList = dispatchingInfo.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();

            var tripIx = dispatchingInfo.Trips.IndexOf(tripFromList);

            dispatchingInfo.Trips[tripIx] = trip;

            //DAL.TripDB.EditTrip(trip);

            DispatcherEventArgs e = new DispatcherEventArgs();
            e.msgType = MessageType.TripLocationAdded;
            e.TripAffected = trip;

            BroadcastMessage(e);
        }

        public void RemoveTrip(Trip trip, Boolean deleteFromDataBase = true)
        {
            Trip tripFromList = dispatchingInfo.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();

            if (tripFromList == null)
                return;

            dispatchingInfo.Trips.Remove(tripFromList);

            DispatcherEventArgs e = new DispatcherEventArgs();
            e.msgType = MessageType.TripRemoved;
            e.TripAffected = trip;

            if (deleteFromDataBase)
                DAL.TripDB.DeleteTrip(trip);

            BroadcastMessage(e);
        }

        public void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced)
        {
            if (dispatchingInfo.Trips.Count == 0)
                return;

            if (trip.TripNo != 0)
            {
                Trip tripFromList = dispatchingInfo.Trips.Where(t => t.TripNo == trip.TripNo).First();

                dispatchingInfo.ChangeTripStatus(tripFromList, status, driver, driverReplaced, BroadcastMessage);
            }
        }

        public void AssignDriverToTrip(Trip trip, Driver driver, bool warnDriverChanged = true)
        {
            dispatchingInfo.AssignDriverToTrip(trip, driver, BroadcastMessage, false, AssignedUnitDiscrepencyOption);
        }

        public void AssignDriverToTrip(int tripID, int driverID)
        {
            Trip trip = dispatchingInfo.Trips.FirstOrDefault(t => t.TripID == tripID);
            var dti = dispatchingInfo.DriversAndTripInformation.FirstOrDefault(d => d.UnitDriver.DriverID == driverID);

            if (trip != null && dti != null && dti.UnitDriver != null)
            {
                dispatchingInfo.AssignDriverToTrip(trip, dti.UnitDriver, BroadcastMessage, false, this.AssignedUnitDiscrepencyOption);
            }
        }

        private void SendEmailToDriver(Trip trip, Driver driver, bool newTrip)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(driver.CellPhoneCarrier) || string.IsNullOrWhiteSpace(driver.CellNumber))
                    return;

                string mailBody = "";

                if (trip.IsLongDistanceTrip)
                {
                    mailBody = "Trip from: " + trip.FromLocation + " " + trip.FromCity + "\nTo: " + trip.ToLocation + " " + trip.ToCity;
                    if (trip.ScheduledFor != null)
                        mailBody = String.Format("{0}\nTime: {1}", mailBody, trip.ScheduledFor.Value.ToShortTimeString());
                    if (!string.IsNullOrWhiteSpace(trip.CallBackPhoneNo))
                        mailBody = mailBody + "\nCallBack: " + trip.CallBackPhoneNo;
                    if (trip.MedicaidDispatchTripNo > 0)
                        mailBody = mailBody + "\nM#: " + trip.MedicaidDispatchTripNo.ToString();
                    if (trip.TotalPrice > 0)
                        mailBody = mailBody + "\nPrice: " + trip.TotalPrice.ToString("c");
                }
                else
                {
                    mailBody = "Trip from: " + trip.FromLocation;
                    if (!string.IsNullOrEmpty(trip.ToLocation))
                        mailBody += "\nTo: " + trip.ToLocation;
                    if (trip.ETA != null)
                        mailBody = mailBody + "\nETA: " + trip.ETA.Value.ToShortTimeString();
                    if (!string.IsNullOrWhiteSpace(trip.CallBackPhoneNo))
                        mailBody = mailBody + "\nCallBack: " + trip.CallBackPhoneNo;
                    if (trip.MedicaidDispatchTripNo > 0)
                        mailBody = mailBody + "\nM#: " + trip.MedicaidDispatchTripNo.ToString();
                }

                if (trip.IsNeedCancelTextMsg)
                {
                    SendEmailMessege("Cancled", mailBody, driver.TextMsgAddress);
                    trip.IsNeedCancelTextMsg = false;
                }

                if (trip.IsNeedToSendTextMsg)
                {
                    SendEmailMessege("Assigned", mailBody, driver.TextMsgAddress);
                    trip.IsNeedToSendTextMsg = false;
                }

                /*
                //from here is if sending long msg and need to split in multiple msgs
                string subject = "";
                string msgPart = "";
                int noMsgs = 1;
                int senderLenght = 31;
                int subjectLenght = 28;
                int totalCapacity = 160 - (senderLenght + subjectLenght);
                int to = totalCapacity;
                int totalSent = 0;
                int remaining = mailBody.Length;

                if (mailBody.Length > totalCapacity)
                        noMsgs = (int)Math.Ceiling((decimal)mailBody.Length / totalCapacity);

                    for (int i = 0; i < noMsgs; i++)
                    {
                        if (remaining < 160)
                            to = remaining;

                        msgPart = mailBody.Substring(totalSent, to);
                        subject = "(" + (i + 1) + "/" + noMsgs + ") " + tripStatus;

                        SendEmailMessege(subject, msgPart, driver.TextMsgAddress); //"3477420942@messaging.sprintpcs.com"
                        //SendEmailMessege(subject, msgPart, "3477420942@messaging.sprintpcs.com"); 

                        remaining = remaining - totalCapacity;
                        totalSent = totalSent + totalCapacity;
                    }
                    trip.IsNeedToSendTextMsg = false;
                  */
            }
            catch (Exception ex) { }
        }

        public static void SendEmailMessege(string Subject, string MailBody, string mailTo)
        {
            try
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    try
                    {
                        string emailAddress = DAL.FacilitySettingsDB.GetFacilitySettings("TripsEmailAddress", null, true).SettingValue;
                        string emailPassword = DAL.FacilitySettingsDB.GetFacilitySettings("TripsEmailPassword", null, true).SettingValue;                       

                        if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(emailPassword))
                            return;

                        MailMessage mm = new MailMessage(emailAddress, mailTo, Subject, MailBody);

                        mm.Body = MailBody;

                        SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
                        emailClient.Port = 587;

                        emailClient.Credentials = new System.Net.NetworkCredential(emailAddress, emailPassword);
                        emailClient.EnableSsl = true;
 
                        emailClient.SendAsync(mm, mm);
                    }
                    catch (Exception ex) { }
                });
            }
            catch (Exception){ }
        }

        public void AddDriver(Driver driver, string signInLocation, bool isLongDistanceDriver)
        {
            try
            {
                DriverAndTripInformation dti = dispatchingInfo.SignInDriver(driver, signInLocation, null, isLongDistanceDriver);

                if (dti == null)
                    return;

                DispatcherEventArgs e = new DispatcherEventArgs();
                e.msgType = MessageType.DriverAdded;
                e.driverAndTripInfo = dti;
                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = dti.UnitDriver.DriverID, DriverStatusType = dti.DriverStatus, TimeLogged = DateTime.Now });

                BroadcastMessage(e);
            }
            catch (Exception ex) { throw new FaultException<Exception>(ex); }
        }

        public void EditDriver(BOL.DriverAndTripInformation driverAndTripInfo)
        {
            try
            {
                DriverAndTripInformation serverDriverAndTrip = dispatchingInfo.DriversAndTripInformation.
                    Where(dti => dti.UnitDriver.DriverID == driverAndTripInfo.UnitDriver.DriverID).FirstOrDefault();

                if (serverDriverAndTrip == null)
                    return;

                var driverIX = dispatchingInfo.DriversAndTripInformation.IndexOf(serverDriverAndTrip);

                dispatchingInfo.DriversAndTripInformation[driverIX] = driverAndTripInfo;

                BroadcastMessage(new DispatcherEventArgs()
                {
                    msgType = MessageType.DriverEdited,
                    driverAndTripInfo = driverAndTripInfo
                });

                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = driverAndTripInfo.UnitDriver.DriverID, DriverStatusType = driverAndTripInfo.DriverStatus, TimeLogged = DateTime.Now });

                dispatchingInfo.SaveDriverStatus(driverAndTripInfo);
            }
            catch (Exception ex) { throw new FaultException<Exception>(ex); }
        }

        public void UpdateDriverTripTimers(DriverAndTripInformation[] driversAndTripsInfo)
        {
            List<DriverAndTripInformation> dtiListToRemove = new List<DriverAndTripInformation>();
            //save which dti to remove
            foreach (var dti in driversAndTripsInfo)
            {
                var dtToUpdate = dispatchingInfo.DriversAndTripInformation.Where(dt => dt.UnitDriver.DriverID == dti.UnitDriver.DriverID).FirstOrDefault();
                dtiListToRemove.Add(dtToUpdate);
            }

            //remove those
            foreach (var dtiToremove in dtiListToRemove)
            {
                dispatchingInfo.DriversAndTripInformation.Remove(dtiToremove);
            }

            //add the changed
            foreach (var dtiToAdd in driversAndTripsInfo)
            {
                if (dispatchingInfo.DriversAndTripInformation.Count(dt => dt.UnitDriver.DriverID == dtiToAdd.UnitDriver.DriverID) == 0)
                    dispatchingInfo.DriversAndTripInformation.Add(dtiToAdd);
            }
            //dispatchingInfo.DriversAndTripInformation = new ObservableCollection<DriverAndTripInformation>(driversAndTripsInfo);
            BroadcastMessage(new DispatcherEventArgs() { msgType = MessageType.DriversAndTripInfoReset });
        }

        public DriverAndTripInformation SignInDriver(Driver driver, string driverLocation)
        {
            DriverAndTripInformation dti = dispatchingInfo.SignInDriver(driver, driverLocation, BroadcastMessage, false);           

            return dti;
        }

        public void SendRequestToDispatcher(int driverID, BOL_OTR.DriversRequestOptions request)
        {
            dispatchingInfo.SendRequestToDispatcher(driverID, request, BroadcastMessage);
        }

        public void EditDriverStatus(Driver driver, DriverStatusOptions driverStatus)
        {
            var dti = dispatchingInfo.ChangeDriversStatus(driver, driverStatus, BroadcastMessage);
        }

        public void EditDriverStatus(int driverID, DriverStatusOptions driverStatus)
        {
            var dti = dispatchingInfo.DriversAndTripInformation.FirstOrDefault(d => d.UnitDriver.DriverID == driverID);

            if (dti != null)
                dispatchingInfo.ChangeDriversStatus(dti.UnitDriver, driverStatus, BroadcastMessage);
        }

        public void Undo98Status(Driver driver)
        {
            dispatchingInfo.Undo98Status(driver, BroadcastMessage);
        }

        public void RemoveDriver(Driver driver)
        {
            var dti = dispatchingInfo.SignOutDriver(driver, BroadcastMessage);
        }

        public void RemoveDriver(int driverID)
        {
            var dti = dispatchingInfo.DriversAndTripInformation.FirstOrDefault(d => d.UnitDriver.DriverID == driverID);

            if (dti != null)
            {
                dispatchingInfo.SignOutDriver(dti.UnitDriver, BroadcastMessage);
            }
        }

        public void ResetTripsAndDrivers(bool isLongDistance)
        {
            dispatchingInfo.Trips = new ObservableCollection<Trip>(dispatchingInfo.Trips.Where(t => t.IsLongDistanceTrip != isLongDistance));
            dispatchingInfo.DriversAndTripInformation = new ObservableCollection<DriverAndTripInformation>(dispatchingInfo.DriversAndTripInformation.Where(dti => dti.IsLongDistanceDriver != isLongDistance));

            BroadcastMessage(new DispatcherEventArgs() { msgType = isLongDistance ? MessageType.TripDriverResetLongDistance : MessageType.TripDriverResetLocal });
        }

        public string[] GetLogOnOffLog()
        {
            return this.logOnOffLog.ToArray();
        }


        #endregion

        #region private methods

        /// <summary>
        /// This method is called when ever one of the chatters
        /// ChatEventHandler delegates is invoked. When this method
        /// is called it will examine the events ChatEventArgs to see
        /// what type of message is being broadcast, and will then
        /// call the correspoding method on the clients callback interface
        /// </summary>
        /// <param name="sender">the sender, which is not used</param>
        /// <param name="e">The ChatEventArgs</param>
        private void EventHandler(object sender, DispatcherEventArgs e)
        {
            try
            {
                switch (e.msgType)
                {
                    case MessageType.TripAdded:
                        callback.TripAdded(e.TripAffected, e.byUserID);
                        break;
                    case MessageType.TripEdited:
                        callback.TripEdited(e.TripAffected);
                        break;
                    case MessageType.TripRemoved:
                        callback.TripRemoved(e.TripAffected);
                        break;
                    case MessageType.DriverAdded:
                        callback.DriverAdded(e.driverAndTripInfo);
                        break;
                    case MessageType.DriverEdited:
                        callback.DriverEdited(e.driverAndTripInfo);
                        break;
                    case MessageType.DriverRemoved:
                        callback.DriverRemoved(e.driverAndTripInfo);
                        break;
                    case MessageType.TripDriverResetLocal:
                        callback.TrisAndDriversWereReset(false);
                        break;
                    case MessageType.TripDriverResetLongDistance:
                        callback.TrisAndDriversWereReset(true);
                        break;
                    default:
                        break;
                }
            }
            catch { }
        }

        public AssignedUnitDiscrepencyOption AssignedUnitDiscrepencyOption()
        {
            var dispatchCallBack = OperationContext.Current.GetCallbackChannel<IDispatcherCallBack>();
            var assignedUnitOptionSelected = dispatchCallBack.GetAssignedUnitOptions();

            return assignedUnitOptionSelected;
        }

        private void BroadcastMessage(DispatcherEventArgs e)
        {
            foreach (User key in dispatchers.Keys)
            {
                IDispatcherCallBack callback = dispatchers[key];

                try
                {
                    switch (e.msgType)
                    {
                        case MessageType.TripAdded:
                            Action<Trip, int> actionTripAdded = (trip, userID) => callback.TripAdded(trip, userID);
                            actionTripAdded.BeginInvoke(e.TripAffected, e.byUserID, null, null);
                            break;
                        case MessageType.TripEdited:
                            Action<Trip> actionTripEdited = (trip) => callback.TripEdited(trip);
                            actionTripEdited.BeginInvoke(e.TripAffected, null, null);
                            break;
                        case MessageType.TripLocationAdded:
                            ((Action<Trip>)callback.TripLocationAdded).BeginInvoke(e.TripAffected, null, null);
                            break;
                        case MessageType.TripRemoved:
                            Action<Trip> actionTripRemoved = (trip) => callback.TripRemoved(trip);
                            actionTripRemoved.BeginInvoke(e.TripAffected, null, null);
                            break;
                        case MessageType.DriverAdded:
                            Action<DriverAndTripInformation> actionDriverAdded = (driver) => callback.DriverAdded(driver);
                            actionDriverAdded.BeginInvoke(e.driverAndTripInfo, null, null);
                            break;
                        case MessageType.DriverEdited:
                            Action<DriverAndTripInformation> actionDriverEdited = (driver) => callback.DriverEdited(driver);
                            actionDriverEdited.BeginInvoke(e.driverAndTripInfo, null, null);
                            break;
                        case MessageType.DriverRemoved:
                            Action<DriverAndTripInformation> actionDriverRemoved = (driver) => callback.DriverRemoved(driver);
                            actionDriverRemoved.BeginInvoke(e.driverAndTripInfo, null, null);
                            break;
                        case MessageType.TripDriverResetLongDistance:
                            Action<bool> actionTripDriverReset = (longDistance) => callback.TrisAndDriversWereReset(longDistance);
                            actionTripDriverReset.BeginInvoke(true, null, null);
                            break;
                        case MessageType.TripDriverResetLocal:
                            Action<bool> actionTripDriverResetlocal = (longDistance) => callback.TrisAndDriversWereReset(longDistance);
                            actionTripDriverResetlocal.BeginInvoke(false, null, null);
                            break;
                        case MessageType.DriversAndTripInfoReset:
                            Action<DriverAndTripInformation[]> actionDriversTripInfoReset = (driversAndTripInformation) => callback.DriversAndTripInfoReset(driversAndTripInformation);
                            actionDriversTripInfoReset.BeginInvoke(dispatchingInfo.DriversAndTripInformation.ToArray(), null, null);
                            break;
                        case MessageType.CarsAvailabilitChanged:
                            Action<SectionCarsAvailability> actionCarsAvailabilitChanged = (sectionCarsAvailability) => callback.SectionCarAvailabilityChanged(sectionCarsAvailability);
                            actionCarsAvailabilitChanged.BeginInvoke(e.SectionAffected, null, null);
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception) { }
            }

            try
            {
                switch (e.msgType)
                {
                    case MessageType.TripAdded:
                        if (e.TripAffected.DrivenBy != null)
                            this.SendEmailToDriver(e.TripAffected, e.TripAffected.DrivenBy, true);
                        break;
                    case MessageType.TripEdited:
                        if (e.TripAffected.DrivenBy != null)
                            this.SendEmailToDriver(e.TripAffected, e.TripAffected.DrivenBy, false);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }


        /// <summary>
        /// Is called as a callback from the asynchronous call, so simply get the
        /// delegate and do an EndInvoke on it, to signal the asynchronous call is
        /// now completed
        /// </summary>
        /// <param name="ar">The asnch result</param>
        private void EndAsync(IAsyncResult ar)
        {
            DispatcherEventHandler d = null;

            try
            {
                //get the standard System.Runtime.Remoting.Messaging.AsyncResult,and then
                //cast it to the correct delegate type, and do an end invoke
                System.Runtime.Remoting.Messaging.AsyncResult asres = (System.Runtime.Remoting.Messaging.AsyncResult)ar;
                d = ((DispatcherEventHandler)asres.AsyncDelegate);
                d.EndInvoke(ar);
            }
            catch
            {
                DispatchEvent -= d;
            }
        }

        #endregion

        void IDispatcher.ProcessScheduledTrip()
        {
            this.GetScheduledLongDistanceTrips();
        }

        public void UpdateSectionCarAvailability(string sectionName, CarsAvailabilityTypes availability)
        {
            var sToChange = dispatchingInfo.SectionsCarsAvailabilityStatus.Where(s => s.SectionName == sectionName).FirstOrDefault();
            if (sToChange != null)
            {
                sToChange.Availability = availability;
                BroadcastMessage(new DispatcherEventArgs() { msgType = MessageType.CarsAvailabilitChanged, SectionAffected = sToChange });
            }
        }

        public SectionCarsAvailability[] GetAllSectionsCarAvailability()
        {
            SectionCarsAvailability[] list = new SectionCarsAvailability[dispatchingInfo.SectionsCarsAvailabilityStatus.Count()];
            //carry out a critical section that copy all SectionsCarsAvailabilityStatus to a new list
            lock (syncObj)
            {
                dispatchingInfo.SectionsCarsAvailabilityStatus.CopyTo(list, 0);
            }
            return list;
        }

        
    }

    public delegate void CallToBroadcastDelegate(DispatcherEventArgs e);
    public delegate AssignedUnitDiscrepencyOption AssignedUnitDiscrepencyOptionDelegate();

    public enum AssignedUnitDiscrepencyOption
    {
        FinishedFirst = 0,
        ReassignToNew = 1,
        CanceledFirst = 2,
        Combine = 3,
        Cancel = 4
    }
}
