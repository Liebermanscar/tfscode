﻿using BOL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CDS
{
    public class DispatchingToProxy : INotifyPropertyChanged, IDispatchingMVVM, DispatcherService.IDispatcherCallBack
    {

        #region IDispatchingMVVM Members

        public event TripChanged_EventHandler On_TripBroadcasted;

        string _CallerIDInfo;

        public string CallerIDInfo
        {
            get { return _CallerIDInfo; }
            set { _CallerIDInfo = value; this.OnPropertyChanged("CallerIDInfo"); }
        }
        private bool _disposed;

        DispatcherService.IDispatcher _dispatcherClient;

        public DispatchingToProxy(SelectTripInListCallback selectTripInList, bool isLongDistance)
        {
            this.DriversAndTripInformation = new ObservableCollection<DriverAndTripInformation>();
            this.Trips = new ObservableCollection<Trip>();
            this.StreetAbbreviations = new ObservableCollection<StreetAbbreviation>();
            this.SectionsCarsAvailabilityStatus = new ObservableCollection<SectionCarsAvailability>();
            this.DriversPermissionRequests = new ObservableCollection<DriverPermissionRequest>();

            this.IsLongDistanceMode = isLongDistance;

            _selectTripInList = selectTripInList;

            var signedIn = this.ConnectToDispatcherService();

            if (signedIn)
            {
                var allTrips = this.Dispatcher.GetAllTrips();
                this.Trips.AddRange(allTrips.Where(t => t.IsLongDistanceTrip == this.IsLongDistanceMode));
                this.DriversAndTripInformation.AddRange(this.Dispatcher.GetAllDriversAndTripInfo().Where(dti => dti.IsLongDistanceDriver == this.IsLongDistanceMode));
                this.SectionsCarsAvailabilityStatus.AddRange(this.Dispatcher.GetAllSectionsCarAvailability());
            }
            else
            {
                MessageBox.Show("User can not connect to server");
                App.Current.Shutdown();
            }
        }

        SelectTripInListCallback _selectTripInList;

        static Uri GetDispatcherServiceAddress()
        {
            return new Uri(string.Format("net.tcp://{0}:{1}/DispatcherService/DispatchService", Properties.Settings.Default.DispatcherServerIPAddress, Properties.Settings.Default.DispatcherServicePort)); ;
        }

        bool ConnectToDispatcherService()
        {
            bool signedIn = false;

            if (_dispatcherClient == null || ((IClientChannel)_dispatcherClient).State == (CommunicationState.Closed | CommunicationState.Faulted))
            {
                var binding = new NetTcpBinding(SecurityMode.None)
                {
                    SendTimeout = new TimeSpan(0, 0, 30),
                    ReceiveTimeout = TimeSpan.MaxValue,
                    MaxBufferPoolSize = 2147483647,
                    MaxBufferSize = 2147483647,
                    MaxReceivedMessageSize = 2147483647,
                    ReliableSession = new OptionalReliableSession() { Enabled = true, Ordered = true, InactivityTimeout = TimeSpan.MaxValue }
                };

                var endpoint = new EndpointAddress(GetDispatcherServiceAddress());
                var client = new DuplexChannelFactory<DispatcherService.IDispatcher>((DispatcherService.IDispatcherCallBack)this, binding, endpoint).CreateChannel();

                signedIn = client.DispatcherJoin(UIController.CurrentSession.SessionUser, 1.0);
                _dispatcherClient = client;

                //((IClientChannel)client).Faulted += (object sender, EventArgs e) =>
                //{
                //    //UIController.ClientChannelState = CommunicationState.Faulted;
                //    InvokeOnUIThread(() => UIController.ShowMessageBox("Faulted"));
                //    SubscribeToMKVControler();
                //};
            }

            return signedIn;
        }

        public DispatcherService.IDispatcher Dispatcher
        {
            get
            {
                if (_dispatcherClient == null)
                    this.ConnectToDispatcherService();

                if (((IClientChannel)_dispatcherClient).State == (CommunicationState.Faulted | CommunicationState.Closed))
                {
                    if (((IClientChannel)_dispatcherClient).State == CommunicationState.Faulted)
                        ((IClientChannel)_dispatcherClient).Abort();

                    this.ConnectToDispatcherService();
                }

                return _dispatcherClient;
            }
        }

        public void AddEditTrip(Trip trip)
        {
            if (trip == null)
            {
                trip = new Trip()
                {
                    ObjectState = BOState.New,
                    TripEnteredBy = TripEnteredOptions.ByDispatcher,
                    VehicleTypeRequested = VehicleTypes.Not_Specified
                };
            }

            this.AddedEditedTrip = trip;
            this.OnPropertyChanged("AddEditTripIsVisible");
        }

        public void AddTripLocation(Trip trip)
        {
            this.Dispatcher.AddTripLocation(trip);
        }

        public void TripLocationAdded(Trip trip)
        {
            if (On_TripBroadcasted != null)
                On_TripBroadcasted(trip, DispatcherService.MessageType.TripLocationAdded);
        }

        public Visibility AddEditTripIsVisible
        {
            get { throw new NotImplementedException(); }
        }

        public void AddTripToList(Trip tripToAdd)
        {
            tripToAdd.IsLongDistanceTrip = this.IsLongDistanceMode;

            this.Dispatcher.AddTrip(tripToAdd, UIController.CurrentSession.SessionUser.UserID, this.IsLongDistanceMode);
        }

        public bool AssignDriverToTrip(Trip trip, Driver driver, bool warnDriverChanged = true)
        {
            if (driver != null && trip.TripStatus != TripStatusTypes.Complete)
            {
                DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).FirstOrDefault();

                if (dti == null)
                {
                    this.Dispatcher.AddDriver(driver, "", this.IsLongDistanceMode);
                    this.Dispatcher.AssignDriverToTrip(trip, driver, warnDriverChanged);
                    return true;
                }

                if (dti != null)
                {
                    if (dti.AssignedToTrip != null)
                    {
                        //AssignedUnitDiscrepencyOption optionSelected = this.GetAssignedUnitOptions();

                        this.Dispatcher.AssignDriverToTrip(trip,
                            driver,
                            false);

                        return true;
                    }
                    else
                    {
                        if (trip.DrivenBy != null && warnDriverChanged)
                        {
                            if (MessageBox.Show("You're about to replace the driver of this trip.\n\nContinue?", "Replace Driver?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                                this.Dispatcher.ChangeTripStatus(trip, TripStatusTypes.ReceivedCall, null, true);
                            else
                                return false;
                        }
                        this.Dispatcher.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Driver has not Signed in", "NOT Signed in", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

            }
            else
                return false;


        }

        AssignedUnitDiscrepencyOption GetAssignedUnitOptions()
        {
            AssignUnitDiscepencyWindow audw = new AssignUnitDiscepencyWindow();
            if (audw.ShowDialog().GetValueOrDefault(false))
                return (AssignedUnitDiscrepencyOption)audw.DiscrepencyOptionsListBox.SelectedIndex;
            else
                return AssignedUnitDiscrepencyOption.Cancel;
        }

        public void ChangeDriversStatus(Driver driver, DriverStatusOptions assignStatus)
        {
            DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).First();
            if (dti != null)
            {
                this.Dispatcher.EditDriverStatus(driver, assignStatus);
            }
        }

        public void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced)
        {
            if (trip != null)
                this.Dispatcher.ChangeTripStatus(trip, status, driver, driverReplaced);

            this.Trips = new ObservableCollection<Trip>(from t in this.Trips
                                                        orderby t.TripStatus descending,
                                                        t.TripStatus == TripStatusTypes.Assigned ? t.TimeDriverAssigned : t.ScheduledFor != null ? t.ScheduledFor : t.TimeOfCall ascending
                                                        select t);
            this.OnPropertyChanged("Trips");
        }

        ObservableCollection<DriverAndTripInformation> _driversAndTripInformation;

        public ObservableCollection<DriverAndTripInformation> DriversAndTripInformation
        {
            get { return _driversAndTripInformation; }
            set
            {
                _driversAndTripInformation = value;
                value.CollectionChanged += delegate (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
                {
                    this.OnPropertyChanged("SortedDriversAndTrips");
                };
            }
        }

        public ObservableCollection<Driver> DriversOnBreak
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         where dt.DriverStatus == DriverStatusOptions.LongBreak || dt.DriverStatus == DriverStatusOptions.ShortBreak
                                                         select dt.UnitDriver));
            }
        }

        public void MarkDriverForBreak(Driver driver, DriverStatusOptions breakType)
        {
            Trip completeFirstTrip = (from d in this.DriversAndTripInformation
                                      where d.UnitDriver.DriverID == driver.DriverID
                                      select d.AssignedToTrip).ToList()[0];
            if (completeFirstTrip != null)
                this.Dispatcher.ChangeTripStatus(completeFirstTrip, TripStatusTypes.Complete, driver, false);

            this.Dispatcher.EditDriverStatus(driver, breakType);
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public void RefereshStatusTimesOnDrivers()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Driver> SignedInDrivers
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         select dt.UnitDriver));
            }
        }

        public DriverAndTripInformation SignInDriver(Driver driver, string signInLocation)
        {
            var driversAndT = this.Dispatcher.GetAllDriversAndTripInfo();

            if (driversAndT.Count(sid => sid.UnitDriver.DriverID == driver.DriverID) == 0)
            {
                this.Dispatcher.AddDriver(driver, signInLocation, this.IsLongDistanceMode);
                this.OnPropertyChanged("SignedInDrivers");
            }

            return null;
        }

        public ObservableCollection<DriverAndTripInformation> SortedDriversAndTrips
        {
            get
            {
                var list = new ObservableCollection<DriverAndTripInformation>(
                    from d in this.DriversAndTripInformation
                    orderby d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                    where d.DriverStatus == DriverStatusOptions.NintyEight || d.DriverStatus == DriverStatusOptions.ShortBreak
                    select d);
                list.AddRange(from d in this.DriversAndTripInformation
                              orderby d.DriverStatus, d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                              where d.DriverStatus == DriverStatusOptions.Assigned
                              select d);
                list.AddRange(from d in this.DriversAndTripInformation
                              orderby d.DriverStatus, d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                              where d.DriverStatus == DriverStatusOptions.LongTrip
                              select d);
                list.AddRange(from d in this.DriversAndTripInformation
                              orderby d.DriverStatus, d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                              where d.DriverStatus == DriverStatusOptions.LongBreak
                              select d);
                return list;
            }
        }

        public ObservableCollection<Trip> SortedTrips
        {
            get
            {
                return new ObservableCollection<Trip>(from t in this.Trips
                                                      orderby t.TripStatus descending,
                                                      t.TripStatus == TripStatusTypes.Assigned ? t.TimeDriverAssigned : t.ScheduledFor != null ? t.ScheduledFor : t.TimeOfCall ascending
                                                      select t);
            }
        }

        ObservableCollection<StreetAbbreviation> _streetAbbreviations;

        public ObservableCollection<StreetAbbreviation> StreetAbbreviations
        {
            get { return _streetAbbreviations; }
            set { _streetAbbreviations = value; this.OnPropertyChanged("StreetAbbreviations"); }
        }

        ObservableCollection<Trip> _trips;

        public ObservableCollection<Trip> Trips
        {
            get { return _trips; }
            set { _trips = value; this.OnPropertyChanged("Trips"); }
        }

        ObservableCollection<SectionCarsAvailability> _sectionsBusyStatus;

        public ObservableCollection<SectionCarsAvailability> SectionsCarsAvailabilityStatus
        {
            get { return _sectionsBusyStatus; }
            set { _sectionsBusyStatus = value; }
        }

        Trip _addedEditedTrip;

        public Trip AddedEditedTrip
        {
            get { return _addedEditedTrip; }
            set
            {
                _addedEditedTrip = value;
                this.OnPropertyChanged("AddedEditedTrip");
                this.OnPropertyChanged("AddEditTripIsVisible");
            }
        }

        public void Undo98Status(DriverAndTripInformation dti)
        {
            this.Dispatcher.Undo98Status(dti.UnitDriver);
        }

        public void CancelAddEditTrip()
        {
            this.AddedEditedTrip = null;
        }

        public void SignOutDriver(Driver driver)
        {
            this.Dispatcher.RemoveDriver(driver);
        }

        public void ResetDispatchingInformation()
        {
            this.Dispatcher.ResetTripsAndDrivers(this.IsLongDistanceMode);
        }

        public void DeleteTrip(Trip trip)
        {
            this.Dispatcher.RemoveTrip(trip, true);
        }

        public void EditTrip(Trip trip)
        {
            if (trip != null)
            {
                this.Dispatcher.EditTrip(trip);
            }
        }

        public string[] GetLogOnOff()
        {
            return this.Dispatcher.GetLogOnOffLog();
        }

        public void ChangeDriverLocation(DriverAndTripInformation driverAndTripInformation, string newLocation)
        {
            driverAndTripInformation.CalledInLocation = newLocation;
            this.Dispatcher.EditDriver(driverAndTripInformation);
        }

        public void UpdateDriverTripTimers()
        {
            var array = this.DriversAndTripInformation.ToArray();
            this.Dispatcher.UpdateDriverTripTimers(array);
        }

        bool _isLongDistanceMode;

        public bool IsLongDistanceMode
        {
            get { return _isLongDistanceMode; }
            set { _isLongDistanceMode = value; }
        }
        bool _playSoundsForNewTrips;

        public bool PlaySoundsForNewTrips
        {
            get { return _playSoundsForNewTrips; }
            set { _playSoundsForNewTrips = value; }
        }
        #endregion

        #region IDispatcherCallback Members

        public void TripAdded(Trip trip, int byUserID)
        {
            if (this.IsLongDistanceMode != trip.IsLongDistanceTrip)
                return;

            this.Trips.Add(trip);

            if (On_TripBroadcasted != null)
                On_TripBroadcasted(trip, DispatcherService.MessageType.TripAdded);

            this.Trips = new ObservableCollection<Trip>(from t in this.Trips
                                                        orderby t.TripStatus descending,
                                                        t.TripStatus == TripStatusTypes.Assigned ? t.TimeDriverAssigned :
                                                        t.ScheduledFor != null ? t.ScheduledFor.Value.AddMinutes(-10) ://add to request at -10 minutes so when its 10 minutes before the reqest at time at should be more on top in list so dispatcher will know to send car
                                                        t.TimeOfCall ascending
                                                        select t);

            if (trip.TakenByDispatcherID != UIController.CurrentSession.SessionUser.UserID)
            {
                this.PlaySound(trip);
            }

            if (_selectTripInList != null)
            {
                _selectTripInList.Invoke(trip, UIController.CurrentSession.SessionUser.UserID == byUserID);
            }
        }

        private string fileToPlay = "";
        void PlaySound(Trip forTrip)
        {
            try
            {
                if (PlaySoundsForNewTrips == false) return;
                if (Trip.IsLongDistance == false)//local play one file and by long distance play adiifrent file
                {
                    fileToPlay = @"Sounds\localTripSound.wav";
                    (new SoundPlayer(fileToPlay)).Play();
                }
                if (Trip.IsLongDistance == true)
                {
                    fileToPlay = @"Sounds\longDistanceTripSound.wav";
                    (new SoundPlayer(fileToPlay)).Play();
                }
            }

            catch (System.IO.FileNotFoundException ex)
            {
                MessageBox.Show("Could not file file: " + fileToPlay + "\n" + ex.Message);
            }
            catch (System.InvalidOperationException ex)
            {
                MessageBox.Show("File was not a valid .WAV file: " + fileToPlay + "\n" + ex.Message);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public void TripEdited(Trip trip)
        {
            if (this.IsLongDistanceMode != trip.IsLongDistanceTrip)
                return;

            Trip tripInList = this.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();

            if (tripInList == null)
                return;

            var tripIndex = this.Trips.IndexOf(tripInList);

            if (trip != null && tripIndex >= 0)
                this.Trips[tripIndex] = trip;

            if (On_TripBroadcasted != null)
                On_TripBroadcasted(trip, DispatcherService.MessageType.TripEdited);
        }

        public void TripRemoved(Trip trip)
        {
            if (this.IsLongDistanceMode != trip.IsLongDistanceTrip)
                return;

            Trip tripInList = this.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();
            if (tripInList != null)
                this.Trips.Remove(tripInList);

            if (On_TripBroadcasted != null)
                On_TripBroadcasted(trip, DispatcherService.MessageType.TripRemoved);
        }

        public void DriverAdded(DriverAndTripInformation driver)
        {
            if (driver.IsLongDistanceDriver != this.IsLongDistanceMode)
                return;

            this.DriversAndTripInformation.Add(driver);
        }

        public void DriverEdited(DriverAndTripInformation driver)
        {
            if (driver.IsLongDistanceDriver != this.IsLongDistanceMode)
                return;

            var dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.UnitDriver.DriverID).FirstOrDefault();
            if (dti == null)
                return;
            var dtiIndex = this.DriversAndTripInformation.IndexOf(dti);
            if (dtiIndex >= 0)
                this.DriversAndTripInformation[dtiIndex] = driver;

            this.OnPropertyChanged("SortedDriversAndTrips");
        }

        public void DriverRemoved(DriverAndTripInformation driver)
        {
            if (driver.IsLongDistanceDriver != this.IsLongDistanceMode)
                return;
            var dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.UnitDriver.DriverID).FirstOrDefault();
            if (dti != null)
                this.DriversAndTripInformation.Remove(dti);
        }

        DispatcherService.AssignedUnitDiscrepencyOption DispatcherService.IDispatcherCallBack.GetAssignedUnitOptions()
        {
            return (DispatcherService.AssignedUnitDiscrepencyOption)this.GetAssignedUnitOptions();
        }

        public void TrisAndDriversWereReset(bool isLongDistance)
        {
            if (isLongDistance != this.IsLongDistanceMode)
                return;

            this.Trips.Clear();
            this.DriversAndTripInformation.Clear();

            MessageBox.Show("Dispatching information was reset by server");
        }

        public void DriversAndTripInfoReset(DriverAndTripInformation[] driverAndTripsArray)
        {
            this.DriversAndTripInformation.Clear();
            this.DriversAndTripInformation.AddRange(driverAndTripsArray.Where(dti => dti.IsLongDistanceDriver == this.IsLongDistanceMode));

            this.OnPropertyChanged("SortedDriversAndTrips");
        }

        public void ProcessScheduledTrip()
        {
            this.Dispatcher.ProcessScheduledTrip();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed)
                return;


            if (_dispatcherClient != null && ((IClientChannel)_dispatcherClient).State == CommunicationState.Opened)
            {
                _dispatcherClient.DispatcherLeave(UIController.CurrentSession.SessionUser);
                ((IClientChannel)_dispatcherClient).Close();
            }

            _disposed = true;
        }

        #endregion

        public void SectionCarAvailabilityChanged(SectionCarsAvailability sectionChanged)
        {
            try
            {
                var cas = this.SectionsCarsAvailabilityStatus.Where(c => c.SectionName == sectionChanged.SectionName).FirstOrDefault();

                cas.Availability = sectionChanged.Availability;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public void UpdateSectionCarAvailability(string sectionName, CarsAvailabilityTypes availability)
        {
            try
            {
                this.Dispatcher.UpdateSectionCarAvailability(sectionName, availability);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        public ObservableCollection<DriverPermissionRequest> DriversPermissionRequests { get; set; }
    }

}
