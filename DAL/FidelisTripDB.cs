﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace DAL
{
    public class FidelisTripDB : Database
    {
        public FidelisTripDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(ObservableCollection<FidelisTrip> fidelisTrips)
        {
            this._localTransaction = true;
            this.Transaction = this.Connection.BeginTransaction();
            int noOfRowsInsetrted = 0;
            try
            {
                foreach (FidelisTrip ft in fidelisTrips)
                {
                    string serverMessage;
                    var outParam = new SqlParameter("@OutParam", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    noOfRowsInsetrted = this.ExecuteNonQuery("AddFidelisTrips", new SqlParameter[]
                    {
                        new SqlParameter("@AuthNo", ft.AuthNo),
                        new SqlParameter("@GroupID", ft.GroupID),
                          new SqlParameter("@MemberID", ft.MemberID),
                          new SqlParameter("@MemberName", ft.MemberName),
                          new SqlParameter("@MemberAddress", ft.MemberAddress),
                          new SqlParameter("@MemberCityStatePostalCode", ft.MemberCityStatePostalCode),
                          new SqlParameter("@MemberPhoneNo", ft.MemberPhoneNo),
                          new SqlParameter("@MemberDOB", ft.MemberDOB),
                          new SqlParameter("@MemberGender", ft.MemberGender),
                          new SqlParameter("@NoOfRiders", ft.NoOfRiders),
                          new SqlParameter("@DestinationTitle", ft.DestinationTitle),
                          new SqlParameter("@DestinationAddress", ft.DestinationAddress),
                          new SqlParameter("@DestinationCityStatePostalCode", ft.DestinationCityStatePostalCode),
                          new SqlParameter("@Comments", ft.Comments),
                          new SqlParameter("@TripScheduledOn", ft.TripScheduledOn),
                          new SqlParameter("@UserName", ft.UserName),
                          new SqlParameter("@SessionID", Database.SessionID),
                          outParam
                    }, System.Data.CommandType.StoredProcedure, out serverMessage);
                    if (noOfRowsInsetrted > 0)
                        ft.FidelisTripID = Convert.ToInt32(outParam.Value);
                }
                this.Transaction.Commit();
                return noOfRowsInsetrted > 0;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }
        public ObservableCollection<FidelisTrip> Get(int fidelisTripID, DateTime? tripDate)
        {
            var fidelisTrips = new ObservableCollection<FidelisTrip>();
            using (SqlDataReader dr = this.GetReader("GetFidelisTrips",
                new SqlParameter[]
                {
                    new SqlParameter("@FidelisTripID", fidelisTripID),
                new SqlParameter("@TripDate", tripDate)
                }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    fidelisTrips.Add(new FidelisTrip()
                    {
                        FidelisTripID = Convert.ToInt32(dr["FidelisTripID"]),
                        AuthNo = dr["AuthNo"].ToString(),
                        GroupID = dr["GroupID"].ToString(),
                        MemberID = dr["MemberId"].ToString(),
                        MemberName = dr["MemberName"].ToString(),
                        MemberAddress = dr["MemberAddress"].ToString(),
                        MemberCityStatePostalCode = dr["MemberCityStatePostalCode"].ToString(),
                        MemberPhoneNo = dr["MemberPhoneNo"].ToString(),
                        MemberDOB = dr["MemberDOB"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["MemberDOB"]),
                        MemberGender = Convert.ToChar(dr["MemberGender"]),
                        NoOfRiders = Convert.ToInt32(dr["NoOfRiders"]),
                        DestinationTitle = dr["DestinationTitle"].ToString(),
                        DestinationAddress = dr["DestinationAddress"].ToString(),
                        DestinationCityStatePostalCode = dr["DestinationCityStatePostalCode"].ToString(),
                        Comments = dr["Comments"].ToString(),
                        TripScheduledOn = dr["TripScheduledOn"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["TripScheduledOn"]),
                        UserName = dr["UserName"].ToString()
                    });
                }
            }

            return fidelisTrips;
        }
        public static bool AddFidelisTrips(ObservableCollection<FidelisTrip> fidelisTrips)
        {
            using (FidelisTripDB ftDB = new FidelisTripDB(null, null))
            {
                return ftDB.Add(fidelisTrips);
            }
        }
        public static ObservableCollection<FidelisTrip> GetFidelisTrips(int fidelisTripID, DateTime? tripDate)
        {
            using (FidelisTripDB ftDB = new FidelisTripDB(null, null))
            {
                return ftDB.Get(fidelisTripID, tripDate);
            }
        }
        public static bool FileWasImported(DateTime tripDate)
        {
            object o;
            using (FidelisTripDB ftDB = new FidelisTripDB(null, null))
            {
                o = ftDB.ExecuteScalar("CheckFileWasAllreadyImported",
                    new SqlParameter[] { new SqlParameter("@Fidelis", true), new SqlParameter("@TripDate", tripDate) },
                    System.Data.CommandType.StoredProcedure);
            }
            var imported = Convert.ToBoolean(o);

            return imported;

        }

        public static bool EditFidelis(FidelisTrip fidelisTrip)
        {
            using (FidelisTripDB ftDB = new FidelisTripDB(null, null))
            {
                return ftDB.Edit(fidelisTrip);
            }
        }

        private bool Edit(FidelisTrip fidelisTrip)
        {
            throw new NotImplementedException();
        }
    }

    public class MedicaidTripDB : Database
    {
        public MedicaidTripDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(ObservableCollection<MedicaidTrip> medicaidTrips)
        {
            this._localTransaction = true;
            this.Transaction = this.Connection.BeginTransaction();
            int noOfRowsInsetrted = 0;
            try
            {
                foreach (MedicaidTrip mt in medicaidTrips)
                {
                    string serverMessage;
                    var outParam = new SqlParameter("@MedicaidTripID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    noOfRowsInsetrted = this.ExecuteNonQuery("AddMedicaidTrips", new SqlParameter[]
                   {
                        new SqlParameter("@ExportID", mt.ExportID),
                        new SqlParameter("@RecordNo", mt.RecordNo),
                        new SqlParameter("@InvoiceNo", mt.InvoiceNo),
                        new SqlParameter("@RecordType", mt.RecordType),
                        new SqlParameter("@FirstName", mt.FirstName),
                        new SqlParameter("@MiddleInitial", mt.MiddleInitial),
                        new SqlParameter("@LastName", mt.LastName),
                        new SqlParameter("@CIN", mt.CIN),
                        new SqlParameter("@PhoneNo", mt.PhoneNo),
                        new SqlParameter("@BirthDate", mt.BirthDate),
                        new SqlParameter("@MedicalProvider", mt.MedicalProvider),
                        new SqlParameter("@ProviderID", mt.ProviderID),
                        new SqlParameter("@TransportCompany", mt.TransportCompany),
                        new SqlParameter("@TransportType", mt.TransportType),
                        new SqlParameter("@ProcedureCode", mt.ProcedureCode),
                        new SqlParameter("@ServiceStarts", mt.ServiceStarts),
                        new SqlParameter("@ServiceEnds", mt.ServiceEnds),
                        new SqlParameter("@StandingOrder", mt.StandingOrder),
                        new SqlParameter("@TripsApproved", mt.TripsApproved),
                        new SqlParameter("@DaysApproved ", mt.DaysApproved),
                        new SqlParameter("@Wheelchair", mt.WheelChair),
                        new SqlParameter("@PickupDate", mt.PickUpDate),
                        new SqlParameter("@PickupTime", mt.PickUpTime),
                        new SqlParameter("@PickupAddress", mt.PickUpAddress),
                        new SqlParameter("@PickupCity", mt.PickUpCity),
                        new SqlParameter("@PickUpState", mt.PickupState),
                        new SqlParameter("@PickupZip", mt.PickUpZip),
                        new SqlParameter("@DropOffDate", mt.DropOffDate),
                        new SqlParameter("@DropOffTime", mt.DropOffTime),
                        new SqlParameter("@DropOffAddress", mt.DropOffAddress),
                        new SqlParameter("@DropOffCity", mt.DropOffCity),
                        new SqlParameter("@DropOffState", mt.DropOffState),
                        new SqlParameter("@DropOffZip", mt.DropOffZip),
                        new SqlParameter("@Instructions", mt.Instructions),
                        new SqlParameter("@SecondaryService", mt.SecondaryService),
                        new SqlParameter("@IsManualEntry",mt.IsManualEntry),
                        new SqlParameter("@SessionID",  Database.SessionID),
                          outParam
                   }, System.Data.CommandType.StoredProcedure, out serverMessage);
                    if (noOfRowsInsetrted > 0)
                        mt.MedicaidTripID = Convert.ToInt32(outParam.Value);
                }
                this.Transaction.Commit();
                return noOfRowsInsetrted > 0;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }
        public ObservableCollection<MedicaidTrip> Get(int exportID, int medicaidTripID, DateTime? pickupDate, bool legOnly = true, bool forSubmit = false, string procedureCode = "", bool unBilledOnly = false)
        {
            ObservableCollection<MedicaidTrip> medicaidTrips = new ObservableCollection<MedicaidTrip>();
            using (var dr = this.GetReader("GetMedicaidTrips", new SqlParameter[]
            {
                  new SqlParameter("@ExportID", exportID),
                  new SqlParameter("@LegOnly", legOnly),
                  new SqlParameter("@ForSubmit", forSubmit),
                  new SqlParameter("@MedicaidTripID", medicaidTripID),
                  new SqlParameter("@PickupDate",pickupDate),
                  new SqlParameter("@ProcedureCode", procedureCode),
                  new SqlParameter("@UnBilledOnly", unBilledOnly)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    medicaidTrips.Add(new MedicaidTrip()
                    {
                        MedicaidTripID = Convert.ToInt32(dr["MedicaidTripID"]),
                        ExportID = Convert.ToInt32(dr["ExportID"]),
                        RecordNo = Convert.ToInt32(dr["RecordNo"]),
                        InvoiceNo = Convert.ToInt32(dr["InvoiceNo"]),
                        RecordType = dr["RecordType"].ToString(),
                        FirstName = dr["FirstName"].ToString(),
                        MiddleInitial = dr["MiddleInitial"].ToString(),
                        LastName = dr["lastName"].ToString(),
                        CIN = dr["CIN"].ToString(),
                        PhoneNo = dr["PhoneNo"].ToString(),
                        BirthDate = dr["BirthDate"].ToString(),
                        MedicalProvider = dr["MedicalProvider"].ToString(),
                        ProviderID = dr["ProviderID"].ToString(),
                        TransportCompany = dr["TransportCompany"].ToString(),
                        TransportType = dr["TransportType"].ToString(),
                        ProcedureCode = dr["ProcedureCode"].ToString(),
                        ServiceStarts = dr["ServiceStarts"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ServiceStarts"]),
                        ServiceEnds = dr["ServiceEnds"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ServiceEnds"]),
                        StandingOrder = Convert.ToBoolean(dr["StandingOrder"]),
                        TripsApproved = Convert.ToInt32(dr["TripsApproved"]),
                        DaysApproved = Convert.ToInt32(dr["DaysApproved"]),
                        WheelChair = dr["Wheelchair"].ToString(),
                        PickUpDate = dr["PickupDate"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["PickupDate"]),
                        PickUpTime = dr["PickupTime"] == DBNull.Value ? new Nullable<TimeSpan>() : (TimeSpan)dr["PickupTime"],
                        PickUpAddress = dr["PickupAddress"].ToString(),
                        PickUpCity = dr["PickupCity"].ToString(),
                        PickupState = dr["PickUpState"].ToString(),
                        PickUpZip = dr["PickupZip"].ToString(),
                        DropOffDate = dr["DropOffDate"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["DropOffDate"]),
                        DropOffTime = dr["DropOffTime"] == DBNull.Value ? new Nullable<TimeSpan>() : (TimeSpan)dr["DropOffTime"],
                        DropOffAddress = dr["DropOffAddress"].ToString(),
                        DropOffCity = dr["DropOffCity"].ToString(),
                        DropOffState = dr["DropOffState"].ToString(),
                        DropOffZip = dr["DropOffZip"].ToString(),
                        Instructions = dr["Instructions"].ToString(),
                        SecondaryService = dr["SecondaryService"].ToString(),
                        PriorApprovalNumber = dr["PriorApprovalNumber"].ToString(),
                        Submitted = Convert.ToBoolean(dr["Submitted"]),
                    });
                }
            }

            return medicaidTrips;
        }
        private bool Edit(MedicaidTrip mt)
        {
            string serverMessage;
            int noOfRowsUpdated = this.ExecuteNonQuery("UpdateMedicaidTrips", new SqlParameter[]
                   {
                        new SqlParameter("@MedicaidTripID", mt.MedicaidTripID),
                        new SqlParameter("@ExportID", mt.ExportID),
                        new SqlParameter("@RecordNo", mt.RecordNo),
                        new SqlParameter("@InvoiceNo", mt.InvoiceNo),
                        new SqlParameter("@RecordType", mt.RecordType),
                        new SqlParameter("@FirstName", mt.FirstName),
                        new SqlParameter("@MiddleInitial", mt.MiddleInitial),
                        new SqlParameter("@LastName", mt.LastName),
                        new SqlParameter("@CIN", mt.CIN),
                        new SqlParameter("@PhoneNo", mt.PhoneNo),
                        new SqlParameter("@BirthDate", mt.BirthDate),
                        new SqlParameter("@MedicalProvider", mt.MedicalProvider),
                        new SqlParameter("@ProviderID", mt.ProviderID),
                        new SqlParameter("@TransportCompany", mt.TransportCompany),
                        new SqlParameter("@TransportType", mt.TransportType),
                        new SqlParameter("@ProcedureCode", mt.ProcedureCode),
                        new SqlParameter("@ServiceStarts", mt.ServiceStarts),
                        new SqlParameter("@ServiceEnds", mt.ServiceEnds),
                        new SqlParameter("@StandingOrder", mt.StandingOrder),
                        new SqlParameter("@TripsApproved", mt.TripsApproved),
                        new SqlParameter("@DaysApproved ", mt.DaysApproved),
                        new SqlParameter("@Wheelchair", mt.WheelChair),
                        new SqlParameter("@PickupDate", mt.PickUpDate),
                        new SqlParameter("@PickupTime", mt.PickUpTime),
                        new SqlParameter("@PickupAddress", mt.PickUpAddress),
                        new SqlParameter("@PickupCity", mt.PickUpCity),
                        new SqlParameter("@PickUpState", mt.PickupState),
                        new SqlParameter("@PickupZip", mt.PickUpZip),
                        new SqlParameter("@DropOffDate", mt.DropOffDate),
                        new SqlParameter("@DropOffTime", mt.DropOffTime),
                        new SqlParameter("@DropOffAddress", mt.DropOffAddress),
                        new SqlParameter("@DropOffCity", mt.DropOffCity),
                        new SqlParameter("@DropOffState", mt.DropOffState),
                        new SqlParameter("@DropOffZip", mt.DropOffZip),
                        new SqlParameter("@Instructions", mt.Instructions),
                        new SqlParameter("@SecondaryService", mt.SecondaryService),
                        new SqlParameter("@SessionID",  Database.SessionID)
                   }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRowsUpdated > 0;
        }
        public bool EditListForSubmit(List<MedicaidTrip> medTrips)
        {
            this._localTransaction = true;
            this.Transaction = this.Connection.BeginTransaction();
            bool success;
            try
            {
                foreach (MedicaidTrip mt in medTrips)
                {
                    success = this.EditForSubmit(mt);
                }
                this.Transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }
        private bool EditForSubmit(MedicaidTrip mt)
        {
            string serverMessage;
            int noOfRowsUpdated = this.ExecuteNonQuery("UpdateMedicaidTripsSubmitted", new SqlParameter[]
                   {
                        new SqlParameter("@InvoiceNo", mt.InvoiceNo),
                        new SqlParameter("@ServiceStarts", mt.ServiceStarts)
                   }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRowsUpdated > 0;
        }

        public static bool EditMedicaidTripForSubmit(MedicaidTrip medicaidTrip)
        {
            using (MedicaidTripDB medicaidTripDB = new MedicaidTripDB(null, null))
            {
                return medicaidTripDB.EditForSubmit(medicaidTrip);
            }
        }

        public static bool AddMedicaidTrips(ObservableCollection<MedicaidTrip> medicaidTrips)
        {
            using (MedicaidTripDB mtDB = new MedicaidTripDB(null, null))
            {
                return mtDB.Add(medicaidTrips);
            }
        }

        public static bool FileWasImported(int exportID)
        {
            object o;
            using (FidelisTripDB ftDB = new FidelisTripDB(null, null))
            {
                o = ftDB.ExecuteScalar("CheckFileWasAllreadyImported",
                    new SqlParameter[] { new SqlParameter("@Fidelis", false), new SqlParameter("@ExportID", exportID) }, System.Data.CommandType.StoredProcedure);
            }
            var imported = Convert.ToBoolean(o);

            return imported;

        }
        public static ObservableCollection<MedicaidTrip> GetMedicaidTrips(int exportID,
            int medicaidTripID,
            DateTime? pickupDate,
            bool legOnly = true,
            bool forSubmit = false,
            string procedureCode = "",
            bool unBilledOnly = false)
        {
            using (var mtDB = new MedicaidTripDB(null, null))
            {
                return mtDB.Get(exportID, medicaidTripID, pickupDate, legOnly, forSubmit, procedureCode, unBilledOnly);
            }
        }

        public static bool EditMedicaidTrip(MedicaidTrip medicaidTrip)
        {
            using (MedicaidTripDB medicaidTripDB = new MedicaidTripDB(null, null))
            {
                return medicaidTripDB.Edit(medicaidTrip);
            }
        }

        public ObservableCollection<MedicaidLoginInfo> getMedicaidLogInInfo()
        {
            ObservableCollection<MedicaidLoginInfo> loglnInfo = new ObservableCollection<MedicaidLoginInfo>();
            using (SqlDataReader dr = this.GetReader
                ("GetMedicaidLoginInfo", new SqlParameter[]
               {
                   // new SqlParameter ("", driverID), 
                   // new SqlParameter("@LoginPassword", IncludeInActive) 
               }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    MedicaidLoginInfo mlogInInfo = new MedicaidLoginInfo()
                    {
                        MLogInID = Convert.ToInt32(dr["MLIID"]),
                        MedicaidLogInUserNmae = dr["LogInUserName"].ToString(),
                        MedicaidLogInPassword = dr["LogInPassword"].ToString(),

                    };

                    loglnInfo.Add(mlogInInfo);
                }

            }

            return loglnInfo;

        }
        public static ObservableCollection<MedicaidLoginInfo> GetMedicaidLogInInfo()
        {
            using (MedicaidTripDB ftDB = new MedicaidTripDB(null, null))
            {
                return ftDB.getMedicaidLogInInfo();
            }

        }

        public bool editMedicaidLogin(MedicaidLoginInfo loginInfo)
        {
            string serverMessage;
            int noOfRowsUpdated = this.ExecuteNonQuery("UpdateMedcaidLoginInfo", new SqlParameter[]
                   {
                        new SqlParameter("@LoginName",loginInfo.MedicaidLogInUserNmae),
                        new SqlParameter("@LoginPassword", loginInfo.MedicaidLogInPassword),


                   }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRowsUpdated > 0;
        }
        public static bool EditMedicaidLogin(MedicaidLoginInfo loginInfo)
        {
            using (MedicaidTripDB ftDB = new MedicaidTripDB(null, null))
            {
                return ftDB.editMedicaidLogin(loginInfo);
            }
        }

        public ObservableCollection<MedicaidLoginInfo> geteMedNYLogInInfo()
        {
            ObservableCollection<MedicaidLoginInfo> loglnInfo = new ObservableCollection<MedicaidLoginInfo>();
            using (SqlDataReader dr = this.GetReader
                ("GeteMedNYLoginInfo", new SqlParameter[]
               {
                   // new SqlParameter ("", driverID), 
                   // new SqlParameter("@LoginPassword", IncludeInActive) 
               }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    MedicaidLoginInfo mlogInInfo = new MedicaidLoginInfo()
                    {
                        MLogInID = Convert.ToInt32(dr["MLIID"]),
                        MedicaidLogInUserNmae = dr["LogInUserName"].ToString(),
                        MedicaidLogInPassword = dr["LogInPassword"].ToString(),

                    };

                    loglnInfo.Add(mlogInInfo);
                }

            }

            return loglnInfo;

        }
        public static ObservableCollection<MedicaidLoginInfo> GeteMedNYLogInInfo()
        {
            using (MedicaidTripDB ftDB = new MedicaidTripDB(null, null))
            {
                return ftDB.geteMedNYLogInInfo();
            }

        }
    }

    public class MedicaidTripPanDB : Database
    {
        MedicaidTripPanDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        //if ap number exists in MedicaidTrips, he wont add it yet
        public bool Add(ObservableCollection<MedicaidTripPan> medicaidTripsPan)
        {
            this._localTransaction = true;
            this.Transaction = this.Connection.BeginTransaction();
            int noOfRowsInsetrted = 0;
            int totalRows = 0;
            try
            {
                foreach (MedicaidTripPan mt in medicaidTripsPan)
                {
                    string serverMessage;
                    var outParam = new SqlParameter("@RecordID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    noOfRowsInsetrted = this.ExecuteNonQuery("AddMedicaidTripsPan", new SqlParameter[]
                    {
                        new SqlParameter("@RecipientID", mt.RecipientID),
                        new SqlParameter("@RecipientName", mt.RecipientName),
                        new SqlParameter("@YOB", mt.YOB),
                        new SqlParameter("@Sex", mt.Sex),
                        new SqlParameter("@InvoiceNumber", mt.InvoiceNumber),
                        new SqlParameter("@PriorApprovalNumber", mt.PriorApprovalNumber),
                        new SqlParameter("@ItemCode", mt.ItemCode),
                        new SqlParameter("@ServiceStarts", mt.ServiceStarts),
                        new SqlParameter("@ServiceEnds", mt.ServiceEnds),
                        new SqlParameter("@ApprovedTo", mt.ApprovedTo),
                        new SqlParameter("@OrderingProvider", mt.OrderingProvider),
                        new SqlParameter("@Amt", mt.Amt),
                        new SqlParameter("@Qty", mt.Qty),
                        new SqlParameter("@DaysTimes", mt.DaysTimes),
                        new SqlParameter("@CDA", mt.CDA),
                          outParam
                    }, System.Data.CommandType.StoredProcedure, out serverMessage);
                    if (noOfRowsInsetrted > 0)
                    {
                        mt.PanID = Convert.ToInt32(outParam.Value);
                        totalRows++;
                    }
                }

                //update pa number in med trips table
                this.UpdatePriorApprovalNumber();

                this.Transaction.Commit();
                return totalRows > 0;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }

        public static bool AddMedicaidTripsPan(ObservableCollection<MedicaidTripPan> medicaidTripsPan)
        {
            using (MedicaidTripPanDB mtDB = new MedicaidTripPanDB(null, null))
            {
                return mtDB.Add(medicaidTripsPan);
            }
        }

        public ObservableCollection<MedicaidTripPan> Get(int id = 0)
        {
            ObservableCollection<MedicaidTripPan> medicaidTripsPan = new ObservableCollection<MedicaidTripPan>();
            using (var dr = this.GetReader("GetMedicaidTripsPan", new SqlParameter[]
             {
                  new SqlParameter("@ID", id)
             }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    medicaidTripsPan.Add(new MedicaidTripPan()
                    {
                        PanID = Convert.ToInt32(dr["PanID"]),
                        RecipientID = dr["RecipientID"].ToString(),
                        RecipientName = dr["RecipientName"].ToString(),
                        YOB = dr["YOB"].ToString(),
                        Sex = dr["Sex"].ToString(),
                        InvoiceNumber = dr["InvoiceNumber"].ToString(),
                        PriorApprovalNumber = dr["PriorApprovalNumber"].ToString(),
                        ItemCode = dr["ItemCode"].ToString(),
                        ServiceStarts = dr["ServiceStarts"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ServiceStarts"]),
                        ServiceEnds = dr["ServiceEnds"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ServiceEnds"]),
                        ApprovedTo = dr["ApprovedTo"] == DBNull.Value ? new Nullable<DateTime>() : Convert.ToDateTime(dr["ApprovedTo"]),
                        OrderingProvider = dr["OrderingProvider"].ToString(),
                        Amt = Convert.ToDecimal(dr["Amt"]),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        DaysTimes = Convert.ToInt32(dr["DaysTimes"]),
                        CDA = dr["CDA"].ToString(),
                        Mod = dr["Mod"].ToString(),
                    });
                }
            }

            return medicaidTripsPan;
        }

        public static ObservableCollection<MedicaidTripPan> GetMedicaidTripPan(int id)
        {
            using (MedicaidTripPanDB mtDB = new MedicaidTripPanDB(null, null))
            {
                return mtDB.Get(id);
            }

        }

        public static DateTime GetMinDate()
        {
            object o;
            using (MedicaidTripPanDB mtDB = new MedicaidTripPanDB(null, null))
            {
                o = mtDB.ExecuteScalar("GetMinDate",
                    new SqlParameter[] { },
                    System.Data.CommandType.StoredProcedure);
            }
            DateTime minDate = Convert.ToDateTime(o);

            return minDate;

        }

        public void UpdatePriorApprovalNumber()
        {

            this.ExecuteScalar("UpdatePriorApprovalNumber",
            new SqlParameter[] { },
            System.Data.CommandType.StoredProcedure);

        }
    }

    public class MedicaidRemittanceDB : Database
    {
        public MedicaidRemittanceDB(SqlConnection conn, SqlTransaction tran) : base(conn, tran) { }
        public static string GetRemittanceTCN(string invoiceNo, decimal amountBilled)
        {
            using (var db = new Database(null, null))
            {
                var result = db.ExecuteScalar("SELECT TOP 1 ControlNo FROM MedicaidRemittance WHERE (DenyReason <> 'CO:97' OR DenyReason IS NULL) AND InvoiceNo = @InvoiceNo AND AmountBilled = @amountBilled", new SqlParameter[] {
                    new SqlParameter("@InvoiceNo", invoiceNo) ,
                    new SqlParameter("@AmountBilled", amountBilled)
                }, System.Data.CommandType.Text);

                if (result != null)
                    return result.ToString();
                else
                    return "";
            }
        }



    }

    public class LocationsDB : Database
    {
        public LocationsDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public List<Location> Get()
        {
            List<Location> locations = new List<Location>();

            using (var dr = this.GetReader("GetLocations", null, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    locations.Add(new Location()
                    {
                        LocationID = Convert.ToInt32(dr["LocationID"]),
                        City = dr["City"].ToString(),
                        State = dr["State"].ToString()
                    });
                }
            }

            return locations;
        }

        public static List<Location> GetLocations()
        {
            using (LocationsDB locationDB = new LocationsDB(null, null))
            {
                return locationDB.Get();
            }
        }
    }

    public class TollRatesDB : Database
    {
        public TollRatesDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public List<TollRate> Get()
        {
            List<TollRate> tollRates = new List<TollRate>();

            using (var dr = this.GetReader("GetTollRates", null, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    tollRates.Add(new TollRate()
                    {
                        TollRateID = Convert.ToInt32(dr["TollRateID"]),
                        FromLocationID = Convert.ToInt32(dr["FromLocationID"]),
                        ToLocationID = Convert.ToInt32(dr["ToLocationID"]),
                        RegPrice = Convert.ToDecimal(dr["RegPrice"]),
                        EZPrice = Convert.ToDecimal(dr["EZPrice"])
                    });
                }
            }

            return tollRates;

        }

        public static List<TollRate> GetTollRates()
        {
            using (TollRatesDB tollDB = new TollRatesDB(null, null))
            {
                return tollDB.Get();
            }
        }
    }

    public class LocalCityDB : Database
    {
        public LocalCityDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(LocalCity localCity)
        {
            var localCityIDParam = new SqlParameter("@LocalCityID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            string serverMessage;
            int noOfrecordsAffected = this.ExecuteNonQuery("AddLocalCities", new SqlParameter[]
            {
                new SqlParameter("@CityName", localCity.CityName),
                localCityIDParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfrecordsAffected > 0)
            {
                localCity.LocalCityID = Convert.ToInt32(localCityIDParam.Value);
                return true;
            }
            else
                return false;
        }

        public ObservableCollection<LocalCity> Get(int localCityID, string cityName)
        {
            var localCities = new ObservableCollection<LocalCity>();
            using (var dr = this.GetReader("GetLocalCities", new SqlParameter[] { }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    localCities.Add(new LocalCity() { LocalCityID = Convert.ToInt32(dr["LocalCityID"]), CityName = dr["CityName"].ToString() });
                }
            }

            return localCities;
        }

        public bool Edit(LocalCity localCity)
        {
            string serverMessage;
            int noOfrecordsAffected = this.ExecuteNonQuery("UpdateLocalCities", new SqlParameter[]
            {
                new SqlParameter("@LocalCityID", localCity.LocalCityID),
                new SqlParameter("@CityName", localCity.CityName)

            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfrecordsAffected > 0;
        }

        public static ObservableCollection<LocalCity> GetLocalCities(int localCityID, string cityName)
        {
            using (LocalCityDB lcDB = new LocalCityDB(null, null))
            {
                return lcDB.Get(localCityID, cityName);
            }
        }

        public static bool EditLocalCity(LocalCity localCity)
        {
            using (LocalCityDB lcDB = new LocalCityDB(null, null))
            {
                return lcDB.Edit(localCity);
            }
        }

        public static bool AddLocalCity(LocalCity localCity)
        {
            using (LocalCityDB lcDB = new LocalCityDB(null, null))
            {
                return lcDB.Add(localCity);
            }
        }
    }
}
