﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{
    public class CustomerPaymentInfoDB : Database
    {

        public CustomerPaymentInfoDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<Payment> Get(int customerID, int paymentID, DateTime? dateFrom, DateTime? dateTill, bool withCustomerInfo, bool creditableOnly, bool withDriverInfo)
        {
            //  string serverMessage;
            var payments = new ObservableCollection<Payment>();
            using (SqlDataReader dr = this.GetReader("GetPayment",
                new SqlParameter[]
                {
                    new SqlParameter("@customerID", customerID),
                    new SqlParameter("@PaymentId", paymentID),
                    new SqlParameter("@DateFrom",dateFrom),
                    new SqlParameter ("@DateTill",dateTill),
                    new SqlParameter ("@CreditableOnly",creditableOnly)

                }
                , System.Data.CommandType.StoredProcedure))                                 //(var dr = this.ExecuteNonQuery("GetPayments",@paymentID,System.Data.SqlDbType.Int,serverMessage)


                while (dr.Read())
                {
                    Payment pmt = new Payment()
                    {
                        PaymentID = Convert.ToInt32(dr["PaymentID"]),
                        DatePaid = Convert.ToDateTime(dr["DatePaid"]),
                        PaymentMethod = (PaymentMethods)Convert.ToInt32(dr["PaymentMethod"]),
                        AmountPaid = Convert.ToDecimal(dr["AmountPaid"]),
                        ReferenceNo = dr["ReferenceNo"].ToString(),
                        PaymentComments = dr["PaymentComments"].ToString(),
                        IsCreditable = Convert.ToBoolean(dr["IsCreditable"]),
                        BookFee = Convert.ToDecimal(dr["BookFee"]),
                        CustomerID = Convert.ToInt32(dr["CustomerID"])
                    };

                    if (withCustomerInfo)
                    {
                        pmt.Customer = new Customer()
                        {
                            CustomerID = Convert.ToInt32(dr["CustomerID"]),
                            LastName = dr["LastName"] == DBNull.Value ? "" : dr["LastName"].ToString(),
                            FirstName = dr["FirstName"] == DBNull.Value ? "" : dr["FirstName"].ToString(),
                            CompanyName = dr["CompanyName"] == DBNull.Value ? "" : dr["CompanyName"].ToString(),
                            CustomerType = dr["CustomerType"] == DBNull.Value ? 0 : (CustomerTypes)Convert.ToInt32(dr["CustomerType"])
                        };
                    }
                    if (withDriverInfo && (dr["DriverID"] != DBNull.Value))
                    {
                        pmt.PaidForTrip = new Trip()
                        {
                            DrivenBy = DAL.DriversDB.GetDrivers(true, driverID: Convert.ToInt32(dr["DriverID"])).FirstOrDefault()
                        };
                    }
                    payments.Add(pmt);

                }

            return payments;
        }
        public static bool AddPayment(Payment payment)
        {
            using (CustomerPaymentInfoDB customerPaymentInfoDB = new CustomerPaymentInfoDB(null, null))
            {
                return customerPaymentInfoDB.Add(payment);
            }
        }
        public static ObservableCollection<Payment> GetPayments(int customerID, int paymentID, DateTime? dateFrom = null, DateTime? dateTill = null, bool withCustomerInfo = false, bool creditableOnly = false, bool withDriverInfo = false)
        {
            using (CustomerPaymentInfoDB CustPaymentDb = new CustomerPaymentInfoDB(null, null))//(null, null))
            {
                return CustPaymentDb.Get(customerID, paymentID, dateFrom, dateTill, withCustomerInfo, creditableOnly, withDriverInfo);//.Get(invoiceID, customerID, invoceBatchID, withTrips);
            }
        }
        private bool Edit(Payment CustPaymentInfo)//CustomerInsuranceInfo customerInsInfo)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdatePayments", new SqlParameter[]
            {
                new SqlParameter("@PaymentID", CustPaymentInfo.PaymentID),
                new SqlParameter("@CustomerID",CustPaymentInfo.Customer.CustomerID ),
                new SqlParameter("@DatePaid", CustPaymentInfo.DatePaid),
                new SqlParameter("@AmountPaid", CustPaymentInfo.AmountPaid),
                new SqlParameter("@PaymentMethod", CustPaymentInfo.PaymentMethod),
                new SqlParameter("@ReferenceNo", CustPaymentInfo.ReferenceNo),
                new SqlParameter("@PaymentComments",CustPaymentInfo.PaymentComments),
                new SqlParameter("@IsCreditable",CustPaymentInfo.IsCreditable)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }
        public bool Add(Payment customerPaymentInfo)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@PaymentID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddPayment", new SqlParameter[]
            {
                new SqlParameter("@CustomerID", customerPaymentInfo.Customer.CustomerID),
                new SqlParameter("@DatePaid",customerPaymentInfo.DatePaid),
                new SqlParameter("@AmountPaid", customerPaymentInfo.AmountPaid),
                new SqlParameter("@PaymentMethod", customerPaymentInfo.PaymentMethod),
                new SqlParameter("@RefrenceNo", customerPaymentInfo.ReferenceNo),
                new SqlParameter("@PaymentComments",customerPaymentInfo.PaymentComments),
                new SqlParameter("@IsCreditable",customerPaymentInfo.IsCreditable),
                new SqlParameter("@BookFee",customerPaymentInfo.BookFee),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customerPaymentInfo.PaymentID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }
        public static bool EditCustomerPaymentsInfo(Payment CustPayment)
        {
            using (var customerPaymentInfoDB = new CustomerPaymentInfoDB(null, null))//(null, null))
            {
                // return CustomerPaymentInfoDB.EditCustomerPaymentsInfo(CustomerPaymentInfoDB);//.Edit(CustomerPayment);//.Edit(CustPaym);
                return customerPaymentInfoDB.Edit(CustPayment);
            }
        }
    }
}
