﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BOL
{
    public class Invoice : BO
    {
        public int InvoiceID { get; set; }
        public int InvoiceBatchID { get; set; }
        public Customer InvoicedCustomer { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime LastInvoiceDate { get; set; }
        public ObservableCollection<Trip> TripsInvoiced { get; set; }
        //   public decimal InvoiceTotal { get; set; }
        decimal _invoiceTotal;

        public decimal InvoiceTotal
        {
            get { return _invoiceTotal; }
            set { _invoiceTotal = value; this.OnPropertyChanged("GrandTotal"); }
        }
        public decimal TaxAmount { get; set; }

        public decimal GrandTotal
        {
            get
            {
                //  return PreviousTotalDue + InvoiceTotal;
                return PreviousTotalDue + InvoiceTotal - TotalPaymentsAndCredits;
            }
        }



        decimal _previousTotalDue;

        public decimal PreviousTotalDue
        {
            get { return _previousTotalDue; }
            set { _previousTotalDue = value; this.OnPropertyChanged("GrandTotal"); }
        }
        private decimal _totalPaymentsAndCredits;

        public decimal TotalPaymentsAndCredits
        {
            get
            {
                if (this.PaymentsFromThisBillingCycle == null)
                    return 0;
                return this.PaymentsFromThisBillingCycle.Sum(p => p.AmountPaid);
            }

            //return _totalPaymentsAndCredits; }
            set { _totalPaymentsAndCredits = value; }
        }



        /* public decimal PreviousTotalDue
         {

             get {
                 if (this.TotalDueOver90Days + this.TotalDueOver60Days + this.TotalDueOver30Days > 0)
                     return (this.TotalDueOver90Days + this.TotalDueOver60Days + this.TotalDueOver30Days);
                 // return PreviousTotalDue;
                 else return 0;
             }
             //set { return  }


                 //_previousTotalDue = value; }
         }
        decimal _totalDueOver90Days;

         public decimal TotalDueOver90Days
         {
             get { return _totalDueOver90Days; }
             set {             
                 _totalDueOver90Days = value;
                 this.OnPropertyChanged("GrandTotal");
             }
         }
         decimal _totalDueOver60Days;

         public decimal TotalDueOver60Days
         {
             get { return _totalDueOver60Days; }
             set { _totalDueOver60Days = value; this.OnPropertyChanged("GrandTotal"); }
         }
         decimal _totalDueOver30Days;

         public decimal TotalDueOver30Days
         {
             get { return _totalDueOver30Days; }
             set { _totalDueOver30Days = value; this.OnPropertyChanged("GrandTotal"); }
         }
         */

        public List<Payment> PaymentsFromThisBillingCycle { get; set; }






        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class UnbilledCustomer : BO
    {
        public Customer CustomerToInvoice { get; set; }
        public decimal UnbilledAmount { get; set; }

        public override bool AllowSave
        {
            get { return this.CustomerToInvoice != null; }
        }
    }
}
