﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;
using Rpts;

namespace CDS
{
    /// <summary>
    /// Interaction logic for TripsListWindow.xaml
    /// </summary>
    public partial class TripsListPage : Page, IDisplayTextBlock
    {
        public TripsListPage()
        {
            InitializeComponent();
            ToDatePicker.SelectedDate = DateTime.Today;

            this.trips = DAL.TripDB.GetTrips(0, 0, 0, 0, null, null, true, null, 0, 0);

        }

        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt1 = new DateTime();
                DateTime dt2 = new DateTime();
                if (FromDatePicker.SelectedDate != null) { dt1 = FromDatePicker.SelectedDate.Value.Date; }
                if (ToDatePicker.SelectedDate != null) { dt2 = ToDatePicker.SelectedDate.Value.Date; }
                TripsByDateDataGrid.DataContext = DAL.TripDB.GetTrips(0, 0, 0, 0, dt1, dt2, false, null, 0, 0);
                this.trips = DAL.TripDB.GetTrips(0, 0, 0, 0, dt1, dt2, false, null, 0, 0);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void ButtonShowReport_Click(object sender, RoutedEventArgs e)
        {
            ShowTRipsReport(trips);
        }
        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            UIController.ExportTelerikGrid(TripsByDateDataGrid, UIController.ExportFormat.Excel);
        }

        internal static void ShowTRipsReport(ObservableCollection<Trip> trips)
        {
            try
            {
                RptTrips rpt = new RptTrips(trips);
                UIController.ShowReportViewer(rpt);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            };
        }
        public ObservableCollection<Trip> trips { get; set; }
        //public ObservableCollection<Driver> driverrs { get; set; }


        public string HeaderText
        {
            get
            {
                return "Trips List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
                // throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
                //  throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "Are You Sure You want ot close this window ?";
            }
            set
            {
                throw new NotImplementedException();
            }
        }




    }
}
