﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AddEditDriverExpense.xaml
    /// </summary>
    public partial class AddEditDriverExpenseWindow : Window
    {
        public AddEditDriverExpenseWindow()
        {
            InitializeComponent();
            this.Drivers = DAL.DriversDB.GetDrivers(false);
            this.driversComboBox.ItemsSource = this.Drivers;

        }


        public AddEditDriverExpenseWindow(BOL.DriverExpense driverExpense)
            : this()
        {
            this.ActiveDriverExpense = driverExpense;
            this.ActiveDriverExpense.DateOfExpense = DateTime.Now;
        }

        private void driversComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            //  DAL.DriverExpenseDB.AddDriverExpense(this.ActiveDriverExpense);
            this.DialogResult = true;

        }
        public DriverExpense ActiveDriverExpense
        {
            get
            {
                return this.DataContext as DriverExpense;
            }
            set
            {
                this.DataContext = value;
            }
        }
        ObservableCollection<Driver> Drivers { get; set; }

    }
}
