﻿using CreditLimitService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditLimitService
{
    public class CustomerInfo
    {
        public Nullable<decimal> NotInvoicedBalance { get; set; }
        public Nullable<decimal> InvoicedBalance { get; set; }
        public Nullable<decimal> OutstandingAmount { get; set; }
        public Customer Customer { get; set; }
    }
}
