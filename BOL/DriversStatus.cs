﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class DriverAndTripInformation : BO
    {
        public DriverTripData BackupData = new DriverTripData();

        public DriverAndTripInformation()
        {
            this.UnitDriver = new Driver();
        }

        bool _isLongDistanceDriver;

        [DataMember]
        public bool IsLongDistanceDriver
        {
            get { return _isLongDistanceDriver; }
            set { _isLongDistanceDriver = value; }
        }

        Driver _unitDriver;

        [DataMember]
        public Driver UnitDriver
        {
            get { return _unitDriver; }
            set { _unitDriver = value; }
        }
        Trip _assignedToTrip;

        [DataMember]
        public Trip AssignedToTrip
        {
            get { return _assignedToTrip; }
            set
            {
                this.BackupData.AssignedTrip = _assignedToTrip;
                _assignedToTrip = value;
                this.CalledInLocation = null;

                if (value != null)
                {
                    value.PropertyChanged += delegate (object sender, System.ComponentModel.PropertyChangedEventArgs e)
                    {
                        if (e.PropertyName == "ToLocation")
                            this.OnPropertyChanged("DriversLocation");
                    };
                }
                this.OnPropertyChanged("AssignedToTrip");
                this.OnPropertyChanged("DriversLocation");
                this.OnPropertyChanged("FromLocation");
            }
        }

        Trip _assignedSecondTrip;

        [DataMember]
        public Trip AssignedSecondTrip
        {
            get { return _assignedSecondTrip; }
            set { _assignedSecondTrip = value; }
        }

        Trip _previousTrip;

        [DataMember]
        public Trip PreviousTrip
        {
            get { return _previousTrip; }
            set
            {
                _previousTrip = value;
                this.OnPropertyChanged("PreviousTrip");
            }
        }

        BOL_OTR.DriversRequestOptions _driverRequest;

        [DataMember]
        public BOL_OTR.DriversRequestOptions DriverRequest
        {
            get
            {
                return _driverRequest;
            }

            set
            {
                _driverRequest = value;
                this.OnPropertyChanged("DriverRequest");
            }
        }
        DriverStatusOptions _driverStatus;

        [DataMember]
        public DriverStatusOptions DriverStatus
        {
            get { return _driverStatus; }
            set
            {

                this.BackupData.DriverStatus = _driverStatus;
                this.BackupData.StatusChangedTime = this.StatusChangedTime;


                DriverStatusOptions oldDriverStatus = _driverStatus;
                _driverStatus = value;

                if (
                    (value != DriverStatusOptions.LongBreak || oldDriverStatus != DriverStatusOptions.ShortBreak) &&
                    (value != DriverStatusOptions.ShortBreak || oldDriverStatus != DriverStatusOptions.NintyEight) &&
                    (value != DriverStatusOptions.NintyEight || oldDriverStatus != DriverStatusOptions.ShortBreak)
                )
                {
                    if (this.AssignedToTrip != null && value == DriverStatusOptions.Assigned && this.AssignedToTrip.IsLongDistanceTrip)
                        this.StatusChangedTime = this.AssignedToTrip.IsRoundTrip ? this.AssignedToTrip.ETABackToBase.GetValueOrDefault(DateTime.Now.AddMinutes(30)) : this.AssignedToTrip.ETAToLocation1.GetValueOrDefault(DateTime.Now.AddMinutes(30));
                    else
                        this.StatusChangedTime = DateTime.Now;
                    //this.InternalStatusChangedTime = this.StatusChangedTime;
                }
                this.OnPropertyChanged("DriverStatus");
                this.OnPropertyChanged("DriversLocation");
                this.OnPropertyChanged("StatusChangedTime");
                this.OnPropertyChanged("StatusChangedTimeElapsed");
            }
        }

        string _calledInLocation;

        [DataMember]
        public string CalledInLocation
        {
            get { return _calledInLocation; }
            set
            {
                _calledInLocation = value;
                this.OnPropertyChanged("CalledInLocation");
                this.OnPropertyChanged("DriversLocation");
            }
        }

        public string FromLocation
        {
            get
            {
                if (this.AssignedToTrip != null)
                    return this.AssignedToTrip.IsRoundTrip ? this.AssignedToTrip.ToLocation : this.AssignedToTrip.FromLocation;
                else
                    return "";
            }
        }

        public string DriversLocation
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(CalledInLocation))
                    return this.CalledInLocation;
                if (this.AssignedToTrip != null)
                    return this.AssignedToTrip.IsRoundTrip ? this.AssignedToTrip.FromLocation : this.AssignedToTrip.ToLocation;
                else if (this.LastTrip != null)
                    return this.LastTrip.ToLocation;
                else
                    return "";
            }


        }

        DateTime? _timeFinishedCall;

        [DataMember(Order = 0)]
        public DateTime? StatusChangedTime
        {
            get { return _timeFinishedCall; }
            set
            {
                _timeFinishedCall = value;
                _internalStatusChangedTime = value;
                this.OnPropertyChanged("TimeFinishedCall");
            }
        }

        DateTime? _internalStatusChangedTime;

        [DataMember(Order = 1)]
        public DateTime? InternalStatusChangedTime
        {
            get { return _internalStatusChangedTime; }
            set { _internalStatusChangedTime = value; }
        }

        public TimeSpan? StatusChangedTimeElapsed
        {
            get
            {
                if (this.StatusChangedTime == null)
                    return null;

                TimeSpan span = this.StatusChangedTime.Value - DateTime.Now;
                return span;
            }
        }


        public override bool AllowSave
        {
            get
            {
                throw new NotImplementedException();
            }

        }

        Trip _lastTrip;

        [DataMember]
        public Trip LastTrip
        {
            get { return _lastTrip; }
            set { _lastTrip = value; this.OnPropertyChanged("LastTrip"); }
        }

        public void Undo98Status()
        {
            if (this.DriverStatus == DriverStatusOptions.NintyEight && this.BackupData.DriverStatus == DriverStatusOptions.Assigned)
            {
                DateTime? oldStatusChangeTime = this.BackupData.StatusChangedTime;
                this.DriverStatus = this.BackupData.DriverStatus;

                this.StatusChangedTime = oldStatusChangeTime;
                this.AssignedToTrip = this.BackupData.AssignedTrip;
                this.AssignedToTrip.DrivenBy = this.UnitDriver;
                this.AssignedToTrip.TripStatus = TripStatusTypes.Assigned;
                this.AssignedToTrip.TimeDriverAssigned = oldStatusChangeTime;
            }
        }

    }


    public struct DriverTripData
    {

        public DriverStatusOptions DriverStatus;

        public DateTime? StatusChangedTime;

        public Trip AssignedTrip;
    }


}
