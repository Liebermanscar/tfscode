
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/08/2015 14:50:15
-- Generated from EDMX file: C:\Users\Sam\Documents\Visual Studio 2015\Projects\App1\DAL_EF\CDS_OTR.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [CDS_OTR];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Drivers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Drivers];
GO
IF OBJECT_ID(N'[dbo].[Vehicles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Drivers'
CREATE TABLE [dbo].[Drivers] (
    [DriverID] int  NOT NULL,
    [LoginUserName] nvarchar(50)  NOT NULL,
    [LoginPassword] varbinary(128)  NULL,
    [IsConnected] bit  NOT NULL,
    [ConnectionID] nvarchar(150)  NOT NULL
);
GO

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [VehicleID] int IDENTITY(1,1) NOT NULL,
    [VehicleCode] nvarchar(50)  NOT NULL,
    [Make] nvarchar(50)  NOT NULL,
    [Model] nvarchar(50)  NOT NULL,
    [Image] varbinary(max)  NULL,
    [TabletID] int  NOT NULL,
    [LastSeenLocation] geography  NULL
);
GO

-- Creating table 'Tablets'
CREATE TABLE [dbo].[Tablets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TabletName] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [DriverID] in table 'Drivers'
ALTER TABLE [dbo].[Drivers]
ADD CONSTRAINT [PK_Drivers]
    PRIMARY KEY CLUSTERED ([DriverID] ASC);
GO

-- Creating primary key on [VehicleID] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([VehicleID] ASC);
GO

-- Creating primary key on [Id] in table 'Tablets'
ALTER TABLE [dbo].[Tablets]
ADD CONSTRAINT [PK_Tablets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------