﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using Microsoft.Win32;
using TwainDotNet;
using TwainDotNet.TwainNative;
using TwainDotNet.Win32;
using TwainDotNet.Wpf;
using iTextSharp.text.pdf;
using System.Threading;
//using TWainLib;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ProcessDailyEnvelopePage.xaml
    /// </summary>
    public partial class ProcessDailyEnvelopePage : Page, IDisplayTextBlock
    {
        private static AreaSettings AreaSettings = new AreaSettings(Units.Centimeters, 0.1f, 5.7f, 0.1F + 2.6f, 5.7f + 2.6f);

        private Twain _twain;
        //private ScanSettings _settings;

        private Bitmap resultImage;
        List<string> scanners;
        public string GetFilesFolder(bool forPDF)
        {
            try
            {
                DailyEnvelope env = this.ActiveDailyEnvelope;
                string driverInfo = env.UnitDriver.DriverID.ToString();// +"_" + env.UnitDriver.DriverCode; remove since they wana be able to change drivers code

                string serverFilesAddress = DAL.FacilitySettingsDB.GetFacilitySettings("ServerFileAddress").SettingValue;
                string pdfFilesFolder = serverFilesAddress + "\\" + driverInfo + "\\";
                string imagesFilesFolder = serverFilesAddress + "\\ImageFiles\\" + driverInfo + "\\";

                if (forPDF)
                {
                    if (!Directory.Exists(pdfFilesFolder))
                        Directory.CreateDirectory(pdfFilesFolder);
                }
                else
                {
                    if (!Directory.Exists(imagesFilesFolder))
                        Directory.CreateDirectory(imagesFilesFolder);
                }

                return forPDF ? pdfFilesFolder : imagesFilesFolder;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex); return null;
            }
        }

        public string GetFileName()
        {
            try
            {
                DailyEnvelope env = this.ActiveDailyEnvelope;
                string driverInfo = env.UnitDriver.DriverID.ToString() + "_" + env.UnitDriver.DriverCode;
                var dateTimeNowFormated = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt").Replace("/", ".");
                dateTimeNowFormated = dateTimeNowFormated.Replace(":", ".");
                dateTimeNowFormated = dateTimeNowFormated.Replace(" ", "_");

                string fileName = string.Concat(
                    env.UnitDriver.DriverID.ToString(), "_",
                    env.UnitDriver.DriverCode, "_",
                    env.DateOfTrips.Value.ToShortDateString().Replace("/", "."), "--",
                    dateTimeNowFormated);

                return fileName;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex); return null;
            }
        }

        public ProcessDailyEnvelopePage()
        {
            InitializeComponent();

            Loaded += delegate
            {
                try
                {
                    _twain = new Twain(new WpfWindowMessageHook(this));
                    scanners = _twain.SourceNames.ToList();
                    this.ManualSource.ItemsSource = scanners;
                    this.ManualSource.SelectedItem = scanners.Where(s => s == Properties.Settings.Default.ScanningDeviceName).FirstOrDefault();


                    _twain.TransferImage += delegate (Object sender, TransferImageEventArgs args)
                    {
                        try
                        {
                            if (args.Image != null)
                            {
                                resultImage = args.Image;
                                IntPtr hbitmap = new Bitmap(args.Image).GetHbitmap();
                                //MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(
                                //        hbitmap,
                                //        IntPtr.Zero,
                                //        Int32Rect.Empty,
                                //        BitmapSizeOptions.FromEmptyOptions());

                                resultImage.Save(System.IO.Path.Combine(GetFilesFolder(false), GetFileName()) + ".jpeg");

                                string fileName = GetFileName(); //save here and reuse, others the name change from the time called
                                ConvertToPdfFromBitmap(resultImage, System.IO.Path.Combine(GetFilesFolder(true), fileName));
                                Gdi32Native.DeleteObject(hbitmap);

                                this.ActiveDailyEnvelope.SavedFiles.Add(new SavedFile()
                                {
                                    FileName = fileName + ".pdf"
                                });

                                this.ActiveDailyEnvelope.OnPropertyChanged("SavedFilesDisplay");
                            }
                        }
                        catch (Exception ex)
                        {
                            //UIController.DisplayErrorMessage(ex);
                        }

                    };
                    _twain.ScanningComplete += delegate
                    {
                        IsEnabled = true;
                    };
                }
                catch (Exception ex)
                {
                    //if (ActiveDailyEnvelope.DailyEnvelopeID > 0)
                    //    return;
                    //UIController.DisplayErrorMessage(ex);
                }


                //var sourceList = _twain.SourceNames;
                //ManualSource.ItemsSource = sourceList;

                //if (sourceList != null && sourceList.Count > 0)
                //    ManualSource.SelectedItem = sourceList[0];
            };

            //tw = new Twain();
            // //IntPtr windowHandle = new WindowInteropHelper(Application.Current.MainWindow).Handle;
            //HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;  
            //if (hwndSource != null)  
            //{  
            //    tw.Init(hwndSource.Handle);  
            //} 

            try
            {
                ProcessNewDailyEnvelope();
                this.Vehicles = DAL.VehicleDB.GetVehicles();
                this.vehiclesComboBox.ItemsSource = this.Vehicles;

                this.Drivers = DAL.DriversDB.GetDrivers(false);
                this.driversComboBox.ItemsSource = this.Drivers;

                this.CommisionTypes = DAL.CommisionTypesDB.GetCommisionTypes();
                this.commisionComboBox.ItemsSource = this.CommisionTypes;
                this.ExpenseTypes = DAL.ExpenseTypeDB.GetExpenseTypes(0);
                this.ExpenseCB.ItemsSource = this.ExpenseTypes;

                bookList = DAL.TicketBookDB.GetTicketBooks();

                int unicode = 65;
                foreach (TicketBook tb in bookList)
                {
                    char character = (char)unicode;
                    tb.BookNameID = character.ToString() + "-" + tb.BookName;
                    unicode++;
                }
                bookList.Insert(0, new TicketBook() { TicketBookID = 0, BookName = "None", BookNameID = "N-None" });
                this.cboTicketTypes.ItemsSource = bookList;

                /*     (this.Resources["ExpenseTypesListData"] as ExpenseTypesList).Clear();
                     foreach (ExpenseType et in DAL.ExpenseTypeDB.GetExpenseTypes())
                     {
                         (this.Resources["ExpenseTypesListData"] as ExpenseTypesList).Add(et);
                     }

                     (this.Resources["TicketBooksListData"] as TicketBooksList).Clear();
                     foreach (TicketBook et in DAL.TicketBookDB.GetTicketBooks())
                     {
                         (this.Resources["TicketBooksListData"] as TicketBooksList).Add(et);
                     }
                 */

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        ObservableCollection<TicketBook> bookList;
        public ProcessDailyEnvelopePage(DailyEnvelope dailyEnvelope) : this()
        {
            if (dailyEnvelope != null)
            {
                dailyEnvelope.DontIncludeAssignedTrips = false;
                dailyEnvelope.ObjectState = BOState.Edit;
                dailyEnvelope.StaticData = false;
                this.ActiveDailyEnvelope = dailyEnvelope;
                this.driversComboBox.IsEnabled = false;
                this.dPicker.IsEnabled = false;
                driversComboBox.Text = this.ActiveDailyEnvelope.UnitDriver.DriverCode;
                driversComboBox.IsEditable = false;
                GetTrips();
                dailyEnvelope.SavedFiles = DAL.SavedFilesDB.GetSavedFiles(dailyEnvelope.DailyEnvelopeID);
                this.cbSelected.IsReadOnly = true;
                foreach (TicketsReceived tr in DAL.DETicketsReceivedDB.GetTicketsRecived(this.ActiveDailyEnvelope.DailyEnvelopeID))
                {
                    tr.DETicketLogs = DAL.DETicketLogDB.GetDETicketLog(tr.FromTicketBook.TicketBookID, this.ActiveDailyEnvelope.DailyEnvelopeID);

                    //tr.QtyReceived = tr.DETicketLogs.Count;
                    //tr.QtyBooked = tr.DETicketLogs.Count;

                    this.ActiveDailyEnvelope.TicketsReceivedByDriver.Add(tr);
                }
                //  this.ActiveDailyEnvelope.Add(
                foreach (DEExpense exp in DAL.DEExpenseDB.GetDEExpense(this.ActiveDailyEnvelope.DailyEnvelopeID))
                {
                    this.ActiveDailyEnvelope.Expenses.Add(exp);
                }
                foreach (TicketBook tb in DAL.TicketBookDB.GetTicketBooks())
                {
                    this.ActiveDailyEnvelope.TicketBooksSold.Add(new TicketBookSold() { TicketBookTypeSold = tb });

                    this.ActiveDailyEnvelope.TicketBooksSold
                        [
                            this.ActiveDailyEnvelope.TicketBooksSold.IndexOf(this.ActiveDailyEnvelope.TicketBooksSold.Last())
                        ].TicketBookStatusesSold = DAL.TicketBookStatusDB.GetTicketBookStatuses(

                            dailyEnvelopeID: this.ActiveDailyEnvelope.DailyEnvelopeID, ticketBookID: tb.TicketBookID

                        );
                    ObservableCollection<TicketBookStatus> tbsc = this.ActiveDailyEnvelope.TicketBooksSold
                        [
                            this.ActiveDailyEnvelope.TicketBooksSold.IndexOf(this.ActiveDailyEnvelope.TicketBooksSold.Last())
                        ].TicketBookStatusesSold;
                    this.ActiveDailyEnvelope.TicketBooksSold[this.ActiveDailyEnvelope.TicketBooksSold.IndexOf(this.ActiveDailyEnvelope.TicketBooksSold.Last())].Qty = tbsc.Count;

                    foreach (TicketBookStatus tbs in tbsc.Where(c => c.CustomerIdSold > 0))
                    {
                        tbs.CustomerSoldTo = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID, tbs.CustomerIdSold.ToString(), DataRequestTypes.LiteData, false)[0];
                    }
                }
            }
        }

        private void ConvertToPdfFromBitmap(Bitmap bitmapImage, string folderPath)
        {
            try
            {
                //string fileName = @"C:\Users\sam2\Documents\Visual Studio 2010\Projects\CDS_3\twaingui_src\TwainGui\bin\Debug\ScanPass1_Pic1.bmp";
                iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
                //iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(0,80,100,100);
                //document.SetPageSize(rec);
                using (var stream = new FileStream(folderPath + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter.GetInstance(document, stream);
                    document.Open();

                    //byte[] imageData = (byte[])bitmapImage;
                    //using (var imageStream = new MemoryStream(imageData,0,imageData.Length,true,true))

                    using (var imageStream = new MemoryStream())
                    {
                        bitmapImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Bmp);
                        imageStream.Position = 0;
                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageStream);
                        //image.ScaleAbsolute(document.PageSize.Width, document.PageSize.Height);

                        var w = document.PageSize.Width / 100;
                        var h = document.PageSize.Height / 100;
                        image.ScaleToFit(document.PageSize.Width - (w * 5), document.PageSize.Height - (h * 5));
                        //image.ScalePercent(25);
                        //document.Add(new iTextSharp.text.Paragraph("Page 1"));
                        document.Add(image);
                    }

                    document.Close();
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void ConvertToPdf1(string folderPath, string fileName)
        {
            try
            {
                //string fileName = @"C:\Users\sam2\Documents\Visual Studio 2010\Projects\CDS_3\twaingui_src\TwainGui\bin\Debug\ScanPass1_Pic1.bmp";
                //string folderPath = "";
                iTextSharp.text.Document document = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER);
                //iTextSharp.text.Rectangle rec = new iTextSharp.text.Rectangle(0,80,100,100);
                //document.SetPageSize(rec);
                using (var stream = new FileStream(folderPath + fileName + ".pdf", FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    PdfWriter.GetInstance(document, stream);
                    document.Open();
                    using (var imageStream = new FileStream(folderPath + fileName + "jpeg", FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        var image = iTextSharp.text.Image.GetInstance(imageStream);
                        image.ScaleAbsolute(document.PageSize.Width, document.PageSize.Height);
                        //image.ScalePercent(25);
                        //document.Add(new iTextSharp.text.Paragraph("Page 1"));
                        document.Add(image);
                    }
                    document.Close();
                }
            }
            catch (Exception)
            {


            }

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                    return;

                switch (e.Key)
                {

                    case Key.F1:
                        SaveEnvelope();
                        e.Handled = true;
                        break;
                    case Key.F2:
                        CancelEnvelope();
                        e.Handled = true;
                        break;

                    case Key.F6:
                        AssignTripToMedicaid();
                        e.Handled = true;
                        break;
                    case Key.F7:
                        //

                        if (this.SelectedTrip == null)
                            return;

                        this.SelectedTrip.ChargeToCustomer = UIController.SelectCustomerFromList(null);


                        //change that the selected column should jump to the amount charged column

                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.ChargeColumn);
                        this.TripsGrid.BeginEdit();


                        e.Handled = true;
                        break;

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }


        private void driversComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GetTicketsOwed();
            if (this.ActiveDailyEnvelope == null || this.ActiveDailyEnvelope.UnitDriver == null)
                return;
            this.dPicker.SelectedDate = DAL.DailyEnvelopeDB.GetNextEnvelopeDate(this.ActiveDailyEnvelope.UnitDriver.DriverID);

            //     var driverInfo
            // this.ActiveDailyEnvelope.DateOfTrips =
            //    DAL.DailyEnvelopeDB.GetNextEnvelopeDate(this.ActiveDailyEnvelope.UnitDriver.DriverID);

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.GetTrips();

        }
        private void btnAddTrip_Click(object sender, RoutedEventArgs e)
        {
            this.AddNewTrip(TripTypes.Regular);
        }
        private void AddNewTrip(TripTypes tripType)
        {
            try
            {
                this.NewTrip = new Trip()
                {
                    TripType = tripType,
                    TripStarted = this.ActiveDailyEnvelope == null ? new Nullable<DateTime>() : this.ActiveDailyEnvelope.DateOfTrips.GetValueOrDefault(DateTime.Today),
                    DrivenBy = this.ActiveDailyEnvelope.UnitDriver,
                    IsBonusEligible = false
                };
                this.AddTripBorder.Visibility = System.Windows.Visibility.Visible;
                this.ChargeToDriverComboBox.ItemsSource = this.Drivers;
                this.fromLocationTextBox.Focus();

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        private void btnSaveNewTrip_Click(object sender, RoutedEventArgs e)
        {
            this.SaveNewTrip();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.NewTrip = null;
            this.AddTripBorder.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void btnAddExpenses_Click(object sender, RoutedEventArgs e)
        {
            this.AddNewExpense();
        }
        private void ChargeCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.NewTrip.ChargeToCustomer = UIController.SelectCustomerFromList(null);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }


        //private bool CheckIfTripNeedsPassCode(Customer customerToCheck)
        //{
        //    if (!customerToCheck.NeedsPassCode)
        //        return true;
        //    string passCode;
        //    var results = UIController.GetInput("Pass Code", "This Charge Requirs a PassCode", out passCode);
        //  //  DAL.CustomerPassCodeDB.GetCustomerPassCode(customerToCheck.CustomerID);


        //    return true;
        //}
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            CancelEnvelope();
        }
        private void SaveDailyEnvelopeButton_Click(object sender, RoutedEventArgs e)
        {
            SaveEnvelope();

        }
        private void fromLocationTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (sender == this.fromLocationTextBox)
                {
                    if (e.Key == Key.Return && !string.IsNullOrWhiteSpace(this.NewTrip.FromLocation))
                    {
                        int spaceIndex = this.NewTrip.FromLocation.IndexOf(" ");
                        string streetName = DAL.StreetAbbreviationDB.GetStreetNameByAbbreviation(this.NewTrip.FromLocation.Substring(spaceIndex < 0 ? 0 : spaceIndex).Trim());
                        if (!string.IsNullOrWhiteSpace(streetName))
                        {
                            this.NewTrip.FromLocation = this.NewTrip.FromLocation.Remove(spaceIndex < 0 ? 0 : spaceIndex) + " " + streetName;
                            this.fromLocationTextBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        }
                    }
                }
                else
                {
                    if (e.Key == Key.Return && !string.IsNullOrWhiteSpace(this.NewTrip.ToLocation))
                    {
                        int spaceIndex = this.NewTrip.ToLocation.IndexOf(" ");
                        string streetName = DAL.StreetAbbreviationDB.GetStreetNameByAbbreviation(this.NewTrip.ToLocation.Substring(spaceIndex < 0 ? 0 : spaceIndex).Trim());
                        if (!string.IsNullOrWhiteSpace(streetName))
                        {
                            this.NewTrip.ToLocation = this.NewTrip.ToLocation.Remove(spaceIndex < 0 ? 0 : spaceIndex) + " " + streetName;
                            this.toLocationTextBox.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        private void TripsGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            this.SelectedTrip.SetDefaults();

            try
            {
                if (e.EditAction == DataGridEditAction.Cancel)
                    return;

                e.Cancel = true;

                Trip trip = e.Row.DataContext as Trip;

                if (trip.TotalPaid != trip.TotalPrice) { MessageBox.Show("Total Paid is diffrent then total Price", "Adjustment needed", MessageBoxButton.OK, MessageBoxImage.Information); return; }
                e.Cancel = false;
            }
            catch (Exception ex)
            { UIController.DisplayErrorMessage(ex); }
            //try
            //{
            //    if (e.EditAction == DataGridEditAction.Cancel)
            //        return;

            //    e.Cancel = true;

            //    Trip trip = e.Row.DataContext as Trip;

            //    if (trip.TripID > 0)
            //        e.Cancel = !DAL.TripDB.EditTrip(trip);
            //}
            //catch (Exception ex)
            //{
            //    UIController.DisplayErrorMessage(ex);

            //}
        }
        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (this.SelectedTrip == null)
                    return;
            }
            ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, (sender as TextBox).Text, DataRequestTypes.LiteData, true);


            //Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

            if (customers.Count == 1)
                this.SelectedTrip.ChargeToCustomer = customers[0];
            else if (customers.Count > 1)
                this.SelectedTrip.ChargeToCustomer = UIController.SelectCustomerFromList(customers);

            DataGridRow dgrow = (DataGridRow)this.TripsGrid.ItemContainerGenerator.ContainerFromItem(TripsGrid.Items[TripsGrid.SelectedIndex]);
            dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));

        }

        private void txtContactName_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {


                if ((sender as TextBox).Text == "")
                    return;
                ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, (sender as TextBox).Text, DataRequestTypes.LiteData, true);
                //Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

                if (customers.Count == 1)
                    this.SelectedTrip.ChargeToCustomer = customers[0];
                else if (customers.Count > 1)
                    this.SelectedTrip.ChargeToCustomer = UIController.SelectCustomerFromList(customers);

                /*  DataGridRow dgrow = (DataGridRow)this.TripsGrid.ItemContainerGenerator.ContainerFromItem(TripsGrid.Items[TripsGrid.SelectedIndex]);
                  dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Last));*/

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                var customerToCharge = UIController.SelectCustomerFromList(null);
                if (customerToCharge == null) return;
                this.SelectedTrip.ChargeToCustomer = customerToCharge;
                //if (this.SelectedTrip.ChargeToCustomer == null)
                //    this.SelectedTrip.AttachedScheduledTrip = null;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        void RemoveTripFromCustomer()
        {
            try
            {
                if (this.SelectedTrip == null)
                    return;
                this.SelectedTrip.ChargeToCustomer = null;
                this.SelectedTrip.AttachedScheduledTrip = null;

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void SaveNewTrip()
        {
            try
            {
                if (this.NewTrip == null || this.NewTrip.ObjectState == BOState.Edit)//edit is when double click in grid to edit so dont add a new trip
                    return;

                //   if (DAL.TripDB.Add(this.NewTrip))
                // {
                this.NewTrip.Selected = true;
                this.NewTrip.ObjectState = BOState.New;
                this.ActiveDailyEnvelope.TripsCovered.Add(this.NewTrip);

                this.AddNewTrip(TripTypes.Regular);
                // }


            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        private void AddNewExpense()
        {
            throw new NotImplementedException();
        }
        void ProcessNewDailyEnvelope()
        {
            try
            {
                this.ActiveDailyEnvelope = new DailyEnvelope() { ObjectState = BOState.New };
                this.ActiveDailyEnvelope.DontIncludeAssignedTrips = true;

                foreach (TicketBook tb in DAL.TicketBookDB.GetTicketBooks())
                {
                    this.ActiveDailyEnvelope.TicketsReceivedByDriver.Add(new TicketsReceived() { FromTicketBook = tb });
                    this.ActiveDailyEnvelope.TicketBooksSold.Add(new TicketBookSold() { TicketBookTypeSold = tb });
                }

                this.AddTripBorder.Visibility = System.Windows.Visibility.Collapsed;

                //this.AddNewTrip(TripTypes.Regular);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }
        void GetTicketsOwed()
        {
            if (this.ActiveDailyEnvelope == null || this.ActiveDailyEnvelope.UnitDriver == null)
                return;
            var ticketsOwes = DAL.DETicketsReceivedDB.getTicketsOwed(this.ActiveDailyEnvelope.UnitDriver.DriverID);

            foreach (var tcktsRecvd in this.ActiveDailyEnvelope.TicketsReceivedByDriver)
            {
                var tcktsOwed = ticketsOwes.Where(to => to.FromTicketBook.TicketBookID == tcktsRecvd.FromTicketBook.TicketBookID).FirstOrDefault();
                if (tcktsOwed == null)
                    tcktsRecvd.Owes = 0;
                else
                    tcktsRecvd.Owes = tcktsOwed.Owes;
            }


        }
        private void GetTrips()
        {
            try
            {
                int count = this.ActiveDailyEnvelope.TripsCovered.Count;

                if (this.ActiveDailyEnvelope.TripsCovered.Count <= 0)
                    this.ActiveDailyEnvelope.TripsCovered.AddRange(DAL.TripDB.GetTrips(0, 0, this.ActiveDailyEnvelope.UnitDriver.DriverID, 0,
                        this.ActiveDailyEnvelope.DateOfTrips,
                        this.ActiveDailyEnvelope.DateOfTrips,//.Value.AddDays(1),
                                                             //false, null, 0, 0, this.ActiveDailyEnvelope.DontIncludeAssignedTrips, this.ActiveDailyEnvelope.DailyEnvelopeID).OrderBy(t => t.TimeDriverAssigned));
                        false, null, 0, 0, this.ActiveDailyEnvelope.DontIncludeAssignedTrips, this.ActiveDailyEnvelope.DailyEnvelopeID).OrderBy(t => t.TimeDriverAssigned.HasValue ? t.TimeDriverAssigned.Value : t.TimeOfCall));//if it has a valua

                //  this.ActiveDailyEnvelope.TripsCovered.OrderBy(t => t.TimeDriverAssigned.Value);

                //    if (ActiveDailyEnvelope.ObjectState != BOState.Edit)
                foreach (var trip in this.ActiveDailyEnvelope.TripsCovered)
                {
                    trip.Selected = true;

                    if (trip.TicketTypePaid != 0)
                    {
                        trip.TicketBook = this.bookList.Where(b => b.TicketBookID == trip.TicketTypePaid).FirstOrDefault();
                    }
                }

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        void SaveEnvelope()
        {
            try
            {
                if (ActiveDailyEnvelope.DailyEnvelopeID == 0 && ActiveDailyEnvelope.SavedFilesDisplay.Count == 0)
                {
                    if (MessageBox.Show("Nothing was scanned, Do You want to go back and complete scanning?", "Scanning", MessageBoxButton.YesNo,
                        MessageBoxImage.Question, MessageBoxResult.Yes) == MessageBoxResult.Yes)
                        return;
                }
                if (ActiveDailyEnvelope.CommisionTypeUsed.PercentOfGasDriverPays < 100 &&
                    ActiveDailyEnvelope.GasPaid == 0)
                {
                    if (MessageBox.Show("ARE YOU SURE YOU WANT TO SAVE DAILY ENVELOPE WITH NO GAS RECEIPT?", "GAS", MessageBoxButton.YesNo,
                        MessageBoxImage.Question, MessageBoxResult.No) != MessageBoxResult.Yes)
                        return;
                }

                this.ActiveDailyEnvelope.AddNotRecordedTrips();
                ObservableCollection<Trip> tripsCoverdToAssignToEnvelope = new ObservableCollection<Trip>();
                tripsCoverdToAssignToEnvelope.AddRange(this.ActiveDailyEnvelope.TripsCovered.Where(tc => tc.Selected));
                this.ActiveDailyEnvelope.TripsCovered = tripsCoverdToAssignToEnvelope;
                this.ActiveDailyEnvelope.SetCompanyIncome();
                // this.ActiveDailyEnvelope.TripsCovered.AddRange(tripsCoverdToAssignToEnvelope);
                if (this.ActiveDailyEnvelope.DailyEnvelopeID > 0)
                {
                    DAL.DailyEnvelopeDB.EditDailyEnvelope(this.ActiveDailyEnvelope);

                    foreach (var tr in this.ActiveDailyEnvelope.TicketsReceivedByDriver)
                    {
                        foreach (var tl in tr.DETicketLogs.Where(t => t != null))
                        {
                            tl.TicketBookId = tr.FromTicketBook.TicketBookID;
                            tl.DailyEnvelopeId = tr.DailyEnvelopID;
                        }
                        DAL.DETicketsReceivedDB.EditTicketBooksRecived(tr);
                    }

                    this.DeleteFilesMarkedForDeletion();
                    this.ProcessNewDailyEnvelope();
                }

                else if (DAL.DailyEnvelopeDB.AddDailyEnvelope(this.ActiveDailyEnvelope))
                    sendEmailsForMedTrips();
                this.DeleteFilesMarkedForDeletion();
                this.ProcessNewDailyEnvelope();


            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        void DeleteFilesMarkedForDeletion()
        {
            try
            {
                foreach (SavedFile file in ActiveDailyEnvelope.SavedFiles.Where(sf => sf.IsMarkedForDeletion))
                {
                    File.Delete(this.GetFilesFolder(true) + file.FileName);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        void CancelEnvelope()
        {
            try
            {
                string message;
                if (this.ActiveDailyEnvelope.ObjectState == BOState.New)
                    message = "You're about to cancel the Active Daily Envelope\n\nContinue?";
                else
                    message = "You're about to cancel the changes made to the Active Daily Envelope\n\nContinue?";
                if (MessageBox.Show(message, "Cancel", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
                    this.ProcessNewDailyEnvelope();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        Trip SelectedTrip
        {
            get { return this.TripsGrid.SelectedItem as Trip; }
        }
        Trip PreviousSelectedTrip
        {
            get { return this.TripsGrid.SelectedIndex > 0 ? this.TripsGrid.Items[this.TripsGrid.SelectedIndex - 1] as Trip : null; }
        }
        Trip NewTrip
        {
            get { return this.AddTripBorder.DataContext as Trip; }
            set { this.AddTripBorder.DataContext = value; }
        }

        public DailyEnvelope ActiveDailyEnvelope
        {
            get { return this.DataContext as DailyEnvelope; }
            set { this.DataContext = value; }
        }

        public Trip ActiveTrip
        {
            get { return this.TripsGrid.SelectedItem as Trip; }
        }

        ObservableCollection<Vehicle> Vehicles { get; set; }
        ObservableCollection<Driver> Drivers { get; set; }
        ObservableCollection<CommisionType> CommisionTypes { get; set; }
        public ObservableCollection<ExpenseType> ExpenseTypes { get; set; }
        public ObservableCollection<ExpenseType> ExpenseTypesList { get; set; }
        ObservableCollection<TicketsReceived> TicketsOwes { get; set; }
        ObservableCollection<TicketBook> TicketBooks { get; set; } //Ticket Book Types
        TicketBookSold SelectedTicketBookSold
        {
            get { return this.TicketBooksSoldGrid.SelectedItem as TicketBookSold; }
            set { this.TicketBooksSoldGrid.SelectedItem = value; }
        }
        TicketsReceived SelectedTicketsReceived
        {
            get { return this.TicketsReceivedGrid.SelectedItem as TicketsReceived; }
            set { this.TicketsReceivedGrid.SelectedItem = value; }
        }


        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Process Daily Envelopes: " + this.ActiveDailyEnvelope.DriverCode;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "You're about to close\n\n" + this.HeaderText + "\n\nContinue?";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion        


        private void MenuItemMedicaidTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AssignTripToMedicaid();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        private void AssignTripToMedicaid()
        {
            try
            {


                // this.NewTrip.IsMedicaidTrip = true;
                ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(this.SelectedTrip.TripStarted.Value, TripAreaTypes.All);
                if (scheduledTripSelected != null)
                {

                    /*if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                                               {
                                                   int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                                                   int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                                                   if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.FromLocationStreetName))
                                                   {
                                                       this.Dispatching.AddedEditedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                                                       this.Dispatching.AddedEditedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                                                   }
                                                   if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.ToLocationStreetName))
                                                   {
                                                       this.Dispatching.AddedEditedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                                                       this.Dispatching.AddedEditedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                                                   }
                                                   this.Dispatching.AddedEditedTrip.AttachedScheduledTrip = scheduledTripSelected;
                                               }
                                               else*/
                    if (this.SelectedTrip != null)
                    {
                        int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                        int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                        if (string.IsNullOrWhiteSpace(this.SelectedTrip.FromLocationStreetName))
                        {
                            this.SelectedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                            this.SelectedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                        }
                        if (string.IsNullOrWhiteSpace(this.SelectedTrip.ToLocationStreetName))
                        {
                            this.SelectedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                            this.SelectedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                        }

                        this.SelectedTrip.AttachedScheduledTrip = scheduledTripSelected;
                        this.SelectedTrip.ChargeToCustomer = scheduledTripSelected.ScheduledByCustomer;
                        tripsToSendEmails.Add(this.SelectedTrip);


                    }
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void dPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dPicker.SelectedDate != null)
            {
                if (this.ActiveDailyEnvelope.ObjectState != BOState.Edit)
                {
                    bool r = DAL.DailyEnvelopeDB.IfDateOfEnvelopeExists(this.ActiveDailyEnvelope.UnitDriver.DriverID, dPicker.SelectedDate.Value);
                    if (r == true)
                        MessageBox.Show("Warning This Driver Has A Envelope Processed for The Selected Day!!", "Warning", MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            }
        }

        List<Trip> tripsToSendEmails = new List<Trip>();

        void sendEmailsForMedTrips()
        {
            foreach (var trip in tripsToSendEmails)
            {

                UIController.SendEmailMessege("Medicaid trip added automated message",
                  string.Format("This is a automated message that a new M # was generated for a trip that was serviced.\n\nTrip date: {0}\nPatient name: {1}\nM # generated: M-{2}\n\nThis email is confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.",
                  trip.TripStarted.Value.ToShortDateString(),
                    trip.ChargeToCustomer.CustomerName,
                        trip.MedicaidDispatchTripNo));

            }

            tripsToSendEmails.Clear();
        }

        /*private void TextBoxDriver_KeyDown(object sender, KeyEventArgs e)
        {

        }*/

        private void ButtonDriver_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var drivers = DAL.DriversDB.GetDrivers(false, 0);
                string gg;
                var driverSelected = UIController.SelectDriver(drivers, "Manual entry on process daily envelope", out gg, false);
                if (driverSelected == null)
                    return;

                if (!driverSelected.AllowsChargeToHisAccount)
                {
                    MessageBox.Show(string.Format("Unit No {0} does NOT allow charges to his account", driverSelected.DriverCode), "Charges NOT Allowed", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.OK);
                    return;
                }
                this.SelectedTrip.ChargeToDriver = driverSelected; //SelectCustomerFromList(null);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void menueItemRemoveFromCustomer_Click(object sender, RoutedEventArgs e)
        {
            RemoveTripFromCustomer();
        }

        private void TicketBooksSoldGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedTicketBookSold == null)
                return;

            if (this.SelectedTicketBookSold.TicketBookStatusesSold == null)
            {
                this.SelectedTicketBookSold.TicketBookStatusesSold = new ObservableCollection<TicketBookStatus>();
                if (this.SelectedTicketBookSold.TicketBookTypeSold.TicketBookID == 13) //local regular
                {
                    this.SelectedTicketBookSold.CountPaidByCC = DAL.TicketBookStatusDB.GetCountOfCCBookPayments(this.ActiveDailyEnvelope.UnitDriver.DriverID, this.ActiveDailyEnvelope.DateOfTrips.Value);
                    for (int i = 0; i < this.SelectedTicketBookSold.CountPaidByCC; i++)
                    {
                        this.SelectedTicketBookSold.TicketBookStatusesSold.Add(new TicketBookStatus()
                        { TicketBookID = this.SelectedTicketBookSold.TicketBookTypeSold.TicketBookID, IsPaidByCC = true });
                    }
                }
            }
            /*
             if (this.SelectedTicketBookSold.TicketBookStatusesSold == null)
             {
                 this.SelectedTicketBookSold.TicketBookStatusesSold = new ObservableCollection<TicketBookStatus>();
                 for (int i = 0; i < SelectedTicketBookSold.Qty; i++)
                 {
                     this.SelectedTicketBookSold.TicketBookStatusesSold.Add(new TicketBookStatus() {TicketBookID = this.SelectedTicketBookSold.TicketBookTypeSold.TicketBookID});
                 }                
             }
             else if (this.SelectedTicketBookSold.TicketBookStatusesSold.Count < this.SelectedTicketBookSold.Qty)
             {
                 for (int i = 0; i < (SelectedTicketBookSold.Qty - this.SelectedTicketBookSold.TicketBookStatusesSold.Count); i++)
                 {
                     this.SelectedTicketBookSold.TicketBookStatusesSold.Add(new TicketBookStatus() {TicketBookID = this.SelectedTicketBookSold.TicketBookTypeSold.TicketBookID});
                 }                
             }
            */
            var Copy = new ObservableCollection<TicketBookStatus>(SelectedTicketBookSold.TicketBookStatusesSold);
            TicketBookSold tbs = UIController.LoadSoldBooksWindow(SelectedTicketBookSold) as TicketBookSold;


            if (tbs != null)
            {
                foreach (TicketBookStatus tbss in tbs.TicketBookStatusesSold)
                {
                    tbss.TicketBookID = tbs.TicketBookTypeSold.TicketBookID;
                }
                this.SelectedTicketBookSold.Qty = tbs.TicketBookStatusesSold.Count;
                // this.SelectedTicketBookSold = tbs;
            }
            else
            {
                this.SelectedTicketBookSold.TicketBookStatusesSold = Copy;
            }
        }

        private void TicketsReceivedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedTicketsReceived.DETicketLogs == null)
            {
                this.SelectedTicketsReceived.DETicketLogs = new ObservableCollection<DETicketLog>();
                //the following is if he wont to enter a number of tickets and rows should get added
                /*
                for (int i = 0; i < SelectedTicketsReceived.QtyReceived; i++)
                {
                    this.SelectedTicketsReceived.DETicketLogs.Add(new DETicketLog() { TicketBookId = this.SelectedTicketsReceived.FromTicketBook.TicketBookID });
                }
            }
            else if (this.SelectedTicketsReceived.DETicketLogs.Count < this.SelectedTicketsReceived.QtyBooked)
            {
                for (int i = 0; i < (SelectedTicketsReceived.QtyBooked - SelectedTicketsReceived.DETicketLogs.Count); i++)
                {
                    this.SelectedTicketsReceived.DETicketLogs.Add(new DETicketLog() { TicketBookId = this.SelectedTicketsReceived.FromTicketBook.TicketBookID });
                }
                 */
            }

            var Copy = new ObservableCollection<DETicketLog>(SelectedTicketsReceived.DETicketLogs);
            TicketsReceived tr = UIController.LoadTicketsRecievedWindow(SelectedTicketsReceived) as TicketsReceived;
            if (tr != null)
            {
                tr.QtyReceived = tr.DETicketLogs.Count;
                //tr.QtyBooked = tr.DETicketLogs.Count;
                this.SelectedTicketsReceived = tr;
            }
            else
            {
                SelectedTicketsReceived.DETicketLogs = Copy;
            }
        }

        private void TripsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedTrip == null)
                return;
            this.NewTrip = this.SelectedTrip;
            this.NewTrip.ObjectState = BOState.Edit;
        }



        /*       private void DataGridCell_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
               {
                   DataGridCell cell = sender as DataGridCell;
                   GridColumnFastEdit(cell, e);
               }

               private void DataGridCell_PreviewTextInput(object sender, TextCompositionEventArgs e)
               {
                   DataGridCell cell = sender as DataGridCell;
                   GridColumnFastEdit(cell, e);
               }

               private static void GridColumnFastEdit(DataGridCell cell, RoutedEventArgs e)
               {
                   if (cell == null || cell.IsEditing || cell.IsReadOnly)
                       return;

                   DataGrid dataGrid = FindVisualParent<DataGrid>(cell);
                   if (dataGrid == null)
                       return;

                   if (!cell.IsFocused)
                   {
                       cell.Focus();
                   }

                   if (cell.Content is CheckBox)
                   {
                       if (dataGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                       {
                           if (!cell.IsSelected)
                               cell.IsSelected = true;
                       }
                       else
                       {
                           DataGridRow row = FindVisualParent<DataGridRow>(cell);
                           if (row != null && !row.IsSelected)
                           {
                               row.IsSelected = true;
                           }
                       }
                   }
                   else
                   {
                       ComboBox cb = cell.Content as ComboBox;
                       if (cb != null)
                       {
                           //DataGrid dataGrid = FindVisualParent<DataGrid>(cell);
                           dataGrid.BeginEdit(e);
                           cell.Dispatcher.Invoke(
                            System.Windows.Threading.DispatcherPriority.Background,
                            new Action(delegate { }));
                           cb.IsDropDownOpen = true;
                       }
                   }
               }

               private static T FindVisualParent<T>(UIElement element) where T : UIElement
               {
                   UIElement parent = element;
                   while (parent != null)
                   {
                       T correctlyTyped = parent as T;
                       if (correctlyTyped != null)
                       {
                           return correctlyTyped;
                       }

                       parent = VisualTreeHelper.GetParent(parent) as UIElement;
                   }
                   return null;
               }*/

        void cbo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0 || this.SelectedTrip == null)
                return;
            this.SelectedTrip.TicketBook = (TicketBook)e.AddedItems[0];
        }

        private void TripsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Alt)
            {
                switch (e.SystemKey) //when holding down the Alt key all keys getting system, so U have to check for system key
                {
                    case Key.C: //cash
                        this.TripsGrid.CommitEdit(); //if entered amount if other payment method should update source and know total paid
                        if (this.SelectedTrip.TotalPaid == 0)
                            this.SelectedTrip.CashPaid = this.SelectedTrip.TotalPrice;

                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.CashColumn);
                        this.TripsGrid.BeginEdit();

                        break;
                    case Key.K:
                        if (this.SelectedTrip.TotalPaid == 0)
                            this.SelectedTrip.CheckPaid = this.SelectedTrip.TotalPrice;

                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.CheckColumn);
                        this.TripsGrid.BeginEdit();
                        break;
                    case Key.G:
                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.ChargeColumn);
                        this.TripsGrid.BeginEdit();
                        break;
                    case Key.T:
                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.cboTicketTypes);
                        this.TripsGrid.BeginEdit();

                        break;
                    case Key.P:
                        this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.tripPrice_column);
                        this.TripsGrid.BeginEdit();
                        break;
                    case Key.D:
                        if (this.SelectedTrip == null)
                            return;
                        this.NewTrip = this.SelectedTrip;
                        this.NewTrip.ObjectState = BOState.Edit;
                        break;
                    default:
                        break;
                }
            }
            /* try
             {


             if (TripsGrid.CurrentCell.Column == this.tripPrice_column)
             {
                 if (this.SelectedTrip != null)
                     if (this.SelectedTrip.TicketTypePaid > 0)
                     {


                       //  decimal textEntertParsed=0;
                         var txt = this.tripPrice_column.GetCellContent(this.SelectedTrip);
                         Debug.WriteLine(txt);
                       // Decimal.TryParse(txt,out textEntertParsed);
                       //  this.SelectedTrip.CashPaid = textEntertParsed; //this.SelectedTrip.TripPrice.GetValueOrDefault(0);
                     }
             }
             }
             catch (Exception ex)
             {

                 UIController.DisplayErrorMessage(ex);
             }*/
        }
        int lastSelectedRowIndex = -1;
        private void TripsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //combo colimn tripggers this event so need to add some logic
            //if (this.TripsGrid.CurrentCell.Column != null &&
            //    (this.TripsGrid.CurrentCell.Column.Header.ToString() == "Ticket Type" ||
            //    this.TripsGrid.CurrentCell.Column.Header.ToString() == "Ticket Qt."))
            //    return;
            if (this.TripsGrid.SelectedIndex != lastSelectedRowIndex)
            {
                this.TripsGrid.CurrentCell = new DataGridCellInfo(this.SelectedTrip, this.tripPrice_column);
                //if (this.PreviousSelectedTrip != null)
                //  this.PreviousSelectedTrip.SetDefaults();
                this.TripsGrid.BeginEdit();

                lastSelectedRowIndex = this.TripsGrid.SelectedIndex;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItemResetPriceAndPmts_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedTrip == null) return;
                this.SelectedTrip.ResetPriceAndPmts();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        // **IH**
        private void MenuItemSelectAllTrips_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var trip in this.ActiveDailyEnvelope.TripsCovered)
                {
                    trip.Selected = true;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        // **IH**
        private void MenuItemDeSelectAllTrips_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var trip in this.ActiveDailyEnvelope.TripsCovered)
                {
                    trip.Selected = false;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        int scaningThread = 0;
        //private Twain tw;
        private void btnScan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string scannerName = ManualSource.SelectedItem.ToString();
                new Thread(() =>
                {
                    try
                    {
                        if (scaningThread == 1)
                            return;

                        Interlocked.Exchange(ref scaningThread, 1);
                        ScanSettings _settings;

                        //IsEnabled = false;
                        bool? baw = true;
                        _settings = new ScanSettings
                        {
                            UseDocumentFeeder = false,
                            ShowTwainUI = false,
                            ShowProgressIndicatorUI = false,
                            UseDuplex = false,
                            Resolution = (baw ?? false)
                                             ? ResolutionSettings.Fax
                                             : ResolutionSettings.ColourPhotocopier,
                            Area = null,
                            ShouldTransferAllPages = true,
                            Rotation = new RotationSettings
                            {
                                AutomaticRotate = false,
                                AutomaticBorderDetection = false
                            }
                        };

                        try
                        {
                            //if (SourceUserSelected.IsChecked == true)
                            _twain.SelectSource(scannerName);
                            // _twain.SelectSource("Neat Mobile Scanner");
                            _twain.StartScanning(_settings);
                        }
                        catch (TwainException ex)
                        {
                            throw ex;
                            //MessageBox.Show(ex.Message);
                        }

                        Interlocked.Exchange(ref scaningThread, 0);
                        //IsEnabled = true;
                        //HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;
                        //if (hwndSource != null)
                        //{
                        //    tw.Init(hwndSource.Handle);
                        //} 
                        //tw.Acquire();
                    }
                    catch (Exception ex)
                    {
                        Interlocked.Exchange(ref scaningThread, 0);
                        App.Current.Dispatcher.Invoke(new Action(() => { UIController.DisplayErrorMessage(ex); }));
                    }
                }).Start();

            }
            catch (Exception) { }
        }

        private void SavedFilesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                SavedFile file = this.SavedFilesGrid.SelectedItem as SavedFile;

                if (file == null)
                    return;

                string path = System.IO.Path.Combine(GetFilesFolder(true), file.FileName);

                if (!string.IsNullOrEmpty(path))
                {
                    System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void btnAttach_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                string filePath;
                string newFilePath;
                openFileDialog.Filter = "PDF files (*.pdf)|*.pdf";

                if (openFileDialog.ShowDialog() == true && openFileDialog.FileName.Length > 0)
                {
                    filePath = openFileDialog.FileName;

                    /*
                    string newFileName = "Any Name" + System.IO.Path.GetExtension(filePath);
                    newFilePath = System.IO.Path.Combine("New File Path", newFileName);

                    if (File.Exists(newFilePath) &&
                        MessageBox.Show("File exists, do You want to overwrite?", "Overwrite", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        return;

                    File.Copy(filePath, newFilePath, true);
                    */
                    string fileName = GetFileName();

                    if (File.Exists(GetFilesFolder(true) + fileName + System.IO.Path.GetExtension(filePath)) &&
                        MessageBox.Show("File exists, do You want to overwrite?", "Overwrite", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        return;


                    File.Copy(filePath, GetFilesFolder(true) + fileName + System.IO.Path.GetExtension(filePath), true);

                    ActiveDailyEnvelope.SavedFiles.Add(new SavedFile() { FileName = fileName + System.IO.Path.GetExtension(filePath) });
                    this.ActiveDailyEnvelope.OnPropertyChanged("SavedFilesDisplay");
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void mnuDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SavedFile file = this.SavedFilesGrid.SelectedItem as SavedFile;
                if (file == null)
                    return;

                file.IsMarkedForDeletion = true;
                this.ActiveDailyEnvelope.OnPropertyChanged("SavedFilesDisplay");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void ManualSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.ScanningDeviceName = this.ManualSource.SelectedItem.ToString();
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
    }


    public class ExpenseTypesList : ObservableCollection<ExpenseType> { }
    public class TicketBooksList : ObservableCollection<TicketBook> { }


    public class ExpenseValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            DEExpense expense = (value as BindingGroup).Items[0] as DEExpense;
            //if (expense.Expense == null)
            //{
            if (expense.ExpenseTypeID == null)
            {

                return new ValidationResult(false,
                    "Please select an expense type");
            }
            if (expense.ExpenseAmount <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a postive Amount");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class TicketBooksSoldValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            TicketBookSold tbs = (value as BindingGroup).Items[0] as TicketBookSold;
            if (tbs.TicketBookTypeSold == null)
            {
                return new ValidationResult(false,
                    "Please select the Book that was sold");
            }
            if (tbs.Price <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a postive Amount");
            }
            if (tbs.Qty <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a postive Quantity");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class TicketReceivedValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            TicketsReceived tbs = (value as BindingGroup).Items[0] as TicketsReceived;
            if (tbs.FromTicketBook == null)
            {
                return new ValidationResult(false,
                    "Please select the ticket type");
            }
            if (tbs.TicketValue <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a postive Amount");
            }
            //if (tbs.QtyReceived <= 0)
            //{
            //    return new ValidationResult(false,
            //        "Please enter a postive Quantity");
            //}
            else
            {

                return ValidationResult.ValidResult;
            }
        }
    }
}
