//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CreditLimitService.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public int CustomerID { get; set; }
        public int CustomerType { get; set; }
        public string CompanyName { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string HouseNo { get; set; }
        public string StreetName { get; set; }
        public string SuiteNo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string HomePhoneNo { get; set; }
        public string BusinessPhoneNo { get; set; }
        public string CellPhoneNo { get; set; }
        public bool IsInvoiceCustomer { get; set; }
        public string FaxNo { get; set; }
        public string EmailAddress { get; set; }
        public bool NeedsPassCode { get; set; }
        public Nullable<decimal> AmountOfCreditLimit { get; set; }
    }
}
