﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{

    [DataContract]
    public class Trip : BO
    {
        public Trip()
        {
            this.TripType = TripTypes.Regular;
            this.IsLongDistanceTrip = Trip.IsLongDistance;
            _callBackPhoneNo = "";
            _callInPhoneNo = "";
            this._IsCommissionEligible = true;
        }

        public BOL_OTR.Trip_OTR ToOTRTrip()
        {
            BOL_OTR.Trip_OTR otr_trip = new BOL_OTR.Trip_OTR()
            {
                TripID = _tripID,
                FromLocation = _fromLocation,
                ToLocation = _toLocation,
                CustomerID = _chargeToCustomerID,
                CustomerHomePhoneNo = _chargeToCustomer.HomePhoneNo,
                CustomerCellPhoneNo = _chargeToCustomer.CellPhoneNo,
                CustomerName = _chargeToCustomer.CustomerName
            };
            return otr_trip;
        }

        [DataMember]
        public BOL_OTR.TripDetailedStatusOptions DetailedStatus { get; set; }

        bool _IsRequestingDestinationChangePermission;
        public bool IsRequestingDestinationChangePermission
        {
            get { return _IsRequestingDestinationChangePermission; }
            set { this._IsRequestingDestinationChangePermission = value; }
        }

        public TripEnteredOptions TripEnteredBy { get; set; }
        decimal _ticketPaidTotal;

        public decimal TicketPaidTotal
        {
            get { return _ticketPaidTotal; }
            set
            {
                _ticketPaidTotal = value;
            }
        }

        bool _ezpass;
        public bool IsEzpass
        {
            get { return _ezpass; }
            set
            {
                _ezpass = value;
            }
        }
        decimal _tollReceivables;

        public decimal TollReceivables
        {
            get { return _tollReceivables; }
            set
            {
                _tollReceivables = value;
            }
        }
        decimal _tollPayables;

        public decimal TollPayables
        {
            get { return _tollPayables; }
            set
            {
                _tollPayables = value;
            }
        }
        decimal _ccFee;

        public decimal CCFee
        {
            get { return _ccFee; }
            set
            {
                _ccFee = value;
            }
        }


        int _tripID;

        [DataMember]
        public int TripID
        {
            get { return _tripID; }
            set { _tripID = value; }
        }

        int _tripNo;
        [DataMember]
        public int TripNo
        {
            get { return _tripNo; }
            set { _tripNo = value; }
        }

        int _ReturnTripOfTripID;
        [DataMember]
        public int ReturnTripOfTripID
        {
            get { return _ReturnTripOfTripID; }
            set { _ReturnTripOfTripID = value; }
        }

        public int _chargeToCustomerID;

        [DataMember]
        public int ChargeToCustomerID
        {
            get { return _chargeToCustomerID; }
            set { _chargeToCustomerID = value; }
        }

        Customer _chargeToCustomer;

        [DataMember(Order = 1)]
        public Customer ChargeToCustomer
        {
            get { return _chargeToCustomer; }
            set
            {
                _chargeToCustomer = value;

                if (value != null)
                {
                    this._tripType = TripTypes.Charge;
                    this._chargeToDriver = null;

                    if (this.TotalPaid > 0)
                    {
                        if (this.CashPaid == this.TotalPrice)
                        {
                            this.ChargePaid = this.TotalPrice;
                            this.CashPaid = 0;
                        }
                    }
                    else if (this.TotalPaid == 0)
                    {
                        this.ChargePaid = this.TotalPrice;
                    }
                }
                else
                {
                    this._tripType = TripTypes.Regular;
                    this.ChargePaid = 0;
                }
                this.OnPropertyChanged("ChargeToCustomer");
                this.OnPropertyChanged("ChargeToDriver");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }
        string _callInPhoneNo;

        [DataMember]
        public string CallInPhoneNo
        {
            get { return _callInPhoneNo; }
            set { _callInPhoneNo = value; }
        }
        string _callBackPhoneNo;

        [DataMember]
        public string CallBackPhoneNo
        {
            get { return _callBackPhoneNo; }
            set { _callBackPhoneNo = value; this.OnPropertyChanged("CallBackPhoneNo"); }
        }
        DateTime? _timeOfCall;

        [DataMember]
        public DateTime? TimeOfCall
        {
            get { return _timeOfCall; }
            set { _timeOfCall = value; }
        }

        string _eTAInMinutes;

        [DataMember]
        public string ETAInMinutes
        {
            get { return _eTAInMinutes; }
            set
            {
                _eTAInMinutes = value;
                if (this.ObjectState == BOState.New || this.ObjectState == BOState.Edit)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        if (value.IndexOf(':') > 0 && value.Length > 2)
                        {
                            DateTime etaDate;
                            DateTime.TryParse(value, out etaDate);
                            if (etaDate != null)
                                this.ETA = etaDate;
                        }
                        else
                        {
                            double noOfMinutes;
                            double.TryParse(value, out noOfMinutes);
                            if (noOfMinutes > 0)
                                this.ETA = DateTime.Now.AddMinutes(noOfMinutes);
                        }
                    }
                    else
                        this.ETA = null;

                }
                this.OnPropertyChanged("ETAInMinutes");
                this.OnPropertyChanged("ETA");
            }
        }

        DateTime? timeDriverAssigned;

        [DataMember]
        public DateTime? TimeDriverAssigned
        {
            get { return timeDriverAssigned; }
            set { timeDriverAssigned = value; this.OnPropertyChanged("TimeDriverAssigned"); }
        }

        DateTime? _eTA;

        [DataMember]
        public DateTime? ETA
        {
            get { return _eTA; }
            set
            {
                _eTA = value;
                this.OnPropertyChanged("TripAdditionNotes"); this.OnPropertyChanged("ETA");
            }
        }

        DateTime? _eTAToLocation1;
        [DataMember]
        public DateTime? ETAToLocation1
        {
            get { return _eTAToLocation1; }
            set { _eTAToLocation1 = value; }
        }

        DateTime? _eTABackToBase;
        [DataMember]
        public DateTime? ETABackToBase
        {
            get { return _eTABackToBase; }
            set { _eTABackToBase = value; }
        }
        DateTime? _tripStarted;

        [DataMember]
        public DateTime? TripStarted
        {
            get { return _tripStarted; }
            set { _tripStarted = value; }
        }

        DateTime? _scheduledFor;

        [DataMember]
        public DateTime? ScheduledFor
        {
            get { return _scheduledFor; }
            set { _scheduledFor = value; this.OnPropertyChanged("ScheduledFor"); }
        }

        string _scheduledForInMinutes;

        [DataMember]
        public string ScheduledForInMinutes
        {
            get { return _scheduledForInMinutes; }
            set
            {
                _scheduledForInMinutes = value;

                if (this.ObjectState == BOState.New || this.ObjectState == BOState.Edit)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        if (value.IndexOf(':') > 0 && value.Length > 2)
                        {
                            DateTime schDate;
                            DateTime.TryParse(value, out schDate);
                            if (schDate != null)
                                this.ScheduledFor = schDate;
                        }
                        else
                        {
                            double noOfMinutes;
                            double.TryParse(value, out noOfMinutes);
                            if (noOfMinutes > 0)
                                this.ScheduledFor = DateTime.Now.AddMinutes(noOfMinutes);
                        }
                    }
                    else
                        this.ScheduledFor = null;
                }
                this.OnPropertyChanged("ScheduledFor");
                this.OnPropertyChanged("ScheduledForInMinutes");

            }

        }

        public bool IsTwoMinutesToTrip
        {
            get
            {
                if ((_eTA != null || _scheduledFor != null))
                {
                    DateTime supposedToBe = _eTA != null ? _eTA.Value : _scheduledFor.Value;

                    return (supposedToBe - DateTime.Now).TotalSeconds <= 120;
                }
                return false;
            }
        }

        public bool IsAssignedNotInTime
        {
            get
            {
                if (timeDriverAssigned != null && (_eTA != null || _scheduledFor != null))
                {
                    DateTime supposedToBe = _eTA != null ? _eTA.Value : _scheduledFor.Value;

                    if ((timeDriverAssigned.Value.TimeOfDay > supposedToBe.TimeOfDay &&
                        (timeDriverAssigned.Value.TimeOfDay - supposedToBe.TimeOfDay).TotalSeconds > 120) ||
                        (timeDriverAssigned.Value.TimeOfDay < supposedToBe.TimeOfDay &&
                        (supposedToBe.TimeOfDay - timeDriverAssigned.Value.TimeOfDay).TotalSeconds > 600))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        DateTime? _tripEnded;

        [DataMember]
        public DateTime? TripEnded
        {
            get { return _tripEnded; }
            set { _tripEnded = value; }
        }
        int _takenByDispatcherID;

        [DataMember]
        public int TakenByDispatcherID
        {
            get { return _takenByDispatcherID; }
            set { _takenByDispatcherID = value; }
        }

        [DataMember]
        public string DispatcherCode { get; set; }

        Driver _drivenBy;

        [DataMember]
        public Driver DrivenBy
        {
            get { return _drivenBy; }
            set { _drivenBy = value; this.OnPropertyChanged("DrivenBy"); }
        }

        string _sortByDriver;

        public string SortByDriver
        {
            get
            {
                if (DrivenBy != null)
                    return _drivenBy.DriverCode;
                return string.Empty;
            }
            set { _sortByDriver = value; }
        }

        Vehicle _drivenWithVehicle;

        [DataMember]
        public Vehicle DrivenWithVehicle
        {
            get { return _drivenWithVehicle; }
            set { _drivenWithVehicle = value; }
        }

        VehicleTypes _vehicleTypeRequested;

        [DataMember]
        public VehicleTypes VehicleTypeRequested
        {
            get { return _vehicleTypeRequested; }
            set { _vehicleTypeRequested = value; this.OnPropertyChanged("VehicleTypeRequested"); }
        }

        [DataMember]
        public double From_Latitude { get; set; }
        [DataMember]
        public double From_Longitude { get; set; }
        [DataMember]
        public double To_Latitude { get; set; }
        [DataMember]
        public double To_Longitude { get; set; }

        string _fromLocationHouseNo = "";

        [DataMember]
        public string FromLocationHouseNo
        {
            get { return _fromLocationHouseNo; }
            set { _fromLocationHouseNo = value; this.OnPropertyChanged("FromLocationHouseNo"); this.OnPropertyChanged("FromLocation"); ; this.TimeStampTrip(); }
        }
        string _fromLocationStreetName = "";

        [DataMember]
        public string FromLocationStreetName
        {
            get { return _fromLocationStreetName; }
            set
            {
                _fromLocationStreetName = value;
                this.OnPropertyChanged("FromLocationStreetName");
                this.OnPropertyChanged("FromLocation"); this.TimeStampTrip();
            }
        }

        string _fromLocation = "";

        [DataMember]
        public string FromLocation
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._fromLocation))
                    return (this.FromLocationHouseNo + " " + this.FromLocationStreetName).Trim(); ;
                return _fromLocation;
            }
            set { _fromLocation = value; this.OnPropertyChanged("FromLocation"); }
        }

        string _toLocationHouseNo = "";

        [DataMember]
        public string ToLocationHouseNo
        {
            get { return _toLocationHouseNo; }
            set { _toLocationHouseNo = value; this.TimeStampTrip(); this.OnPropertyChanged("ToLocation"); this.OnPropertyChanged("ToLocationHouseNo"); }
        }
        string _toLocationStreetName = "";

        [DataMember]
        public string ToLocationStreetName
        {
            get { return _toLocationStreetName; }
            set
            {
                _toLocationStreetName = value; this.OnPropertyChanged("ToLocation"); this.TimeStampTrip();
                this.OnPropertyChanged("ToLocationStreetName");
            }
        }

        string _toLocation;

        [DataMember]
        public string ToLocation
        {
            get
            {
                if ((this.IsRoundTrip == true) && (string.IsNullOrWhiteSpace(this._toLocation)))
                    return (this.ToLocationHouseNo + " " + this.ToLocationStreetName + "/" + this.FromLocationStreetName).Trim();
                if (string.IsNullOrWhiteSpace(this._toLocation))
                    return (this.ToLocationHouseNo + " " + this.ToLocationStreetName).Trim();

                return _toLocation;
            }
            set
            {
                _toLocation = value;
            }
        }

        string _toCity;
        [DataMember]
        public string ToCity
        {
            get { return _toCity; }
            set { _toCity = value; }
        }

        string _fromCity;
        [DataMember]
        public string FromCity
        {
            get { return _fromCity; }
            set { _fromCity = value; }
        }

        string _fromLocationComment;

        [DataMember]
        public string FromLocationComment
        {
            get { return _fromLocationComment; }
            set { _fromLocationComment = value; }
        }
        string _toLocationComment;

        [DataMember]
        public string ToLocationComment
        {
            get { return _toLocationComment; }
            set { _toLocationComment = value; this.OnPropertyChanged("TripAdditionNotes"); }
        }

        TripStatusTypes _tripStatus;

        [DataMember]
        public TripStatusTypes TripStatus
        {
            get { return _tripStatus; }
            set
            {
                _tripStatus = value;

                this.OnPropertyChanged("TripStatus");
                this.OnPropertyChanged("IsCanceldTrip");
            }
        }

        public bool IsCanceldTrip
        {
            get { return this.TripStatus == TripStatusTypes.Canceled; }
        }

        bool _isRoundTrip;

        [DataMember]
        public bool IsRoundTrip
        {
            get { return _isRoundTrip; }
            set
            {
                _isRoundTrip = value;
                this.OnPropertyChanged("TripAdditionNotes");
                this.OnPropertyChanged("IsRoundTrip");
                this.OnPropertyChanged("IsRoundTrip");
            }
        }

        int _medicaidDispatchTripNo;
        [DataMember]
        public int MedicaidDispatchTripNo
        {
            get
            { return _medicaidDispatchTripNo; }
            set
            {
                _medicaidDispatchTripNo = value;
                this.OnPropertyChanged("MedicaidDispatchTripNo");
                this.OnPropertyChanged("SMnumber");
            }
        }

        public string SMnumber
        {
            get
            {
                if (this.MedicaidDispatchTripNo > 0)
                {
                    return "M-" + this.MedicaidDispatchTripNo.ToString();
                }
                else
                    return "";
            }
        }

        TripTypes _tripType;

        public TripTypes TripType
        {
            get { return _tripType; }
            set
            {
                _tripType = value;
                this.OnPropertyChanged("IsChargeTrip");
                this.OnPropertyChanged("IsFreeTrip");
                this.OnPropertyChanged("ChargeToCustomer");
                this.OnPropertyChanged("ChargeToDriver");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }

        int _billedOnReferenceID;

        [DataMember]
        public int BilledOnReferenceID
        {
            get { return _billedOnReferenceID; }
            set { _billedOnReferenceID = value; }
        }

        int _assignedToDailyEnvelopeID;

        [DataMember]
        public int AssignedToDailyEnvelopeID
        {
            get { return _assignedToDailyEnvelopeID; }
            set { _assignedToDailyEnvelopeID = value; }
        }

        Driver _chargeToDriver;

        [DataMember(Order = 1)]
        public Driver ChargeToDriver
        {
            get { return _chargeToDriver; }
            set
            {
                _chargeToDriver = value;

                if (value != null)
                {
                    this._tripType = TripTypes.DriverTrip;
                    this._chargeToCustomer = null;

                    if (this.TotalPaid == 0)
                    {
                        this.ChargePaid = this.TotalPrice;
                    }
                }
                else
                {
                    if (this._chargeToCustomer == null)
                    {
                        this._tripType = TripTypes.Regular;
                        this.ChargePaid = 0;
                    }
                }


                this.OnPropertyChanged("ChargeToDriver");
                this.OnPropertyChanged("ChargeToCustomer");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }

        public string ChargedTo
        {
            get
            {
                if (this.TripType == TripTypes.Regular)
                    return string.Empty;
                return this.TripType == TripTypes.DriverTrip ? this.ChargeToDriver.ToString() : this.ChargeToCustomer.ToString();
            }
        }

        int? _noOfStops;

        [DataMember]
        public int? NoOfStops
        {
            get { return _noOfStops; }
            set { _noOfStops = value; this.OnPropertyChanged("TripAdditionNotes"); this.OnPropertyChanged("NoOfStops"); }
        }

        double _waitingTime;

        [DataMember]
        public double WaitingTime
        {
            get { return _waitingTime; }
            set { _waitingTime = value; }
        }
        decimal _stopsCharge;

        [DataMember]
        public decimal StopsCharge
        {
            get { return _stopsCharge; }
            set
            {
                _stopsCharge = value;
                this.OnPropertyChanged("TotalPrice");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }
        decimal _waitingCharge;

        [DataMember]
        public decimal WaitingCharge
        {
            get { return _waitingCharge; }
            set
            {
                _waitingCharge = value;
                this.OnPropertyChanged("TotalPrice");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }
        decimal _tollCharge;

        [DataMember]
        public decimal TollCharge
        {
            get { return _tollCharge; }
            set
            {
                _tollCharge = value;
                this.OnPropertyChanged("TotalPrice");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }

        decimal _miniVanCharge;

        [DataMember]
        public decimal MiniVanCharge
        {
            get { return _miniVanCharge; }
            set
            {
                _miniVanCharge = value;
                this.OnPropertyChanged("TotalPrice");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }
        decimal _outOfCarCharge;

        [DataMember]
        public decimal OutOfCarCharge
        {
            get { return _outOfCarCharge; }
            set { _outOfCarCharge = value; }
        }
        decimal _extraCharge;

        [DataMember]
        public decimal ExtraCharge
        {
            get { return _extraCharge; }
            set
            {
                _extraCharge = value;
                this.OnPropertyChanged("TotalPrice");
                this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }

        MedicaidTrip _medicaidTripInfo;

        [DataMember]
        public MedicaidTrip MedicaidTripInfo
        {
            get { return _medicaidTripInfo; }
            set { _medicaidTripInfo = value; }
        }

        Vehicle _vehicleInfo;
        [DataMember]
        public Vehicle VehicleInfo
        {
            get { return _vehicleInfo; }
            set { _vehicleInfo = value; }
        }

        public string TripAdditionNotes
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append(this.ToLocationComment);
                if ((NoOfStops != null) && (NoOfStops > 0))
                    sb.Append("\nStops: " + this.NoOfStops);
                //if(ETA != null)
                //    sb.AppendLine("ETA: " + this.ETA.Value.ToString("hh:MM"));
                if (this.IsRoundTrip)
                    sb.Append(" - Round Trip");
                if (this.MiniVanCharge > 0 || this.VehicleTypeRequested == VehicleTypes.MiniVan)
                    sb.Append(" - MiniVan");

                return sb.ToString();
            }

        }

        decimal? _tripPrice;

        [DataMember]
        public decimal? TripPrice
        {
            get { return _tripPrice; }
            set
            {
                _tripPrice = value;
                this.OnPropertyChanged("TotalPrice"); this.OnPropertyChanged("TripPrice"); this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }
        decimal _totalAdditionalCharges;

        [DataMember]
        public decimal TotalAdditionalCharges
        {
            get { return _totalAdditionalCharges; }
            set { _totalAdditionalCharges = value; this.OnPropertyChanged("TotalPrice"); this.OnPropertyChanged("AllowSaveToDailyEnvelope"); }
        }

        decimal _totalPrice;

        [DataMember]
        public decimal TotalPrice
        {
            get
            {
                if (!this.StaticData)
                {
                    return this.TripPrice.GetValueOrDefault(0) + this.StopsCharge + this.WaitingCharge + this.TollCharge + this.MiniVanCharge + this.ExtraCharge;

                }
                else
                    return _totalPrice;

            }
            set
            {
                _totalPrice = value; this.OnPropertyChanged("AllowSaveToDailyEnvelope");

            }
        }

        decimal _totalTripForDailyEnvelope;

        [DataMember]
        public decimal TotalTripForDailyEnvelope
        {
            get
            {
                if (!this.StaticData)
                    return this.TripPrice.GetValueOrDefault(0) + this.StopsCharge + this.WaitingCharge + this.MiniVanCharge + this.ExtraCharge;
                else
                    return _totalTripForDailyEnvelope;

            }
            set
            {
                _totalTripForDailyEnvelope = value; this.OnPropertyChanged("AllowSaveToDailyEnvelope");
            }
        }

        public decimal TotalPriceCompanyIncome
        {
            get { return this.TripPrice.GetValueOrDefault(0) + this.StopsCharge + this.WaitingCharge + this.MiniVanCharge + this.ExtraCharge; }
        }

        public decimal TotalPriceDriverIncome
        {
            get { return this.TripPrice.GetValueOrDefault(0) + this.StopsCharge + this.WaitingCharge + this.MiniVanCharge + this.ExtraCharge - this.CCPaidCompanyCharge; }
        }

        string _tripDescription;

        [DataMember]
        public string TripDescription
        {
            get { return _tripDescription; }
            set { _tripDescription = value; }
        }

        public bool IsChargeTrip
        {
            get { return this.TripType == TripTypes.Charge; }
        }

        public bool IsFreeTrip
        {
            get { return this.TripType == TripTypes.DriverTrip; }
        }

        [DataMember]
        public int AddedBySessionID { get; set; }

        public void TimeStampTrip()
        {
            if (this.TripEnteredBy == TripEnteredOptions.ByDispatcher
                && this.ObjectState == BOState.New
                && this.TimeOfCall == null)
            {
                this.TimeOfCall = DateTime.Now;
                this.OnPropertyChanged("TimeOfCall");
            }
        }

        int paymentID;

        [DataMember]
        public int PaymentID
        {
            get { return paymentID; }
            set { paymentID = value; }
        }

        decimal _cashPaid;
        [DataMember]
        public decimal CashPaid
        {
            get { return _cashPaid; }
            set
            {
                _cashPaid = value;
                this.OnPropertyChanged("CashPaid"); this.OnPropertyChanged("TotalPaid");
            }
        }

        decimal _checkPaid;
        [DataMember]
        public decimal CheckPaid
        {
            get { return _checkPaid; }
            set { _checkPaid = value; this.OnPropertyChanged("CheckPaid"); this.OnPropertyChanged("TotalPaid"); }
        }

        decimal _ccPaid;
        [DataMember]
        public decimal CCPaid
        {
            get { return _ccPaid; }
            set
            {
                _ccPaid = value;
                this.OnPropertyChanged("TotalPaid");
            }
        }

        public decimal CCPaidDriverComission
        {
            get
            {
                decimal value = this._ccPaid;
                if (value <= 10m) //then .50 company charge for cc not included in driver commission
                {
                    decimal decimalPart = (value - (int)value);

                    if (decimalPart == 0.50m)
                        value = value - decimalPart;
                }
                return value;
            }
        }

        public decimal CCPaidCompanyCharge
        {
            get
            {
                decimal value = this._ccPaid;
                if (value <= 10m) //then .50 company charge for cc not included in driver commission
                {
                    decimal decimalPart = (value - (int)value);

                    if (decimalPart == 0.50m)
                        value = decimalPart;
                }
                return value;
            }
        }

        decimal _chargePaid;
        [DataMember]
        public decimal ChargePaid
        {
            get { return _chargePaid; }
            set { _chargePaid = value; this.OnPropertyChanged("TotalPaid"); this.OnPropertyChanged("ChargePaid"); }
        }

        decimal _ticketPaid;
        [DataMember]
        public decimal TicketPaid
        {
            get
            {
                if (this._tripID > 0 && this._ticketBook == null)
                    return _ticketPaid;
                return this._ticketBook == null ? 0 : this._ticketBook.TicketValue * this._ticketQt;
            }
            set
            {
                _ticketPaid = value;
            }
        }

        int _ticketTypePaid;
        [DataMember]
        public int TicketTypePaid
        {
            get { return _ticketTypePaid; }
            set
            {
                _ticketTypePaid = value;

                this.OnPropertyChanged("TicketPaid");
                this.OnPropertyChanged("TotalCharges");
                this.OnPropertyChanged("TotalPaid"); //
            }
        }

        public void SetDefaults()
        {
            this.SetPriceFromTicekts();
            this.SetPriceFromCC();
            this.SetChargeFromAssigned();
            this.SetCashPaidDefault();
        }
        public void SetPriceFromTicekts()
        {
            if (this.TotalPrice == 0)
                this.TripPrice = this.TicketPaid;
            //    this.TripPrice = this.TicketPaid+this.CashPaid;
        }
        public void SetPriceFromCC()
        {
            if (this.TotalPrice == 0)
                this.TripPrice = this.CCPaid;
        }
        public void SetChargeFromAssigned()
        {
            if (this.TotalPaid == 0 && (this.ChargeToCustomer != null || this.ChargeToDriver != null))
                this.ChargePaid = this.TotalPrice;

            if (this.TripPrice == 0)
                this.TripPrice = this.CashPaid + this.ChargePaid + this.CCPaid + this.CheckPaid;

        }
        public void SetCashPaidDefault()
        {
            if (this.TotalPaid == 0)
                this.CashPaid = this.TotalPrice;
        }

        public void ResetPriceAndPmts()
        {
            this.TripPrice = 0;
            //this.TotalPaid = 0;
            this.CashPaid = 0;
            this.CheckPaid = 0;
            this.ChargePaid = 0;
            this.TicketTypePaid = 0;
            this.TicketPaid = 0;


        }
        TicketBook _ticketBook;

        public TicketBook TicketBook
        {
            get { return _ticketBook; }
            set
            {
                _ticketBook = value;

                if (value != null && this._ticketQt == 0)
                {
                    this._ticketQt = 1;
                    this.OnPropertyChanged("TicketQt");
                }
                else if (value != null && _ticketBook.TicketBookID == 0)
                {
                    this._ticketQt = 0;
                    this.OnPropertyChanged("TicketQt");
                }

                this.OnPropertyChanged("TicketPaid");
                this.OnPropertyChanged("TotalPaid");
            }
        }

        int _ticketQt;

        public int TicketQt
        {
            get { return _ticketQt; }
            set
            {
                _ticketQt = value;
                this.OnPropertyChanged("TicketPaid");
                this.OnPropertyChanged("TotalPaid");
            }
        }

        decimal _companyIncome;

        public decimal CompanyIncome
        {
            get { return _companyIncome; }
            set { _companyIncome = value; }
        }

        decimal _MedicaidTripPay;

        public decimal MedicaidTripPay
        {
            get { return _MedicaidTripPay; }
            set { _MedicaidTripPay = value; }
        }

        decimal _MedicaidOverPay;

        public decimal MedicaidOverPay
        {
            get { return _MedicaidOverPay; }
            set { _MedicaidOverPay = value; }
        }

        public decimal TotalCharges
        {
            get { return this.CCPaidDriverComission + this.ChargePaid + this.TicketPaid; }
        }

        public decimal TotalPaid
        {
            get { return this.CashPaid + this.CheckPaid + this.CCPaid + this.ChargePaid + this.TicketPaid; }
        }
        public decimal TotalPaidResult
        {
            get { return (this.CashPaid + this.CheckPaid + this.CCPaid + this.ChargePaid + this.TicketPaidTotal) - (this.TollPayables + this.TollReceivables + CCFee); }
            //get { return this.CashPaid + this.CheckPaid + this.CCPaid + this.ChargePaid + this.TicketPaidTotal; }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.FromLocation) ||
                    !string.IsNullOrWhiteSpace(this.ToLocation);
            }

        }

        public bool AllowSaveToDailyEnvelope
        {
            get
            {
                return this.DrivenBy != null &&
                    this.TripStarted != null &&
                    this.TripPrice > 0 &&
                    !string.IsNullOrWhiteSpace(this.FromLocation) &&
                    !string.IsNullOrWhiteSpace(this.ToLocation) &&
                    (
                        (this.TripType == TripTypes.Charge && this.ChargeToCustomer != null)
                        ||
                        (this.TripType == TripTypes.DriverTrip && this.ChargeToDriver != null)
                        ||
                        this.TripType == TripTypes.Regular
                    );

            }
        }

        ScheduledTrip _attachedScheduledTrip;

        [DataMember]
        public ScheduledTrip AttachedScheduledTrip
        {
            get { return _attachedScheduledTrip; }
            set
            {
                _attachedScheduledTrip = value;
                this.OnPropertyChanged("AttachedFidelisTrip");
                this.OnPropertyChanged("IsFidelisTrip");
                this.OnPropertyChanged("IsMedicaidTrip");
                this.OnPropertyChanged("IsScheduldedTrip");
                this.OnPropertyChanged("ScheduledTripTypeAbbrevations");
                this.OnPropertyChanged("MedicaidDispatchTripNo");
            }
        }

        public bool IsScheduledTrip
        {
            get { return this.AttachedScheduledTrip != null; }
        }

        public bool IsFidelisTrip
        {
            get { return this.AttachedScheduledTrip != null && this.AttachedScheduledTrip.ScheduledTripType == ScheduledTripTypes.ByFidelis; }
        }

        [DataMember]
        public bool IsMedicaidTrip
        {
            get
            {
                return this.AttachedScheduledTrip != null && this.AttachedScheduledTrip.ScheduledTripType == ScheduledTripTypes.ByMedicaid;
            }

            set
            {
                //  this.AttachedScheduledTrip = null;
                this.OnPropertyChanged("IsMedicaidTrip");
                this.OnPropertyChanged("AttachedScheduledTrip");

            }
        }
        public string ScheduledTripTypeAbbrevations
        {
            get
            {
                if (this.AttachedScheduledTrip == null)
                    return "";
                else
                    switch (this.AttachedScheduledTrip.ScheduledTripType)
                    {
                        case ScheduledTripTypes.ByCustomer:
                            return "S";

                        case ScheduledTripTypes.ByMedicaid:
                            return "M";
                        case ScheduledTripTypes.ByFidelis:
                            return "F";
                        default:
                            return "";
                    }
            }
        }

        //bool _isLongDistanceTrip;
        //[DataMember]
        [DataMember]
        TripAreaTypes _tripAreaType;
        public TripAreaTypes TripAreaType
        {
            get { return _tripAreaType; }
            set
            {
                _tripAreaType = value;
                this.OnPropertyChanged("IsLongDistanceTrip");
            }
        }

        public bool IsLongDistanceTrip
        {
            get { return this.TripAreaType == TripAreaTypes.LongDistance; }
            set
            {
                if (value)
                    this._tripAreaType = TripAreaTypes.LongDistance;
                else
                    this._tripAreaType = TripAreaTypes.Local;

                this.OnPropertyChanged("TripAreaType");
            }
        }
        public bool StaticData { get; set; }
        public static bool IsLongDistance { get; set; }

        int _customerPassCodeID;
        [DataMember]
        public int CustomerPassCodeID
        {
            get { return _customerPassCodeID; }
            set { _customerPassCodeID = value; }
        }

        string _missingEnvelopeTripDrivenBy;

        public string MissingEnvelopeTripDrivenBy
        {
            get { return _missingEnvelopeTripDrivenBy; }
            set { _missingEnvelopeTripDrivenBy = value; }
        }

        bool isNeedToSendTextMsg;
        [DataMember]
        public bool IsNeedToSendTextMsg
        {
            get { return isNeedToSendTextMsg; }
            set { isNeedToSendTextMsg = value; }
        }

        bool _isNeedCancelTextMsg;
        [DataMember]
        public bool IsNeedCancelTextMsg
        {
            get { return _isNeedCancelTextMsg; }
            set { _isNeedCancelTextMsg = value; }
        }

        bool _IsCommissionEligible;
        [DataMember]
        public bool IsBonusEligible
        {
            get { return _IsCommissionEligible; }
            set { _IsCommissionEligible = value; }
        }

    }
}
