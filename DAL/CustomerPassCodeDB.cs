﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{
    public class CustomerPassCodeDB : Database
    {
        public CustomerPassCodeDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(CustomerPassCode customerPassCode)//(string passUserName, string passUserCode, int forCustomerID)
        {
            if (!customerPassCode.AllowSave)
                return false;

            string serverMessage;
            SqlParameter outParam = new SqlParameter("@CustomerPassCodeID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCustomerPassCode", new SqlParameter[]
            {
                new SqlParameter("@PassUserName",customerPassCode.PassUserName),
                new SqlParameter("@PassUserCode",customerPassCode.PassUserCode),
                new SqlParameter("@ForCustomerID",customerPassCode.ForCustomerID),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customerPassCode.CustomerPassCodeID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;

        }
        public static bool AddCustomerPassCode(CustomerPassCode customerPassCode)
        {
            using (CustomerPassCodeDB customerPassCoedDB = new CustomerPassCodeDB(null, null))
            {
                return customerPassCoedDB.Add(customerPassCode);
            }
        }


        public bool Edit(CustomerPassCode customerPassCode)
        {
            string serverMessage;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@CustomerPassCodeID",customerPassCode.CustomerPassCodeID),
                new SqlParameter("@PassUserName",customerPassCode.PassUserName),
                new SqlParameter("@PassUserCode",customerPassCode.PassUserCode),
                new SqlParameter("@ForCustomerID",customerPassCode.ForCustomerID),
            };

            int nofOfRecordsAffected = this.ExecuteNonQuery("UpdateCustomerPassCode", param, System.Data.CommandType.StoredProcedure, out serverMessage);
            return nofOfRecordsAffected > 0;



        }
        public static bool EditCustomerPassCode(CustomerPassCode customerPassCode)
        {
            using (CustomerPassCodeDB customerPassCoedDB = new CustomerPassCodeDB(null, null))
            {
                return customerPassCoedDB.Edit(customerPassCode);
            }
        }


        public static ObservableCollection<CustomerPassCode> GetCustomerPassCode(int customerID)
        {
            using (CustomerPassCodeDB customerPassCodeDB = new CustomerPassCodeDB(null, null))
            {
                return customerPassCodeDB.Get(customerID);
            }

        }
        public ObservableCollection<CustomerPassCode> Get(int customerID)
        {
            var customerPassCoeds = new ObservableCollection<CustomerPassCode>();
            using (SqlDataReader dr = this.GetReader(
                "GetCustomerPassCodes", new SqlParameter[]{
                    new SqlParameter("@ForCustomerID", customerID),
                    }, System.Data.CommandType.StoredProcedure)
                )
            {
                while (dr.Read())
                {
                    customerPassCoeds.Add(new CustomerPassCode()
                    {
                        CustomerPassCodeID = Convert.ToInt32(dr["CustomerPassCodeID"]),
                        ForCustomerID = Convert.ToInt32(dr["ForCustomerID"]),
                        PassUserName = dr["PassUserName"].ToString(),
                        PassUserCode = dr["PassUserCode"].ToString()
                    });
                }
            }

            return customerPassCoeds;
        }

        public bool Check(int customerID, string passCodeToCheck)
        {
            bool results = false;


            using (var db = new Database(null, null))
            {
                var YesNO = db.ExecuteScalar("SELECT dbo.CheckIfPassCodeIsCorrect(" + customerID + ",'" + passCodeToCheck + "')", new SqlParameter[] { }, System.Data.CommandType.Text);

                if (YesNO != DBNull.Value)
                    results = Convert.ToBoolean(YesNO);

            }

            return results;
        }
        public static bool CheckIfPassCodeCorrect(int customerID, string passCodeToCheck)
        {
            using (CustomerPassCodeDB customerPassCodeDB = new CustomerPassCodeDB(null, null))
            {
                return customerPassCodeDB.Check(customerID, passCodeToCheck);

            }
        }

    }
}

