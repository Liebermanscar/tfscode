﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace DAL
{
    public class UserDB : Database
    {
        public UserDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<User> Get(int userID)
        {
            var users = new ObservableCollection<User>();
            using (SqlDataReader dr = this.GetReader("GetUsers", new SqlParameter[] {new SqlParameter("@UserID", userID)}, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    users.Add(new User()
                    {
                        UserID = Convert.ToInt32(dr["UserID"]),
                        UserName = dr["UserName"].ToString(),
                        UserCode = dr["UserCode"].ToString(),
                        Password = dr["UserPassword"].ToString(),
                        UserType = (UserTypes)dr["UserType"]
                    });
                }
                return users;
            }
        }

        public static ObservableCollection<User> GetUsers(int userID)
        {
            using (UserDB userDB = new UserDB(null, null))
            {
                return userDB.Get(userID);
            }
        }
    }
}
