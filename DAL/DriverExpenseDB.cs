﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{
    public class DriverExpenseDB : Database
    {
        public DriverExpenseDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        public ObservableCollection<DriverExpense> Get(int driverID, int driverExpenseID, bool notPaidOnly, int driverPaymentID)
        {
            var driverExpenses = new ObservableCollection<DriverExpense>();
            using (SqlDataReader dr = this.GetReader("GetDriverExpenses", new SqlParameter[] {

            new SqlParameter("@DriverID", driverID),
            new SqlParameter("@DriverExpenseID", driverExpenseID),
            new SqlParameter("@NotPaidOnly", notPaidOnly),
            new SqlParameter("@DriverPaymentID", driverPaymentID)
            }, System.Data.CommandType.StoredProcedure))
            {

                while (dr.Read())
                {
                    driverExpenses.Add(new DriverExpense()
                    {

                        DriverID = Convert.ToInt32(dr["DriverID"]),
                        DriverCode = dr["DriverCode"].ToString(),
                        DriverExpenseID = Convert.ToInt32(dr["DriverExpenseID"]),
                        DateOfExpense = Convert.ToDateTime(dr["DateOfExpense"]),
                        DetailsOfExpense = dr["DetailsOfExpense"].ToString(),
                        AmountOfExpense = Convert.ToDecimal(dr["AmountOfExpense"])

                    });
                }
                return driverExpenses;
            }
        }
        public static ObservableCollection<DriverExpense> GetDriverExpense(int driverID, int driverExpenseID, bool notPaidOnly, int driverPaymentID)
        {
            using (DriverExpenseDB driverExpenseDB = new DriverExpenseDB(null, null))
            {
                return driverExpenseDB.Get(driverID, driverExpenseID, notPaidOnly, driverPaymentID);
            }
        }

        public bool Add(DriverExpense driverExpense)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@DriverExpenseID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddDriverExpense", new SqlParameter[]
              {
                new SqlParameter("@DriverID",driverExpense.DriverID),
                new SqlParameter("@DateOfExpense",driverExpense.DateOfExpense),
                new SqlParameter("@DetailsOfExpense", driverExpense.DetailsOfExpense),
                new SqlParameter("@AmountOfExpense", driverExpense.AmountOfExpense),
                outParam
              }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                driverExpense.DriverExpenseID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;

        }
        public static bool AddDriverExpense(DriverExpense driverExpense)
        {

            using (DriverExpenseDB driverExpenseDB = new DriverExpenseDB(null, null))

            {
                return driverExpenseDB.Add(driverExpense);
            }
        }

        public bool Edit(DriverExpense driverExpense)
        {

            return true;
        }
        public static bool EditDriverExpense(DriverExpense driverExpense)
        {
            using (DriverExpenseDB driverExpenseDB = new DriverExpenseDB(null, null))
            {
                return driverExpenseDB.Edit(driverExpense);
            }
        }

    }
}
