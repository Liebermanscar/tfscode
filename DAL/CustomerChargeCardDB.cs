﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;
using CCProc;


namespace DAL
{
    public class CustomerChargeCardDB : Database
    {
        public CustomerChargeCardDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public ObservableCollection<CreditCardResponse> Get(int customerID, int CCID, int ResponseID)
        {
            //  string serverMessage;
            var response = new ObservableCollection<CreditCardResponse>();
            using (SqlDataReader dr = this.GetReader("GetCreditCardResponse",
                new SqlParameter[]
                { new SqlParameter("@customerID",customerID),
                    new SqlParameter("@CCID", CCID),
                    new SqlParameter("@ResponseId",ResponseID)
                }
                , System.Data.CommandType.StoredProcedure))


                while (dr.Read())
                {
                    response.Add(new CreditCardResponse()
                    {
                        ResponseID = Convert.ToInt32(dr["PaymentID"]),
                        CcID = Convert.ToInt32(dr["PaymentID"]),
                        CCResponse = new CCProc.TransactionResponse()
                        {
                            AuthorizationCode = dr["AuthorizationCode"].ToString(),
                            ErrorCode = dr["ErrorCode"].ToString(),
                            ResponseDescription = dr["ResponseDescription"].ToString(),
                            Result = dr["Result"].ToString(),
                            TransactionNo = dr["TransactionNo"].ToString()
                        }
                    });
                }

            return response;
        }
        public static ObservableCollection<CreditCardResponse> GetResponses(int customerID, int CCID, int ResponseID)
        {
            using (CustomerChargeCardDB CustChargeResponceDb = new CustomerChargeCardDB(null, null))//(null, null))
            {
                return CustChargeResponceDb.Get(customerID, CCID, ResponseID);
            }
        }

        public bool Add(CreditCardResponse customerChargeCardResponse)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@ResponseId", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCreditCardResponse", new SqlParameter[]
            {
                new SqlParameter("@CCID",customerChargeCardResponse.CcID),
                new SqlParameter("@CustomerID",customerChargeCardResponse.CustomerID),
              //  new SqlParameter("@ResponseType",customerChargeCardResponse.CCResponse),
                new SqlParameter("@AuthorizationCode",customerChargeCardResponse.CCResponse.AuthorizationCode),
                new SqlParameter("@ErrorCode", customerChargeCardResponse.CCResponse.ErrorCode),
                new SqlParameter("@ResponseDescription", customerChargeCardResponse.CCResponse.ResponseDescription),
                new SqlParameter("@Result", customerChargeCardResponse.CCResponse.Result),
                new SqlParameter("@TransactionNo", customerChargeCardResponse.CCResponse.TransactionNo),
                  outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customerChargeCardResponse.ResponseID = Convert.ToInt32(outParam.Value);
            return noOfRecordsAffected > 0;
        }
        public static bool AddResponse(CreditCardResponse response)
        {
            using (CustomerChargeCardDB customerChargeCardDB = new CustomerChargeCardDB(null, null))
            {
                return customerChargeCardDB.Add(response);
            }
        }
        private bool Edit(CCProc.TransactionResponse CustPaymentInfo)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdatePayments", new SqlParameter[]
            {
               /* new SqlParameter("@PaymentID", CustPaymentInfo.PaymentID),
               new SqlParameter("@CustomerID",CustPaymentInfo.Customer.CustomerID ),
                new SqlParameter("@DatePaid", CustPaymentInfo.DatePaid),
                new SqlParameter("@AmountPaid", CustPaymentInfo.AmountPaid),
                new SqlParameter("@PaymentMethod", CustPaymentInfo.PaymentMethod),
                new SqlParameter("@ReferenceNo", CustPaymentInfo.ReferenceNo)*/
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }

        public static bool EditCustomerResponseInfo(CCProc.TransactionResponse CardResponse)
        {
            using (var customerChargeCardDB = new CustomerChargeCardDB(null, null))//(null, null))
            {
                // return CustomerPaymentInfoDB.EditCustomerPaymentsInfo(CustomerPaymentInfoDB);//.Edit(CustomerPayment);//.Edit(CustPaym);
                return customerChargeCardDB.Edit(CardResponse);
            }
        }

    }
}
