﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BOL
{
    public class DETicketLog : BO
    {
        private int _ticketLogId;

        public int TicketLogId
        {
            get { return _ticketLogId; }
            set { _ticketLogId = value; }
        }
        private int _ticketBookId;

        public int TicketBookId
        {
            get { return _ticketBookId; }
            set { _ticketBookId = value; }
        }
        private int _ticketNumber;

        public int TicketNumber
        {
            get { return _ticketNumber; }
            set { _ticketNumber = value; }
        }
        private int _dailyEnvelopeId;

        public int DailyEnvelopeId
        {
            get { return _dailyEnvelopeId; }
            set { _dailyEnvelopeId = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }
}
