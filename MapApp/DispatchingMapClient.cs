﻿using DispatcherService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using BOL;

namespace MapApp
{
    public class DispatchingMapClient : IDispatcherCallBack
    {
        static string endpointUrl = Properties.Settings.Default.DispatcherServiceURL;

        static EndpointAddress endpoint = new EndpointAddress(endpointUrl);
        static DuplexChannelFactory<IDispatcher> channelFactory;
        static IDispatcher clientProxy;
        DispatchingMapModel _MapModel;
        event TripAddedEventHandler Trip_Added;

        public DispatchingMapClient(DispatchingMapModel mapModel, TripAddedEventHandler tripAdded)
        {
            _MapModel = mapModel;
            Trip_Added = tripAdded;
        }

        public bool ConnectToDispatcherService()
        {
            try
            {
                NetTcpBinding binding = new NetTcpBinding(SecurityMode.None)
                {
                    SendTimeout = new TimeSpan(0, 0, 30),
                    ReceiveTimeout = TimeSpan.MaxValue,
                    MaxBufferPoolSize = 2147483647,
                    MaxBufferSize = 2147483647,
                    MaxReceivedMessageSize = 2147483647,
                    ReliableSession = new OptionalReliableSession() { Enabled = true, Ordered = true, InactivityTimeout = TimeSpan.MaxValue }
                };

                channelFactory = new DuplexChannelFactory<IDispatcher>(this, binding, endpoint);
                clientProxy = channelFactory.CreateChannel();

                return clientProxy.DispatcherJoin(new User() { UserID = 123456789, UserName = "MapApp" },2.0);

            }
            catch (Exception ex) { return false; }
        }

        public BOL.Trip[] GetAllTrips()
        {
            return clientProxy.GetAllTrips();
        }

        public BOL.DriverAndTripInformation[] GetAllDrivers()
        {
            return clientProxy.GetAllDriversAndTripInfo();
        }

        public void TripAdded(Trip trip, int byUserID)
        {
            //_MapModel.AddTrip(trip);
            Trip_Added(trip);
        }

        public void TripEdited(Trip trip)
        {
            //if (trip.DrivenBy != null)
            //    MapsController.UpdateTrip(trip, ObjectAction.Remove);
            //else
            //{
            //    MapsController.UpdateTrip(trip, ObjectAction.Remove);
            //    MapsController.UpdateTrip(trip, ObjectAction.Add);
            //}
        }

        public void TripRemoved(Trip trip)
        {
            TripOnMap tripToRemove = _MapModel.Trips.FirstOrDefault(t => t.Trip.TripID == trip.TripID);

            if (tripToRemove != null)
                _MapModel.Trips.Remove(tripToRemove);
        }

        public void DriverAdded(DriverAndTripInformation driver)
        {
            //MapsController.BingMap.
            //MapsController.UpdateCarStatus(new CarOnMap(driver.UnitDriver.DrivesVehicle.VehicleCode,
            //    driver.UnitDriver.DriverCode,
            //    driver.DriverStatus.ToDescription(), 
            //    driver.DriversLocation));
        }

        public void DriverEdited(DriverAndTripInformation driver)
        {
            //if (driver.DriverStatus == DriverStatusOptions.NintyEight)
            //    MapsController.UpdateCarStatus(driver, ObjectAction.Add);

        }

        public void DriverRemoved(DriverAndTripInformation driver)
        {
            //var carOnMap = new CarOnMap(driver.UnitDriver.DriverID, driver.UnitDriver.DriverCode, 0,0,);
            //MapsController.UpdateCarStatus(driver, ObjectAction.Remove);
        }

        AssignedUnitDiscrepencyOption GetAssignedUnitOptions()
        {
            throw new NotImplementedException();
        }

        void TrisAndDriversWereReset(bool isLongDistance)
        {
            throw new NotImplementedException();
        }

        public void DriversAndTripInfoReset(DriverAndTripInformation[] driverAndTripsArray)
        {
            throw new NotImplementedException();
        }

        public void SectionCarAvailabilityChanged(SectionCarsAvailability sectionChanged)
        {
            throw new NotImplementedException();
        }

        AssignedUnitDiscrepencyOption IDispatcherCallBack.GetAssignedUnitOptions()
        {
            throw new NotImplementedException();
        }

        void IDispatcherCallBack.TrisAndDriversWereReset(bool isLongDistance)
        {
            throw new NotImplementedException();
        }

        public void TripLocationAdded(Trip trip)
        {
            throw new NotImplementedException();
        }
    }

    public delegate void TripAddedEventHandler(Trip trip);
}
