﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class Vehicle : BO, IEditableObject
    {
        public Vehicle()
        {
            this.vehicleData = new VehicleDataStruct();
        }

        //[DataContract]
        struct VehicleDataStruct
        {
            //  [DataMember]
            public int VehicleID;
            //            [DataMember]      
            public string VehicleCode;
            //    [DataMember]
            public string Make;
            //[DataMember]
            public string Model;
            //[DataMember]
            public int Year;
            //[DataMember]
            public byte[] Image;
        }

        VehicleDataStruct vehicleData;
        VehicleDataStruct backupData;

        private bool inTxn = false;

        public bool IsEditing
        {
            get { return this.inTxn; }
        }

        [DataMember]
        public int VehicleID
        {
            get { return this.vehicleData.VehicleID; }
            set { this.vehicleData.VehicleID = value; }
        }


        [DataMember]
        public string VehicleCode
        {
            get { return this.vehicleData.VehicleCode; }
            set
            {
                this.vehicleData.VehicleCode = value;
                this.OnPropertyChanged("VehicleCode");
                this.OnPropertyChanged("AllowSave");
            }
        }

        [DataMember]
        public string Make
        {
            get { return this.vehicleData.Make; }
            set { this.vehicleData.Make = value; this.OnPropertyChanged("Make"); }
        }

        [DataMember]
        public string Model
        {
            get { return this.vehicleData.Model; }
            set { this.vehicleData.Model = value; this.OnPropertyChanged("Make"); }
        }

        [DataMember]
        public int Year
        {
            get { return this.vehicleData.Year; }
            set { this.vehicleData.Year = value; this.OnPropertyChanged("Make"); }
        }

        [DataMember]
        public byte[] Image
        {
            get { return this.vehicleData.Image; }
            set { this.vehicleData.Image = value; this.OnPropertyChanged("Image"); }
        }

        bool _isCompanyEZPass;

        public bool IsCompanyEZPass
        {
            get { return _isCompanyEZPass; }
            set { _isCompanyEZPass = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.VehicleCode);
            }

        }

        #region IEditableObject Members

        public void BeginEdit()
        {
            if (!inTxn)
            {
                this.backupData = this.vehicleData;
                this.inTxn = true;
                this.OnPropertyChanged("IsEditing");
            }
        }

        public void CancelEdit()
        {
            if (inTxn)
            {
                this.vehicleData = backupData;
                inTxn = false;
                this.OnPropertyChanged("Make");
                this.OnPropertyChanged("Model");
                this.OnPropertyChanged("VehicleCode");
                this.OnPropertyChanged("Year");
                this.OnPropertyChanged("Image");

                this.OnPropertyChanged("IsEditing");
            }
        }

        public void EndEdit()
        {
            if (inTxn)
            {
                backupData = new VehicleDataStruct();
                inTxn = false;
                this.OnPropertyChanged("IsEditing");
            }
        }

        #endregion
    }
}
