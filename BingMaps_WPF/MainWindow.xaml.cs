﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BingMaps_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow Self;    
        public MainWindow()
        {
            InitializeComponent();

            Self = this;

            
        }

        public MapMVVM mapMVVM
        {
            set { this.DataContext = value; }
            get { return this.DataContext as MapMVVM; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MapsController.UpdateCarStatus(new CarOnMap(236, "SYL", "Break", 40.702207, -73.952132));            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MapsController.UpdateCarStatus(new CarOnMap(236, "IL", "On Trip", 40.701439, -73.953033));

            MapsController.UpdateTrip(new TripOnMap(3456, "140 Middleton St", 0, 0), ObjectAction.Add);

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MapsController.Initialize();

            MapsController.SetZoomLevel(16);
        }
    }
}
