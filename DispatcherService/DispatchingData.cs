﻿using BOL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DispatcherService
{
    public class DispatchingData
    {
        public DispatchingData()
        {
            this.DriversAndTripInformation = new ObservableCollection<BOL.DriverAndTripInformation>();
            this.Trips = new ObservableCollection<BOL.Trip>();
            this.SectionsCarsAvailabilityStatus = new List<BOL.SectionCarsAvailability>();

            for (int i = 0; i < 8; i++)
            {
                this.SectionsCarsAvailabilityStatus.Add(new SectionCarsAvailability() { SectionName = (i + 1).ToString(), Availability = CarsAvailabilityTypes.Available });
            }
        }

        ObservableCollection<BOL.Trip> _trips;
        public ObservableCollection<BOL.Trip> Trips
        {
            get { return _trips; }
            set { _trips = value; }
        }

        List<SectionCarsAvailability> _sectionsBusyStatus;

        public List<SectionCarsAvailability> SectionsCarsAvailabilityStatus
        {
            get { return _sectionsBusyStatus; }
            set { _sectionsBusyStatus = value; }
        }

        BOL.Trip _addedEditedTrip;
        public BOL.Trip AddedEditedTrip
        {
            get { return _addedEditedTrip; }
            set
            {
                _addedEditedTrip = value;
            }
        }

        public BOL.DriverAndTripInformation SignInDriver(BOL.Driver driver, string signInLocation, CallToBroadcastDelegate callToBroadCast, bool isLongDistance)
        {
            if (this.SignedInDrivers.Count(sid => sid.DriverID == driver.DriverID) == 0)
            {
                BOL.DriverAndTripInformation dti = new BOL.DriverAndTripInformation()
                {
                    UnitDriver = driver,
                    DriverStatus = DriverStatusOptions.NintyEight,
                    CalledInLocation = signInLocation,
                    IsLongDistanceDriver = isLongDistance
                };
                this.DriversAndTripInformation.Add(dti);

                SaveDriverStatus(dti);

                if (callToBroadCast != null)
                    callToBroadCast.Invoke(new DispatcherEventArgs() { driverAndTripInfo = dti, msgType = MessageType.DriverAdded, TripAffected = null });

                //notify manger
                NotifyMangerOfBreakAndReturn(dti.UnitDriver, dti.DriverStatus, true);

                return dti;
            }
            else
                return null;
        }

        public void SaveDriverStatus(Driver driver)
        {
            Task.Factory.StartNew(() =>
            {
                DAL.DriversDB.EditDriver(driver, true);
            });
        }

        public void SaveDriverStatus(DriverAndTripInformation dti)
        {
            Task.Factory.StartNew(() =>
            {
                var driver = dti.UnitDriver;
                driver.DriverStatus = dti.DriverStatus;
                driver.CurrentLocation = dti.CalledInLocation;
                driver.AssignedToTripID = dti.AssignedToTrip == null ? 0 : dti.AssignedToTrip.TripID;
                driver.LastTripID = dti.LastTrip == null ? 0 : dti.LastTrip.TripID;
                driver.IsLongDistanceDriver = dti.IsLongDistanceDriver;
                driver.StatusChangeTime = dti.StatusChangedTime;
                DAL.DriversDB.EditDriver(driver, true);
            });
        }

        ObservableCollection<DriverAndTripInformation> _driversAndTripInformation;
        public ObservableCollection<DriverAndTripInformation> DriversAndTripInformation
        {
            get { return _driversAndTripInformation; }
            set { _driversAndTripInformation = value; }
        }

        public ObservableCollection<Driver> SignedInDrivers
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         select dt.UnitDriver));
            }
        }

        public ObservableCollection<Driver> DriversOnBreak
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         where dt.DriverStatus == DriverStatusOptions.LongBreak || dt.DriverStatus == DriverStatusOptions.ShortBreak
                                                         select dt.UnitDriver));
            }
        }

        public void AddTripToList(Trip tripToAdd, CallToBroadcastDelegate callToBroadcast, AssignedUnitDiscrepencyOptionDelegate callToAssignDriverOption,
            int byUserID, bool isSystemTrips = false)
        {
            //added to show ld trips
            try
            {
                if (tripToAdd.DrivenBy == null || this.AssignDriverToTrip(tripToAdd, tripToAdd.DrivenBy, callToBroadcast, false, callToAssignDriverOption, isSystemTrips))
                {
                    if (tripToAdd.IsLongDistanceTrip && tripToAdd.TripID > 0 && this.Trips.Count(t => t.TripID == tripToAdd.TripID) > 0)
                        return;

                    tripToAdd.TripNo = this.Trips.Count == 0 ? 1 : this.Trips.Max(t => t.TripNo) + 1;

                    this.Trips.Add(tripToAdd);

                    tripToAdd.TripStarted = DateTime.Now;

                    if (tripToAdd.TripID == 0)
                        DAL.TripDB.AddTrip(tripToAdd);

                    if (callToBroadcast != null)
                        callToBroadcast.Invoke(new DispatcherEventArgs() { TripAffected = tripToAdd, msgType = MessageType.TripAdded, byUserID = byUserID });

                    this.AddedEditedTrip = null;
                    this.ChangeTripStatus(tripToAdd,
                        tripToAdd.DrivenBy == null ? TripStatusTypes.ReceivedCall : TripStatusTypes.Assigned,
                        tripToAdd.DrivenBy, false, callToBroadcast);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException(new FaultReason(ex.Message), new FaultCode("Data Access Error"));
            }
        }

        public bool AssignDriverToTrip(Trip trip, Driver driver, CallToBroadcastDelegate callToBroadcast, bool warnDriverChanged = true,
            AssignedUnitDiscrepencyOptionDelegate callToGetUnitDiscOptions = null, bool isSystemTrips = false)
        {
            if (driver != null && trip.TripStatus != TripStatusTypes.Complete)
            {
                DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).FirstOrDefault();

                if (dti == null)
                {
                    dti = this.SignInDriver(driver, "", callToBroadcast, trip.IsLongDistanceTrip);
                }

                if (dti != null)
                {
                    if (dti.AssignedToTrip != null)
                    {
                        if (isSystemTrips)
                        {
                            //Combine
                            dti.AssignedSecondTrip = dti.AssignedToTrip;
                            this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                            return true;
                        }

                        AssignedUnitDiscrepencyOption optionSelected = callToGetUnitDiscOptions();
                        switch (optionSelected)
                        {
                            case AssignedUnitDiscrepencyOption.FinishedFirst:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Complete, null, false, callToBroadcast);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                                return true;
                            case AssignedUnitDiscrepencyOption.ReassignToNew:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.ReceivedCall, null, false, callToBroadcast);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                                return true;
                            case AssignedUnitDiscrepencyOption.CanceledFirst:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Canceled, null, false, callToBroadcast);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                                return true;
                            case AssignedUnitDiscrepencyOption.Combine:
                                dti.AssignedSecondTrip = dti.AssignedToTrip;
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                                return true;
                            case AssignedUnitDiscrepencyOption.Cancel:
                                return false;
                            default:
                                return false;
                        }
                    }
                    else
                    {
                        if (trip.DrivenBy != null)
                        {
                            this.ChangeTripStatus(trip, TripStatusTypes.ReceivedCall, null, true, callToBroadcast);
                        }

                        this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false, callToBroadcast);
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
                return false;
        }

        public void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced, CallToBroadcastDelegate callToBroadcast)
        {
            try
            {
                Trip serverTrip = this.Trips.Where(t => t.TripNo == trip.TripNo).FirstOrDefault();

                if (serverTrip == null)
                    serverTrip = trip;

                switch (status)
                {
                    case TripStatusTypes.ReceivedCall:
                        serverTrip.TripStatus = status;
                        serverTrip.TimeDriverAssigned = null;
                        Driver wasAssigned = serverTrip.DrivenBy;

                        if (wasAssigned != null)
                        {
                            DriverAndTripInformation dti1 = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == wasAssigned.DriverID).FirstOrDefault();

                            if (dti1 != null)
                            {
                                if (driverReplaced)
                                {
                                    DateTime? oldStatusChangedTime = dti1.BackupData.StatusChangedTime;
                                    dti1.DriverStatus = dti1.BackupData.DriverStatus;
                                    dti1.StatusChangedTime = oldStatusChangedTime;
                                }
                                else
                                    dti1.DriverStatus = DriverStatusOptions.NintyEight;

                                dti1.AssignedToTrip = null;

                                if (callToBroadcast != null)
                                    callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = dti1 });
                                
                                //Save Drivar Status Log
                                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog()
                                {
                                    //if changing driver then signout previous driver
                                    DriverID = driverReplaced ? wasAssigned.DriverID : dti1.UnitDriver.DriverID,
                                    DriverStatusType = dti1.DriverStatus,
                                    TimeLogged = DateTime.Now
                                });

                                SaveDriverStatus(dti1);
                            }
                        }

                        serverTrip.DrivenBy = null;

                        if (callToBroadcast != null)
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverTrip });

                        if (serverTrip.TripID > 0)
                            DAL.TripDB.EditTrip(serverTrip);
                        break;
                    case TripStatusTypes.Assigned:
                        if (serverTrip.DrivenBy != null)
                            this.ChangeTripStatus(serverTrip, TripStatusTypes.ReceivedCall, serverTrip.DrivenBy, true, callToBroadcast);

                        serverTrip.TripStatus = TripStatusTypes.Assigned;
                        serverTrip.IsNeedToSendTextMsg = true;
                        serverTrip.DrivenBy = driver;
                        serverTrip.TimeDriverAssigned = DateTime.Now;

                        DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).FirstOrDefault();
                        dti.LastTrip = dti.AssignedToTrip;
                        dti.AssignedToTrip = serverTrip;
                        dti.DriverStatus = DriverStatusOptions.Assigned;

                        if (callToBroadcast != null)
                        {
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverTrip });
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = dti });
                        }

                        //Save Drivar Status Log
                        DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = dti.UnitDriver.DriverID, DriverStatusType = dti.DriverStatus, TimeLogged = DateTime.Now });

                        if (serverTrip.TripID > 0)
                            DAL.TripDB.EditTrip(serverTrip);

                        SaveDriverStatus(dti);
                        break;
                    case TripStatusTypes.InProcess:
                        break;
                    case TripStatusTypes.Complete:
                        DriverAndTripInformation driverTI = this.DriversAndTripInformation.Where(d => 
                            (d.AssignedToTrip != null && d.AssignedToTrip.TripNo == serverTrip.TripNo) || 
                            (d.AssignedSecondTrip != null && d.AssignedSecondTrip.TripNo == serverTrip.TripNo)).
                            FirstOrDefault();

                        DateTime timeCompleted = DateTime.Now;

                        if (driverTI != null)
                        {
                            driverTI.DriverStatus = DriverStatusOptions.NintyEight;
                            driverTI.StatusChangedTime = timeCompleted;
                            driverTI.LastTrip = driverTI.AssignedToTrip;
                            driverTI.AssignedToTrip = null;

                            if (callToBroadcast != null)
                                callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = driverTI });
                            
                            //Save Drivar Status Log
                            DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = driverTI.UnitDriver.DriverID, DriverStatusType = driverTI.DriverStatus, TimeLogged = DateTime.Now });

                            SaveDriverStatus(driverTI);
                        }

                        serverTrip.TripStatus = TripStatusTypes.Complete;
                        serverTrip.DetailedStatus = BOL_OTR.TripDetailedStatusOptions.TripCompleted;
                        serverTrip.TripEnded = timeCompleted;

                        if (callToBroadcast != null)
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverTrip });

                        if (serverTrip.TripID > 0)
                            DAL.TripDB.EditTrip(serverTrip);
                        break;

                    case TripStatusTypes.Canceled:
                        int i = this.DriversAndTripInformation.Count(d => d.AssignedToTrip != null && d.AssignedToTrip.TripNo == serverTrip.TripNo);

                        if (i > 0)
                        {
                            DriverAndTripInformation driverTI2 = this.DriversAndTripInformation.
                                Where(d => d.AssignedToTrip != null && d.AssignedToTrip.TripNo == serverTrip.TripNo).First();

                            DateTime? oldStatusChangedTime = driverTI2.BackupData.StatusChangedTime;
                            driverTI2.DriverStatus = DriverStatusOptions.NintyEight;
                            driverTI2.StatusChangedTime = oldStatusChangedTime;
                            driverTI2.AssignedToTrip = null;

                            if (callToBroadcast != null)
                                callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = driverTI2 });
                            
                            //Save Drivar Status Log
                            DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = driverTI2.UnitDriver.DriverID, DriverStatusType = driverTI2.DriverStatus, TimeLogged = DateTime.Now });

                            SaveDriverStatus(driverTI2);
                        }

                        serverTrip.TripStatus = TripStatusTypes.Canceled;
                        serverTrip.DrivenBy = null;

                        if (callToBroadcast != null)
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverTrip });

                        if (serverTrip.TripID > 0)
                            DAL.TripDB.EditTrip(serverTrip);
                        break;

                    case TripStatusTypes.None:
                        if (serverTrip.TripID > 0)
                            DAL.TripDB.EditTrip(serverTrip);

                        if (callToBroadcast != null)
                            callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverTrip });

                        break;
                    default:
                        break;
                }

                if (serverTrip.TripID == 0)
                {
                    if (trip.TripID == 0)
                        DAL.TripDB.AddTrip(trip);
                    else
                        DAL.TripDB.EditTrip(trip);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException(new FaultReason(ex.Message), new FaultCode("Data Access Error"));
            }
        }

        public void MarkDriverForBreak(Driver driver, DriverStatusOptions breakType, CallToBroadcastDelegate callToBroadcast)
        {
            Trip completeFirstTrip = (from d in this.DriversAndTripInformation
                                      where d.UnitDriver.DriverID == driver.DriverID
                                      select d.AssignedToTrip).ToList()[0];
            if (completeFirstTrip != null)
                this.ChangeTripStatus(completeFirstTrip, TripStatusTypes.Complete, driver, false, callToBroadcast);

            this.ChangeDriversStatus(driver, breakType, callToBroadcast);
        }

        public void Undo98Status(Driver driver, CallToBroadcastDelegate callToBroadcast)
        {
            var serverDTI = this.DriversAndTripInformation.Where(d => driver.DriverID == d.UnitDriver.DriverID).FirstOrDefault();

            if (serverDTI != null)
            {
                serverDTI.Undo98Status();

                callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = serverDTI });

                if (serverDTI.AssignedToTrip != null)
                {
                    callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.TripEdited, TripAffected = serverDTI.AssignedToTrip });

                    if (serverDTI.AssignedToTrip.TripID > 0)
                        DAL.TripDB.EditTrip(serverDTI.AssignedToTrip);
                }

                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = serverDTI.UnitDriver.DriverID, DriverStatusType = serverDTI.DriverStatus, TimeLogged = DateTime.Now });

                SaveDriverStatus(serverDTI);
            }
        }

        public void SendRequestToDispatcher(int driverID, BOL_OTR.DriversRequestOptions request, CallToBroadcastDelegate callToBroadcast)
        {
            var tripInfo = DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driverID).FirstOrDefault();
            if (tripInfo != null)
                tripInfo.DriverRequest = request;

            if (callToBroadcast != null)
                callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = tripInfo });
        }

        public DriverAndTripInformation ChangeDriversStatus(Driver driver, DriverStatusOptions assignStatus, CallToBroadcastDelegate callToBroadcast)
        {
            DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).FirstOrDefault();

            if (dti != null)
            {
                dti.DriverStatus = assignStatus;

                if (callToBroadcast != null)
                    callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverEdited, driverAndTripInfo = dti });
                
                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = dti.UnitDriver.DriverID, DriverStatusType = dti.DriverStatus, TimeLogged = DateTime.Now });

                SaveDriverStatus(dti);

                NotifyMangerOfBreakAndReturn(driver, assignStatus);

                return dti;
            }
            else
                return null;
        }

        void LogError(Exception ex)
        {
            DispatchService.LogEvent(ex.Message, System.Diagnostics.EventLogEntryType.Error, 101);
        }

        internal void NotifyMangerOfBreakAndReturn(Driver driver, DriverStatusOptions driverStatus, bool firstTimeSignIn = false)
        {
            return;
            Task.Factory.StartNew(() => 
            {
                try
                {
                    if (driverStatus != DriverStatusOptions.LongBreak && driverStatus != DriverStatusOptions.ShortBreak && driverStatus != DriverStatusOptions.NintyEight)
                    {
                        return;
                    }

                    //get emailAddressTo
                    //var emailToSetting = DAL.FacilitySettingsDB.GetFacilitySettings("DriverStatusChangedNotificationEmail");
                    var emailToSetting = DAL.FacilitySettingsDB.GetFacilitySettings("DriverStatusChangedNotificationEmail");
                    if (emailToSetting == null) return;

                    if (firstTimeSignIn) //just send notification of sign in no need to other calculations
                    {
                        var msgToSend = string.Format("{0}: On {1} Now ,", driver.DriverCode, driverStatus.ToDescription());//first time today
                                                                                                                            //var ds = new DispatcherService.DispatchService();                    
                                                                                                                            //ds.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        DispatchService.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        return;
                    }

                    if (driverStatus == DriverStatusOptions.LongBreak || driverStatus == DriverStatusOptions.ShortBreak)
                    {
                        //if driver is now on brak get his logs for today and send alert to pre set email address of count of breaks today anf d
                        //get count of breaks today 
                        //when driver takes short or long brake, write in message is its long or short
                        //send email 
                        var countsOfBreakesToday = "";
                        var todaysLogs = DAL.DriverStatusLogDB.GetDriverStatusLogs(DateTime.Today, DateTime.Now, driver.DriverID);
                        if (todaysLogs != null && todaysLogs.Count() > 0)
                        {
                            countsOfBreakesToday = todaysLogs.
                                Where(t => t.DriverStatusType == DriverStatusOptions.ShortBreak ||
                                t.DriverStatusType == DriverStatusOptions.LongBreak).ToList().Count().ToString();
                        }

                        var msgToSend = string.Format("{0}: On {1} Now, Break# {2},", driver.DriverCode, driverStatus.ToDescription(), countsOfBreakesToday);
                        // var ds = new DispatcherService.DispatchService();
                        // ds.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        DispatchService.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        return;
                    }
                    else if (driverStatus == DriverStatusOptions.NintyEight)
                    {
                        //check if this is the first time of NintyEight today //defiend as first time avilbale today 
                        //when driver is available first time after brake, //(not after each trip)               

                        var todaysLogs = DAL.DriverStatusLogDB.GetDriverStatusLogs(DateTime.Today, DateTime.Now, driver.DriverID);
                        //not get the time spna between one before last and last
                        var lastStatusLog = todaysLogs.LastOrDefault();
                        //now remove the last
                        todaysLogs.Remove(lastStatusLog);
                        //now get the last again whihc is the one before last
                        var oneBeforeLast = todaysLogs.LastOrDefault();

                        //error somewhere that it sends logs even if this is not after a break so make sure that tha last log was a short break or long break
                        if (oneBeforeLast.DriverStatusType != DriverStatusOptions.ShortBreak || driverStatus == DriverStatusOptions.LongBreak) { return; }
                        TimeSpan spanDuration = lastStatusLog.TimeLogged.Subtract(oneBeforeLast.TimeLogged);

                        var msgToSend = string.Format("{0} : finished break, duration: {1}:{2}:{3}", driver.DriverCode, spanDuration.Hours, spanDuration.Minutes, spanDuration.Seconds);
                        //send email 
                        //var ds = new DispatcherService.DispatchService();
                        //ds.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        DispatchService.SendEmailMessege("Status Change", msgToSend, emailToSetting.SettingValue);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }
            });
        }

        internal DriverAndTripInformation SignOutDriver(Driver driver, CallToBroadcastDelegate callToBroadcast)
        {
            DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).FirstOrDefault();

            if (dti != null)
            {
                if (dti.AssignedToTrip != null)
                    this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Complete, driver, false, callToBroadcast);

                if (dti.AssignedSecondTrip != null)
                {
                    this.ChangeTripStatus(dti.AssignedSecondTrip, TripStatusTypes.Complete, driver, false, callToBroadcast);
                    dti.AssignedSecondTrip = null;
                }

                this.DriversAndTripInformation.Remove(dti);

                if (callToBroadcast != null)
                    callToBroadcast.Invoke(new DispatcherEventArgs() { msgType = MessageType.DriverRemoved, driverAndTripInfo = dti });
                
                //Save Drivar Status Log
                DAL.DriverStatusLogDB.AddDriverStatusLog(new DriverStatusLog() { DriverID = dti.UnitDriver.DriverID, DriverStatusType = DriverStatusOptions.SignedOut, TimeLogged = DateTime.Now });

                driver.DriverStatus = DriverStatusOptions.SignedOut;
                driver.IsLongDistanceDriver = false;
                SaveDriverStatus(driver);

                return dti;
            }
            else
                return null;
        }
    }
}
