﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Data;


namespace DAL
{
    public class TicketBookStatusDB : Database
    {
        public TicketBookStatusDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(ObservableCollection<TicketBookStatus> TicketBookStatuses)
        {

            if (this.Transaction == null)
            {
                this._localTransaction = true;
                this.Transaction = this.Connection.BeginTransaction();
            }
            int noOfRowsInsetrted = 0;
            int totalNoOfRowsAffected = 0;
            try
            {
                foreach (TicketBookStatus ticketBookStatus in TicketBookStatuses)
                {
                    string serverMessage;
                    SqlParameter outParam = new SqlParameter("@TicketBookStatusID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                    noOfRowsInsetrted = this.ExecuteNonQuery("AddTicketBooksStatuses", new SqlParameter[]
                    {
                        new SqlParameter("@BookNumber",ticketBookStatus.BookNumber),
                        new SqlParameter("@TicketBookID", ticketBookStatus.TicketBookID),
                        new SqlParameter("@BookStatus",ticketBookStatus.BookStatus),
                        new SqlParameter("@LastSessionID",Database.SessionID),
                        new SqlParameter("@InPossessionID",ticketBookStatus.InPossessionID),
                        new SqlParameter("@TicketBookStatusIDIn",ticketBookStatus.TicketBookStatusID),
                        new SqlParameter("@CustomerIdSold",ticketBookStatus.CustomerIdSold),
                        new SqlParameter("@SoldToInfo",ticketBookStatus.SoldToInfo),
                        new SqlParameter("@PriceSold",ticketBookStatus.PriceSold),
                        new SqlParameter("@DiscountType",ticketBookStatus.TBDiscountType),
                        new SqlParameter("@TicketNote",ticketBookStatus.TicketNote),
                        new SqlParameter("@PaymentID",ticketBookStatus.PaymentID),
                        outParam
                    }, System.Data.CommandType.StoredProcedure, out serverMessage);

                    if (noOfRowsInsetrted > 0)
                        ticketBookStatus.TicketBookStatusID = Convert.ToInt32(outParam.Value);
                    totalNoOfRowsAffected += noOfRowsInsetrted;
                }
                //if (this.Transaction.Connection == null)
                //    this.Transaction = this.Connection.BeginTransaction();
                if (this._localTransaction)
                    this.Transaction.Commit();

                return totalNoOfRowsAffected > 0;
            }
            catch (Exception)
            {
                if (this._localTransaction)
                    this.Transaction.Rollback();
                throw;
            }

            finally
            {
                if (this._localTransaction)
                    this.Transaction.Dispose();
            }

        }
        public static bool AddTicketBookStatuses(ObservableCollection<TicketBookStatus> ticketBookStatuses)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.Add(ticketBookStatuses);
            }
        }
        public bool Edit(TicketBookStatus ticketBookStatus, bool forUpdateCustID, bool newBarCode = false, bool forUpdateSoldToInfo = false, bool forAutoAttach = false)
        {
            string serverMessage;
            SqlParameter outParam1 = new SqlParameter("@GivenToOut", System.Data.SqlDbType.NVarChar, 50) { Direction = ParameterDirection.Output, Value = "" };
            SqlParameter outParam2 = new SqlParameter("@CustomerIdSoldOut", System.Data.SqlDbType.Int) { Direction = ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateTicketBooksStatuses", new SqlParameter[]
            {
                new SqlParameter("@BookNumber",ticketBookStatus.BookNumber),
                new SqlParameter("@TicketBookID", ticketBookStatus.TicketBookID),
                new SqlParameter("@BookStatus",ticketBookStatus.BookStatus),
                new SqlParameter("@LastSessionID",Database.SessionID),
                new SqlParameter("@InPossessionID",ticketBookStatus.InPossessionID),
                new SqlParameter("@TicketBookStatusID",ticketBookStatus.TicketBookStatusID),
                new SqlParameter("@DailyEnvelopeIdSold",ticketBookStatus.DailyEnvelopeIdSold),
                new SqlParameter("@CustomerIdSold",ticketBookStatus.CustomerIdSold),
                new SqlParameter("@NewBarCode",newBarCode),
                new SqlParameter("@SoldToInfo",ticketBookStatus.SoldToInfo),
                new SqlParameter("@TicketNote",ticketBookStatus.TicketNote),
                new SqlParameter("@DiscountType",ticketBookStatus.TBDiscountType),
                new SqlParameter("@PriceSold",ticketBookStatus.PriceSold),
                new SqlParameter("@ForUpdateCustID",forUpdateCustID),
                new SqlParameter("@ForUpdateSoldToInfo",forUpdateSoldToInfo),
                new SqlParameter("@GivenTo",ticketBookStatus.GivenTo),
                new SqlParameter("@AutoAttach",forAutoAttach),
                outParam1,
                outParam2
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
            {
                if (outParam1.Value != DBNull.Value)
                {
                    ticketBookStatus.GivenTo = Convert.ToString(outParam1.Value);
                }
                if (outParam2.Value != DBNull.Value)
                {
                    ticketBookStatus.CustomerIdSold = Convert.ToInt32(outParam2.Value);
                }

            }
            return noOfRecordsAffected > 0;
        }
        public static bool EditTicketBookStatus(TicketBookStatus ticketBookStatus, bool forUpdateCustID = false, bool forUpdateSoldToInfo = false, bool newBarCode = false, bool forAutoAttach = false)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.Edit(ticketBookStatus, forUpdateCustID, forUpdateSoldToInfo: forUpdateSoldToInfo, forAutoAttach: forAutoAttach);
            }
        }
        public bool EditCollection(ObservableCollection<TicketBookStatus> ticketBookStatuses, bool newBarCode)
        {
            if (this.Transaction == null)
            {
                this._localTransaction = true;
                this.Transaction = this.Connection.BeginTransaction();
            }
            int noOfRowsInsetrted = 0;
            int totalNoOfRowsAffected = 0;
            try
            {
                foreach (TicketBookStatus ticketBookStatus in ticketBookStatuses)
                {
                    string serverMessage;
                    int noOfRecordsAffected = this.ExecuteNonQuery("UpdateTicketBooksStatuses", new SqlParameter[]
                    {
                        new SqlParameter("@BookNumber",ticketBookStatus.BookNumber),
                        new SqlParameter("@TicketBookID", ticketBookStatus.TicketBookID),
                        new SqlParameter("@BookStatus",ticketBookStatus.BookStatus),
                        new SqlParameter("@LastSessionID",Database.SessionID),
                        new SqlParameter("@InPossessionID",ticketBookStatus.InPossessionID),
                        new SqlParameter("@TicketBookStatusID",ticketBookStatus.TicketBookStatusID),
                        new SqlParameter("@DailyEnvelopeIdSold",ticketBookStatus.DailyEnvelopeIdSold),
                        new SqlParameter("@CustomerIdSold",ticketBookStatus.CustomerIdSold),
                        new SqlParameter("@NewBarCode",newBarCode),
                        new SqlParameter("@SoldToInfo",ticketBookStatus.SoldToInfo),
                        new SqlParameter("@IsPaidByCC",ticketBookStatus.IsPaidByCC),
                        new SqlParameter("@DiscountType",ticketBookStatus.TBDiscountType)
                    }, System.Data.CommandType.StoredProcedure, out serverMessage);
                    if (noOfRowsInsetrted > 0)
                        totalNoOfRowsAffected += noOfRowsInsetrted;
                }

                if (this._localTransaction)
                    this.Transaction.Commit();

                return totalNoOfRowsAffected > 0;
            }
            catch (Exception)
            {
                if (this._localTransaction)
                    this.Transaction.Rollback();
                throw;
            }

            finally
            {
                if (this._localTransaction)
                    this.Transaction.Dispose();
            }
        }
        /// <summary>
        /// Used to edit a ObservableCollection of TicketBookStatus
        /// </summary>
        /// <param name="ticketBookStatuses"></param>
        /// <returns></returns>
        public static bool EditTicketBookStatuses(ObservableCollection<TicketBookStatus> ticketBookStatuses, bool newBarCodes = false)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.EditCollection(ticketBookStatuses, newBarCodes);
            }
        }
        public ObservableCollection<TicketBookStatus> Get(int driverID, int ticketBooksStatusID, int bookNumber, int ticketBookID, int dailyEnvelopeID, int customerID, bool GetCount)//(DateTime fromDate, DateTime tillDate)
        {
            var ticketBookStatuses = new ObservableCollection<TicketBookStatus>();
            using (SqlDataReader dr = this.GetReader("GetTicketBooksStatuses", new SqlParameter[]
            {
                new SqlParameter("@DriverID",driverID),
                new SqlParameter("@TicketBooksStatusID",ticketBooksStatusID),
                new SqlParameter("@BookNumber",bookNumber),
                new SqlParameter("@TicketBookID",ticketBookID),
                new SqlParameter("@DailyEnvelopeIdSold",dailyEnvelopeID),
                new SqlParameter("@CustomerIdSold",customerID),
                new SqlParameter("@GetCount",GetCount)
               // new SqlParameter("@FromDate",fromDate),
                 // new SqlParameter("@FromDate",tillDate),
                
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var ticketBookStatus = new TicketBookStatus()
                    {
                        TicketBookType = new TicketBook()
                        {
                            TicketBookID = Convert.ToInt32(dr["TicketBookID"]),
                            BookName = dr["BookName"].ToString(),
                        },
                        BookNumber = Convert.ToInt32(dr["BookNumber"]),
                        BookStatus = (TicketBookStatusOptions)Convert.ToInt32(dr["BookStatus"]),
                        UserLogged = dr["UserLogged"].ToString(),
                        DateLogged = Convert.ToDateTime(dr["DateLogged"]),
                        GivenTo = dr["GivenTo"].ToString(),
                        TicketBookStatusID = Convert.ToInt32(dr["TicketBookStatusID"]),
                        InPossessionID = Convert.ToInt32(dr["InPossessionID"]),
                        DailyEnvelopeIdSold = Convert.ToInt32(dr["DailyEnvelopeIdSold"]),
                        CustomerIdSold = Convert.ToInt32(dr["CustomerIdSold"]),
                        SoldToInfo = dr["SoldToInfo"].ToString(),
                        TicketNote = dr["TicketNote"].ToString(),
                        TBDiscountType = (BookDiscountTypes)Convert.ToInt32(dr["DiscountType"]),
                        PriceSold = Convert.ToDecimal(dr["PriceSold"]),
                        IsPaidByCC = Convert.ToBoolean(dr["IsPaidByCC"])


                    };
                    ticketBookStatuses.Add(ticketBookStatus);
                }
            }
            return ticketBookStatuses;
        }
        public static ObservableCollection<TicketBookStatus> GetTicketBookStatuses(int driverID = 0, int ticketBooksStatusID = 0, int bookNumber = 0, int ticketBookID = 0, int dailyEnvelopeID = 0, int customerID = 0, bool GetCount = false)//(DateTime fromDate, DateTime tillDate)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.Get(driverID, ticketBooksStatusID, bookNumber, ticketBookID, dailyEnvelopeID, customerID, GetCount);//(fromDate, tillDate);
            }
        }
        public ObservableCollection<TicketBookStatus> GetDetail(int driverID)//(DateTime fromDate, DateTime tillDate)
        {
            var ticketBookStatuses = new ObservableCollection<TicketBookStatus>();
            using (SqlDataReader dr = this.GetReader("GetTicketBooksStatusesDetail", new SqlParameter[]
            {
                new SqlParameter("@DriverID",driverID)
               // new SqlParameter("@FromDate",fromDate),
                 // new SqlParameter("@FromDate",tillDate),
                
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var ticketBookStatus = new TicketBookStatus()
                    {
                        TicketBookType = new TicketBook()
                        {
                            TicketBookID = Convert.ToInt32(dr["TicketBookID"]),
                            BookName = dr["BookName"].ToString(),
                        },
                        BookNumber = Convert.ToInt32(dr["BookNumber"]),
                        BookStatus = (TicketBookStatusOptions)Convert.ToInt32(dr["BookStatus"]),
                        UserLogged = dr["UserLogged"].ToString(),
                        DateLogged = Convert.ToDateTime(dr["DateLogged"]),
                        GivenTo = dr["GivenTo"].ToString()


                    };
                    ticketBookStatuses.Add(ticketBookStatus);
                }
            }
            return ticketBookStatuses;
        }
        public static ObservableCollection<TicketBookStatus> GetTicketBookStatusesDetail(int driverID = 0)//(DateTime fromDate, DateTime tillDate)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.GetDetail(driverID);//(fromDate, tillDate);
            }
        }
        public ObservableCollection<Posession> getPossessions()
        {
            ObservableCollection<Posession> posessions = new ObservableCollection<Posession>();
            using (SqlDataReader dr = this.GetReader("GetPosessions", null, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    posessions.Add(new Posession()
                    {
                        InPossessionID = Convert.ToInt32(dr["InPosessionID"]),
                        InPosessionName = dr["InPosessionName"].ToString(),

                        TicketBookStatusType = (TicketBookStatusOptions)Convert.ToInt32(dr["TicketBookStatusType"])

                    });
                }
            }
            return posessions;
        }
        public static ObservableCollection<Posession> GetPossessions()
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.getPossessions();
            }
        }
        public int getMaxBookNumber(int bookID, int forUserID)
        {
            int iii;
            using (var db = new Database(null, null))
            {
                SqlParameter outParam = new SqlParameter("@BookNumber", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                db.ExecuteScalar("GetMaxBookNumber", new SqlParameter[]
                {
                   new SqlParameter("@BookID",bookID),
                   new SqlParameter("@ForUserID",forUserID),
                   outParam
                },
                    System.Data.CommandType.StoredProcedure);
                iii = Convert.ToInt32(outParam.Value);
                return iii;
            }



        }
        public static int GetMaxBookNumber(int bookID, int forUserID)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.getMaxBookNumber(bookID, forUserID);
            }
        }

        public int getTotalOfTicektBooksOnHand(int forDriverID)
        {
            int noOfBooksObHand;

            using (var db = new Database(null, null))
            {
                SqlParameter outParam = new SqlParameter("@CountOfBooksOnHand", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
                db.ExecuteScalar("GetCountOfTicektBooksOnHand", new SqlParameter[]
                {
                    new SqlParameter("@ForDriverID", forDriverID),
                    outParam
                },
                    System.Data.CommandType.StoredProcedure);
                noOfBooksObHand = Convert.ToInt32(outParam.Value);
                return noOfBooksObHand;
            }
        }
        public static int GetTotalOfTicektBooksOnHand(int forDriverID)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.getTotalOfTicektBooksOnHand(forDriverID);
            }
        }

        public bool checkIfCouldGiveAwayTickets(int startingBookNumber, int endingBookNumber, int forBookType, int byUserID)
        {
            bool couldGiveAway = false;

            using (var db = new Database(null, null))
            {
                var uu = db.ExecuteScalar("SELECT dbo.CheckIfCouldGiveAwayTickets(" + startingBookNumber + "," + endingBookNumber + "," + forBookType + "," + byUserID + ")", new SqlParameter[] { }, System.Data.CommandType.Text);
                if (uu != DBNull.Value)
                    couldGiveAway = Convert.ToBoolean(uu);
                return couldGiveAway;

            }
        }
        public static bool CheckIfCouldGiveAwayTickets(int startingBookNumber, int endingBookNumber, int forBookType, int byUserID)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.checkIfCouldGiveAwayTickets(startingBookNumber, endingBookNumber, forBookType, byUserID);
            }
        }

        public bool checkIfTicketsInStock(int startingBookNumber, int endingBookNumber, int forBookType)
        {
            bool isInStock = false;

            using (var db = new Database(null, null))
            {
                var uu = db.ExecuteScalar("SELECT dbo.CheckIfTicketsInStock(" + startingBookNumber + "," + endingBookNumber + "," + forBookType + ")", new SqlParameter[] { }, System.Data.CommandType.Text);
                if (uu != DBNull.Value)
                    isInStock = Convert.ToBoolean(uu);
                return isInStock;

            }
        }
        public static bool CheckIfTicketsInStock(int startingBookNumber, int endingBookNumber, int forBookType)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.checkIfTicketsInStock(startingBookNumber, endingBookNumber, forBookType);
            }
        }

        public bool checkIfIsPaidMember(string homePhone, string cellPhone)
        {
            if (homePhone == "")
            {
                homePhone = "''";
            }
            if (cellPhone == "")
            {
                cellPhone = "''";
            }

            bool isPaidMember = false;

            using (var db = new Database(null, null))
            {
                var m = db.ExecuteScalar("SELECT dbo.IsPaidMember(" + homePhone + "," + cellPhone + ")", new SqlParameter[] { }, System.Data.CommandType.Text);
                if (m != DBNull.Value)
                    isPaidMember = Convert.ToBoolean(m);
                return isPaidMember;

            }
        }
        public static bool CheckIfIsPaidMember(string homePhone, string cellPhone)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.checkIfIsPaidMember(homePhone, cellPhone);
            }
        }

        public int getCountOfCCBookPayments(int driverID, DateTime dateOfTrip)
        {
            int countCCPmt = 0;

            using (var db = new Database(null, null))
            {
                var c = db.ExecuteScalar("SELECT dbo.GetCountOfCCBookPayments(" + driverID + ",'" + dateOfTrip + "','" + dateOfTrip + "'," + 0 + "," + 0 + "," + 0 + ")", new SqlParameter[] { }, System.Data.CommandType.Text);
                if (c != DBNull.Value)
                    countCCPmt = Convert.ToInt32(c);
                return countCCPmt;
            }
        }
        public static int GetCountOfCCBookPayments(int driverID, DateTime dateOfTrip)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.getCountOfCCBookPayments(driverID, dateOfTrip);
            }
        }

        public bool isBookSold(int bookNumber, int ticketBookType)
        {
            using (var db = new Database(null, null))
            {
                bool isSold = false;
                var c = db.ExecuteScalar("SELECT dbo.IsTicketSold(" + bookNumber + "," + ticketBookType.ToString() + ")", new SqlParameter[] { }, System.Data.CommandType.Text);
                if (c != DBNull.Value)
                    isSold = Convert.ToBoolean(c);
                return isSold;
            }
        }
        public static bool IsBookSold(int bookNumber, int ticketBookType)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.isBookSold(bookNumber, ticketBookType);
            }
        }

        public ObservableCollection<TicketBookSummery> getBooksSummery(bool onlyLost)
        {
            string sqlStatment = "";
            if (onlyLost)
            {
                sqlStatment = "SELECT * FROM dbo.LostTickets";
            }
            else
            {
                sqlStatment = "SELECT * FROM dbo.TicketBookSummery";
            }

            ObservableCollection<TicketBookSummery> tbSummery = new ObservableCollection<TicketBookSummery>();
            using (SqlDataReader dr = this.GetReader(sqlStatment, null, System.Data.CommandType.Text))
            {
                while (dr.Read())
                {
                    tbSummery.Add(new TicketBookSummery()
                    {
                        TicketBookID = new TicketBook() { TicketBookID = Convert.ToInt32(dr["TicketBookID"]), BookName = dr["BookName"].ToString() },
                        BookNumber = Convert.ToInt32(dr["BookNumber"]),
                        CustomerIdSold = Convert.ToInt32(dr["CustomerIdSold"]),
                        SoldToInfo = dr["SoldToInfo"].ToString(),
                        SoldDate = Convert.ToDateTime(dr["SoldDate"]),
                        CountUsed = Convert.ToInt32(dr["CountUsed"]),
                        DateFirstTicket = Convert.ToDateTime(dr["FirstTicket"]),
                        DateLastTicket = Convert.ToDateTime(dr["LastTicket"]),
                        DaysDiff = Convert.ToInt32(dr["Diff"]),
                        CustomerName = dr["Name"].ToString()
                    });
                }
            }
            return tbSummery;
        }

        public static ObservableCollection<TicketBookSummery> GetBooksSummery(bool onlyLost = false)
        {
            using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(null, null))
            {
                return ticketBookStatusDB.getBooksSummery(onlyLost);
            }
        }

    }
}
