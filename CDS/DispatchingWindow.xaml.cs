﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Data;
using BOL;
using Extensions;
using System.Windows.Controls.Primitives;
using System.Timers;
using System.Windows.Threading;
using System.ServiceModel;
using System.Media;
using Microsoft.AspNet.SignalR.Client;



namespace CDS
{
    /// <summary>
    /// Interaction logic for DispatchingPage.xaml
    /// </summary>
    public partial class DispatchingWindow : Window
    {
        bool _dispatchRemotely = false;
        bool _isLongDistance = false;
        MapWindow mapWindow = null;
        List<CallerID.PhoneInfo> CallerIDList = new List<CallerID.PhoneInfo>();

        public DispatchingWindow(bool dispatchRemotely, bool isLongDistance)
        {
            InitializeComponent();

            _dispatchRemotely = dispatchRemotely;
            _isLongDistance = isLongDistance;

            System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    var callerIDListener = new CallerID.Listener();

                    callerIDListener.StartListener((phoneLine, phoneNo, name) =>
                    {

                        App.Current.Dispatcher.BeginInvoke(new Action(() =>
                            {
                                CallerID.PhoneInfo line = null;
                                if (this.CallerIDList.Exists(c => c.PhoneLine == phoneLine))
                                {
                                    line = this.CallerIDList.Where(l => l.PhoneLine == phoneLine).FirstOrDefault();
                                    line.CallerID = phoneNo;
                                    line.CallerName = name;
                                }
                                else
                                    this.CallerIDList.Add(new CallerID.PhoneInfo() { PhoneLine = phoneLine, CallerID = phoneNo, CallerName = name });
                            }));
                    });
                });

            if (_isLongDistance)
            {
                Trip.IsLongDistance = true;
                this.Title += " - LONG DISTANCE";
                this.toLocation_column.FontSize = 10;
                this.toCity_column.Visibility = Visibility.Visible;
            }
            else
                this.Title += " - LOCAL";

            this.WindowState = System.Windows.WindowState.Maximized;
           
            if (_dispatchRemotely)
            {
                this.Dispatching = new DispatchingToProxy(this.SelectTrip, isLongDistance);

                if (!_isLongDistance)
                {
                    mapWindow = new MapWindow(Dispatching as DispatchingToProxy);
                    mapWindow.Show();
                }
            }
            else
                this.Dispatching = new DispatchingMVVM(this.SelectTrip, isLongDistance);

            this.Dispatching.IsLongDistanceMode = _isLongDistance;
            this.Dispatching.StreetAbbreviations = DAL.StreetAbbreviationDB.GetStreetAbbreviations();

            (this.Resources["StreetAbbreviationsListData"] as StreetAbbreviationsList).Clear();

            foreach (StreetAbbreviation sa in this.Dispatching.StreetAbbreviations)
            {
                (this.Resources["StreetAbbreviationsListData"] as StreetAbbreviationsList).Add(sa);
            }

            this.AddEditTripBorder.Focus();
            this.ProcessAddEditTrip(null, false);

            this.RefreshDriverStatusTimeElapsed.Tick += new EventHandler(RefreshDriverStatusTimeElapsed_Tick);
            this.RefreshDriverStatusTimeElapsed.Start();

            #region subscribe to signalR hub

            if (Properties.Settings.Default.UseSignalR)
            {
                SignalRHubDispatchingClient.DispatchingRef = this.Dispatching;
                SignalRHubDispatchingClient.Subscribe(Properties.Settings.Default.SignalRServiceIPAddressAndPort);
            }  
                     
            #endregion

        }
        

        private bool IsSystemKey(Key key)
        {
            //KEYS ALLOWED REGARDLESS OF SHIFT KEY

            //various editing keys
            return key == Key.Back ||
            key == Key.Tab ||
            key == Key.Up ||
            key == Key.Down ||
            key == Key.Left ||
            key == Key.Right ||
            key == Key.Delete ||
            key == Key.Home ||
            key == Key.LeftShift ||
             key == Key.End;
        }

        void RefreshDriverStatusTimeElapsed_Tick(object sender, EventArgs e)
        {
            foreach (DriverAndTripInformation dti in this.Dispatching.DriversAndTripInformation)
            {
                dti.OnPropertyChanged("StatusChangedTimeElapsed");
            }

            foreach (Trip trip in this.Dispatching.Trips.Where(t => t.ETA != null || t.ScheduledFor != null))
            {
                trip.OnPropertyChanged("IsTwoMinutesToTrip");
                trip.OnPropertyChanged("IsAssignedNotInTime");
            }
        }

        void RefershTime()
        {
            this.Dispatching.RefereshStatusTimesOnDrivers();
         }

        public IDispatchingMVVM Dispatching
        {
            get
            {
                return this.DataContext as IDispatchingMVVM;
            }
            set { this.DataContext = value; }
        }

        DispatcherTimer RefreshDriverStatusTimeElapsed = new DispatcherTimer() { Interval = new TimeSpan(0, 0, 1) };

        void ProcessAddEditTrip(Trip trip, bool addActiveTripToList)
        {
            try
            {
                if (addActiveTripToList && this.Dispatching.AddedEditedTrip.AllowSave)
                {
                    this.Dispatching.AddedEditedTrip.AddedBySessionID = UIController.CurrentSession.SessionID;
                    this.Dispatching.AddedEditedTrip.TakenByDispatcherID = UIController.CurrentSession.SessionUser.UserID;
                    this.Dispatching.AddedEditedTrip.DispatcherCode = UIController.CurrentSession.SessionUser.UserCode;
                    Trip tripToAdd = this.Dispatching.AddedEditedTrip;

                    if (_isLongDistance)
                    {
                        var scheduldTrip = new ScheduledTrip()
                        {
                            PickupDate = DateTime.Today,
                            PickupTime = tripToAdd.ScheduledFor,
                            PickupAddress = tripToAdd.FromLocation,
                            DropOffAddress = tripToAdd.ToLocation,
                            SFromLocationComment = tripToAdd.FromLocationComment,
                            SToLocationComment = tripToAdd.ToLocationComment,
                            ETAToLocation1 = tripToAdd.ETAToLocation1,
                            ETABackToBase = tripToAdd.ETABackToBase,
                            TripPrice = tripToAdd.TripPrice == null ? 0 : (decimal)tripToAdd.TripPrice,
                            TripAreaType = TripAreaTypes.LongDistance,
                            SVehicleTypeRequested = tripToAdd.VehicleTypeRequested,
                            AssignedToDriverID = tripToAdd.DrivenBy != null ? tripToAdd.DrivenBy.DriverID : 0,
                            CustomerPhoneNo = tripToAdd.CallBackPhoneNo,
                            ScheduledByCustomer = tripToAdd.ChargeToCustomer,
                            SCustomerPassCodeID = tripToAdd.CustomerPassCodeID,
                            SNoOfStops = tripToAdd.NoOfStops,
                        };

                        DAL.ScheduledTripDB.AddScheduledTrip(scheduldTrip);
                        this.Dispatching.ProcessScheduledTrip();
                        this.lastFocusedInNewTripElement = null;
                        this.Dispatching.AddEditTrip(null);
                        this.txtLine.Focus();
                        //this.FromHouseNoTextBox.Focus();
                       
                        return;

                    }

                    this.Dispatching.AddTripToList(this.Dispatching.AddedEditedTrip);
                    this.lastFocusedInNewTripElement = null;
                }

                this.Dispatching.AddEditTrip(null);
                this.txtLine.Focus();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }            
        }

        IInputElement lastFocusedInNewTripElement;
        
        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            try         
            {
                
                if (e.Key == Key.Return)
                    return;

                switch (e.Key)
                {
                    case Key.Insert:
                        
                        this.ProcessAddEditTrip(null, true);
                        e.Handled = true;
                        break;
                    case Key.F1:
                        e.Handled = true;
                        string s;

                        //this.setLastFocusedElement();

                        Driver assignDriverToTrip = UIController.SelectDriver(DAL.DriversDB.GetDrivers(false), "Assign to Trip", out s);

                        if (assignDriverToTrip == null)
                            return;

                        if (this.Dispatching.DriversOnBreak.Count(d => d.DriverID == assignDriverToTrip.DriverID) > 0)
                        {
                            if (MessageBox.Show(string.Format("Unit {0} is on Break\n\nContinue?", assignDriverToTrip.DriverCode), "On Break", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) != MessageBoxResult.Yes)
                                return;
                        }

                        DriverAndTripInformation driverAndTrip = this.Dispatching.SignInDriver(assignDriverToTrip, "");

                        if (this.TripsDataGrid.IsFocused || this.IsObjectFocused(true, this.TripsDataGrid))
                        {
                            if (this.SelectedTrip != null)
                            {
                                int selectedIndex = this.TripsDataGrid.SelectedIndex;

                                if (this.Dispatching.AssignDriverToTrip(this.SelectedTrip, assignDriverToTrip))
                                {
                                    this.TripsDataGrid.SelectedIndex = ++selectedIndex;

                                    this.selectRow();
                                }
                            }
                        }

                        else if (this.Dispatching.AddedEditedTrip != null)
                            this.Dispatching.AddedEditedTrip.DrivenBy = assignDriverToTrip;

                        if (driverAndTrip != null)
                            MessageBox.Show(string.Format("Unit {0} was signed in", driverAndTrip.UnitDriver.DriverCode), "Signed In");

                        break;

                    case Key.F2:
                        ObservableCollection<Driver> drivers = DAL.DriversDB.GetDrivers(false);
                        string signInLocation;

                        Driver driver = UIController.SelectDriver(drivers, "Available", out signInLocation, true);

                        if (driver != null)
                        {
                            bool isSignedIn = this.Dispatching.SignedInDrivers.Count(d => d.DriverID == driver.DriverID) > 0;

                            if (isSignedIn)
                            {
                                Trip completeTrip = (from d in this.Dispatching.DriversAndTripInformation
                                                     where d.UnitDriver.DriverID == driver.DriverID
                                                     select d.AssignedToTrip).ToList()[0];
                                if (completeTrip != null)
                                    this.Dispatching.ChangeTripStatus(completeTrip, TripStatusTypes.Complete, driver, false);
                                else
                                    this.Dispatching.ChangeDriversStatus(driver, DriverStatusOptions.NintyEight);
                            }
                            else
                            {
                                this.Dispatching.SignInDriver(driver, signInLocation);
                                MessageBox.Show(string.Format("Unit {0} was signed in", driver.DriverCode), "Signed In");
                            }
                        }
                        e.Handled = true;
                        break;

                    case Key.F3:
                        if (e.Key == Key.F3 && Keyboard.Modifiers == ModifierKeys.Control)
                        {
                            e.Handled = true;
                            string longTripBreakString;

                            Driver driverToChangeStatusLONGTRIPBREAK = UIController.SelectDriver(this.Dispatching.SignedInDrivers, "Select the Unit for a  LONG TRIP BREAK", out longTripBreakString);

                            if (driverToChangeStatusLONGTRIPBREAK == null)
                                return;
                            this.Dispatching.ChangeDriversStatus(driverToChangeStatusLONGTRIPBREAK, DriverStatusOptions.LongTrip);
                            break;
                        }
                        else
                        {
                            e.Handled = true;
                            string ss;
                            Driver driverToChangeStatus = UIController.SelectDriver(this.Dispatching.SignedInDrivers, "Select the Unit for a SHORT Break", out ss);

                            if (driverToChangeStatus == null)
                                return;

                            this.Dispatching.MarkDriverForBreak(driverToChangeStatus, DriverStatusOptions.ShortBreak);


                            break;
                        }

                  

                    case Key.F4:
                        e.Handled = true;
                        string s2;

                        

                        Driver driverToChangeStatus1 = UIController.SelectDriver(this.Dispatching.SignedInDrivers, "Select the Unit for a LONG Break", out s2);

                        if (driverToChangeStatus1 != null)
                            this.Dispatching.ChangeDriversStatus(driverToChangeStatus1, DriverStatusOptions.LongBreak);



                        break;

                    case Key.F5:
                        e.Handled = true;
                        string s3;
                        Driver driverToChangeStatus3 = UIController.SelectDriver(this.Dispatching.SignedInDrivers, "Quit Unit for the Day", out s3);

                        if (driverToChangeStatus3 != null)                       
                            this.Dispatching.SignOutDriver(driverToChangeStatus3);                           
                                                                              

                        break;


                    //case Key.F2:
                    //    string s;
                    //    Driver finishedDriver = UIController.SelectDriver(new ObservableCollection<Driver>(from d in this.Dispatching.DriversAndTripInformation
                    //                                                    where d.DriverStatus == DriverStatusOptions.Assigned
                    //                                                    select d.UnitDriver), out s, false);

                    //    if (driver != null)
                    //    {
                    //        Trip completeTrip = (from d in this.Dispatching.DriversAndTripInformation                                            
                    //                            where d.UnitDriver.DriverID == finishedDriver.DriverID
                    //                            select d.AssignedToTrip).ToList()[0];
                    //        this.Dispatching.ChangeTripStatus(completeTrip, TripStatusTypes.Complete, finishedDriver);
                    //    }

                    case Key.F6:
                        e.Handled = true;
                        TripAreaTypes tripAreType;
                        if (_isLongDistance)
                            tripAreType = TripAreaTypes.LongDistance;
                        else tripAreType = TripAreaTypes.Local;
                        ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(DateTime.Today, tripAreType);
                        if (scheduledTripSelected != null)
                        {
                            if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                            {
                                int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                                int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                                if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.FromLocationStreetName))
                                {
                                    this.Dispatching.AddedEditedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                                    this.Dispatching.AddedEditedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                                }
                                if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.ToLocationStreetName))
                                {
                                    this.Dispatching.AddedEditedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                                    this.Dispatching.AddedEditedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                                }
                                this.Dispatching.AddedEditedTrip.AttachedScheduledTrip = scheduledTripSelected;

                            }
                            else
                                if (this.SelectedTrip != null)
                                {
                                    int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                                    int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                                    if (string.IsNullOrWhiteSpace(this.SelectedTrip.FromLocationStreetName))
                                    {
                                        this.SelectedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                                        this.SelectedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                                    }
                                    if (string.IsNullOrWhiteSpace(this.SelectedTrip.ToLocationStreetName))
                                    {
                                        this.SelectedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                                        this.SelectedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                                    }
                                   
                           //       this.Dispatching.AddedEditedTrip.AttachedScheduledTrip = scheduledTripSelected; // added by yidel  to edit the trip in db bur not working?
                                    this.SelectedTrip.AttachedScheduledTrip = scheduledTripSelected;
                                    this.Dispatching.EditTrip(this.SelectedTrip);
                                    //this.Dispatching.ChangeTripStatus(this.SelectedTrip, TripStatusTypes.None, null, false);
                                    
                                    
                                }
                        }
                        break;
                    case Key.F7:
                        e.Handled = true;
                        if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                        {
                            if (this.Dispatching.AddedEditedTrip.ChargeToCustomer == null || 
                                (MessageBox.Show("This trip has been assigned to a customer\n\nDo you want to Re-Assign", "Customer Assigned ",
                                    MessageBoxButton.YesNo) == MessageBoxResult.Yes))
                            {
                                var customer = UIController.SelectCustomerFromList(null);
                                if (customer == null)
                                    return;
                              
                                /*if (UIController.VerifyPassCode(customer) != true)
                                    return;*/
                                
                              //  int n;
                              
                                this.Dispatching.AddedEditedTrip.CustomerPassCodeID =UIController.VerifyPassCode(customer); //customerPassCode;
                                this.Dispatching.AddedEditedTrip.ChargeToCustomer = customer;
                                if (this.Dispatching.AddedEditedTrip.TimeOfCall == null)
                                    this.Dispatching.AddedEditedTrip.TimeOfCall = DateTime.Now;
                                this.Dispatching.ChangeTripStatus(this.Dispatching.AddedEditedTrip, TripStatusTypes.None, null, false);
                            }
                        }
                        else if (this.SelectedTrip != null && (this.SelectedTrip.ChargeToCustomer == null ||
                                (MessageBox.Show("This trip has been assigned to a customer\n\nDo you want to Re-Assign", "Customer Assigned ",
                                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)))
                        {
                            var customer = UIController.SelectCustomerFromList(null);
                            if (customer != null)
                            {
                               /* if (UIController.VerifyPassCode(customer) != true)
                                    return;*/
                                this.SelectedTrip.CustomerPassCodeID = UIController.VerifyPassCode(customer);
                                this.SelectedTrip.ChargeToCustomer = customer;
                                this.SelectedTrip.ChargeToCustomerID = customer.CustomerID;
                                if (this.SelectedTrip.TimeOfCall == null)
                                    this.SelectedTrip.TimeOfCall = DateTime.Now;
                                //this.SelectedTrip.TripType = TripTypes.Charge;
                                this.Dispatching.ChangeTripStatus(this.SelectedTrip, TripStatusTypes.None, null, false);
                                this.Dispatching.EditTrip(this.SelectedTrip);
                            }
                        }//End if (this.SelectedTrip.ChargeToCustomer.CustomerID==null)
                      //  else
                      //  {

                      //      MessageBoxResult result = new MessageBoxResult();
                      //      result = MessageBox.Show("This trip has been assigned to a customer do you want to Unassign", "Customer Assigned ", MessageBoxButton.YesNoCancel); 
 
                      //              if (result == MessageBoxResult.Yes)
                      //              { 
                      //              //    this.SelectedTrip.ChargeToCustomer.CustomerID = 0;
                      //                  this.SelectedTrip.ChargeToCustomer = null;
                      //                  this.Dispatching.EditTrip(this.SelectedTrip);
                      //              }
                      //              else  
                      //              {
                      //                  return;
                      //              } 
                  
                      //}

                        break;
                    case Key.F8:
                        try
                        {
                            if (this.SelectedTrip == null)
                                return;

                            PaymentDetailsWindow.EnterPaymentDetails(this.SelectedTrip);
                            this.Dispatching.EditTrip(this.SelectedTrip);
                        }
                        catch (Exception ex)
                        {
                            UIController.DisplayErrorMessage(ex);
                        }
                        break;
                    case Key.F9:
                        try
                        {
                            if (this.SelectedTrip == null)
                                return;

                            //set focused row color, dispatcher should see clear to attach CC to right trip
                            DataGridRow dgr = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(this.SelectedTrip);
                            dgr.Background = new SolidColorBrush(Colors.Blue);
                            bool creditPmt = this.SelectedTrip.ChargeToCustomer == null ? false : this.SelectedTrip.ChargeToCustomer.IsInvoiceCustomer;
                            int cstmerID = this.SelectedTrip.ChargeToCustomer == null ? 0 : this.SelectedTrip.ChargeToCustomer.CustomerID;
                            int pmtID = 0;
                            decimal ccChargeAmount = 0;
                            decimal ccChargeForTrip = 0;
                            //UIController.ShowChargeCCWindow(new CCOnFile() { CustomerID = cstmerID }, creditPmt, ref pmtID);

                            bool charged = UIController.ShowChargeCCWindow(new CCOnFile() { CustomerID = cstmerID }, false, ref pmtID, ref ccChargeAmount, ref ccChargeForTrip); //requested to be always default non creditable payment for dispatcher
                            if (charged)
                            {
                                this.SelectedTrip.PaymentID = pmtID;
                                this.SelectedTrip.CCPaid = ccChargeForTrip;
                                this.SelectedTrip.ToLocationComment = string.Format("CC Charged for {0:C2}", ccChargeAmount.ToString("C2"));
                                this.Dispatching.EditTrip(this.SelectedTrip); //to save pmt id
                            }

                            //set back the default background color
                            //dgr.ClearValue(DataGridRow.BackgroundProperty);
                        }
                        catch (Exception){}
                        finally
                        {
                            if (this.SelectedTrip != null)
                            {
                                //set back the default background color
                                DataGridRow dgr = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(this.SelectedTrip);
                                dgr.ClearValue(DataGridRow.BackgroundProperty);
                            }
                        }
                        
                        break;
                    case Key.System:
                        if (e.SystemKey == Key.F10)
                        {
                            e.Handled = true;
                            string s1;

                            //this.setLastFocusedElement();

                            Driver assignDriverToTrip1 = UIController.SelectDriver(DAL.DriversDB.GetDrivers(false), "Send trip to Driver", out s);

                            if (assignDriverToTrip1 == null)
                                return;

                            if (this.Dispatching.DriversOnBreak.Count(d => d.DriverID == assignDriverToTrip1.DriverID) > 0)
                            {
                                if (MessageBox.Show(string.Format("Unit {0} is on Break\n\nContinue?", assignDriverToTrip1.DriverCode), "On Break", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Yes) != MessageBoxResult.Yes)
                                    return;
                            }

                            DriverAndTripInformation driverAndTrip1 = this.Dispatching.SignInDriver(assignDriverToTrip1, "");

                            if (this.TripsDataGrid.IsFocused || this.IsObjectFocused(true, this.TripsDataGrid))
                            {
                                if (this.SelectedTrip != null)
                                {
                                    //int selectedIndex = this.TripsDataGrid.SelectedIndex;

                                    SignalRHubDispatchingClient.DispatchTripToDriver(new BOL_OTR.Trip_OTR()
                                    {
                                        TripID = this.SelectedTrip.TripID,
                                        AssignedByDispatcherUserID = UIController.CurrentSession.SessionUser.UserID,
                                        FromLocation = this.SelectedTrip.FromLocation,
                                        ToLocation = this.SelectedTrip.ToLocation,
                                        DriverID = assignDriverToTrip1.DriverID,
                                        TotalStops = SelectedTrip.NoOfStops.GetValueOrDefault(0),
                                        Price = SelectedTrip.TripPrice.GetValueOrDefault(0),
                                        TripDetailedStatus = BOL_OTR.TripDetailedStatusOptions.RequestingToAcceptFromDriver
                                    });
                                }
                            }
                            else if (this.Dispatching.AddedEditedTrip != null)
                            {
                                SignalRHubDispatchingClient.DispatchTripToDriver(new BOL_OTR.Trip_OTR()
                                {
                                    TripID = this.Dispatching.AddedEditedTrip.TripID,
                                    AssignedByDispatcherUserID = UIController.CurrentSession.SessionUser.UserID,
                                    FromLocation = this.Dispatching.AddedEditedTrip.FromLocation,
                                    ToLocation = this.Dispatching.AddedEditedTrip.ToLocation,
                                    DriverID = assignDriverToTrip1.DriverID
                                });
                            }

                        }
                        break;
                    case Key.F10:
                        
                        break;
                    case Key.F11:
                       if (!_isLongDistance)
                            return;
                        
                        UIController.ShowAssignDriversToScheduledTrips(DateTime.Today);
                        // REfresh dispatching Window and get LongDistancetrips
                        this.Dispatching.ProcessScheduledTrip();
                        break;

                    case Key.F12:
                        if (!_isLongDistance)
                            return;
                        UIController.LoadScheduleTripWindow();
                      // REfresh dispatching Window and get LongDistancetrips
                        this.Dispatching.ProcessScheduledTrip();
                        break;
                    case Key.Escape:
                        if (this.Dispatching.AddedEditedTrip != null)
                        {
                            if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                            {
                                this.ProcessAddEditTrip(null, false);
                                e.Handled = true;
                            }
                        }
                        break;

                    case Key.Next:
                        if (this.Dispatching.AddedEditedTrip != null)
                        {
                            this.AddEditTripBorder.Focus();
                            if (lastFocusedInNewTripElement != null)
                                lastFocusedInNewTripElement.Focus();
                            //else
                            //    this.FromHouseNoTextBox.Focus();
                            e.Handled = true;
                        }
                        break;

                    case Key.Prior:
                        if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                            lastFocusedInNewTripElement = System.Windows.Input.FocusManager.GetFocusedElement(this);
                        this.TripsDataGrid.Focus();
                        if (this.TripsDataGrid.Items.Count > 0)
                        {
                            DataGridRow dgrow = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(this.TripsDataGrid.SelectedItem);
                            if (dgrow != null)
                                dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                            else
                            {
                                this.TripsDataGrid.SelectedIndex = this.TripsDataGrid.Items.Count - 1;
                                dgrow = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(this.TripsDataGrid.SelectedItem);
                                if (dgrow != null)
                                    dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                            }

                        }
                        e.Handled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        

        void setLastFocusedElement()
        {
            lastFocusedInNewTripElement = System.Windows.Input.FocusManager.GetFocusedElement(this);
        }

        private void ComboBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            //ComboBox combo = (sender as ComboBox);
            //if (combo.IsDropDownOpen)
            //    return;

            //    combo.IsDropDownOpen = true;
        }

        private void FromLocationStreetNameComboBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Return)
                    return;

                char c;
                bool converted = char.TryParse(e.Key.ToString(), out c);

                if (!converted)
                    return;

                ComboBox combo = (sender as ComboBox);

                //if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift && e.KeyboardDevice.IsKeyDown(Key.Tab))
                //{
                    
                //        (e.OriginalSource as UIElement).MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                //        e.Handled = true;
                    
                //}

                //if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
                //{
                //    if (combo == FromLocationStreetNameComboBox)
                //        this.NoteFromLocationTextBox.Focus();
                //    else if (combo == this.ToLocationStreetNameComboBox)
                //        this.NoteToLocationTextBox.Focus();
                //    e.Handled = true;

                //    return;
                //}

                //if (e.Key == Key.Return)
                //{
                //    TextBox TxtBox = (TextBox)combo.Template.FindName("PART_EditableTextBox", combo);
                //    int selectionIndex = TxtBox.SelectionStart;
                    
                //    string textEntered = "";
                //    if (selectionIndex > 0)
                //        textEntered = TxtBox.Text.Remove(selectionIndex);
                //    else
                //        textEntered = TxtBox.Text;

                //    string streetName = DAL.StreetAbbreviationDB.GetStreetNameByAbbreviation(textEntered);
                //    if (!string.IsNullOrWhiteSpace(streetName))
                //    {
                //        combo.Text = streetName;
                //        (e.OriginalSource as UIElement).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                //        e.Handled = true;
                //    }
                                        
                //    return;
                //}


                if (!combo.IsDropDownOpen)
                    combo.IsDropDownOpen = true;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                
            }
        }

        private void SaveTripButton_Click(object sender, RoutedEventArgs e)
        
        {
            try
            {
     //           Trip tripToAdd = this.Dispatching.AddedEditedTrip;
                //this.Dispatching.AddTripToList(this.Dispatching.AddedEditedTrip);
                //this.lastFocusedInNewTripElement = null;
                
                ////this.TripsDataGrid.Focus();
                //this.TripsDataGrid.SelectedItem = tripToAdd;

                //this.TripsDataGrid.ScrollIntoView(this.TripsDataGrid.SelectedItem);

                //if (this.TripsDataGrid.Items.Count > 0)
                //{

                //    //DataGridCell cell = GetCell(this.TripsDataGrid.Items.IndexOf(tripToAdd), 1);
                //    //if(cell != null)
                //    //    cell.Focus();
                //    DataGridRow dgrow = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(tripToAdd);
                //    if (dgrow != null)
                //        dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                //}
                
                this.ProcessAddEditTrip(new Trip() { ObjectState = BOState.New }, true);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                
            }
        }

        void selectRow()
        {
            DataGridRow dgrow = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromItem(this.SelectedTrip);
            if (dgrow != null)
                dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        public DataGridCell GetCell(int row, int column)
        {

            DataGridRow rowContainer = GetRow(row);

            if (rowContainer != null)
            {
                DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(rowContainer);

                // try to get the cell but it may possibly be virtualized
                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                if (cell == null)
                {
                    // now try to bring into view and retreive the cell
                    this.TripsDataGrid.ScrollIntoView(rowContainer, this.TripsDataGrid.Columns[column]);
                    cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                }
                return cell;
            }
            return null;
        }

        public DataGridRow GetRow(int index)
        {
            DataGridRow row = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromIndex(index);
            if (row == null)
            {
                // may be virtualized, bring into view and try again
                this.TripsDataGrid.ScrollIntoView(this.TripsDataGrid.Items[index]);
                row = (DataGridRow)this.TripsDataGrid.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return row;
        }

        static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }


        private void CancelAddEdiTripButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ProcessAddEditTrip(null, false);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);                
            }
        }

        private void ProcessCancelAddEditTrip()
        {
            this.Dispatching.CancelAddEditTrip();
            this.AddEditTripBorder.Focus();
            this.ToHouseNoTextBox.Focus();
        }

        public Trip SelectedTrip
        {
            get { return this.TripsDataGrid.SelectedItem as Trip; }
            set { this.TripsDataGrid.SelectedItem = value; }
        }

        public DriverAndTripInformation SelectedDriverAndTripInfo
        {
            get { return this.DriverAndTripListBox.SelectedItem as DriverAndTripInformation; }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Dispatching.Dispose();

            if (mapWindow != null)
                mapWindow.Close();

            UIController.MainWindowReference.Visibility = System.Windows.Visibility.Visible;
            
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedTrip == null)
                return;
            if ((sender as Control).Tag.ToString() == "Complete")
                this.Dispatching.ChangeTripStatus(this.SelectedTrip, TripStatusTypes.Complete, null, false);
            else if ((sender as Control).Tag.ToString() == "Canceled")
                this.Dispatching.ChangeTripStatus(this.SelectedTrip, TripStatusTypes.Canceled, null, false);
            else if ((sender as Control).Tag.ToString() == "Remove Unit")
                this.Dispatching.ChangeTripStatus(this.SelectedTrip, TripStatusTypes.ReceivedCall, null, true);
            else if ((sender as Control).Tag.ToString()=="Not Medicaid")
            {
                this.SelectedTrip.AttachedScheduledTrip = null;
                this.Dispatching.AddedEditedTrip.IsMedicaidTrip = false;
                this.Dispatching.EditTrip(this.SelectedTrip); 
            }


            //int selctedIx = this.TripsDataGrid.SelectedIndex;
            this.TripsDataGrid.SelectedIndex = -1;
            //this.TripsDataGrid.SelectedIndex = selctedIx;
        }

        private void ListBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            ListBox parent = (ListBox)sender;
            dragSource = parent;
            object data = GetDataFromListBox(dragSource, e.GetPosition(parent));

            if (data != null)
            {
                var status = (data as DriverAndTripInformation).DriverStatus;
                if (status == DriverStatusOptions.NintyEight || status == DriverStatusOptions.ShortBreak)
                  //  if(!_isLongDistance )
                    DragDrop.DoDragDrop(parent, data, DragDropEffects.Move);
            }
        }

        private void ListBox_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            object ClickedOnData = GetDataFromListBox(this.DriverAndTripListBox, e.GetPosition(this.DriverAndTripListBox));
            if (ClickedOnData != null)
            {
                DriverAndTripInformation driverAndTripInfo = ClickedOnData as DriverAndTripInformation;
                this.processClickFor98(driverAndTripInfo);
                
            }
        }

        void processClickFor98(DriverAndTripInformation driverAndTripInfo)
        {
            if (driverAndTripInfo == null || driverAndTripInfo.DriverStatus == DriverStatusOptions.NintyEight)
                return;
            if (driverAndTripInfo.DriverStatus == DriverStatusOptions.Assigned)
            {
                this.Dispatching.ChangeTripStatus(driverAndTripInfo.AssignedToTrip, TripStatusTypes.Complete, driverAndTripInfo.UnitDriver, false);
                if (driverAndTripInfo.AssignedSecondTrip != null)
                {
                    this.Dispatching.ChangeTripStatus(driverAndTripInfo.AssignedSecondTrip, TripStatusTypes.Complete, driverAndTripInfo.UnitDriver, false);
                    driverAndTripInfo.AssignedSecondTrip = null;
                }
            }
            else
                this.Dispatching.ChangeDriversStatus(driverAndTripInfo.UnitDriver, DriverStatusOptions.NintyEight);
        }

        #region GetDataFromListBox(ListBox,Point)
        ListBox dragSource;
        private static object GetDataFromListBox(ListBox source, Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    data = source.ItemContainerGenerator.ItemFromContainer(element);
                    if (data == DependencyProperty.UnsetValue)
                    {
                        element = VisualTreeHelper.GetParent(element) as UIElement;
                    }
                    if (element == source)
                    {
                        return null;
                    }
                }
                if (data != DependencyProperty.UnsetValue)
                {
                    return data;
                }
            }
            return null;
        }

        #endregion

        private void ListBox_Drop(object sender, DragEventArgs e)
        {
            ListBox parent = (ListBox)sender;
            object dropedOn = GetDataFromListBox(parent, e.GetPosition(parent));
            if (dropedOn == null)
                return;

            DriverAndTripInformation data = (DriverAndTripInformation)e.Data.GetData(typeof(DriverAndTripInformation));
            DriverAndTripInformation dropedOnData = (DriverAndTripInformation)dropedOn;

            if (data == dropedOnData)
            {
                this.processClickFor98(data);
                return;
            }

            if (dropedOnData.StatusChangedTime != null)
                data.InternalStatusChangedTime = dropedOnData.InternalStatusChangedTime.Value.AddTicks(-1);

            foreach (DriverAndTripInformation dti in this.Dispatching.DriversAndTripInformation)
            {
                if (dti != data && dti != dropedOnData)
                {
                    if (dti.InternalStatusChangedTime < dropedOnData.InternalStatusChangedTime)
                        dti.InternalStatusChangedTime = dti.InternalStatusChangedTime.Value.AddTicks(-1);
                }
            }

            this.Dispatching.UpdateDriverTripTimers();
            this.Dispatching.OnPropertyChanged("SortedDriversAndTrips");
            
        }

        private void NoteFromLocationTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
                {
                    this.ToHouseNoTextBox.Focus();
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                
            }
        }

        private void FromHouseNoTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (this.IsSystemKey(e.Key))
                //    return;

                TextBox houseNo = (sender as TextBox);

                if (e.Key == Key.Space)
                {
                    (e.OriginalSource as UIElement).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    e.Handled = true;
                    return;
                }

                char c;
                bool converted = char.TryParse(e.Key.ToString(), out c);

                if ((houseNo.Text.Length == 0 && converted && char.IsLetter(Convert.ToChar(e.Key.ToString()))))
                {
                    TextBox TxtBox = null;
                    ComboBox combo = null;
                    if (houseNo.Text.Length == 0 && char.IsLetter(Convert.ToChar(e.Key.ToString())))
                    {
                        
                        if (sender == this.FromHouseNoTextBox)
                        {
                            //this.Dispatching.AddedEditedTrip.FromLocationStreetName = e.Key.ToString();
                            combo = this.FromLocationStreetNameComboBox;
                        }
                        else
                        {
                            //this.Dispatching.AddedEditedTrip.ToLocationStreetName = e.Key.ToString();
                            combo = this.ToLocationStreetNameComboBox;
                        }

                        TxtBox = (TextBox)combo.Template.FindName("PART_EditableTextBox", combo);

                        

                        
                    }

                    (e.OriginalSource as UIElement).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));

                    if (TxtBox != null)
                    {


                        if (!combo.IsDropDownOpen)
                            combo.IsDropDownOpen = true;

                        combo.Text = e.Key.ToString();
                        TxtBox.SelectionStart = TxtBox.Text.Length;
                    }

                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = MessageBox.Show("You're about to close the dispatching system.\n\nAre you sure you want to continue?", "Exit?", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes;
        }



        private void FromLocationStreetNameComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if(this.IsSystemKey(e.Key))
                return;

            ComboBox MyComboBox = (sender as ComboBox);

            string text = MyComboBox.Text;

            if (string.IsNullOrWhiteSpace(MyComboBox.Text))
                MyComboBox.ItemsSource = this.Dispatching.StreetAbbreviations;
            else
                MyComboBox.ItemsSource = this.Dispatching.StreetAbbreviations.Where(s => s.StreetName.ToUpper().StartsWith(MyComboBox.Text.ToUpper()));
            if(!char.IsControl(Convert.ToChar(e.Key)) &&
                !string.IsNullOrWhiteSpace(text) && 
                string.IsNullOrWhiteSpace(MyComboBox.Text))
                MyComboBox.Text = text;            
        }

        private void FromLocationStreetNameComboBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ComboBox combo = (sender as ComboBox);

            TextBox TxtBox = (TextBox)combo.Template.FindName("PART_EditableTextBox", combo);
            int selectionIndex = TxtBox.SelectionStart;

            string textEntered = "";
            if (selectionIndex > 0 && selectionIndex < TxtBox.Text.Length)
                textEntered = TxtBox.Text.Remove(selectionIndex);
            else
                textEntered = TxtBox.Text;

            string streetName = DAL.StreetAbbreviationDB.GetStreetNameByAbbreviation(textEntered);
            if (!string.IsNullOrWhiteSpace(streetName))
            {
                combo.Text = streetName;
                //(e.OriginalSource as UIElement).MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                //e.Handled = true;
            }
        }

        private void LongBreakMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedDriverAndTripInfo == null)
                    return;

                if (sender == this.LongBreakMenuItem)
                    this.Dispatching.MarkDriverForBreak(this.SelectedDriverAndTripInfo.UnitDriver, DriverStatusOptions.LongBreak);
                else if (sender == this.ShortBreakMenuItem)
                    this.Dispatching.MarkDriverForBreak(this.SelectedDriverAndTripInfo.UnitDriver, DriverStatusOptions.ShortBreak);
                else if (sender == this.LongTripBreakMenuItem)
                    this.Dispatching.ChangeDriversStatus(this.SelectedDriverAndTripInfo.UnitDriver, DriverStatusOptions.LongTrip);
                else if (sender == this.SignOutMenuItem)
                    this.Dispatching.SignOutDriver(this.SelectedDriverAndTripInfo.UnitDriver);
                else if (sender == this.Undo98MenuItem)
                    this.Dispatching.Undo98Status(this.SelectedDriverAndTripInfo);
                else if (sender == this.ChangeLocationMenuItem)
                {
                    string newLocation;
                    if (UIController.GetInput("New Location", "Change Location to", out newLocation))
                    {
                        this.Dispatching.ChangeDriverLocation(this.SelectedDriverAndTripInfo, newLocation);
                        //this.Dispatching.ChangeDriversStatus(this.SelectedDriverAndTripInfo.UnitDriver, this.SelectedDriverAndTripInfo.DriverStatus);
                    }
                }

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);                
            }
        }

        private void ResetDispatchingInfoButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("You're about to reset the Trips and Drivers\n\nContinue?", "Reset Dispatching Info", MessageBoxButton.YesNo
                    , MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.Yes)
                    this.Dispatching.ResetDispatchingInformation();
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void SelectTrip(Trip tripToSelect, bool startNewTrip)
        {
            try
            {
                //this.TripsDataGrid.Focus();
                //this.TripsDataGrid.SelectedItem = null;
                //this.TripsDataGrid.SelectedItems.Clear();
                //this.TripsDataGrid.SelectedItem = tripToSelect;
                this.TripsDataGrid.ScrollIntoView(this.TripsDataGrid.Items[this.TripsDataGrid.Items.Count -1]);// ScrollIntoView(this.TripsDataGrid.SelectedItem);

                if (startNewTrip)
                    this.txtLine.Focus();
                    //this.FromHouseNoTextBox.Focus();

            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void TripsDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                if(this.SelectedTrip != null)                
                {                    
                    var result = MessageBox.Show("You're about to delete the selected trip\n\nContinue?", "Delete Trip?", MessageBoxButton.YesNo);
                    if (result == MessageBoxResult.Yes)                    
                        this.Dispatching.DeleteTrip(this.SelectedTrip);
                }

                e.Handled = true;
            }
        }

        private void TripsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (this.SelectedTrip != null && e.EditAction == DataGridEditAction.Commit)
            {
                this.Dispatching.EditTrip(this.SelectedTrip);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> list = this.Dispatching.GetLogOnOff().ToList();
                string s = "";
                foreach (string str in list)
                {
                    s += str + "\n";
                }
                MessageBox.Show(s);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);                
            }
        }

        private void TripsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Show_HideRemovedTrips_Click(object sender, RoutedEventArgs e)
        {
            UIController.LoadTripsListFroDispatcherWindow();
        }

        private void txtLine_TextChanged(object sender, TextChangedEventArgs e)
        {
            int line = 0;
            int.TryParse(this.txtLine.Text, out line);
            if (line > 0)
            {
                CallerID.PhoneInfo phoneInfo = this.CallerIDList.Where(l => l.PhoneLine == line).FirstOrDefault();
                if (phoneInfo != null)
                    this.Dispatching.AddedEditedTrip.CallBackPhoneNo = phoneInfo.CallerID;

                //txtPreviousLine.Text = txtLine.Text;
                //this.txtLine.Clear();
                
                //string msg = "List:";
                
                //foreach (var phone in CallerIDList)
                //{
                //    string m = string.Format("\nLine: {0}, Number: {1}, Name: {2}", phone.PhoneLine, phone.CallerID, phone.CallerName);
                //    msg = msg + m;
                //}
                //msg = msg + "\nEndList:";
                //MessageBox.Show(msg + "\nGot " + phoneInfo.CallerID);
            }
        }

        private void txtLine_GotFocus(object sender, RoutedEventArgs e)
        {
            txtLine.Clear();
        }

        private void btnPmtDetails_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PaymentDetailsWindow.EnterPaymentDetails(this.SelectedTrip);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var ca = ((TextBlock)sender).DataContext as BOL.SectionCarsAvailability;
                ChangeSectionCarAvailability(ca);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        void ChangeSectionCarAvailability(BOL.SectionCarsAvailability ca)
        {
            try
            {
                CarsAvailabilityTypes carAvail = CarsAvailabilityTypes.Available;

                switch (ca.Availability)
                {
                    case CarsAvailabilityTypes.Available:
                        carAvail = CarsAvailabilityTypes.WillBeAvailabe;
                        break;
                    case CarsAvailabilityTypes.NotAvailable:
                        carAvail = CarsAvailabilityTypes.Available; 
                        break;
                    case CarsAvailabilityTypes.WillBeAvailabe:
                        carAvail = CarsAvailabilityTypes.NotAvailable; 
                        break;
                    default:
                        break;
                }

                this.Dispatching.UpdateSectionCarAvailability(ca.SectionName, carAvail);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        private void OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                //get the button clicked
                var originalSource = e.OriginalSource;

                //set selected item on click
                object clicked = (e.OriginalSource as FrameworkElement).DataContext;
                //var lbi = lstPermissions.ItemContainerGenerator.ContainerFromItem(clicked) as ListBoxItem;
                //lbi.IsSelected = true;

                //DriverPermissionRequest request = lstPermissions.SelectedItem as DriverPermissionRequest;
                DriverPermissionRequest request = clicked as DriverPermissionRequest;

                if (request == null)
                    return;

                bool isPermited = (originalSource as Button).Tag.ToString() == "1";

                SignalRHubDispatchingClient.AnswerPermissionRequest(request.DriverAndTripInfo.UnitDriver.DriverID, isPermited);

                Dispatching.DriversPermissionRequests.Remove(request);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
    }

    public static class SignalRHubDispatchingClient
    {
        //signalR variables
        static string _dispatchingHubUrl;
        static HubConnection _hubConnection;
        static IHubProxy _dispatchingHubProxy;
        public static IDispatchingMVVM DispatchingRef;

        static bool _isSubscribed = false;
        private static Microsoft.AspNet.SignalR.Client.ConnectionState _currentConnectionState;

        internal static void Subscribe(string url)
        {
            _dispatchingHubUrl = "http://" + url + "/signalr/";
            var querystringData = new Dictionary<string, string>();
            querystringData.Add("clientType", "Dispatcher");
            querystringData.Add("dispatherID", UIController.CurrentSession.SessionUser.UserID.ToString());

            _hubConnection = new HubConnection(_dispatchingHubUrl, querystringData);

            _dispatchingHubProxy = _hubConnection.CreateHubProxy("DispatchingHub");

            _hubConnection.Start().Wait();

            _dispatchingHubProxy.Invoke("Join", "Dispatcher");

            _isSubscribed = true;

            _hubConnection.StateChanged += (StateChange obj) =>
            {
                _currentConnectionState = obj.NewState;
                switch (obj.NewState)
                {
                    case Microsoft.AspNet.SignalR.Client.ConnectionState.Connecting:
                        break;
                    case Microsoft.AspNet.SignalR.Client.ConnectionState.Connected:
                        _isSubscribed = true;
                        break;
                    case Microsoft.AspNet.SignalR.Client.ConnectionState.Reconnecting:
                        break;
                    case Microsoft.AspNet.SignalR.Client.ConnectionState.Disconnected:
                        _isSubscribed = false;
                        break;
                    default:
                        break;
                }
            };

            //call from signalr hub                
            _dispatchingHubProxy.On<int, int, double, double, float>("TabletReportedLocation", (driverID, tablet, latitude, longitude, speed) =>
            {

            });

            _dispatchingHubProxy.On<int, int>("DriverRequestChangeDestinationPermission", (driverID, tripID) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var trip = DispatchingRef.DriversAndTripInformation.FirstOrDefault(t => t.UnitDriver.DriverID == driverID &&
                                                                                            t.AssignedToTrip != null && t.AssignedToTrip.TripID == tripID);

                    if (trip != null)
                    {
                        trip.AssignedToTrip.IsRequestingDestinationChangePermission = true;
                    }

                    DispatchingRef.DriversPermissionRequests.Add(new DriverPermissionRequest()
                    {
                        DriverAndTripInfo = trip
                    });
                });
            });

        }


        #region Calls To SignalR

        internal static void DispatchTripToDriver(BOL_OTR.Trip_OTR trip)
        {
            if (!_isSubscribed)
                throw new Exception("Not subscribed to SignalR Dispatching Hub");

            _dispatchingHubProxy.Invoke<BOL_OTR.Trip_OTR>("DispatchTripToDriver", trip);
        }

        internal static void AnswerPermissionRequest(int driverID, bool isPermited)
        {
            _dispatchingHubProxy.Invoke("PermissionRequestResponse", driverID, isPermited);
        }

        #endregion Calls To SignalR

    }

    public delegate void SelectTripInListCallback(Trip tripToSelect, bool startNewTrip);

    public enum AssignedUnitDiscrepencyOption
    {
        FinishedFirst = 0,
        ReassignToNew = 1,
        CanceledFirst = 2,
        Combine = 3,
        Cancel = 4
    }

    public class StreetAbbreviationsList : ObservableCollection<StreetAbbreviation> { }

    public class DriverPermissionRequest
    {
        public DriverAndTripInformation DriverAndTripInfo { get; set; }
        public Driver Driver { get; set; } 
        public Trip Trip { get; set; }
        public DateTime TimeRequested { get; set; }

        public string RequestDescription
        {
            get { return "Destination Change"; }
        }
    }

}
