﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    public class StreetAbbreviation : BO, IEditableObject
    {
        public StreetAbbreviation()
        {
            this.StreetAbbData = new StreetAbbreviationData() { Abbreviation = "", StreetName = "", City = "", State = "", PostalCode = "" };
        }

        struct StreetAbbreviationData
        {
            internal int StreetAbbreviationID;
            internal string Abbreviation;
            internal string StreetName;
            internal string City;
            internal string State;
            internal string PostalCode;
        }

        StreetAbbreviationData StreetAbbData;
        StreetAbbreviationData backupData;
        private bool inTxn = false;
        public bool IsEditing
        {
            get { return this.inTxn; }
        }


        public int StreetAbbreviationID
        {
            get { return this.StreetAbbData.StreetAbbreviationID; }
            set { this.StreetAbbData.StreetAbbreviationID = value; }
        }


        public string Abbreviation
        {
            get { return this.StreetAbbData.Abbreviation; }
            set { this.StreetAbbData.Abbreviation = value; this.OnPropertyChanged("AllowSave"); }
        }


        public string StreetName
        {
            get { return this.StreetAbbData.StreetName; }
            set { this.StreetAbbData.StreetName = value; this.OnPropertyChanged("AllowSave"); }
        }

        public string City
        {
            get { return this.StreetAbbData.City; }
            set { this.StreetAbbData.City = value; this.OnPropertyChanged("AllowSave"); }
        }

        public string State
        {
            get { return this.StreetAbbData.State; }
            set { this.StreetAbbData.State = value; this.OnPropertyChanged("AllowSave"); }
        }

        public string PostalCode
        {
            get { return this.StreetAbbData.PostalCode; }
            set { this.StreetAbbData.PostalCode = value; this.OnPropertyChanged("AllowSave"); }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.Abbreviation)
                    && !string.IsNullOrWhiteSpace(this.StreetName);
            }

        }



        #region IEditableObject Members

        public void BeginEdit()
        {
            if (!inTxn)
            {
                this.backupData = this.StreetAbbData;
                this.inTxn = true;
                this.OnPropertyChanged("IsEditing");
            }
        }

        public void CancelEdit()
        {
            if (inTxn)
            {
                this.StreetAbbData = backupData;
                inTxn = false;
                this.OnPropertyChanged("StreetName");
                this.OnPropertyChanged("Abbreviation");
                this.OnPropertyChanged("IsEditing");
            }
        }

        public void EndEdit()
        {
            if (inTxn)
            {
                backupData = new StreetAbbreviationData();
                inTxn = false;
                this.OnPropertyChanged("IsEditing");
            }
        }

        #endregion
    }
}
