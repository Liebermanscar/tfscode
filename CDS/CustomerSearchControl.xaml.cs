﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Collections.ObjectModel;
using DAL;
using BOL;

namespace CDS
{
    public partial class CustomerSearchControl
    {
        public CustomerSearchControl()
        {
            this.InitializeComponent();
            //this.AlertsListBox.Visibility = Visibility.Collapsed;

        }



        private Customer customerSelected;
        public Customer CustomerSelected
        {
            get { return (this.DataContext as Customer); }
            set
            {
                Customer previousCustomer = this.DataContext as Customer;
                this.DataContext = value;
                if (OnCustomerSelected != null)
                {
                    OnCustomerSelected(this, new CustomerSearchEventArgs() { CustomerSelected = this.CustomerSelected, CustomerList = this.CustomerList, PreviousCustomer = previousCustomer });
                }
            }

        }
        public CustomerFilterOptions CustomerFilterOptionSelected { get; set; }
        bool allowChange;
        public bool AllowChange
        {
            get { return allowChange; }
            set { allowChange = value; tbSearchBox.IsEnabled = value; btSearchOptions.IsEnabled = value; }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            btSearchOptions.ContextMenu.PlacementTarget = btSearchOptions;
            btSearchOptions.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            ContextMenuService.SetPlacement(btSearchOptions, System.Windows.Controls.Primitives.PlacementMode.Bottom);

            cbFilterOption.SelectedIndex = (int)Properties.Settings.Default.DefaultCustomerFilterOption;
            btSearchOptions.ContextMenu.IsOpen = true;
        }

        public object Owner { get; set; }
        public ObservableCollection<Customer> CustomerList { get; set; }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            //PropertyValues["DefaultAHFilter"].PropertyValue
            Properties.Settings.Default.DefaultCustomerFilterOption = (CustomerFilterOptions)(sender as ComboBox).SelectedIndex;
            Properties.Settings.Default.Save();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (sender == miName)
                GetCustomer(CustomerFilterOptions.Name);
            else if (sender == miPhoneNo)
                GetCustomer(CustomerFilterOptions.PhoneNo);
            else if (sender == miAutomatic)
                GetCustomer(CustomerFilterOptions.Automatic);
            else
                GetCustomer(CustomerFilterOptions.CustomerID);
        }

        public string FilterText { get { return this.tbSearchBox.Text; } }

        private void tbSearchBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {
                e.Handled = true;
                GetCustomer(Properties.Settings.Default.DefaultCustomerFilterOption);
            }
        }

        private void GetCustomer(CustomerFilterOptions filterOptions)// ,bool InvoiceCustomersOnly)
        {
            if (this.FilterText.Length == 0)
                return;
            ObservableCollection<Customer> customers = UIController.GetCustomer(filterOptions, this.FilterText, DataRequestTypes.LiteData, this.InvoiceCustomersOnlyCheckBox.IsChecked.GetValueOrDefault(true));



            if (customers == null)
            {
                this.tbNotFound.Visibility = Visibility.Visible;
                return;
            }
            else
                this.tbNotFound.Visibility = Visibility.Collapsed;

            this.CustomerList = customers;
            /*
                        if (customers.Count == 1)
                        {
                            if (this.Owner is SelectCustomerWindow)
                            {
                                (this.Owner as Window).DataContext = customers;
                                (this.Owner as SelectCustomerWindow).CustomerGridControl.Focus();
                                (this.Owner as SelectCustomerWindow).SelectCustomerButton.IsEnabled = true;
                                this.CustomerSelected = customers[0];
                            }
                            else
                                this.CustomerSelected = UIController.SelectCustomerFromList(customers);


                          //  this.CustomerSelected = UIController.SelectCustomerFromList(customers);
                        }
                        else*/
            if (this.Owner is SelectCustomerWindow)
            {
                (this.Owner as Window).DataContext = customers;
                (this.Owner as SelectCustomerWindow).CustomerGridControl.Focus();
                if (customers.Count > 0)// >= 1)
                {
                    (this.Owner as SelectCustomerWindow).SelectCustomerButton.IsEnabled = true;
                    this.CustomerSelected = customers[0];
                }
            }
            else
                this.CustomerSelected = UIController.SelectCustomerFromList(customers);


            //this.DataContext = this.AHSelected;            
        }

        public event CDS.CustomerSearchResultEventHandler OnCustomerSelected;

        private void tbSearchBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.tbSearchBox.Text = "";
        }

        public void ClearAH()
        {
            this.CustomerSelected = null;
            this.DataContext = null;
        }


        private void btnAddAH_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.LoadAccountInfoPage(null, true);
            if (this.Owner is CustomerInfoPage)
            {
                (this.Owner as CustomerInfoPage).NewCustomer();
                return;
            }

            if (this.Owner is SelectCustomerWindow)
                (this.Owner as SelectCustomerWindow).DialogResult = false;
        }



        public CustomerFilterOptions FilterdBy { get; set; }

        private void btnOpenAH_Click(object sender, RoutedEventArgs e)
        {
            if (this.CustomerSelected == null)
                return;
            //MainWindow.LoadAccountInfoPage(this.CustomerSelected, false);

            if (this.Owner is SelectCustomerWindow)
                (this.Owner as SelectCustomerWindow).DialogResult = false;
        }

        private void btnAction_Click(object sender, RoutedEventArgs e)
        {/*
            btnAction.ContextMenu.PlacementTarget = btnAction;
            btnAction.ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            ContextMenuService.SetPlacement(btnAction, System.Windows.Controls.Primitives.PlacementMode.Bottom);
            btnAction.ContextMenu.IsOpen = true;    */
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if (this.CustomerSelected == null)
                return;
            if (this.Owner is SelectCustomerWindow)
                return;

            this.CustomerSelected = UIController.GetCustomer(CustomerFilterOptions.CustomerID, this.CustomerSelected.CustomerID.ToString(), DataRequestTypes.FullData, this.InvoiceCustomersOnlyCheckBox.IsChecked.GetValueOrDefault(true))[0];


            if (this.Owner is SelectCustomerWindow)
                (this.Owner as SelectCustomerWindow).DialogResult = false;
        }

        private void CheckBox_SourceUpdated(object sender, DataTransferEventArgs e)
        {

        }









    }
}