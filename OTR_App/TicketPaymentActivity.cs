﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
	[Activity (Label = "Ticket Payment")]			
	public class TicketPaymentActivity : Activity 
	{
        Button btnOk;
        Button btnCancel;

		protected override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);

            int pos = int.Parse(Intent.GetStringExtra("TicketSelectedPosition"));

            SetContentView(Resource.Layout.TicketPayment);

            btnOk = FindViewById<Button>(Resource.Id.ticketPayment_btnOK);
            btnCancel = FindViewById<Button>(Resource.Id.ticketPayment_btnCancel);

            var tickets = UIController.TicketTypes;
            var ticketsAdapter = new TicketsAdapter(this, tickets.ToList());
            var ticketsListView = FindViewById<ListView>(Resource.Id.ticketPayment_ListView);
            ticketsListView.ChoiceMode = ChoiceMode.Single;
            ticketsListView.SetItemChecked(pos, true);
            ticketsListView.Adapter = ticketsAdapter;

            //ticketsListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            //{
            //    var selected = tickets[e.Position];
            //};

            btnOk.Click += delegate {
                Intent resultIntent = new Intent(this, typeof(ActiveTripActivity));

                if (ticketsListView.CheckedItemPosition > -1)
                {
                    var selected = tickets[ticketsListView.CheckedItemPosition];
                    //var selectedId = ticketsAdapter.GetItemId(ticketsListView.CheckedItemPosition);

                    resultIntent.PutExtra("TicketSelectedPosition", ticketsListView.CheckedItemPosition.ToString());
                    resultIntent.PutExtra("TicketSelectedId", selected.TicketID.ToString());
                    resultIntent.PutExtra("TicketSelectedValue", selected.TicketValue.ToString());

                    SetResult(Result.Ok, resultIntent);
                    Finish();
                }
            };

            btnCancel.Click += delegate
            {
                SetResult(Result.Canceled);
                Finish();
            };
        }
	}
}

