using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace OTR_App
{
    public class TicketsAdapter : BaseAdapter
    {
        List<BOL_OTR.TicketPaid_OTR> _items;
        Activity _activity;

        public TicketsAdapter(Activity context, List<BOL_OTR.TicketPaid_OTR> items)
        {
            _activity = context;
            _items = items;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return _items[position].TicketID;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? _activity.LayoutInflater.Inflate(Resource.Layout.TicketListItem, parent, false);

            var ticketName = view.FindViewById<TextView>(Resource.Id.ticketItem_TicketName);
            var ticketValue = view.FindViewById<TextView>(Resource.Id.ticketItem_TicketValue);

            ticketName.Text = _items[position].TicketName;
            ticketValue.Text = _items[position].TicketValue.ToString("c");

            return view;
        }
    }
}