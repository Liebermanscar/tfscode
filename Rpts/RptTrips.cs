using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using BOL;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using System.Collections.ObjectModel;

namespace Rpts
{    

    /// <summary>
    /// Summary description for RptTrips.
    /// </summary>
    public partial class RptTrips : Telerik.Reporting.Report
    {
        public RptTrips()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
           
        
        }


        public RptTrips(ObservableCollection<Trip> trips): this()
        {
            this.DocumentName = "Report All Trips";
            this.DataSource = trips;
        }
    
    }
}