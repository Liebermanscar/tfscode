﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hipaa837
{
    public class CompanyInformation
    {
        public string CompanyName;
        public string MedicaidProviderNo;
        public string ETIN;
        public string TaxId;
        public string ContactName;
        public string CompanyPhoneNo;
        public string CompanyAddress;
        public string CompanyCity;
        public string CompanyState;
        public string CompanyZip;
    }

    public class FileInformation
    {
        public int FileUniqueId;
        public DateTime FileDate;
    }

    public class ClaimInformation
    {
        public int ClaimUniqueId;
        public string PatientLastName;
        public string PatientFirstName;
        public string MedicaidNo;
        public string PatientAddress;
        public string PatientCity;
        public string PatientState;
        public string PatientZip;
        public DateTime DateOfBirth;
        public string Gender;
        public string InvoiceNo;
        public int AdjustmentCode;
        public bool HasDelayReason;
        public int DelayReasonCode;
        public string PriorAuthNo;
        public string RemittanceTCN;
        public string DiagnosisCode = "ABK:R69";
        public string BillingCode;
        public string Modifier;
        public double Amount;
        public double Units;
        public DateTime DateOfService;
    }

    public class Hipaa837
    {
        private CompanyInformation _ci = null;
        private FileInformation _fi = null;
        public List<ClaimInformation> _claims = null;

        public Hipaa837(CompanyInformation Company, FileInformation File)
        {
            _ci = Company;
            _fi = File;
            _claims = new List<ClaimInformation>();
        }

        public void AddClaim(ClaimInformation Claim)
        {
            _claims.Add(Claim);
        }

        public void GenerateFile(string Filename)
        {
            //Templates for header, claims and supplemental elements/records
            string _headerTemplate = "ISA*00*          *00*          *ZZ*{MedProvNoPadded}*ZZ*EMEDNYBAT      *{FileDateShort}*{FileTime}*U*00501*{FileIdPadded}*1*P*:~";
            _headerTemplate += "GS*HC*{MedProvNo}*EMEDNYBAT*{FileDateLong}*{FileTime}*{FileId}*X*005010X222A1~";
            _headerTemplate += "ST*837*0001*005010X222A1~";
            _headerTemplate += "BHT*0019*00*{FileId}*{FileDateLong}*{FileTime}*CH~";
            _headerTemplate += "NM1*41*2*{CompanyName}*****46*{MedProvNo}~";
            _headerTemplate += "PER*IC*{ContactName}*TE*{CompanyPhoneNo}~";
            _headerTemplate += "NM1*40*2*NYSDOH*****46*141797357~";
            _headerTemplate += "HL*1**20*1~";
            _headerTemplate += "NM1*85*2*{CompanyName}~";
            _headerTemplate += "N3*{CompanyAddress}~";
            _headerTemplate += "N4*{CompanyCity}*{CompanyState}*{CompanyZip}~";
            _headerTemplate += "REF*EI*{TaxId}~";
            _headerTemplate += "{Claims}";
            _headerTemplate += "SE*{SegmentCount}*0001~";
            _headerTemplate += "GE*1*{FileId}~";
            _headerTemplate += "IEA*1*{FileIdPadded}~";

            string _claimTemplate = "HL*{ClaimUniqueId}*1*22*0~";
            _claimTemplate += "SBR*P*18**MEDICAID*****MC~";
            _claimTemplate += "NM1*IL*1*{PatientLastName}*{PatientFirstName}****MI*{MedicaidNo}~";
            _claimTemplate += "N3*{PatientAddress}~";
            _claimTemplate += "N4*{PatientCity}*{PatientState}*{PatientZip}~";
            _claimTemplate += "DMG*D8*{DateOfBirth}*{Gender}~";
            _claimTemplate += "NM1*PR*2*NYSDOH*****PI*141797357~";
            _claimTemplate += "REF*G2*{ETIN}~";
            _claimTemplate += "CLM*{InvoiceNo}*{Amount}***99:B:{AdjustmentCode}*Y*A*Y*N{DelaySuplemment}~";
            _claimTemplate += "REF*G1*{PriorAuthNo}~";
            _claimTemplate += "{AdjustmentSupplement}";
            _claimTemplate += "HI*{DiagnosisCode}~";
            _claimTemplate += "LX*1~";
            _claimTemplate += "SV1*HC:{BillingCode}*{Amount}*UN*{Units}*99**1**~";
            _claimTemplate += "DTP*472*D8*{DateOfService}~";
            _claimTemplate += "REF*G1*{PriorAuthNo}~";
            _claimTemplate += "REF*6R*{ClaimUniqueId}~";

            string _delayedClaimSuplemmentTemplate = "***********{DelayReason}";

            string _adjustmentSuplemmentTemplate = "REF*F8*{RemittanceTCN}~";

            //Start building envelope and header
            string filedata = _headerTemplate.Replace("{MedProvNoPadded}", _ci.MedicaidProviderNo.PadRight(15));
            filedata = filedata.Replace("{FileDateShort}", _fi.FileDate.ToString("yyMMdd"));
            filedata = filedata.Replace("{FileTime}", _fi.FileDate.ToString("HHmm"));
            filedata = filedata.Replace("{FileIdPadded}", _fi.FileUniqueId.ToString().PadLeft(9, '0'));
            filedata = filedata.Replace("{MedProvNo}", _ci.MedicaidProviderNo);
            filedata = filedata.Replace("{FileDateLong}", _fi.FileDate.ToString("yyyyMMdd"));
            filedata = filedata.Replace("{FileId}", _fi.FileUniqueId.ToString());
            filedata = filedata.Replace("{CompanyName}", _ci.CompanyName);
            filedata = filedata.Replace("{ContactName}", _ci.ContactName);
            filedata = filedata.Replace("{CompanyPhoneNo}", _ci.CompanyPhoneNo);
            filedata = filedata.Replace("{CompanyAddress}", _ci.CompanyAddress);
            filedata = filedata.Replace("{CompanyCity}", _ci.CompanyCity);
            filedata = filedata.Replace("{CompanyState}", _ci.CompanyState);
            filedata = filedata.Replace("{CompanyZip}", _ci.CompanyZip);
            filedata = filedata.Replace("{TaxId}", _ci.TaxId);

            //Build claims loops
            string claimsdata = "";
            foreach (var claim in _claims)
            {
                string claimdata = _claimTemplate.Replace("{ClaimUniqueId}", claim.ClaimUniqueId.ToString());
                claimdata = claimdata.Replace("{PatientLastName}", claim.PatientLastName);
                claimdata = claimdata.Replace("{PatientFirstName}", claim.PatientFirstName);
                claimdata = claimdata.Replace("{MedicaidNo}", claim.MedicaidNo);
                claimdata = claimdata.Replace("{PatientAddress}", claim.PatientAddress);
                claimdata = claimdata.Replace("{PatientCity}", claim.PatientCity);
                claimdata = claimdata.Replace("{PatientState}", claim.PatientState);
                claimdata = claimdata.Replace("{PatientZip}", claim.PatientZip);
                claimdata = claimdata.Replace("{DateOfBirth}", claim.DateOfBirth.ToString("yyyyMMdd"));
                claimdata = claimdata.Replace("{Gender}", claim.Gender);
                claimdata = claimdata.Replace("{ETIN}", _ci.ETIN);
                claimdata = claimdata.Replace("{InvoiceNo}", claim.InvoiceNo);
                claimdata = claimdata.Replace("{Amount}", claim.Amount.ToString("f2"));
                claimdata = claimdata.Replace("{AdjustmentCode}", claim.AdjustmentCode.ToString());
                claimdata = claimdata.Replace("{PriorAuthNo}", claim.PriorAuthNo);
                claimdata = claimdata.Replace("{DiagnosisCode}", claim.DiagnosisCode);
                claimdata = claimdata.Replace("{BillingCode}", claim.BillingCode + (claim.Modifier != "" ? ":" + claim.Modifier : ""));
                claimdata = claimdata.Replace("{Units}", claim.Units.ToString("f2"));
                claimdata = claimdata.Replace("{DateOfService}", claim.DateOfService.ToString("yyyyMMdd"));

                //Delayed claims
                string delaySupplement = "";
                if (claim.HasDelayReason)
                    delaySupplement = _delayedClaimSuplemmentTemplate.Replace("{DelayReason}", claim.DelayReasonCode.ToString());
                claimdata = claimdata.Replace("{DelaySuplemment}", delaySupplement);

                //Adjustments
                string adjustmentSupplement = "";
                if (claim.AdjustmentCode != 0)
                    adjustmentSupplement = _adjustmentSuplemmentTemplate.Replace("{RemittanceTCN}", claim.RemittanceTCN);
                claimdata = claimdata.Replace("{AdjustmentSupplement}", adjustmentSupplement);
                claimsdata += claimdata;
            }

            //Incoporate claims and complete file data
            filedata = filedata.Replace("{Claims}", claimsdata);
            int segmentCount = filedata.Count(c => c == '~');
            segmentCount -= 4; //First two and final segment are not counted as well as the first occurence of the tilde.
            filedata = filedata.Replace("{SegmentCount}", segmentCount.ToString());

            //Save file
            System.IO.File.WriteAllText(Filename, filedata);
        }
    }
}
