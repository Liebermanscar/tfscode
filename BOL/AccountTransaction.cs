﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BOL
{
    public class AccountTransaction : BO
    {

        decimal _amount;

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        string _typeOfTransaction;

        public string TypeOfTransaction
        {
            get { return _typeOfTransaction; }
            set { _typeOfTransaction = value; }
        }
        User _byUser;

        public User ByUser
        {
            get { return _byUser; }
            set { _byUser = value; }
        }

        DateTime _dateOfTransaction;

        public DateTime DateOfTransaction
        {
            get { return _dateOfTransaction; }
            set { _dateOfTransaction = value; }
        }
        decimal _accountRuningBalance;

        public decimal AccountRuningBalance
        {
            get { return _accountRuningBalance; }
            set { _accountRuningBalance = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }
}
