﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Net;
using System.Runtime.Serialization.Json;

namespace BingMaps_WPF
{
    public static class MapsController
    {
        public static MapMVVM _mapMVVMRef;
        public static Map _bingMap;
        public static ConsumeDispatcherService _dispatcherService;

        public static string BingMapsKey = Properties.Settings.Default.BingMapsKey;
        public static string MapSessionID;
        public static void Initialize()
        {
            var mvvm = new MapMVVM();

            _mapMVVMRef = (MainWindow.Self.mapMVVM = mvvm);
            _bingMap = MainWindow.Self.map;
            _dispatcherService = new ConsumeDispatcherService();

            _bingMap.CredentialsProvider.GetCredentials(c => MapSessionID = c.ApplicationId);

            if (_dispatcherService.ConnectToDispatcherService())
            {
                var trips = _dispatcherService.GetAllTrips();
                foreach (var trip in trips.Where(t => t.DrivenBy == null))
                {
                    UpdateTrip(trip, ObjectAction.Add);
                }

                var dti = _dispatcherService.GetAllDrivers();
                foreach (var driver in dti.Where(d => d.DriverStatus == BOL.DriverStatusOptions.NintyEight))
                {
                    UpdateCarStatus(driver, ObjectAction.Add);
                }
            }

        }

        public static void UpdateCarStatus(BOL.DriverAndTripInformation driver, ObjectAction action)
        {
            var carOnMap = new CarOnMap(driver.UnitDriver.DriverID, driver.UnitDriver.DriverCode, "", 0, 0);

            var Point = RESTService.GetReverseGeocode("10950", driver.DriversLocation);
            carOnMap.Location = new Microsoft.Maps.MapControl.WPF.Location( Point.Coordinates[0], Point.Coordinates[1]);
            MapsController.UpdateCarStatus(carOnMap, ObjectAction.Remove);
        }

        public static void UpdateCarStatus(CarOnMap carOnMap, ObjectAction action = ObjectAction.AddOrUpdate)
        {
            InvokeOnUIThread(() =>
            {
                var carToUpdate = _mapMVVMRef.Cars.Where(c => c.CarID == carOnMap.CarID).FirstOrDefault();
                if (action == ObjectAction.Remove && carToUpdate != null)
                    _mapMVVMRef.Cars.Remove(carToUpdate);

                else
                {
                    

                    carToUpdate = carToUpdate != null ? carToUpdate : carOnMap;
                    carToUpdate.Location = new Microsoft.Maps.MapControl.WPF.Location(carOnMap.Location);

                    _mapMVVMRef.Cars.AddOrUpdate(carToUpdate);
                }
            });
        }

        public static void UpdateTrip(BOL.Trip trip, ObjectAction action)
        {
            UpdateTrip(new TripOnMap(trip.TripID, trip.FromLocation, 0, 0), action);
        }

        public static void UpdateTrip(TripOnMap trip, ObjectAction action)
        {
            try
            {
                Point point = null;
                if (action == ObjectAction.Add)
                    point = RESTService.GetReverseGeocode("10950", trip.Address);

                InvokeOnUIThread(() =>
                {

                    if (action == ObjectAction.Add)
                    {
                        if (point != null)
                            trip.Location = new Microsoft.Maps.MapControl.WPF.Location(point.Coordinates[0], point.Coordinates[1]);
                        MapsController._mapMVVMRef.Trips.Add(trip);
                    }
                    else
                    {
                        var tripToRemove = _mapMVVMRef.Trips.Where(t => t.TripID == trip.TripID).FirstOrDefault();
                        MapsController._mapMVVMRef.Trips.Remove(tripToRemove);
                    }
                }
                    );
            }
            catch (Exception)
            {

                throw;
            }
        }

        /*internal static void UpdateCar(CarOnMap carOnMap, ObjectAction action)
        {
            try
            {
                

                InvokeOnUIThread(() =>
                {
                    if (action == ObjectAction.Add)
                    {
                        if()

                        if (point != null)
                            trip.Location = new Microsoft.Maps.MapControl.WPF.Location(point.Coordinates[0], point.Coordinates[1]);
                        MapsController.MapMVVMRef.Trips.Add(trip);
                    }
                    else
                    {
                        var tripToRemove = MapMVVMRef.Trips.Where(t => t.TripID == trip.TripID).FirstOrDefault();
                        MapsController.MapMVVMRef.Trips.Remove(tripToRemove);
                    }
                }
                    );
            }
            catch (Exception)
            {

                throw;
            }
        }*/

        //public static void AddTrip(TripOnMap trip)
        //{            
        //    MapMVVMRef.Trips.Add(trip);
        //}

        //public static void RemoveTrip(TripOnMap trip)
        //{
        //    var tripToRemove = MapMVVMRef.Trips.Where(t => t.TripID == trip.TripID).FirstOrDefault();
        //    if(tripToRemove != null)
        //        MapMVVMRef.Trips.Remove(tripToRemove);
        //}

        internal static void SetZoomLevel(int zoomLevel)
        {
            _mapMVVMRef.MapZoomLevel = zoomLevel;
        }

        private static void InvokeOnUIThread(Action action)
        {
            try
            {
                Application.Current.Dispatcher.InvokeAsync(action);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static class RESTService
        {

            public static string CreateRequest(string queryString)
            {
                string UrlRequest = "http://dev.virtualearth.net/REST/v1/Locations?" +
                                     queryString +
                                     //"?output=xml" +
                                     " &key=" + ((MapSessionID == string.Empty) ? BingMapsKey : MapSessionID);
                return (UrlRequest);
            }

            public static Response MakeRequest(string requestUrl)
            {
                try
                {
                    HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                            throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Response));
                        object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                        Response jsonResponse = objResponse as Response;
                        return jsonResponse;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
            }

            public static Point ProcessGeocode(Response locationsResponse)
            {
                Location location = (Location)locationsResponse.ResourceSets[0].Resources[0];
                return location.Point;                
            }

            static public Point GetReverseGeocode(string postalCode, string addressLine)
            {
                var req = CreateRequest("postalCode="+ postalCode + "&addressLine=" + addressLine);

                var res = MakeRequest(req);

                return ProcessGeocode(res);
            }

            /*static public void ProcessResponse(Response locationsResponse)
            {

                int locNum = locationsResponse.ResourceSets[0].Resources.Length;

                //Get formatted addresses: Option 1
                //Get all locations in the response and then extract the formatted address for each location
                Console.WriteLine("Show all formatted addresses");
                for (int i = 0; i < locNum; i++)
                {
                    Location location = (Location)locationsResponse.ResourceSets[0].Resources[i];
                    Console.WriteLine(location.Address.FormattedAddress);
                }
                Console.WriteLine();

                //Get the Geocode Points for each Location
                for (int i = 0; i < locNum; i++)
                {
                    Location location = (Location)locationsResponse.ResourceSets[0].Resources[i];
                    Console.WriteLine("Geocode Points for " + location.Address.FormattedAddress);
                    int geocodePointNum = location.GeocodePoints.Length;
                    for (int j = 0; j < geocodePointNum; j++)
                    {
                        Console.WriteLine("    Point: " + location.GeocodePoints[j].Coordinates[0].ToString() + "," +
                                                     location.GeocodePoints[j].Coordinates[1].ToString());
                        double test = location.GeocodePoints[j].Coordinates[1];
                        Console.Write("    Usage: ");
                        for (int k = 0; k < location.GeocodePoints[j].UsageTypes.Length; k++)
                        {
                            Console.Write(location.GeocodePoints[j].UsageTypes[k].ToString() + " ");
                        }
                        Console.WriteLine("\n\n");
                    }
                }
                Console.WriteLine();


                //Get all locations that have a MatchCode=Good and Confidence=High
                Console.WriteLine("Locations that have a Confidence=High");
                for (int i = 0; i < locNum; i++)
                {
                    Location location = (Location)locationsResponse.ResourceSets[0].Resources[i];
                    if (location.Confidence == "High")
                        Console.WriteLine(location.Address.FormattedAddress);
                }
                Console.WriteLine();

                Console.WriteLine("Press any key to exit");
                Console.ReadKey();


            }*/

        }
    }
}
