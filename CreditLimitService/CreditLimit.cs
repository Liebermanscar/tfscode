﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Sathish.ServiceScheduler;
using log4net;
using System.Reflection;
using System.Configuration;
using CreditLimitService.DAL;
using System.Threading;


namespace CreditLimitService
{
    public partial class CreditLimit : ServiceBase
    {
        Scheduler sch = new Scheduler("CreditLimit");
        static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        //static readonly ILog _log = LogManager.GetLogger(typeof(CreditLimit));
        public CreditLimit()
        {
            InitializeComponent();           
           // Test();
        }        
        protected override void OnStart(string[] args)
        {
          //  _log.Info("Service Started at " + DateTime.Now.ToString());
            sch.SchedulerFired += new EventHandler(CheckCreditLimit);
            sch.ScheduleDaily(ConfigurationManager.AppSettings["time"].ToString());
        }

        private void CheckCreditLimit(object sender, EventArgs e)
        {
            ProcessLimitCheck();
        }
        private void Test()
        {
            ProcessLimitCheck();
        }
        private void ProcessLimitCheck()
        {
            List<CustomerInfo> emailCustomerRecords = new List<CustomerInfo>();
            List<Customer> customers = CreditMethods.GetAllCustomerWithCreditLImit();
            CustomerInfo cusomerInfo = null;
            //List<CustomerInfo>
            foreach (var customer in customers)
            {
                cusomerInfo = new CustomerInfo
                {
                    Customer = customer
                };
                try
                {
                    bool expired = CreditMethods.IsCustomerCreditLimitExpired(cusomerInfo);
                    if (expired)
                    {
                        emailCustomerRecords.Add(cusomerInfo);
                    }
                }
                catch(Exception ex)
                {
                    //_log.Error(ex.Message.ToString());
                }
            }
            if (emailCustomerRecords.Count > 0)
            {
                Thread tr = new Thread(() => SendEmail(emailCustomerRecords));
                tr.Start();
            }
        }
        private void SendEmail(List<CustomerInfo> customers)
        {
            foreach (var customer in customers)
            {
                try
                {
                    CreditMethods.SendEmail(customer);
                }
                catch (Exception ex)
                {
                   // _log.Error(ex.Message.ToString());
                }
            }
        }

        protected override void OnStop()
        {
           // _log.Info("Service stopped at " + DateTime.Now.ToString());
        }


    }
}
