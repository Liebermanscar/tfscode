﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
using System.Data;
namespace DAL
{
    public class DailyStartupDB : Database
    {
        public DailyStartupDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(DailyStartup dailyStartup)
        {
            this.Transaction = this.Connection.BeginTransaction();
            try
            {
                string serverMessage;
                SqlParameter outParam = new SqlParameter("@DailyStartupID", System.Data.SqlDbType.Int) { Direction = ParameterDirection.Output };
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@ForDriverID", dailyStartup.ForDriverID),
                    new SqlParameter("@StartupDateAndTime", dailyStartup.StartupDateAndTime),
                    new SqlParameter("@EnvelopeDate", dailyStartup.EnvelopeDate),
                    new SqlParameter("@SessionIDAdded", Database.SessionID),
                    outParam
                };

                int noOfRecordsAffected = this.ExecuteNonQuery("AddDailyStartups", param, CommandType.StoredProcedure, out serverMessage);
                if (noOfRecordsAffected > 0)
                {
                    dailyStartup.DailyStartupID = Convert.ToInt32(outParam.Value);
                    using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(this.Connection, this.Transaction))
                    {
                        foreach (var generateTicketBookStatuses in dailyStartup.StatusChangedForTicketBooksGivenToDriver)
                        {
                            ticketBookStatusDB.Add(generateTicketBookStatuses.TicketBookStatuses);

                        }
                    }
                }
                if (this.Transaction.Connection == null)
                    this.Transaction = this.Connection.BeginTransaction();

                this.Transaction.Commit();

                return noOfRecordsAffected > 0;
            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw ex;
            }
            finally
            {
                this.Transaction.Dispose();
            }
        }

        public bool Edit(DailyStartup dailyStartup)
        {
            string serverMessage;
            try
            {
                this.Transaction = this.Connection.BeginTransaction();


                int noOfRecAffected = this.ExecuteNonQuery("UpdateDailyStartups", new SqlParameter[]
           {
                    new SqlParameter("@DailyStartupID", dailyStartup.DailyStartupID),
                    new SqlParameter("@ForDriverID", dailyStartup.ForDriverID),
                    new SqlParameter("@StartupDateAndTime", dailyStartup.StartupDateAndTime),
                    new SqlParameter("@EnvelopeDate", dailyStartup.EnvelopeDate),

           }, CommandType.StoredProcedure, out serverMessage);

                using (TicketBookStatusDB ticketBookStatusDB = new TicketBookStatusDB(this.Connection, this.Transaction))
                {
                    foreach (var generateTicketBookStatuses in dailyStartup.StatusChangedForTicketBooksGivenToDriver)
                    {
                        ticketBookStatusDB.Add(generateTicketBookStatuses.TicketBookStatuses);

                    }
                }
                return noOfRecAffected > 0;

            }
            catch (Exception ex)
            {
                this.Transaction.Rollback();
                throw ex;
            }
            finally
            {
                this.Transaction.Dispose();
            }

        }
        public ObservableCollection<DailyStartup> Get()
        {
            var dailyStartups = new ObservableCollection<DailyStartup>();
            return dailyStartups;
        }
        public static bool AddDailyStartup(DailyStartup dailyStartup)
        {
            using (DailyStartupDB dailyStartupDB = new DailyStartupDB(null, null))
            {
                return dailyStartupDB.Add(dailyStartup);
            }
        }
        public static bool EditDailyStartup(DailyStartup dailyStartup)
        {
            using (DailyStartupDB dailyStartupDB = new DailyStartupDB(null, null))
            {
                return dailyStartupDB.Edit(dailyStartup);
            }
        }
        public ObservableCollection<DailyStartup> GetDailyStartups()
        {
            using (DailyStartupDB dailyStartupDB = new DailyStartupDB(null, null))
            {
                return dailyStartupDB.Get();
            }
        }


        public int getTripsMissingEnvelopesTotal(int driverID)
        {

            DailyStartup ds = new DailyStartup();
            using (SqlDataReader dr = this.GetReader("GetCountOfMissingEnvelopes", new SqlParameter[]
            {
                new SqlParameter("@JustTotal", true),
                new SqlParameter("@ForDriverID",driverID),
            }, CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    ds.CountOfOutstandingEnvelopes = Convert.ToInt32(dr["Total"]);
                }
            };
            return ds.CountOfOutstandingEnvelopes;
        }

        public static int GetTripsNotAssigned(int driverID)
        {
            using (DailyStartupDB dailyStartupDB = new DailyStartupDB(null, null))
            {
                return dailyStartupDB.getTripsMissingEnvelopesTotal(driverID);
            }
        }
        public ObservableCollection<DateTime> getDatesOfMissingEnvelopes(int driverID)
        {
            var missingEnvelopesDates = new ObservableCollection<DateTime>();

            using (SqlDataReader dr = this.GetReader("GetCountOfMissingEnvelopes", new SqlParameter[]
            {
                new SqlParameter("@JustTotal", false),
                new SqlParameter("@ForDriverID",driverID),
            }, CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    // var missingDate = new DateTime();
                    var missingDate = Convert.ToDateTime(dr["DateOfTrips"]);

                    missingEnvelopesDates.Add(missingDate);

                }
            }
            return missingEnvelopesDates;
        }

        public static ObservableCollection<DateTime> GetDatesOfMissingEnvelopes(int driverID)
        {
            using (DailyStartupDB dailyStartupDB = new DailyStartupDB(null, null))
            {
                return dailyStartupDB.getDatesOfMissingEnvelopes(driverID);

            }
        }

    }
}
