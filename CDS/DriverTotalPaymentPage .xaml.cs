﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriverPaymentsListPage.xaml
    /// </summary>
    public partial class DriverTotalPaymentPage : Page, IDisplayTextBlock
    {
        public ObservableCollection<Trip> trips { get; set; }
        public DriverTotalPaymentPage()
        {
            InitializeComponent();
            ToDatePicker.SelectedDate = DateTime.Today;
            FromDatePicker.SelectedDate = DateTime.Today;
            // this.trips = DAL.TripDB.GetAverageTripsAmountByDriver(0, 0, 0, 0, null, null, true, null, 0, 0);

        }
        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt1 = new DateTime();
                DateTime dt2 = new DateTime();
                if (FromDatePicker.SelectedDate != null)
                { dt1 = FromDatePicker.SelectedDate.Value.Date; }
                else { dt1 = DateTime.Now.Date; }
                if (ToDatePicker.SelectedDate != null)
                { dt2 = ToDatePicker.SelectedDate.Value.Date; }
                else { dt2 = DateTime.Now.Date; }
                DriverPaymetnsDataGrid.DataContext = DAL.TripDB.GetAverageTripsAmountByDriver(0, 0, 0, 0, dt1, dt2, false, null, 0, 0);
                this.trips = DAL.TripDB.GetAverageTripsAmountByDriver(0, 0, 0, 0, dt1, dt2, false, null, 0, 0);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        # region
        public string HeaderText
        {
            get
            {
                return "Driver Payment List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "Are You Sure You want ot close this window?";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion



        private void RadContextMenu_Opened(object sender, RoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (row != null)
            {
                row.IsSelected = row.IsCurrent = true;
                GridViewCell cell = menu.GetClickedElement<GridViewCell>();
                if (cell != null)
                {
                    cell.IsCurrent = true;
                }
            }
            else
            {
                menu.IsOpen = false;
            }
        }

        private void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            RadContextMenu menu = (RadContextMenu)sender;
            RadMenuItem clickedItem = e.OriginalSource as RadMenuItem;
            GridViewRow row = menu.GetClickedElement<GridViewRow>();

            if (clickedItem != null && row != null)
            {
                string header = Convert.ToString(clickedItem.Header);

                switch (header)
                {
                    case "Export to Excel":
                        UIController.ExportTelerikGrid(DriverPaymetnsDataGrid, UIController.ExportFormat.Excel);
                        break;
                    default:
                        break;
                }
            }
        }

    }
}
