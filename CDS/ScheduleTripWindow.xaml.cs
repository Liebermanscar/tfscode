﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ScheduleTripWindow.xaml
    /// </summary>
    public partial class ScheduleTripWindow : Window
    {
        public ScheduleTripWindow()
        {
            InitializeComponent();
            this._scheduledTrip = new ScheduledTrip()
            {
                ObjectState = BOState.New
            };
            this.dpPickUpDate.SelectedDate = DateTime.Today;
            // this._scheduledTrip.TripAreaType =TripAreaTypes.LongDistance;


        }
        public ScheduleTripWindow(ScheduledTrip scheduledTrip)
            : this()
        {
            this._scheduledTrip = scheduledTrip;
            //ObjectState = BOState.Edit;
        }
        private void ChargeCustomerButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (this.cbNeedToConfirm.IsChecked == true)
                this._scheduledTrip.NeedToConfirm = true;
            else
                this._scheduledTrip.NeedToConfirm = false;

            this.DialogResult = true;
        }

        ScheduledTrip _scheduledTrip
        {
            get { return this.DataContext as ScheduledTrip; }
            set { this.DataContext = value; }
        }

        private void txtContactName_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            try
            {
                if ((sender as TextBox).Text == "")
                    return;
                Customer cust = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.PhoneNo, (sender as TextBox).Text, DataRequestTypes.LiteData, true).FirstOrDefault();

                if (cust != null)
                {
                    var i = UIController.VerifyPassCode(cust);
                    this._scheduledTrip.SCustomerPassCodeID = i;
                    this._scheduledTrip.ScheduledByCustomer = cust;
                    this._scheduledTrip.CustomerPhoneNo = cust.HomePhoneNo;

                }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                this._scheduledTrip.ScheduledByCustomer = UIController.SelectCustomerFromList(null);
                var i = UIController.VerifyPassCode(this._scheduledTrip.ScheduledByCustomer);
                this._scheduledTrip.SCustomerPassCodeID = i;
                //    this._scheduledTrip.DrivenFirstName = this._scheduledTrip.ScheduledByCustomer.FirstName;
                //  this._scheduledTrip.DrivenLastName = this._scheduledTrip.ScheduledByCustomer.LastName;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
    }
}
