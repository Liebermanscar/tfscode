﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using BOL;
using System.Reflection;
using System.Collections.ObjectModel;
using DevExpress.Xpf.Printing;
using Microsoft.Win32;
using Rpts;
using System.IO;
using Telerik.Windows.Controls;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Threading;
using DevExpress.Xpf.Core;


namespace CDS
{
    public static class UIController
    {
        public static MainWindow MainWindowReference { get; set; }

        public static UserSession CurrentSession { get; set; }

        public static void StartupInitalization()
        {
            App.Current.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);
            DAL.Database.ConnectionString = Properties.Settings.Default.DBConnectionString;

            CCProc.ProcessCC.SourceKey = "12iAtNYBr80g1Xm8mszNUWl428b0gGd3";//fidelty old"s0i0p7u85uq717F97Lkn9pGmLBL20j8b";
            UIController.LoadLogonPage();
        }

        public static void SetDBUser(string userCode)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.DBConnectionString);
                builder.Password = "lIEB@3000";
                builder.UserID = userCode;
                DAL.Database.ConnectionString = builder.ConnectionString;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        static void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                UIController.DisplayErrorMessage(e.Exception);
            }
            catch (Exception ex)
            {
                MessageBox.Show(e.Exception +"\n\n" + ex.Message);                
            }
        }

        internal static void CreateNewSession(User user)
        {
            try
            {
                int sessionID;
                UserSession userSession = DAL.SessionDB.AddNewSession(user.UserID, out sessionID);
                UIController.SetDBUser(user.UserCode); 
                UIController.CurrentSession = userSession;
                DAL.Database.SessionID = userSession.SessionID;
                UIController.BlankPage();

                UIController.MainWindowReference.mainSideBar.IsEnabled = true;// MainNavigationGroupBox.Visibility = Visibility.Visible;
                //UIController.LoadHomePage();
                //MainWindowReference.RibbonControl.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        static public void DisplayErrorMessage(Exception ex)
        {
            new Action(() =>
            {
                try
                {
                    UIController.SendEmailMessege("Error In CDS", string.Format("Error Message: {0}\n\nStatck Trace: {1}\n\nInner Exception: {2}", ex.Message, ex.StackTrace, ex.InnerException.Message));
                }
                catch
                { }
            }).BeginInvoke(null, null);

            ErrorInfoWindow errInfoWindow = new ErrorInfoWindow();
            errInfoWindow.ClientsFeedback = new ClientFeedback() { ExceptionCatched = ex, ApplicationInfo = Assembly.GetEntryAssembly().FullName };
            try
            {
                //errInfoWindow.ClientsFeedback.OrganizationInformation = DAL.OrganizationDB.Organization;
                errInfoWindow.ClientsFeedback.ReplyToEmailAddress = errInfoWindow.ClientsFeedback.OrganizationInformation.EmailAddress;
            }
            catch { }
            errInfoWindow.ShowDialog();
        }

        public static void PrintErrorInfo(ClientFeedback clientFeedback)
        {
            //using (XRptErrorInfo rptErrInfo = new XRptErrorInfo())
            //{
            //    BindingList<ClientFeedback> lst = new BindingList<ClientFeedback>();
            //    lst.Add(clientFeedback);
            //    rptErrInfo.DataSource = lst;
            //    rptErrInfo.ShowPreviewDialog();
            //}
        }

        #region Load Pages
        public static int NoOfOpenPages()
        {
            return UIController.MainWindowReference.MainTabControl.Items.Count;
        }

        static private Page OpenPage(Page page, bool WithCloseOption, string addToCaption = "")
        {
            try
            {
                    if (page is IDisplayTextBlock && (page as IDisplayTextBlock).AllowOnlyOnce)
                {
                    foreach (TabItem tItem in MainWindowReference.MainTabControl.Items)
                    {
                        Page existingPage = ((tItem.Content as Frame).Content as Page);
                        if (existingPage.GetType() == page.GetType())
                        {
                            MainWindowReference.MainTabControl.SelectedItem = tItem;
                            return existingPage;
                        }
                    }
                    //Page existingPage = this.mainTabControl.Items
                    //   .Cast<Page>()
                    //   .Where(p => p.GetType() == page.GetType()).FirstOrDefault();
                    //if (existingPage != null)
                    //{
                    //    this.mainTabControl.SelectedItem = existingPage;
                    //    return existingPage;
                    //}
                }

                Frame frame = new Frame();
                frame.Name = "MainFrame";

                //frame.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                //frame.VerticalContentAlignment = VerticalAlignment.Top;

                frame.Content = page;
                //frame.VerticalContentAlignment = VerticalAlignment.
                //frame.HorizontalContentAlignment = HorizontalAlignment.Stretch;

                //ScrollViewer sv = new ScrollViewer();
                //sv.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                //sv.Content = frame;


                //if(pages.w

                TabItem ti = new TabItem();
                ti.VerticalContentAlignment = VerticalAlignment.Stretch;
                ti.HorizontalContentAlignment = HorizontalAlignment.Stretch;

                ti.Content = frame;// sv;
                if (WithCloseOption)
                {
                    TabHeaderControl thc = new TabHeaderControl();
                    thc.Tag = ti;
                    //thc.tbHeaderTitle.Text = page.Title;

                    if (page is IDisplayTextBlock)
                    {
                        thc.tbHeaderTitle.DataContext = page;
                        if (!string.IsNullOrEmpty(addToCaption))
                        {
                            IDisplayTextBlock disp = (page as IDisplayTextBlock);
                            disp.HeaderText += " - [" + addToCaption + "]";
                        }
                        Binding b = new Binding("HeaderText") { Mode = BindingMode.OneWay };
                        thc.tbHeaderTitle.SetBinding(TextBlock.TextProperty, b);
                    }
                    else
                    {
                        Binding b = new Binding("DisplayInformation");
                        thc.tbHeaderTitle.DataContext = (page.DataContext as BO);
                        thc.tbHeaderTitle.SetBinding(TextBlock.TextProperty, b);
                    }
                    thc.btnCloseTab.Click += delegate(object sender, RoutedEventArgs e)
                    {
                        TabItem tabItem = (TabItem)(sender as Button).Tag;
                        UIController.ClosePage(tabItem);

                    };
                    thc.miClose.Click += delegate(object sender, RoutedEventArgs e)
                    {
                        TabItem tabItem = (TabItem)((TabHeaderControl)((ContextMenu)((MenuItem)sender).Parent).PlacementTarget).Tag;
                        UIController.ClosePage(tabItem);
                    };

                    thc.miCloseAll.Click += delegate(object sender, RoutedEventArgs e)
                    {
                        for (int i = UIController.MainWindowReference.MainTabControl.Items.Count - 1; i >= 0; i--)
                        {
                            UIController.ClosePage(UIController.MainWindowReference.MainTabControl.Items[i] as TabItem);
                        }
                    };

                    thc.miCloseAllButThis.Click += delegate(object sender, RoutedEventArgs e)
                    {
                        TabItem tabItem = (TabItem)((TabHeaderControl)((ContextMenu)((MenuItem)sender).Parent).PlacementTarget).Tag;
                        for (int i = UIController.MainWindowReference.MainTabControl.Items.Count - 1; i >= 0; i--)
                        {
                            if (UIController.MainWindowReference.MainTabControl.Items[i] != tabItem)
                                UIController.ClosePage(UIController.MainWindowReference.MainTabControl.Items[i] as TabItem);
                        }
                    };

                    thc.btnCloseTab.Tag = ti;
                    ti.Header = thc;
                }
                else
                    ti.Header = page.Title;

                //if (page is HomePage)
                //    MainWindowReference.MainTabControl.Items.Insert(0, ti);
                //else
                MainWindowReference.MainTabControl.Items.Add(ti);
                MainWindowReference.MainTabControl.SelectedItem = ti;
                page.Focus();
               
                return page;
            }
            catch (Exception e)
            { DisplayErrorMessage(e); return null; }

        }

        static void LoadLogonPage()
        {
            //UIController.MainWindowReference.RibbonControl.Visibility = Visibility.Collapsed;
            LogOnPage logonPage = new LogOnPage(DAL.UserDB.GetUsers(0).ToList());
            UIController.OpenPage(logonPage, false, "");
        }

        static void ClosePage(TabItem tabItem)
        {
            try
            {
                TabHeaderControl tabHeaderControl = tabItem.Header as TabHeaderControl;
                IDisplayTextBlock displayTextBlock = (tabItem.Content as Frame).Content as IDisplayTextBlock;
                string messageString = displayTextBlock.ConfirmOnClosingText;
                if (messageString == "")
                    messageString = string.Format("Are you sure you want to close page?\n\n'{0}'", tabHeaderControl.tbHeaderTitle.Text);
                if ((displayTextBlock != null && !displayTextBlock.ConfirmOnClosing) ||
                    MessageBox.Show(messageString, "Close Page?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    MainWindowReference.MainTabControl.SelectedItem = tabItem;
                    UIController.BlankPage();
                };
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void CloseActivePage()
        {
            UIController.ClosePage(MainWindowReference.MainTabControl.SelectedItem as TabItem);
        }
           
        private static void BlankPage()
        {
            MainWindowReference.MainTabControl.Items.Remove(MainWindowReference.MainTabControl.SelectedItem);
        }

        internal static void LoadCustomerInfoPage(Customer customer)
        {
            try
            {
                if (customer == null)
                    customer = new Customer() { ObjectState = BOState.New };
                UIController.OpenPage(new CustomerInfoPage(customer), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadProcessDailyEnvelopesPage(DailyEnvelope dailyEnvelope)
        {
            try
            {
                UIController.OpenPage(new ProcessDailyEnvelopePage(dailyEnvelope), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadDriversListPage()
        {
            try
            {
                UIController.OpenPage(new DriversListPage(DAL.DriversDB.GetDrivers(true)), true, ""); ;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadCommisionTypesListPage()
        {
            try
            {
                UIController.OpenPage(new CommisionTypesListPage(DAL.CommisionTypesDB.GetCommisionTypes()), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadPaymentsListPage()
        {
            try
            {
                UIController.OpenPage(new PaymentsListPage(), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadCustomersListPage()
        {
            try
            {
                UIController.OpenPage(new CustomersListPage(new List<Customer>(DAL.CustomerDB.GetCustomers(CustomerFilterOptions.None, string.Empty, DataRequestTypes.LiteData, false))), true);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public static void LoadMedicaidTripsPage()
        {
            try
            {
                // UIController.OpenPage(new CustomersListPage(new List<Customer>(DAL.CustomerDB.GetCustomers(CustomerFilterOptions.None, string.Empty, DataRequestTypes.LiteData))), true);
                UIController.OpenPage(new MedicaidTripsListPage(), true);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        internal static void LoadSetupPage()
        {
            try
            {
                UIController.OpenPage(new SetupPage(true), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        internal static void LoadMedBillingPage()
        {
            try
            {
                UIController.OpenPage(new MedBillingPage(), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        public static DriverExpense LoadAddEditDriverExpenseWindow(int driverExpenseID, ModificationTypes modificationType)
        {
            try
            {
                DriverExpense driveExpenseToUpdate;

                if (modificationType == ModificationTypes.Edit)
                {
                    driveExpenseToUpdate = DAL.DriverExpenseDB.GetDriverExpense(0, driverExpenseID, false, 0).FirstOrDefault();// DAL.DriversDB.GetDrivers(true,driverExpenseID).FirstOrDefault();
                    if (driveExpenseToUpdate == null)
                        return null;

                    else
                    {
                        driveExpenseToUpdate.ObjectState = BOState.Edit;
                        AddEditDriverExpenseWindow adew = new AddEditDriverExpenseWindow(driveExpenseToUpdate);
                        if (adew.ShowDialog().GetValueOrDefault(false))
                        {
                            if (DAL.DriverExpenseDB.EditDriverExpense(driveExpenseToUpdate))
                                return driveExpenseToUpdate;
                            else
                                return null;
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    driveExpenseToUpdate = new DriverExpense() { ObjectState = BOState.New };
                    AddEditDriverExpenseWindow adew = new AddEditDriverExpenseWindow(driveExpenseToUpdate);
                    if (adew.ShowDialog().GetValueOrDefault(false))
                    {
                        if (DAL.DriverExpenseDB.AddDriverExpense(driveExpenseToUpdate))
                            return driveExpenseToUpdate;
                        else
                            return null;
                    }
                    else
                        return null;
                }
            }

            catch (Exception ex)
            {

                DisplayErrorMessage(ex);
                return null;
            }
        }

        public static bool ShowChargeCCWindow(CCOnFile ccOnfile, bool isCreditable, ref int paymentID, ref decimal chargeAmount, ref decimal tripPart)
        {
            bool charged;
            ChargeCardWindow chargeCardWnd = new ChargeCardWindow(ccOnfile);
            chargeCardWnd.AmountToChargeBox.Text = chargeAmount.ToString("G0");
            chargeCardWnd.BookFeeBox.Text = chargeAmount.ToString("G0");
            chargeCardWnd.chkIsPmtCreditable.IsChecked = isCreditable;
            charged = chargeCardWnd.ShowDialog().GetValueOrDefault(false);

            if (charged)
            {
                CCProc.ProcessCC proc = new CCProc.ProcessCC();
                var transResponse = proc.ProcessPayTraceTransaction(                         //test 1
                        ccOnfile.CcInformation,
                        CCProc.CCTransactionTypes.Sale,
                        Convert.ToDouble(chargeCardWnd.AmountToChargeBox.Text),
                        chargeCardWnd.Description_txtBox.Text,
                        UIController.CurrentSession.SessionUser.UserName
                    );

                MessageBox.Show(transResponse.ResponseDescription);                    //till here test 1

                decimal bookFee = 0;
                decimal amtPaid = 0;
                decimal.TryParse(chargeCardWnd.BookFeeBox.Text, out bookFee);
                decimal.TryParse(chargeCardWnd.AmountToChargeBox.Text, out amtPaid);
                if (transResponse.Result.ToLower().Contains("Approved".ToLower()))//if (transResponse.Result == "Approved") //if (true)// //test 2 
                {
                    var pmt = new Payment()
                    {
                        Customer = new Customer() { CustomerID = ccOnfile.CustomerID },
                        DatePaid = DateTime.Today,
                        AmountPaid = (amtPaid - bookFee),
                        BookFee = bookFee,
                        PaymentMethod = PaymentMethods.CreditCard,
                        ReferenceNo = ccOnfile.CcInformation.CCNumber.Substring(ccOnfile.CcInformation.CCNumber.Length - 5), //"", test 3
                        IsCreditable = chargeCardWnd.chkIsPmtCreditable.IsChecked.Value, //need to take from here cause if he uncheck should pick up changes
                        PaymentComments = "CC Comment: " + chargeCardWnd.Description_txtBox.Text
                    };
                    DAL.CustomerPaymentInfoDB.AddPayment(pmt);
                    paymentID = pmt.PaymentID;
                    chargeAmount = Convert.ToDecimal(chargeCardWnd.AmountToChargeBox.Text);
                    tripPart = (amtPaid - bookFee);
                }


                //if(addPayment)
                DAL.CustomerChargeCardDB.AddResponse(new CreditCardResponse() { CCResponse = transResponse, CustomerID = ccOnfile.CustomerID, CcID = ccOnfile.CcID }); //test 4
                                                                                                                                                                       //GetResponses(ccOnfile.CustomerID, ccOnfile.CcID);


            }
            return charged;
        }

        internal static void LoadProcessInvoicesPage()
        {
            try
            {
                UIController.OpenPage(new ProcessInvoicesPage(), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void ShowAddEditMedicaidTripWindow(MedicaidTrip medicaidTrip)
        {
            try
            {
                if (medicaidTrip == null)
                    return;
                AddEditMedicaidTripWindow aemw = new AddEditMedicaidTripWindow(medicaidTrip);
                if (aemw.ShowDialog().GetValueOrDefault(false))
                {
                    if (medicaidTrip.MedicaidTripID == 0)
                    {
                        medicaidTrip.IsManualEntry = true;
                        DAL.MedicaidTripDB.AddMedicaidTrips(new ObservableCollection<MedicaidTrip>() { medicaidTrip });
                    }
                    else
                        DAL.MedicaidTripDB.EditMedicaidTrip(medicaidTrip);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void ShowAddEditFidelisTripWindow(FidelisTrip fidelisTrip)
        {
            try
            {
                if (fidelisTrip == null)
                    return;
                AddEditFidelisTripWindow aefw = new AddEditFidelisTripWindow(fidelisTrip);
                if (aefw.ShowDialog().GetValueOrDefault(false))
                {
                    if (fidelisTrip.FidelisTripID == 0)
                        DAL.FidelisTripDB.AddFidelisTrips(new ObservableCollection<FidelisTrip>() { fidelisTrip });
                    else
                        DAL.FidelisTripDB.EditFidelis(fidelisTrip);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void LoadDispatchingPage(bool remotely,bool isLongDistance)
        {
            try
            {
                DispatchingWindow dw = new DispatchingWindow(remotely,isLongDistance);
                UIController.MainWindowReference.Visibility = Visibility.Hidden;
                dw.Show();
                //UIController.OpenPage(new DispatchingWindow(), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }


        internal static void LoadFidelisImportPage(ObservableCollection<FidelisTrip> fidelisTrips, string fileName)
        {
            try
            {
                UIController.OpenPage(new FidelisTripsImportPage(fidelisTrips, fileName), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void LoadMedicaidTripsPage(ObservableCollection<MedicaidTrip> medicaidTrips)
        {
            try
            {
                UIController.OpenPage(new MedicaidTripsPage(medicaidTrips), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        internal static void LoadScheduledTripsPage()
        {
            try
            {
                UIController.OpenPage(new ScheduledTripsPage(null), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }
        internal static Driver SelectDriver(ObservableCollection<Driver> drivers,
            string windowHeader,
            out string signInLocation,
            bool showSignInLocation = false)
        {
            signInLocation = "";
            try
            {

                SelectDriverWindow sdw = new SelectDriverWindow(drivers, windowHeader, showSignInLocation);
                if (sdw.ShowDialog().GetValueOrDefault(false))
                {
                    if (showSignInLocation)
                        signInLocation = sdw.SignInLocationTextBox.Text;


                    return sdw.SelectedDriver;

                }
                return null;
            }

            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }


        }

        internal static bool GetInput(string header, string description, out string inputText)
        {
            return InputBox.GetInput(header, description, out inputText);
        }
        #endregion

        internal static void CloseProgram(bool p)
        {
            UIController.MainWindowReference.Close();
        }

        internal static ObservableCollection<Customer> GetCustomer(CustomerFilterOptions customerFilterOption, string value, DataRequestTypes dataRequestType, bool invoiceCustomersOnly)
        {
            try
            {
                return DAL.CustomerDB.GetCustomers(customerFilterOption, value, dataRequestType, invoiceCustomersOnly); ;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }

        }

        internal static Customer SelectCustomerFromList(ObservableCollection<Customer> customers, bool invoicedCustomersOnly = true)
        {
            try
            {
                SelectCustomerWindow scw = new SelectCustomerWindow(customers);
                scw.customerSearchCtrl.InvoiceCustomersOnlyCheckBox.IsChecked = invoicedCustomersOnly;
                if (scw.ShowDialog().GetValueOrDefault(false))
                {
                    return scw.SelectedCustomer;
                }

                return null;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }
        }

        internal static CustomerPassCode SelectCustomerPassCodeFromList(ObservableCollection<CustomerPassCode> customerPassCodes)
        {
            try
            {
                SelectCustomerPassCodeWindow scw = new SelectCustomerPassCodeWindow(customerPassCodes);
                if (scw.ShowDialog().GetValueOrDefault(false))
                {
                    return scw.SelectedCustomerPassCode;
                }

                return null;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }
        }

        public static Driver AddEditDriver(int driverID, ModificationTypes modificationType)
        {
            try
            {
                Driver driverToUpdate;
                if (modificationType == ModificationTypes.Edit)
                {
                    driverToUpdate = DAL.DriversDB.GetDrivers(true, driverID, false, true).FirstOrDefault();
                    if (driverToUpdate == null)
                        return null;

                    else
                    {
                        driverToUpdate.ObjectState = BOState.Edit;
                        AddDriverWindow adw = new AddDriverWindow(driverToUpdate);
                        if (adw.ShowDialog().GetValueOrDefault(false))
                        {
                            if (DAL.DriversDB.EditDriver(driverToUpdate))
                                return driverToUpdate;
                            else
                                return null;
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    driverToUpdate = new Driver() { ObjectState = BOState.New };
                    AddDriverWindow adw = new AddDriverWindow(driverToUpdate);
                    if (adw.ShowDialog().GetValueOrDefault(false))
                    {
                        if (DAL.DriversDB.AddDriver(driverToUpdate))
                            return driverToUpdate;
                        else
                            return null;
                    }
                    else
                        return null;
                }
            }

            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }

        }

        public static CommisionType AddEditCommisionType(int commisionTypeID, ModificationTypes modificationType)
        {
            try
            {
                CommisionType commisionTypeToUpdate;
                if (modificationType == ModificationTypes.Edit)
                {
                    commisionTypeToUpdate = DAL.CommisionTypesDB.GetCommisionTypes(commisionTypeID).FirstOrDefault();
                    if (commisionTypeToUpdate == null)
                        return null;

                    else
                    {
                        commisionTypeToUpdate.ObjectState = BOState.Edit;
                        AddCommisionTypeWindow actw = new AddCommisionTypeWindow(commisionTypeToUpdate);
                        if (actw.ShowDialog().GetValueOrDefault(false))
                        {
                            if (DAL.CommisionTypesDB.EditCommisionType(commisionTypeToUpdate))
                                return commisionTypeToUpdate;
                            else
                                return null;
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    commisionTypeToUpdate = new CommisionType() { ObjectState = BOState.New };
                    AddCommisionTypeWindow actw = new AddCommisionTypeWindow(commisionTypeToUpdate);
                    if (actw.ShowDialog().GetValueOrDefault(false))
                    {
                        if (DAL.CommisionTypesDB.AddCommisiontype(commisionTypeToUpdate))
                            return commisionTypeToUpdate;
                        else
                            return null;
                    }
                    else
                        return null;
                }
            }

            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }

        }

        public static byte[] FileToByteArray(string _FileName)
        {
            byte[] _Buffer = null;

            try
            {
                // Open file for reading
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // attach filestream to binary reader
                System.IO.BinaryReader _BinaryReader = new System.IO.BinaryReader(_FileStream);

                // get total byte length of the file
                long _TotalBytes = new System.IO.FileInfo(_FileName).Length;

                // read entire file into buffer
                _Buffer = _BinaryReader.ReadBytes((Int32)_TotalBytes);

                // close file reader
                _FileStream.Close();
                _FileStream.Dispose();
                _BinaryReader.Close();
            }
            catch (Exception _Exception)
            {
                // Error
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }

            return _Buffer;
        }
        public static string ReportFolder
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Reports\\";
            }
        }
        public static string Hipaa837Folder
        {
            get
            {
                return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Hipaa837Folder\\";
            }
        }

        public static void PrintCustomersLastName()
        {
            try
            {
                ObservableCollection<Customer> customers = DAL.CustomerDB.GetCustomerLastName();
                Rpts.XrptLastNames rptLastNames = new Rpts.XrptLastNames();
                rptLastNames.DataSource = customers;
                //XtraReportPreviewModel model = new XtraReportPreviewModel(rptLastNames);
                //DocumentPreviewWindow window = new DocumentPreviewWindow() { Model = model };
                //rptLastNames.CreateDocument(true);
                //window.Show();

                var window = new DocumentPreviewWindow();
                window.PreviewControl.DocumentSource = rptLastNames;
                rptLastNames.CreateDocument();
                window.Show();
            }
	        catch (Exception ex)
	        {
                UIController.DisplayErrorMessage(ex);		
	        }
        }

        public static void PrintInvoice(List<Invoice> invoices)
        {
            try 
	        {	        		
                //Rpts.XrptInvoice rptInvoice = new Rpts.XrptInvoice();
                //rptInvoice.LoadLayout(ReportFolder + "Invoice.Repx");
                
                //rptInvoice.DataSource = invoices;
                //XtraReportPreviewModel model = new XtraReportPreviewModel(rptInvoice);
                //DocumentPreviewWindow window = new DocumentPreviewWindow() { Model = model };
                //rptInvoice.CreateDocument(true);
                //window.Show();
                if (invoices.Count == 0)
                    return;
                foreach (var custInvoice in invoices)
                {

                    if (custInvoice.InvoiceDate == null)
                        return;
                    custInvoice.PaymentsFromThisBillingCycle = DAL.CustomerPaymentInfoDB.GetPayments(custInvoice.InvoicedCustomer.CustomerID, 0, custInvoice.LastInvoiceDate, custInvoice.InvoiceDate,false,true).ToList();
                    custInvoice.PreviousTotalDue = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, custInvoice.LastInvoiceDate.AddDays(-1));
                    
                    /* DateTime dtOver30 = custInvoice.InvoiceDate;
                    dtOver30 = dtOver30.AddDays(-30);
                    custInvoice.TotalDueOver30Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtOver30);
                  DateTime dtover60 = custInvoice.InvoiceDate;//.AddDays(-60);
                    dtover60 = dtover60.AddDays(-60);
                    custInvoice.TotalDueOver60Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtover60);
                    DateTime dtover90 = custInvoice.InvoiceDate;//.AddDays(-90);
                    dtover90 = dtover90.AddDays(-90);
                    custInvoice.TotalDueOver90Days = DAL.InvoiceDB.GetPreviousTotalDue(custInvoice.InvoicedCustomer.CustomerID, dtover90);*/
                }

                Rpts.XrptInvoiceStatmentLook rptInvoice = new Rpts.XrptInvoiceStatmentLook();
                rptInvoice.LoadLayout(ReportFolder + "XrptInvoiceStatmentLook.Repx");

                rptInvoice.DataSource = invoices;
                //XtraReportPreviewModel model = new XtraReportPreviewModel(rptInvoice);
                //DocumentPreviewWindow window = new DocumentPreviewWindow() { Model = model };
                //rptInvoice.CreateDocument(true);
                //window.Show();

                var window = new DocumentPreviewWindow();
                window.PreviewControl.DocumentSource = rptInvoice;
                rptInvoice.CreateDocument();
                window.Show();
            }
	        catch (Exception ex)
	        {
                UIController.DisplayErrorMessage(ex);		
	        }
        }


        public static ObservableCollection<FidelisTrip> ParseFidelisFile(string fidelisFile)
        {
            var fidelisTrips = new ObservableCollection<FidelisTrip>();

            string[] fileRows = fidelisFile.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            int pageRowNo = 0;
            int tripRowNo = 0;

            FidelisTrip fidelisTrip = new FidelisTrip();

            //for (int i = 0; i < fileRows.Length; i++)
            //{
            //    if (fileRows[i].Contains("Transportation Provider"))
            //    {
            //        pageRowNo = 1;
            //        continue;
            //    }

            //    if (pageRowNo < 7)
            //    {
            //        pageRowNo++;
            //        continue;
            //    }

            //    if (pageRowNo == 7)
            //    {
            //        tripRowNo = 1;
            //    }

            //    switch (tripRowNo)
            //    {
            //        case 1:
            //            fidelisTrip = new FidelisTrip();
            //            fidelisTrip.MemberName = fileRows[i];
            //            break;
            //        case 2:
            //            fidelisTrip.MemberAddress = fileRows[i];
            //            break;
            //        case 3:
            //            fidelisTrip.MemberCityStatePostalCode = fileRows[i];
            //            break;
            //        case 4:
            //            fidelisTrip.MemberPhoneNo = fileRows[i];
            //            break;
            //        case 5:
            //            string[] dob = fileRows[i].Trim().Split(new char[]{':'}, StringSplitOptions.RemoveEmptyEntries);
            //            if (dob.Length > 1)                        
            //                fidelisTrip.MemberDOB = Convert.ToDateTime(dob[1].Trim());
            //            break;
            //        case 6:
            //            fidelisTrip.DestinationTitle = fileRows[i];
            //            break;
            //        case 7:
            //            fidelisTrip.DestinationAddress = fileRows[i];
            //            break;
            //        case 8:
            //            fidelisTrip.DestinationCityStatePostalCode = fileRows[i];
            //            break;
            //        case 9:
            //            fidelisTrip.MemberID = fileRows[i];
            //            break;
            //        case 10:
            //            fidelisTrip.TripScheduledOn = Convert.ToDateTime(fileRows[i]);                                               
            //            break;
            //        case 11:
            //            string date;
            //            if (fileRows[i].IndexOf(' ') > 0)
            //            {
            //                int spaceIndex = fileRows[i].IndexOf(' ');
            //                date = fileRows[i].Substring(0, spaceIndex + 1);
            //                //tripDate = Convert.ToDateTime(date);

            //                string comment = fileRows[i].Substring(spaceIndex, fileRows[i].Length - spaceIndex);
            //                fidelisTrip.Comments = comment;
            //            }
            //            else
            //                date = fileRows[i];

            //            fidelisTrip.TripScheduledOn =  Convert.ToDateTime(date + " " + fidelisTrip.TripScheduledOn.TimeOfDay.ToString());
            //            break;
            //        case 12:
            //            fidelisTrip.MemberDOB = Convert.ToDateTime(fileRows[i]);
            //            break;
            //        case 13:
            //        case 14:
            //            break;
            //        case 15:
            //            fidelisTrip.GroupID = fileRows[i];
            //            break;
            //        case 16:
            //            fidelisTrip.MemberGender = fileRows[i][0];
            //            string[] lastRowSplit = fileRows[i + 1].Split(' ');
            //            tripRowNo = 0;
            //            break;
            //        default:
            //            break;
            //}

            foreach (string row in fileRows)
            {
                if (row.Contains("Transportation Provider"))
                {
                    pageRowNo = 1;
                    continue;
                }

                if (pageRowNo < 7)
                {
                    pageRowNo++;
                    continue;
                }

                if (pageRowNo == 7)
                {
                    tripRowNo = 1;
                }

                if (row.Contains("Page"))
                {
                    if (tripRowNo == 17)
                        fidelisTrips.Add(fidelisTrip);
                    continue;
                }

                if (row.Trim() == "FCNY0001")
                    tripRowNo = 15;

                if (row.Contains("\f"))
                    continue;

                //if(tripRowNo == 1 && (row.Contains("#") || row.Contains("Unit") || row.Contains("Apt")))                
                //    continue;

                if (tripRowNo == 1)
                {

                    if (UIController.HasDigit(row))
                        continue;
                }

                if (row.Trim().Length == 9 && UIController.IsDigits(row.Trim()))
                    tripRowNo = 9;




                switch (tripRowNo)
                {
                    case 1:
                        fidelisTrip = new FidelisTrip();
                        fidelisTrip.MemberName = row;
                        break;
                    case 2:
                        fidelisTrip.MemberAddress = row;
                        break;
                    case 3:
                        fidelisTrip.MemberCityStatePostalCode = row;
                        break;
                    case 4:
                        if (UIController.HasDigit(row))
                        {
                            fidelisTrip.MemberPhoneNo = row;
                            break;
                        }
                        else
                        {
                            tripRowNo++;
                            goto case 5;
                        }

                    case 5:
                        string[] dob = row.Trim().Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        if (dob.Length > 1)
                        {
                            DateTime dt;
                            if (DateTime.TryParse(dob[1].Trim(), out dt))
                                fidelisTrip.MemberDOB = dt;
                        }
                        break;
                    case 6:
                        fidelisTrip.DestinationTitle = row;
                        break;
                    case 7:
                        fidelisTrip.DestinationAddress = row;
                        break;
                    case 8:
                        fidelisTrip.DestinationCityStatePostalCode = row;
                        break;
                    case 9:
                        fidelisTrip.MemberID = row;
                        break;
                    case 10:
                        DateTime dt2;
                        if (DateTime.TryParse(row, out dt2))
                            fidelisTrip.TripScheduledOn = dt2;
                        break;
                    case 11:

                        string date;
                        if (row.IndexOf(' ') > 0)
                        {
                            int spaceIndex = row.IndexOf(' ');
                            date = row.Substring(0, spaceIndex + 1);
                            //tripDate = Convert.ToDateTime(date);

                            string comment = row.Substring(spaceIndex, row.Length - spaceIndex);
                            string[] ridersAndUserName = comment.Trim().Split(' ');
                            int noOfRiders;
                            if (int.TryParse(ridersAndUserName[0].Trim(), out noOfRiders))
                                fidelisTrip.NoOfRiders = noOfRiders;
                            fidelisTrip.UserName = ridersAndUserName[ridersAndUserName.Length - 1].Trim();
                            string trimedComment = comment.Trim();
                            fidelisTrip.Comments = comment.Substring(trimedComment.IndexOf(' ') + 1, trimedComment.LastIndexOf(' ') + 1 - trimedComment.IndexOf(' ')).Trim();
                        }
                        else
                            date = row;

                        DateTime dt3;

                        if (fidelisTrip.TripScheduledOn.HasValue)
                            if (DateTime.TryParse(date + " " + fidelisTrip.TripScheduledOn.Value.TimeOfDay.ToString(), out dt3))
                                fidelisTrip.TripScheduledOn = dt3;
                        //fidelisTrip.TripScheduledOn =  Convert.ToDateTime(date + " " + fidelisTrip.TripScheduledOn.TimeOfDay.ToString());
                        break;
                    case 12:
                        DateTime dt4;

                        if (DateTime.TryParse(row, out dt4))
                            fidelisTrip.MemberDOB = dt4;
                        break;
                    case 13:
                    case 14:
                        break;
                    case 15:
                        //if (row.Trim() != "FCNY0001")
                        //    tripRowNo--;
                        //else
                        fidelisTrip.GroupID = row;
                        break;
                    case 16:
                        if (row[0] != 'M' && row[0] != 'F')
                        {
                            tripRowNo--;
                            break;
                        }
                        fidelisTrip.MemberGender = row[0];
                        break;
                    case 17:
                        if (row.Contains("Unit") || UIController.HasDigit(row))
                        {
                            fidelisTrip.MemberAddress += " " + row;
                            fidelisTrips.Add(fidelisTrip);
                            tripRowNo = 0;
                        }
                        else
                        {
                            fidelisTrips.Add(fidelisTrip);
                            fidelisTrip = new FidelisTrip() { MemberName = row };
                            tripRowNo = 1;
                        }
                        break;
                    default:
                        break;
                }



                tripRowNo++;

                pageRowNo++;
            }

            return fidelisTrips;
        }

        static bool HasDigit(string s)
        {
            char[] chars = s.ToCharArray();
            foreach (char c in chars)
            {
                if (char.IsDigit(c))
                {
                    return true;
                }
            }

            return false;
        }

        static bool IsDigits(string s)
        {
            char[] chars = s.ToCharArray();
            foreach (char c in chars)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }

            return true;
        }

        internal static void ImportFidelisTrips()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Fidelis Text File|*.txt";
                if (ofd.ShowDialog().GetValueOrDefault(false))
                {
                    System.IO.StreamReader myFile = new System.IO.StreamReader(ofd.FileName);
                    string importFile = myFile.ReadToEnd();
                    myFile.Close();

                    var fidelisTrips = UIController.ParseFidelisFile(importFile);
                    UIController.LoadFidelisImportPage(fidelisTrips, ofd.FileName);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static ScheduledTrip SelectScheduledTrips(DateTime tripDate, TripAreaTypes tripAreaType)
        {
            try
            {
                var scheduledTrips = DAL.ScheduledTripDB.GetScheduledTrips(0, tripDate, null, null, true, tripAreaType);
                SelectSceduledTripWindow sftw = new SelectSceduledTripWindow(scheduledTrips);
                if (sftw.ShowDialog().GetValueOrDefault(false))
                    return sftw.SelectedScheduledTrip;
                else
                    return null;
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); return null; }
        }
        internal static void ShowAssignDriversToScheduledTrips(DateTime tripDate)
        {
            try
            {
                AssignDriversToScheduledTrips adtst = new AssignDriversToScheduledTrips();
                adtst.ShowDialog();//.GetValueOrDefault(false);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static string[] GetMedicaidTripsFromWeb()
        {
            MedicaidLoginInfo medicaidLoginInfo;
            medicaidLoginInfo = DAL.MedicaidTripDB.GetMedicaidLogInInfo().FirstOrDefault();

            try
            {
                WatiN.Core.IE ie = new WatiN.Core.IE();
                // ie.TextField(WatiN.Core.Find.ByName("User_Name")).TypeText("slieberman");
                //  ie.TextField(WatiN.Core.Find.ByName("Password")).TypeText("ping3359");



                ie.GoTo("https://www.medanswering.com/login.taf");

                ie.WaitForComplete();

                ie.TextField(WatiN.Core.Find.ByName("User_Name")).TypeText(medicaidLoginInfo.MedicaidLogInUserNmae);
                ie.TextField(WatiN.Core.Find.ByName("Password")).TypeText(medicaidLoginInfo.MedicaidLogInPassword);
                ie.Button(WatiN.Core.Find.ByValue("Login")).Click();

                ie.GoTo("https://www.medanswering.com/adminvendorexport.taf?&");
                ie.SelectList(WatiN.Core.Find.ByName("Medicaid_County_Number")).Option(WatiN.Core.Find.ByValue("33")).Select();

                ie.TextField(WatiN.Core.Find.ByName("Service_Date_Start")).TypeText(DateTime.Today.ToShortDateString());
                ie.TextField(WatiN.Core.Find.ByName("Service_Date_End")).TypeText(DateTime.Today.ToShortDateString());
                ie.Button(WatiN.Core.Find.ByValue("Export Trips")).Click();

                ie.Element(WatiN.Core.Find.ByText("Trip Export File")).Click();

                var s = ie.Text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                ie.Close();

                if (s.Length <= 1)
                {
                    MessageBox.Show("There no trips to download");
                    return null;
                }
                else
                    return s;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }

        }

        internal static ObservableCollection<MedicaidTrip> ParseMedicaidTrips(string[] trips)
        {
            var medicalTrips = new ObservableCollection<MedicaidTrip>();
            if (trips != null && trips.Length > 1)
            {
                int tripNo = 0;

                foreach (string trip in trips)
                {
                    if (tripNo == 0)
                    {
                        tripNo++;
                        continue;
                    }

                    string[] tripFields = trip.Split('\t');
                    bool haveNotModField = tripFields.Count() == 35;
                    BOL.MedicaidTrip medicaidTrip = new BOL.MedicaidTrip();
                    int fieldNo = 1;
                    foreach (string field in tripFields)
                    {
                        switch (fieldNo)
                        {
                            case 1:
                                medicaidTrip.ExportID = Convert.ToInt32(field);
                                break;
                            case 2:
                                medicaidTrip.RecordNo = Convert.ToInt32(field);
                                break;
                            case 3:
                                medicaidTrip.InvoiceNo = Convert.ToInt32(field);
                                break;
                            case 4:
                                medicaidTrip.RecordType = field;
                                break;
                            case 5:
                                medicaidTrip.FirstName = field;
                                break;
                            case 6:
                                medicaidTrip.MiddleInitial = field;
                                break;
                            case 7:
                                medicaidTrip.LastName = field;
                                break;
                            case 8:
                                medicaidTrip.CIN = field;
                                break;
                            case 9:
                                medicaidTrip.PhoneNo = field;
                                break;
                            case 10:
                                medicaidTrip.BirthDate = field;
                                break;
                            case 11:
                                medicaidTrip.MedicalProvider = field;
                                break;
                            case 12:
                                medicaidTrip.ProviderID = field;
                                break;
                            case 13:
                                medicaidTrip.TransportCompany = field;
                                break;
                            case 14:
                                medicaidTrip.TransportType = field;
                                break;
                            case 15:
                                medicaidTrip.ProcedureCode = field;
                                break;
                            case 16:
                                if (haveNotModField)
                                {
                                    fieldNo++;
                                    goto case 17;
                                }
                                medicaidTrip.Modifier = field;
                                break;
                            case 17:

                                medicaidTrip.ServiceStarts = Convert.ToDateTime(field);
                                break;
                            case 18:
                                medicaidTrip.ServiceEnds = Convert.ToDateTime(field);
                                break;
                            case 19:
                                medicaidTrip.StandingOrder = field != "No";
                                break;
                            case 20:

                                medicaidTrip.TripsApproved = Convert.ToInt32(field);
                                break;
                            case 21:

                                medicaidTrip.DaysApproved = Convert.ToInt32(field);
                                break;
                            case 22:

                                medicaidTrip.WheelChair = field;
                                break;
                            case 23:

                                DateTime dt1;
                                if (DateTime.TryParse(field, out dt1))
                                    medicaidTrip.PickUpDate = dt1;
                                break;
                            case 24:

                                DateTime dt2;
                                string time1 = field;

                                if (field.Length == 3)
                                    time1 = "0" + field;

                                if (time1.Length == 4)
                                    time1 = time1.Insert(2, ":");

                                //string startTime = string.Format("{0:hh:mm}", field); 
                                if (DateTime.TryParse(time1, out dt2))
                                {
                                    medicaidTrip.PickUpTime = dt2.TimeOfDay;
                                    //medicaidTrip.PickUpTime = Convert.ToDateTime(medicaidTrip.PickupDate.Value.ToShortDateString() + " " + medicaidTrip.PickUpTime.Value.ToShortTimeString());
                                }
                                break;
                            case 25:

                                medicaidTrip.PickUpAddress = field;
                                break;
                            case 26:

                                medicaidTrip.PickUpCity = field;
                                break;
                            case 27:

                                medicaidTrip.PickupState = field;
                                break;
                            case 28:

                                medicaidTrip.PickUpZip = field;
                                break;

                            case 29:

                                DateTime dt3;
                                if (DateTime.TryParse(field, out dt3))
                                    medicaidTrip.DropOffDate = dt3;
                                break;
                            case 30:

                                DateTime dt4;

                                string time2 = field;

                                if (field.Length == 3)
                                    time2 = "0" + field;

                                if (time2.Length == 4)
                                    time2 = time2.Insert(2, ":");

                                //string startTime = string.Format("{0:hh:mm}", field); 
                                if (DateTime.TryParse(time2, out dt4))
                                {
                                    medicaidTrip.DropOffTime = dt4.TimeOfDay;
                                    //medicaidTrip.DropOffTime = Convert.ToDateTime(medicaidTrip.DropOffDate.Value.ToShortDateString() + " " + medicaidTrip.DropOffTime.Value.ToShortTimeString());
                                }
                                break;
                            case 31:

                                medicaidTrip.DropOffAddress = field;
                                break;
                            case 32:

                                medicaidTrip.DropOffCity = field;
                                break;
                            case 33:

                                medicaidTrip.DropOffState = field;
                                break;
                            case 34:

                                medicaidTrip.DropOffZip = field;
                                break;
                            case 35:

                                medicaidTrip.Instructions = field;
                                break;
                            case 36:
                                medicaidTrip.SecondaryService = field;
                                break;
                            default:
                                break;
                        }

                        fieldNo++;
                    }
                    medicalTrips.Add(medicaidTrip);
                    tripNo++;
                }
            }

            return medicalTrips;
        }

        internal static string[] GetMedicaidTripsFromFile()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Medicaid Text File|*.txt";
                if (ofd.ShowDialog().GetValueOrDefault(false))
                {
                    System.IO.StreamReader myFile = new System.IO.StreamReader(ofd.FileName);
                    string importFile = myFile.ReadToEnd();

                    myFile.Close();

                    return importFile.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else return null;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }
        }

        internal static void ImportMedicaidTrips(bool getFromWeb)
        {
            try
            {

                string[] medicaidTrips;
                if (getFromWeb)
                    medicaidTrips = UIController.GetMedicaidTripsFromWeb();
                else
                    medicaidTrips = UIController.GetMedicaidTripsFromFile();
                if (medicaidTrips != null)
                {
                    var medicaidTripsParsed = UIController.ParseMedicaidTrips(medicaidTrips);
                    if (medicaidTripsParsed.Count > 0 && !DAL.MedicaidTripDB.FileWasImported(medicaidTripsParsed[0].ExportID))
                    {
                        if (DAL.MedicaidTripDB.AddMedicaidTrips(medicaidTripsParsed))
                        {
                            var medicaidTripsFromDB = DAL.MedicaidTripDB.GetMedicaidTrips(medicaidTripsParsed[0].ExportID, 0, DateTime.Today);
                            if (medicaidTripsFromDB.Count > 0)
                                UIController.LoadMedicaidTripsPage(medicaidTripsFromDB);

                        }
                    }
                    else
                        MessageBox.Show("File was already imported", "Already imported", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        #region ImportMedicaidPAN

        internal static void ImportMedicaidPAN()
        {
            try
            {

                string[] medicaidPAN;

                medicaidPAN = UIController.GetMedicaidPANFromWeb(null, null);

                if (medicaidPAN != null)
                {
                    var medicaidPANParsed = UIController.ParseMedicaidPAN(medicaidPAN);
                    if (medicaidPANParsed.Count > 0)
                    {
                        if (DAL.MedicaidTripPanDB.AddMedicaidTripsPan(medicaidPANParsed))
                        {
                            MessageBox.Show("Import Completed", "Completed", MessageBoxButton.OK);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static string[] GetMedicaidPANFromWeb(DateTime? fromDate, DateTime? toDate)
        {
            MedicaidLoginInfo medicaidLoginInfo;
            medicaidLoginInfo = DAL.MedicaidTripDB.GetMedicaidLogInInfo().FirstOrDefault();
            //DateTime minDate = DAL.MedicaidTripPanDB.GetMinDate();
            DateTime minDate = Convert.ToDateTime("12/01/2012");
            int days = DateTime.Today.Subtract(minDate).Days;

            if (days > 30)
            {
                minDate = DateTime.Today.AddDays(-30);
            }
            //DateTime minDate = Convert.ToDateTime("12/01/2012");
            //DateTime tillDate = minDate.AddMonths(2);
            DateTime tillDate = DateTime.Today;

            if (fromDate != null)
                minDate = fromDate.Value;
            if (toDate != null)
                tillDate = toDate.Value;

            try
            {
                var ss = new List<string>();
                WatiN.Core.Settings.WaitForCompleteTimeOut = 10000;
                WatiN.Core.IE ie = new WatiN.Core.IE("https://www.medanswering.com/login.taf");
                ie.WaitForComplete();

                // ie.TextField(WatiN.Core.Find.ByName("User_Name")).TypeText("slieberman");
                //  ie.TextField(WatiN.Core.Find.ByName("Password")).TypeText("ping3359");
                ie.TextField(WatiN.Core.Find.ByName("User_Name")).TypeText(medicaidLoginInfo.MedicaidLogInUserNmae);
                ie.TextField(WatiN.Core.Find.ByName("Password")).TypeText(medicaidLoginInfo.MedicaidLogInPassword);
                ie.Button(WatiN.Core.Find.ByValue("Login")).Click();

                //for (int i = 0; i < 50; i++)
                //{
                try
                {
                    ie.GoTo("https://www.medanswering.com/admintrips.taf?&");
                    ie.SelectList(WatiN.Core.Find.ByName("PA_Submission_Result")).Option(WatiN.Core.Find.ByValue("Accepted")).Select();
                    //DateTime fromDate = DateTime.Today.AddDays(-10.0);

                    ie.TextField(WatiN.Core.Find.ByName("Start_Effective_Date")).TypeText(minDate.ToShortDateString());
                    ie.TextField(WatiN.Core.Find.ByName("End_Effective_Date")).TypeText(tillDate.ToShortDateString());
                    ie.Button(WatiN.Core.Find.ByValue("Find Trips")).Click();

                    ie.Element(WatiN.Core.Find.ByText("Export Roster")).Click();
                    ie.WaitForComplete();
                    ie.Element(WatiN.Core.Find.ByText("Download Roster Export")).Click();
                    ie.WaitForComplete();

                    var s = ie.Text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    ss.AddRange(s.Skip(1).ToList()); //I want to skip the header of the text file

                    //ie.Close();

                    /*minDate = tillDate;
                    tillDate = tillDate.AddMonths(2);

                    //for situations where its left small amount of days and will return empty next time and crash
                    if (DateTime.Today.Subtract(tillDate).Days < 30)
                    {
                        tillDate = DateTime.Today;
                        i = 49; //so its last chance to loop
                    }

                    //wana make sure the till date isnt greater then today
                    if (tillDate > DateTime.Today)
                    {
                        tillDate = DateTime.Today;
                        i = 49; //so its last chance to loop
                    }*/
                }
                catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
                //}

                //ie.Close();

                if (ss.Count <= 1)
                {
                    MessageBox.Show("There no trips to download");
                    return null;
                }
                else
                    return ss.ToArray<string>();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }

        }

        internal static ObservableCollection<MedicaidTripPan> ParseMedicaidPAN(string[] tripsPAN)
        {
            var medicalTripsPAN = new ObservableCollection<MedicaidTripPan>();
            if (tripsPAN != null && tripsPAN.Length > 1)
            {
                int tripNo = 0;

                foreach (string trip in tripsPAN)
                {
                    /*if (tripNo == 0)
                    {
                        tripNo++;
                        continue;
                    }
                     not needed anymore cause did skip in the GetMedicaidPANFromWeb() method*/

                    string[] tripFields = trip.Split('\t');
                    BOL.MedicaidTripPan medicaidTrip = new BOL.MedicaidTripPan();

                    int fieldNo = 0;
                    foreach (string field in tripFields)
                    {
                        switch (fieldNo)
                        {
                            case 1:
                                medicaidTrip.RecipientID = field;
                                break;
                            case 2:
                                medicaidTrip.RecipientName = field;
                                break;
                            case 3:
                                medicaidTrip.YOB = field;
                                break;
                            case 4:
                                medicaidTrip.Sex = field;
                                break;
                            case 5:
                                medicaidTrip.InvoiceNumber = field;
                                break;
                            case 6:
                                medicaidTrip.PriorApprovalNumber = field.Substring(6);
                                break;
                            case 7:
                                medicaidTrip.ItemCode = field;
                                break;
                            case 9:
                                DateTime dt1;
                                if (DateTime.TryParse(field, out dt1))
                                    medicaidTrip.ServiceStarts = dt1;
                                break;
                            case 10:
                                DateTime dt2;
                                if (DateTime.TryParse(field, out dt2))
                                    medicaidTrip.ServiceEnds = dt2;
                                break;
                            case 11:
                                DateTime dt3;
                                if (DateTime.TryParse(field, out dt3))
                                    medicaidTrip.ApprovedTo = dt3;
                                break;
                            case 12:
                                medicaidTrip.OrderingProvider = field;
                                break;
                            case 13:
                                decimal amt;
                                if (decimal.TryParse(field, out amt))
                                    medicaidTrip.Amt = amt;
                                break;
                            case 14:
                                decimal qty;
                                if (decimal.TryParse(field, out qty))
                                    medicaidTrip.Qty = qty;
                                break;
                            case 15:
                                int days;
                                if (int.TryParse(field, out days))
                                    medicaidTrip.DaysTimes = days;
                                break;
                            case 16:
                                medicaidTrip.CDA = field;
                                break;
                            default:
                                break;
                        }

                        fieldNo++;
                    }
                    medicalTripsPAN.Add(medicaidTrip);
                    tripNo++;
                }
            }

            return medicalTripsPAN;
        }
        #endregion

        #region SubmitPAN

        internal static void SubmitPAN(bool through_hipaa837 = false, bool adjustment = true)
        {
            try
            {
                string password;
                if (!InputBox.GetInput("Password", "Enter Password", out password))
                    return;

                if ((password != "5763088" && !adjustment) || (password != "5763089" && adjustment))
                {
                    MessageBox.Show("You fool!", "Wrong password", MessageBoxButton.OK, MessageBoxImage.Warning);

                    return;
                }

                bool billedFirstClaim = false;
                MedicaidLoginInfo medicaidLoginInfo;
                medicaidLoginInfo = DAL.MedicaidTripDB.GeteMedNYLogInInfo().FirstOrDefault();
                ObservableCollection<MedicaidTrip> medicaidTripsForSubmit;
                //the main is the last parameter when for submit true he returens right after
                medicaidTripsForSubmit = DAL.MedicaidTripDB.GetMedicaidTrips(0, 0, DateTime.Now, true, true);
                ObservableCollection<MedicaidTripPan> medicaidTripsPan;
                medicaidTripsPan = DAL.MedicaidTripPanDB.GetMedicaidTripPan(0);
                int invoice = 0;
                DateTime? serviceStarts = new DateTime();
                if (!through_hipaa837)
                {

                    WatiN.Core.Settings.WaitForCompleteTimeOut = 10000;
                    WatiN.Core.IE ie = new WatiN.Core.IE("https://www.emedny.org/epaces/");
                    ie.TextField(WatiN.Core.Find.ByName("Username")).TypeText(medicaidLoginInfo.MedicaidLogInUserNmae);
                    ie.TextField(WatiN.Core.Find.ByName("Password")).TypeText(medicaidLoginInfo.MedicaidLogInPassword);
                    ie.CheckBox(WatiN.Core.Find.ById("chkbxAgree")).Click();
                    ie.Image(WatiN.Core.Find.ById("btnAgreeLogin")).Click();
                    Sleep(2);
                    foreach (MedicaidTrip mt in medicaidTripsForSubmit)
                    {
                        if (mt.InvoiceNo == invoice && mt.ServiceStarts == serviceStarts)
                        { continue; }
                        invoice = mt.InvoiceNo;
                        serviceStarts = mt.ServiceStarts;
                        if (billedFirstClaim == false)
                        {
                            //first time we need to press the button from the menu on upper left side, for the rest we need to press on bottom
                            ie.Element(WatiN.Core.Find.ById("ctl00_Menu1_linkNewClaim")).Click();
                            billedFirstClaim = true;
                        }
                        else
                            ie.Element(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_hrefEnterAnotherClaim")).Click();

                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptPatientAccntNum")).TypeText(mt.MedicaidTripID.ToString());
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptLocationCode")).TypeText("003");
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptClientNum")).TypeText(mt.CIN);
                        Sleep(2);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnGoClientID")).Click();
                        Sleep(1);
                        ie.SelectList(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$epddClaimType")).Option(WatiN.Core.Find.ByValue("R")).Select();
                        Sleep(2);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnNext")).Click();

                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eppuPlaceOfService")).TypeText("99");
                        ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eprblProvSigOnFile_0")).Click();
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eppuAssignementBenefits")).TypeText("Y");
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eppuReleaseOfInfo")).TypeText("Y");
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eppuAcceptMedAss")).TypeText("C");
                        ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eppuSigSource_0")).Click();
                        ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eppuExemptCopay_0")).Click();
                        ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eppuPatientPregnant_1")).Click();
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptPriorAuthOrRef")).TypeText(mt.PriorApprovalNumber);
                        Sleep(5);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnNext")).Click();
                        Sleep(2);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnNext")).Click();
                        //since 10/1/15
                        //var newRuleDate = new DateTime(2015, 10, 1);
                        //if (mt.PickUpDate >= newRuleDate)
                        //{
                        ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eprbICD_1")).Click();
                        ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptDiag1")).TypeText("R69");
                        //}
                        //else
                        //{
                        //    ie.RadioButton(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_eprbICD_0")).Click();
                        //    ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$eptDiag1")).TypeText("7999");
                        //}

                        Sleep(2);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnNext")).Click();
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnNext")).Click();

                        string medicaidTripInvNo = mt.InvoiceNo < 100000000 ? mt.InvoiceNo.ToString().Insert(0, "0") : mt.InvoiceNo.ToString();
                        var mtp = from mdtp in medicaidTripsPan
                                  where mdtp.InvoiceNumber == medicaidTripInvNo
                                  select mdtp;

                        int i = 0;

                        foreach (MedicaidTripPan mdt in mtp)
                        {
                            i++;
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$epdDOSFrom")).TypeText(mdt.ServiceStarts.Value.ToShortDateString());
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$eptHCPCS")).TypeText(mdt.ItemCode);
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$eptModifier1")).TypeText(mdt.Mod);
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$eptChargeAmount")).TypeText(mdt.Amt.ToString());
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$eptServiceCount")).TypeText(mdt.Qty.ToString());
                            ie.TextField(WatiN.Core.Find.ByName("ctl00$ContentPlaceHolder1$ServiceLine" + i + "$eptDXPtr1")).TypeText("1");
                        }
                        Sleep(10);
                        ie.Image(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_btnFinish")).Click();
                        ie.Element(WatiN.Core.Find.ById("ctl00_ContentPlaceHolder1_hrefSubmitRTClaim")).Click();
                        //ctl00_ContentPlaceHolder1_hrefSubmitRTClaim
                        DAL.MedicaidTripDB.EditMedicaidTripForSubmit(mt);
                        Sleep(3);
                    }

                    ie.Close();
                }
                else
                {
                    if (medicaidTripsForSubmit.Count <= 0)
                    {
                        MessageBox.Show("Nothing to bill", "Not Billed", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    var _companyInfo = new Hipaa837.CompanyInformation()
                    {
                        CompanyName = "Liebermans Car Service Inc",
                        CompanyAddress = "48 Bakertown Rd Unit 506",
                        CompanyCity = "Monroe",
                        CompanyState = "NY",
                        CompanyZip = "10950",
                        CompanyPhoneNo = "8457835000",
                        ContactName = "Simon Lieberman",
                        ETIN = "03142096",
                        MedicaidProviderNo = "ACKA",
                        TaxId = "261621208"
                    };

                    var _fileInfo = new Hipaa837.FileInformation()
                    {
                        FileDate = DateTime.Now,
                        FileUniqueId = Guid.NewGuid().GetHashCode()

                    };
                    var _837 = new Hipaa837.Hipaa837(_companyInfo, _fileInfo);

                    foreach (MedicaidTrip mt in medicaidTripsForSubmit)
                    {
                        if (mt.InvoiceNo == invoice && mt.ServiceStarts == serviceStarts)
                        { continue; }
                        invoice = mt.InvoiceNo;
                        serviceStarts = mt.ServiceStarts;

                        string medicaidTripInvNo = mt.InvoiceNo < 100000000 ? mt.InvoiceNo.ToString().Insert(0, "0") : mt.InvoiceNo.ToString();
                        var mtp = from mdtp in medicaidTripsPan
                                  where mdtp.InvoiceNumber == medicaidTripInvNo
                                  select mdtp;





                        foreach (MedicaidTripPan mdt in mtp)
                        {
                            var _claimInfo = new Hipaa837.ClaimInformation()
                            {
                                ClaimUniqueId = mdt.PanID,
                                PatientFirstName = mt.FirstName,
                                PatientLastName = mt.LastName,
                                MedicaidNo = mdt.RecipientID,
                                PatientAddress = "48 Bakertown Rd Unit 506",
                                PatientCity = "Monroe",
                                PatientState = "NY",
                                PatientZip = "10950",

                                Gender = mdt.Sex,
                                DateOfBirth = Convert.ToDateTime(mt.BirthDate),
                                InvoiceNo = mt.InvoiceNo.ToString(),
                                DateOfService = mdt.ServiceStarts.Value,

                                PriorAuthNo = mt.PriorApprovalNumber,
                                HasDelayReason = false,
                                DelayReasonCode = 0,
                                AdjustmentCode = 0,
                                Amount = Convert.ToDouble(mdt.Amt),
                                BillingCode = mdt.ItemCode,
                                Units = Convert.ToDouble(mdt.Qty),
                                Modifier = mdt.Mod,
                                RemittanceTCN = "0"
                            };

                            if (adjustment)
                            {
                                _claimInfo.AdjustmentCode = 7;
                                _claimInfo.RemittanceTCN = DAL.MedicaidRemittanceDB.GetRemittanceTCN(mdt.InvoiceNumber, mdt.Amt);

                            }

                            _837.AddClaim(_claimInfo);
                        }


                    }

                    _fileInfo.FileUniqueId = _837._claims[0].ClaimUniqueId;

                    var fileName = string.Format("{0}Hipaa 837 File {1} {2}.txt", UIController.Hipaa837Folder, _fileInfo.FileUniqueId, DateTime.Now.ToString("MM-dd-yy H-mm-ss"));

                    _837.GenerateFile(fileName);

                    using (DAL.MedicaidTripDB db = new DAL.MedicaidTripDB(null, null))
                    {
                        db.EditListForSubmit(medicaidTripsForSubmit.ToList());
                    }

                    MessageBox.Show("The Hipaa file was created in the folowing folder\n\n" + fileName, "Hipaa File Created", MessageBoxButton.OK, MessageBoxImage.Information);

                }

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                // return null;
            }

        }

        internal static void Sleep(int seconds)
        {
            System.Threading.Thread.Sleep(seconds * 1000);
        }
        #endregion

        internal static void ShowReportViewer(Telerik.Reporting.Report report)
        {
            ReportViewerWindow rvw = new ReportViewerWindow();
            try
            {
                //rvw.reportViewer1.Report = report;//dosent work for new version
                Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource() { ReportDocument = report };

                rvw.reportViewer1.ReportSource = instanceReportSource1;
                rvw.Show();
            }
            catch (Exception ex)
            {
                DisplayErrorMessage(ex);
            }
        }

        internal static void LoadReportDesigner()
        {
            try
            {
                Rpts.ReportDesigner.ShowReportDesigner();
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static void LoadProcessDriverPayment(ObservableCollection<DriverPayment> driverPayment)
        {
            try
            {
                UIController.OpenPage(new ProcessDriverPaymentPage(), true, "");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }


        internal static void LoadAllTripsPage()
        {
            try
            {
                UIController.OpenPage(new TripsListPage(), true, "");
                
            }
            catch (Exception ex)
            {

                DisplayErrorMessage(ex);
            }
        }


        internal static void LoadDriverPaymentStub(ObservableCollection<DriverPayment> driverPayment)
        {
            RptDriverPaymentStub rpt = new RptDriverPaymentStub(driverPayment);
            UIController.ShowReportViewer(rpt);

        }

        internal static void LoadDriverPaymentListPage()
        {
            string password;
            if (!InputBox.GetInput("Password", "Enter Password", out password))
                return;

            if (password != "5763088")
            {
                MessageBox.Show("You fool!", "Wrong password", MessageBoxButton.OK, MessageBoxImage.Warning);

                return;
            }

            UIController.OpenPage(new DriverPaymentsListPage(), true, "");
            DriverPayment dd = new DriverPayment();

        }
        internal static void DriverPaymentwithFilterPage()
        {
            string password;
            if (!InputBox.GetInput("Password", "Enter Password", out password))
                return;

            if (password != "5763088")
            {
                MessageBox.Show("You fool!", "Wrong password", MessageBoxButton.OK, MessageBoxImage.Warning);

                return;
            }

            UIController.OpenPage(new DriverTotalPaymentPage(), true, "");
            DriverPayment dd = new DriverPayment();
        }
        public enum ExportFormat
        {
            Excel, Csv, Html
        }
        public static void ExportTelerikGrid(Telerik.Windows.Controls.RadGridView radGridView, ExportFormat exportFotmat)
        {
            try
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                switch (exportFotmat)
                {
                    case ExportFormat.Excel:
                        saveFile.DefaultExt = ".xls";
                        break;
                    case ExportFormat.Csv:
                        saveFile.DefaultExt = ".txt";
                        break;
                    case ExportFormat.Html:
                        saveFile.DefaultExt = ".html";
                        break;
                    default:
                        break;
                }

                saveFile.Filter = "Excel (*.xls)|*.xls|Comma Delimited|*.scv|HTML|*.html";

                if (saveFile.ShowDialog().Value)
                {
                    Telerik.Windows.Controls.ExportFormat telerikExportFormat = Telerik.Windows.Controls.ExportFormat.Text;
                    string content = string.Empty;

                    var extension = System.IO.Path.GetExtension(saveFile.FileName);
                    if (extension == ".xls")
                        telerikExportFormat = Telerik.Windows.Controls.ExportFormat.ExcelML;
                    else if (extension == ".csv")
                        telerikExportFormat = Telerik.Windows.Controls.ExportFormat.Csv;
                    else if (extension == ".html")
                        telerikExportFormat = Telerik.Windows.Controls.ExportFormat.Html;



                    string path = saveFile.FileName;

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }

                    using (Stream fs = saveFile.OpenFile())// File.Create(path))
                    {
                        radGridView.Export(fs,
                       new GridViewExportOptions()
                       {
                           Format = telerikExportFormat,
                           Encoding = System.Text.Encoding.UTF8,
                           ShowColumnHeaders = true,
                           ShowColumnFooters = true,
                           ShowGroupFooters = false,
                       });
                        //Byte[] info = Encoding.Default.GetBytes(content);
                        //fs.Write(info, 0, info.Length);
                    }

                    System.Diagnostics.Process.Start(path);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        internal static void LoadDailyEnvelopePage()
        {
            try
            {
                string password;
                if (!InputBox.GetInput("Password", "Enter Password", out password))
                    return;

                if (password != "5763088")
                {
                    MessageBox.Show("You fool!", "Wrong password", MessageBoxButton.OK, MessageBoxImage.Warning);

                    return;
                }

                UIController.OpenPage(new DailyEnvelopesListPage(), true, "");
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }


        internal static void SendEmailMessege(string Subject, string MessegeDetails)
        {
            try
            {
                UIController.MainWindowReference.Cursor = Cursors.Wait;
                MailMessage mm = new MailMessage("Shloma@LiebermansCarService.com", "Voicemail@Liebermanscarservice.com", Subject, MessegeDetails);//"Voicemail@Liebermanscarservice.com", Subject, MessegeDetails);
                SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
                emailClient.Credentials = new System.Net.NetworkCredential("Shloma@LiebermansCarService.com", "?Blue#2835!");
                emailClient.Port = 587;
                emailClient.EnableSsl = true;
                emailClient.Send(mm);
                return;

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
            finally
            {
                UIController.MainWindowReference.Cursor = Cursors.Arrow;
            }


        }

        internal static void LoadInvoiceListPage()
        {
            try
            {
                UIController.OpenPage(new InvoiceListPage(), true, "");
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadCashAccountTransactionsListPage()
        {
            try
            {
                UIController.OpenPage(new CashAccountTransactionsListPage(), true, "");

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadScheduleTripWindow()
        {
            try
            {
                ScheduledTrip st = new ScheduledTrip();
                ScheduleTripWindow stw = new ScheduleTripWindow(st);
                if (stw.ShowDialog().GetValueOrDefault(false))
                {
                    /*if(st.ObjectState == BOState.Edit)
                     * if(st.ScheduledTripID>0)
                    DAL.ScheduledTripDB.EditScheduledTrip(st);*/
                    // if(st.ScheduledTripID==0)
                    DAL.ScheduledTripDB.AddScheduledTrip(st);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadCustomerOpenBalancePage()
        {
            try
            {
                UIController.OpenPage(new CustomerOpenBalancePage(), true, "");
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadHomePage()
        {
            try
            {
                UIController.OpenPage(new HomePage(), true, "");

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static void LoadDriverStatusLogListPage()
        {
            try
            {
                UIController.OpenPage(new DriverStatusLogListPage(), true, "");

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadDirverHoursAndAmountToalsListPage()
        {
            try
            {
                UIController.OpenPage(new DriversHoursAndAmountTotalsPage(), true, "");
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static void LoadLogTicketBookPage()
        {
            try
            {
                string passwordEnterd = "";
                if (UIController.GetInput("Password", "Enter Password...", out passwordEnterd))
                    if (passwordEnterd == "623623")
                    {
                        UIController.OpenPage(new LogTicketBookPage(new GenerateTicketBookStatuses()), true, "");
                    }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        //internal static bool VerifyPassCode(Customer customerToCheck)
        //{
        //    if (!customerToCheck.NeedsPassCode)
        //        return true;


        //    var CustomerPassCodes = DAL.CustomerPassCodeDB.GetCustomerPassCode(customerToCheck.CustomerID);//new ObservableCollection<CustomerPassCode>();
        //    if (UIController.SelectCustomerPassCodeFromList(CustomerPassCodes).CustomerPassCodeID > 0)
        //        return true;
        //    else return false;
        //    /* string passCodeToCheck;
        //    var results = UIController.GetInput("Pass Code", "This Charge Requires a PassCode", out passCodeToCheck);
        //    if (DAL.CustomerPassCodeDB.CheckIfPassCodeCorrect(customerToCheck.CustomerID, passCodeToCheck) == true)
        //    {
        //         return true;
        //    }
        //    else return false;*/

        //}

        internal static int VerifyPassCode(Customer customerToCheck)//, out int customerPassCodeID) 
        {
            if (!customerToCheck.NeedsPassCode)
                return 0;
            // customerPassCodeID= 0;
            var CustomerPassCodes = DAL.CustomerPassCodeDB.GetCustomerPassCode(customerToCheck.CustomerID);//new ObservableCollection<CustomerPassCode>();
            var r = UIController.SelectCustomerPassCodeFromList(CustomerPassCodes);
            // customerPassCodeID= r.CustomerPassCodeID;
            return r.CustomerPassCodeID;// customerPassCodeID;


        }

        internal static void LoadTripsListFroDispatcherWindow()
        {
            try
            {
                TripsListForDispatcherWindow tw = new TripsListForDispatcherWindow();
                tw.Show();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        // **IH**
        internal static void LoadTripsListFroMainWindow()
        {
            try
            {
                TripsListForDispatcherWindow tw = new TripsListForDispatcherWindow();
                tw.ShowDialog();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        internal static void LoadTicketsStatusesListPage()
        {
            try
            {
                string passwordEnterd = "";
                if (UIController.GetInput("Password", "Enter Password...", out passwordEnterd))
                    if (passwordEnterd == "623623")
                    {
                        UIController.OpenPage(new TicketBookStatusListPage(), true, "");
                    }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void LoadSQLPage()
        {
            try
            {
                string passwordEnterd = "";
                if (UIController.GetInput("Password", "Enter Password...", out passwordEnterd))
                    if (passwordEnterd == "623623")
                    {
                        UIController.OpenPage(new SqlPage(), true, "");
                    }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void LoadTicketBookSummeryPage()
        {
            try
            {
                UIController.OpenPage(new TicketBookSummeryPage(), true, "");
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);

            }
        }

        internal static void LoadProcessDailyStartupPage()
        {
            try
            {
                UIController.OpenPage(new ProcessDailyStartupPage(), true, "");

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static void LoadTicketBooksOnHandWindow(ObservableCollection<TicketBookStatus> ticketBookStatuses)
        {
            try
            {
                TicketBooksOnHandWindow tbohw = new TicketBooksOnHandWindow(ticketBookStatuses);
                tbohw.ShowDialog();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static void LoadMissingEnvelopesWindow(ObservableCollection<DateTime> missingdates)
        {
            try
            {
                MissingEnvlopesWindow mew = new MissingEnvlopesWindow(missingdates);
                mew.ShowDialog();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static TicketBookSold LoadSoldBooksWindow(TicketBookSold ticketBookSold)
        {
            try
            {
                SoldBooksWindow sbw = new SoldBooksWindow(ticketBookSold);
                sbw.ShowDialog();
                return sbw.DataContext as TicketBookSold;
            }
            catch (Exception ex)
            {
                return null;
                UIController.DisplayErrorMessage(ex);
            }
        }
        internal static TicketsReceived LoadTicketsRecievedWindow(TicketsReceived ticketsReceived)
        {
            try
            {
                RecievedTicketsWindow rtw = new RecievedTicketsWindow(ticketsReceived);
                rtw.ShowDialog();
                return rtw.DataContext as TicketsReceived;
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
                return null;
            }
        }
    }  

    public interface IDisplayTextBlock 
    {
        string HeaderText { get; set; }
        bool AllowOnlyOnce { get; set; }
        bool ConfirmOnClosing { get; set; }
        string ConfirmOnClosingText { get; set; }
    }

    public class CustomerSearchEventArgs : System.EventArgs
    {
        public Customer CustomerSelected { get; set; }
        public ObservableCollection<Customer> CustomerList { get; set; }
        public Customer PreviousCustomer { get; set; }
    }


    public delegate void CustomerSearchResultEventHandler(object sender, CustomerSearchEventArgs e);

    

}
