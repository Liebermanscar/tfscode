﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Text.RegularExpressions;

namespace CDS
{
    public class ClientFeedback : BO
    {
        public Exception ExceptionCatched { get; set; }
        string _clientInput;

        public string ClientInput
        {
            get { return _clientInput; }
            set { _clientInput = value; OnPropertyChanged("ClientInput"); OnPropertyChanged("AllowSave"); }
        }
        public string ClientName { get; set; }
        public string ClientEmailAddress { get; set; }
        public string ApplicationInfo { get; set; }
        public string ApplicationVersionNo { get; set; }
        string _replyToEmailAddress;

        public string ReplyToEmailAddress
        {
            get { return _replyToEmailAddress; }
            set { _replyToEmailAddress = value; OnPropertyChanged("ReplyToEmailAddress"); OnPropertyChanged("AllowSave"); }
        }



        public override bool AllowSave
        {
            get
            {
                return (this.ReplyToEmailAddress != null &&
                    this.ReplyToEmailAddress.Length > 0 &&
                    this.ClientInput != null &&
                    this.ClientInput.Length > 1 &&
                    Regex.IsMatch(this.ReplyToEmailAddress, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b"));
            }

        }
        public OrganizationInfo OrganizationInformation { get; set; }

    }
}
