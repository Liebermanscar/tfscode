﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;


namespace BOL
{
    public class DailyEnvelope : BO
    {

        public DailyEnvelope()
        {
            //   this.DateOfTrips = DateTime.Today;
            this.TripsCovered = new ObservableCollection<Trip>();
            this.Expenses = new ObservableCollection<DEExpense>();
            this.TicketBooksSold = new ObservableCollection<TicketBookSold>();
            this.TicketsReceivedByDriver = new ObservableCollection<TicketsReceived>();
            this.SavedFiles = new ObservableCollection<SavedFile>();
        }

        ObservableCollection<SavedFile> _SavedFiles;

        public ObservableCollection<SavedFile> SavedFiles
        {
            get { return _SavedFiles; }
            set { _SavedFiles = value; this.OnPropertyChanged("SavedFilesDisplay"); }
        }

        public ObservableCollection<SavedFile> SavedFilesDisplay
        {
            get
            {
                return new ObservableCollection<SavedFile>(_SavedFiles.Where(sf => !sf.IsMarkedForDeletion).ToList());
            }
        }

        int _dailyEnvelopeID;

        public int DailyEnvelopeID
        {
            get { return _dailyEnvelopeID; }
            set
            {
                _dailyEnvelopeID = value;
                foreach (var tbs in this._ticketBooksSold.Where(tbs => tbs.TicketBookStatusesSold != null))
                {
                    //if (tbs != null && tbs.TicketBookStatusesSold != null && tbs.TicketBookStatusesSold.Count > 0)
                    //{
                    foreach (var tbss in tbs.TicketBookStatusesSold)
                    {
                        if (tbss != null)
                        {
                            tbss.DailyEnvelopeIdSold = value;
                        }
                    }
                    //}
                }
            }
        }
        DateTime? _tripDate;

        public DateTime? DateOfTrips
        {
            get { return _tripDate; }
            set
            {
                _tripDate = value; this.OnPropertyChanged("AllowGetTrips");


                this.TripsCovered = new ObservableCollection<Trip>(); this.OnPropertyChanged("TripsCovered");
            }
        }

        Driver _unitDriver;

        public Driver UnitDriver
        {
            get { return _unitDriver; }
            set
            {
                _unitDriver = value;
                this.OnPropertyChanged("AllowGetTrips");
                this.OnPropertyChanged("AllowSave");
                if (this.ObjectState == BOState.New)
                {
                    this.CommisionTypeUsed = this.UnitDriver.OnCommisionType;
                    this.VehicleUsed = this.UnitDriver.DrivesVehicle;
                }
                this.TripsCovered = new ObservableCollection<Trip>();
                this.OnPropertyChanged("TripsCovered");
                this.OnPropertyChanged("OwedForDriverAmount");
                this.OnPropertyChanged("DriverCell");
                this.OnPropertyChanged("UnitDriver");
            }
        }
        string _driverCell;

        public string DriverCell
        {
            get
            {
                if (this.UnitDriver != null)
                    return this.UnitDriver.CellNumber;
                else

                    return "";
            }

        }
        Vehicle _vehicleUsed;

        public Vehicle VehicleUsed
        {
            get { return _vehicleUsed; }
            set
            {
                _vehicleUsed = value;
                this.OnPropertyChanged("VehicleUsed");
                this.OnPropertyChanged("AllowSave");

            }
        }
        CommisionType _commisionTypeUsed;

        public CommisionType CommisionTypeUsed
        {
            get { return _commisionTypeUsed; }
            set
            {
                _commisionTypeUsed = value;
                this.OnPropertyChanged("CommisionTypeUsed");
                this.OnPropertyChanged("CompanyCoveredExpenseAmount");
                this.OnPropertyChanged("DriverCommisionAmount");
                this.OnPropertyChanged("GrossBooking");
                this.OnPropertyChanged("TotalForDriverAmount");
                this.OnPropertyChanged("OwedForDriverAmount");
                this.OnPropertyChanged("AllowGetTrips");
                this.OnPropertyChanged("AllowSave");
            }
        }

        decimal _gasPaid;

        public decimal GasPaid
        {
            get { return _gasPaid; }
            set
            {
                _gasPaid = value;
                this.OnPropertyChanged("GasPaid");
                this.OnPropertyChanged("CompanyCoveredExpenseAmount");
                this.OnPropertyChanged("TotalExpensesPaid");
                this.OnPropertyChanged("TotalForDriverAmount");
                this.OnPropertyChanged("OwedForDriverAmount");

            }
        }

        decimal _gasCharged;

        public decimal GasCharged
        {
            get { return _gasCharged; }
            set
            {
                _gasCharged = value;
                this.OnPropertyChanged("GasCharged");
                this.OnPropertyChanged("GasPaid");
                this.OnPropertyChanged("CompanyCoveredExpenseAmount");
                this.OnPropertyChanged("TotalExpensesPaid");
                this.OnPropertyChanged("TotalForDriverAmount");
                this.OnPropertyChanged("OwedForDriverAmount");
                this.OnPropertyChanged("CompanyExpenseAmount");

            }
        }

        int _noOfTrips;

        public int NoOfTrips
        {
            get { return this.TripsCovered.Count(dt => dt.Selected); }
            set { _noOfTrips = value; }
        }
        int _noOfChargeTrips;

        public int NoOfChargeTrips
        {
            get { return this.ChargeTrips.Count(dt => dt.Selected); }
            set { _noOfChargeTrips = value; }
        }

        int _noOfFreeTrips;

        public int NoOfFreeTrips
        {
            get { return this.DriverTrip.Count(dt => dt.Selected); }
            set { _noOfFreeTrips = value; }
        }

        int noOfBooksSold;

        public int NoOfBooksSold
        {
            get { return this.TicketBooksSold.Sum(tbs => tbs.Qty); }
            set { noOfBooksSold = value; }
        }

        public decimal IncomeOfBooksSold
        {
            get { return this.TicketBooksSold.Sum(tbs => tbs.TotalPrice); }
        }

        public decimal TicketsReceivedValue
        {
            // get {return this.TicketsReceivedByDriver.Sum(tr => tr.TotalValue); 
            get
            {
                //return this.TicketsReceivedByDriver.Where(tr =>tr.QtyMissing =tr.Owes)//> tr.Owes)
                return this.TicketsReceivedByDriver.Sum(tr => tr.TotalValue);
            }
        }

        public decimal TotalValueOfMissingTickets
        {
            get { return this.TicketsReceivedByDriver.Sum(tr => tr.AmountOfMissingTickets); }
            //   get { return this.TicketsReceivedByDriver.Sum(tr => tr.TotalValue); }
        }

        //not correct dont use
        public decimal TotalValueOfExtraTicketsSubmitted
        {
            get { return this.TicketsReceivedByDriver.Sum(tr => tr.TotalExtraSubmited); }
        }

        //new version
        public decimal TotalValueExtraTicketsPaidOws
        {
            get { return this.TicketsReceivedByDriver.Sum(tr => tr.TotalPaidOws); }
        }

        //new version
        public decimal TotalValueExtraTicketsForCommission
        {
            get
            {
                if (this.CommisionTypeUsed == null)
                    return 0;

                decimal totalUnreportedTripTickets = this.TicketsReceivedByDriver.Sum(tr => tr.TotalExtraForCommission);
                decimal totalCommision = totalUnreportedTripTickets * Convert.ToDecimal((this.CommisionTypeUsed.DriversCommision / 100));
                return totalCommision;
            }
        }

        public decimal TotalNotRecordedTrips
        {
            get
            {
                return this.TicketsReceivedByDriver.Sum(tr => tr.NoOfTripsNotReported);
            }
        }

        public void AddNotRecordedTrips()
        {
            try
            {
                foreach (var tb in this.TicketsReceivedByDriver.Where(tr => tr.NoOfTripsNotReported > 0))
                {
                    for (int i = 0; i < tb.NoOfTripsNotReported; i++)
                    {
                        Trip trip = new Trip() { Selected = true, FromLocationStreetName = "Auto Entry", TicketBook = tb.FromTicketBook, TicketTypePaid = tb.FromTicketBook.TicketBookID };
                        trip.SetDefaults();
                        this.TripsCovered.Add(trip);

                        var ticketToUse = tb;
                        ticketToUse.QtyBooked = this.TripsCovered.Where(t => t.TicketBook != null &&
                            t.TicketBook.TicketBookID == ticketToUse.FromTicketBook.TicketBookID).Sum(t => t.TicketQt);
                    }
                }

            }
            catch (Exception) { throw; }
        }
        /*  public decimal AmountToTakeOffFromeDriverForMissingTickets 
          {
              get { return this.TicketsReceivedByDriver.Sum(tr => tr.AmountDriversMissingTicketsValue); }
          }*/

        decimal _grossIncome;

        public decimal CashAmountBooked
        {
            //get { return _grossIncome; }
            get
            {
                if (!this.StaticData)
                    return this.TotalCashPayments + this.TotalCheckPayments;
                else
                    return this._grossIncome;
            }

            set
            {
                _grossIncome = value;
                this.OnPropertyChanged("CashAmountBooked");
                this.OnPropertyChanged("TripsIncome");
                this.OnPropertyChanged("DriverCommisionAmount");
                this.OnPropertyChanged("GrossBooking");
                this.OnPropertyChanged("TotalForDriverAmount");
                this.OnPropertyChanged("OwedForDriverAmount");
                this.OnPropertyChanged("AllowSave");
            }
        }

        decimal _amountPaid;

        public decimal AmountSubmitted
        {
            get { return _amountPaid; }
            set
            {
                _amountPaid = value;
                this.OnPropertyChanged("AmountSubmitted");
                this.OnPropertyChanged("OwedForDriverAmount");
                this.OnPropertyChanged("AllowSave");

            }
        }
        public decimal TotalMissingCash
        {
            get
            {
                decimal val = 0;
                if (this.CashAmountBooked > this.AmountSubmitted)
                    val = this.CashAmountBooked - this.AmountSubmitted;
                return val;
            }
        }
        public decimal TotalExtraCash
        {
            get
            {
                decimal val = 0;
                if (this.CashAmountBooked < this.AmountSubmitted)
                    val = this.AmountSubmitted - this.CashAmountBooked;
                return val;
            }
        }
        /*   decimal _amountDriversMissingTicketsValue;
    public decimal AmountDriversMissingTicketsValue
          {
              get { return TicketsReceivedByDriver.QtyMissing * TicketValue; }
          //  get  { return _amountDriversMissingTicketsValue; }
             // set { _amountDriversMissingTicketsValue = value; }
          }*/

        decimal _owedForDriverAmount;
        public decimal OwedForDriverAmount
        {
            get
            {
                if (!this.StaticData)
                    return this.TotalForDriverAmount +
                        this.TotalExtraCash +
                        this.CompanyCoveredExpenseAmount +
                        this.TotalValueExtraTicketsPaidOws +
                        this.TotalValueExtraTicketsForCommission +
                        this.DriverExpenseAmount -
                        this.CompanyExpenseAmount -
                        this.TotalValueOfMissingTickets -
                        this.TotalMissingCash -
                        this.TotalCashFromBooksSold; //this.AmountToTakeOffFromeDriverForMissingTickets;//-this.AmountToTakeOffFromeDriverForMissingTickets;//.DriverExpenseAmount) ;
                else
                    return _owedForDriverAmount;
            }
            set { _owedForDriverAmount = value; this.OnPropertyChanged("OwedForDriverAmount"); }

        }
        decimal _driverCommisionAmount;
        public decimal DriverCommisionAmount
        {
            get
            {
                if (!this.StaticData)
                {
                    if (this.CommisionTypeUsed == null)
                    {
                        return 0;
                    }
                    decimal totalFromTrips = (this.TripsIncome + this.TotalChargeTrips + this.TotalDriverTrips);// - this.TotalTollChargesForCompany
                    decimal totalCommision = totalFromTrips * Convert.ToDecimal((this.CommisionTypeUsed.DriversCommision / 100));// + this.TotalTollChargesForDriver
                    return totalCommision;// (this.TripsIncome + this.TotalChargeTrips + this.TotalDriverTrips + (this.TicketsReceivedValue)) * Convert.ToDecimal((this.CommisionTypeUsed.DriversCommision / 100));
                }
                else return _driverCommisionAmount;
            }
            set { _driverCommisionAmount = value; }
        }
        decimal _totalOfExpenses;

        public decimal TotalOfExpenses
        {
            get { return this.Expenses.Sum(e => e.ExpenseAmount); }
            set { _totalOfExpenses = value; }
        }

        public decimal TotalExpensesPaid
        {
            get
            {
                return this.TotalOfExpenses + this.GasPaid;
            }
        }

        //  decimal _totalChargeTrips;
        public decimal TotalChargeTrips
        {
            get
            {

                return this.ChargeTrips.Sum(ct => (ct.ChargePaid - ct.TollCharge));//return this.ChargeTrips.Sum(ct => ct.TotalTripForDailyEnvelope); (for new system)

            }
            /*    set {
                    _totalChargeTrips = value;
                this.OnPropertyChanged("TotaFromChargeAndDriverTrips");
                }*/

        }
        //  decimal _totalDriverTrips;
        public decimal TotalDriverTrips
        {
            get
            {

                return this.DriverTrip.Sum(ft => (ft.ChargePaid - ft.TollCharge));

            }
            /* set
             {
                 _totalDriverTrips = value;
                 this.OnPropertyChanged("TotaFromChargeAndDriverTrips");
             }*/
        }



        public decimal TotaFromChargeAndDriverTrips
        {
            get
            {

                return this.TotalChargeTrips + this.TotalDriverTrips;
            }
        }
        decimal _tripsIncome;

        public decimal TripsIncome
        {
            //get { return this.CashAmountBooked - this.IncomeOfBooksSold; } //first version was removed for now dosent need to track income of books
            get { return this.CashAmountBooked + this.TotalCCDriverPart + this.TotalTicketPayments - this.TotalTollChargesForRegularTrips; }
            set { _tripsIncome = value; }
        }

        string _noteForDriver;

        public string NoteForDriver
        {
            get { return _noteForDriver; }
            set { _noteForDriver = value; }
        }

        ObservableCollection<Trip> _chargeTrips;

        public ObservableCollection<Trip> ChargeTrips
        {
            get
            {
                return new ObservableCollection<Trip>
                    (this.TripsCovered.Where(tc => tc.TripType == TripTypes.Charge && tc.Selected));
            }//&&(this.TripsCovered.Where(tc=>tc.Selected))); }
            set { _chargeTrips = value; }
        }
        //ObservableCollection<Trip> _chargeTrips;

        //public ObservableCollection<Trip> ChargeTrips
        //{
        //    get { return new ObservableCollection<Trip>(this.TripsCovered.Where(tc => tc.TripType == TripTypes.Charge)); }
        //    set { _chargeTrips = value; }
        //}

        ObservableCollection<TicketBookSold> _ticketBooksSold;

        public ObservableCollection<TicketBookSold> TicketBooksSold
        {
            get { return _ticketBooksSold; }
            set
            {
                _ticketBooksSold = value;
                this._ticketBooksSold.CollectionChanged += delegate (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs ex)
                {
                    if (ex.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (TicketBookSold tbs in ex.NewItems)
                        {
                            tbs.PropertyChanged += delegate (object sender2, System.ComponentModel.PropertyChangedEventArgs ex2)
                            {
                                this.OnPropertyChanged("NoOfBooksSold");
                                this.OnPropertyChanged("IncomeOfBooksSold");
                                this.OnPropertyChanged("TripsIncome");
                                this.OnPropertyChanged("DriverCommisionAmount");
                                this.OnPropertyChanged("TotalForDriverAmount");
                                this.OnPropertyChanged("OwedForDriverAmount");
                                this.OnPropertyChanged("GrossBooking");
                                this.OnPropertyChanged("CashAmountBooked");
                                this.OnPropertyChanged("TotalCashFromBooksSold");
                            };


                        }
                    }
                    this.OnPropertyChanged("NoOfBooksSold");
                    this.OnPropertyChanged("InComeOfBooksSold");
                    this.OnPropertyChanged("TripsIncome");
                    this.OnPropertyChanged("DriverCommisionAmount");
                    this.OnPropertyChanged("TotalForDriverAmount");
                    this.OnPropertyChanged("OwedForDriverAmount");
                    this.OnPropertyChanged("GrossBooking");
                    this.OnPropertyChanged("CashAmountBooked");
                    this.OnPropertyChanged("TotalCashFromBooksSold");

                };

            }
        }

        ObservableCollection<DEExpense> _expenses;

        public ObservableCollection<DEExpense> Expenses
        {
            get { return _expenses; }
            set
            {
                _expenses = value;
                this.Expenses.CollectionChanged += new NotifyCollectionChangedEventHandler(Expenses_CollectionChanged);
            }
        }

        void Expenses_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (DEExpense expense in e.NewItems)
                {
                    expense.PropertyChanged += delegate (object sender2, System.ComponentModel.PropertyChangedEventArgs ex)
                    {
                        /*  bool tempStaticData;
                          if (StaticData == true)
                          {
                              tempStaticData = StaticData;
                              this.StaticData = false;
                          }
                          else tempStaticData = false;*/


                        this.OnPropertyChanged("TotalOfExpenses");
                        this.OnPropertyChanged("TotalExpensesPaid");
                        this.OnPropertyChanged("CompanyCoveredExpenseAmount");
                        this.OnPropertyChanged("OwedForDriverAmount");

                        this.OnPropertyChanged("TotalForDriverAmount");
                        /*  if (tempStaticData == true)
                          {
                              StaticData = true;

                          }*/

                    };
                }
            }

            this.OnPropertyChanged("TotalOfExpenses");
            this.OnPropertyChanged("CompanyCoveredExpenseAmount");
            this.OnPropertyChanged("TotalExpensesPaid");
            this.OnPropertyChanged("OwedForDriverAmount");
            this.OnPropertyChanged("TotalForDriverAmount");

        }



        ObservableCollection<Trip> _tripsCovered;

        public ObservableCollection<Trip> TripsCovered
        {
            get { return _tripsCovered; }
            set
            {
                _tripsCovered = value;
                this.OnPropertyChanged("TripsCovered");
                this.OnPropertyChanged("NoOfTrips");
                value.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(value_CollectionChanged);
            }
        }

        void value_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.OnPropertyChanged("NoOfTrips");
            this.OnPropertyChanged("NoOfChargeTrips");
            this.OnPropertyChanged("NoOfFreeTrips");
            this.OnPropertyChanged("ChargeTrips");
            this.OnPropertyChanged("FreeTrips");
            this.OnPropertyChanged("TotalChargeTrips");
            this.OnPropertyChanged("TotalDriverTrips");
            this.OnPropertyChanged("TotaFromChargeAndDriverTrips");
            this.OnPropertyChanged("DriverCommisionAmount");
            this.OnPropertyChanged("TotalForDriverAmount");
            this.OnPropertyChanged("OwedForDriverAmount");
            this.OnPropertyChanged("GrossBooking");
            this.OnPropertyChanged("TotalCashPayments");
            this.OnPropertyChanged("TotalCheckPayments");
            this.OnPropertyChanged("TotalChargesPayments");
            this.OnPropertyChanged("TotalChargePayments");
            this.OnPropertyChanged("TotalCCDriverPart");
            this.OnPropertyChanged("TotalTicketPayments");
            this.OnPropertyChanged("CashAmountBooked");
            this.OnPropertyChanged("TripsIncome");
            this.OnPropertyChanged("DriverCommisionAmount");
            this.OnPropertyChanged("GrossBooking");
            this.OnPropertyChanged("TotalForDriverAmount");
            this.OnPropertyChanged("OwedForDriverAmount");
            this.OnPropertyChanged("TotalTollChargesForDriver");
            this.OnPropertyChanged("AllowSave");

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Trip trip in e.NewItems)
                {
                    trip.PropertyChanged += new PropertyChangedEventHandler(trip_PropertyChanged);
                }
            }
        }

        void trip_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TicketBook" || e.PropertyName == "TicketQt" || e.PropertyName == "TicketPaid")
            {
                foreach (var ticket in this.TicketsReceivedByDriver)
                {
                    var ticketToUse = ticket;
                    ticketToUse.QtyBooked = this.TripsCovered.Where(t => t.TicketBook != null &&
                        t.TicketBook.TicketBookID == ticketToUse.FromTicketBook.TicketBookID).Sum(t => t.TicketQt);
                }
                this.OnPropertyChanged("TicketsReceivedByDriver");
                //Trip trip = (Trip)sender;
                //TicketsReceived tr = this.TicketsReceivedByDriver.Where(t => t.FromTicketBook.TicketBookID == trip.TicketTypePaid).FirstOrDefault();
                //if (tr != null)
                //    tr.QtyBooked = this.TripsCovered.Where(t => t.TicketTypePaid == trip.TicketTypePaid).Sum(t => t.TicketQt);
                //tr.OnPropertyChanged("QtyBooked");
            }
            //this.OnPropertyChanged("TicketsReceivedByDriver");
            this.OnPropertyChanged("NoOfTrips");
            this.OnPropertyChanged("NoOfChargeTrips");
            this.OnPropertyChanged("NoOfFreeTrips");
            this.OnPropertyChanged("ChargeTrips");
            this.OnPropertyChanged("FreeTrips");
            this.OnPropertyChanged("TotalChargeTrips");
            this.OnPropertyChanged("TotalDriverTrips");
            this.OnPropertyChanged("TotaFromChargeAndDriverTrips");
            this.OnPropertyChanged("DriverCommisionAmount");
            this.OnPropertyChanged("TotalForDriverAmount");
            this.OnPropertyChanged("OwedForDriverAmount");
            this.OnPropertyChanged("GrossBooking");
            this.OnPropertyChanged("TotalCashPayments");
            this.OnPropertyChanged("TotalCheckPayments");
            this.OnPropertyChanged("TotalChargesPayments");
            this.OnPropertyChanged("TotalChargePayments");
            this.OnPropertyChanged("TotalCCDriverPart");
            this.OnPropertyChanged("TotalTicketPayments");
            this.OnPropertyChanged("CashAmountBooked");
            this.OnPropertyChanged("TripsIncome");
            this.OnPropertyChanged("DriverCommisionAmount");
            this.OnPropertyChanged("GrossBooking");
            this.OnPropertyChanged("TotalForDriverAmount");
            this.OnPropertyChanged("OwedForDriverAmount");
            this.OnPropertyChanged("TotalTollChargesForDriver");
            this.OnPropertyChanged("AllowSave");
        }
        ObservableCollection<Trip> _freeTrips;

        public ObservableCollection<Trip> DriverTrip
        {
            get { return new ObservableCollection<Trip>(this.TripsCovered.Where(tc => tc.TripType == TripTypes.DriverTrip && tc.Selected)); }
            set { _freeTrips = value; }
        }

        ObservableCollection<TicketsReceived> _totalTicketsReceived;

        public ObservableCollection<TicketsReceived> TicketsReceivedByDriver
        {
            get { return _totalTicketsReceived; }
            set
            {
                _totalTicketsReceived = value;
                value.CollectionChanged += delegate (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs ex)
                {
                    if (ex.Action == NotifyCollectionChangedAction.Add)
                    {
                        foreach (TicketsReceived tr in ex.NewItems)
                        {
                            tr.PropertyChanged += delegate (object sender2, System.ComponentModel.PropertyChangedEventArgs ex2)
                            {
                                this.OnPropertyChanged("TicketsReceivedValue");
                                this.OnPropertyChanged("TotalPaidForDriverWithTickets");
                                this.OnPropertyChanged("DriverCommisionAmount");
                                this.OnPropertyChanged("GrossBooking");
                                this.OnPropertyChanged("TotalForDriverAmount");
                                this.OnPropertyChanged("OwedForDriverAmount");
                            };
                        }
                    }

                    this.OnPropertyChanged("TicketsReceivedValue");
                    this.OnPropertyChanged("TotalPaidForDriverWithTickets");
                    this.OnPropertyChanged("DriverCommisionAmount");
                    this.OnPropertyChanged("TotalForDriverAmount");
                    this.OnPropertyChanged("GrossBooking");
                    this.OnPropertyChanged("OwedForDriverAmount");
                };
            }
        }

        decimal _grossBooking;

        public decimal GrossBooking
        {
            get
            {
                if (!this.StaticData)
                    return (this.TripsIncome + this.TotalChargeTrips + this.TotalDriverTrips);// + this.TicketsReceivedValue (removed for new system)
                else return _grossBooking;
            }
            set { _grossBooking = value; }
        }

        public decimal CompanyCoveredExpenseAmount
        {
            get
            {
                if (this.CommisionTypeUsed == null)
                    return this.TotalOfExpenses;
                return this.TotalOfExpenses + (this.GasPaid - (this.GasPaid * Convert.ToDecimal(this.CommisionTypeUsed.PercentOfGasDriverPays / 100)));
            }

        }

        public decimal DriverExpenseAmount
        {
            get
            {
                return this.TotalOfExpenses + this.GasPaid - this.CompanyCoveredExpenseAmount;
            }
        }

        public decimal CompanyExpenseAmount
        {
            get
            {
                if (this.CommisionTypeUsed == null)
                    return 0;
                return this.GasCharged * Convert.ToDecimal(this.CommisionTypeUsed.PercentOfGasDriverPays / 100);
            }

        }
        decimal _totalForDriverAmount;
        public decimal TotalForDriverAmount
        {
            get
            {
                if (!this.StaticData)
                {
                    return (this.DriverCommisionAmount + this.TotalTollChargesForDriver) - this.DriverExpenseAmount;// -AmountToTakeOffFromeDriverForMissingTickets; //- this.DriverExpenseAmount;
                }
                else return _totalForDriverAmount;

            }
            set { _totalForDriverAmount = value; }
        }

        public bool AllowGetTrips
        {
            get { return this.UnitDriver != null && this.CommisionTypeUsed != null && this.DateOfTrips != null; }
        }

        public decimal TotalCashPayments
        {
            get { return this.TripsCovered.Sum(t => t.CashPaid); }
        }

        public decimal TotalCheckPayments
        {
            get { return this.TripsCovered.Sum(t => t.CheckPaid); }
        }

        public decimal TotalChargesPayments
        {
            get { return this.TripsCovered.Sum(t => t.TotalCharges); }
        }

        public decimal TotalCCDriverPart
        {
            get { return this.TripsCovered.Sum(t => t.CCPaidDriverComission); }
        }

        public decimal TotalChargePayments
        {
            get { return this.TripsCovered.Sum(t => t.ChargePaid); }
        }

        public decimal TotalTicketPayments
        {
            get { return this.TripsCovered.Sum(t => t.TicketPaid); }
        }

        decimal _totalCashFromBooksSold;
        public decimal TotalCashFromBooksSold
        {
            get
            {
                if (!this.StaticData)
                {
                    var total = this.TicketBooksSold.Sum(q => q.QtyPaidCash * q.Price);// Where(tbs => tbs.IsPaidByCC == false).Sum(q => q.TotalPrice);
                    return total;
                }
                else return _totalCashFromBooksSold;
            }
            set { this._totalCashFromBooksSold = value; }
        }

        public decimal TotalTollChargesForRegularTrips
        {
            get { return this.TripsCovered.Where(t => t.TripType == TripTypes.Regular).Sum(t => t.TollCharge); }
        }
        /// <summary>
        /// Toll what we need to give back full amount to driver
        /// </summary>
        public decimal TotalTollChargesForDriver
        {
            get
            {
                decimal toll = 0;

                if (this.VehicleUsed != null && !this.VehicleUsed.IsCompanyEZPass)
                {
                    toll = this.TripsCovered.Where(t => t.TripType != TripTypes.Regular || t.CCPaid > 0).Sum(t => t.TollCharge);
                }

                return toll;
            }
        }

        /// <summary>
        /// Toll what we need to give back full amount to company
        /// </summary>
        public decimal TotalTollChargesForCompany
        {
            get
            {
                decimal toll = 0;

                if (this.VehicleUsed != null && this.VehicleUsed.IsCompanyEZPass)
                {
                    toll = this.TripsCovered.Sum(t => t.TollCharge); //Where(t => t.TripType == TripTypes.Regular).
                }

                return toll;
            }
        }

        public void SetCompanyIncome()
        {
            decimal commission = Convert.ToDecimal((this.CommisionTypeUsed.DriversCommision / 100));
            foreach (Trip trip in this.TripsCovered)
            {
                decimal driverComission = trip.TotalPriceDriverIncome * commission;
                trip.CompanyIncome = trip.TotalPriceCompanyIncome - driverComission;
            }
        }

        public override bool AllowSave
        {
            get
            {
                if (this.VehicleUsed != null)
                {
                    return this.VehicleUsed.VehicleID > 0 && this.UnitDriver != null && this.CommisionTypeUsed != null;
                }
                return this.UnitDriver != null && this.CommisionTypeUsed != null && this.VehicleUsed != null;


                //&& this.CashAmountBooked > 0;
            }

        }

        public bool StaticData { get; set; }
        public bool DontIncludeAssignedTrips { get; set; }

        public string DriverCode { get; set; }
        public int DriverPaymentID { get; set; }
    }

    public enum TripEnteredOptions
    {
        ByDispatcher = 1
    }

    public class DEExpense : BO, IDataErrorInfo
    {
        int _dEExpenseID;

        public int DEExpenseID
        {
            get { return _dEExpenseID; }
            set { _dEExpenseID = value; }
        }
        int _dailyEnvelopeID;

        public int DailyEnvelopeID
        {
            get { return _dailyEnvelopeID; }
            set { _dailyEnvelopeID = value; }
        }

        int _expenseTypeID;

        public int ExpenseTypeID
        {
            get { return _expenseTypeID; }
            set { _expenseTypeID = value; }
        }


        ExpenseType _expense;


        public ExpenseType Expense
        {
            get { return _expense; }
            set { _expense = value; this.OnPropertyChanged("Expense"); }
        }
        decimal _expenseAmount;

        public decimal ExpenseAmount
        {
            get { return _expenseAmount; }
            set { _expenseAmount = value; this.OnPropertyChanged("ExpenseAmount"); }
        }

        public override bool AllowSave
        {
            get
            {
                return
                    // this.ExpenseAmount > 0 &&
                    this.Expense != null;
            }

        }

        #region IDataErrorInfo Members

        string IDataErrorInfo.Error
        {
            get { if (this.Expense == null) return "Please Select an Expense Type"; else return null; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get { return (this.Expense == null) ? "Please Select an Expense Type" : null; }
        }

        #endregion
    }

    public class DriverExpense : BO
    {
        int _driverExpensesID;

        public int DriverExpenseID
        {
            get { return _driverExpensesID; }
            set { _driverExpensesID = value; }
        }
        int _driverID;

        public int DriverID
        {
            get { return _driverID; }
            set { _driverID = value; this.OnPropertyChanged("AllowSave"); }
        }
        string _driverCode;

        public string DriverCode
        {
            get { return _driverCode; }
            set { _driverCode = value; }
        }
        DateTime _dateOfExpense;

        public DateTime DateOfExpense
        {
            get { return _dateOfExpense; }
            set
            {
                _dateOfExpense = value;
                this.OnPropertyChanged("AllowSave");
            }
        }
        string _detailsOfExpense;

        public string DetailsOfExpense
        {
            get { return _detailsOfExpense; }
            set { _detailsOfExpense = value; }
        }
        decimal _amountOfExpense;

        public decimal AmountOfExpense
        {
            get { return _amountOfExpense; }
            set { _amountOfExpense = value; this.OnPropertyChanged("AllowSave"); }
        }

        public override bool AllowSave
        {
            get
            {
                return this.AmountOfExpense != 0 && this.DriverID > 0 && this.DateOfExpense != null; ;
            }

        }


    }

    public class ExpenseType
    {

        int _expensetypeID;

        public int ExpenseTypeID
        {
            get { return _expensetypeID; }
            set { _expensetypeID = value; }
        }
        string _expenseCode;

        public string ExpenseCode
        {
            get { return _expenseCode; }
            set { _expenseCode = value; }
        }
        public bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.ExpenseCode);
            }
        }

    }

}
