﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BOL
{
    public class MedicaidTripPan : BO
    {
        public MedicaidTripPan() { }

        int _panID;

        public int PanID
        {
            get { return _panID; }
            set { _panID = value; }
        }
        string _recipientID;

        public string RecipientID
        {
            get { return _recipientID; }
            set { _recipientID = value; }
        }
        string _recipientName;

        public string RecipientName
        {
            get { return _recipientName; }
            set { _recipientName = value; }
        }
        string _YOB;

        public string YOB
        {
            get { return _YOB; }
            set { _YOB = value; }
        }
        string _sex;

        public string Sex
        {
            get { return _sex; }
            set { _sex = value; }
        }
        string _invoiceNumber;

        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set { _invoiceNumber = value; }
        }
        string _priorApprovalNumber;

        public string PriorApprovalNumber
        {
            get { return _priorApprovalNumber; }
            set { _priorApprovalNumber = value; }
        }
        string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }
        DateTime? _serviceStarts;

        public DateTime? ServiceStarts
        {
            get { return _serviceStarts; }
            set { _serviceStarts = value; }
        }
        DateTime? _serviceEnds;

        public DateTime? ServiceEnds
        {
            get { return _serviceEnds; }
            set { _serviceEnds = value; }
        }
        DateTime? _approvedTo;

        public DateTime? ApprovedTo
        {
            get { return _approvedTo; }
            set { _approvedTo = value; }
        }
        string _orderingProvider;

        public string OrderingProvider
        {
            get { return _orderingProvider; }
            set { _orderingProvider = value; }
        }
        decimal _Amt;

        public decimal Amt
        {
            get { return _Amt; }
            set { _Amt = value; }
        }
        decimal _Qty;

        public decimal Qty
        {
            get { return _Qty; }
            set { _Qty = value; }
        }
        int _daysTimes;

        public int DaysTimes
        {
            get { return _daysTimes; }
            set { _daysTimes = value; }
        }
        string _CDA;

        public string CDA
        {
            get { return _CDA; }
            set { _CDA = value; }
        }
        string _mod;

        public string Mod
        {
            get { return _mod; }
            set { _mod = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }
}
