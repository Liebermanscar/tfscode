using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Android.Support.V7.App;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using BOL_OTR;

namespace OTR_App
{
	public static class UIController
    {
        public static class Modal
        {
            public static string SHARED_PREF_FILE_NAME = "SHARED_PREF";

            public static bool IsAuthenticated = false;

            public static string TabletName;

			public static string TabletID;

            internal static int DriverID;

            public static BOL_OTR.DriverInfo DriverInfo;
        }

		public static MainActivity MainActivityRef;

		public static ListenToDispatchingServiceBinder ltdServiceBinder;
		public static bool isBound = false;

	    static ISharedPreferences sharedPref = Application.Context.GetSharedPreferences(Modal.SHARED_PREF_FILE_NAME, FileCreationMode.Private);

		public static BOL_OTR.Trip_OTR NewTrip;
		public static BOL_OTR.Trip_OTR ActiveTrip;
        public static BOL_OTR.TicketPaid_OTR[] TicketTypes;

        public static void StarStartupActivity()
		{
			var startApp = new Intent(Application.Context, typeof(StartupActivity));
			startApp.AddFlags (ActivityFlags.NewTask);
			Application.Context.StartActivity (startApp);

		}

        public static void StartListenToDispatchingService()
        {
            new Task(() =>
            {
                var startApp = new Intent(Application.Context, typeof(ListenToDispatchingService));
                startApp.AddFlags(ActivityFlags.NewTask);
                Application.Context.StartService(startApp);
            }).Start();
		}

		public static void UpdateMainUI()
		{
			if (MainActivityRef == null)
				return;
		}

        internal static void DisplayDriverInfoActivity()
        {
            var driverInfoActivityItnt = new Intent(Application.Context, typeof(DriverInfoActivity));
            driverInfoActivityItnt.AddFlags(ActivityFlags.NewTask);
            Application.Context.StartActivity(driverInfoActivityItnt);
        }

		internal static void DisplayTripScreen()
		{
			var activeTripActivityInt = new Intent (Application.Context, typeof(ActiveTripActivity));
			activeTripActivityInt.AddFlags (ActivityFlags.NewTask);
			Application.Context.StartActivity (activeTripActivityInt);
		}

        internal static BOL_OTR.DriverInfo GetDriverInfo()
        {
            Modal.DriverInfo = CallWebApi.GetDriverInfo(UIController.GetDriverID());
            return Modal.DriverInfo;
        }

        public static void StarMainActivity()
		{
			var startApp = new Intent(Application.Context, typeof(MainActivity));
			startApp.AddFlags (ActivityFlags.NewTask);
			Application.Context.StartActivity (startApp);
		}

        public static bool Login(string userName, string password)
        {
            var result = CallWebApi.Login(userName, password, GetTabletName());
            if (result.Contains("DriverID"))
            {
                var s = result.Split(':');

                SaveStringToSharedPref("UserName_SignedIn", userName);
                SaveStringToSharedPref("DriverID_SignedIn", s[1].Trim());

				UIController.Modal.IsAuthenticated = true;
                
                return true;
            }
			return false;
        }

		public static bool Logout()
        {
			var driverId = UIController.GetDriverID ();
			var tabletName = UIController.GetTabletName ();
			if (CallWebApi.Logout (driverId, tabletName) == "SIGNED_OUT") {

                UIController.SignalRCalls.SingOutDriver();

                SaveStringToSharedPref ("UserName_SignedIn", "");
				SaveStringToSharedPref ("DriverID_SignedIn", "");

				UIController.Modal.IsAuthenticated = false;

				return true;
			} else
				return false;
        }

        public static string GetTabletName()
        {
            //ISharedPreferences sharedPref = Application.Context.GetSharedPreferences(Modal.SHARED_PREF_FILE_NAME, FileCreationMode.Private);
			if(string.IsNullOrWhiteSpace(Modal.TabletName))
				Modal.TabletName = sharedPref.GetString("TabletName", "");
			
			return Modal.TabletName;
        }

		public static string GetTabletID()
		{
			//ISharedPreferences sharedPref = Application.Context.GetSharedPreferences(Modal.SHARED_PREF_FILE_NAME, FileCreationMode.Private);
			if(string.IsNullOrWhiteSpace(Modal.TabletName))
				Modal.TabletName = sharedPref.GetString("TabletID", "");

			return Modal.TabletID;
		}

        public static bool SaveTabletName(string tabletName)
        {
            try
            {
				var tabletID = CallWebApi.ConfirmTabletName(tabletName);
				if (tabletID != "0")
				{					
                    SaveStringToSharedPref("TabletName", tabletName);
					SaveStringToSharedPref("TabletID", tabletID);
					return true;
				}
                else
                    return false;
            }
            catch (Exception ex)
            {
                UIController.HandleError(ex);
                return false;
            }
		}

		public static string GetUserName()
		{
			return GetStringFromSharedPref ("UserName_SignedIn");
		}

		public static string GetPassword()
		{
			return GetStringFromSharedPref ("Password_SignedIn");
		}

		public static string GetDriverID()
		{
			return GetStringFromSharedPref ("DriverID_SignedIn");
		}

        public static int GetDriverIDint()
        {
            int id;
            int.TryParse(GetDriverID(), out id);
            return id;
        }

        public static bool SaveStringToSharedPref(string key, string value)
        {
            try
            {
                //ISharedPreferences sharedPref = Application.Context.GetSharedPreferences(Modal.SHARED_PREF_FILE_NAME, FileCreationMode.Private);
                ISharedPreferencesEditor editor = sharedPref.Edit();
                editor.PutString(key, value);
                return editor.Commit();
            }
            catch(Exception ex)
            {
                UIController.HandleError(ex);
                return false;
            }
        }

        public static string GetStringFromSharedPref(string key, string defValue = "")
        {
            try
            {
                //ISharedPreferences sharedPref = Application.Context.GetSharedPreferences(Modal.SHARED_PREF_FILE_NAME, FileCreationMode.Private);
                return sharedPref.GetString(key, defValue);
            }
            catch (Exception ex)
            {
                UIController.HandleError(ex);
                return "";
            }
        }

		private static void HandleError(Exception ex)
        {
			AlertDialog.Builder error = new AlertDialog.Builder (MainActivityRef);
			error.SetTitle ("Error")
				.SetCancelable(true)
				.SetMessage (ex.Message)
				.Show();


			//Toast.MakeText(Application.Context, ex.Message, ToastLength.Short).Show();
            //new AlertDialog.Builder(Application.Context)
            //     .SetPositiveButton("OK", (sender, args) => { })
            //     .SetMessage(ex.Message)
            //     .SetTitle("Error")
            //     .Show();
        }

		public static void ShowAlrtDialog(Context context, string title, string message)
		{
			new AlertDialog.Builder (context)
			     .SetPositiveButton ("OK", (sender, args) => {
			})
				.SetMessage (message)
				.SetTitle (title)
			     .Show ();
		}

		public static void ShowNewTripActivity(Trip_OTR newTrip)
		{
			NewTrip = newTrip;

			var intent = new Intent (Application.Context, typeof(NewTripActivity));
			intent.AddFlags(ActivityFlags.NewTask);
			Application.Context.StartActivity(intent);
		}

		//public static void NewTripStatus(bool accept)
		//{
		//	if (accept) {
		//		ltdServiceBinder.GetListenToDispatchingServiceBinder().ResponseToTripAssignment(accept);
  //              ActiveTrip = NewTrip;
  //              ActiveTrip.TripDetailedStatus = TripDetailedStatusOptions.GoingToPickupLocation;
  //              NewTrip = null;
		//		DisplayTripScreen();
		//	}
		//}

        public static class SignalRCalls
        {
            public static ListenToDispatchingService SignalR
            {
                get { return ltdServiceBinder.GetListenToDispatchingServiceBinder(); }
            }

            public static void SingOutDriver()
            {
                SignalR.RemoveDriver();
            }

            public static void NewTripStatus(bool accept)
            {
                if (accept)
                {
                    SignalR.ResponseToTripAssignment(accept);
                    ActiveTrip = NewTrip;
                    ActiveTrip.TripDetailedStatus = TripDetailedStatusOptions.GoingToPickupLocation;
                    NewTrip = null;
                    DisplayTripScreen();
                }
            }
        }
    }

	public class Length
	{

		public static double MetersToFeet(double Meters)
		{
			return Meters / 0.304800610;
		}

		public static double FeetToMeters(double Feet)
		{
			return Feet * 0.304800610;
		}
	}
}