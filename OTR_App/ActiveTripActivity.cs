﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Locations;
using Android.Support.V7.App;
using Android.Support.V4.Content;

namespace OTR_App
{
	[Activity (Label = "ActiveTripActivity")]			
	public class ActiveTripActivity : Activity, ILocationListener
    {
        #region Controls
        TextView tvPickup;
		TextView tvDropOff;
		TextView tvCustPhoneNo;
		TextView tvCustName;
		TextView tvTripDetailedStatus;
        TextView tvTotalWaitingTime;
        TextView tvWaitingTime;
        TextView tvStopsCount;
        TextView tvTripPrice;

        Button btnMoveToNextState;
        Button btnChangeDestination;
		Button btnAddStop;
		Button btnTripFinished;
		Button btnRevertToLastState;
		Button btnCanceled;
        #endregion Controls

        static string AddStops = "Add Stops";
        static string ChangeStops = "Change Stops";

        Location tripStartLocation;
		Location tripEndLocation;

        BOL_OTR.Trip_OTR ActiveTrip;
        Timer waitingTimer;
        DateTime? startWaiting;
        double totalWaitingSeconds = 0;
        //DispatcherResponsBroadcastReceiver drbr;
        DispatcherResponsBroadcastReceiver permissionReciever;

        protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

            permissionReciever = new DispatcherResponsBroadcastReceiver();
            permissionReciever.ActiveTripRef = this;
            IntentFilter filter = new IntentFilter();
            filter.AddAction("PermissionGranted");
            filter.AddAction("PermissionRevoked");
            RegisterReceiver(permissionReciever, filter);

            //drbr = new DispatcherResponsBroadcastReceiver();
            //drbr.ActiveTripRef = this;
            //LocalBroadcastManager.GetInstance(this).RegisterReceiver(drbr, new IntentFilter(DispatcherResponsBroadcastReceiver.Active_Trip_Listener));

            //UIController.ActiveTrip = new BOL_OTR.Trip_OTR()
            //{
            //    FromLocation = "89 Wallabout St",
            //    ToLocation = "179 Penn St",
            //    CustomerName = "Joel Weber",
            //    CustomerHomePhoneNo = "718-388-8586",
            //    CustomerCellPhoneNo = "347-742-0942",
            //    TotalStops = 2,
            //    TripDetailedStatus = BOL_OTR.TripDetailedStatusOptions.GoingToPickupLocation
            //};
            ActiveTrip = UIController.ActiveTrip;

			SetContentView(Resource.Layout.ActiveTrip);

            #region FindViews
            tvPickup = FindViewById<TextView>(Resource.Id.tvPickUp);
			tvDropOff = FindViewById<TextView>(Resource.Id.tvDropOff);
			tvCustPhoneNo = FindViewById<TextView>(Resource.Id.tvCustPhonNo);
			tvCustName = FindViewById<TextView>(Resource.Id.tvCustName);
			tvTripDetailedStatus = FindViewById<TextView>(Resource.Id.tvTripDetailedStatus);
            tvTotalWaitingTime = FindViewById<TextView>(Resource.Id.tvTotalWaitingTime);
            tvWaitingTime = FindViewById<TextView>(Resource.Id.tvWaitingTime);
            tvStopsCount = FindViewById<TextView>(Resource.Id.tvStopsCount);
            tvTripPrice = FindViewById<TextView>(Resource.Id.tvTripCost);

            btnMoveToNextState = FindViewById<Button> (Resource.Id.btnMoveToNextState);
            btnChangeDestination = FindViewById<Button>(Resource.Id.btnChangeDestination);
			btnAddStop = FindViewById<Button> (Resource.Id.btnAddStop);
			btnTripFinished = FindViewById<Button> (Resource.Id.btnTripFinished);
			btnRevertToLastState = FindViewById<Button> (Resource.Id.btnRevertToLastState);
			btnCanceled = FindViewById<Button> (Resource.Id.btnCanceled);
            #endregion FindViews

            #region Initialize Control Values
            tvPickup.Text = ActiveTrip.FromLocation;
            tvDropOff.Text = ActiveTrip.ToLocation;
            tvCustPhoneNo.Text = ActiveTrip.CustomerHomePhoneNo;
            tvCustName.Text = ActiveTrip.CustomerName;
            tvTripPrice.Text = ActiveTrip.Price.ToString("c");
            #endregion Initialize Control Values

            btnMoveToNextState.Click += (object sender, EventArgs e) => {				
				BOL_OTR.TripDetailedStatusOptions newState;

				newState = MoveToNextState();

				UpdateTripStatus(newState);

                if (newState == BOL_OTR.TripDetailedStatusOptions.TripPayment)
                {
                    GetPayment();
                }
                if (newState == BOL_OTR.TripDetailedStatusOptions.TripCompleted)
                {
                    ReportTripStatusChanged(newState);
                    ReportSaveTripDetails();
                    Finish();
                }
			};

            btnChangeDestination.Click += (object sender, EventArgs e) =>
            {
                RequestPremissionFromDispatcher();
            };

            StopsChanged();

			btnAddStop.Click += (object sender, EventArgs e) => {

                Intent changeStopIntent = new Intent(this, typeof(AddStopsActivity));
                changeStopIntent.PutExtra("messageText", "How Much Stops?");
                changeStopIntent.PutExtra("presetNumber", ActiveTrip.TotalStops.ToString());
                StartActivityForResult(changeStopIntent, 1);

            };

			btnRevertToLastState.Click += (object sender, EventArgs e) => {
				RevertToLastAction();
			};

			btnTripFinished.Click += (object sender, EventArgs e) => {
				GetPayment();
			};

            waitingTimer = new Timer((obj) =>
            {
                if (startWaiting.HasValue)
                {
                    var totalTime = DateTime.Now - startWaiting.Value;
                    RunOnUiThread(() => { tvWaitingTime.Text = totalTime.ToString("hh\\:mm\\:ss"); });
                }
            });

            new Thread(new ThreadStart(() => {
                var geo = new Geocoder(this);

                var fromAddresses = geo.GetFromLocationName(
                    string.Format("{0}, Brooklyn, NY 11211", UIController.ActiveTrip.FromLocation), 1);

                var toAddresses = geo.GetFromLocationName(
                    string.Format("{0}, Brooklyn, NY 11211", UIController.ActiveTrip.ToLocation), 1);

                RunOnUiThread(() => {

                    var result = fromAddresses.ToList().FirstOrDefault();

                    if (result != null)
                    {
                        UIController.ActiveTrip.FromCoordinate = new BOL_OTR.GeoCoordinate()
                        {
                            Latitude = result.Latitude,
                            Longitude = result.Longitude
                        };

                        tripStartLocation = new Location(LocationManager.GpsProvider)
                        {
                            Latitude = result.Latitude,
                            Longitude = result.Longitude
                        };
                    }

                    result = toAddresses.ToList().FirstOrDefault();

                    if (result != null)
                    {
                        UIController.ActiveTrip.ToCoordinate = new BOL_OTR.GeoCoordinate()
                        {
                            Latitude = result.Latitude,
                            Longitude = result.Longitude
                        };

                        tripEndLocation = new Location(LocationManager.GpsProvider)
                        {
                            Latitude = result.Latitude,
                            Longitude = result.Longitude
                        };
                    }
                });

            }));//.Start();

			var locMgr = Application.Context.GetSystemService(Context.LocationService) as LocationManager;

			var provider = LocationManager.NetworkProvider;

			//provider = locMgr.GetBestProvider(locationCriteria, true);
			//if (locMgr.IsProviderEnabled (provider)) {				
			//locMgr.RequestLocationUpdates (provider, 5000, 0, this);
			locMgr.RequestLocationUpdates (provider, 5000, 0, this);
			//locMgr.RequestSingleUpdate (provider, this, null);
			var loc = locMgr.GetLastKnownLocation (provider);

		}

        protected override void OnDestroy()
        {
            //Android.Support.V4.Content.LocalBroadcastManager.GetInstance(this).UnregisterReceiver(drbr);
            UnregisterReceiver(permissionReciever);
            base.OnDestroy();
        }
        public void RevertToLastAction()
		{
			if (UIController.ActiveTrip.TripLogs.Count == 0)
				return;

			var lastLog = UIController.ActiveTrip.TripLogs.LastOrDefault (null);

			if (lastLog != null) {
				UIController.ActiveTrip.TripDetailedStatus = lastLog.TripStatus;
				UIController.ActiveTrip.TripDetailedSetOn = lastLog.LogTime;
				UIController.ActiveTrip.TripLogs.Remove (lastLog);

				UpdateTripStatus (lastLog.TripStatus, true);
			}
		}

		public BOL_OTR.TripDetailedStatusOptions MoveToNextState()
		{
			switch (UIController.ActiveTrip.TripDetailedStatus)
            {
                case BOL_OTR.TripDetailedStatusOptions.RequestingToAcceptFromDriver:
                    return BOL_OTR.TripDetailedStatusOptions.GoingToPickupLocation;
                case BOL_OTR.TripDetailedStatusOptions.GoingToPickupLocation:
                    return BOL_OTR.TripDetailedStatusOptions.ArrivedAtPickupLocation;
                case BOL_OTR.TripDetailedStatusOptions.ArrivedAtPickupLocation:
                case BOL_OTR.TripDetailedStatusOptions.ArrivedToStop:
                    if (ActiveTrip.HaveStopsToDo)
                    {
                        return BOL_OTR.TripDetailedStatusOptions.GoingToStop;
                    }
                    return BOL_OTR.TripDetailedStatusOptions.GoingToDestination;
                case BOL_OTR.TripDetailedStatusOptions.GoingToStop:
                    return BOL_OTR.TripDetailedStatusOptions.ArrivedToStop;
                case BOL_OTR.TripDetailedStatusOptions.GoingToDestination:
                    return BOL_OTR.TripDetailedStatusOptions.HeadingFinalDestination;
                case BOL_OTR.TripDetailedStatusOptions.HeadingFinalDestination:
                    return BOL_OTR.TripDetailedStatusOptions.ArrivedAtDestination;
                case BOL_OTR.TripDetailedStatusOptions.ArrivedAtDestination:
                    return BOL_OTR.TripDetailedStatusOptions.TripPayment;
                case BOL_OTR.TripDetailedStatusOptions.TripPayment:
                    return BOL_OTR.TripDetailedStatusOptions.TripCompleted;
                default:
                    return BOL_OTR.TripDetailedStatusOptions.None;
			}
		}

		public void UpdateTripStatus(BOL_OTR.TripDetailedStatusOptions status, bool uiOnly = false)
		{
			if (!uiOnly) {
				var currentTime = DateTime.Now;

				UIController.ActiveTrip.TripDetailedStatus = status;
				UIController.ActiveTrip.TripDetailedSetOn = currentTime;

				UIController.ActiveTrip.TripLogs.Add(new BOL_OTR.TripLog()
                {
                    LogTime = currentTime,
                    TripStatus = status
                });			
			}

            switch (UIController.ActiveTrip.TripDetailedStatus)
            {
                case BOL_OTR.TripDetailedStatusOptions.GoingToPickupLocation:
                    tvTripDetailedStatus.Text = "Going to Pickup";
                    btnMoveToNextState.Text = "Arrived at Pickup";
                    break;
                case BOL_OTR.TripDetailedStatusOptions.ArrivedAtPickupLocation:
                    tvTripDetailedStatus.Text = "Waiting For Customer";
                    btnMoveToNextState.Text = "Trip Started";

                    if (!uiOnly)
                        StartWaitingCounter();
                    break;
                //case BOL_OTR.TripDetailedStatusOptions.WaitingForCustomer:
                //    tvTripDetailedStatus.Text = "Waiting for Customer";
                //    btnMoveToNextState.Text = "Going To Destination";
                //    btnAddStop.Enabled = false;
                //    break;
                case BOL_OTR.TripDetailedStatusOptions.GoingToStop:
                    tvTripDetailedStatus.Text = "On way to Stop";
                    btnMoveToNextState.Text = "Arrived to Stop";

                    if (!uiOnly)
                        StopWaitingCounter();
                    break;
                case BOL_OTR.TripDetailedStatusOptions.ArrivedToStop:
                    tvTripDetailedStatus.Text = "Arrived at stop";

                    if (!uiOnly)
                    {
                        ActiveTrip.StopsDone++;
                        UpdateStopsCount();
                        StartWaitingCounter();
                    }

                    if (ActiveTrip.HaveStopsToDo)
                        btnMoveToNextState.Text = "Going to Stop";
                    else
                        btnMoveToNextState.Text = "Going to Destination";

                    break;
                case BOL_OTR.TripDetailedStatusOptions.GoingToDestination:
                    tvTripDetailedStatus.Text = "On way to Destination";
                    btnMoveToNextState.Text = "Final Destination";

                    if (!uiOnly)
                        StopWaitingCounter();
                    break;
                case BOL_OTR.TripDetailedStatusOptions.HeadingFinalDestination:
                    tvTripDetailedStatus.Text = "On way to Destination";
                    btnMoveToNextState.Text = "Arrived at Destination";

                    btnChangeDestination.Enabled = true;
                    break;
                case BOL_OTR.TripDetailedStatusOptions.ArrivedAtDestination:
                    tvTripDetailedStatus.Text = "Arrived at Destination";
                    btnMoveToNextState.Text = "Enter Payment";
                    break;
                case BOL_OTR.TripDetailedStatusOptions.TripPayment:
                    tvTripDetailedStatus.Text = "Arrived at Destination";
                    btnMoveToNextState.Text = "Trip Completed";
                    break;
                default:
                    break;
            }

            //UIController.ltdServiceBinder.GetListenToDispatchingServiceBinder().UpdateSignalRServiceWithDetailedTripSatus(UIController.ActiveTrip.TripDetailedStatus);           
		}

        public void StartWaitingCounter()
        {
            startWaiting = DateTime.Now;
            waitingTimer.Change(0, 1000);
        }

        public void StopWaitingCounter()
        {
            waitingTimer.Change(Timeout.Infinite, Timeout.Infinite);

            if (startWaiting.HasValue)
            {
                totalWaitingSeconds += (DateTime.Now - startWaiting).Value.TotalSeconds;
                tvTotalWaitingTime.Text = new TimeSpan(0, 0, (int)totalWaitingSeconds).ToString("hh\\:mm\\:ss");
            }

            tvWaitingTime.Text = "";
            startWaiting = null;
        }

		public void GetPayment()
		{
			var intent = new Intent(this, typeof(TripPaymentActivity));
			StartActivityForResult(intent, 0);
		}

        void UpdateStopsCount()
        {
            tvStopsCount.Text = string.Format("{0} Of {1}", ActiveTrip.StopsDone, ActiveTrip.TotalStops);
        }

        void StopsChanged()
        {
            UpdateStopsCount();

            if (ActiveTrip.HaveStopsToDo)
            {
                btnAddStop.Text = ChangeStops;

                if (ActiveTrip.TripDetailedStatus == BOL_OTR.TripDetailedStatusOptions.GoingToDestination)
                {
                    UpdateTripStatus(BOL_OTR.TripDetailedStatusOptions.GoingToStop, true);
                }
                else if (ActiveTrip.TripDetailedStatus == BOL_OTR.TripDetailedStatusOptions.ArrivedToStop)
                {
                    UpdateTripStatus(ActiveTrip.TripDetailedStatus, true);
                }
            }
            else
            {
                btnAddStop.Text = AddStops;

                if (ActiveTrip.TripDetailedStatus == BOL_OTR.TripDetailedStatusOptions.GoingToStop)
                {
                    UpdateTripStatus(BOL_OTR.TripDetailedStatusOptions.GoingToDestination, true);
                }
                else if (ActiveTrip.TripDetailedStatus == BOL_OTR.TripDetailedStatusOptions.ArrivedToStop)
                {
                    UpdateTripStatus(ActiveTrip.TripDetailedStatus, true);
                }
            }
        }

        void RequestPremissionFromDispatcher()
        {
            UIController.SignalRCalls.SignalR.RequestPermissionFromDispatcher();
        }

        void ReportTripStatusChanged(BOL_OTR.TripDetailedStatusOptions changedTo)
        {
            UIController.SignalRCalls.SignalR.UpdateSignalRServiceWithTripSatus(changedTo);
        }

        void ReportSaveTripDetails()
        {
            UIController.SignalRCalls.SignalR.SaveTripDetails();
        }

        public void OnDispathcerResponded(int requestType, bool isGranted)
        {
            if (isGranted)
            {
                btnChangeDestination.Text = "Granted";
                btnChangeDestination.Enabled = false;
            }
            else
            {
                btnChangeDestination.Text = "Not Granted";
                btnChangeDestination.Enabled = true;
            }
        }

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                if (requestCode == 1)
                {
                    ActiveTrip.TotalStops = int.Parse(data.GetStringExtra("NumberOfStops"));

                    StopsChanged();
                }
            }
            
		}

		public void OnLocationChanged (Location location)
		{
			var speedInM = location.Speed;

            if (speedInM == 0) {

                if (tripStartLocation == null)
                {
                    return;
                }

				var distanceToInM = location.DistanceTo(tripStartLocation);

				var distanceToInF = Length.MetersToFeet(distanceToInM);

//				if (distanceToInF <= 30) {
//					UIController.ActiveTrip.TripDetailedStatus = BOL_OTR.TripDetailedStatusOptions.ArrivedAtDestination;
//					UIController.ActiveTrip.StartWaitingTime = DateTime.Now.TimeOfDay;
//
//					UpdateTripStatus ();
//				}
					
			}

		}

		public void OnProviderDisabled (string provider)
		{
			throw new NotImplementedException ();
		}

		public void OnProviderEnabled (string provider)
		{
			throw new NotImplementedException ();
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{
			throw new NotImplementedException ();
		}

        public void OnRequestAnswered(bool isPermited)
        {
            if (isPermited)
            {
                btnChangeDestination.Text = "Granted";
                btnChangeDestination.Enabled = false;
            }
            else
            {
                btnChangeDestination.Text = "Not Granted";
                btnChangeDestination.Enabled = true;
            }
        }
    }

    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "PermissionGranted", "PermissionRevoked" })]
    public class MyBroadcastReceiver : BroadcastReceiver
    {

        public override void OnReceive(Context context, Intent intent)
        {
            // GET BROADCAST FROM RECEIVER IN THE BACKGROUND SERVICE CLASS
            if (intent.Action == "LocationData")
            {
                double lat = intent.GetDoubleExtra("Latitude", 0);
                double lng = intent.GetDoubleExtra("Longitude", 1);
            }
        }
    }

    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { "PermissionGranted", "PermissionRevoked" })]
    public class DispatcherResponsBroadcastReceiver : BroadcastReceiver
    {
        public static readonly string Active_Trip_Listener = "Active_Trip_Listener";

        public ActiveTripActivity ActiveTripRef;

        public override void OnReceive(Context context, Intent intent)
        {
            bool permited = intent.Action == "PermissionGranted";

            if (intent.Action == "PermissionGranted" || intent.Action == "PermissionRevoked")
            {
                try
                {
                    if (ActiveTripRef != null)
                        ActiveTripRef.OnDispathcerResponded(0, permited);

                }
                catch (Exception ex)
                {
                    Toast.MakeText(context, ex.Message, ToastLength.Short).Show();
                }
            }

            //int requestType;
            //var rt = intent.GetStringExtra("requestType");
            //int.TryParse(rt, out requestType);

            //bool isGranted;
            //var ig = intent.GetStringExtra("isGranted");
            //bool.TryParse(ig, out isGranted);

            //if (ActiveTripRef != null)
            //    ActiveTripRef.OnDispathcerResponded(0, isGranted);
        }

        
    }
}

