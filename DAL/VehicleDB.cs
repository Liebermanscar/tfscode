﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{
    public class VehicleDB : Database
    {
        public VehicleDB(SqlConnection conn, SqlTransaction trans)
            : base(conn, trans)
        {

        }

        public ObservableCollection<Vehicle> Get(int vehicleID)
        {
            var vehicles = new ObservableCollection<Vehicle>();

            using (SqlDataReader dr = this.GetReader("GetVehicles", new SqlParameter[] { new SqlParameter("@VehicleID", vehicleID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    vehicles.Add(new Vehicle()
                    {
                        VehicleID = Convert.ToInt32(dr["VehicleID"]),
                        VehicleCode = dr["VehicleCode"].ToString(),
                        Make = dr["Make"].ToString(),
                        Model = dr["Model"].ToString(),
                        Year = Convert.ToInt32(dr["Year"]),
                        Image = dr["VehicleImage"] == DBNull.Value ? null : (byte[])dr["VehicleImage"],
                        IsCompanyEZPass = Convert.ToBoolean(dr["IsCompanyEZPass"])
                    });
                }
            }

            return vehicles;
        }

        private bool Add(Vehicle vehicle)
        {
            string serverMessage;
            var outParam = new SqlParameter("@VehicleID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddVehicles", new SqlParameter[]
            {
               new SqlParameter("@VehicleCode", vehicle.VehicleCode),
               new SqlParameter("@Make", vehicle.Make),
               new SqlParameter("@Model", vehicle.Model),
               new SqlParameter("@Year", vehicle.Year),
               new SqlParameter("@IsCompanyEZPass", vehicle.IsCompanyEZPass),
               outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                vehicle.VehicleID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }

        private bool Edit(Vehicle vehicle)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateVehicles", new SqlParameter[]
            {
               new SqlParameter("@VehicleID", vehicle.VehicleID),
               new SqlParameter("@VehicleCode", vehicle.VehicleCode),
               new SqlParameter("@Make", vehicle.Make),
               new SqlParameter("@Model", vehicle.Model),
               new SqlParameter("@Year", vehicle.Year),
               new SqlParameter("@VehicleImage", vehicle.Image == null ? null : vehicle.Image),
               new SqlParameter("@IsCompanyEZPass", vehicle.IsCompanyEZPass)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);



            return noOfRecordsAffected > 0;
        }

        public static ObservableCollection<Vehicle> GetVehicles(int vehicleID = 0)
        {
            using (VehicleDB vDB = new VehicleDB(null, null))
            {
                return vDB.Get(vehicleID);
            }
        }

        public static bool EditVehicle(Vehicle vehicle)
        {
            using (VehicleDB vDB = new VehicleDB(null, null))
            {
                return vDB.Edit(vehicle);
            }
        }

        public static bool AddVehicle(Vehicle vehicle)
        {
            using (VehicleDB vDB = new VehicleDB(null, null))
            {
                return vDB.Add(vehicle);
            }
        }


    }


    public class CommisionTypesDB : Database
    {
        public CommisionTypesDB(SqlConnection conn, SqlTransaction trans)
            : base(conn, trans) { }
        public ObservableCollection<CommisionType> Get(int commisionTypeID)
        {
            var commisionTypes = new ObservableCollection<CommisionType>();
            using (SqlDataReader dr = this.GetReader("GetCommisionTypes", new SqlParameter[] { new SqlParameter("@CommisionTypeID", commisionTypeID) }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    commisionTypes.Add(new CommisionType()
                    {
                        CommisionTypeID = Convert.ToInt32(dr["CommisionTypeID"]),
                        CommisionTypeCode = dr["CommisionTypeCode"].ToString(),
                        DriversCommision = Convert.ToDouble(dr["DriversCommision"]),
                        PercentOfGasDriverPays = Convert.ToDouble(dr["PercentOfGasDriverPays"]),
                        IsBonusEligible = Convert.ToBoolean(dr["IsBonusEligible"])
                    });
                }
            }
            return commisionTypes;
        }
        private bool Edit(CommisionType commisionType)
        {
            string serverMessage;
            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateCommisionTypes", new SqlParameter[]
            {
                new SqlParameter("@CommisionTypeID", commisionType.CommisionTypeID),
                new SqlParameter("@CommisionTypeCode", commisionType.CommisionTypeCode),
                new SqlParameter("@DriversCommision", commisionType.DriversCommision),
                new SqlParameter("@PercentOfGasDriverPays", commisionType.PercentOfGasDriverPays),
                new SqlParameter("@IsBonusEligible", commisionType.IsBonusEligible)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;

        }
        private bool Add(CommisionType commisionType)
        {
            string serverMessage;
            var outParam = new SqlParameter("@CommisionTypeID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCommisionTypes", new SqlParameter[]
            {
                new SqlParameter("@CommisionTypeCode", commisionType.CommisionTypeCode),
                new SqlParameter("@DriversCommision", commisionType.DriversCommision),
                new SqlParameter("@PercentOfGasDriverPays", commisionType.PercentOfGasDriverPays),
                new SqlParameter("@IsBonusEligible", commisionType.IsBonusEligible),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                commisionType.CommisionTypeID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }

        public static ObservableCollection<CommisionType> GetCommisionTypes(int commisionTypeID = 0)
        {
            using (CommisionTypesDB ctDB = new CommisionTypesDB(null, null))
            {
                return ctDB.Get(commisionTypeID);
            }
        }

        public static bool EditCommisionType(CommisionType commisionType)
        {
            using (CommisionTypesDB ctDB = new CommisionTypesDB(null, null))
            {
                return ctDB.Edit(commisionType);
            }
        }



        public static bool AddCommisiontype(CommisionType commisionType)
        {
            using (CommisionTypesDB ctDb = new CommisionTypesDB(null, null))
            {
                return ctDb.Add(commisionType);
            }
        }


    }
}
