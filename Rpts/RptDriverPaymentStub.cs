namespace Rpts
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;
    using System.Collections.ObjectModel;
    using BOL;

    /// <summary>
    /// Summary description for RptDriverPaymentStub.
    /// </summary>
    public partial class RptDriverPaymentStub : Telerik.Reporting.Report
    {
        public RptDriverPaymentStub()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        public RptDriverPaymentStub(ObservableCollection<DriverPayment> driverPayment)
            : this()
        {
            this.DocumentName = "Driver Payment Stub";
            this.DataSource = driverPayment;
        }
    
    }
}