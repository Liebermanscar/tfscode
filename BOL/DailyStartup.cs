﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
namespace BOL
{
    public class DailyStartup : BO
    {
        public DailyStartup()
        {

            this.StartupDateAndTime = DateTime.Now;
            this.EnvelopeDate = DateTime.Today;
            this.StatusChangedForTicketBooksGivenToDriver = new ObservableCollection<GenerateTicketBookStatuses>();

        }
        int _dailyStartupID;
        public int DailyStartupID
        {
            get { return _dailyStartupID; }
            set { _dailyStartupID = value; }
        }

        int _forDriverID;
        public int ForDriverID
        {
            get { return _forDriverID; }
            set { _forDriverID = value; this.OnPropertyChanged("AllowSave"); }
        }

        DateTime _startupDateAndTime;
        public DateTime StartupDateAndTime
        {
            get { return _startupDateAndTime; }
            set { _startupDateAndTime = value; this.OnPropertyChanged("AllowSave"); }
        }
        DateTime? _envelopeDate;
        public DateTime? EnvelopeDate
        {
            get { return _envelopeDate; }
            set { _envelopeDate = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("EnvelopeDate"); }
        }

        int _countOfBooksOnHandOfDriver;

        public int CountOfBooksOnHandOfDriver
        {
            get { return _countOfBooksOnHandOfDriver; }
            set { _countOfBooksOnHandOfDriver = value; this.OnPropertyChanged("CountOfBooksOnHandOfDriver"); }
        }
        int _countOfOutstandingEnvelopes;

        public int CountOfOutstandingEnvelopes
        {
            get { return _countOfOutstandingEnvelopes; }
            set { _countOfOutstandingEnvelopes = value; this.OnPropertyChanged("CountOfOutstandingEnvelopes"); }
        }


        public override bool AllowSave
        {
            get
            {
                return this.ForDriverID > 0
                & this.StartupDateAndTime.Date != DateTime.MinValue;
                // & this.EnvelopeDate != DateTime.MinValu  }

            }
        }
        ObservableCollection<GenerateTicketBookStatuses> _statusChangedForTicketBooksGivenToDriver;

        public ObservableCollection<GenerateTicketBookStatuses> StatusChangedForTicketBooksGivenToDriver
        {
            get { return _statusChangedForTicketBooksGivenToDriver; }
            set { _statusChangedForTicketBooksGivenToDriver = value; }
        }
    }
}
