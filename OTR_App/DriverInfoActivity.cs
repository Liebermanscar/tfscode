using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
    [Activity(Label = "Driver Info")]
    public class DriverInfoActivity : Activity
    {
        TextView tvDriverID;
        TextView tvDFName;
        TextView tvDLName;
        TextView tvComm;
        TextView tvGas;
        CheckBox tvComEZPass;
        TextView tvEZPassTNum;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.DriverInfo);

            tvDriverID = FindViewById<TextView>(Resource.Id.driverID);
            tvDFName = FindViewById<TextView>(Resource.Id.fname);
            tvDLName = FindViewById<TextView>(Resource.Id.lname);
            tvComm = FindViewById<TextView>(Resource.Id.comm);
            tvGas = FindViewById<TextView>(Resource.Id.gas);
            tvComEZPass = FindViewById<CheckBox>(Resource.Id.EZPass);
            tvEZPassTNum = FindViewById<TextView>(Resource.Id.EZPassTNum);

            tvDriverID.Text = UIController.Modal.DriverInfo.DriverID.ToString();
            tvDFName.Text = UIController.Modal.DriverInfo.FirstName;
            tvDLName.Text = UIController.Modal.DriverInfo.Lastname;
            tvComm.Text = UIController.Modal.DriverInfo.CommissionType;
            tvGas.Text = UIController.Modal.DriverInfo.PecentageGasDriverPays.ToString();
            tvComEZPass.Checked = UIController.Modal.DriverInfo.IsCompanyEZPass;
            tvEZPassTNum.Text = UIController.Modal.DriverInfo.EZPassTagNumber;

            Button btnOK = FindViewById<Button>(Resource.Id.btnOK);

            btnOK.Click += (object sender, EventArgs e) =>
                            {
                                Finish();
                            };

        }

       
    }
}