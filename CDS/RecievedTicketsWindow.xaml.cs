﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for RecievedTicketsWindow.xaml
    /// </summary>
    public partial class RecievedTicketsWindow : Window
    {
        public RecievedTicketsWindow()
        {
            InitializeComponent();
        }
        public RecievedTicketsWindow(TicketsReceived ticketsReceived)
            : this()
        {
            this.ActiveTicketsReceived = ticketsReceived;
            if (ticketsReceived.DailyEnvelopID > 0)
            {
                LockTableForDelete();
            }
        }

        TicketsReceived ActiveTicketsReceived
        {
            get
            {
                return this.DataContext as TicketsReceived;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //by editing an existing DailyEnvelope, if its an existing log still need to remove from db
            this.ActiveTicketsReceived.DETicketLogs.Remove(this.RecievedTicketsDataGrid.SelectedItem as DETicketLog);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ActiveTicketsReceived = null;
        }

        private void LockTableForDelete()
        {
            RecievedTicketsDataGrid.ContextMenu = null;

        }



        /*
        private void RecievedTicketsDataGrid_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            if (ActiveTicketsReceived.DailyEnvelopID > 0)
            {
                DETicketLog tl = e.Row.DataContext as DETicketLog;
                if (tl.TicketLogId > 0)
                {
                    e.Row.IsEnabled = false;
                    this.ActiveDETicketLog = tl;
                }
            }
        }*/

        private void RecievedTicketsDataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            DETicketLog tl = e.Row.Item as DETicketLog;
            if (tl != null && tl.TicketLogId > 0)
            {
                e.Row.IsEnabled = false;
            }

            /*
            if (ActiveTicketsReceived.DailyEnvelopID > 0)
            {
                DETicketLog tl = e.Row.DataContext as DETicketLog;
                if (tl.TicketLogId > 0)
                {
                    e.Row.IsEnabled = false;

                }
            }*/
        }

        private void RecievedTicketsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            DETicketLog tl = e.Row.DataContext as DETicketLog;
            if (tl.TicketNumber == 0)
            {
                this.ActiveTicketsReceived.DETicketLogs.Remove(this.RecievedTicketsDataGrid.SelectedItem as DETicketLog);
            }
            else
            {
                DETicketLog activeDEtl = this.RecievedTicketsDataGrid.SelectedItem as DETicketLog;
                if (!DAL.TicketBookStatusDB.IsBookSold(activeDEtl.TicketNumber, this.ActiveTicketsReceived.FromTicketBook.TicketBookID))
                {
                    MessageBox.Show("This Book Is Still Not Sold!!", "Alert", MessageBoxButton.OK);
                }
            }
        }
    }
}
