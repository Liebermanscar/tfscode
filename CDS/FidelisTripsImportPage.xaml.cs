﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for FidelisTripsImportPage.xaml
    /// </summary>
    public partial class FidelisTripsImportPage : Page, IDisplayTextBlock
    {
        public FidelisTripsImportPage()
        {
            InitializeComponent();
        }

        public FidelisTripsImportPage(ObservableCollection<FidelisTrip> fidelisTripes, string fileName) : this()
        {
            this.FidelisTrips = fidelisTripes;
            this.FidelisFileName = fileName;
        }

        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Import Fidelis Trips Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        public ObservableCollection<FidelisTrip> FidelisTrips
        {
            get
            {
                return this.DataContext as ObservableCollection<FidelisTrip>;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.FidelisTrips.Count > 0 && !DAL.FidelisTripDB.FileWasImported(this.FidelisTrips[0].TripScheduledOn.Value))
                {
                    if (DAL.FidelisTripDB.AddFidelisTrips(this.FidelisTrips))
                    {
                        MessageBox.Show(this.FidelisFileName + "\nWas imported", "File Imported", MessageBoxButton.OK, MessageBoxImage.Information);
                        UIController.CloseActivePage();
                    }
                }
                else
                    MessageBox.Show("File was already imported", "Already imported", MessageBoxButton.OK, MessageBoxImage.Stop);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public string FidelisFileName { get; set; }

        private void DataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit)
            {
                FidelisTrip ft = e.Row.DataContext as FidelisTrip;

                if (!ft.AllowSave)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }
    }
}
