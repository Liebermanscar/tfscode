﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public static class PaymentTypes
    {
        public static string Cash = "Cash";
        public static string CC = "CC";
        public static string On_Account = "On_Account";
        public static string Ticket = "Ticket";
        public static string Not_Paid = "Not_Paid";
    }
}
