﻿using BOL;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CDS
{
    public class DispatchingMVVM : INotifyPropertyChanged, IDispatchingMVVM
    {
        public DispatchingMVVM(SelectTripInListCallback selectTripInList, bool isLongDistance)
        {
            this.DriversAndTripInformation = new ObservableCollection<DriverAndTripInformation>();
            this.Trips = new ObservableCollection<Trip>();
            this.StreetAbbreviations = new ObservableCollection<StreetAbbreviation>();

            _selectTripInList = selectTripInList;
            _isLongDistanceMode = isLongDistance;
        }

        SelectTripInListCallback _selectTripInList;
        string _CallerIDInfo;


        public string CallerIDInfo
        {
            get { return _CallerIDInfo; }
            set { _CallerIDInfo = value; this.OnPropertyChanged("CallerIDInfo"); }
        }

        ObservableCollection<Trip> _trips;

        public ObservableCollection<Trip> Trips
        {
            get { return _trips; }
            set { _trips = value; }
        }
        Trip _addedEditedTrip;

        public Trip AddedEditedTrip
        {
            get { return _addedEditedTrip; }
            set
            {
                _addedEditedTrip = value;
                this.OnPropertyChanged("AddedEditedTrip");
                this.OnPropertyChanged("AddEditTripIsVisible");
            }
        }

        public DriverAndTripInformation SignInDriver(Driver driver, string signInLocation)
        {
            if (this.SignedInDrivers.Count(sid => sid.DriverID == driver.DriverID) == 0)
            {

                DriverAndTripInformation dti = new DriverAndTripInformation()
                {
                    UnitDriver = driver,
                    DriverStatus = DriverStatusOptions.NintyEight,
                    CalledInLocation = signInLocation
                };
                this.DriversAndTripInformation.Add(dti);


                this.OnPropertyChanged("SignedInDrivers");

                return dti;
            }
            else
                return null;
        }

        public ObservableCollection<Trip> SortedTrips
        {
            get
            {
                return new ObservableCollection<Trip>(from t in this.Trips
                                                      orderby t.TripStatus descending,
                                                      t.TripStatus == TripStatusTypes.Assigned ? t.TimeDriverAssigned :
                                                      //t.ScheduledFor != null ? t.ScheduledFor : 
                                                      t.ScheduledFor != null ? t.ScheduledFor.Value.AddMinutes(-10) ://add to request at -10 minutes so when its 10 minutes before the reqest at time at should be more on top in list so dispatcher will know to send car
                                                      t.TimeOfCall ascending
                                                      select t);
            }
        }

        ObservableCollection<DriverAndTripInformation> _driversAndTripInformation;
        public ObservableCollection<DriverAndTripInformation> DriversAndTripInformation
        {
            get { return _driversAndTripInformation; }
            set
            {
                _driversAndTripInformation = value;
                value.CollectionChanged += delegate (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
                {
                    this.OnPropertyChanged("SortedDriversAndTrips");
                };
            }
        }

        public ObservableCollection<DriverAndTripInformation> SortedDriversAndTrips
        {
            get
            {
                var list = new ObservableCollection<DriverAndTripInformation>(
                    from d in this.DriversAndTripInformation
                    orderby d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                    where d.DriverStatus == DriverStatusOptions.NintyEight || d.DriverStatus == DriverStatusOptions.ShortBreak
                    select d);
                list.AddRange(from d in this.DriversAndTripInformation
                              orderby d.DriverStatus, d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay, d.AssignedToTrip != null ? d.AssignedToTrip.TimeDriverAssigned.Value.TimeOfDay : d.InternalStatusChangedTime.GetValueOrDefault(DateTime.Now).TimeOfDay
                              where d.DriverStatus == DriverStatusOptions.Assigned || d.DriverStatus == DriverStatusOptions.LongBreak
                              select d);

                return list;
            }
        }

        public ObservableCollection<Driver> SignedInDrivers
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         select dt.UnitDriver));

            }
        }

        public ObservableCollection<Driver> DriversOnBreak
        {
            get
            {
                return new ObservableCollection<Driver>((from dt in this.DriversAndTripInformation
                                                         where dt.DriverStatus == DriverStatusOptions.LongBreak || dt.DriverStatus == DriverStatusOptions.ShortBreak
                                                         select dt.UnitDriver));

            }
        }

        public void AddTripToList(Trip tripToAdd)
        {
            if (tripToAdd.DrivenBy == null || this.AssignDriverToTrip(tripToAdd, tripToAdd.DrivenBy, false))
            {
                //tripToAdd.TripStatus = TripStatusTypes.ReceivedCall;
                tripToAdd.TripNo = this.Trips.Count == 0 ? 1 : this.Trips.Max(t => t.TripNo) + 1;
                tripToAdd.IsLongDistanceTrip = this.IsLongDistanceMode;

                this.Trips.Add(tripToAdd);
                if (tripToAdd.TripID == 0)
                    DAL.TripDB.AddTrip(tripToAdd);//
                this.AddedEditedTrip = null;
                this.ChangeTripStatus(tripToAdd, tripToAdd.DrivenBy == null ? TripStatusTypes.ReceivedCall : TripStatusTypes.Assigned, tripToAdd.DrivenBy, false);

                if (_selectTripInList != null)
                    _selectTripInList.Invoke(tripToAdd, true);

                this.OnPropertyChanged("SortedTrips");

                // DAL.TripDB.AddTrip(tripToAdd);
            }

        }
        public void AddMedicadTripNo(MedicaidTrip mt)
        {
            //MedicaidTrip tt = new MedicaidTrip();
            //tt.MedicaidDispatchTripNo = 1;
            // tt.
        }

        public bool AssignDriverToTrip(Trip trip, Driver driver, bool warnDriverChanged = true)
        {
            if (driver != null && trip.TripStatus != TripStatusTypes.Complete)
            {
                DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).First();

                if (dti == null)
                {
                    dti = this.SignInDriver(driver, "");
                }

                if (dti != null)
                {
                    if (dti.AssignedToTrip != null)
                    {
                        AssignedUnitDiscrepencyOption optionSelected = this.GetAssignedUnitOptions();
                        switch (optionSelected)
                        {
                            case AssignedUnitDiscrepencyOption.FinishedFirst:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Complete, null, false);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                                return true;
                            case AssignedUnitDiscrepencyOption.ReassignToNew:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.ReceivedCall, null, false);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                                return true;
                            case AssignedUnitDiscrepencyOption.CanceledFirst:
                                this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Canceled, null, false);
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                                return true;
                            case AssignedUnitDiscrepencyOption.Combine:
                                dti.AssignedSecondTrip = dti.AssignedToTrip;
                                this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                                return true;
                            case AssignedUnitDiscrepencyOption.Cancel:
                                return false;
                            default:
                                return false;
                        }
                    }
                    else
                    {
                        if (trip.DrivenBy != null && warnDriverChanged)
                        {
                            if (MessageBox.Show("You're about to replace the driver of this trip.\n\nContinue?", "Replace Driver?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                                this.ChangeTripStatus(trip, TripStatusTypes.ReceivedCall, null, true);
                            else
                                return false;
                        }
                        this.ChangeTripStatus(trip, TripStatusTypes.Assigned, driver, false);
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Driver has not Signed in", "NOT Signed in", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

            }
            else
                return false;
        }

        public void ChangeTripStatus(Trip trip, TripStatusTypes status, Driver driver, bool driverReplaced)
        {
            try
            {
                switch (status)
                {
                    case TripStatusTypes.ReceivedCall:
                        trip.TripStatus = status;
                        trip.TimeDriverAssigned = null;
                        Driver wasAssigned = trip.DrivenBy;
                        if (wasAssigned != null)
                        {
                            DriverAndTripInformation dti1 = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == wasAssigned.DriverID).First();
                            if (dti1 != null)
                            {
                                if (driverReplaced)
                                {
                                    DateTime? oldStatusChangedTime = dti1.BackupData.StatusChangedTime;
                                    dti1.DriverStatus = dti1.BackupData.DriverStatus;
                                    dti1.StatusChangedTime = oldStatusChangedTime;
                                }
                                else
                                    dti1.DriverStatus = DriverStatusOptions.NintyEight;
                                dti1.AssignedToTrip = null;

                            }
                        }
                        trip.DrivenBy = null;
                        break;
                    case TripStatusTypes.Assigned:
                        if (trip.DrivenBy != null)
                            this.ChangeTripStatus(trip, TripStatusTypes.ReceivedCall, trip.DrivenBy, true);
                        trip.TripStatus = TripStatusTypes.Assigned;
                        //if(trip.DrivenBy != null && trip.DrivenBy != driver)
                        //{
                        //    this.ChangeDriversStatus(trip.DrivenBy, DriverStatusOptions.

                        //    }
                        trip.DrivenBy = driver;
                        trip.TimeDriverAssigned = DateTime.Now;
                        trip.TripStarted = DateTime.Now;

                        DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).First();
                        dti.LastTrip = dti.AssignedToTrip;
                        dti.AssignedToTrip = trip;
                        dti.DriverStatus = DriverStatusOptions.Assigned;
                        break;
                    case TripStatusTypes.InProcess:
                        break;
                    case TripStatusTypes.Complete:
                        DriverAndTripInformation driverTI = this.DriversAndTripInformation.Where(d => d.AssignedToTrip == trip || d.AssignedSecondTrip == trip).First();
                        DateTime timeCompleted = DateTime.Now;
                        if (driverTI != null)
                        {
                            driverTI.DriverStatus = DriverStatusOptions.NintyEight;
                            driverTI.StatusChangedTime = timeCompleted;
                            //driverTI.InternalStatusChangedTime = 
                            driverTI.LastTrip = driverTI.AssignedToTrip;
                            driverTI.AssignedToTrip = null;
                        }
                        trip.TripStatus = TripStatusTypes.Complete;
                        trip.TripEnded = timeCompleted;
                        break;

                    case TripStatusTypes.Canceled:
                        int i = this.DriversAndTripInformation.Count(d => d.AssignedToTrip == trip);
                        if (i > 0)
                        {
                            DriverAndTripInformation driverTI2 = this.DriversAndTripInformation.Where(d => d.AssignedToTrip == trip).First();
                            DateTime? oldStatusChangedTime = driverTI2.BackupData.StatusChangedTime;
                            driverTI2.DriverStatus = DriverStatusOptions.NintyEight;
                            driverTI2.StatusChangedTime = oldStatusChangedTime;
                            driverTI2.AssignedToTrip = null;
                        }
                        trip.TripStatus = TripStatusTypes.Canceled;
                        trip.DrivenBy = null;
                        break;
                    default:
                        break;
                }
                this.OnPropertyChanged("SortedDriversAndTrips");

                //     DAL.TripDB.EditTrip(trip);
                //  this.OnPropertyChanged("SortedTrips");
                if (trip.TripID == 0)
                    DAL.TripDB.AddTrip(trip);
                else
                    DAL.TripDB.EditTrip(trip);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public void MarkDriverForBreak(Driver driver, DriverStatusOptions breakType)
        {
            Trip completeFirstTrip = (from d in this.DriversAndTripInformation
                                      where d.UnitDriver.DriverID == driver.DriverID
                                      select d.AssignedToTrip).ToList()[0];
            if (completeFirstTrip != null)
                this.ChangeTripStatus(completeFirstTrip, TripStatusTypes.Complete, driver, false);

            this.ChangeDriversStatus(driver, breakType);
        }

        public void Undo98Status(DriverAndTripInformation dti)
        {
            dti.Undo98Status();
            this.OnPropertyChanged("SortedDriversAndTrips");
            this.OnPropertyChanged("SortedTrips");
        }

        public void ChangeDriversStatus(Driver driver, DriverStatusOptions assignStatus)
        {
            DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).First();
            if (dti != null)
            {
                dti.DriverStatus = assignStatus;
                this.OnPropertyChanged("SortedDriversAndTrips");
            }
        }

        public void RefereshStatusTimesOnDrivers()
        {

        }


        AssignedUnitDiscrepencyOption GetAssignedUnitOptions()
        {
            AssignUnitDiscepencyWindow audw = new AssignUnitDiscepencyWindow();
            if (audw.ShowDialog().GetValueOrDefault(false))
                return (AssignedUnitDiscrepencyOption)audw.DiscrepencyOptionsListBox.SelectedIndex;
            else
                return AssignedUnitDiscrepencyOption.Cancel;
        }

        public void AddEditTrip(Trip trip)
        {
            if (trip == null)
                trip = new Trip() { ObjectState = BOState.New, TripEnteredBy = TripEnteredOptions.ByDispatcher, VehicleTypeRequested = VehicleTypes.Not_Specified };
            this.AddedEditedTrip = trip;
            this.OnPropertyChanged("AddEditTripIsVisible");
        }

        public Visibility AddEditTripIsVisible
        {
            get
            {
                if (this.AddedEditedTrip != null)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;

            }
        }

        ObservableCollection<StreetAbbreviation> _streetAbbreviations;

        public ObservableCollection<StreetAbbreviation> StreetAbbreviations
        {
            get { return _streetAbbreviations; }
            set { _streetAbbreviations = value; this.OnPropertyChanged("StreetAbbreviations"); }
        }

        public void CancelAddEditTrip()
        {
            this.AddedEditedTrip = null;
        }

        public void SignOutDriver(Driver driver)
        {
            DriverAndTripInformation dti = this.DriversAndTripInformation.Where(d => d.UnitDriver.DriverID == driver.DriverID).First();
            if (dti != null)
            {
                if (dti.AssignedToTrip != null)
                    this.ChangeTripStatus(dti.AssignedToTrip, TripStatusTypes.Complete, driver, false);
                if (dti.AssignedSecondTrip != null)
                {
                    this.ChangeTripStatus(dti.AssignedSecondTrip, TripStatusTypes.Complete, driver, false);
                    dti.AssignedSecondTrip = null;
                }
                this.DriversAndTripInformation.Remove(dti);
            }
        }

        public void ResetDispatchingInformation()
        {
            this.Trips.Clear();
            this.DriversAndTripInformation.Clear();
        }


        public void EditTrip(Trip trip)
        {
            try
            {
                DAL.TripDB.EditTrip(trip);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        public void DeleteTrip(Trip trip)
        {
            if (trip == null)
                return;

            try
            {
                this.Trips.Remove(trip);
                DAL.TripDB.DeleteTrip(trip);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        public string[] GetLogOnOff()
        {
            return null;
        }

        public void ChangeDriverLocation(DriverAndTripInformation driverAndTripInformation, string newLocation)
        {
            driverAndTripInformation.CalledInLocation = newLocation;
        }

        public void UpdateDriverTripTimers()
        {
            this.OnPropertyChanged("SortedDriversAndTrips");
        }

        bool _isLongDistanceMode;

        public bool IsLongDistanceMode
        {
            get { return _isLongDistanceMode; }
            set { _isLongDistanceMode = value; }
        }

        bool _playSoundsForNewTrips;

        public bool PlaySoundsForNewTrips
        {
            get { return _playSoundsForNewTrips; }
            set { _playSoundsForNewTrips = value; }
        }

        public ObservableCollection<DriverPermissionRequest> DriversPermissionRequests
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        public void ProcessScheduledTrip()
        {
            throw new NotImplementedException();
        }

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion


        public void UpdateSectionCarAvailability(string sectionName, CarsAvailabilityTypes availability)
        {
            throw new NotImplementedException();
        }

        public void AddTripLocation(Trip trip)
        {
            throw new NotImplementedException();
        }
    }
}
