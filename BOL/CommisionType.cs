﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    [DataContract]
    public class CommisionType : BO
    {
        int _commisionTypeID;

        [DataMember]
        public int CommisionTypeID
        {
            get { return _commisionTypeID; }
            set { _commisionTypeID = value; }
        }
        string _commisionTypeCode;

        [DataMember]
        public string CommisionTypeCode
        {
            get { return _commisionTypeCode; }
            set { _commisionTypeCode = value; this.OnPropertyChanged("AllowSave"); }
        }
        double _driversCommision;

        [DataMember]
        public double DriversCommision
        {
            get { return _driversCommision; }
            set { _driversCommision = value; }
        }
        double _percentOfGasDriverPays;

        [DataMember]
        public double PercentOfGasDriverPays
        {
            get { return _percentOfGasDriverPays; }
            set { _percentOfGasDriverPays = value; }
        }

        bool _IsBonusEligible;

        public bool IsBonusEligible
        {
            get { return _IsBonusEligible; }
            set { _IsBonusEligible = value; }
        }
        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.CommisionTypeCode);
            }

        }
    }
}
