﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{

    public class DriversDB : Database
    {
        public DriversDB(SqlConnection conn, SqlTransaction trans)
            : base(conn, trans)
        {

        }

        public ObservableCollection<Driver> Get(bool IncludeInActive, int driverID, bool firstEmpty, bool withLinkedDrivers)
        {
            ObservableCollection<Driver> drivers = new ObservableCollection<Driver>();
            using (SqlDataReader dr = this.GetReader
                ("GetDrivers", new SqlParameter[]
                {
                    new SqlParameter ("@DriverID", driverID),
                     new SqlParameter("@IncludeInactiveDrivers", IncludeInActive)
                }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    Driver driver = new Driver()
                    {
                        DriverID = Convert.ToInt32(dr["DriverID"]),
                        DriverCode = dr["DriverCode"].ToString(),
                        DriverFirstName = dr["DriverFirstName"].ToString(),
                        DriverLastName = dr["DriverLastName"].ToString(),
                        DriverLicenseNumber = dr["LicenseNumber"].ToString(),
                        CellNumber = dr["CellNumber"].ToString(),
                        DriverActive = Convert.ToBoolean(dr["DriverActive"]),
                        AllowsChargeToHisAccount = Convert.ToBoolean(dr["AllowsChargeToHisAccount"]),
                        CellPhoneCarrier = dr["CellPhoneCarrier"].ToString(),
                        IsMain = Convert.ToBoolean(dr["IsMain"]),
                        LinkedDriverID = Convert.ToInt32(dr["LinkedDriverID"]),
                        BonusBracket = Convert.ToDecimal(dr["BonusBracket"]),
                        BonusPercent = Convert.ToDecimal(dr["BonusPercent"]),
                        CurrentLocation = dr["CurrentLocation"].ToString(),
                        DriverStatus = (DriverStatusOptions)Convert.ToInt32(dr["DriverStatus"]),
                        AssignedToTripID = Convert.ToInt32(dr["AssignedToTripID"]),
                        LastTripID = Convert.ToInt32(dr["LastTripID"]),
                        IsLongDistanceDriver = Convert.ToBoolean(dr["IsLongDistanceDriver"]),
                        StatusChangeTime = dr["StatusChangeTime"] == DBNull.Value ? new DateTime?() : Convert.ToDateTime(dr["StatusChangeTime"]),
                        OnCommisionType = new CommisionType()
                        {
                            CommisionTypeID = Convert.ToInt32(dr["CommisionTypeID"]),
                            CommisionTypeCode = dr["CommisionTypeCode"].ToString(),
                            DriversCommision = Convert.ToDouble(dr["DriversCommision"]),
                            PercentOfGasDriverPays = Convert.ToDouble(dr["PercentOfGasDriverPays"])
                        }
                    };

                    if (withLinkedDrivers && driver.LinkedDriverID > 0)
                    {
                        using (DriversDB driverDB = new DriversDB(null, null))
                        {
                            driver.LinkedDriver = driverDB.Get(false, driver.LinkedDriverID, false, false).FirstOrDefault();
                        }

                    }

                    if (dr["VehicleID"] != DBNull.Value)
                    {
                        driver.DrivesVehicle = new Vehicle()
                        {
                            VehicleID = Convert.ToInt32(dr["VehicleID"]),
                            VehicleCode = dr["VehicleCode"].ToString(),
                            Make = dr["Make"].ToString(),
                            Model = dr["Model"].ToString(),
                            Year = Convert.ToInt32(dr["Year"])
                        };
                    };
                    drivers.Add(driver);
                }
            }
            if (firstEmpty)
                drivers.Insert(0, new Driver() { DriverID = 0 });
            return drivers;
        }
        private bool Edit(Driver driver, bool recoveryOnly = false)
        {
            string serverMessage;
            return this.ExecuteNonQuery("UpdateDriver", new SqlParameter[]
            {
               new SqlParameter("@DriverID", driver.DriverID),
               new SqlParameter("@DriverCode", driver.DriverCode),
               new SqlParameter("@DriverFirstName", driver.DriverFirstName),
               new SqlParameter("@DriverLastName", driver.DriverLastName),
               new SqlParameter("@DriverLicenseNumber", driver.DriverLicenseNumber),
               new SqlParameter("@DriverCellNumber", driver.CellNumber),
               new SqlParameter("@IsActiveDriver", driver.DriverActive),
               new SqlParameter("@AllowsChargeToHisAccount", driver.AllowsChargeToHisAccount),
               new SqlParameter("@CommisionTypeID", driver.OnCommisionType.CommisionTypeID),
               new SqlParameter("@DrivesVehicleID", driver.DrivesVehicle == null ? 0 : driver.DrivesVehicle.VehicleID),
               new SqlParameter("@CellPhoneCarrier", driver.CellPhoneCarrier),
               new SqlParameter("@IsMain", driver.IsMain),
               new SqlParameter("@LinkedDriverID", driver.LinkedDriverID),
               new SqlParameter("@BonusBracket", driver.BonusBracket),
               new SqlParameter("@BonusPercent", driver.BonusPercent),
               new SqlParameter("@CurrentLocation", driver.CurrentLocation),
               new SqlParameter("@DriverStatus", driver.DriverStatus),
               new SqlParameter("@AssignedToTripID", driver.AssignedToTripID),
               new SqlParameter("@LastTripID", driver.LastTripID),
               new SqlParameter("@IsLongDistanceDriver", driver.IsLongDistanceDriver),
               new SqlParameter("@StatusChangeTime", driver.StatusChangeTime),
               new SqlParameter("@RecoveryInfoOnly", recoveryOnly)
            }, System.Data.CommandType.StoredProcedure, out serverMessage) > 0;
        }

        private bool Add(Driver driver)
        {
            string serverMessage;
            var outParam = new SqlParameter("@DriverID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddDriver", new SqlParameter[]
            {
               new SqlParameter("@DriverCode", driver.DriverCode),
               new SqlParameter("@DriverFirstName", driver.DriverFirstName),
               new SqlParameter("@DriverLastName", driver.DriverLastName),
               new SqlParameter("@DriverLicenseNumber", driver.DriverLicenseNumber),
               new SqlParameter("@DriverCellNumber", driver.CellNumber),
               new SqlParameter("@IsActiveDriver", driver.DriverActive),
               new SqlParameter("@AllowsChargeToHisAccount", driver.AllowsChargeToHisAccount),
               new SqlParameter("@CommisionTypeID", driver.OnCommisionType.CommisionTypeID),
               new SqlParameter("@DrivesVehicleID", driver.DrivesVehicle == null ? 0 : driver.DrivesVehicle.VehicleID),
               new SqlParameter("@CellPhoneCarrier", driver.CellPhoneCarrier),
               new SqlParameter("@IsMain", driver.IsMain),
               new SqlParameter("@LinkedDriverID", driver.LinkedDriverID),
               new SqlParameter("@BonusBracket", driver.BonusBracket),
               new SqlParameter("@BonusPercent", driver.BonusPercent),
               outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                driver.DriverID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }
        public static ObservableCollection<Driver> GetDrivers(bool includeInAciveDrivers, int driverID = 0, bool firstDriverEmpty = false, bool withLinkedDrivers = false)
        {
            using (DriversDB dDB = new DriversDB(null, null))
            {
                return dDB.Get(includeInAciveDrivers, driverID, firstDriverEmpty, withLinkedDrivers);
            }
        }


        public static bool EditDriver(Driver driver, bool recoveryOnly = false)
        {
            using (DriversDB driverDB = new DriversDB(null, null))
            {
                return driverDB.Edit(driver, recoveryOnly);
            }
        }



        public static bool AddDriver(Driver driver)
        {
            using (DriversDB driverDB = new DriversDB(null, null))
            {
                return driverDB.Add(driver);
            }
        }

        public static bool AuthenticateDriver(string loginUserName, string loginPassword)
        {
            using (DriversDB driverDB = new DriversDB(null, null))
            {
                return driverDB.Authenticate(loginUserName, loginPassword);
            }
        }

        private bool Authenticate(string loginUserName, string loginPassword)
        {
            return (bool)this.ExecuteScalar("AutheticatDriver",
                new SqlParameter[]
                {
                    new SqlParameter("@loginUserName", loginUserName),
                    new SqlParameter("@loginPassword", loginPassword)
                }, System.Data.CommandType.StoredProcedure);
        }
    }
}
