﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;
using Telerik.Windows.Controls;


namespace CDS
{
    /// <summary>
    /// Interaction logic for TicketBookStatusListPage.xaml.xaml
    /// </summary>
    public partial class TicketBookStatusListPage : Page, IDisplayTextBlock
    {
        public TicketBookStatusListPage()
        {
            InitializeComponent();
        }


        public ObservableCollection<TicketBookStatus> ActiveTicketBookStatuses
        {
            get { return this.DataContext as ObservableCollection<TicketBookStatus>; }
            set { this.DataContext = value; }
        }

        TicketBookStatus ActiveTicketBookStatus
        {
            get
            {
                return this.TicketBooksStatusesGrid.SelectedItem as TicketBookStatus;
            }
        }
        #region IDisplayTextBlockMemebrs
        public string HeaderText
        {
            get
            {
                return "Ticket Book Status List Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion



        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            try
            {
                UIController.ExportTelerikGrid(this.TicketBooksStatusesGrid, UIController.ExportFormat.Excel);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ActiveTicketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatuses();//this.dpFromDate.SelectedDate.Value, this.dpTillDate.SelectedDate.Value);
                this.BookNumberField.Header = "Book Number";
                this.AttatchToCustomerMenu.IsEnabled = false;
                this.GivenToColumn.IsReadOnly = false;
                this.DateLoggedColumn.IsVisible = true;
                this.UserLoggedColumn.IsVisible = true;
                this.CustomerIDColumn.IsVisible = true;
                this.NotesColumn.IsVisible = true;
                this.DiscountTypeColumn.IsVisible = true;
                this.PriceSoldColumn.IsVisible = true;
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ActiveTicketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatusesDetail();//this.dpFromDate.SelectedDate.Value, this.dpTillDate.SelectedDate.Value);
                this.BookNumberField.Header = "Book Number";
                this.AttatchToCustomerMenu.IsEnabled = false;
                this.GivenToColumn.IsReadOnly = true;
                this.DateLoggedColumn.IsVisible = true;
                this.UserLoggedColumn.IsVisible = true;
                this.CustomerIDColumn.IsVisible = false;
                this.NotesColumn.IsVisible = false;
                this.DiscountTypeColumn.IsVisible = false;
                this.PriceSoldColumn.IsVisible = false;
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ActiveTicketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatuses(GetCount: true);
                this.BookNumberField.Header = "Count of books";
                this.AttatchToCustomerMenu.IsEnabled = true;
                this.GivenToColumn.IsReadOnly = true;
                this.DateLoggedColumn.IsVisible = false;
                this.UserLoggedColumn.IsVisible = false;
                this.CustomerIDColumn.IsVisible = false;
                this.NotesColumn.IsVisible = false;
                this.DiscountTypeColumn.IsVisible = false;
                this.PriceSoldColumn.IsVisible = false;
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void RadMenuItemAttachToCustomer_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            try
            {
                TicketBookStatus tbs = this.ActiveTicketBookStatus;
                if (tbs != null && tbs.CustomerIdSold == 0)
                {
                    ObservableCollection<Customer> customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, tbs.SoldToInfo, DataRequestTypes.LiteData, false);

                    if (customers.Count == 0)
                    {
                        return;
                        /*customers = UIController.GetCustomer(CustomerFilterOptions.Automatic, "", DataRequestTypes.LiteData, false);*/
                    }
                    if (customers.Count == 1)
                        tbs.CustomerIdSold = customers[0].CustomerID;
                    else if (customers.Count > 1)
                        tbs.CustomerIdSold = UIController.SelectCustomerFromList(customers).CustomerID;

                    DAL.TicketBookStatusDB.EditTicketBookStatus(tbs, forUpdateCustID: true);
                    //refresh the list
                    this.ActiveTicketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatuses(GetCount: true);
                }

            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }

        }

        private void TicketBooksStatusesGrid_RowEditEnded(object sender, Telerik.Windows.Controls.GridViewRowEditEndedEventArgs e)
        {
            if (this.ActiveTicketBookStatus.BookStatus == TicketBookStatusOptions.Sold)
            {
                DAL.TicketBookStatusDB.EditTicketBookStatus(this.ActiveTicketBookStatus, forUpdateSoldToInfo: true);
            }
        }


        private void Button_AutoAttach(object sender, RoutedEventArgs e)
        {
            try
            {
                DAL.TicketBookStatusDB.EditTicketBookStatus(new TicketBookStatus(), forAutoAttach: true);
                MessageBox.Show("Successfully attached", "Success", MessageBoxButton.OK);
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void Button_LoadSummeryWindow(object sender, RoutedEventArgs e)
        {
            UIController.LoadTicketBookSummeryPage();
        }

        private void TicketBooksStatusesGrid_CellEditEnded(object sender, Telerik.Windows.Controls.GridViewCellEditEndedEventArgs e)
        {
            //in row edit we try to reattach the customer so if its already attached, need first to remove the customer id
            if (this.ActiveTicketBookStatus.CustomerIdSold != 0 && e.Cell.Column.UniqueName == "GivenTo" && e.NewData != e.OldData)
            {
                this.ActiveTicketBookStatus.GivenTo = (string)e.OldData;
                MessageBox.Show("You can not re-attach that ticket, You need first to reset the customer id in the Customer ID column to 0");
            }
        }


    }
}
