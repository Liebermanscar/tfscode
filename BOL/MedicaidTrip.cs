﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BOL
{
    public class MedicaidTrip : BO
    {



        public MedicaidTrip() { this.RecordType = "Leg"; }
        int _medicaidTripID;

        public int MedicaidTripID
        {
            get { return _medicaidTripID; }
            set { _medicaidTripID = value; }
        }
        int _exportID;

        public int ExportID
        {
            get { return _exportID; }
            set { _exportID = value; }
        }
        int _recordNo;

        public int RecordNo
        {
            get { return _recordNo; }
            set { _recordNo = value; }
        }
        int _invoiceNo;

        public int InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }
        string _recordType;

        public string RecordType
        {
            get { return _recordType; }
            set { _recordType = value; }
        }
        string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        string _middleInitial;

        public string MiddleInitial
        {
            get { return _middleInitial; }
            set { _middleInitial = value; }
        }
        string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; this.OnPropertyChanged("AllowSave"); }
        }
        string _CIN;

        public string CIN
        {
            get { return _CIN; }
            set { _CIN = value; this.OnPropertyChanged("AllowSave"); }
        }
        string _phoneNo;

        public string PhoneNo
        {
            get { return _phoneNo; }
            set { _phoneNo = value; this.OnPropertyChanged("AllowSave"); }
        }
        string _birthDate;

        public string BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }
        string _medicalProvider;

        public string MedicalProvider
        {
            get { return _medicalProvider; }
            set { _medicalProvider = value; }
        }
        string _providerID;

        public string ProviderID
        {
            get { return _providerID; }
            set { _providerID = value; }
        }
        string _transportCompany;

        public string TransportCompany
        {
            get { return _transportCompany; }
            set { _transportCompany = value; }
        }
        string _transportType;

        public string TransportType
        {
            get { return _transportType; }
            set { _transportType = value; }
        }
        string _procedureCode;

        public string ProcedureCode
        {
            get { return _procedureCode; }
            set { _procedureCode = value; }
        }

        string _Modifier;

        public string Modifier
        {
            get { return _Modifier; }
            set { _Modifier = value; }
        }
        DateTime? _serviceStarts;

        public DateTime? ServiceStarts
        {
            get { return _serviceStarts; }
            set { _serviceStarts = value; }
        }
        DateTime? _serviceEnds;

        public DateTime? ServiceEnds
        {
            get { return _serviceEnds; }
            set { _serviceEnds = value; }
        }
        bool _standingOrder;

        public bool StandingOrder
        {
            get { return _standingOrder; }
            set { _standingOrder = value; }
        }
        int _tripsApproved;

        public int TripsApproved
        {
            get { return _tripsApproved; }
            set { _tripsApproved = value; }
        }
        int _daysApproved;

        public int DaysApproved
        {
            get { return _daysApproved; }
            set { _daysApproved = value; }
        }
        string _wheelChair;

        public string WheelChair
        {
            get { return _wheelChair; }
            set { _wheelChair = value; }
        }

        DateTime? _pickupDate;

        public DateTime? PickUpDate
        {
            get { return _pickupDate; }
            set { _pickupDate = value; this.OnPropertyChanged("AllowSave"); }
        }
        TimeSpan? _pickUpTime;

        public TimeSpan? PickUpTime
        {
            get { return _pickUpTime; }
            set { _pickUpTime = value; }
        }
        string _pickUpAddress;

        public string PickUpAddress
        {
            get { return _pickUpAddress; }
            set { _pickUpAddress = value; }
        }
        string _pickUpCity;

        public string PickUpCity
        {
            get { return _pickUpCity; }
            set { _pickUpCity = value; }
        }
        string _pickupState;

        public string PickupState
        {
            get { return _pickupState; }
            set { _pickupState = value; }
        }
        string _pickupZip;

        public string PickUpZip
        {
            get { return _pickupZip; }
            set { _pickupZip = value; }
        }

        DateTime? _dropoffDate;

        public DateTime? DropOffDate
        {
            get { return _dropoffDate; }
            set { _dropoffDate = value; }
        }
        TimeSpan? _dropoffTime;

        public TimeSpan? DropOffTime
        {
            get { return _dropoffTime; }
            set { _dropoffTime = value; }
        }
        string _dropoffAddress;

        public string DropOffAddress
        {
            get { return _dropoffAddress; }
            set { _dropoffAddress = value; }
        }
        string _dropoffCity;

        public string DropOffCity
        {
            get { return _dropoffCity; }
            set { _dropoffCity = value; }
        }
        string _dropoffState;

        public string DropOffState
        {
            get { return _dropoffState; }
            set { _dropoffState = value; }
        }
        string _dropoffZip;

        public string DropOffZip
        {
            get { return _dropoffZip; }
            set { _dropoffZip = value; }
        }
        string _instructions;

        public string Instructions
        {
            get { return _instructions; }
            set { _instructions = value; }
        }
        string _secondaryService;

        public string SecondaryService
        {
            get { return _secondaryService; }
            set { _secondaryService = value; }
        }
        decimal _TollPrice;

        public decimal TollPrice
        {
            get { return _TollPrice; }
            set { _TollPrice = value; this.OnPropertyChanged("TollPrice"); }
        }
        int _medicaidDispatchTripNo;

        public int MedicaidDispatchTripNo
        {
            get { return _medicaidDispatchTripNo; }
            set { _medicaidDispatchTripNo = value; }
        }



        bool _isManualEntry;

        public bool IsManualEntry
        {
            get { return _isManualEntry; }
            set { _isManualEntry = value; }
        }

        string _priorApprovalNumber;

        public string PriorApprovalNumber
        {
            get { return _priorApprovalNumber; }
            set { _priorApprovalNumber = value; }
        }

        bool _submitted;

        public bool Submitted
        {
            get { return _submitted; }
            set { _submitted = value; }
        }
        public override bool AllowSave
        {
            get
            {
                return this.LastName != null && this.PhoneNo != null && this.CIN != null && this.PickUpDate != null;
            }

        }

    }

    public class MedicaidLoginInfo : BO
    {
        int _mLogInID;

        public int MLogInID
        {
            get { return _mLogInID; }
            set { _mLogInID = value; }
        }
        string _medicaidLogInUserNmae;

        public string MedicaidLogInUserNmae
        {
            get { return _medicaidLogInUserNmae; }
            set { _medicaidLogInUserNmae = value; }
        }


        string _medicaidLogInPassword;

        public string MedicaidLogInPassword
        {
            get { return _medicaidLogInPassword; }
            set { _medicaidLogInPassword = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class LocalCity : BO, IEditableObject
    {
        public struct LocalCityStruct
        {
            public int _localCityID;
            public string _cityName;
        }

        LocalCityStruct LocalCityData;
        LocalCityStruct _backupCityData;
        bool _inEditMode = false;

        public LocalCity()
        {
            this.LocalCityData = new LocalCityStruct();
        }

        public int LocalCityID
        {
            get { return this.LocalCityData._localCityID; }
            set { this.LocalCityData._localCityID = value; }
        }

        public string CityName
        {
            get { return this.LocalCityData._cityName; }
            set { this.LocalCityData._cityName = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.CityName);
            }

        }

        #region IEditableObject Members

        public void BeginEdit()
        {
            if (!_inEditMode)
            {
                this._inEditMode = true;
                this._backupCityData = this.LocalCityData;
            }
        }

        public void CancelEdit()
        {
            if (this._inEditMode)
            {
                this._inEditMode = false;
                this.LocalCityData = this._backupCityData;
                this._backupCityData = new LocalCityStruct();
            }
        }

        public void EndEdit()
        {
            if (this._inEditMode)
            {
                this._inEditMode = false;
                this._backupCityData = new LocalCityStruct();
            }
        }

        #endregion
    }


}
