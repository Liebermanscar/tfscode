﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AssignUnitDiscepencyWindow.xaml
    /// </summary>
    public partial class AssignUnitDiscepencyWindow : Window
    {
        public AssignUnitDiscepencyWindow()
        {
            InitializeComponent();
            this.DiscrepencyOptionsListBox.Focus();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
