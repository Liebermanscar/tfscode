﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using BOL;
using Telerik.Windows.Controls.Map;
using RadMap = Telerik.Windows.Controls.Map;
using System.Collections.ObjectModel;

namespace CDS
{
    public partial class MapWindow : Window
    {
        BingGeocodeProvider geocodeProvider;
        public static string BingMapsKey = Properties.Settings.Default.BingMapsKey;
        public string DefaultCityState = Properties.Settings.Default.DefaultCityState;
        DispatchingToProxy Dispatching_Service;

        public MapWindow(DispatchingToProxy dispatchingData)
        {
            InitializeComponent();

            Dispatching_Service = dispatchingData;

            radMap.Provider = new BingMapProvider(MapMode.Road, true, BingMapsKey);
            DataContext = new DispatchingMapModel(dispatchingData);

            dispatchingData.On_TripBroadcasted += (trip, msgType) => 
            {
                switch (msgType)
                {
                    case DispatcherService.MessageType.TripAdded:
                        //if is dispatcher who added the trip, get geo location
                        if (trip.TakenByDispatcherID == UIController.CurrentSession.SessionUser.UserID)
                        {
                            requestFromGeoCode = true;
                            RequstGeoCode(trip, true);
                        }
                        break;
                    case DispatcherService.MessageType.TripEdited:
                        ActiveMapModel.EditTrip(trip);
                        break;
                    case DispatcherService.MessageType.TripRemoved:
                        ActiveMapModel.RemoveTrip(trip);
                        break;
                    case DispatcherService.MessageType.DriverAdded:
                        break;
                    case DispatcherService.MessageType.DriverEdited:
                        break;
                    case DispatcherService.MessageType.DriverRemoved:
                        break;
                    case DispatcherService.MessageType.GetAssignedUnitOptions:
                        break;
                    case DispatcherService.MessageType.TripDriverResetLocal:
                        break;
                    case DispatcherService.MessageType.TripDriverResetLongDistance:
                        break;
                    case DispatcherService.MessageType.DriversAndTripInfoReset:
                        break;
                    case DispatcherService.MessageType.CarsAvailabilitChanged:
                        break;
                    case DispatcherService.MessageType.TripLocationAdded:

                        ActiveMapModel.AddTrip(trip,
                    new RadMap.Location(trip.From_Latitude, trip.From_Longitude),
                    new RadMap.Location(trip.To_Latitude, trip.To_Longitude));
                        break;
                    default:
                        break;
                }
                
            };

            InitializeGeoCodeProvider();
        }

        public DispatchingMapModel ActiveMapModel
        {
            get { return DataContext as DispatchingMapModel; }
        }

        void InitializeGeoCodeProvider()
        {
            try
            {
                if (geocodeProvider == null)
                {
                    geocodeProvider = new BingGeocodeProvider();
                    geocodeProvider.ApplicationId = BingMapsKey;
                    geocodeProvider.MapControl = radMap;
                    geocodeProvider.GeocodeCompleted += geocodeProvider_GeocodeCompleted;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        bool requestFromGeoCode = false;
        bool requestToGeoCode = false;

        private void geocodeProvider_GeocodeCompleted(object sender, GeocodeCompletedEventArgs e)
        {
            try
            {
                Trip trip = (Trip)e.Response.RequestUserData;
                var location = e.Response.Results.FirstOrDefault();

                if (location != null)
                {
                    var tripLocation = location.Locations.FirstOrDefault();

                    radMap.Center = new RadMap.Location(tripLocation.Latitude, tripLocation.Longitude);

                    if (requestFromGeoCode)
                    {
                        trip.From_Latitude = tripLocation.Latitude;
                        trip.From_Longitude = tripLocation.Longitude;
                    }
                    else if (requestToGeoCode)
                    {
                        trip.To_Latitude = tripLocation.Latitude;
                        trip.To_Longitude = tripLocation.Longitude;
                    }
                }

                if (requestFromGeoCode)
                {
                    requestFromGeoCode = false;
                    requestToGeoCode = true;

                    RequstGeoCode(trip, false);
                }
                else if (requestToGeoCode)
                {
                    requestToGeoCode = false;

                    //send to service
                    Dispatching_Service.AddTripLocation(trip);
                }

                //if (e.Response.Results.Count > 1)
                //{
                //    var results = string.Join("\n", e.Response.Results.Select(r => r.DisplayName));
                //    MessageBox.Show(string.Format("Address is not clear, there are multiple results\n{0}", results));
                //}
                //else
                //{
                //    ActiveMapModel.AddTrip(trip, location.Locations.FirstOrDefault());
                //}
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void RequstGeoCode(Trip trip, bool from)
        {
            try
            {
                GeocodeRequest request = new GeocodeRequest();
                request.Query = string.Format("{0} {1}", from ? trip.FromLocation : trip.ToLocation, DefaultCityState);
                request.UserData = trip;
                geocodeProvider.GeocodeAsync(request);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ActiveMapModel.AddTrip(new Trip() { TripNo = 101 }, 
                new RadMap.Location(40.703805, -73.956989),
                new RadMap.Location(40.706312, -73.952008));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            foreach (var item in ActiveMapModel.TripsFrom.ToList())
            {
                ActiveMapModel.TripsFrom.Remove(item);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var secondaryScreen = System.Windows.Forms.Screen.AllScreens.Where(s => !s.Primary).FirstOrDefault();

            if (secondaryScreen != null)
            {
                var workingArea = secondaryScreen.WorkingArea;
                Left = workingArea.Left;
                Top = workingArea.Top;
                Width = workingArea.Width;
                Height = workingArea.Height;
                WindowState = WindowState.Maximized;
            }
        }
    }

    public class DispatchingMapModel : INotifyPropertyChanged
    {
        public DispatchingMapModel(IDispatchingMVVM dispatchingData)
        {
            //Trips = new ObservableCollection<TripOnMap>();
            _TripsFrom = new ObservableCollection<TripOnMap>();
            _TripsTo = new ObservableCollection<TripOnMap>();

            //if have location add to map
            foreach (var trip in dispatchingData.Trips.Where(t => t.From_Latitude != 0 || t.To_Latitude != 0))
            {
                AddTrip(trip,
                    new RadMap.Location(trip.From_Latitude, trip.From_Longitude),
                    new RadMap.Location(trip.To_Latitude, trip.To_Longitude));
            }

            _Drivers = new ObservableCollection<DriverAndTripInformation>();
        }

        public void AddTrip(Trip trip, RadMap.Location from, RadMap.Location to)
        {
            if (from != null && from.Latitude != 0 && from.Longitude != 0)
            {
                _TripsFrom.Add(new TripOnMap() { Trip = trip, From_GeoLocation = from });

                OnPropertyChanged("TripsFrom");
            }

            if (to != null && to.Latitude != 0 && to.Longitude != 0)
            {
                _TripsTo.Add(new TripOnMap() { Trip = trip, To_GeoLocation = to });

                OnPropertyChanged("TripsTo");
            }
        }

        public void EditTrip(Trip trip)
        {
            TripOnMap tripFrom = _TripsFrom.FirstOrDefault(t => t.Trip.TripID == trip.TripID);

            if (tripFrom != null)
            {
                int tripIndex = _TripsFrom.IndexOf(tripFrom);
                _TripsFrom[tripIndex].Trip = trip;

                tripFrom.NotifyPropertyChange();
                OnPropertyChanged("TripsFrom");
            }

            TripOnMap tripTo = _TripsTo.FirstOrDefault(t => t.Trip.TripID == trip.TripID);

            if (tripTo != null)
            {
                int tripIndex = _TripsTo.IndexOf(tripTo);
                _TripsTo[tripIndex].Trip = trip;

                tripTo.NotifyPropertyChange();
                OnPropertyChanged("TripsTo");
            }

        }

        public void RemoveTrip(Trip trip)
        {
            TripOnMap tripFrom = _TripsFrom.FirstOrDefault(t => t.Trip.TripID == trip.TripID);

            if (tripFrom != null)
            {
                _TripsFrom.Remove(tripFrom);
                OnPropertyChanged("TripsFrom");
            }

            TripOnMap tripTo = _TripsTo.FirstOrDefault(t => t.Trip.TripID == trip.TripID);

            if (tripTo != null)
            {
                _TripsTo.Remove(tripTo);
                OnPropertyChanged("TripsTo");
            }
        }

        ObservableCollection<TripOnMap> _TripsFrom;
        ObservableCollection<TripOnMap> _TripsTo;

        //public ObservableCollection<TripOnMap> Trips { get; set; }

        public ObservableCollection<TripOnMap> TripsFrom
        {
            get
            {
                return new ObservableCollection<TripOnMap>(_TripsFrom.
                    Where(t => 
                    t.Trip.TripStatus != TripStatusTypes.Complete &&
                    t.Trip.TripStatus != TripStatusTypes.Canceled).
                    ToList());
            }
        }
        public ObservableCollection<TripOnMap> TripsTo
        {
            get
            {
                return new ObservableCollection<TripOnMap>(_TripsTo.
                    Where(t =>
                    t.Trip.TripStatus != TripStatusTypes.Complete &&
                    t.Trip.TripStatus != TripStatusTypes.Canceled).
                    ToList());
            }
        }

        public ObservableCollection<DriverAndTripInformation> Drivers
        {
            get
            {
                return _Drivers;
            }

            set
            {
                _Drivers = value;
            }
        }

        ObservableCollection<DriverAndTripInformation> _Drivers;

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class TripOnMap : INotifyPropertyChanged
    {
        private RadMap.Location from_Geolocation = RadMap.Location.Empty;

        public RadMap.Location From_GeoLocation
        {
            get
            {
                return from_Geolocation;
            }

            set
            {
                from_Geolocation = value;
                OnPropertyChanged("From_GeoLocation");
            }
        }

        private RadMap.Location to_Geolocation = RadMap.Location.Empty;

        public RadMap.Location To_GeoLocation
        {
            get
            {
                return to_Geolocation;
            }

            set
            {
                to_Geolocation = value;
                OnPropertyChanged("To_GeoLocation");
            }
        }

        public string TripNumber
        {
            get { return _Trip.TripNo.ToString(); }
        }

        public string ToolTipDisplay
        {
            get { return string.Format("{0}\n{1}", _Trip.FromLocation, _Trip.ToLocation); }
        }

        public string DriverCode
        {
            get
            {
                if (_Trip.DrivenBy != null)
                {
                    return _Trip.DrivenBy.DriverCode;
                }

                return "";
            }
        }

        Trip _Trip;
        public Trip Trip
        {
            get { return _Trip; }

            set { _Trip = value; }
        }

        public void NotifyPropertyChange()
        {
            OnPropertyChanged("DriverCode");
            OnPropertyChanged("ToolTipDisplay");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public delegate void TripChanged_EventHandler(Trip trip, DispatcherService.MessageType messageType);
}
