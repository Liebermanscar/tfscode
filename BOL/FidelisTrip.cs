﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace BOL
{
    public class FidelisTrip : BO, IEditableObject
    {
        public struct FidelisTripDataStruct
        {
            public string GroupID;
            public string MemberID;
            public string MemberName;
            public string MemberAddress;
            public string MemberCityStatePostalCode;
            public DateTime? MemberDOB;
            public string MemberPhoneNo;
            public char MemberGender;
            public int NoOfRiders;
            public string AuthNo;
            public DateTime? TripScheduledOn;
            public string DestinationTitle;
            public string DestinationAddress;
            public string DestinationCityStatePostalCode;
            public string Comments;
            public string UserName;
        }

        private FidelisTripDataStruct fidelisTripData = new FidelisTripDataStruct();
        private FidelisTripDataStruct backUp;

        public int FidelisTripID { get; set; }


        public string GroupID
        {
            get { return fidelisTripData.GroupID; }
            set { fidelisTripData.GroupID = value; }
        }

        public string MemberID
        {
            get { return fidelisTripData.MemberID; }
            set { fidelisTripData.MemberID = value; }
        }


        public string MemberName
        {
            get { return fidelisTripData.MemberName; }
            set { fidelisTripData.MemberName = value; }
        }


        public string MemberAddress
        {
            get { return fidelisTripData.MemberAddress; }
            set { fidelisTripData.MemberAddress = value; }
        }


        public DateTime? MemberDOB
        {
            get { return fidelisTripData.MemberDOB; }
            set { fidelisTripData.MemberDOB = value; }
        }


        public string MemberPhoneNo
        {
            get { return fidelisTripData.MemberPhoneNo; }
            set { fidelisTripData.MemberPhoneNo = value; }
        }


        public char MemberGender
        {
            get { return fidelisTripData.MemberGender; }
            set { fidelisTripData.MemberGender = value; }
        }


        public int NoOfRiders
        {
            get { return fidelisTripData.NoOfRiders; }
            set { fidelisTripData.NoOfRiders = value; }
        }


        public string AuthNo
        {
            get { return fidelisTripData.AuthNo; }
            set { fidelisTripData.AuthNo = value; }
        }


        public string DestinationAddress
        {
            get { return fidelisTripData.DestinationAddress; }
            set { fidelisTripData.DestinationAddress = value; }
        }


        public string Comments
        {
            get { return fidelisTripData.Comments; }
            set { fidelisTripData.Comments = value; }
        }


        public string UserName
        {
            get { return fidelisTripData.UserName; }
            set { fidelisTripData.UserName = value; }
        }

        public string MemberCityStatePostalCode
        {
            get { return this.fidelisTripData.MemberCityStatePostalCode; }
            set { this.fidelisTripData.MemberCityStatePostalCode = value; }
        }

        public string DestinationTitle
        {
            get { return this.fidelisTripData.DestinationTitle; }
            set { this.fidelisTripData.DestinationTitle = value; }
        }

        public string DestinationCityStatePostalCode
        {
            get { return this.fidelisTripData.DestinationCityStatePostalCode; }
            set { this.fidelisTripData.DestinationCityStatePostalCode = value; }
        }

        public DateTime? TripScheduledOn
        {
            get { return this.fidelisTripData.TripScheduledOn; }
            set { this.fidelisTripData.TripScheduledOn = value; }
        }

        public bool isInEdit;

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.MemberID);
            }

        }

        #region IEditableObject Members

        public void BeginEdit()
        {
            if (this.isInEdit)
                return;
            this.isInEdit = true;
            this.backUp = this.fidelisTripData;

        }

        public void CancelEdit()
        {
            this.isInEdit = false;
            this.fidelisTripData = this.backUp;
            this.backUp = new FidelisTripDataStruct();
        }

        public void EndEdit()
        {
            this.isInEdit = false;
            this.backUp = new FidelisTripDataStruct();
        }

        #endregion


    }
}
