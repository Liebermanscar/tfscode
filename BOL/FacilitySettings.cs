﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOL
{
    public class FacilitySetting : BO
    {
        int _settingID;

        public int SettingID
        {
            get { return _settingID; }
            set { _settingID = value; }
        }

        string _settingKey;

        public string SettingKey
        {
            get { return _settingKey; }
            set { _settingKey = value; }
        }

        decimal _settingNum;

        public decimal SettingNum
        {
            get { return _settingNum; }
            set { _settingNum = value; }
        }

        string _settingValue;

        public string SettingValue
        {
            get { return _settingValue; }
            set { _settingValue = value; }
        }
        bool _settingBit;

        public bool SettingBit
        {
            get { return _settingBit; }
            set { _settingBit = value; }
        }
        TimeSpan _settingTime;

        public TimeSpan SettingTime
        {
            get { return _settingTime; }
            set { _settingTime = value; }
        }

        DateTime _FromDate;

        public DateTime FromDate
        {
            get { return _FromDate; }
            set { _FromDate = value; }
        }

        DateTime _ToDate;

        public DateTime ToDate
        {
            get { return _ToDate; }
            set { _ToDate = value; }
        }

        public override bool AllowSave
        {
            get { return true; }
        }
    }
}
