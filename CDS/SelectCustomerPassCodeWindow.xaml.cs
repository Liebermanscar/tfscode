﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SelectCustomerPassCodeWindow.xaml
    /// </summary>
    public partial class SelectCustomerPassCodeWindow : Window
    {
        public SelectCustomerPassCodeWindow()
        {
            InitializeComponent();
        }
        public SelectCustomerPassCodeWindow(ObservableCollection<CustomerPassCode> customerPassCodes)
            : this()
        {
            this.CustomerPassCodes = customerPassCodes;
        }
        /*
           if (!this.customerSearchCtrl.IsFocused &&

                e.Key == Key.Return &&

                this.Customers != null &&

                this.Customers.Count > 0)
            {

                e.Handled = true;

                this.DialogResult = true;

            }
         
         
         */
        private void CustomerPassCodeControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = true;
        }

        public ObservableCollection<CustomerPassCode> CustomerPassCodes
        {
            get { return this.DataContext as ObservableCollection<CustomerPassCode>; }
            set { this.DataContext = value; }
        }

        public CustomerPassCode SelectedCustomerPassCode
        {
            get { return this.CustomerPassCodeControlGrid.SelectedItem as CustomerPassCode; }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (
                e.Key == Key.Return &&

                this.CustomerPassCodes != null &&

                this.CustomerPassCodes.Count > 0)
            {

                e.Handled = true;

                this.DialogResult = true;

            }

        }
    }
}
