﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class TicketBookSold : BO
    {
        int _qty;

        public int Qty
        {
            get { return _qty; }
            set
            {
                _qty = value; this.TotalPrice = this.Price * value;
                this.OnPropertyChanged("Qty");
                this.OnPropertyChanged("TotalPrice");
            }
        }
        TicketBook _ticketBookTypeSold;

        public TicketBook TicketBookTypeSold
        {
            get { return _ticketBookTypeSold; }
            set
            {
                _ticketBookTypeSold = value;
                this.Price = value.BookPrice;

                this.OnPropertyChanged("TicketBookTypeSold");
                this.OnPropertyChanged("Price");
                this.OnPropertyChanged("TotalPrice");

            }
        }
        decimal _price;

        public decimal Price
        {
            get { return _price; }
            set
            {
                _price = value;
                this.TotalPrice = this.Qty * value;
                this.OnPropertyChanged("Price");
                this.OnPropertyChanged("TotalPrice");
            }
        }

        decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        ObservableCollection<TicketBookStatus> ticketBookStatusesSold;

        public ObservableCollection<TicketBookStatus> TicketBookStatusesSold
        {
            get { return ticketBookStatusesSold; }
            set
            {
                ticketBookStatusesSold = value;
                this.OnPropertyChanged("TicketBookStatusesSold");
                this.OnPropertyChanged("TotalCashFromBooksSold");

            }
        }

        public int QtyPaidCash
        {
            get
            {
                if (TicketBookStatusesSold == null)
                    return 0;
                return TicketBookStatusesSold.Count(t => !t.IsPaidByCC);
            }
        }

        public override bool AllowSave
        {
            get
            {
                throw new NotImplementedException();
            }

        }

        int _countPaidByCC;

        public int CountPaidByCC
        {
            get { return _countPaidByCC; }
            set { _countPaidByCC = value; }
        }
    }
}
