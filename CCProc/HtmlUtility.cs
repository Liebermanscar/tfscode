﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCProc
{
  public static class HtmlUtility
    {
        public static string GetUrlEncode(string url)
        {
            //  return System.Web.HttpUtility.UrlEncode(url);
            return Uri.EscapeUriString(url).Replace("~", "%7e");
            // return "";
        }
    }
}
