﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for LogOnPage.xaml
    /// </summary>
    public partial class LogOnPage : Page, IDisplayTextBlock
    {
        public LogOnPage()
        {
            InitializeComponent();
        }

        public LogOnPage(List<User> users)
            : this()
        {
            this.SelectedUser = new User();
            this.ecUsers.ItemsSource = users;

            this.ecUsers.SelectedItem = users.Where(u => u.UserID == Properties.Settings.Default.LastLogedOnUser).FirstOrDefault();
            if (this.SelectedUser != null)
                this.ePassword.Focus();
           
        }

        private void btnLogOn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedUser.Password != this.ePassword.Password)
                    this.MessageText.Text = "Logon credentials are incorrect, Please try again";
                else
                {
                    Properties.Settings.Default.LastLogedOnUser = this.SelectedUser.UserID;
                    Properties.Settings.Default.Save();
                    UIController.CreateNewSession(this.SelectedUser);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            UIController.CloseProgram(false);
        }

        public User SelectedUser 
        {
            get { return this.DataContext as User; }
            set { this.DataContext = value; }
        }

        private void ecUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var o = (sender as ComboBox).SelectedItem;
            if(o != null)
                this.SelectedUser = o as User;
        }



        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Log On";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
