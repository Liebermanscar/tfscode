﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriverStatusLogListPage.xaml
    /// </summary>
    public partial class DriverStatusLogListPage : Page, IDisplayTextBlock
    {
        public DriverStatusLogListPage()
        {
            InitializeComponent();
        }

        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            try
            {


                this.DriverStatusLogs = DAL.DriverStatusLogDB.GetDriverStatusLogs(
                FromDatePicker.SelectedDate == null ? new Nullable<DateTime>() : FromDatePicker.SelectedDate.Value,
                ToDatePicker.SelectedDate == null ? new Nullable<DateTime>() : ToDatePicker.SelectedDate.Value);

                DriverStatusLogsByDateDataGrid.DataContext = this.DriverStatusLogs;


            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
        public ObservableCollection<DriverStatusLog> DriverStatusLogs { get; set; }
        #region
        public string HeaderText
        {
            get
            {
                return "Driver Status Logs List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            try
            {
                UIController.ExportTelerikGrid(this.DriverStatusLogsByDateDataGrid, UIController.ExportFormat.Excel);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

    }
}
