﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTRWebApi.Controllers
{
    public class TripSectionController : ApiController
    {
        /// <summary>
        /// gets in which section the point is
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public IHttpActionResult GetPointInTripSections(int lat, int lon)
        {
            var poinrIntripSections = DAL_EF.DAL.TripSectionsDb.GetPointInTripSections(lat, lon);

            if (poinrIntripSections == null)
                return NotFound();
            else
                return Ok(poinrIntripSections);
        }

        // GET: api/TripSection
        public IHttpActionResult Get()
        {
            var tripSections = DAL_EF.DAL.TripSectionsDb.GetTripSections(0);

            if (tripSections == null)
                return NotFound();
            else
                return Ok(tripSections);
        }
       

        // GET: api/TripSection/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/TripSection
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/TripSection/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TripSection/5
        public void Delete(int id)
        {
        }
    }
}
