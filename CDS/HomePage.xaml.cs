﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for HomePage.xaml
    /// </summary>
    public partial class HomePage : Page, IDisplayTextBlock
    {
        public HomePage()
        {
            InitializeComponent();
            if (UIController.CurrentSession.SessionUser.UserType != UserTypes.Supervisor)
            {
                this.tempAdminCtrl.Visibility = Visibility.Hidden;
                this.tempCompanyFinancialControl.Visibility = Visibility.Hidden;
            }
            if (UIController.CurrentSession.SessionUser.UserType != UserTypes.BookKeeping)
            {
                this.tempMissingEnvelopesControl.Visibility = Visibility.Hidden;
            }
            if (UIController.CurrentSession.SessionUser.UserType != UserTypes.DriverManger)
            {
                this.tempManagerControl.Visibility = Visibility.Hidden;
            }
        }

        public string HeaderText
        {
            get
            {
                return "Home Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
