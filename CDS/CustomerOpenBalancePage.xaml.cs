﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for CusotmerOpenBalance.xaml
    /// </summary>
    public partial class CustomerOpenBalancePage : Page, IDisplayTextBlock
    {
        public CustomerOpenBalancePage()
        {
            InitializeComponent();

            this.Customers = DAL.CustomerDB.GetCustomerOpenBalance(0);

            this.CustomersOpenBalanceDateGrid.DataContext = this.Customers;
        }


        /* public CustomerOpenBalancePage(bool OnlyNotInvoiced)
             : this()
         {
             if (OnlyNotInvoiced == true)
             {
                 this.Customers = DAL.CustomerDB.GetCustomerOpenBalance(0, true);


             this.CustomersOpenBalanceDateGrid.DataContext = this.Customers;}

         }*/

        private void CustomersOpenBalanceDateGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SelectedCustomer == null)
                return;
            var custID = SelectedCustomer.CustomerID;
            var custFullInfo = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID, custID.ToString(), DataRequestTypes.FullData, false).FirstOrDefault();
            UIController.LoadCustomerInfoPage(custFullInfo);

        }


        Customer SelectedCustomer
        {
            get { return this.CustomersOpenBalanceDateGrid.SelectedItem as Customer; }
        }


        ObservableCollection<Customer> _customers;

        public ObservableCollection<Customer> Customers
        {
            get { return _customers; }
            set { _customers = value; }
        }

        public string HeaderText
        {
            get
            {
                return "Customer - Open Balance";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        private void RadMenuItemShowInvoice_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }


    }
}
