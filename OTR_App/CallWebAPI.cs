﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Android.Content;
using Android.Preferences;
using Android.App;
using System.Threading.Tasks;
using ModernHttpClient;
using Newtonsoft.Json;

namespace OTR_App
{
    public static class CallWebApi
    {
        static string _webAPIurl = "http://192.168.1.11:37999/api/";
        static HttpClient _httpClient;


        static string DriverController = "Driver";
        static string SignInAction = "SignIn";
		static string SignOutAction = "SignOut";

        static CallWebApi()
        {
			_httpClient = new HttpClient(new NativeMessageHandler());
            _httpClient.BaseAddress = new Uri(_webAPIurl);
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
           // _httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "ojc_fund", "fund_ojc"))));
        }

        public  static string Login(string userName, string password, string tabletName)
        {

            HttpResponseMessage response  =  _httpClient.GetAsync(string.Format("{0}/{1}?username={2}&password={3}&tabletName={4}", DriverController, SignInAction, userName, password, tabletName)).Result;

			if (response.IsSuccessStatusCode) {
				string s = response.Content.ReadAsStringAsync().Result;

				return s.Replace("\"", String.Empty);
            } else
				return "";
        }

		public  static string Logout(string driverID, string tabletName)
		{

			HttpResponseMessage response  =  _httpClient.GetAsync(string.Format("{0}/{1}?driverID={2}&tabletName={3}", DriverController, SignOutAction, driverID, tabletName)).Result;

			if (response.IsSuccessStatusCode) {
				string s = response.Content.ReadAsStringAsync ().Result;

                return s.Replace("\"", String.Empty);
            } else
				return "";
		}

		public static string ConfirmTabletName(string tabletName)
		{
			//HttpClient client =  _httpClient;

			HttpResponseMessage response = _httpClient.GetAsync(string.Format("{0}/IsTabletNameInSystem?tabletName={1}", DriverController, tabletName)).Result;
			if (response.IsSuccessStatusCode) {
				var s = response.Content.ReadAsStringAsync ().Result;
				return s.Replace("\"","") ;
			}
			else
				return "0";


			//var result = client.GetAsync(string.Format("{0}/{1}?tabletName={3}", _webAPIurl, DriverController, "IsTabletNameInSystem", tabletName)).Result;

            //string s = result.Content.ReadAsStringAsync().Result;

			//return s.ToUpper() == "TRUE";
        }

        public static BOL_OTR.DriverInfo GetDriverInfo(string driverID)
        {
            HttpResponseMessage response = _httpClient.GetAsync(string.Format("{0}/GetDriverInfo?driverID={1}", DriverController, driverID)).Result;
            if (response.IsSuccessStatusCode)
            {
                var s = response.Content.ReadAsStringAsync().Result;

                var o = JsonConvert.DeserializeObject<BOL_OTR.DriverInfo>(s);


                return o;// s.Replace("\"", "");
            }
            else
                return null;
        }
    }


}

