﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;

namespace CallerID
{
    public class PhoneInfo
    {
        public int PhoneLine;
        public string CallerID;
        public string CallerName;
    }
    public class Listener
    {
        int listenToPort = 3520;


        
        public void StartListener(CallerIDReceived callerIDRcvdCalback)
        {
                       

            try
            {
                UdpClient udpClient = new UdpClient(listenToPort);

                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                while (true)
                {
                    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);

                    string CallRecord = Encoding.UTF7.GetString(receiveBytes);

                    CallRecord = CallRecord.Remove(0, CallRecord.IndexOf('$') + 1);

                    Console.WriteLine("This is the message you received " +
                                                CallRecord.ToString());
                    

                    var CallMatch = Regex.Match(CallRecord, @".*(\d\d) ([IO]) ([ES]) (\d{4}) ([GB]) (.)(\d) (\d\d/\d\d \d\d:\d\d [AP]M) (.{8,15})(.*)");
        //                                       |      |      |      |       |    |   |            |                      |      |
        //                                   01 Line #  |      |      |       |    | 07 Ring#       |                      |    10 Name
        //                               02 Inbound/outbound   |   04 Duration| 06 Ring Type        |                 09 Phone Number
        //                                               03 Start/End recrod  |          08 Date of call start
        //                                                                05 Good/Bad record checksum
                    //Console.WriteLine("This is the message you received " +
                    //                            CallRecord.ToString());
                    //Console.WriteLine("This message was sent from " +
                    //                            RemoteIpEndPoint.Address.ToString() +
                    //                            " on their port number " +
                    //                            RemoteIpEndPoint.Port.ToString());

                    if (CallMatch.Success)
                    {
                        if (callerIDRcvdCalback != null)
                            callerIDRcvdCalback.BeginInvoke
                                (
                                    Convert.ToInt32(CallMatch.Groups[1].Value), 
                                    CallMatch.Groups[9].Value.Trim(' '), 
                                    CallMatch.Groups[10].Value.Replace("\r", "").Trim(' '), 
                                    null,
                                    null
                                );
                    }

                    Console.WriteLine();
                }
               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }

    public delegate void CallerIDReceived(int lineNo, string phoneNo, string name);


}
