﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace BOL
{
    public class ScheduledTrip : BO, IEditableObject
    {
        public ScheduledTrip()
        {
            this.ScheduledTripData = new ScheduledTripDataStruct();
            this.PickupDate = DateTime.Today;//.AddDays(1);
            this.ScheduledTripType = ScheduledTripTypes.ByCustomer;
        }

        struct ScheduledTripDataStruct
        {
            public int _scheduledTripID;
            public ScheduledTripTypes _scheduledTripType;
            public int _fidelisMedicaidReferencNo;
            public int _noOfLegs;
            public string _customerName;
            public string _drivenFirstName;
            public string _drivenLastName;
            public string _customerPhoneNo;
            public DateTime _pickupDate;
            public DateTime? _pickupTime;
            public string _pickupAddress;
            public string _pickupCity;
            public string _pickupState;
            public string _pickupZip;
            public string _dropOffAddress;
            public string _dropOffCity;
            public string _dropOffState;
            public string _dropOffZip;
            public string _cardNo;
            public TripAreaTypes _tripAreaType;
            public bool _isLongDistance;
            public int _assignedToDriverID;

            public DateTime? _dateAdded;
            public DateTime? _eTAToLocation1;
            public DateTime? _eTABackToBase;
            public decimal _tripPrice;
            public decimal _waitingCahrge;
            public decimal _extraCharge;
            public decimal _miniVanCharge;
            public int _tripID;
            public bool _isRoundTrip;
            public bool _needToConfirm;
            public bool _isCanceldTrip;

            public string _sFromLocationComment;
            public string _sToLocationComment;
            public VehicleTypes sVehicleTypeRequested;
            public bool _needsMiniVan;

            public int _sCustomerPassCodeID;
            public int? _sNoOfStops;






            //   public   Trip _tripInfo;



        }

        ScheduledTripDataStruct ScheduledTripData;
        ScheduledTripDataStruct _backUpData;

        public int ScheduledTripID
        {
            get { return ScheduledTripData._scheduledTripID; }
            set
            {
                ScheduledTripData._scheduledTripID = value;
                this.OnPropertyChanged("IsNotSaved");
                this.OnPropertyChanged("IsSaved");
            }
        }
        public ScheduledTripTypes ScheduledTripType
        {
            get { return ScheduledTripData._scheduledTripType; }
            set { ScheduledTripData._scheduledTripType = value; }
        }

        public bool IsLongDistance
        {
            get { return ScheduledTripData._isLongDistance; }
            set { ScheduledTripData._isLongDistance = value; }
        }
        public int FidelisMedicaidReferencNo
        {
            get { return ScheduledTripData._fidelisMedicaidReferencNo; }
            set { ScheduledTripData._fidelisMedicaidReferencNo = value; }
        }
        public int NoOfLegs
        {
            get { return ScheduledTripData._noOfLegs; }
            set { ScheduledTripData._noOfLegs = value; }
        }

        Customer _scheduledByCustomer;
        public Customer ScheduledByCustomer
        {
            get { return _scheduledByCustomer; }
            set
            {
                _scheduledByCustomer = value;
                if (value != null && string.IsNullOrEmpty(this.ScheduledTripData._customerPhoneNo))
                    this.ScheduledTripData._customerPhoneNo = value.HomePhoneNo;


                this.OnPropertyChanged("ScheduledByCustomer");
                this.OnPropertyChanged("DrivenFirstName");
                this.OnPropertyChanged("DrivenLastName");
                this.OnPropertyChanged("CustomerPhoneNo");
                this.OnPropertyChanged("AllowSave");
            }
        }
        public string CustomerName
        {
            get { return ScheduledTripData._customerName; }
            set
            {
                ScheduledTripData._customerName = value;
                this.OnPropertyChanged("DrivenFirstName");
                this.OnPropertyChanged("DrivenLastName");

                this.OnPropertyChanged("AllowSave");
            }
        }



        public string DrivenFirstName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ScheduledTripData._drivenFirstName) && ScheduledByCustomer != null)

                    return ScheduledByCustomer.FirstName;
                else

                    return ScheduledTripData._drivenFirstName; ;
            }
            //return ScheduledTripData._drivenFirstName; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))

                    value = ScheduledTripData._customerName;

                ScheduledTripData._drivenFirstName = value;
            }




            //get { return ScheduledTripData._drivenFirstName; }
            //set { ScheduledTripData._drivenFirstName = value; }
        }
        public string DrivenLastName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ScheduledTripData._drivenLastName) && ScheduledByCustomer != null)

                    return ScheduledByCustomer.LastName;
                else

                    return ScheduledTripData._drivenLastName;
            }

            set { ScheduledTripData._drivenLastName = value; }
        }


        public string CustomerPhoneNo
        {
            get { return ScheduledTripData._customerPhoneNo; }
            set
            {
                ScheduledTripData._customerPhoneNo = value;
                this.OnPropertyChanged("AllowSave");
            }
        }



        public const string MatchPhonePattern = @"^(1?(-?\d{3})-?)?(\d{3})(-?\d{4})$";
        public bool IsPhoneNumber(string phoneNumber)
        {
            if (phoneNumber != null)
                return System.Text.RegularExpressions.Regex.IsMatch(phoneNumber, MatchPhonePattern);
            else
                return false;
        }






        public string CardNo
        {
            get { return ScheduledTripData._cardNo; }
            set { ScheduledTripData._cardNo = value; }
        }
        public DateTime PickupDate
        {
            get { return ScheduledTripData._pickupDate; }
            set { ScheduledTripData._pickupDate = value; }
        }

        public DateTime? PickupTime
        {
            get { return ScheduledTripData._pickupTime; }
            set
            {
                ScheduledTripData._pickupTime = value;
                this.OnPropertyChanged("AllowSave");
            }
        }
        public string PickupAddress
        {
            get { return ScheduledTripData._pickupAddress; }
            set
            {
                ScheduledTripData._pickupAddress = value;
                this.OnPropertyChanged("AllowSave");
            }
        }
        public string PickupCity
        {
            get { return ScheduledTripData._pickupCity; }
            set
            {
                ScheduledTripData._pickupCity = value;
                this.OnPropertyChanged("AllowSave");

            }
        }
        public string PickupState
        {
            get { return ScheduledTripData._pickupState; }
            set { ScheduledTripData._pickupState = value; }
        }
        public string PickupZip
        {
            get { return ScheduledTripData._pickupZip; }
            set { ScheduledTripData._pickupZip = value; }
        }
        public string DropOffAddress
        {
            get { return ScheduledTripData._dropOffAddress; }
            set { ScheduledTripData._dropOffAddress = value; }
        }
        public string DropOffCity
        {
            get { return ScheduledTripData._dropOffCity; }
            set
            {
                ScheduledTripData._dropOffCity = value;
                this.OnPropertyChanged("AllowSave");
            }
        }
        public string DropOffState
        {
            get { return ScheduledTripData._dropOffState; }
            set { ScheduledTripData._dropOffState = value; }
        }
        public string DropOffZip
        {
            get { return ScheduledTripData._dropOffZip; }
            set { ScheduledTripData._dropOffZip = value; }
        }

        public string DropOffCityStatePostalCode
        {
            get
            {
                return this.DropOffCity + ", " + this.DropOffState + "  " + this.DropOffZip;
            }
        }

        public TripAreaTypes TripAreaType
        {
            get { return ScheduledTripData._tripAreaType; }
            set
            {
                ScheduledTripData._tripAreaType = value;
                this.OnPropertyChanged("IsLongDistanceTrip");
            }
        }

        public bool IsLongDistanceTrip
        {
            get { return this.TripAreaType == TripAreaTypes.LongDistance; }
            set
            {
                if (value)
                    this.ScheduledTripData._tripAreaType = TripAreaTypes.LongDistance;
                else
                    this.ScheduledTripData._tripAreaType = TripAreaTypes.Local;

                this.OnPropertyChanged("TripAreaType");
            }
        }
        public int AssignedToDriverID
        {
            get { return ScheduledTripData._assignedToDriverID; }
            set { ScheduledTripData._assignedToDriverID = value; }
        }

        int _eTAToLocation1Min;

        public int ETAToLocation1Min
        {
            get { return ETAToLocation1 == null ? 0 : (int)(this.ETAToLocation1.GetValueOrDefault(DateTime.Now) - this.PickupTime.GetValueOrDefault(DateTime.Now)).TotalMinutes; ; }
            set
            { //_eTAToLocation1Min = value;
                this.ETAToLocation1 =
                    this.PickupTime.GetValueOrDefault(DateTime.Now).AddMinutes(value);
                this.OnPropertyChanged("ETAToLocation1");

            }
        }

        int _eTABackToBaseMin;

        public int ETABackToBaseMin
        {
            get { return ETABackToBase == null ? 0 : (int)(this.ETABackToBase.GetValueOrDefault(DateTime.Now) - this.PickupTime.GetValueOrDefault(DateTime.Now)).TotalMinutes; }
            set
            {
                // _eTABackToBaseMin = value;
                this.ETABackToBase =
                    this.PickupTime.GetValueOrDefault(DateTime.Now).AddMinutes(value);
                this.OnPropertyChanged("ETABackToBase");

            }
        }

        public DateTime? ETAToLocation1
        {
            /* get { return ScheduledTripData._eTAToLocation1; }
             set { ScheduledTripData._eTAToLocation1 = value; }*/

            get { return ScheduledTripData._eTAToLocation1; }
            set
            {
                if (value != null)
                {
                    DateTime e = value.Value;
                    ScheduledTripData._eTAToLocation1 = ScheduledTripData._pickupDate.Add(e.TimeOfDay);
                }
                else
                    ScheduledTripData._eTAToLocation1 = value;
                return;

            }
        }

        public DateTime? ETABackToBase
        {
            get { return ScheduledTripData._eTABackToBase; }
            set
            {

                //       ScheduledTripData._eTABackToBase = value; 
                if (value != null)
                {
                    DateTime e = value.Value;
                    ScheduledTripData._eTABackToBase = ScheduledTripData._pickupDate.Add(e.TimeOfDay);
                }
                else
                    ScheduledTripData._eTABackToBase = value;
                return;

            }
        }
        public DateTime? DateAdded
        {
            get { return ScheduledTripData._dateAdded; }
            set { ScheduledTripData._dateAdded = value; }
        }



        public decimal TripPrice
        {
            get { return ScheduledTripData._tripPrice; }
            set { ScheduledTripData._tripPrice = value; }
        }
        public decimal WaitingCahrge
        {
            get { return ScheduledTripData._waitingCahrge; }
            set { ScheduledTripData._waitingCahrge = value; }
        }


        public decimal ExtraCharge
        {
            get { return ScheduledTripData._extraCharge; }
            set { ScheduledTripData._extraCharge = value; }
        }


        public decimal MiniVanCharge
        {
            get { return ScheduledTripData._miniVanCharge; }
            set { ScheduledTripData._miniVanCharge = value; }
        }

        public int TripID
        {
            get { return ScheduledTripData._tripID; }
            set { ScheduledTripData._tripID = value; }
        }
        public bool NeedToConfirm
        {
            get { return ScheduledTripData._needToConfirm; }
            set
            {
                ScheduledTripData._needToConfirm = value;
                this.OnPropertyChanged("NeedToConfirm");
            }
        }
        public bool IsRoundTrip
        {
            get { return ScheduledTripData._isRoundTrip; }
            set { ScheduledTripData._isRoundTrip = value; }
        }
        public bool IsCanceldTrip
        {
            get { return ScheduledTripData._isCanceldTrip; }
            set { ScheduledTripData._isCanceldTrip = value; }
        }
        public string SFromLocationComment
        {
            get { return ScheduledTripData._sFromLocationComment; }
            set { ScheduledTripData._sFromLocationComment = value; }
        }
        public string SToLocationComment
        {
            get { return ScheduledTripData._sToLocationComment; }
            set { ScheduledTripData._sToLocationComment = value; }
        }
        public VehicleTypes SVehicleTypeRequested
        {
            get { return ScheduledTripData.sVehicleTypeRequested; }
            set
            {


                ScheduledTripData.sVehicleTypeRequested = value;
            }
        }
        [DataMember]
        public int SCustomerPassCodeID
        {
            get { return ScheduledTripData._sCustomerPassCodeID; }
            set { ScheduledTripData._sCustomerPassCodeID = value; }
        }
        [DataMember]
        public int? SNoOfStops
        {
            get { return ScheduledTripData._sNoOfStops; }
            set { ScheduledTripData._sNoOfStops = value; }
        }

        public bool NeedsMiniVan
        {
            get
            {
                //  if ((this.NeedsMiniVan == false)&&(this.SVehicleTypeRequested!=VehicleTypes.Car))
                //   this.ScheduledTripData.sVehicleTypeRequested = VehicleTypes.Not_Specified;

                return ScheduledTripData._needsMiniVan;
            }
            set
            {
                if (value == true)
                    this.SVehicleTypeRequested = VehicleTypes.MiniVan;
                else if ((value == false) && (this.SVehicleTypeRequested != VehicleTypes.Car))
                    this.ScheduledTripData.sVehicleTypeRequested = VehicleTypes.Not_Specified;

                ScheduledTripData._needsMiniVan = value;

                this.OnPropertyChanged("SVehicleTypeRequested");
            }
        }

        public override bool AllowSave
        {
            get
            {
                return
                /*(this.ScheduledByCustomer != null ||
                 !string.IsNullOrWhiteSpace(this.CustomerName))

                 &&*/
                !string.IsNullOrWhiteSpace(this.PickupAddress)
                 // && this.PickupTime != null
                 && !string.IsNullOrWhiteSpace(this.PickupCity)
                 && !string.IsNullOrWhiteSpace(this.DropOffCity)
                 && (!string.IsNullOrWhiteSpace(this.CustomerPhoneNo) ||
                 this.ScheduledByCustomer != null);
            }

        }
        #region IEditableObject Members
        bool _inEdit = false;

        public bool InEdit
        {
            get { return _inEdit; }
            set { _inEdit = value; }
        }
        public void BeginEdit()
        {
            if (!this._inEdit)
            {
                this._inEdit = true;
                this._backUpData = this.ScheduledTripData;
            }
        }

        public void CancelEdit()
        {
            if (!this._inEdit)
                return;

            this.ScheduledTripData = this._backUpData;
            this._inEdit = false;
        }

        public void EndEdit()
        {
            this._inEdit = false;
            this._backUpData = new ScheduledTripDataStruct();
        }

        #endregion

        public int FirstLegAppliedToTripID { get; set; }

        public int SecondLegAppledToTripID { get; set; }

        public int ThirdLegAppliedToTripID { get; set; }



        public bool IsSaved
        {
            get { return this.ScheduledTripID > 0; }
        }

        public bool IsNotSaved
        {
            get { return this.ScheduledTripID == 0; }
        }


    }
}
