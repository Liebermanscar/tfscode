﻿using CreditLimitService.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CreditLimitService
{

    public static class CreditMethods
    {
        // static CDSEntities context = new CDSEntities();
        public static List<Customer> GetAllCustomerWithCreditLImit()
        {
            using (CDSEntities context = new CDSEntities())
            {
                string val = ConfigurationManager.AppSettings["CheckOnlyIfCreditAdded"].ToString();
                if (val == "1")
                {
                    var d = context.Customers.Where(k => k.AmountOfCreditLimit > 0).ToList();
                    return d.ToList();
                }
                else
                {
                    var d = context.Customers.ToList();
                    return d.ToList();
                }
            }
        }
        public static bool IsCustomerCreditLimitExpired(CustomerInfo customer)
        {
            using (CDSEntities context = new CDSEntities())
            {
                decimal TotalDueAmount = 0;
                decimal TotalNonInvoice = 0;
                var tt = context.GetCustomerOpenBalance(customer.Customer.CustomerID, false).FirstOrDefault();
                TotalDueAmount = tt.TotalDue != null ? tt.TotalDue.Value : TotalDueAmount;
                TotalNonInvoice = tt.TotalNotInvoiced != null ? tt.TotalNotInvoiced.Value : TotalNonInvoice;
                //var trip = context.Trips.Where(k => k.DrivenCustomerID == customer.Customer.CustomerID).ToList();
                //TripTotalAmount = trip.Count > 0 ? trip.Select(s => s.TotalPrice).Sum() : TripTotalAmount;
                //var db = context.Payments.Where(k => k.CustomerID == customer.Customer.CustomerID && k.isCreditable == true).ToList();
                //if (db.Count > 0)
                //{
               // PaymentTotalAmount = db.Count > 0 ? db.Select(s => s.AmountPaid).Sum() : PaymentTotalAmount;
                var balanceAmount = TotalDueAmount + TotalNonInvoice;
                customer.OutstandingAmount = balanceAmount;
                customer.NotInvoicedBalance = TotalNonInvoice;
                customer.InvoicedBalance = TotalDueAmount;
                decimal? creditAmount = string.IsNullOrEmpty(Convert.ToString(customer.Customer.AmountOfCreditLimit)) ? 0 : customer.Customer.AmountOfCreditLimit;
                customer.Customer.AmountOfCreditLimit = creditAmount;
                if (creditAmount < balanceAmount)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //}
                //else
                //{

                //    return true;
                //}
            }
        }
        public static void SendEmail(CustomerInfo customer)
        {
            string emailaddress = string.Empty;
            string adminkey = ConfigurationManager.AppSettings["AdminReportEmail"].ToString();
            string toAddress = string.Empty;
            using (CDSEntities context = new CDSEntities())
            {
                try
                {
                    emailaddress = context.FacilitySettings.Where(k => k.SettingKey == adminkey).FirstOrDefault().SettingValue;
                }
                catch
                {
                    emailaddress = string.Empty;
                }
                if (string.IsNullOrEmpty(emailaddress))
                {
                    toAddress = ConfigurationManager.AppSettings["adminEmail"].ToString();
                }
                else
                {
                    toAddress = emailaddress;
                }
            }
            MailMessage msg = new MailMessage();
            foreach (var address in toAddress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                msg.To.Add(address);
            }
            //msg.To.Add(new MailAddress(toAddress));
            msg.Subject = ConfigurationManager.AppSettings["MailSubject"].ToString();
            msg.Body = CreateEmailBody(customer);
            msg.IsBodyHtml = true;
            SmtpClient emailClient = new SmtpClient();
            emailClient.Send(msg);
        }
        private static string CreateEmailBody(CustomerInfo customer)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "/Email.html"))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{name}", customer.Customer.FirstName + ' ' + customer.Customer.LastName);
            body = body.Replace("{outstanding}", "$" + decimal.Parse(customer.OutstandingAmount.ToString()).ToString("N2"));
            body = body.Replace("{iob}", "$" + decimal.Parse(customer.InvoicedBalance.ToString()).ToString("N2"));
            body = body.Replace("{niob}", "$" + decimal.Parse(customer.NotInvoicedBalance.ToString()).ToString("N2"));
            body = body.Replace("{creditlimit}", "$" + decimal.Parse(customer.Customer.AmountOfCreditLimit.ToString()).ToString("N2"));
            body = body.Replace("{email}", customer.Customer.EmailAddress);
            string PhoneNumber = string.Empty;
            if (!string.IsNullOrEmpty(customer.Customer.HomePhoneNo))
            {
                PhoneNumber = customer.Customer.HomePhoneNo;
            }
            if (!string.IsNullOrEmpty(customer.Customer.BusinessPhoneNo))
            {
                if (!string.IsNullOrEmpty(PhoneNumber))
                    PhoneNumber += ", " + customer.Customer.BusinessPhoneNo;
                else
                    PhoneNumber = customer.Customer.BusinessPhoneNo;
            }
            if (!string.IsNullOrEmpty(customer.Customer.CellPhoneNo))
            {
                if (!string.IsNullOrEmpty(PhoneNumber))
                    PhoneNumber += ", " + customer.Customer.CellPhoneNo;
                else
                    PhoneNumber = customer.Customer.CellPhoneNo;
            }

            body = body.Replace("{phone}", PhoneNumber);
            body = body.Replace("{address}", customer.Customer.StreetName + "<br/>" + customer.Customer.State + ' ' + customer.Customer.City + ' ' + customer.Customer.PostalCode);

            return body;
        }

    }
}
