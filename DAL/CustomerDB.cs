﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOL;
using System.Collections.ObjectModel;

namespace DAL
{
    public class CustomerDB : Database
    {
        public CustomerDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(Customer customer)
        {

            string serverMessage;
            SqlParameter outParam = new SqlParameter("@CustomerID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCustomers", new SqlParameter[]
            {
                new SqlParameter("@CustomerType", customer.CustomerType),
                new SqlParameter("@Prefix", customer.Prefix),
                new SqlParameter("@FirstName", customer.FirstName),
                new SqlParameter("@MI", customer.MI),
                new SqlParameter("@LastName", customer.LastName),
                new SqlParameter("@Suffix", customer.Suffix),
                new SqlParameter("@CompanyName", customer.CompanyName),
                new SqlParameter("@HouseNo", customer.HouseNo),
                new SqlParameter("@StreetName", customer.StreetName),
                new SqlParameter("@SuiteNo", customer.SuiteNo),
                new SqlParameter("@City", customer.City),
                new SqlParameter("@State", customer.State),
                new SqlParameter("@PostalCode", customer.PostalCode),
                new SqlParameter("@Country", customer.Country),
                new SqlParameter("@HomePhoneNo", customer.HomePhoneNo),
                new SqlParameter("@BusinessPhoneNo", customer.BusinessPhoneNo),
                new SqlParameter("@CellPhoneNo", customer.CellPhoneNo),
                new SqlParameter("@FaxNo",customer.FaxNo),
                new SqlParameter("@EmailAddress",customer.EmailAddress),
                new SqlParameter("@IsinvoiceCustomer",customer.IsInvoiceCustomer),
                new SqlParameter("@NeedsPassCode",customer.NeedsPassCode),
                new SqlParameter("@AmountOfCreditLimit",customer.AmountOfCreditLimit),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customer.CustomerID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }

        public ObservableCollection<Customer> Get(CustomerFilterOptions customerFilterOption,
            string value,
            DataRequestTypes dataRequestType, bool invoiceCustomerOnly)
        {
            ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
            var param = new List<SqlParameter>();
            switch (customerFilterOption)
            {
                case CustomerFilterOptions.Name:
                    string lastName = "";
                    string firstName = "";
                    var nameSplit = value.Split(',');
                    lastName = nameSplit[0];
                    if (nameSplit.Length > 1)
                        firstName = nameSplit[1];
                    param.Add(new SqlParameter("@LastName_CompanyName", lastName));
                    param.Add(new SqlParameter("@FirstName", firstName));
                    break;
                case CustomerFilterOptions.PhoneNo:
                    param.Add(new SqlParameter("@PhoneNo", value));
                    break;
                case CustomerFilterOptions.CustomerID:
                    param.Add(new SqlParameter("@CustomerID", Convert.ToInt32(value)));
                    break;
                case CustomerFilterOptions.Automatic:
                    int n;
                    bool isNumeric = int.TryParse(value.Substring(1), out n);//when entered phone of 10 digits the tryparse returns false cause its greater then the max of integer, so substring fix it
                    if (isNumeric == true)
                        param.Add(new SqlParameter("@PhoneNo", value));
                    else
                    {
                        string lName = "";
                        string fName = "";
                        var nSplit = value.Split(',');
                        lName = nSplit[0];
                        if (nSplit.Length > 1)
                            fName = nSplit[1];
                        param.Add(new SqlParameter("@LastName_CompanyName", lName));
                        param.Add(new SqlParameter("@FirstName", fName));

                    }



                    break;
                case CustomerFilterOptions.None:
                    break;
                default:
                    break;



            }
            param.Add(new SqlParameter("@InvoiceCustomerOnly", invoiceCustomerOnly));


            using (SqlDataReader dr = this.GetReader("GetCustomers", param.ToArray(), System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    var cust = FillCustomer(dr);

                    //customers.Add(new Customer()
                    //{
                    //    CustomerID = Convert.ToInt32(dr["CustomerID"]),
                    //    CustomerType = (CustomerTypes)Convert.ToInt32(dr["CustomerType"]),
                    //    CompanyName = dr["CompanyName"].ToString(),
                    //    Prefix = dr["Prefix"].ToString(),
                    //    FirstName = dr["FirstName"].ToString(),
                    //    MI = dr["MI"].ToString(),
                    //    LastName = dr["LastName"].ToString(),
                    //    Suffix = dr["Suffix"].ToString(),
                    //    HouseNo = dr["HouseNo"].ToString(),
                    //    StreetName = dr["StreetName"].ToString(),
                    //    SuiteNo = dr["SuiteNo"].ToString(),
                    //    City = dr["City"].ToString(),
                    //    State = dr["State"].ToString(),
                    //    PostalCode = dr["PostalCode"].ToString(),
                    //    Country = dr["Country"].ToString(),

                    //    HomePhoneNo = dr["HomePhoneNo"].ToString(),
                    //    BusinessPhoneNo = dr["BusinessPhoneNo"].ToString(),
                    //    CellPhoneNo = dr["CellPhoneNo"].ToString()

                    //});

                    if (dataRequestType == DataRequestTypes.FullData)
                    {
                        cust.InsuranceInformation = DAL.CustomerInsuranceInfoDB.GetCustomersInsuranceInformation(cust.CustomerID);
                        cust.CustomerInvoices = DAL.InvoiceDB.GetInvoices(0, cust.CustomerID, 0);
                    }
                    customers.Add(cust);
                }
            }

            return customers;
        }

        public static bool AddCustomer(Customer customer)
        {
            using (CustomerDB customerDb = new CustomerDB(null, null))
            {
                return customerDb.Add(customer);
            }
        }

        public static ObservableCollection<Customer> GetCustomers(CustomerFilterOptions customerFilterOption, string value, DataRequestTypes dataRequestType, bool invoiceCustomersOnly)
        {
            using (CustomerDB customerDB = new CustomerDB(null, null))
            {
                return customerDB.Get(customerFilterOption, value, dataRequestType, invoiceCustomersOnly);
            }
        }

        public ObservableCollection<Customer> getOpenBalance(int customerID, bool onlyNotInvoiced)
        {
            var customers = new ObservableCollection<Customer>();

            using (SqlDataReader dr = this.GetReader("GetCustomerOpenBalance", new SqlParameter[]
            {
                new SqlParameter("@CustomerID", customerID),
                new SqlParameter("@OnlyNotInvoiced", onlyNotInvoiced),

            }, System.Data.CommandType.StoredProcedure))
            {

                if (onlyNotInvoiced == false)
                {
                    while (dr.Read())
                    {

                        /* var cust = FillCustomer(dr);
                         cust.OpenBalanceDue = Convert.ToDecimal(dr["TotalDue"]);
                         customers.Add(cust);*/
                        customers.Add(new Customer()
                        {
                            CustomerID = Convert.ToInt32(dr["CustomerID"]),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            CompanyName = dr["CompanyName"].ToString(),
                            HomePhoneNo = dr["HomePhoneNo"].ToString(),
                            OpenBalanceDue = Convert.ToDecimal(dr["TotalDue"]),
                            BalanceNotInvoiced = dr["TotalNotInvoiced"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["TotalNotInvoiced"])
                        });
                    }
                }
                else
                {
                    /*  string sum;
                      while (dr.Read())
                      { 
                           sum = dr["TotalNotInvoiced"].ToString();
                      }
                   return sum;*/

                }
            };

            return customers;
        }

        public ObservableCollection<Customer> getLastName()
        {
            var customers = new ObservableCollection<Customer>();

            using (SqlDataReader dr = this.GetReader("GetCustomers", new SqlParameter[]
            {
                new SqlParameter("@GetLastName", true),

            }, System.Data.CommandType.StoredProcedure))
            {

                while (dr.Read())
                {
                    customers.Add(new Customer()
                    {
                        CustomerID = Convert.ToInt32(dr["CustomerID"]),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        HomePhoneNo = dr["HomePhoneNo"].ToString()
                    });
                }
            };

            return customers;
        }

        public static ObservableCollection<Customer> GetCustomerLastName()
        {
            using (CustomerDB customerDB = new CustomerDB(null, null))
            {
                return customerDB.getLastName();
            }
        }

        public string getSumNotInvoiced(int customerID, bool onlyNotInvoiced)
        {

            using (SqlDataReader dr = this.GetReader("GetCustomerOpenBalance", new SqlParameter[]
            {
                new SqlParameter("@CustomerID", customerID),
                new SqlParameter("@OnlyNotInvoiced", onlyNotInvoiced),

            }, System.Data.CommandType.StoredProcedure))
            {

                {
                    string sum;
                    while (dr.Read())
                    {
                        return sum = dr["TotalNotInvoiced"].ToString();
                    }
                    //  return sum;
                    return sum = "";
                }
            };

            //  return customers;
        }

        public static string GetSumNotInvoiced(int customerID, bool onlyNotInvoiced)
        {

            using (CustomerDB customerDB = new CustomerDB(null, null))
            {
                return customerDB.getSumNotInvoiced(customerID, onlyNotInvoiced);
            }


        }
        public static ObservableCollection<Customer> GetCustomerOpenBalance(int customerID = 0, bool onlyNotInvoiced = false)
        {
            using (CustomerDB customerDB = new CustomerDB(null, null))
            {
                return customerDB.getOpenBalance(customerID, onlyNotInvoiced);
            }
        }
        public static bool EditCustomer(Customer customer)
        {
            using (CustomerDB customerDB = new CustomerDB(null, null))
            {
                return customerDB.Edit(customer);
            }
        }

        private bool Edit(Customer customer)
        {
            string serverMessage;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@CustomerID", customer.CustomerID),
                new SqlParameter("@CustomerType", customer.CustomerType),
                new SqlParameter("@CompanyName", customer.CompanyName),
                new SqlParameter("@Prefix", customer.Prefix),
                new SqlParameter("@FirstName", customer.FirstName),
                new SqlParameter("@MI", customer.MI),
                new SqlParameter("@LastName", customer.LastName),
                new SqlParameter("@Suffix", customer.Suffix),
                new SqlParameter("@HouseNo", customer.HouseNo),
                new SqlParameter("@StreetName", customer.StreetName),
                new SqlParameter("@SuiteNo",customer.SuiteNo),
                new SqlParameter("@City",customer.City),
                new SqlParameter("@State", customer.State),
                new SqlParameter("@PostalCode", customer.PostalCode),
                new SqlParameter("@Country", customer.Country),
                new SqlParameter("@HomePhoneNo", customer.HomePhoneNo),
                new SqlParameter("@BusinessPhoneNo", customer.BusinessPhoneNo),
                new SqlParameter("@CellPhoneNo",customer.CellPhoneNo),
                new SqlParameter("@FaxNo",customer.FaxNo),
                new SqlParameter("@EmailAddress",customer.EmailAddress),
                new SqlParameter("@IsinvoiceCustomer",customer.IsInvoiceCustomer),
                new SqlParameter("@NeedsPassCode",customer.NeedsPassCode),
                new SqlParameter("@AmountOfCreditLimit",customer.AmountOfCreditLimit)
            };

            int nofOfRecordsAffected = this.ExecuteNonQuery("UpdateCustomers", param, System.Data.CommandType.StoredProcedure, out serverMessage);

            return nofOfRecordsAffected > 0;
        }

        public static Customer FillCustomer(SqlDataReader customerDR)
        {
            return new Customer()
            {
                CustomerID = Convert.ToInt32(customerDR["CustomerID"]),
                CustomerType = (CustomerTypes)Convert.ToInt32(customerDR["CustomerType"]),
                CompanyName = customerDR["CompanyName"].ToString(),
                Prefix = customerDR["Prefix"].ToString(),
                FirstName = customerDR["FirstName"].ToString(),
                MI = customerDR["MI"].ToString(),
                LastName = customerDR["LastName"].ToString(),
                Suffix = customerDR["Suffix"].ToString(),
                HouseNo = customerDR["HouseNo"].ToString(),
                StreetName = customerDR["StreetName"].ToString(),
                SuiteNo = customerDR["SuiteNo"].ToString(),
                City = customerDR["City"].ToString(),
                State = customerDR["State"].ToString(),
                PostalCode = customerDR["PostalCode"].ToString(),
                Country = customerDR["Country"].ToString(),

                HomePhoneNo = customerDR["HomePhoneNo"].ToString(),
                BusinessPhoneNo = customerDR["BusinessPhoneNo"].ToString(),
                CellPhoneNo = customerDR["CellPhoneNo"].ToString(),
                FaxNo = customerDR["FaxNo"].ToString(),
                EmailAddress = customerDR["EmailAddress"].ToString(),
                IsInvoiceCustomer = Convert.ToBoolean(customerDR["IsInvoiceCustomer"]),
                NeedsPassCode = Convert.ToBoolean(customerDR["NeedsPassCode"]),
                AmountOfCreditLimit = string.IsNullOrEmpty(Convert.ToString(customerDR["AmountOfCreditLimit"])) ? 0 : Convert.ToDecimal(customerDR["AmountOfCreditLimit"])
            };
        }
    }

    public class CustomerInsuranceInfoDB : Database
    {
        public CustomerInsuranceInfoDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }
        public bool Add(CustomerInsuranceInfo customerInsInfo)
        {
            string serverMessage;
            SqlParameter outParam = new SqlParameter("@CustomerInsuranceInfoID", System.Data.SqlDbType.Int) { Direction = System.Data.ParameterDirection.Output };
            int noOfRecordsAffected = this.ExecuteNonQuery("AddCustomersInsuranceInfo", new SqlParameter[]
            {
                new SqlParameter("@CustomerID", customerInsInfo.CustomerID),
                new SqlParameter("@InsuranceType", customerInsInfo.InsuranceType),
                new SqlParameter("@CardNo", customerInsInfo.CardNo),
                new SqlParameter("@InsuredName", customerInsInfo.InsuredName),
                new SqlParameter("@InsuredGender", customerInsInfo.InsuredGender),
                outParam
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            if (noOfRecordsAffected > 0)
                customerInsInfo.CustomerInsuranceInfoID = Convert.ToInt32(outParam.Value);

            return noOfRecordsAffected > 0;
        }

        private bool Edit(CustomerInsuranceInfo customerInsInfo)
        {
            string serverMessage;

            int noOfRecordsAffected = this.ExecuteNonQuery("UpdateCustomersInsuranceInfo", new SqlParameter[]
            {
                new SqlParameter("@CustomerInsuranceInfoID", customerInsInfo.CustomerInsuranceInfoID),
                new SqlParameter("@CustomerID", customerInsInfo.CustomerID),
                new SqlParameter("@InsuranceType", customerInsInfo.InsuranceType),
                new SqlParameter("@CardNo", customerInsInfo.CardNo),
                new SqlParameter("@InsuredName", customerInsInfo.InsuredName),
                new SqlParameter("@InsuredGender", customerInsInfo.InsuredGender)
            }, System.Data.CommandType.StoredProcedure, out serverMessage);

            return noOfRecordsAffected > 0;
        }

        public ObservableCollection<CustomerInsuranceInfo> Get(int customerInsuranceID,
            int customerID,
            string cardNo,
            InsuranceTypes insuranceType
            )
        {
            var customerInsuranceList = new ObservableCollection<CustomerInsuranceInfo>();
            using (var dr = this.GetReader("GetCustomersInsuranceInfo", new SqlParameter[]
            {
                new SqlParameter("@CustomerId", customerID),
                new SqlParameter("@CustomerInsuranceInfoID", customerInsuranceID),
                new SqlParameter("@CardNo", cardNo),
                new SqlParameter("@InsuranceType", insuranceType)
            }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    customerInsuranceList.Add(new CustomerInsuranceInfo()
                    {
                        CustomerInsuranceInfoID = Convert.ToInt32(dr["CustomerInsuranceInfoID"]),
                        CustomerID = Convert.ToInt32(dr["CustomerID"]),
                        InsuranceType = (InsuranceTypes)Convert.ToInt32(dr["InsuranceType"]),
                        InsuredGender = dr["InsuredGender"].ToString(),
                        InsuredName = dr["InsuredName"].ToString(),
                        CardNo = dr["CardNo"].ToString()
                    });
                }
            }

            return customerInsuranceList;
        }

        internal static ObservableCollection<CustomerInsuranceInfo> GetCustomersInsuranceInformation(int customerID)
        {
            using (var custInsuranceInfoDB = new CustomerInsuranceInfoDB(null, null))
            {
                return custInsuranceInfoDB.Get(0, customerID, "", 0);
            }
        }

        public static bool AddCustomerInsuranceInfo(CustomerInsuranceInfo custIns)
        {
            using (var custInsuranceInfoDB = new CustomerInsuranceInfoDB(null, null))
            {
                return custInsuranceInfoDB.Add(custIns);
            }
        }

        public static bool EditCustomerInsuranceInfo(CustomerInsuranceInfo custIns)
        {
            using (var custInsuranceInfoDB = new CustomerInsuranceInfoDB(null, null))
            {
                return custInsuranceInfoDB.Edit(custIns);

            }
        }


    }



}
