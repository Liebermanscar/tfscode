﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SqlPage.xaml
    /// </summary>
    public partial class SqlPage : Page, IDisplayTextBlock
    {
        public SqlPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (DAL.Database db = new DAL.Database(null, null))
                {
                    var dr = db.GetReader(string.IsNullOrWhiteSpace(this.txtSqlStatment.SelectedText) ? this.txtSqlStatment.Text : this.txtSqlStatment.SelectedText, null, System.Data.CommandType.Text);

                    DataTable myTable = new DataTable();

                    myTable.Load(dr);

                    //for (int i = 0; i < dr.FieldCount; i++)
                    //{
                    //    myTable.Columns.Add(dr.GetName(i));
                    //}

                    //while (dr.Read())
                    //{
                    //    List<string> row = new List<string>();

                    //    for (int i = 0; i < dr.FieldCount; i++)
                    //    {
                    //        row.Add(dr[i].ToString());
                    //    }

                    //    myTable.Rows.Add(row.ToArray());
                    //}

                    this.sqlDataGrid.ItemsSource = myTable.AsDataView();
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }

        }

        private void ButtonPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                PrintDialog pd = new PrintDialog();
                pd.PrintVisual(this.sqlDataGrid, "Report");

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        #region IDisplay
        public string HeaderText
        {
            get
            {
                return "SQL Reports";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
