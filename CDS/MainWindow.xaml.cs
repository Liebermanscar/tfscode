﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            UIController.MainWindowReference = this;
            UIController.StartupInitalization();
        }

        private void lCustomerInfo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCustomerInfoPage(null);
        }

        private void lProcessDailyEnvelope_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadProcessDailyEnvelopesPage(null);
        }
        private void lProcessDriverPayment_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadProcessDriverPayment(null);
        }

        private void lDrivers_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDriversListPage();
        }

        private void lOpenCommisionTypes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCommisionTypesListPage();
        }

        private void lSetup_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadSetupPage();
        }

        private void lProcessInvoices_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadProcessInvoicesPage();
        }

        private void lDispatching_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDispatchingPage(sender == this.lDispatchingRemotely, false);
        }
        private void lDispatchingLongDistance_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDispatchingPage(sender == this.lDispatchingLongDistanceRemotely, true);
        }
        // **IH**
        private void lDispatchTripList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadTripsListFroMainWindow();
        }

        private void lImportFidelisTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.ImportFidelisTrips();
        }

        private void lImportMedicaidTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.GetMedicaidTripsFromWeb();
        }

        private void lImpoertMedicaidTripsFromWeb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.ImportMedicaidTrips(true);
        }

        private void lImpoertPANFromWeb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //UIController.ImportMedicaidPAN();
            UIController.LoadMedBillingPage();
        }

        private void lSubmitPANFromWeb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.SubmitPAN((sender == this.lSubmitPANHipaa || sender == lSubmitPANHipaaAdjustment), sender == lSubmitPANHipaaAdjustment);
        }

        private void lImportMedicaidTripsFromFile_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.ImportMedicaidTrips(false);
        }

        private void lScheduledTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadScheduledTripsPage();
        }

        private void lAddMedicaidTrip_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.ShowAddEditMedicaidTripWindow(new MedicaidTrip());
        }

        private void lAddFidelisTrip_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.ShowAddEditFidelisTripWindow(new FidelisTrip());
        }

        private void lReportDesigner_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadReportDesigner();
        }

        private void lCustomers_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCustomersListPage();
        }

        private void lReportMedicaidtrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadMedicaidTripsPage();
        }

        private void lAddDriverExpense_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadAddEditDriverExpenseWindow(0, ModificationTypes.Add);

        }

        private void lReportAllTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadAllTripsPage();
        }

        private void lListDriverPayments_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDriverPaymentListPage();
        }

        private void lListDailyEnvelopes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDailyEnvelopePage();
        }

        private void lListInvoices_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadInvoiceListPage();
        }

        private void lListCashAccountTransactions_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCashAccountTransactionsListPage();
        }

        private void lScheduleLongDistanceTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadScheduleTripWindow();
        }

        private void lListPayments_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadPaymentsListPage();
        }

        private void lAssignLongDistanceTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Today;
            dt = dt.AddDays(1);
            UIController.ShowAssignDriversToScheduledTrips(dt);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (UIController.NoOfOpenPages() > 0)
                {
                    e.Cancel = MessageBox.Show("You've Pages open\n\nAny data that are not save will be Lost\n\nContinue?", "Close Tabs?", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes;
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void lCustomersOpenBalance_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadCustomerOpenBalancePage();
        }

        private void lHomePage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            UIController.LoadHomePage();
        }

        private void IDriversStatusLogs_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadDriverStatusLogListPage();
        }

        private void lLogTicketBooks_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadLogTicketBookPage();
        }

        private void TicketStatusList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                UIController.LoadTicketsStatusesListPage();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        private void DriverPaymentwithFilter_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                UIController.DriverPaymentwithFilterPage();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.PrintCustomersLastName();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (this.txtExpanderHeader != null)
                this.txtExpanderHeader.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            if (this.txtExpanderHeader != null)
                this.txtExpanderHeader.Visibility = System.Windows.Visibility.Visible;
        }

        private void CommonTasks_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.stackCommonTasks.Visibility = (this.stackCommonTasks.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void ImportTrips_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.importTrips.Visibility = (this.importTrips.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void Lists_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Lists.Visibility = (this.Lists.Visibility == Visibility.Visible) ? Visibility.Collapsed : Visibility.Visible;
        }

        private void SQL_TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UIController.LoadSQLPage();
        }
    }
}
