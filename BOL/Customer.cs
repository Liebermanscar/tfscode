﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace BOL
{

    [DataContract]
    public class Customer : BO
    {

        public static Customer NewCustomer()
        {
            return new Customer() { ObjectState = BOState.New };
        }

        public Customer()
        {
            this.Payments = new ObservableCollection<Payment>();
            this.CCOnFile = new ObservableCollection<CCOnFile>();
        }

        int _customerID;

        [DataMember]
        public int CustomerID
        {
            get { return _customerID; }
            set { _customerID = value; }
        }

        string _prefix;

        [DataMember]
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }
        string _suffix;

        [DataMember]
        public string Suffix
        {
            get { return _suffix; }
            set { _suffix = value; }
        }
        string _mI;

        [DataMember]
        public string MI
        {
            get { return _mI; }
            set { _mI = value; }
        }

        string _lastName;

        [DataMember]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("CustomerName"); }
        }
        string _firstName;

        [DataMember]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("CustomerName"); }
        }
        CustomerTypes _customerType;

        [DataMember]
        public CustomerTypes CustomerType
        {
            get { return _customerType; }
            set { _customerType = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("CustomerName"); }
        }

        string _companyName;

        [DataMember]
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; this.OnPropertyChanged("AllowSave"); this.OnPropertyChanged("CustomerName"); }
        }

        string _homePhoneNo;

        [DataMember]
        public string HomePhoneNo
        {
            get { return _homePhoneNo; }
            set { _homePhoneNo = value; }
        }
        string _businessPhoneNo;

        [DataMember]
        public string BusinessPhoneNo
        {
            get { return _businessPhoneNo; }
            set { _businessPhoneNo = value; }
        }
        string _cellPhoneNo;

        [DataMember]
        public string CellPhoneNo
        {
            get { return _cellPhoneNo; }
            set { _cellPhoneNo = value; }
        }

        string _faxNo;

        [DataMember]
        public string FaxNo
        {
            get { return _faxNo; }
            set { _faxNo = value; }
        }

        string _emailAddress;

        [DataMember]
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

        public string CustomerName
        {
            get
            {
                if (this.CustomerType == CustomerTypes.Company)
                    return this.CompanyName;
                else if (this.CustomerType == CustomerTypes.Person)
                    return this.FirstName + ' ' + this.LastName;
                else
                    return "No Acount";
            }
        }

        string _houseNo;

        [DataMember]
        public string HouseNo
        {
            get { return _houseNo; }
            set { _houseNo = value; }
        }
        string _streetName;

        [DataMember]
        public string StreetName
        {
            get { return _streetName; }
            set { _streetName = value; this.OnPropertyChanged("StreetName"); }
        }
        string _suiteNo;

        [DataMember]
        public string SuiteNo
        {
            get { return _suiteNo; }
            set { _suiteNo = value; this.OnPropertyChanged("SuiteNo"); }
        }
        string _city;

        [DataMember]
        public string City
        {
            get { return _city; }
            set { _city = value; this.OnPropertyChanged("City"); }
        }
        string _state;

        [DataMember]
        public string State
        {
            get { return _state; }
            set { _state = value; this.OnPropertyChanged("State"); }
        }
        string _postalCode;

        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; this.OnPropertyChanged("PostalCode"); }
        }
        string _country;

        [DataMember]
        public string Country
        {
            get { return _country; }
            set { _country = value; this.OnPropertyChanged("Country"); }
        }

        public string CityStatePostalCode
        {
            get { return this.City + ", " + this.State + "  " + this.PostalCode; }
        }

        public string StreetAddress
        {
            get
            {
                return this.HouseNo + " " + this.StreetName + "  " + this.SuiteNo;
            }
        }


        public string CustomersAddress
        {
            get { return this.StreetAddress + " " + this.City + ", " + this.State + "  " + this.PostalCode; }

        }

        public string CustomerPhoneNos
        {
            get
            {
                return string.Format("H:{0}  B:{1}  C:{2} F:{3}",
              string.IsNullOrWhiteSpace(this.HomePhoneNo) ? "na" : this.HomePhoneNo,
              string.IsNullOrWhiteSpace(this.BusinessPhoneNo) ? "na" : this.HomePhoneNo,
              string.IsNullOrWhiteSpace(this.CellPhoneNo) ? "na" : this.HomePhoneNo,
              string.IsNullOrWhiteSpace(this.FaxNo) ? "na" : this.FaxNo);
            }
        }

        bool _isInvoiceCustomer;

        [DataMember]
        public bool IsInvoiceCustomer
        {
            get { return _isInvoiceCustomer; }
            set { _isInvoiceCustomer = value; }
        }

        bool _needsPassCode;
        [DataMember]
        public bool NeedsPassCode
        {
            get { return _needsPassCode; }
            set { _needsPassCode = value; }
        }

        decimal _balanceNotInvoiced;

        public decimal BalanceNotInvoiced
        {
            get { return _balanceNotInvoiced; }
            set { _balanceNotInvoiced = value; this.OnPropertyChanged("BalanceNotInvoiced"); this.OnPropertyChanged("TotalBalance"); }
        }

        decimal _amountOfCreditLimit;

        [DataMember]
        public decimal AmountOfCreditLimit
        {
            get { return _amountOfCreditLimit; }
            set { _amountOfCreditLimit = value; this.OnPropertyChanged("AmountOfCreditLimit"); }
        }

        decimal _openBalanceDue;
        [DataMember]
        public decimal OpenBalanceDue
        {
            get { return _openBalanceDue; }
            set { _openBalanceDue = value; this.OnPropertyChanged("OpenBalanceDue"); this.OnPropertyChanged("TotalBalance"); }
        }

        public decimal TotalBalance
        {
            get
            { //decimal b; decimal.TryParse(BalanceNotInvoiced,out b); return OpenBalanceDue + b;
                return OpenBalanceDue + BalanceNotInvoiced;
            }
        }

        ObservableCollection<Trip> _customerTrips;

        [DataMember]
        public ObservableCollection<Trip> CustomerTrips
        {
            get { return _customerTrips; }
            set { _customerTrips = value; this.OnPropertyChanged("CustomerTrips"); }
        }
        ObservableCollection<Invoice> _customerInvoices;

        [DataMember]
        public ObservableCollection<Invoice> CustomerInvoices
        {
            get { return _customerInvoices; }
            set { _customerInvoices = value; this.OnPropertyChanged("CustomerInvoices"); }
        }

        ObservableCollection<CustomerInsuranceInfo> _insuranceInformation;

        [DataMember]
        public ObservableCollection<CustomerInsuranceInfo> InsuranceInformation
        {
            get { return _insuranceInformation; }
            set { _insuranceInformation = value; this.OnPropertyChanged("InsuranceInformation"); }
        }

        ObservableCollection<Payment> _payments;

        [DataMember]
        public ObservableCollection<Payment> Payments
        {
            get { return _payments; }
            set { _payments = value; this.OnPropertyChanged("Payments"); }
        }

        ObservableCollection<CCOnFile> _cCOnFile;

        [DataMember]
        public ObservableCollection<CCOnFile> CCOnFile
        {
            get { return _cCOnFile; }
            set { _cCOnFile = value; this.OnPropertyChanged("CCOnFile"); }///all objects that use  INotifyPropertyChanged  }
        }

        ObservableCollection<CustomerPassCode> _custPassCodes;
        [DataMember]
        public ObservableCollection<CustomerPassCode> CustPassCodes
        {
            get { return _custPassCodes; }
            set { _custPassCodes = value; this.OnPropertyChanged("CustPassCodes"); }
        }

        ObservableCollection<TicketBookStatus> _custTicketBookStatuses;

        [DataMember]
        public ObservableCollection<TicketBookStatus> CustTicketBookStatuses
        {
            get { return _custTicketBookStatuses; }
            set { _custTicketBookStatuses = value; this.OnPropertyChanged("CustTicketBookStatuses"); }
        }

        public override string ToString()
        {
            return this.CustomerName;
        }

        public override bool AllowSave
        {
            get
            {
                return (this.CustomerType == CustomerTypes.Person && !string.IsNullOrWhiteSpace(this.LastName))
                    || (this.CustomerType == CustomerTypes.Company && !string.IsNullOrWhiteSpace(this.CompanyName));
            }

        }
    }

    public class CustomerInsuranceInfo : BO, IEditableObject
    {
        int _customerInsuranceInfoID;

        public int CustomerInsuranceInfoID
        {
            get { return _customerInsuranceInfoID; }
            set { _customerInsuranceInfoID = value; }
        }
        InsuranceTypes _insuranceType;

        public InsuranceTypes InsuranceType
        {
            get { return _insuranceType; }
            set { _insuranceType = value; }
        }
        string _cardNo;

        public string CardNo
        {
            get { return _cardNo; }
            set { _cardNo = value; }
        }
        string _insuredName;

        public string InsuredName
        {
            get { return _insuredName; }
            set { _insuredName = value; }
        }
        string _insuredGender;

        public string InsuredGender
        {
            get { return _insuredGender; }
            set { _insuredGender = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.InsuredName)
                    && !string.IsNullOrWhiteSpace(this.CardNo);
            }

        }


        public object CustomerID { get; set; }
    }

    public enum InsuranceTypes
    {
        Medicaid = 1,
        Fidelis = 2
    }


    public enum CustomerFilterOptions
    {
        Name = 0,
        PhoneNo = 1,
        Automatic = 2,
        CustomerID = 3,
        None = 4

    }

    [DataContract]
    public enum CustomerTypes
    {
        [EnumMember]
        Person = 1,
        [EnumMember]
        Company = 2
    }
}
