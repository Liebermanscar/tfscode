﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for TicketBookSummeryPage.xaml
    /// </summary>
    public partial class TicketBookSummeryPage : Page, IDisplayTextBlock
    {
        public TicketBookSummeryPage()
        {
            InitializeComponent();
            try
            {
                this.DataContext = DAL.TicketBookStatusDB.GetBooksSummery();
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }

        }

        public ObservableCollection<TicketBookSummery> ActiveTicketBookSummery
        {
            get { return this.DataContext as ObservableCollection<TicketBookSummery>; }
            set { this.DataContext = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.DataContext = DAL.TicketBookStatusDB.GetBooksSummery();
            }
            catch (Exception ex) { UIController.DisplayErrorMessage(ex); }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DataContext = DAL.TicketBookStatusDB.GetBooksSummery(true);
        }

        #region IDisplayTextBlockMemebrs
        public string HeaderText
        {
            get
            {
                return "Ticket Book Summery Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
