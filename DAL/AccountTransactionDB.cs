﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOL;
using System.Data.SqlClient;
using System.Collections.ObjectModel;
//using System.Data.Sql;


namespace DAL
{

    public class AccountTransactionDB : Database
    {
        public AccountTransactionDB(SqlConnection conn, SqlTransaction trans) : base(conn, trans) { }

        ObservableCollection<AccountTransaction> Get()
        {
            ObservableCollection<AccountTransaction> AccountTransactions = new ObservableCollection<AccountTransaction>();

            using (SqlDataReader dr = this.GetReader("GetAccountTransactions",
               new SqlParameter[]
                {
                //    new SqlParameter( "@ForUserId",accountTransaction.ByUserSession.SessionUser.UserName)
                }, System.Data.CommandType.StoredProcedure))
            {
                while (dr.Read())
                {
                    AccountTransactions.Add(new AccountTransaction()
                    {
                        ByUser = dr["UserName"] == DBNull.Value ? null : new User()
                        {

                            UserName = dr["UserName"].ToString(),
                        },

                        Amount = Convert.ToDecimal(dr["Amount"]),
                        DateOfTransaction = Convert.ToDateTime(dr["Date"]),
                        TypeOfTransaction = dr["Transaction Type"].ToString(),

                    });
                }
            }

            return AccountTransactions;

        }



        public static ObservableCollection<AccountTransaction> GetAccountTransaction()
        {
            using (AccountTransactionDB accountTransactionDB = new AccountTransactionDB(null, null))

            {
                return accountTransactionDB.Get();
            }

        }


        public static double TotalOfOpenInvoices()
        {



            double results = 0;


            using (var db = new Database(null, null))
            {
                var toatl = db.ExecuteScalar("SELECT dbo.TotalOpenInvoices()", new SqlParameter[] { }, System.Data.CommandType.Text);

                if (toatl != DBNull.Value)
                    results = Convert.ToDouble(toatl);
                return results;
            }
        }

    }
}
