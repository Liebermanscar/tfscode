﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SelectCustomerWindow.xaml
    /// </summary>
    public partial class SelectCustomerWindow : Window
    {
        public SelectCustomerWindow()
        {
            InitializeComponent();
            this.customerSearchCtrl.Focus();
            this.customerSearchCtrl.tbSearchBox.Focus();

        }

        public SelectCustomerWindow(ObservableCollection<Customer> customers) : this()
        {
            this.Customers = customers;
            this.customerSearchCtrl.Owner = this;

            if (this.Customers != null && this.Customers.Count > 0)
            {
                this.CustomerGridControl.SelectedIndex = 0;
                this.CustomerGridControl.Focus();
                this.SelectCustomerButton.IsEnabled = true;
            }

        }

        void FocusGrid()
        {



            //CustomerGridControl.ScrollIntoView(CustomerGridControl.Items[index]);

            DataGridRow dgrow = (DataGridRow)CustomerGridControl.ItemContainerGenerator.ContainerFromItem(CustomerGridControl.Items[0]);

            dgrow.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));

        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {


                if (this.Customers == null)
                    return;
                if (this.Customers.Count > 0)

                    this.FocusGrid();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if (!this.customerSearchCtrl.IsFocused &&

                e.Key == Key.Return &&

                this.Customers != null &&

                this.Customers.Count > 0)
            {

                e.Handled = true;

                this.DialogResult = true;

            }

        }

        public ObservableCollection<Customer> Customers
        {
            get { return this.DataContext as ObservableCollection<Customer>; }
            set { this.DataContext = value; }
        }

        public Customer SelectedCustomer
        {
            get { return this.CustomerGridControl.SelectedItem as Customer; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void CustomerGridControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = true;
        }

        /*  private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
          {
              //if(!this.customerSearchCtrl.IsFocused)
              //    if (e.Key == Key.Return)
              //    {
              //        e.Handled = true;
              //        this.DialogResult = true;
              //    }
          }

          */
    }
}
