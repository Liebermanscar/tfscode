﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOL
{
    public class Location
    {
        int _LocationID;

        public int LocationID
        {
            get { return _LocationID; }
            set { _LocationID = value; }
        }
        string _City;

        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
    }

    public class TollRate
    {
        int _TollRateID;

        public int TollRateID
        {
            get { return _TollRateID; }
            set { _TollRateID = value; }
        }
        int _FromLocationID;

        public int FromLocationID
        {
            get { return _FromLocationID; }
            set { _FromLocationID = value; }
        }
        int _ToLocationID;

        public int ToLocationID
        {
            get { return _ToLocationID; }
            set { _ToLocationID = value; }
        }
        Location _FromLocation;

        public Location FromLocation
        {
            get { return _FromLocation; }
            set { _FromLocation = value; }
        }
        Location _ToLocations;

        public Location ToLocations
        {
            get { return _ToLocations; }
            set { _ToLocations = value; }
        }
        decimal _RegPrice;

        public decimal RegPrice
        {
            get { return _RegPrice; }
            set { _RegPrice = value; }
        }
        decimal _EZPrice;

        public decimal EZPrice
        {
            get { return _EZPrice; }
            set { _EZPrice = value; }
        }
    }
}
