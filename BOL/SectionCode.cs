﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOL
{
   public  class SectionCode : BO
    {

        int _sectionID;

        public int SectionID
        {
            get { return _sectionID; }
            set { _sectionID = value; }
        }


       string _sectionName;

        public string SectionName
        {
            get { return _sectionName; }
            set { _sectionName = value; }
        }

        public override bool AllowSave
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.SectionName);
            }
        }
    }
}
