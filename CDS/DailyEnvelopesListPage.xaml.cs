﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DailyEnvelopesPage.xaml
    /// </summary>
    public partial class DailyEnvelopesListPage : Page, IDisplayTextBlock
    {
        public DailyEnvelopesListPage()
        {
            InitializeComponent();
            ToDatePicker.SelectedDate = DateTime.Today;
        }

        private void buttonGetList_Click(object sender, RoutedEventArgs e)
        {
            this.dailyEnvelopes = DAL.DailyEnvelopeDB.GetDailyEnvelopes(0, 0, DateTime.Today, false, 0);
            this.EnvelopesDateDataGrid.DataContext = this.dailyEnvelopes;
        }



        public ObservableCollection<DailyEnvelope> dailyEnvelopes { get; set; }

        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            UIController.ExportTelerikGrid(EnvelopesDateDataGrid, UIController.ExportFormat.Excel);
        }

        #region

        public string HeaderText
        {
            get
            {
                return "Daily Envelopes List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        private void EnvelopesDateDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {

                if (SelectedEnvelope != null)
                {
                    UIController.LoadProcessDailyEnvelopesPage(DAL.DailyEnvelopeDB.GetDailyEnvelopes(SelectedEnvelope.DailyEnvelopeID, 0, null, false, 0)[0]);
                }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        DailyEnvelope SelectedEnvelope
        {
            get { return this.EnvelopesDateDataGrid.SelectedItem as DailyEnvelope; }
        }


    }
}
