﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OTR_App
{
	[Activity (Label = "Payment", ScreenOrientation=Android.Content.PM.ScreenOrientation.Landscape)]			
	public class TripPaymentActivity : Activity
	{
        #region Controls

        //TextView tvTripPayment_Price;
        EditText etTripPrice;
        Button btnChangePrice;
        EditText etCashPmt;
        EditText etCheckPmt;
        TextView tvTicketValue;
        Button btnGetTicket;
        Button btnChargeCC;
        Button btnGetCustomer;
        Button btnGetDriver;
        EditText etMediNo;
        Button btnAcceptPayment;
        Button btnCancelPayment;

        #endregion Controls

        int selectedTicketPosition;

        protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

            SetContentView(Resource.Layout.TripPayment);

            #region WireViewes

            etTripPrice = FindViewById<EditText>(Resource.Id.tripPayment_etTripPrice);
            btnChangePrice = FindViewById<Button>(Resource.Id.tripPayment_btnChangePrice);
            etCashPmt = FindViewById<EditText>(Resource.Id.tripPayment_etCashPmt);
            etCheckPmt = FindViewById<EditText>(Resource.Id.tripPayment_etCheckPmt);
            tvTicketValue = FindViewById<TextView>(Resource.Id.tripPayment_tvTicketValue);
            btnGetTicket = FindViewById<Button>(Resource.Id.tripPayment_btnGetTicket);
            btnChargeCC = FindViewById<Button>(Resource.Id.tripPayment_btnChargeCC);
            btnGetCustomer = FindViewById<Button>(Resource.Id.tripPayment_btnGetCustomer);
            btnGetDriver = FindViewById<Button>(Resource.Id.tripPayment_btnGetDriver);
            etMediNo = FindViewById<EditText>(Resource.Id.tripPayment_etMediNo);
            btnAcceptPayment = FindViewById<Button>(Resource.Id.tripPayment_btnAcceptPayment);
            btnCancelPayment = FindViewById<Button>(Resource.Id.tripPayment_btnCancelPayment);

            #endregion WireViewes

            #region Initialize Control Values

            //tvTripPayment_Price.Text = UIController.ActiveTrip.Price.ToString("c");
            etTripPrice.Text = UIController.ActiveTrip.Price.ToString();

            btnChangePrice.Click += (object sender, EventArgs e) => 
            {
                if ((sender as Button).Text == "Change Price")
                {
                    etTripPrice.Enabled = true;
                    btnChangePrice.Text = "Save Price";
                }
                else
                {
                    etTripPrice.Enabled = false;
                    btnChangePrice.Text = "Change Price";
                }
            };

            btnGetTicket.Click += (object sender, EventArgs e) =>
            {
                Intent ticketsIntent = new Intent(this, typeof(TicketPaymentActivity));
                ticketsIntent.PutExtra("PresetSelectedPosition", selectedTicketPosition);
                StartActivityForResult(ticketsIntent, 1);
            };

            btnChargeCC.Click += (object sender, EventArgs e) =>
            {

            };

            btnGetCustomer.Click += (object sender, EventArgs e) =>
            {

            };

            btnGetDriver.Click += (object sender, EventArgs e) =>
            {

            };

            btnAcceptPayment.Click += (object sender, EventArgs e) => 
            {
                decimal cash;
                decimal check;

                decimal.TryParse(etCashPmt.Text, out cash);
                decimal.TryParse(etCheckPmt.Text, out check);

                UIController.ActiveTrip.CashPaid = cash;
                UIController.ActiveTrip.CheckPaid = check;

                Finish();
            };

            btnCancelPayment.Click += (object sender, EventArgs e) =>
            {
                Finish();
            };

            #endregion Initialize Control Values
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode == Result.Ok)
            {
                if (requestCode == 1) //tickets
                {
                    selectedTicketPosition = int.Parse(data.GetStringExtra("TicketSelectedPosition"));
                    int selectedId = int.Parse(data.GetStringExtra("TicketSelectedId"));
                    decimal selectedValue = decimal.Parse(data.GetStringExtra("TicketSelectedValue"));

                    tvTicketValue.Text = selectedValue.ToString("c");
                }
            }
        }
    }
}

