﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOL
{
    public class DriverStatusLog : BO
    {

        int _driverStatusLogID;

        public int DriverStatusLogID
        {
            get { return _driverStatusLogID; }
            set { _driverStatusLogID = value; }
        }
        DriverStatusOptions _driverStatusType;

        public DriverStatusOptions DriverStatusType
        {
            get { return _driverStatusType; }
            set { _driverStatusType = value; }
        }

        DateTime _timeLogged;

        public DateTime TimeLogged
        {
            get { return _timeLogged; }
            set { _timeLogged = value; }
        }

        int _driverID;

        public int DriverID
        {
            get { return _driverID; }
            set { _driverID = value; }
        }

        Driver _unitDriver;

        public Driver UnitDriver
        {
            get { return _unitDriver; }
            set { _unitDriver = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }

    public class DriverLogTotal : BO
    {
        Driver _logedDriver;

        public Driver LogedDriver
        {
            get { return _logedDriver; }
            set { _logedDriver = value; }
        }

        DateTime _dateLoged;
        public DateTime DateLoged
        {
            get { return _dateLoged; }
            set { _dateLoged = value; }
        }
        TimeSpan _totalWorked;

        public TimeSpan TotalWorked
        {
            get { return _totalWorked; }
            set { _totalWorked = value; }
        }
        TimeSpan _totalWorkedWithBreaks;

        public TimeSpan TotalWorkedWithBreaks
        {
            get { return _totalWorkedWithBreaks; }
            set { _totalWorkedWithBreaks = value; }
        }
        int _qtySmallBreaks;

        public int QtySmallBreaks
        {
            get { return _qtySmallBreaks; }
            set { _qtySmallBreaks = value; }
        }
        TimeSpan _totalSmallBreaks;

        public TimeSpan TotalSmallBreaks
        {
            get { return _totalSmallBreaks; }
            set { _totalSmallBreaks = value; }
        }
        int _qtyLongBreaks;

        public int QtyLongBreaks
        {
            get { return _qtyLongBreaks; }
            set { _qtyLongBreaks = value; }
        }
        TimeSpan _totalLongBreaks;

        public TimeSpan TotalLongBreaks
        {
            get { return _totalLongBreaks; }
            set { _totalLongBreaks = value; }
        }


        decimal _totalAmountBooked;

        public decimal TotalAmountBooked
        {
            get { return _totalAmountBooked; }
            set { _totalAmountBooked = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }
    }
}
