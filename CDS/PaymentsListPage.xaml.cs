﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using BOL;

namespace CDS
{
    /// <summary>
    /// Interaction logic for PaymentsListPage.xaml
    /// </summary>
    public partial class PaymentsListPage : Page, IDisplayTextBlock
    {
        public PaymentsListPage()
        {
            InitializeComponent();
            FromDatePicker.SelectedDate = DateTime.Today;
            ToDatePicker.SelectedDate = DateTime.Today;
        }

        private Payment SelectedPayment
        {
            get
            {
                return this.PaymentsListDataGrid.SelectedItem as Payment;
            }
        }

        private void RadMenuItemExportToExcel_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            UIController.ExportTelerikGrid(PaymentsListDataGrid, UIController.ExportFormat.Excel);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = DAL.CustomerPaymentInfoDB.GetPayments(0, 0, FromDatePicker.SelectedDate, ToDatePicker.SelectedDate, true, withDriverInfo: true);
        }

        #region IDispayText
        public string HeaderText
        {
            get
            {
                return "Payments List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        private void PaymentsListDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedPayment == null || SelectedPayment.CustomerID == 0)
                return;

            try
            {
                Customer customer = DAL.CustomerDB.GetCustomers(CustomerFilterOptions.CustomerID, SelectedPayment.Customer.CustomerID.ToString(), DataRequestTypes.FullData, false).FirstOrDefault();
                UIController.LoadCustomerInfoPage(customer);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

    }
}
