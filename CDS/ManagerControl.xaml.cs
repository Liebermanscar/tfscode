﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ManagerControl.xaml
    /// </summary>
    public partial class ManagerControl : UserControl
    {
        public ManagerControl()
        {
            InitializeComponent();
        }

        private void TextBlockToalHoursDriver_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            try
            {
                UIController.LoadDirverHoursAndAmountToalsListPage();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                UIController.LoadProcessDailyStartupPage();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
    }
}
