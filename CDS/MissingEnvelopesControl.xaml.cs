﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for MissingEnvelopesControl.xaml
    /// </summary>
    public partial class MissingEnvelopesControl : UserControl
    {
        public MissingEnvelopesControl()
        {
            InitializeComponent();
            try
            {

                LoadData();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        void LoadData()
        {
            try
            {
                var Total = DAL.DailyStartupDB.GetTripsNotAssigned(0); //DAL.TripDB.GetTripsNotAssigned(0);

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TextBlock_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.LoadData();
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        /*Trip _noOftipsMissingEnvelopes;

        public Trip NoOftipsMissingEnvelopes
        {
            get { return _noOftipsMissingEnvelopes; }
            set { _noOftipsMissingEnvelopes = value; }
        }*/
    }
}
