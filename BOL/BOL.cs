﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections;


namespace BOL
{
    [DataContract]
    public abstract class BO : IDataErrorInfo, INotifyPropertyChanged, ICloneable, IEditableObject
    {
        public BO() { }

        bool _selected;

        [IgnoreDataMember]
        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; OnPropertyChanged("Selected"); }
        }

        public bool IsNew
        {
            get { return this.ObjectState == BOState.New; }
        }

        public bool IsNotnew
        {
            get { return !this.IsNew; }
        }

        [IgnoreDataMember]
        public abstract bool AllowSave { get; }

        [IgnoreDataMember]
        public ObservableCollection<DataModificationLog> ModificationLog { get; set; }

        [IgnoreDataMember]
        public string Comment { get; set; }

        [IgnoreDataMember]
        public bool IsActive { get; set; }

        #region IDataErrorInfo Members


        public string Error
        {
            //get { throw new NotImplementedException(); }
            get { return ""; }
        }

        public string this[string columnName]
        {
            //get { throw new NotImplementedException(); }
            get { return ""; }

        }

        #endregion

        #region INotifyPropertyChanged Members


        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        BOState objectState;

        [IgnoreDataMember]
        public BOState ObjectState
        {
            get { return objectState; }
            set
            {
                objectState = value; OnPropertyChanged("ObjectState");
                this.OnPropertyChanged("IsNew");
                this.OnPropertyChanged("IsNotNew");
            }
        }
        public BO(BOState boState)
        {
            ObjectState = boState;
        }
        public virtual void CopyFrom(BO BObject) { throw new NotImplementedException(); }



        #region ICloneable Members

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        public virtual string DisplayInformation
        {
            get { return this.ToString(); }
        }

        #region IEditableObject Members

        System.Collections.Hashtable props = null;

        public void BeginEdit()
        {
            if (null != props) return;


            //enumerate properties

            PropertyInfo[] properties = (this.GetType()).GetProperties
                        (BindingFlags.Public | BindingFlags.Instance);

            props = new Hashtable(properties.Length - 1);

            for (int i = 0; i < properties.Length; i++)
            {
                //check if there is set accessor

                if (null != properties[i].GetSetMethod())
                {
                    object value = properties[i].GetValue(this, null);
                    props.Add(properties[i].Name, value);
                }
            }
        }

        public void CancelEdit()
        {
            if (null == props) return;

            //restore old values

            PropertyInfo[] properties = (this.GetType()).GetProperties
                (BindingFlags.Public | BindingFlags.Instance);
            for (int i = 0; i < properties.Length; i++)
            {
                //check if there is set accessor

                if (null != properties[i].GetSetMethod())
                {

                    object value = props[properties[i].Name];

                    properties[i].SetValue(this, value, null);
                }
            }

            //delete current values

            props = null;
        }

        public void EndEdit()
        {
            props = null;
        }

        #endregion
    }

    public static class Extand
    {
        public static string ToDescription(this Enum en) //ext method
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes
                                              (
                                                typeof(DescriptionAttribute),
                                                false
                                              );

                if (attrs != null && attrs.Length > 0)

                    return ((DescriptionAttribute)attrs[0]).Description;
                else
                    return en.ToString();

            }

            return en.ToString();

        }
    }
    [DataContract]
    public class UserSession : BO
    {
        public int SessionID { get; set; }
        User _user;

        [DataMember]
        public User SessionUser
        {
            get { return _user; }
            set { _user = value; this.OnPropertyChanged("SessionUser"); }
        }

        DateTime _overrideDate;

        public DateTime OverrideDate
        {
            get { return _overrideDate; }
            set { _overrideDate = value; }
        }

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }




        public override bool AllowSave
        {
            get
            {
                throw new NotImplementedException();
            }


        }
    }


    public enum BOState
    {
        New = 1,
        Edit,
        Saved
    }

    public enum DataRequestTypes
    {
        FullData = 1,
        LiteData = 2
    }


    public enum ModificationTypes
    {
        Add = 1,
        Edit = 2,
        Delete = 3
    }

    public enum InformationTypes
    {
        ContactName = 1,
        Address = 2,
        PhoneNo = 3,
        Obligation = 4,
        Payment = 5,
        Relationship = 6,
        Correspondence = 7,
        User = 8,
        Student = 9,
        StudentRegistration = 10,
        Contract = 11
    }

    public class DataModificationLog
    {
        int _dataModificationlogID;

        public int DataModificationlogID
        {
            get { return _dataModificationlogID; }
            set { _dataModificationlogID = value; }
        }
        UserSession _userSession;

        public UserSession UserSession
        {
            get { return _userSession; }
            set { _userSession = value; }
        }
        DateTime _modifiedOn;

        public DateTime ModifiedOn
        {
            get { return _modifiedOn; }
            set { _modifiedOn = value; }
        }
        ModificationTypes _modificationType;

        public ModificationTypes ModificationType
        {
            get { return _modificationType; }
            set { _modificationType = value; }
        }
        int _referenceID;

        public int ReferenceID
        {
            get { return _referenceID; }
            set { _referenceID = value; }
        }
        string _oldData;

        public string OldData
        {
            get { return _oldData; }
            set { _oldData = value; }
        }
        string _newData;

        public string NewData
        {
            get { return _newData; }
            set { _newData = value; }
        }
    }

    public static class Extend
    {
        // Extend ObservableCollection<T> Class
        public static void AddRange<T>(this ObservableCollection<T> observableCollection, IEnumerable<T> rangeList)
        {
            foreach (T item in rangeList)
            {
                observableCollection.Add(item);
            }
        }
    }

}
