﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BOL_OTR
{
    public class DriverInfo
    {
        public int DriverID { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string CommissionType { get; set; }
        public double PecentageGasDriverPays { get; set; }
        public string VehicleNo { get; set; }
        public bool IsCompanyEZPass { get; set; }
        public string EZPassTagNumber { get; set; }
    }

    /// <summary>
    /// Drivers daily total - Calculates his doing from a full day
    /// </summary>
    public class DriverDailyTotal
    {
        public DateTime DateOfTotal { get; set; }

        public int ForDriverID { get; set; }
        /// <summary>
        /// Cash he got
        /// </summary>
        public decimal Cash { get; set; }
        /// <summary>
        /// CC he got
        /// </summary>
        public decimal Credit_Card { get; set; }
        /// <summary>
        /// Got written on customers account
        /// </summary>
        public decimal On_Account { get; set; }
        /// <summary>
        /// paid with Tickets
        /// </summary>
        public decimal Tickets { get; set; }
        /// <summary>
        /// NOT PAID AT ALL
        /// </summary>
        public decimal Not_Paid { get; set; }
        /// <summary>
        /// Full income that the driver rceived for trips...(Cash, Checks, Tickets collected (not sold), On Account, Not Paid)
        /// </summary>
        public decimal Amount_Booked { get; set; }
        /// <summary>
        /// Drivers expenses
        /// </summary>
        public decimal Expenses { get; set; }
        /// <summary>
        /// Ticket Books Sold
        /// </summary>
        public decimal Books_Sold { get; set; }

        public decimal Cash_for_office_from_trips { get; set; }

        public decimal Cash_for_office_from_ticket_books_sold { get; set; }

        public int Tickets_for_office { get; set; }

        public decimal Tools_to_pay_back_for_office { get; set; }
    }

    public enum DriverStatusOptions
    {
        None = 0,
        NintyEight = 1,
        ShortBreak = 2,
        LongTrip = 202,
        Assigned = 3,
        LongBreak = 4,
        SignedOut = 5
    }

    [DataContract]
    public enum TripDetailedStatusOptions
    {
		[EnumMember]
		None = 0,
        RequestingToAcceptFromDriver = 95,
		[EnumMember]
		GoingToPickupLocation = 99,
        [EnumMember]
        ArrivedAtPickupLocation = 101,
        [EnumMember]
        WaitingForCustomer = 201,
        [EnumMember]
        GoingToStop = 301,
        [EnumMember]
        ArrivedToStop = 401,
        [EnumMember]
        GoingToDestination = 501, 
        [EnumMember]
        HeadingFinalDestination = 502,       
        [EnumMember]
        ArrivedAtDestination = 601,
        [EnumMember]
        TripPayment = 701,
		[EnumMember]
		TripCompleted = 801
        
    }
}
