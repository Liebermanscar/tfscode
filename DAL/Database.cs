﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{

    public class Database : IDisposable
    {
        public static int SessionID { get; set; }
        //private static string _connectionString;


        public static string ConnectionString = "Data Source=(local);Initial Catalog=CDS;Integrated Security=False;Persist Security Info=True;User ID=dev;Password=dev;MultipleActiveResultSets=True";
        
        //  public static string ConnectionString { get; set; }
        //{
        //    get
        //    {
        //        if (_connectionString == null)
        //            _connectionString = Properties.Settings.Default.FLPConnetionString;
        //        return _connectionString;
        //    }
        //}

        public Database(SqlConnection conn)
        {

            if (conn != null)
            {
                _dbConnection = conn;
                _connOpenedLocaly = false;
            }
            else
            {
                _dbConnection = new SqlConnection(ConnectionString);
                _dbConnection.Open();
                _connOpenedLocaly = true;
            }
        }

        public Database(SqlConnection conn, SqlTransaction trans)
            : this(conn)
        {

            this.Transaction = trans;
        }
        //string connStr = Properties.Settings.Default.FLPConnetionString;
        private SqlConnection _dbConnection;

        public SqlConnection Connection
        {
            get { return _dbConnection; }
            set { _dbConnection = value; }
        }

        bool _connOpenedLocaly;

        public bool ConnOpenedLocaly
        {
            get { return _connOpenedLocaly; }

        }



        public SqlTransaction Transaction { get; set; }

        public object ExecuteScalar(string SQL, SqlParameter[] parameters, CommandType commandType)
        {
            try
            {

                SqlCommand cmd = new SqlCommand(SQL, _dbConnection);
                cmd.CommandType = commandType;
                if (Transaction != null)
                    cmd.Transaction = this.Transaction;
                if (parameters != null)
                    cmd.Parameters.AddRange(parameters);
                return cmd.ExecuteScalar();

            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                return null;
            }
        }

        public SqlDataReader GetReader(string SQL, SqlParameter[] parameters, CommandType commandType)
        {
            SqlCommand cmd = new SqlCommand(SQL, _dbConnection, this.Transaction);

            cmd.CommandType = commandType;
            if (parameters != null)
                cmd.Parameters.AddRange(parameters);
            return cmd.ExecuteReader();
        }

        public int ExecuteNonQuery(string SQL, SqlParameter[] parameters, CommandType commandType, out string serverMessage)
        {
            string serverMessageLocal = "";
            int rowsAffected = 0;
            SqlCommand cmd = new SqlCommand(SQL, _dbConnection);
            cmd.CommandType = commandType;
            if (Transaction != null)
                cmd.Transaction = this.Transaction;
            if (parameters != null)
                cmd.Parameters.AddRange(parameters);

            this.Connection.InfoMessage += delegate(object sender, SqlInfoMessageEventArgs e)
            {
                serverMessageLocal = e.Message;
            };

            rowsAffected = cmd.ExecuteNonQuery();
            serverMessage = serverMessageLocal;
            return rowsAffected;
        }

        public bool TryConnection()
        {
            try
            {

                this.ExecuteScalar("", null, CommandType.Text);

                return true;
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                return false;
            }
        }

        public bool BackupDatabase(string fileName, out string serverMessage)
        {
            try
            {
                return this.ExecuteNonQuery("ProcessBackup", new SqlParameter[]
                {
                    new SqlParameter("@FileName", fileName),
                    new SqlParameter("@DatabaseName", this.Connection.Database)
                }, CommandType.StoredProcedure, out serverMessage) > 0;
                //ServerConnection srvConn = new ServerConnection(this.Connection.DataSource);
                //Server srvSql = new Server(srvConn);

                //if (srvSql != null)
                //{
                //    // Create a new backup operation
                //    Backup bkpDatabase = new Backup();

                //    // Set the backup type to a database backup
                //    bkpDatabase.Action = BackupActionType.Database;

                //    // Set the database that we want to perform a backup on
                //    bkpDatabase.Database = this.Connection.Database;

                //    // Set the backup device to a file
                //    BackupDeviceItem bkpDevice = new BackupDeviceItem(fileName, DeviceType.File);

                //    // Add the backup device to the backup
                //    bkpDatabase.Devices.Add(bkpDevice);

                //    // Perform the backup
                //    bkpDatabase.SqlBackup(srvSql);

                //    return true;
                //}
                //else
                //    return false;


            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }

        }


        #region IDisposable Members

        public void Dispose()
        {

            if (_connOpenedLocaly)
            {
                _dbConnection.Close();
                _dbConnection.Dispose();
            }
        }

        #endregion

        public bool _localTransaction;
    }


    public static class DataRecordExtensions
    {
        public static bool HasColumn(this SqlDataReader dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

    }
    
}
