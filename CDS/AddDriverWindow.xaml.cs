﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for AddDriverWindow.xaml
    /// </summary>
    public partial class AddDriverWindow : Window
    {
        public AddDriverWindow()
        {
            InitializeComponent();

            ObservableCollection<CommisionType> commisionTypeList = this.Resources["CommisionTypeListData"] as ObservableCollection<CommisionType>;
            commisionTypeList.AddRange(DAL.CommisionTypesDB.GetCommisionTypes());

            ObservableCollection<Vehicle> vehiclesList = this.Resources["VehiclesListData"] as ObservableCollection<Vehicle>;
            vehiclesList.AddRange(DAL.VehicleDB.GetVehicles());

            ObservableCollection<Driver> driversList = this.Resources["DriversListDate"] as ObservableCollection<Driver>;
            driversList.AddRange(DAL.DriversDB.GetDrivers(false, 0, true, true));
            //this.ChargeToDriverComboBox.ItemsSource = DAL.DriversDB.GetDrivers(false, 0, false, true);


        }

        public AddDriverWindow(Driver driver) : this()
        {
            this.ActiveDriver = driver;
        }

        public Driver ActiveDriver
        {
            get { return this.DataContext as Driver; }
            set { this.DataContext = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ActiveDriver.LinkedDriver != null && ActiveDriver.LinkedDriver.IsMain)
            {
                MessageBox.Show("The linked driver is a main driver, You cant link main to main", "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                this.DialogResult = false;
                return;
            }
            this.DialogResult = true;
        }


    }


    public class VehiclesList : ObservableCollection<Vehicle> { }
    public class CommisionTypeList : ObservableCollection<CommisionType> { }
    public class DriversList : ObservableCollection<Driver> { }
}
