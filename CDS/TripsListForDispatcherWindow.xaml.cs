﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for TripsListForDispatcherWindow.xaml
    /// </summary>
    public partial class TripsListForDispatcherWindow : Window
    {
        public TripsListForDispatcherWindow()
        {
            InitializeComponent();

            this.dPickerFrom.SelectedDate = DateTime.Today;
            this.dPickerTo.SelectedDate = DateTime.Today;

            GetTrips();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(this.SearchTextBox.Text))
                this.TripsDataGrid.DataContext = this.FullListOfTrips;
            else
            {
                this.TripsList = new ObservableCollection<Trip>(
                    this.FullListOfTrips.Where(
                    ft =>
                        ft.FromLocation.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                        ||
                        ft.ToLocation.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                    //  ||
                    // ft.DrivenBy.DriverCode.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())
                    // ft.DrivenBy ?null :ft.DrivenBy.DriverCode.ToUpper().Contains(this.SearchTextBox.Text.ToUpper())

                          ).ToList()

                          );
                this.TripsDataGrid.DataContext = this.TripsList;

            }
        }

        void GetTrips()
        {
            try
            {
                Trips = DAL.TripDB.GetTrips(0, 0, 0, 0, this.dPickerFrom.SelectedDate.Value, this.dPickerTo.SelectedDate.Value, false, null, 0, 0, false, 0, false, 0, 0);



                this.TripsList = Trips;
                this.FullListOfTrips = TripsList;

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }


        }
        public ObservableCollection<Trip> Trips
        {
            get
            {
                return this.TripsDataGrid.DataContext as ObservableCollection<Trip>;
            }
            set
            {
                this.TripsDataGrid.DataContext = value;
            }
        }


        public Trip SelectedTrip
        {
            get { return this.TripsDataGrid.SelectedItem as Trip; }
            set { this.TripsDataGrid.SelectedItem = value; }
        }


        public ObservableCollection<Trip> FullListOfTrips = new ObservableCollection<Trip>();
        public ObservableCollection<Trip> TripsList
        {
            get { return this.DataContext as ObservableCollection<Trip>; }
            set { this.DataContext = value; }

        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                return;
            switch (e.Key)
            {
                case Key.F1:
                    try
                    {


                        if (this.SelectedTrip == null) { return; }
                        if (this.SelectedTrip.AssignedToDailyEnvelopeID > 0)
                        {
                            System.Windows.MessageBox.Show("Could not change driver this trip daily Enevelope has been processed");
                            return;
                        }
                        e.Handled = true;
                        string s;



                        Driver assignDriverToTrip = UIController.SelectDriver(DAL.DriversDB.GetDrivers(false), "Assign to Trip", out s);

                        if (assignDriverToTrip == null)
                            return;


                        this.SelectedTrip.DrivenBy = assignDriverToTrip;
                        if (!DAL.TripDB.EditTrip(this.SelectedTrip)) { MessageBox.Show("Error changing driver"); }
                    }
                    catch (Exception ex)
                    {

                        UIController.DisplayErrorMessage(ex);
                    }
                    e.Handled = true;
                    break;



                case Key.F6:
                    try
                    {


                        if (this.SelectedTrip.AssignedToDailyEnvelopeID > 0)
                        {
                            System.Windows.MessageBox.Show("Could not change driver this trip daily Enevelope has been processed");
                            return;
                        }
                        AssignTripToMedicaid();
                    }
                    catch (Exception ex)
                    {

                        UIController.DisplayErrorMessage(ex);
                    }
                    e.Handled = true;
                    break;
                case Key.F9:

                    try
                    {
                        bool creditPmt = this.SelectedTrip.ChargeToCustomer == null ? false : this.SelectedTrip.ChargeToCustomer.IsInvoiceCustomer;
                        int cstmerID = this.SelectedTrip.ChargeToCustomer == null ? 0 : this.SelectedTrip.ChargeToCustomer.CustomerID;
                        int pmtID = 0;
                        decimal chargeAmount = 0;
                        decimal amoutnForTrip = 0;

                        //UIController.ShowChargeCCWindow(new CCOnFile() { CustomerID = cstmerID }, creditPmt, ref pmtID);
                        bool charged = UIController.ShowChargeCCWindow(new CCOnFile() { CustomerID = cstmerID }, false, ref pmtID, ref chargeAmount, ref amoutnForTrip); //requested to be always default non creditable payment for dispatcher
                        if (charged)
                        {
                            this.SelectedTrip.PaymentID = pmtID;
                            this.SelectedTrip.CCPaid = amoutnForTrip;
                            this.SelectedTrip.ToLocationComment = string.Format("CC Charged for {0:C2}", chargeAmount.ToString("C2"));
                            DAL.TripDB.EditTrip(this.SelectedTrip); //to save pmt id
                        }

                    }
                    catch (Exception) { }
                    break;
            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.GetTrips();
        }

        private void TripsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            DAL.TripDB.EditTrip(this.SelectedTrip);
        }

        //  ObservableCollection<Trip> _trips;
        private void AssignTripToMedicaid()
        {
            try
            {


                // this.NewTrip.IsMedicaidTrip = true;
                ScheduledTrip scheduledTripSelected = UIController.SelectScheduledTrips(this.SelectedTrip.TripStarted.Value, TripAreaTypes.All);
                if (scheduledTripSelected != null)
                {

                    /*if (this.AddEditTripBorder.IsFocused || this.IsObjectFocused(true, this.AddEditTripBorder))
                                               {
                                                   int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                                                   int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                                                   if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.FromLocationStreetName))
                                                   {
                                                       this.Dispatching.AddedEditedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                                                       this.Dispatching.AddedEditedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                                                   }
                                                   if (string.IsNullOrWhiteSpace(this.Dispatching.AddedEditedTrip.ToLocationStreetName))
                                                   {
                                                       this.Dispatching.AddedEditedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                                                       this.Dispatching.AddedEditedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                                                   }
                                                   this.Dispatching.AddedEditedTrip.AttachedScheduledTrip = scheduledTripSelected;
                                               }
                                               else*/
                    if (this.SelectedTrip != null)
                    {
                        int fromStreetNameIndex = scheduledTripSelected.PickupAddress.IndexOf(' ');
                        int toStreetNameIndex = scheduledTripSelected.DropOffAddress.IndexOf(' ');
                        if (string.IsNullOrWhiteSpace(this.SelectedTrip.FromLocationStreetName))
                        {
                            this.SelectedTrip.FromLocationHouseNo = scheduledTripSelected.PickupAddress.Substring(0, fromStreetNameIndex + 1);
                            this.SelectedTrip.FromLocationStreetName = scheduledTripSelected.PickupAddress.Substring(fromStreetNameIndex + 1);
                        }
                        if (string.IsNullOrWhiteSpace(this.SelectedTrip.ToLocationStreetName))
                        {
                            this.SelectedTrip.ToLocationHouseNo = scheduledTripSelected.DropOffAddress.Substring(0, toStreetNameIndex + 1);
                            this.SelectedTrip.ToLocationStreetName = scheduledTripSelected.DropOffAddress.Substring(toStreetNameIndex + 1);
                        }

                        this.SelectedTrip.AttachedScheduledTrip = scheduledTripSelected;
                        this.SelectedTrip.ChargeToCustomer = scheduledTripSelected.ScheduledByCustomer;

                        if (!DAL.TripDB.EditTrip(this.SelectedTrip)) { MessageBox.Show("Error changing medicaid trip"); }

                    }
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void MenuItemRemoveMedicaidTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedTrip == null) { return; }
                if (this.SelectedTrip.AssignedToDailyEnvelopeID > 0)
                {
                    System.Windows.MessageBox.Show("Could not change driver this trip daily Enevelope has been processed");
                    return;
                }

                this.SelectedTrip.IsMedicaidTrip = false;
                this.SelectedTrip.ChargeToCustomerID = 0;
                this.SelectedTrip.ChargeToCustomer = null;
                this.SelectedTrip.AttachedScheduledTrip = null;
                if (!DAL.TripDB.EditTrip(this.SelectedTrip)) { MessageBox.Show("Error saving"); }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void menueItemRemoveFromCustomer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedTrip == null) { return; }
                if (this.SelectedTrip.AssignedToDailyEnvelopeID > 0)
                {
                    System.Windows.MessageBox.Show("Could not change driver this trip daily Enevelope has been processed");
                    return;
                }


                this.SelectedTrip.ChargeToCustomerID = 0;
                this.SelectedTrip.ChargeToCustomer = null;
                // this.SelectedTrip.AttachedScheduledTrip = null;
                if (!DAL.TripDB.EditTrip(this.SelectedTrip)) { MessageBox.Show("Error saving"); }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }
    }
}
