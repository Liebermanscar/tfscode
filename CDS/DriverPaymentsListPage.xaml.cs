﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for DriverPaymentsListPage.xaml
    /// </summary>
    public partial class DriverPaymentsListPage : Page, IDisplayTextBlock
    {
        public DriverPaymentsListPage()
        {
            InitializeComponent();


        }
        /* public DriverPaymentsListPage(ObservableCollection<DriverPaymentsListPage>{} this();
         {

         }*/



        #region
        public string HeaderText
        {
            get
            {
                return "Driver Payment List";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion


        void LoadData()
        {
            try
            {
                DateTime? paymentDate = null;
                if (this.FromDatePicker.SelectedDate.HasValue)
                    paymentDate = this.FromDatePicker.SelectedDate.Value;

                this.driverPayments = DAL.DriverPaymentDB.GetDriverPayment(0, 0, paymentDate, DateTime.Now);
                DriverPaymetnsDataGrid.ItemsSource = this.driverPayments;
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void buttonGetReport_Click(object sender, RoutedEventArgs e)
        {
            this.LoadData();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt1 = new DateTime();
                if (FromDatePicker.SelectedDate != null)
                {
                    dt1 = FromDatePicker.SelectedDate.Value.Date;
                    Trip trip = new Trip();

                    //  MedicaidTripsByDateDataGrid.DataContext = DAL.TripDB.GetTrips(0, 0, 0, 0, dt1, dt1, true, dt1,0);
                }
                else MessageBox.Show("Select date");
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void ButtonShowReport_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ObservableCollection<DriverPayment> S = new ObservableCollection<DriverPayment>(driverPayments.Where(dp => dp.Selected).ToList());
                ObservableCollection<DriverPayment> R = new ObservableCollection<DriverPayment>();
                foreach (var results in S)
                {
                    var drvrPmt = DAL.DriverPaymentDB.GetDriverPayment(results.DriverPaymentID, 0, DateTime.Now, DateTime.Now)[0];
                    drvrPmt.DriverExpenses = DAL.DriverExpenseDB.GetDriverExpense(0, 0, false, results.DriverPaymentID);
                    drvrPmt.ChargeToDriverTrips = DAL.TripDB.GetTrips(0, 0, 0, 0, null, null, true, null, 0, results.DriverPaymentID);
                    drvrPmt.EnvelopesOpen = DAL.DailyEnvelopeDB.GetDailyEnvelopes(0, 0, null, false, results.DriverPaymentID);

                    foreach (var eo in drvrPmt.EnvelopesOpen.Where(en => en.NoteForDriver != null && en.NoteForDriver != string.Empty))
                    {
                        drvrPmt.DriverNotes += eo.DateOfTrips.Value.ToShortDateString().PadRight(13) + " Note: " + eo.NoteForDriver + "\n";
                    }

                    R.Add(drvrPmt);
                }
                UIController.LoadDriverPaymentStub(R);

            }

            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }


        public ObservableCollection<DriverPayment> driverPayments { get; set; }

        private void MenuItemSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var P = this.DriverPaymetnsDataGrid.Items.Cast<DriverPayment>();

                foreach (var pmt in P)
                {
                    if (sender == this.SelectAllMenuItem)
                    {
                        pmt.Selected = true;

                    }
                    else
                        pmt.Selected = false;
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void MenuItemUnSelectAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var P = this.DriverPaymetnsDataGrid.Items.Cast<DriverPayment>();

                foreach (var pmt in P)
                {
                    if (sender == this.UnSelectAllMenuItem)
                    {
                        pmt.Selected = false;

                    }
                    else
                        pmt.Selected = true;
                }

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void mnuDeletePayment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DriverPayment pmt = this.DriverPaymetnsDataGrid.SelectedItem as DriverPayment;

                if (pmt == null)
                    return;

                if (pmt.BonusPaidWithID > 0)
                {
                    MessageBox.Show("Cant delete payment with bonus paid", "Error", MessageBoxButton.OK);
                    return;
                }

                if (MessageBox.Show("Are You sure You want to delete this payment?", "Confirm", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    string pmtID;
                    int paymentID;
                    InputBox.GetInput("Payment ID", "Please enter the payment ID You want to delete", out pmtID);

                    if (!int.TryParse(pmtID, out paymentID))
                        return;

                    if (paymentID != pmt.DriverPaymentID)
                        return;

                    if (DAL.DriverPaymentDB.DeleteDriverPayment(pmt))
                        this.driverPayments.Remove(pmt);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void DriverPaymetnsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                DriverPayment driverPayment = this.DriverPaymetnsDataGrid.SelectedItem as DriverPayment;
                if (driverPayment == null)
                    return;
                if (driverPayment.BonusPaidWithID > 0)
                {
                    return;
                }
                DAL.DriverPaymentDB.EditDriverPayment(driverPayment, false);
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void mnuCancelBonus_Click(object sender, RoutedEventArgs e)
        {
            DriverPayment driverPayment = this.DriverPaymetnsDataGrid.SelectedItem as DriverPayment;
            if (driverPayment == null)
                return;

            if (MessageBox.Show("Are You sure You want to cancel the bonus from this payment?", "Cancel?", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            if (DAL.DriverPaymentDB.EditDriverPayment(driverPayment, true))
            {
                driverPayment.BonusPayment = 0;

                foreach (DriverPayment pmt in this.driverPayments.Where(p => p.BonusPaidWithID == driverPayment.DriverPaymentID))
                {
                    pmt.BonusPaidWithID = 0;
                }
            }
        }

    }
}
