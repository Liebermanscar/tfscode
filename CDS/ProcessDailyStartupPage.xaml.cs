﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ProcessDriverStartupPage.xaml
    /// </summary>
    public partial class ProcessDailyStartupPage : Page, IDisplayTextBlock
    {
        public ProcessDailyStartupPage()
        {


            InitializeComponent();
            try
            {
                this.ProcessNewDailyStartupPage();
                this.driversComboBox.ItemsSource = DAL.DriversDB.GetDrivers(false);

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }



        void ProcessNewDailyStartupPage()
        {
            try
            {
                this.ActiveDailyStartup = new DailyStartup()
                {
                    ObjectState = BOState.New
                };
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }



        public DailyStartup ActiveDailyStartup
        {
            get { return this.DataContext as DailyStartup; }
            set { this.DataContext = value; }
        }




        private void driversComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (this.driversComboBox.SelectedValue == null)
                    return;
                this.ActiveDailyStartup.CountOfBooksOnHandOfDriver = DAL.TicketBookStatusDB.GetTotalOfTicektBooksOnHand(this.ActiveDailyStartup.ForDriverID);
                this.ActiveDailyStartup.CountOfOutstandingEnvelopes = DAL.DailyStartupDB.GetTripsNotAssigned(this.ActiveDailyStartup.ForDriverID);

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void dPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        #region IDisplayTextBlockMembers
        public string HeaderText
        {
            get
            {
                return "Process Daily Startup Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;

            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        public bool SaveData()
        {
            try
            {
                bool ticektBookStatusSavingAllowed = true;

                //first check if any  sequence of books is more then 15 books, pop up box 'Are you sure you want to transfer more then 15 books?'
                if (this.ActiveDailyStartup.StatusChangedForTicketBooksGivenToDriver.Where(ts => ts.CountOfBooks > 15).Count() > 0)
                {



                    string messageString = "You are trying to transfer more then 15 books of one type \n" +
                  " Are you sure you want to continue!!!";
                    if (MessageBox.Show(messageString, "Are You Sure?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        return false;
                    }
                }


                //Check If could give this user has this ticket books if not dont allow to save
                foreach (var ticektBookStatus in this.ActiveDailyStartup.StatusChangedForTicketBooksGivenToDriver)
                {

                    var startingBookNumber = ticektBookStatus.StartBookNumber;
                    var endingBookNumber = ticektBookStatus.EndBookNumber;
                    var forBookType = ticektBookStatus.TicketBookID;
                    var byUserID = UIController.CurrentSession.SessionUser.UserID;

                    if (!DAL.TicketBookStatusDB.CheckIfCouldGiveAwayTickets(startingBookNumber, endingBookNumber, forBookType, byUserID))
                    {
                        ticektBookStatusSavingAllowed = false;
                        MessageBox.Show("You Don't Have This Ticket books in your possession  \n" +
                      " first take this books into your possession!!!", "Cant Save", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return false;
                    }

                }
                if ((this.ActiveDailyStartup.AllowSave) && (ticektBookStatusSavingAllowed))
                    if (this.ActiveDailyStartup.DailyStartupID > 0)
                    {
                        if (DAL.DailyStartupDB.EditDailyStartup(this.ActiveDailyStartup))
                            return true;

                    }
                    else
                        if (DAL.DailyStartupDB.AddDailyStartup(this.ActiveDailyStartup))
                        return true;



                return false;


            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
                return false;
            }
        }

        private void SaveDailyStartupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SaveData())
                {
                    this.ActiveDailyStartup = new DailyStartup();
                    this.driversComboBox.SelectedValue = 0;
                }
            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }



        private void TextBlockCountOfBooksObHand_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (this.ActiveDailyStartup == null)
                    return;
                var ticketBookStatuses = DAL.TicketBookStatusDB.GetTicketBookStatuses(this.ActiveDailyStartup.ForDriverID);

                UIController.LoadTicketBooksOnHandWindow(ticketBookStatuses);

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        private void TextBlockCountOFOutstandingEnvelops_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (this.ActiveDailyStartup == null)
                    return;
                var dates = DAL.DailyStartupDB.GetDatesOfMissingEnvelopes(this.ActiveDailyStartup.ForDriverID);
                UIController.LoadMissingEnvelopesWindow(dates);

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }



        private void dgChangeTicketBookStatuses_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                e.Cancel = true;
                if (this.driversComboBox.SelectedValue == null) return;
                var ticketBookStatus = e.Row.DataContext as GenerateTicketBookStatuses;

                if ((ticketBookStatus == null) || (ticketBookStatus.EndBookNumber == 0) || (ticketBookStatus.EndBookNumber < ticketBookStatus.StartBookNumber))
                    return;



                ticketBookStatus.BookStatus = TicketBookStatusOptions.Driver;
                ticketBookStatus.InPossessionID = (int)this.driversComboBox.SelectedValue;
                ticketBookStatus.GenarateBooks();
                e.Cancel = false;

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DataGrid_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try
            {
                var ticketBooksForThisUser = new ObservableCollection<TicketBookStatus>();

                var gtbs = e.Row.DataContext as GenerateTicketBookStatuses;
                if (gtbs == null)
                    return;

                if (e.Column.Header.ToString() == "Book Type")
                    gtbs.StartBookNumber = DAL.TicketBookStatusDB.GetMaxBookNumber(gtbs.TicketBookID, UIController.CurrentSession.SessionUser.UserID);

                if (e.Column.Header.ToString() == "")

                    // var o = ticketBooksForThisUser.Contains(gtbs.bo
                    //create function in sql pass in the user id from this session and from and till ticket number and book type return if it could save it


                    return;


            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }

        }

        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Insert)
                    if (this.SaveData())
                        this.ActiveDailyStartup = new DailyStartup();

            }
            catch (Exception ex)
            {

                UIController.DisplayErrorMessage(ex);
            }
        }


    }
}
