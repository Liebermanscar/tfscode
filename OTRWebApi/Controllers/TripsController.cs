﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTRWebApi.Controllers
{
    public class TripsController : ApiController
    {
        /// <summary>
        /// Get available trips in a section 
        /// </summary>
        /// <returns></returns>
        /// 
        //public BOL_OTR.Trip_OTR GetAvailTripsFromSection(int sectionID)
        //{
        //     DAL_EF.DAL.TripSectionsDb.GetAvailTripsInSection(sectionID);
        //}

        // GET: api/Trips
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        public void ApplyPaymentToTrip(int tripID, BOL_OTR.PaymentMethodTypes pmtType, decimal pmtAmt, int chargeToDriverID = 0,
                int chargeToCustomerID = 0)
        {
            DAL_EF.DAL.TripDB.ApplyPaymentToTrip(tripID, pmtType, pmtAmt, chargeToDriverID, chargeToCustomerID);
        }

        // GET: api/Trips/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Trips
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Trips/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Trips/5
        public void Delete(int id)
        {
        }
    }
}
