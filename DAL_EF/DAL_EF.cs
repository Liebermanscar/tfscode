﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.Data.Entity;

namespace DAL_EF
{
    public static class DAL_OTR
    {
        public static void HandleException(Exception ex)
        {
            if (!(ex is DataException))
                return;

            DataException de = (ex as DataException);

            new Task(() => ExceptionDb.AddException(ex)).Start();


            //TODO: handle the exception


        }

        public static class DriverDb
        {
            public static bool? AuthenticateDriver(string loginUserName, string loginPassword)
            {
                using (var db = new CDS_OTREntities())
                {
                    var Exist = (db.UnitDrivers.Where(d => d.LoginUserName == loginUserName && PassHashing.ValidateSHA1HashData(loginPassword, d.LoginPassword.ToString())).Count() == 1);

                    return Exist;
                }
            }

            /// <summary>
            /// Gets the signalr connection id of the tablet
            /// </summary>
            /// <param name="driverID"></param>
            /// <returns></returns>
            public static string GetDriversTabletConnectionID(int driverID)
            {
                using (var db = new CDS_OTREntities())
                {
                    var connectionID = (from unitDrivers in db.UnitDrivers
                                        join vehicle in db.UnitVehicles
                                           on unitDrivers.LoggedOnToTabletID equals vehicle.TabletID
                                        join tablet in db.Tablets
                                            on vehicle.TabletID equals tablet.Id
                                        where unitDrivers.Id == driverID
                                        select tablet.ConnectionID).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(connectionID))
                        return connectionID;
                    else
                        return "";
                }
            }

            public static int GetDriversIDFromConnectionID(string connectionID)
            {
                using (var db = new CDS_OTREntities())
                {
                    var tblt = db.Tablets.Where(t => t.ConnectionID == connectionID).FirstOrDefault();
                    if (tblt != null)
                    {
                        var drvr = db.UnitDrivers.Where(d => d.LoggedOnToTabletID == tblt.Id).FirstOrDefault();
                        return drvr == null ? 0 : drvr.Id;
                    }
                    return 0;
                }
            }

            /// <summary>
            /// Method the app call when the driver logs into the App
            /// </summary>
            /// <param name="loginUserName"></param>
            /// <param name="loginPassword"></param>
            /// <param name="onTabletName"></param>
            /// <param name="message"></param>
            /// <param name="exception"></param>
            /// <returns></returns>
            public static DbSaveResult DriverLogIn(string loginUserName, string loginPassword, string onTabletName, ref string message, ref int driverID, Exception exception = null)
            {
                using (var db = new CDS_OTREntities())
                {
                    try
                    {
                        var driverPass = db.UnitDrivers.Where(d => d.LoginUserName == loginUserName).FirstOrDefault();

                        if (driverPass != null && loginPassword == driverPass.LoginPassword)
                        {
                            var tablet = db.Tablets.Where(t => t.TabletName == onTabletName).FirstOrDefault();

                            var driver = db.UnitDrivers.Where(d => d.LoginUserName == loginUserName).FirstOrDefault();

                            if (driver.IsLoggedOn)
                            {
                                string message2 = "";
                                DriverLogOut(driver.Id, onTabletName, message2, null, db);
                                //if ( != (DbSaveResult.Success || DbSaveResult.NotApplicable))
                                //{
                                //    message = message2;
                                //    return DbSaveResult.Faliure;
                                //}
                            }

                            driver.IsLoggedOn = true;
                            driver.LoggedOnToTabletID = tablet.Id;

                            LogDb.AddLog(new Log() { LogType = "DRIVER", LogedName = loginUserName, TabletName = onTabletName, LogedID = driver.Id, LogedValue = "SIGNED_IN" }, db);



                            if (db.SaveChanges() > 0)
                            {
                                driverID = driver.Id;
                                return DbSaveResult.Success;
                            }
                            else
                                return DbSaveResult.Faliure;
                        }
                        else
                        {
                            LogDb.AddLog(new Log() { LogType = "DRIVER", LogedName = loginUserName, TabletName = onTabletName, LogedID = 0, LogedValue = "NOT_AUTHENTICATED" }, db);
                            message = "NOT_AUTHENTICATED";
                            return DbSaveResult.Faliure;
                        }
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);

                        exception = ex;
                        message = ex.Message;

                        return DbSaveResult.Error;
                    }
                }
            }

            /// <summary>
            /// Method the app call when the driver logs out of the app
            /// </summary>
            /// <param name="driverID"></param>
            /// <param name="message"></param>
            /// <param name="exception"></param>
            /// <returns></returns>
            public static DbSaveResult DriverLogOut(int driverID, string tabletName, string message, Exception exception = null, CDS_OTREntities dbToUse = null)
            {
                CDS_OTREntities db = null;
                try
                {

                    if (dbToUse == null)
                        db = new CDS_OTREntities();
                    else
                        db = dbToUse;
                    {
                        try
                        {
                            var driver = db.UnitDrivers.Where(d => d.Id == driverID).FirstOrDefault();
                            if (driver != null)
                            {
                                var wasLoggedOntoTbltName = tabletName;

                                int wasLogedOnToTabletID = 0;
                                if (driver.IsLoggedOn)
                                {
                                    driver.IsLoggedOn = false;
                                    wasLogedOnToTabletID = driver.LoggedOnToTabletID;

                                    driver.LoggedOnToTabletID = 0;
                                }

                                if (wasLogedOnToTabletID > 0)
                                {
                                    var tblt = db.Tablets.Where(t => t.Id == wasLogedOnToTabletID).FirstOrDefault();
                                    wasLoggedOntoTbltName = tblt.TabletName;
                                }

                                DAL_EF.DAL_OTR.LogDb.AddLog(new Log()
                                {
                                    LogType = "DRIVER",
                                    LogedName = driver.LoginUserName,
                                    LogedID = driver.Id,
                                    TabletName = wasLoggedOntoTbltName,
                                    LogedValue = "SIGNED_OUT"
                                }, db);

                                if (dbToUse == null)
                                {
                                    if (db.SaveChanges() > 0)
                                        return DbSaveResult.Success;
                                    else
                                        return DbSaveResult.Faliure;
                                }
                                else
                                    return DbSaveResult.NotApplicable;
                            }
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex);

                            exception = ex;
                            message = ex.Message;

                            return DbSaveResult.Error;
                        }

                        message = "Driver NOT authenticated";
                        return DbSaveResult.Faliure;
                    }
                }
                finally
                {
                    if (dbToUse == null)
                        db.Dispose();
                }
            }
        }

        /// <summary>
        /// Class to control the tablet table
        /// </summary>
        public static class TabletDb
        {
            public static DbSaveResult MarkTabletConnectionStatus(string tabletName, bool isConnected, string connectionID = "", string message = "", Exception exception = null)
            {
                using (var db = new CDS_OTREntities())
                {
                    try
                    {
                        Tablet tablet;
                        // if(isConnected)
                        tablet = db.Tablets.Where(t => t.TabletName == tabletName).FirstOrDefault();
                        //else
                        //   tablet = db.Tablets.Where(t => t.TabletName == connectionID).FirstOrDefault();
                        if (tablet != null)
                        {
                            tablet.IsConnected = isConnected;
                            tablet.ConnectionID = connectionID;

                            db.SigmalRConnectionLogs.Add(new SigmalRConnectionLog() { ConnectionID = connectionID, IsConnected = isConnected, TimeLoged = DateTime.Now });
                            //db.SaveChangesAsync();
                            if (db.SaveChanges() > 0)
                                return DbSaveResult.Success;
                            else
                                return DbSaveResult.Faliure;
                        }
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);

                        exception = ex;
                        message = ex.Message;

                        return DbSaveResult.Error;
                    }

                    message = "Driver NOT authenticated";
                    return DbSaveResult.Faliure;
                }
            }

            public static int IsTabletNameInSystem(string tabletName)
            {
                using (var db = new CDS_OTREntities())
                {
                    var tblt = db.Tablets.Where(t => t.TabletName == tabletName).FirstOrDefault();
                    if (tblt != null)
                        return tblt.Id;
                    else
                        return 0;
                }
            }



            public static void LogGPSRecording(string connectionID, double latitude, double longitude, float speed)
            {
                using (var db = new CDS_OTREntities())
                {
                    var tblt = db.Tablets.Where(t => t.ConnectionID == connectionID).FirstOrDefault();
                    if (tblt != null)
                    {
                        var drvr = db.UnitDrivers.Where(d => d.LoggedOnToTabletID == tblt.Id).FirstOrDefault();

                        db.UnitGPSLogs.Add(new UnitGPSLog() { DriverID = drvr == null ? 0 : drvr.Id, TabletID = tblt.Id, Latitude = latitude, Longitude = longitude, Speed = speed, TimeOfReading = DateTime.Now });
                        db.SaveChanges();
                    }
                }
            }
        }

        public static class DispatcherDb
        {
            public static string GetDispatchersConnectionID(int dispatcherID)
            {
                using (var db = new CDS_OTREntities())
                {
                    try
                    {
                        Dispatcher dispatcher;
                        // if(isConnected)
                        dispatcher = db.Dispatchers.Where(t => t.Id == dispatcherID).FirstOrDefault();
                        //else
                        //   tablet = db.Tablets.Where(t => t.TabletName == connectionID).FirstOrDefault();
                        if (dispatcher != null)
                            return dispatcher.ConnectionID;
                        else
                            return "";
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);

                        return "";
                    }
                }
            }

            public static DbSaveResult MarkDispatcherConnectionStatus(int dispatcherID, bool isConnected, string connectionID = "", string message = "", Exception exception = null)
            {
                using (var db = new CDS_OTREntities())
                {
                    try
                    {
                        Dispatcher dispatcher;
                        // if(isConnected)
                        dispatcher = db.Dispatchers.Where(t => t.Id == dispatcherID).FirstOrDefault();
                        //else
                        //   tablet = db.Tablets.Where(t => t.TabletName == connectionID).FirstOrDefault();
                        if (dispatcher != null)
                        {
                            dispatcher.IsConnected = isConnected;
                            dispatcher.ConnectionID = connectionID;

                            db.SigmalRConnectionLogs.Add(new SigmalRConnectionLog() { ConnectionID = connectionID, IsConnected = isConnected, TimeLoged = DateTime.Now });
                            //db.SaveChangesAsync();
                            if (db.SaveChanges() > 0)
                                return DbSaveResult.Success;
                            else
                                return DbSaveResult.Faliure;
                        }
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);

                        exception = ex;
                        message = ex.Message;

                        return DbSaveResult.Error;
                    }

                    message = "Dispatcher NOT authenticated";
                    return DbSaveResult.Faliure;
                }
            }
           
        }

        public static class ExceptionDb
        {
            public static void AddException(Exception ex)
            {
                try
                {
                    using (var db = new CDS_OTREntities())
                    {
                        db.ExceptionLogs.Add(new ExceptionLog() { ExceptionType = ex.ToString(), ExceptionMessage = ex.Message, ExceptionTrace = ex.StackTrace });
                        db.SaveChanges();
                    }
                }
                catch (Exception ex2)
                {

                }
            }
        }

        public static class LogDb
        {
            public static void AddLog(Log logInfo, CDS_OTREntities db = null)
            {
                logInfo.LogTime = DateTime.Now;
                if (db != null)
                {
                    db.Logs.Add(logInfo);
                    return;
                }

                try
                {
                    using (var db1 = new CDS_OTREntities())
                    {
                        db1.Logs.Add(logInfo);
                        db1.SaveChanges();
                    }
                }
                catch (Exception ex2)
                {

                }

            }
        }

    }

    public static class DAL
    {
        public static CDSEntities1 GetDb()
        {
            return new CDSEntities1();
        }
        public static class DriverDb
        {
            public static string GetDailyDriverTotal(int driverID, DateTime forDate)
            {
                throw new NotImplementedException();
            }

            public static BOL_OTR.DriverInfo GetDriverInfo(int driverID)
            {
                using (var db = GetDb())
                {
                    //var driverInfo = (from drivers in db.Drivers
                    //                  join comm in db.CommisionTypes
                    //                    on drivers.CommisionTypeID equals comm.CommisionTypeID into commGrp
                    //                  from c in commGrp.DefaultIfEmpty()

                    //                  join vehicles in db.Vehicles
                    //                    on drivers.DrivesVehicleID equals vehicles.VehicleID into vehGrp
                    //                  from v in commGrp.DefaultIfEmpty()

                    //                  join ezpass in db.EZPassTags
                    //                    on vehicles.VehicleID equals ezpass.VehicleID   into ezpassGrp


                    //                  where drivers.DriverID == driverID
                    //                  select new BOL_OTR.DriverInfo
                    //                  {
                    //                      DriverID = drivers.DriverID,
                    //                      FirstName = drivers.DriverFirstName,
                    //                      Lastname = drivers.DriverLastName,
                    //                      CommissionType = comm.CommisionTypeCode,
                    //                      PecentageGasDriverPays = comm.PercentOfGasDriverPays,
                    //                      IsCompanyEZPass = vehicles.IsCompanyEZPass,
                    //                      VehicleNo = vehicles.VehicleCode,
                    //                      EZPassTagNumber = ezpass.TagNumber
                    //                  }).FirstOrDefault();

                    var driverInfo = (from di in db.Driver_Comm_Vehicle_Info
                                      where di.DriverID == driverID
                                      select new BOL_OTR.DriverInfo
                                      {
                                          DriverID = di.DriverID,
                                          FirstName = di.DriverFirstName,
                                          Lastname = di.DriverLastName,
                                          CommissionType = di.CommisionTypeCode,
                                          PecentageGasDriverPays = di.PercentOfGasDriverPays ?? 0,
                                          IsCompanyEZPass = di.IsCompanyEZPass ?? false,
                                          VehicleNo = di.VehicleCode,
                                          EZPassTagNumber = di.TagNumber
                                      }).FirstOrDefault();

                    return driverInfo;
                }
            }
        }

        public static class TripSectionsDb
        {
            public static GetTripSections_Result[] GetTripSections(int tripSectionID)
            {
                using (var db = GetDb())
                {
                    var tripSections = db.GetTripSections(null);
                    return (from a in tripSections select a).ToArray();
                }
            }

            public static GetTripSections_Result[] GetPointInTripSections(double lat, double lon)
            {
                using (var db = GetDb())
                {
                    var pointIsInSections = db.GetTripSections(System.Data.Entity.Spatial.DbGeometry.FromText(string.Format("POINT({0} {1}", lat, lon), 4326));

                    return (from a in pointIsInSections select a).ToArray();
                }
            }

            //public static GetTripSections_Result[] GetAvailTripsInSection(int sectionID)
            //{
            //    using (var db = GetDb())
            //    {

            //        var pointIsInSections = db.GetTripSections(System.Data.Entity.Spatial.DbGeometry.FromText(string.Format("POINT({0} {1}", lat, lon), 4326));

            //        return (from a in pointIsInSections select a).ToArray();
            //    }
            //}
        }

        public static class TripDB
        {
            public static void ApplyPaymentToTrip(
                int tripID,
                BOL_OTR.PaymentMethodTypes pmtMethodType,
                decimal paymentAmount,
                int chargeToDriverID = 0, 
                int chargeToCustomerID = 0)
            {
                using (var db = GetDb())
                {
                    var trip = (from trips in db.Trips
                                where trips.TripID == tripID
                                select trips).FirstOrDefault();

                    if (trip != null)
                    {
                        if (chargeToDriverID > 0)
                            trip.ChargeToDriverID = chargeToDriverID;
                        if (chargeToCustomerID > 0)
                            trip.DrivenCustomerID = chargeToCustomerID;

                        switch (pmtMethodType)
                        {
                            case BOL_OTR.PaymentMethodTypes.None:
                                
                                break;
                            case BOL_OTR.PaymentMethodTypes.Cash:
                                trip.CashPaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.Check:
                                trip.CheckPaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.CC:
                                trip.CCPaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.CustomerAccount:
                                trip.ChargePaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.DriverAccount:
                                trip.ChargePaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.Ticket:
                                trip.TicketPaid = paymentAmount;
                                break;
                            case BOL_OTR.PaymentMethodTypes.NotPaid:
                                break;
                            case BOL_OTR.PaymentMethodTypes.Other:
                                break;
                            default:
                                break;
                        }
                        db.SaveChanges();
                    }
                }
            }
        }
    }


    public enum DbSaveResult
    {
        Success,
        Faliure,
        Error,
        NotApplicable
    }

}
