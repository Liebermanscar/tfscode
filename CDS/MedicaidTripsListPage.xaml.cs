﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using Rpts;
using System.Collections.ObjectModel;

namespace CDS
{
    /// <summary>
    /// Interaction logic for ReportsMain.xaml
    /// </summary>
    public partial class MedicaidTripsListPage : Page, IDisplayTextBlock
    {
        public MedicaidTripsListPage()
        {
            InitializeComponent();


        }

        private void buttonGetReport_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void buttonGetReport_Click(object sender, RoutedEventArgs e)
        {
            bool IncludeUnDrivenTrips = false;
            if (cbIncludeUnDrivenTrips.IsChecked == true)
                IncludeUnDrivenTrips = true;



            try
            {
                DateTime dt1 = new DateTime();
                if (FromDatePicker.SelectedDate != null)
                {
                    dt1 = FromDatePicker.SelectedDate.Value.Date;
                    ActiveTrips = DAL.TripDB.GetTrips(0, 0, 0, 0, dt1, dt1, true, dt1, 0, 0);


                    foreach (var tempTrip in ActiveTrips.Where(t => t.TripAreaType == TripAreaTypes.Local))//SO ill get the time of call for the local trips filled in the scheduld for field for report
                    {
                        tempTrip.ScheduledFor = tempTrip.TimeDriverAssigned;
                    }
                    if (IncludeUnDrivenTrips == true)
                    {
                        this.UnusedMedicaidTrips = DAL.MedicaidTripDB.GetMedicaidTrips(0, 0, dt1, true);
                        foreach (var mTrip in UnusedMedicaidTrips)
                        {

                            var trip = new Trip()

                            {
                                MedicaidTripInfo = new MedicaidTrip()
                                {
                                    FirstName = mTrip.FirstName,
                                    LastName = mTrip.LastName,
                                    PickUpAddress = mTrip.PickUpAddress,
                                    DropOffAddress = mTrip.DropOffAddress,
                                    CIN = mTrip.CIN,
                                    MedicaidDispatchTripNo = mTrip.MedicaidDispatchTripNo,
                                    IsManualEntry = mTrip.IsManualEntry,

                                },

                                /*  AttachedScheduledTrip = new ScheduledTrip()

                                  {//   PickUpTime = dr["PickupTime"] == DBNull.Value ? new Nullable<TimeSpan>() : (TimeSpan)dr["PickupTime"],


                                      PickupTime =mTrip.PickUpTime==null? new Nullable<DateTime>():mTrip.PickUpDate.Value.Add(mTrip.PickUpTime.Value),

                                  },*/

                                ScheduledFor = mTrip.PickUpTime == null ? new Nullable<DateTime>() : mTrip.PickUpDate.Value.Add(mTrip.PickUpTime.Value),



                            };
                            ActiveTrips.Add(trip);
                        }
                    }
                    MedicaidTripsByDateDataGrid.DataContext = ActiveTrips;
                }
                else MessageBox.Show("Select date");

            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var grid = this.MedicaidTripsByDateDataGrid;
                grid.SelectAllCells();
                grid.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, grid);

                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "Medicaid Trips"; // Default file name
                dlg.DefaultExt = ".xls"; // Default file extension
                dlg.Filter = "Excel|*.xls"; // Filter files by extension

                // Show save file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process save file dialog box results
                if (result == true)
                {
                    // Save document
                    string filename = dlg.FileName;

                    System.IO.File.WriteAllText(filename, Clipboard.GetText());

                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }
        private void ButtonShowReport_Click(object sender, RoutedEventArgs e)
        {
            //  ShowMediTRipsReport(medicaidTrips);
        }
        ObservableCollection<Trip> trips;

        public ObservableCollection<Trip> ActiveTrips
        {
            get { return trips; }
            set { trips = value; }
        }


        private ObservableCollection<MedicaidTrip> _unusedMedicaidTrips;

        public ObservableCollection<MedicaidTrip> UnusedMedicaidTrips
        {
            get { return _unusedMedicaidTrips; }
            set { _unusedMedicaidTrips = value; }
        }

        /* internal static void ShowMediTRipsReport(ObservableCollection<MedicaidTrip> medicaidTrip)
             {               
                 try
                 {  RptTrips rpt = new RptTrips();                  
                     rpt.DataSource = medicaidTrip;
                     UIController.ShowReportViewer(rpt);
                 }
                 catch (Exception ex)
                 { 
                     UIController.DisplayErrorMessage(ex); 
                 };
             }
         public ObservableCollection<MedicaidTrip> medicaidTrips { get; set; }*/

        #region

        public string HeaderText
        {
            get
            {
                return "Medicaid Trips";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

    }
}

