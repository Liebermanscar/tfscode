﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOL;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Text.RegularExpressions;

namespace CDS
{
    /// <summary>
    /// Interaction logic for SetupPage.xaml
    /// </summary>
    public partial class SetupPage : Page, IDisplayTextBlock
    {
        public SetupPage()
        {
            InitializeComponent();
        }

        public SetupPage(bool getLists)
            : this()
        {
            try
            {
                this.Drivers = DAL.DriversDB.GetDrivers(true, 0, false, true);
                this.CommisionTypes = DAL.CommisionTypesDB.GetCommisionTypes();
                this.StreetAbbrivations = DAL.StreetAbbreviationDB.GetStreetAbbreviations();
                this.TicketBooks = DAL.TicketBookDB.GetTicketBooks();
                this.Vehicles = DAL.VehicleDB.GetVehicles();
                this.LocalCities = DAL.LocalCityDB.GetLocalCities(0, "");
                this.ExpenseType = DAL.ExpenseTypeDB.GetExpenseTypes(0);
                this.MedicaidLoginInfo = DAL.MedicaidTripDB.GetMedicaidLogInInfo();

                this.FacilitySettingEmailNotificationSettings = new ObservableCollection<FacilitySetting>();//.Add(DAL.FacilitySettingsDB.GetFacilitySettings("DriverStatusChangedNotificationEmail"));
                this.FacilitySettingEmailNotificationSettings.Add(DAL.FacilitySettingsDB.GetFacilitySettings("DriverStatusChangedNotificationEmail"));

                this.AdminReportSettingEmailNotificationSettings = new ObservableCollection<FacilitySetting>();
                this.AdminReportSettingEmailNotificationSettings.Add(DAL.FacilitySettingsDB.GetFacilitySettings("AdminReportEmail"));
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }


        public ObservableCollection<Driver> Drivers
        {
            get { return this.DriversDataGrid.DataContext as ObservableCollection<Driver>; }
            set { this.DriversDataGrid.DataContext = value; }
        }

        public ObservableCollection<CommisionType> CommisionTypes
        {
            get { return this.CommisionTypesDataGrid.DataContext as ObservableCollection<CommisionType>; }
            set { this.CommisionTypesDataGrid.DataContext = value; }
        }

        public ObservableCollection<Vehicle> Vehicles
        {
            get { return this.VehiclesDataGrid.DataContext as ObservableCollection<Vehicle>; }
            set { this.VehiclesDataGrid.DataContext = value; }
        }

        public ObservableCollection<StreetAbbreviation> StreetAbbrivations
        {
            get { return this.StreetAbbreviationsDataGrid.DataContext as ObservableCollection<StreetAbbreviation>; }
            set { this.StreetAbbreviationsDataGrid.DataContext = value; }
        }

        public ObservableCollection<TicketBook> TicketBooks
        {
            get { return this.TicketBooksDataGrid.DataContext as ObservableCollection<TicketBook>; }
            set { this.TicketBooksDataGrid.DataContext = value; }
        }

        public ObservableCollection<LocalCity> LocalCities
        {
            get { return this.LocalCitiesDataGrid.DataContext as ObservableCollection<LocalCity>; }
            set { this.LocalCitiesDataGrid.DataContext = value; }
        }

        public ObservableCollection<ExpenseType> ExpenseType
        {
            get { return this.ExpenseTypeGrid.DataContext as ObservableCollection<ExpenseType>; }
            set { this.ExpenseTypeGrid.DataContext = value; }
        }

        public ObservableCollection<MedicaidLoginInfo> MedicaidLoginInfo
        {
            get { return this.MedicaidLoginGrid.DataContext as ObservableCollection<MedicaidLoginInfo>; }
            set { this.MedicaidLoginGrid.DataContext = value; }

        }




        public ObservableCollection<FacilitySetting> FacilitySettingEmailNotificationSettings
        {
            get { return this.DriverStatusNotificationSettingGrid.DataContext as ObservableCollection<FacilitySetting>; }
            set { this.DriverStatusNotificationSettingGrid.DataContext = value; }
        }

        public ObservableCollection<FacilitySetting> AdminReportSettingEmailNotificationSettings
        {
            get { return this.AdminReportNotificationSettingGrid.DataContext as ObservableCollection<FacilitySetting>; }
            set { this.AdminReportNotificationSettingGrid.DataContext = value; }
        }

        CommisionType SelectedCommisionType
        {
            get
            {
                return this.CommisionTypesDataGrid.SelectedItem as CommisionType;
            }
        }
        public Driver SelectedDriver { get { return this.DriversDataGrid.SelectedItem as Driver; } }
        public Vehicle SelectedVehicle { get { return this.VehiclesDataGrid.SelectedItem as Vehicle; } }

        private void EditDriverMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedDriver == null)
                    return;

                Driver editedDriver = UIController.AddEditDriver(this.SelectedDriver.DriverID, ModificationTypes.Edit);
                if (editedDriver != null)
                {
                    this.Drivers.Remove(this.SelectedDriver);
                    this.Drivers.Add(editedDriver);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void AddDriverMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Driver eaddedDriver = UIController.AddEditDriver(0, ModificationTypes.Add);
                if (eaddedDriver != null)
                {
                    this.Drivers.Add(eaddedDriver);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void AddCommisionTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CommisionType updatedCommisionType = UIController.AddEditCommisionType(0, ModificationTypes.Add);
                if (updatedCommisionType != null)
                {
                    this.CommisionTypes.Add(updatedCommisionType);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);
            }
        }

        private void EditCommisionTypeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.SelectedCommisionType == null)
                    return;
                CommisionType updatedCommisionType = UIController.AddEditCommisionType(this.SelectedCommisionType.CommisionTypeID, ModificationTypes.Edit);
                if (updatedCommisionType != null)
                {
                    this.CommisionTypes.Remove(this.SelectedCommisionType);
                    this.CommisionTypes.Add(updatedCommisionType);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }


        }



        #region IDisplayTextBlock Members

        public string HeaderText
        {
            get
            {
                return "Setup Page";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool AllowOnlyOnce
        {
            get
            {
                return true;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool ConfirmOnClosing
        {
            get
            {
                return false;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConfirmOnClosingText
        {
            get
            {
                return "";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void StreetAbbreviationsDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {

                    StreetAbbreviation sa = e.Row.DataContext as StreetAbbreviation;

                    if (!sa.AllowSave)
                    {
                        e.Cancel = true;
                        return;
                    }
                    try
                    {
                        if (sa.StreetAbbreviationID > 0)
                        {

                            if (!DAL.StreetAbbreviationDB.EditStreetAbbreviation(sa))
                                e.Cancel = true;

                        }
                        else
                        {
                            if (!DAL.StreetAbbreviationDB.AddStreetAbbreviation(sa))
                                e.Cancel = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }

        private void TicketBooksDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {

                    TicketBook tb = e.Row.DataContext as TicketBook;

                    if (!tb.AllowSave)
                    {
                        e.Cancel = true;
                        return;
                    }
                    try
                    {
                        if (tb.TicketBookID > 0)
                        {

                            if (!DAL.TicketBookDB.EditTicketBook(tb))
                                e.Cancel = true;

                        }
                        else
                        {
                            if (!DAL.TicketBookDB.AddTicketBook(tb))
                                e.Cancel = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void VehiclesDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {

                    Vehicle vehicle = e.Row.DataContext as Vehicle;

                    if (!vehicle.AllowSave)
                    {
                        e.Cancel = true;
                        return;
                    }
                    try
                    {
                        if (vehicle.VehicleID > 0)
                        {

                            if (!DAL.VehicleDB.EditVehicle(vehicle))
                                e.Cancel = true;

                        }
                        else
                        {
                            if (!DAL.VehicleDB.AddVehicle(vehicle))
                                e.Cancel = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void GetImageButton_Click(object sender, RoutedEventArgs e)
        {
            this.GetImage();
        }

        private void GetImage()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.CheckFileExists = true;
                if (ofd.ShowDialog().GetValueOrDefault(false))
                {
                    this.SelectedVehicle.Image = UIController.FileToByteArray(ofd.FileName);
                    if (this.SelectedVehicle.VehicleID > 0)
                        DAL.VehicleDB.EditVehicle(this.SelectedVehicle);
                    else
                        DAL.VehicleDB.AddVehicle(this.SelectedVehicle);
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void LocalCitiesDataGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {

                    LocalCity localCity = e.Row.DataContext as LocalCity;

                    if (!localCity.AllowSave)
                    {
                        e.Cancel = true;
                        return;
                    }
                    try
                    {
                        if (localCity.LocalCityID > 0)
                            e.Cancel = !DAL.LocalCityDB.EditLocalCity(localCity);
                        else
                            e.Cancel = !DAL.LocalCityDB.AddLocalCity(localCity);
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void ExpenseTypeGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    ExpenseType expenseType = e.Row.DataContext as ExpenseType;

                    if (!expenseType.AllowSave)
                    {
                        e.Cancel = true;
                        return;
                    }
                    try
                    {
                        if (expenseType.ExpenseTypeID > 0)
                            e.Cancel = !DAL.ExpenseTypeDB.EditExpenseType(expenseType);
                        else
                            e.Cancel = !DAL.ExpenseTypeDB.AddExpenseType(expenseType);
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }

        }

        private void MedicaidLoginGridGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    MedicaidLoginInfo loginInfo = e.Row.DataContext as MedicaidLoginInfo;

                    /*  if (!loginInfo.AllowSave)
                      {
                          e.Cancel = true;
                          return;
                      }*/
                    try
                    {
                        if (loginInfo.MLogInID > 0)
                            e.Cancel = !DAL.MedicaidTripDB.EditMedicaidLogin(loginInfo);//ExpenseTypeDB.EditExpenseType(loginInfo);
                        else
                            //   e.Cancel = !DAL.ExpenseTypeDB.AddExpenseType(loginInfo);
                            return;
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void DriverStatusNotificationSettingGridGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    FacilitySetting facilitySetting = e.Row.DataContext as FacilitySetting;

                    /*  if (!loginInfo.AllowSave)
                      {
                          e.Cancel = true;
                          return;
                      }*/
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(facilitySetting.SettingKey))
                            e.Cancel = !DAL.FacilitySettingsDB.EditFacilitySetting(facilitySetting);
                        else
                            //   e.Cancel = !DAL.ExpenseTypeDB.AddExpenseType(loginInfo);
                            return;
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

        private void AdminReportNotificationSettingGrid_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                if (e.EditAction == DataGridEditAction.Commit)
                {
                    FacilitySetting facilitySetting = e.Row.DataContext as FacilitySetting;

                    /*  if (!loginInfo.AllowSave)
                      {
                          e.Cancel = true;
                          return;
                      }*/
                    try
                    {
                        string[] emails = facilitySetting.SettingValue.Split(';');
                        string pattern = null;
                        pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
                        foreach (var email in emails)
                        {
                            if (!Regex.IsMatch(email, pattern))
                            {
                                MessageBox.Show("The email " + email + " Not a valid Email address ");
                                this.AdminReportSettingEmailNotificationSettings = new ObservableCollection<FacilitySetting>();
                                this.AdminReportSettingEmailNotificationSettings.Add(DAL.FacilitySettingsDB.GetFacilitySettings("AdminReportEmail"));
                                return;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(facilitySetting.SettingKey))
                            e.Cancel = !DAL.FacilitySettingsDB.EditFacilitySetting(facilitySetting);
                        else
                            //   e.Cancel = !DAL.ExpenseTypeDB.AddExpenseType(loginInfo);
                            return;
                    }
                    catch (Exception ex)
                    {
                        UIController.DisplayErrorMessage(ex);
                        e.Cancel = true;
                    }
                }
            }
            catch (Exception ex)
            {
                UIController.DisplayErrorMessage(ex);

            }
        }

    }


    public class StreetAbbreviationValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            StreetAbbreviation sa = (value as BindingGroup).Items[0] as StreetAbbreviation;
            if (string.IsNullOrWhiteSpace(sa.StreetName))
            {
                return new ValidationResult(false,
                    "Please enter a street name");
            }
            if (string.IsNullOrWhiteSpace(sa.Abbreviation))
            {
                return new ValidationResult(false,
                    "Please enter an Abbreviation");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class TicketBookValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            TicketBook tb = (value as BindingGroup).Items[0] as TicketBook;
            if (string.IsNullOrWhiteSpace(tb.BookName))
            {
                return new ValidationResult(false,
                    "Please enter a book name");
            }
            if (tb.BookPrice <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a Book Price");
            }
            if (tb.TicketValue <= 0)
            {
                return new ValidationResult(false,
                    "Please enter a Ticket Price");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }

    public class VehicleValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
            System.Globalization.CultureInfo cultureInfo)
        {
            Vehicle vehicle = (value as BindingGroup).Items[0] as Vehicle;
            if (string.IsNullOrWhiteSpace(vehicle.VehicleCode))
            {
                return new ValidationResult(false,
                    "Please enter a code name for this Vehicle");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
