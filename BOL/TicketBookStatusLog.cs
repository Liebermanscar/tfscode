﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BOL
{
    public class TicketBookStatusLog : BO
    {

        int _ticketBookStatusLogID;

        public int TicketBookStatusLogID
        {
            get { return _ticketBookStatusLogID; }
            set { _ticketBookStatusLogID = value; }
        }
        int _loggedTicketBookStatusID;

        public int LoggedTicketBookStatusID
        {
            get { return _loggedTicketBookStatusID; }
            set { _loggedTicketBookStatusID = value; }
        }
        DateTime _loggedTime;

        public DateTime LoggedTime
        {
            get { return _loggedTime; }
            set { _loggedTime = value; }
        }
        int _loggedBookStatus;

        public int LoggedBookStatus
        {
            get { return _loggedBookStatus; }
            set { _loggedBookStatus = value; }
        }
        int _sessionID;

        public int SessionID
        {
            get { return _sessionID; }
            set { _sessionID = value; }
        }
        int _inPossessionID;

        public int InPossessionID
        {
            get { return _inPossessionID; }
            set { _inPossessionID = value; }
        }
        TicketBookStatusOptions _ticketBookStatusType;

        public TicketBookStatusOptions TicketBookStatusType
        {
            get { return _ticketBookStatusType; }
            set { _ticketBookStatusType = value; }
        }

        public override bool AllowSave
        {
            get { throw new NotImplementedException(); }
        }



    }


}
